import { AppRegistry,YellowBox } from 'react-native'
import App from './src/App'
import NotificationDetail from './src/NotificationDetail'
YellowBox.ignoreWarnings([
    'Warning: componentWillMount has been renamed, and is not recommended for use. See https://fb.me/react-unsafe-component-lifecycles for details.',
    'Warning: componentWillUpdate has been renamed, and is not recommended for use. See https://fb.me/react-unsafe-component-lifecycles for details.',
    'Warning: componentWillReceiveProps has been renamed, and is not recommended for use. See https://fb.me/react-unsafe-component-lifecycles for details.',
]);
AppRegistry.registerComponent('pylonpile', () => App)
