import I18n from 'i18n-js';
import en from './locales/en';
import th from './locales/th';

// I18n.fallbacks = true;
I18n.defaultLocale = "th";
I18n.locale = "th";

I18n.translations = {
  en,
  th
};

export default I18n;
