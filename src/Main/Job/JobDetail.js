import React, { Component } from "react"
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Linking,
  Image,
  Dimensions,
  ScrollView,
  TouchableHighlight,
  Modal,
  Alert,
  BackHandler,
  Platform,
} from "react-native"
import { Container, Content } from "native-base"
import { Button, Icon } from "react-native-elements"
import ImageZoom from "react-native-image-pan-zoom"
import { getJobDetail } from "../../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"
import { MENU_GREY,GRAY,MENU_ORANGE_BORDER, MAIN_COLOR, DEFAULT_COLOR_1, DEFAULT_COLOR_4,CATEGORY_1 } from "../../Constants/Color"
import FastImage from 'react-native-fast-image'
import MapView from "react-native-maps"
import DeviceInfo from "react-native-device-info"
import I18n from '../../../assets/languages/i18n'
import Share, {ShareSheet} from 'react-native-share'
import AsyncStorage from '@react-native-community/async-storage';
class JobDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showImage: false,
      data: [],
      loading: true,
      region: {
        latitude: 13.7969258,
        longitude: 100.5674529,
        latitudeDelta: 0.0017945135245298616,
        longitudeDelta: 0.0026825442910336506
      },
      display:'',
      name:'',
      image:'',
      visible:false
    }
    // console.warn("data JobDetail",this.props.jobid)
  }

  async componentDidMount() {
    BackHandler.addEventListener("pop", () => this._handleBack())
    try {
      const value = await AsyncStorage.getItem("token")
      if (value !== null) {
        token = value
        console.log("TOKEN FROM JobDetail : ", token)
      }
    } catch (error) {
      console.log(error)
    }
    var data = {
      jobid: this.props.jobid,
      token: token,
      Language: I18n.locale
    }

    this.props.getJobDetail(data)
  }

  componentWillUnmount() {
    console.log("UNMOUNT")
    BackHandler.removeEventListener("pop")
  }

  _handleBack() {
    Actions.pop()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error != null) {
      Alert.alert("ERROR", nextProps.error, [{ text: "OK" }])
    }
    if (nextProps.success && nextProps.error == null) {
      this.setState({
        data: nextProps.data,
        loading: false,
        region: {
          latitude: nextProps.data.JobDetail.latitude,
          longitude: nextProps.data.JobDetail.longitude,
          latitudeDelta: 0.0017945135245298616,
          longitudeDelta: 0.0026825442910336506
        }
      })
    }
  }
  onCancel() {
    console.log("CANCEL")
    this.setState({visible:false})
  }
  onOpen() {
    console.log("OPEN")
    this.setState({visible:true})
  }
  _renderEmployee = () => {
    const employees = this.state.data.Employees
    console.warn('this.state.data.Employees',this.state.data.Employees[this.state.data.Employees.length-1])
    var emp = employees.map((position, index) => {
      return position.employees.map((name, index) => {
        return (
          <TouchableOpacity key={index} onPress={()=> this.setState({display:'see',name:name.nickname
          ? name.nickname + "-" + name.firstname + " " + name.lastname
          : name.firstname + " " + name.lastname,image:name.picture})}>
          <People
            left={position.position}

            right={
              name.nickname
                ? name.nickname + "-" + name.firstname + " " + name.lastname
                : name.firstname + " " + name.lastname
            }
            // subright={name.job_position_name}
            mobile={name.mobile}
            key={index}
          />
          </TouchableOpacity>
        )
      })
    })
    if (emp.length < 1) return null
    return (
      <View style={{ marginTop: 10 }}>
        <Topic topic={I18n.t('jobdetail.projectProsonel')} />
        {emp}
      </View>
    )
  }
  renderPreview() {
    const width = Dimensions.get("window").width
    const height = Dimensions.get("window").height

    
    return (<View style={styles.container}>
      <View style={[styles.navigation, 
        styles.navigationBlue]}>
        <View style={[styles.navigationTitle,{flexDirection:'row'}]}>
          
          <Text style={[styles.navigationTitleText,{flex:6}]}>{this.state.name}</Text>
          <TouchableOpacity  style={[styles.buttonRight,{flex:1}]} onPress={() => this.setState({display:''})}>
            <Image source={require('../../../assets/image/icon_cancel.png')} style={styles.buttonLeftResize} />
          </TouchableOpacity>
        </View>
      </View>
      
      <View style={styles.previewContainer}>
        <View style={styles.preview}>
          <ImageZoom cropWidth={width}
            cropHeight={height}
            imageWidth={350}
            imageHeight={350}
            panToMove={"true"}
            pinchToZoom={"true"}
          >
            <FastImage
              style={styles.previewImg}
              source={{
                uri: this.state.image.search('http') == 0 ? this.state.image + '?' + Math.random() : this.state.image,
                priority: FastImage.priority.low,
              }}
              resizeMode={FastImage.resizeMode.contain}
            />
            <View style={styles.previewCropImg}></View>
            {/* {
            this.state.loading == true ?
            <View style={styles.previewCropImg}>
              <ActivityIndicator size="large" color="#007CC2" />
            </View>
            :
            <View/>
          } */}
          </ImageZoom>
        </View>
        {/* <Text style={styles.capture} onPress={() => this.onPreviewContinue()}>[Continue]</Text> */}
      </View>
    </View>)
  }
  _renderImage() {
    const width = Dimensions.get("window").width
    const height = Dimensions.get("window").width * 3 / 4
    const image = this.state.data.JobDetail.map_url
    if (!image) return null
    return (
      <View style={{ marginTop: 10 }}>
        <Topic topic={I18n.t('jobdetail.map')} />
        <View style={{ height: "100%", width: "100%" }}>
          <TouchableHighlight onPress={() => this.setState({ showImage: true })}>
            <Image source={{ uri: image }} style={{ width: width, height: height }} resizeMode="contain" />
          </TouchableHighlight>
        </View>
      </View>
    )
  }

  _renderMap() {
    const region = {
      latitude: 13.7969258,
      longitude: 100.5674529,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421
    }
    const width = Dimensions.get("window").width
    const height = Dimensions.get("window").width * 3 / 4
    return (
      <View
        style={{
          flex: 1,
          minHeight: 500
        }}
      >
        <MapView initialRegion={region} style={{ flex: 1 }} />
      </View>
    )
  }

  render() {
    const job = this.state.data.JobDetail || []
    const type = this.state.data.TypePiles || []
    const width = Dimensions.get("window").width
    const height = Dimensions.get("window").width * 3 / 4
    const initRegion = {
      latitude: 13.7969258,
      longitude: 100.5674529,
      latitudeDelta: 0.0017945135245298616,
      longitudeDelta: 0.0026825442910336506
    }
    if (this.state.loading) {
      return null
    }
    if(this.state.display==''){
    return (
      <Container style={{backgroundColor:GRAY}}>
        <Modal visible={this.state.showImage} onRequestClose={() => this.setState({ showImage: false })}>
          <TouchableOpacity onPress={() => this.setState({ showImage: false })}>
            <View style={styles.closeButton2}>
              <Icon name="close" type="evilicons" color="black" style={{ marginRight: 10 }} />
            </View>
          </TouchableOpacity>
          <ImageZoom
            cropWidth={width}
            cropHeight={Dimensions.get("window").height - 30}
            imageWidth={width}
            imageHeight={Dimensions.get("window").height - 30}
          >
            <Image
              style={{ width: width, height: Dimensions.get("window").width - 30 }}
              source={{ uri: job.map_url }}
              resizeMode="contain"
            />
          </ImageZoom>
        </Modal>
        <Content>
          <View style={{ marginTop: 5 }}>
            <Topic topic={I18n.t('jobdetail.projectInfomation')} />
            <SubTopic left={I18n.t('jobdetail.workowner')} right={job.company} />
            <SubTopic left={I18n.t('jobdetail.workNo')} right={job.job_no} />
            <SubTopic left={I18n.t('jobdetail.projectNamefull')} right={job.job_name} />
            <SubTopic left={I18n.t('jobdetail.projectNameinitial')} right={job.job_shortname} />
            <SubTopic left={I18n.t('jobdetail.projectowner')} right={job.owner} />
            <SubTopic left={I18n.t('jobdetail.quotationNo')} right={job.boq} />
            <SubTopic left={I18n.t('jobdetail.costandmaterail')} right={job.labour_type} />
            <SubTopic left={I18n.t('jobdetail.Employer')} right={job.client} />
            <SubTopic left={I18n.t('jobdetail.workstart')} right={job.start_date} />
            <SubTopic left={I18n.t('jobdetail.contractperiod')} right={job.total_date + ` ${I18n.t('jobdetail.day')}`} />
            <SubTopic left={I18n.t('jobdetail.date')} right={job.end_date} />
          </View>
          <View style={{ marginTop: 10 }}>
            <Topic topic={I18n.t('jobdetail.type')} />
            {type.Details &&
              type.Details.map((pile, index) => {
                return <SubTopic left={pile.ProductType} right={pile.Total} key={index} unit={pile.UnitNo} />
              })}
            {type.TotalBP ? (<SubTopic left={I18n.t('jobdetail.ballpile')} right={type.TotalBP || '0'} main unit={type.UnitNoBP || I18n.t('jobdetail.UnitNoBP')} />) : null}
            {type.TotalDW ? (<SubTopic left={I18n.t('jobdetail.dwallpile')} right={type.TotalDW || '0'} main unit={type.UnitNoDW || I18n.t('jobdetail.UnitNoDW')} />) : null}
          </View>
          {this._renderEmployee()}
          {job.latitude &&
            job.longitude && (
              <View>
                <View style={{ marginTop: 10 }}>
                  <Topic topic={I18n.t('jobdetail.map2')} />
                </View>
                <View style={{ height: 300 }}>
                  <MapView
                    // initialRegion={initRegion}
                    style={{ flex: 1 }}
                    region={this.state.region}
                    onRegionChangeComplete={region => this.setState({ region: region })}
                  >
                    <MapView.Marker
                      coordinate={{ latitude: job.latitude, longitude: job.longitude }}
                      title={job.job_shortname}
                      onPress={e =>
                        Linking.openURL(
                          "geo:0,0" + "?q=" + job.latitude + "," + job.longitude + `(${job.job_shortname})`
                        )
                      }
                    />
                  </MapView>
                </View>
              </View>
            )}

          {this._renderImage()}
        </Content>
      </Container>
    )
  }else if(this.state.display == 'see'){
    return (this.renderPreview())
  }
  }
}

const mapStateToProps = state => {
  return {
    success: state.jobdetail.success,
    error: state.jobdetail.error,
    data: state.jobdetail.data
  }
}

export default connect(mapStateToProps, { getJobDetail })(JobDetail)

const styles = StyleSheet.create({
  topic: {
    backgroundColor: "#FFF",
    height: DeviceInfo.isTablet() ? 60 : 40,
    justifyContent: "center"
  },
  textTopic: {
    marginLeft: 12,
    color: "#007CC2",
    fontSize: DeviceInfo.isTablet() ? 30 : 20,
    fontFamily: "THSarabunNewBold",
    fontWeight: "bold"
  },
  sub: {
    backgroundColor: "#FFF",
    marginTop: 2,
    flexDirection: "row",
    height: "auto",
    alignItems: "center",
    paddingTop: 2,
    paddingBottom: 2
  },
  subtopic: {
    width: "40%"
  },
  textSubTopic: {
    marginLeft: 12,
    fontFamily: "THSarabunNewBold",
    fontSize: DeviceInfo.isTablet() ? 22 : 15
  },
  closeButton2: {
    borderColor: "black",
    padding: 8,
    borderRadius: 3,
    margin: 10,
    alignSelf: "flex-end"
  },container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  navigation: {
    paddingTop: Platform.OS === 'ios'
      ? 25
      : 10,
    backgroundColor: 'transparent',
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1
  },
  buttonRight: {
    // position: 'absolute',
    // top: 0,
    // right: 20
  },
  buttonLeftResize: {
    width: 25,
    height: 25
  },
  navigationTitleText: {
    color: '#fff',
    fontSize: 20,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10
  },
  previewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    backgroundColor: '#fff'
  },
  preview: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: 'rgba(0,0,0,0.9)'
  },
  previewImg: {
    width: '100%',
    height: '100%',
    // resizeMode: 'contain'
  },
  previewCropImg: {
    width: 350,
    height: 350,
    borderRadius: 350,
    position: 'absolute'
  },
  navigationBlue: {
    backgroundColor: '#007cc2',
    borderBottomWidth: 1,
    borderColor: '#848789'
  },
})

class People extends Component {
  render() {
    return (
      <View style={[styles.sub, { height: 70 }]}>
        <View style={styles.subtopic}>
          <Text style={styles.textSubTopic}>{this.props.left}</Text>
        </View>
        <View style={{ width: "45%" }}>
          <Text style={styles.textSubTopic}>{this.props.right}</Text>
          <Text style={styles.textSubTopic}>{this.props.mobile}</Text>
          {/* <Text style={styles.textSubTopic}>{ this.props.subright}</Text> */}
        </View>
        <View>
          {this.props.mobile ? (
            <Icon
              name="phone"
              type="font-awesome"
              color="#57c3e8"
              onPress={() => Linking.openURL("tel:" + this.props.mobile)}
            />
          ) : (
            <Icon name="phone" type="font-awesome" color={MENU_GREY} />
          )}
        </View>
      </View>
    )
  }
}

class Topic extends Component {
  render() {
    return (
      <View style={styles.topic}>
        <Text style={styles.textTopic}>{this.props.topic}</Text>
      </View>
    )
  }
}

class SubTopic extends Component {
  render() {
    return (
      <View style={[styles.sub, this.props.main && { backgroundColor: "#57c3e8" }]}>
        <View style={[styles.subtopic, this.props.unit && { width: "70%"}]}>
          <Text style={styles.textSubTopic}>{this.props.left}</Text>
        </View>
        <View style={this.props.unit ? { width: "15%" } : { width: "60%" }}>
          <Text style={[styles.textSubTopic, this.props.unit && { textAlign: 'right' }]}>{this.props.right}</Text>
        </View>
        {this.props.unit && (
          <View style={{ width: "15%"}}>
            <Text style={[styles.textSubTopic]}>{this.props.unit}</Text>
          </View>
        )}
      </View>
    )
  }
}
