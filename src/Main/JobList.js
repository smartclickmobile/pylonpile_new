import React, { Component } from "react"
import { FlatList, StyleSheet, Text, View, Alert, BackHandler } from "react-native"
import { Container, Content, Card, CardItem, Body, Right, List, ListItem, Button, Separator } from "native-base"
import { Icon, CheckBox } from "react-native-elements"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import { jobItems, getUserinfo } from "../Actions"
import Spinner from "react-native-loading-spinner-overlay"
import DeviceInfo from "react-native-device-info"
import firebase from "react-native-firebase"
import remoteConfigDefaults from "../Constants/remoteConfigDefaults"
import I18n from '../../assets/languages/i18n'
import {GRAY} from '../Constants/Color'
const config = firebase.config()
// config.setDefaults(remoteConfigDefaults)

class JobList extends Component {
  constructor(props) {
    super(props)

    this.state = {
      data: [],
      loading: true,
      showall: false,
      checkbox: I18n.t('joblist.myjobonly'),
      mywork: I18n.t('joblist.myjob'),
      otherwork: I18n.t('joblist.otherjob')
    }
  }

  componentDidMount() {
    this.setFirebaseCurrentScreen()
    // this.fetchFirebaseConfig()
    this.fetchData()
    BackHandler.addEventListener("backJob", () => this.onBackPress())
    var data ={
      Language:I18n.locale
    }
    this.props.getUserinfo(data)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("backJob")
  }

  setFirebaseCurrentScreen() {
    firebase.analytics().setCurrentScreen("JobList")
  }

  fetchFirebaseConfig() {
    const values = ["joblist_checkbox_th", "joblist_mywork_th", "joblist_otherwork_th"]
    config
      .fetch()
      .then(() => config.activateFetched())
      .then(() => config.getValues(values))
      .then(data => {
      this.setState({ 
        checkbox: data.joblist_checkbox_th.val(),
        mywork: data.joblist_mywork_th.val(),
        otherwork: data.joblist_otherwork_th.val()
      })
    })
    .catch(err => Alert.alert(err.message))
  }

  onBackPress() {
    return true
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error != null) {
      this.setState({ loading: false })
      Alert.alert("ERROR", nextProps.error, [{ text: "OK" }])
    }
    if (nextProps.success && nextProps.error == null) {
      this.setState({ data: nextProps.data, loading: false })
    }
  }

  fetchData() {
    this.props.jobItems()
  }

  async iconPressed(jobid) {
    Actions.jobdetail({ title: I18n.t('projectlist.projectinfo'), jobid: jobid })
  }

  async projectPress(title, jobid) {
    console.log('projectPress',title, jobid)
    Actions.projectlist({ title: "", project: title, jobid: jobid })
  }

  renderItem = (rowData, sectionID, rowID) => {
    return (
      <ListItem
        key={rowData.item.id}
        style={{ backgroundColor: "transparent" }}
        onPress={() => {
          if (rowData.item.permission_flag || __DEV__)
            this.projectPress(rowData.item.jobname, rowData.item.jobid)

        }}
        selected={!rowData.item.permission_flag}
      >
        <Body>
          <Text style={styles.textTitle}>{rowData.item.jobname}</Text>
          <Text style={styles.textDescription}>
            {I18n.t('joblist.amountfinishpile')} {rowData.item.done}/{rowData.item.piles}
          </Text>
        </Body>
        <Right>
          <Icon
            reverse
            raised
            name="file-text"
            type="font-awesome"
            color="#007CC2"
            size={DeviceInfo.isTablet() ? 40 : 20}
            onPress={() => this.iconPressed(rowData.item.jobid)}
          />
        </Right>
      </ListItem>
    )
  }

  renderList = item => {
    return (
      <ListItem
        key={item.id}
        style={{ backgroundColor: "transparent" }}
        onPress={() => this.projectPress(item.jobname, item.jobid)}
        disabled={!item.permission_flag}
      >
        <Body>
          <Text style={styles.textTitle}>{item.jobname}</Text>
          <Text style={styles.textDescription}>
            {I18n.t('joblist.amountfinishpile')} {item.done}/{item.piles}
          </Text>
        </Body>
        <Right>
          <Icon
            reverse
            raised
            name="file-text"
            type="font-awesome"
            color="#007CC2"
            size={DeviceInfo.isTablet() ? 40 : 20}
            onPress={() => this.iconPressed(item.jobid)}
          />
        </Right>
      </ListItem>
    )
  }

  render() {
    // console.warn('search',this.props.result,this.props.noperm.length)
    if (this.state.loading) {
      return (
        <Container style={{backgroundColor:GRAY}}>
          <Content>
            <View style={{ flex: 1 }}>
              <Spinner
                visible={this.state.loading}
                textContent={"Loading..."}
                textStyle={{ color: "#FFF" }}
                overlayColor="rgba(0, 0, 0, 0.5)"
              />
            </View>
          </Content>
        </Container>
      )
    } else {
      return (
        <Container style={{backgroundColor:GRAY}}>
          <Content keyboardShouldPersistTaps="always">
            <View style={[{ margin: 1 }, this.props.searching ? { marginTop: 30 } : {}]}>
              <View style={styles.checkbox}>
                <CheckBox
                  title={this.state.checkbox}
                  checked={this.state.showall}
                  onPress={() => this.setState({ showall: !this.state.showall })}
                  size={DeviceInfo.isTablet() ? 35 : 24}
                />
              </View>
              {this.props.haveperm.length > 0 && (
                <View>
                  <View style={styles.header}>
                    <Text style={styles.headerText}>{this.state.mywork}</Text>
                  </View>
                  <FlatList
                    data={this.props.haveperm.filter(item => {
                      return this.props.result != null
                        ? item.jobname.toLowerCase().search(this.props.result.toLowerCase()) >= 0
                        : true
                    })}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => index.toString()}
                    refreshing={this.state.isRefreshing}
                    onRefresh={this.onRefresh}
                    keyboardShouldPersistTaps="always"
                  />
                </View>
              )}
              {this.props.noperm.length > 0 &&
                !this.state.showall && (
                  <View>
                    <View style={styles.header}>
                      <Text style={styles.headerText}>{this.state.otherwork}</Text>
                    </View>
                    <FlatList
                      data={this.props.noperm.filter(item => {
                        return this.props.result != null
                          ? item.jobname.toLowerCase().search(this.props.result.toLowerCase()) >= 0
                          : true
                      })}
                      renderItem={this.renderItem}
                      keyExtractor={(item, index) => index.toString()}
                      refreshing={this.state.isRefreshing}
                      onRefresh={this.onRefresh}
                      keyboardShouldPersistTaps="always"
                    />
                  </View>
                )}
            </View>
            <Text>{this.state.result}</Text>
          </Content>
        </Container>
      )
    }
  }
}

const mapStateToProps = state => {
  return {
    success: state.joblist.success,
    error: state.joblist.error,
    data: state.joblist.data,
    haveperm: state.joblist.haveperm,
    noperm: state.joblist.noperm
  }
}

const styles = StyleSheet.create({
  textTitle: {
    fontSize: DeviceInfo.isTablet() ? 30 : 16,
    fontFamily: "THSarabunNew",
    fontWeight: "bold"
  },
  textDescription: {
    fontSize: DeviceInfo.isTablet() ? 32 : 18,
    fontFamily: "THSarabunNew"
  },
  checkbox: {
    height: DeviceInfo.isTablet() ? 70 : 30,
    backgroundColor: "#FFF",
    alignItems: "flex-end"
  },
  header: {
    height: DeviceInfo.isTablet() ? 60 : 40,
    backgroundColor: "#cac8c8",
    justifyContent: "center"
  },
  headerText: {
    marginLeft: 10,
    fontSize: DeviceInfo.isTablet() ? 30 : 17
  }
})

export default connect(mapStateToProps, { jobItems,getUserinfo })(JobList)
