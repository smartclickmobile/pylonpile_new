import React, { Component } from "react"
import { View, Text, StyleSheet, FlatList, Alert, TouchableOpacity, ActivityIndicator } from "react-native"
import { process1, process2, process3, process4, process5, process6, process7, process8, process9, process10, process11 } from '../assets/historytext/historytext'
import { process1en, process2en, process3en, process4en, process5en, process6en, process7en, process8en, process9en, process10en, process11en } from '../assets/historytext/historytexten'
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "./Actions"
import { Container, Content } from "native-base"
import { MAIN_COLOR, SUB_COLOR } from "./Constants/Color"
import ModalSelector from "react-native-modal-selector"
import I18n from "./../assets/languages/i18n"
import { Icon, Button } from "react-native-elements"

class History extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [
        { key: 100, section: true, label: I18n.t('history.process') },
        { key: 0, label: I18n.t('history.all') },
        { key: 1, label: I18n.t("mainpile.step.1") },
        { key: 2, label: I18n.t("mainpile.step.2") },
        { key: 3, label: I18n.t("mainpile.step.3") },
        { key: 4, label: I18n.t("mainpile.step.4") },
        { key: 5, label: this.props.category == 1 || this.props.category == 4?I18n.t("mainpile.step.5"):I18n.t("mainpile.step.5_dw") },
        { key: 6, label: I18n.t("mainpile.step.6") },
        { key: 7, label: I18n.t("mainpile.step.7") },
        { key: 8, label: I18n.t("mainpile.step.8") },
        { key: 9, label: I18n.t("mainpile.step.9") },
        { key: 10, label: I18n.t("mainpile.step.10") },
        { key: 11, label: I18n.t("mainpile.step.11") }
      ],
      data_dw:[
        { key: 100, section: true, label: I18n.t('history.process') },
        { key: 0, label: I18n.t('history.all') },
        { key: 1, label: I18n.t("mainpile.step.1") },
        { key: 2, label: I18n.t("mainpile.step.2") },
        { key: 3, label: I18n.t("mainpile.step.3") },
        { key: 4, label: I18n.t("mainpile.step.4") },
        { key: 5, label: this.props.category == 1 || this.props.category == 4?I18n.t("mainpile.step.5"):I18n.t("mainpile.step.5_dw") },
        { key: 6, label: I18n.t("mainpile.step.6") },
        { key: 7, label: I18n.t("mainpile.step.7") },
        { key: 8, label: I18n.t("mainpile.step.8") },
        { key: 9, label: I18n.t("mainpile.step.9") },
        { key: 10, label: I18n.t("mainpile.step.10") },
        { key: 11, label: I18n.t("mainpile.step.11") }
      ],
      history: [],
      process: this.props.index + 1,
      page: 1,
      dropdown: I18n.t('history.all'),
      end: false,
      loading: true,
      jobdata: [{ key: 1000, id: 100, section: true, label: "โครงการ" }, { key: 0, id: 0, label: I18n.t('history.all') }],
      joblabel: I18n.t('history.all'),
      jobid: null,
      pileid: null,
      historyfilter:[]
    }
  }

  // componentDidMount() {
  //   console.log('DID mount')
  //   console.log('PROPS', this.props.pileid, this.props.jobid)
  //   console.log('STATE', this.state.pileid, this.state.jobid)

  //   this.setState({
  //     pileid: this.props.pileid,
  //     jobid: this.props.jobid
  //   })
  // }

  componentWillReceiveProps(nextProps) {
    console.log("props")
    if(nextProps.error == null) {
      this.setState({ history: nextProps.history, end: nextProps.end, loading: false })
    }
    if(nextProps.error != null) {
      this.setState({ loading: false })
      Alert.alert("ERROR", nextProps.error, [{ text: "OK" }])
    }

    if(nextProps.joblist && nextProps.joblist.length + 2 != this.state.jobdata.length) {
      var jobdata = [{ key: 1000, id: 100, section: true, label: I18n.t('history.project') }, { key: 0, id: 0, label: I18n.t('history.all') }]
      nextProps.joblist.map((data, index) => {
        jobdata.push({ id: data.jobid, label: data.jobname, key: index + 1 })
        if(this.props.jobid == data.jobid) {
          this.setState({ joblabel: data.jobname })
        }
      })
      this.setState({ jobdata })
    }
    // console.log('history',nextProps.history)
    if(nextProps.history != [] && nextProps.history != null){
      var data = []
      for(var i = 0;i<nextProps.history.length;i++){
        if(i == 0){
          data.push({
            CreatedDate: nextProps.history[i].CreatedDate,
            Name: (nextProps.history[i].Employee_NickName &&nextProps.history[i].Employee_NickName + "-") +
            nextProps.history[i].Employee_FirstName +
            " " +
            nextProps.history[i].Employee_LastName,
            ProcessName: nextProps.history[i].ProcessName,
            DeviceName:nextProps.history[i].DeviceName,
            ChildId:nextProps.history[i].ChildId,
            ProcessId:nextProps.history[i].ProcessId,
            list:[{
              Title:nextProps.history[i].Title,
              Detail:nextProps.history[i].Detail,
              process:nextProps.history[i].ProcessId
            }]
          })
        }
        else{
          if(
            nextProps.history[i].CreatedDate != data[data.length-1].CreatedDate
           
          ){
            data.push({
              CreatedDate: nextProps.history[i].CreatedDate,
              Name: (nextProps.history[i].Employee_NickName &&nextProps.history[i].Employee_NickName + "-") +
              nextProps.history[i].Employee_FirstName +
              " " +
              nextProps.history[i].Employee_LastName,
              ProcessName: nextProps.history[i].ProcessName,
              DeviceName:nextProps.history[i].DeviceName,
              ChildId:nextProps.history[i].ChildId,
              ProcessId:nextProps.history[i].ProcessId,
              list:[{
                Title:nextProps.history[i].Title,
                Detail:nextProps.history[i].Detail,
                process:nextProps.history[i].ProcessId
              }]
            })
          }else{
            data[data.length-1].list.push({
              Title:nextProps.history[i].Title,
              Detail:nextProps.history[i].Detail,
              process:nextProps.history[i].ProcessId
            })
          }
        }
      }
      // console.warn(data.length,data[1])
      this.setState({historyfilter:data})
    }
  }

  componentWillMount() {
    console.log("Will mount")
    console.log("props", this.props.pileid, this.props.jobid)
    console.log("state", this.state.pileid, this.state.jobid)
    var dropdown = ""
    var number = this.props.index + 1
    if(this.props.category == 1 || this.props.category == 4){
      dropdown = I18n.t("mainpile.step." + number)
    }else{
      if(number == 5){
        dropdown = I18n.t("mainpile.step.5_dw")
      }else{
        dropdown = I18n.t("mainpile.step." + number)
      }
    }
    


    this.setState(
      {
        pileid: this.props.pileid,
        jobid: this.props.jobid,
        dropdown
      },
      () => this.loadHistory()
    )
  }

  loadHistory() {
    data = {
      page: this.state.page,
      process: this.state.process,
      jobid: this.state.jobid == 0 ? null : this.state.jobid,
      pileid: this.state.pileid == 0 ? null : this.state.pileid
    }
    console.log("DATA", data)
    this.props.historyList(data)
    // this.props.historyList(this.state.page, this.state.process)
  }

  gethistorytext(process,title,processid){
    // console.warn('process',process)
    if(this.props.category != 1 && this.props.category != 4){
      title = title+'_dw'
    }
    // console.warn('title',title)
    if(I18n.locale == 'th'){
      if(process == 1){
        return process1[title] != undefined?process1[title]:title
      }else if(process == 2){
        return process2[title]!= undefined?process2[title]:title
      }else if(process == 3){
        return process3[title]!= undefined?process3[title]:title
      }else if(process == 4){
        return process4[title]!= undefined?process4[title]:title
      }else if(process == 5){
        return process5[title]!= undefined?process5[title]:title
      }else if(process == 6){
        return process6[title]!= undefined?process6[title]:title
      }else if(process == 7){
        return process7[title]!= undefined?process7[title]:title
      }else if(process == 8){
        return process8[title]!= undefined?process8[title]:title
      }else if(process == 9){
        return process9[title]!= undefined?process9[title]:title
      }else if(process == 10){
        return process10[title]!= undefined?process10[title]:title
      }else if(process == 11){
        return process11[title]!= undefined?process11[title]:title
      }else if(process == 0){
        if(processid == 1){
          return process1[title] != undefined?process1[title]:title
        }else if(processid == 2){
          return process2[title]!= undefined?process2[title]:title
        }else if(processid == 3){
          return process3[title]!= undefined?process3[title]:title
        }else if(processid == 4){
          return process4[title]!= undefined?process4[title]:title
        }else if(processid == 5){
          return process5[title]!= undefined?process5[title]:title
        }else if(processid == 6){
          return process6[title]!= undefined?process6[title]:title
        }else if(processid == 7){
          return process7[title]!= undefined?process7[title]:title
        }else if(processid == 8){
          return process8[title]!= undefined?process8[title]:title
        }else if(processid == 9){
          return process9[title]!= undefined?process9[title]:title
        }else if(processid == 10){
          return process10[title]!= undefined?process10[title]:title
        }else if(processid == 11){
          return process11[title]!= undefined?process11[title]:title
        }
      }
    }else{
      if(process == 1){
        return process1en[title] != undefined?process1en[title]:title
      }else if(process == 2){
        return process2en[title]!= undefined?process2en[title]:title
      }else if(process == 3){
        return process3en[title]!= undefined?process3en[title]:title
      }else if(process == 4){
        return process4en[title]!= undefined?process4en[title]:title
      }else if(process == 5){
        return process5en[title]!= undefined?process5en[title]:title
      }else if(process == 6){
        return process6en[title]!= undefined?process6en[title]:title
      }else if(process == 7){
        return process7en[title]!= undefined?process7en[title]:title
      }else if(process == 8){
        return process8en[title]!= undefined?process8en[title]:title
      }else if(process == 9){
        return process9en[title]!= undefined?process9en[title]:title
      }else if(process == 10){
        return process10en[title]!= undefined?process10en[title]:title
      }else if(process == 11){
        return process11en[title]!= undefined?process11en[title]:title
      }else if(process == 0){
        if(processid == 1){
          return process1en[title] != undefined?process1en[title]:title
        }else if(processid == 2){
          return process2en[title]!= undefined?process2en[title]:title
        }else if(processid == 3){
          return process3en[title]!= undefined?process3en[title]:title
        }else if(processid == 4){
          return process4en[title]!= undefined?process4en[title]:title
        }else if(processid == 5){
          return process5en[title]!= undefined?process5en[title]:title
        }else if(processid == 6){
          return process6en[title]!= undefined?process6en[title]:title
        }else if(processid == 7){
          return process7en[title]!= undefined?process7en[title]:title
        }else if(processid == 8){
          return process8en[title]!= undefined?process8en[title]:title
        }else if(processid == 9){
          return process9en[title]!= undefined?process9en[title]:title
        }else if(processid == 10){
          return process10en[title]!= undefined?process10en[title]:title
        }else if(processid == 11){
          return process11en[title]!= undefined?process11en[title]:title
        }
    }
    
    }
  
}

  renderItem = (row, section, id) => {
    console.log('row.item',row.item)
    return (
      <View style={styles.rowView}>
      <View style={{borderBottomColor:SUB_COLOR,borderBottomWidth:1}}>
      <Row left={I18n.t('history.date')} right={row.item.CreatedDate} />
        <Row
          left={I18n.t('history.user')}
          right={
            // (row.item.Employee_NickName && row.item.Employee_NickName + "-") +
            // row.item.Employee_FirstName +
            // " " +
            // row.item.Employee_LastName
            row.item.Name
          }
        />
        <Row left={I18n.t('history.process')} right={row.item.ProcessName} />
        {
          row.item.ChildId != null && row.item.ChildId != '' ?
          <Row left={I18n.t('history.ChildId')} right={row.item.ChildId} />
          :<View/>
        }
        <Row left={I18n.t('history.phonid')} right={row.item.DeviceName} />
        {/*<Row left="รหัสเครื่อง" right={row.item.DeviceName} />
        <Row left="หัวข้อที่แก้ไข" right={row.item.Title} />
        <Row left="รายละเอียด" right={row.item.Detail} />*/}
      </View>
      {row.item.list.map((item, index)=>{
        // console.warn(item)
        return(
          <View style={{borderBottomColor:'#ccc',borderBottomWidth:1}}>
          <Row left={I18n.t('history.title')} right={this.gethistorytext(this.state.process,item.Title,item.process)} />
          <Row left={I18n.t('history.detail')} right={item.Detail} />
        </View>
        )
       
      })

      }
      {/*<FlatList
            data={
              row.item.list
            }
            renderItem={this.renderItem2}
            keyExtractor={(item, index) => index.toString()}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0.5}
            ListFooterComponent={<ActivityIndicator animating={this.state.loading} />}
            refreshing={this.state.loading}
            onRefresh={this.refresh}
          />*/}
      </View>
    )
  }

  renderItem2 = (row, section, id) => {
    return (
      <View style={{borderBottomColor:'#ccc',borderBottomWidth:1}}>
        <Row left={I18n.t('history.title')} right={row.item.Title} />
        <Row left={I18n.t('history.detail')} right={row.item.Detail} />
      </View>
    )
  }

  changeDropdown(option) {
    this.setState({ dropdown: option.label, process: option.key, page: 1 }, () => this.loadHistory())
  }

  changeJobdata(option) {
    this.setState({ joblabel: option.label, jobid: option.id, page: 1 }, () => this.loadHistory())
  }

  refresh = () => {
    this.setState({ loading: true, page: 1 }, () => this.loadHistory())
  }

  handleLoadMore = () => {
    if(!this.state.end && !this.state.loading) {
      console.log("handleLoadMore")
      this.setState({ loading: true, page: this.state.page + 1 }, () => {
        data = {
          page: this.state.page,
          process: this.state.process,
          jobid: this.state.jobid == 0 ? null : this.state.jobid,
          pileid: this.state.pileid == 0 ? null : this.state.pileid
        }
        this.props.historyListMore(data)
      })
    }
  }

  renderDropdown = () => (
    <View style={styles.dropdown}>
      { /**
         <View>
        <ModalSelector
          data={this.state.jobdata}
          initValue={this.state.joblabel}
          onChange={option => this.changeJobdata(option)}
          cancelText="Cancel"
        >
          <TouchableOpacity style={styles.button2} underlayColor="white">
            <View style={styles.buttonTextStyle}>
              <Text style={styles.textButton} numberOfLines={0}>
                {this.state.joblabel}
              </Text>
              <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                <Icon name="arrow-drop-down" type="MaterialIcons" color="#6bacd5" />
              </View>
            </View>
          </TouchableOpacity>
        </ModalSelector>
      </View>
       */ }
      <View>
        <ModalSelector
          data={this.props.category == 1 || this.props.category == 4?this.state.data:this.state.data_dw}
          initValue={this.state.dropdown}
          onChange={option => this.changeDropdown(option)}
          cancelText="Cancel"
        >
          <TouchableOpacity style={styles.button2} underlayColor="white">
            <View style={styles.buttonTextStyle}>
              <Text style={[styles.textButton,{fontSize:I18n.locale=='en'?16:20}]} numberOfLines={2}>
                {this.state.dropdown}
              </Text>
              <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                <Icon name="arrow-drop-down" type="MaterialIcons" color="#6bacd5" />
              </View>
            </View>
          </TouchableOpacity>
        </ModalSelector>
      </View>
    </View>
  )

  render() {
    return (
      <Container>
        {__DEV__ && (
          <Text>
            INDEX -> {this.props.index}
          </Text>
        )}
        <View style={styles.topicView}>
          <Text style={styles.topic}>{I18n.t('drawer.history')}</Text>
          {this.renderDropdown()}
        </View>
        <View style={styles.flatlist}>
          <FlatList
            data={
              // this.state.history.filter(item => {
              // if(this.state.process == 0) return true
              // return item.ProcessId == this.state.process
              // })
              this.state.historyfilter
            }
            renderItem={this.renderItem}
            keyExtractor={(item, index) => index.toString()}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0.5}
            ListFooterComponent={<ActivityIndicator animating={this.state.loading} />}
            refreshing={this.state.loading}
            onRefresh={this.refresh}
          />
        </View>
      </Container>
    )
  }
}



const styles = StyleSheet.create({
  rowView: {
    margin: 10,
    borderWidth: 1,
    borderColor: SUB_COLOR,
    borderRadius: 10,
    backgroundColor: "#FFF"
  },
  row: {
    height: "auto",
    minHeight: 30,
    flexDirection: "row",
    borderColor: SUB_COLOR
  },
  leftBox: {
    width: "40%",
    justifyContent: "center"
  },
  rightBox: {
    width: "60%",
    justifyContent: "center"
  },
  textBox: {
    marginLeft: 10,
    textAlignVertical: "center",
    color: "black",
    fontSize: 15
  },
  topicView: {
    marginTop: 10,
    alignItems: "center"
  },
  topic: {
    fontSize: 30,
    color: MAIN_COLOR
  },
  button2: {
    padding: 5,
    // backgroundColor: "#FFF",
    borderRadius: 5
  },
  buttonTextStyle: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  textButton: {
    color: "#007CC2",
    fontSize: 20
  },
  flatlist: {
    flex: 1
  },
  dropdown: {
    // flexDirection: "row"
  }
})

const mapStateToProps = state => {
  return {
    error: state.history.error,
    history: state.history.historylist,
    end: state.history.end,
    joblist: state.joblist.data
  }
}

export default connect(mapStateToProps, actions)(History)

class Row extends Component {
  render() {
    return (
      <View style={styles.row}>
        <View style={styles.leftBox}>
          <Text style={styles.textBox}>{this.props.left}</Text>
        </View>
        <View style={styles.rightBox}>
          <Text style={styles.textBox}>{this.props.right}</Text>
        </View>
      </View>
    )
  }
}
