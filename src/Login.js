import React, { Component, useState, useEffect} from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Linking,
  ToastAndroid,
  BackHandler,
  Dimensions,
  PermissionsAndroid
} from "react-native"
import AsyncStorage from '@react-native-community/async-storage';
import {
  Container,
  Content,
  Form,
  Item,
  Label,
  Input,
  Icon,
  Button,
  Picker as Pk
} from "native-base"
import { Actions } from "react-native-router-flux"
import { Icon as Ic } from "react-native-elements"
import Background from "./Components/Background"
import Spinner from "react-native-loading-spinner-overlay"
import ModalSelector from "react-native-modal-selector"
import DeviceInfo from "react-native-device-info"
import {
  Login as CallLogin,
  JobList,
  GroupEmployee,
  Extra
} from "./Controller/API"
import I18n from "../assets/languages/i18n"
import { connect } from "react-redux"
import * as actions from "./Actions"
import firebase from "react-native-firebase"
// import FingerprintScanner from "react-native-fingerprint-scanner"
import FingerprintPopup from "../src/Components/FingerprintPopup"
import FusedLocation from "react-native-fused-location"
import clear from 'react-native-clear-app-cache'
import axios from "axios"
import { API_IP } from "./Constants/Constant"
import {
  AUTH_LOGIN_ERROR,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGOUT_SUCCESS,
  AUTH_LOGOUT_ERROR,
  AUTH_LOGIN_DUP,
  AUTH_RESET,
  AUTH_CHANGEPASS_SUCCESS,
  AUTH_CHANGEPASS_FAIL,
  AUTH_LOADING,
  USER_INFO_SUCCESS,
  USER_INFO_ERROR
} from "./Actions/types"

// const useMount = func => useEffect(() => func(), []);


class Login extends Component {
  
  constructor(props) {
    super(props)
    this.state = {
      language:
        I18n.locale == "en"
          ? I18n.t("loginpage.english")
          : I18n.t("loginpage.thai"),
      username: "",
      password: "",
      loading: false,
      employee: [{ key: 0, section: true, label: I18n.t("loginpage.admin") }],
      doubleBackToExitPressedOnce: false,
      latitude: "",
      longitude: "",
      firebasetoken: "0",
      loc: true,
      errorMessage: "",
      popupShowed: false,
      touch: ""
    }
  }
  

  async componentDidMount() {

    Linking.addEventListener('url', this.props.TokenautoLogin);
    // const initialUrl = await Linking.getInitialURL();
    // 
    // console.log('initialUrl',initialUrl.substring(18))
    // const config = {
    //   headers: {
    //     "content-type": "application/x-www-form-urlencoded"
    //   },
    //   timeout: 10000
    // }
    // var data ={
    //   token: initialUrl.substring(18)
    // }
    // var data1 = []
    // for (var property in data) {
    //   var encodedKey = encodeURIComponent(property)
    //   var encodedValue = encodeURIComponent(data[property])
    //   data1.push(encodedKey + "=" + encodedValue)
    // }
    // data1 = data1.join("&")

    // // console.log('data1จริงหรือเปล่าาาา', data1)
    // axios
    //   .post(API_IP + "login", data1, config)
    //   .then(function(response) {
    //     console.log('testยิงข้อมูล deeplink!!!',response)
    //     console.log('DashBoardlist!!!!!!!!!!!!!!!!!!!!!!',response.data.DashboardList)
    //     if (response.data.Message.Code == "102") {
    //       return dispatch({ type: AUTH_LOGIN_DUP, payload: response.data.Message.Message })
    //     } else if (response.data.Message.Code == "001") {
          
    //       firebase.messaging().subscribeToTopic("employee_" + String(response.data.UserInfo.employeeid))
    //       firebase.crashlytics().setUserIdentifier(String(response.data.UserInfo.username))
    //       firebase.analytics().setUserId(String(response.data.UserInfo.username))
    //       firebase.analytics().logEvent("login", { name: String(response.data.UserInfo.username), id: String(response.data.UserInfo.employeeid) })
    //       AsyncStorage.multiSet([
    //         ["token", response.data.UserInfo.token],
    //         ["userid", String(response.data.UserInfo.userid)],
    //         ["employeeid", String(response.data.UserInfo.employeeid)],
    //         ["username", String(response.data.UserInfo.username)],
    //         ["usergroupname", String(response.data.UserInfo.usergroupname)],
    //         ["qrcode", String(response.data.UserInfo.qrcode)],
    //         ["profilepic", String(response.data.UserInfo.picture)],
    //         ["fullname", String(response.data.UserInfo.firstname) + " " + String(response.data.UserInfo.lastname)],
    //         ["nickname", String(response.data.UserInfo.nickname)],
    //         ["user", String('')],
    //         ["password", String('')],
    //         ["position",String(response.data.UserInfo.position)]
    //       ]).then(value => {})
    //       return dispatch({ type: AUTH_LOGIN_SUCCESS, payload: response.data.UserInfo, dashboard: response.data.DashboardList})
    //     } else {
    //       return dispatch({ type: AUTH_LOGIN_ERROR, payload: response.data.Message.Message })
    //     }
    //   })
    //   .catch(function(error) {
    //     console.log(error)
    //     firebase.crashlytics().log('login')
    //     firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
    //     return dispatch({ type: AUTH_LOGIN_ERROR, payload: error.message })
    //   })
    // }
    this.setState({ loading: true })
    const initialUrl = await Linking.getInitialURL()
    
    console.log('await Linking.getInitialURL()',await Linking.getInitialURL())
    const Token = initialUrl;
    if( initialUrl !== '' && initialUrl !== null){
    let data ={
        token: Token.substring(22)
      }
      console.log('data token หรือ เปล่าหว่า',data);
    this.props.TokenLogin(data)
    }else{
      this.setState({ loading: false })
    }
    clear.clearAppCache(() => {
      
      // console.warn("test")
  
    })
    this._loadEmployee()
    this.requestCameraPermission()
    this.getCurrentPosition()
    this.getToken()
    BackHandler.addEventListener("back", () => this._handleBack())
    console.log("1234, didmount")
    let touch = await AsyncStorage.getItem("Touch")
    
    this.setState({ touch: touch })
    // FingerprintScanner
    //   .isSensorAvailable()
    //   .catch(error =>
    //     console.warn(error.message)
    //     // this.setState({ errorMessage: error.message })
    //   )
  }
  handleFingerprintShowed = () => {
    this.setState({ popupShowed: true })
  }

  handleFingerprintDismissed = (username, password, value) => {
    if (value) {
      this.setState({
        popupShowed: false,
        username: username,
        password: password
      })
      this.loginPressed()
    } else {
      this.setState({ popupShowed: false })
    }
  }

  // componentWillMount() {
  //   this._loadEmployee()
  //   this.getCurrentPosition()
  //   this.getToken()
  // }

  componentWillUnmount() {
    Linking.removeEventListener('url', this.props.TokenautoLogin);
    BackHandler.removeEventListener("back")
    console.log("4321, unmount")
  }
  async requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.requestMultiple(
        [
          PermissionsAndroid.PERMISSIONS.CAMERA,
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
        ],
        {
          title: " App Permission",
          message: "App needs access to phone feature "
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the camera")
      } else {
        console.log("Camera permission denied")
      }
    } catch (err) {
      // console.warn(err)
    }
  }

  
  

  async getCurrentPosition() {
    // navigator.geolocation.getCurrentPosition(
    //   position => {
    //     console.warn('geolocation',position.coords)
    //     this.setState({ latitude: position.coords.latitude, longitude: position.coords.longitude })

    //   },
    //   error => {
    //     console.warn("error: ", error.message)
    //     this.setState({ loc: false })
    //   },
    //   { timeout: 20000,  maximumAge: 1000 }
    // )

    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: "App needs to access your location",
        message:
          "App needs access to your location " +
          "so we can let our app be even more awesome."
      }
    )
    if (granted) {
      FusedLocation.setLocationPriority(FusedLocation.Constants.HIGH_ACCURACY)
      const location = await FusedLocation.getFusedLocation()
      this.props.setlocation(location)
      this.setState({
        latitude: location.latitude,
        longitude: location.longitude
      })
      // console.warn('FusedLocation',location.latitude,location.longitude)
    
    } else {
  
    }
  }

  getToken = () => {
    firebase
      .messaging()
      .getToken()
      .then(token => {
        this.setState({ firebasetoken: token })
        console.log("get token", token)
      })
    firebase.messaging().onTokenRefresh(token => {
      this.setState({ firebasetoken: token })
      console.log("refresh", token)
    })
  }

  _handleBack() {
    console.log(Actions.currentScene)
    if (Actions.currentScene == "_login" || Actions.currentScene == "login") {
      console.log("PRESS BACK IN LOGIN", Actions.currentScene)
      if (this.state.doubleBackToExitPressedOnce) {
        BackHandler.exitApp()
      }
      ToastAndroid.show("Press back again to exit", ToastAndroid.SHORT)
      this.setState({ doubleBackToExitPressedOnce: true })
      setTimeout(() => {
        this.setState({ doubleBackToExitPressedOnce: false })
      }, 2000)
      return true
    }
    return false
  }


  async componentWillReceiveProps(nextProps) {
    this.setState({ loading: false })
    if (
      nextProps.error != null &&
      !nextProps.login &&
      nextProps.logout == null
    ) {
      if (nextProps.duplicate) {
        let macid = null
        await DeviceInfo.getMacAddress().then(mac => {
          macid = mac
        })
        var DeviceName = await DeviceInfo.getDeviceName()
        var data = {
          username: this.state.username,
          password: this.state.password,
          machineid: macid,
          locationlat: this.state.latitude,
          locationlong: this.state.longitude,
          language: I18n.currentLocale(),
          devicename: DeviceName,
          firebasetoken: this.state.firebasetoken,
          // version: DeviceInfo.getVersion()
        }

        Alert.alert("ERROR", nextProps.error, [
          {
            text: "Cancel",
            onPress: () => {
              this.props.authReset()
            },
            style: "cancel"
          },
          {
            text: "OK",
            onPress: () => {
              data.ConfirmLogin = true
              this.props.authLogin(data)
            }
          }
        ])
        return
      }
      Alert.alert("ERROR", nextProps.error, [
        { text: "OK", onPress: () => this.props.authReset() }
      ])
      return
    }
    if (nextProps.login) {
      console.log("Open app from noti redirect to ->", this.props.noti)
      if (this.props.noti != null) {
        if (this.props.noti.type == "approve") {
          Actions.reset("drawer")
          Actions.notification({
            fromnoti: true,
            alertid: this.props.noti.alertid
          })
          return
        } else if (this.props.noti.type == "redirectpile") {
          const { noti } = this.props
          var pileno = noti.pileno || ""
          var pileid = noti.pileid || 0
          var jobid = noti.jobid || 0
          var toProcess = noti.process || 1
          var sp_parentid = noti.sp_parentid
          var pile_childs = noti.pile_childs
          toProcess = toProcess - 1
          Actions.reset("drawer")
          // Actions.
          Actions.mainpile({
            title: pileno,
            pileid,
            jobid,
            toProcess,
            fromnoti: true,
            viewType: 0,
            finish: false,
            sp_parentid: sp_parentid,
            pile_childs: pile_childs
          })
        }
      } else {
        // Type not approve do nothing from now
        Actions.reset("drawer")
      }
    }
    // Actions.reset("drawer")
    // Actions.drawer()
    // Actions.joblist()
    // console.log(nextProps.data)
  }

  async _loadEmployee() {
    var param = {
      language: I18n.currentLocale()
    }
    console.log('Testlogin employees')
    Extra(
      param,
      "contact",
      json => {
        emp = [{ key: 0, section: true, label: I18n.t("loginpage.admin") }]
        let employees = json.GroupEmployeeList
        employees.map((employee, index) => {
          emp.push({
            key: index + 1,
            label:
              (employee.nickname && employee.nickname + "-") +
              employee.firstname +
              " " +
              employee.lastname,
            tel: employee.mobile
          })
        })
        
        this.setState({ employee: emp })
      },
      error => {
        console.log(error)
      }
    )
  }

  async loginPressed() {
    let macid = null
    await DeviceInfo.getMacAddress().then(mac => {
      macid = mac
    })
    var DeviceName = await DeviceInfo.getDeviceName()
    var data = {
      username: this.state.username,
      password: this.state.password,
      machineid: macid,
      locationlat: this.state.latitude,
      locationlong: this.state.longitude,
      language: I18n.currentLocale(),
      devicename: DeviceName,
      firebasetoken: this.state.firebasetoken,
      // version: DeviceInfo.getVersion()
    }
    console.log(data)
    this.setState({ loading: true })
    this.props.authLogin(data)
    // Alert.alert('',I18n.t('alert.errorconcrete'))
    
  }

  changeLanguage(option) {
    switch (option.key) {
      case 1:
        I18n.locale = "th"
        this.setState({ language: I18n.t("loginpage.thai") })
        this._loadEmployee()
        break
      case 2:
        I18n.locale = "en"
        this.setState({ language: I18n.t("loginpage.english") })
        this._loadEmployee()
        break
      default:
        console.log("default")
    }
  }

  devThing() {
    if (!__DEV__) return
  }

  render() {
    const data = [
      { key: 0, section: true, label: "Language" },
      { key: 1, label: I18n.t("loginpage.thai") },
      { key: 2, label: I18n.t("loginpage.english") }
    ]
    const { errorMessage, popupShowed } = this.state

    return (
      <Container style={{ marginTop: -20 }}>
       
        <Background source={require("../assets/image/loginbg.png")}>
          <Content
            keyboardShouldPersistTaps="handled"
            keyboardDismissMode="interactive"
          >
            <View style={styles.logo}>
              <Image
                source={require("../assets/image/pylonlogo.png")}
                style={{ width: 304.5, height: 87 }}
                resizeMode="center"
              />
            </View>
            <View style={styles.form}>
              <Item>
                <Icon active name="person" style={{ color: "#FFF" }} />
                <Input
                  style={styles.inputFont}
                  placeholder={I18n.t("loginpage.username")}
                  placeholderTextColor="#FFF"
                  onChangeText={username => this.setState({ username })}
                  returnKeyType="next"
                  onSubmitEditing={event => this.refs.pass._root.focus()}
                  blurOnSubmit={false}
                  ref="12e"
                  value={this.state.username}
                />
              </Item>
              <Item>
                <Icon active name="lock" style={{ color: "#FFF" }} />
                <Input
                  ref="pass"
                  secureTextEntry
                  returnKeyLabel="Login"
                  onSubmitEditing={() => this.loginPressed()}
                  blurOnSubmit={false}
                  style={styles.inputFont}
                  placeholder={I18n.t("loginpage.password")}
                  placeholderTextColor="#FFF"
                  onChangeText={password => this.setState({ password })}
                  value={this.state.password}
                />
              </Item>
            </View>
            <View style={styles.langText}>
              <Text style={styles.inputFont}>
                {I18n.t("loginpage.language")}
              </Text>
            </View>
            <View style={styles.button}>
              <ModalSelector
                data={data}
                initValue={this.state.language}
                onChange={option => this.changeLanguage(option)}
                cancelText="Cancel"
              >
                <TouchableOpacity style={styles.button2} underlayColor="white">
                  <View style={styles.buttonTextStyle}>
                    <Text style={styles.textButton}>{this.state.language}</Text>
                    <View
                      style={{
                        justifyContent: "flex-end",
                        flexDirection: "row"
                      }}
                    >
                      <Ic
                        name="arrow-drop-down"
                        type="MaterialIcons"
                        color="#6bacd5"
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </ModalSelector>
              <Button
                block
                light
                style={{ marginTop: 20, backgroundColor:'#ffffff' }}
                onPress={() => this.loginPressed()}
              >
                <Text style={styles.textButton}>
                  {I18n.t("loginpage.login")}
                </Text>
              </Button>
            </View>
            {this.state.touch == "YES" ? (
              <TouchableOpacity
                style={styles.fingerprint}
                onPress={this.handleFingerprintShowed}
                // disabled={!!errorMessage}
              >
                <Image
                  style={{ alignSelf: "center" }}
                  source={require("../assets/image/finger_print.png")}
                />
              </TouchableOpacity>
            ) : (
              <View />
            )}
            <View style={styles.langText}>
              <ModalSelector
                data={this.state.employee}
                initValue={this.state.language}
                onChange={option => Linking.openURL("tel:" + option.tel)}
                cancelText="Cancel"
              >
                <Text style={styles.inputFont}>
                  {I18n.t("loginpage.forgotpassword")}
                </Text>
              </ModalSelector>
            </View>
            <View style={{ flex: 1 }}>
              <Spinner
                visible={this.state.loading}
                textContent={"Loading..."}
                textStyle={{ color: "#FFF" }}
                overlayColor="rgba(0, 0, 0, 0.5)"
              />
            </View>
          </Content>
          <View style={styles.version}>
            {__DEV__ && <Text>{this.state.firebasetoken}</Text>}
            {__DEV__ && (
              <Text>{this.state.latitude + " , " + this.state.longitude}</Text>
            )}
            <Text
              onPress={() => this.devThing()}
              style={[
                this.state.latitude == ""
                  ? { color: "#FFF" }
                  : { color: "#B4F574" },
                !this.state.loc ? { color: "red" } : {}
              ]}
            >
              v{DeviceInfo.getVersion()}
            </Text>
          </View>
        </Background>
        {popupShowed && (
          <FingerprintPopup
            style={[styles.popup]}
            handlePopupDismissed={this.handleFingerprintDismissed}
          />
        )}
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  logo: {
    marginTop: 45,
    alignItems: "center",
    marginLeft: 20,
    marginRight: 20
  },
  popup: {
    width: 300
  },
  fingerprint: {
    padding: 0,
    marginVertical: 15
  },
  form: {
    marginTop: 100,
    paddingLeft: 40,
    paddingRight: 40
  },
  langText: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20
  },
  button: {
    marginTop: 10,
    paddingRight: 40,
    paddingLeft: 40
  },
  textButton: {
    color: "#007CC2",
    fontSize: 20,
  },
  inputFont: {
    color: "#FFF",
    fontFamily: "THSarabunNew",
    fontSize: 21
  },
  button2: {
    padding: 5,
    backgroundColor: "#FFF",
    borderRadius: 5
  },

  buttonTextStyle: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  version: {
    alignItems: "flex-end",
    marginRight: 10
  }
})

const mapStateToProps = state => {
  return {
    login: state.auth.login,
    logout: state.auth.logout,
    error: state.auth.error,
    data: state.auth.data,
    duplicate: state.auth.duplicate,
    noti: state.noti.notidata
  }
}
export default connect(mapStateToProps, actions)(Login)
