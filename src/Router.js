import React, { Component } from "react"
import { Icon } from "react-native-elements"
import {  StyleSheet, Text, View, Alert, TouchableOpacity,Dimensions } from "react-native"
import { Scene, Router, Stack,  Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../src/Actions"
import { CATEGORY_1, DEFAULT_COLOR_1, DEFAULT_COLOR_4 } from "./Constants/Color"

import Login from "./Login"
import SideBar from "./Components/SideBar"
import CustomNavBar from "./Components/CustomNavBar"
import ProjectList from "./ProjectList"
import MainPile from "./Piles/MainPile"
import PileDetail from "./PileDetail"
import JobList from "./Main/JobList"
import JobDetail from "./Main/Job/JobDetail"
import Splash from "./Components/Splash"
import SteelDetail from "./Piles/SteelDetail"
import UserProfile from "./UserProfile"
import CameraRoll from "./Piles/CameraRoll"
import SelfApprove from "./Piles/SelfAprrove"
import ScanQrCode from "./Piles/ScanQrCode"
import LiquidTestInsert from "./Piles/LiquidTestInsert"
import DrillPileInsert from "./Piles/DrillPileInsert"
import Notification from "./Notification"
import NotificationDetail from "./NotificationDetail"
import History from "./History"
import TremieInsert from "./Piles/TremieInsert"
import RefreshPage from "./Piles/RefreshPage"
import ConcreteRegister from "./Piles/ConcreteRegister"
import SlumpApprove from "./Piles/SlumpApprove"
import Note from "./Piles/Note"
import DropConcrete from "./Piles/DropConcrete"
import DeviceInfo from "react-native-device-info"
import I18n from '../assets/languages/i18n'
import Dashboard from './Dashboard'
class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: null,
      stack_main: []
    }
  }

  componentDidMount() {
    this.props.notiList()
  }

  _RightButton = data => {
    return (
      <CustomNavBar color={data.category==3||data.category==5?DEFAULT_COLOR_4:data.category==2?DEFAULT_COLOR_1:CATEGORY_1}/>
    )
  }
  _Navigation = data => {
    const height = 80
    if (DeviceInfo.isTablet()) {
      if(data.category == 3|| data.category == 5){
        return { backgroundColor: DEFAULT_COLOR_4, height: height}
      }else if (data.category == 1 || data.shape == 1) {
        return { backgroundColor: CATEGORY_1, height: height }
      } else if (data.category == 2) {
        return { backgroundColor: DEFAULT_COLOR_1, height: height }
      } else if (data.shape == 2) {
        return { backgroundColor: DEFAULT_COLOR_1, height: height }
      } else {
        return { backgroundColor: CATEGORY_1, height: height }
      }
    } else {
      if(data.category == 3|| data.category == 5){
        return { backgroundColor : DEFAULT_COLOR_4, height: height}
      }else if (data.category == 1 || data.shape == 1) {
        return { backgroundColor: CATEGORY_1 }
      } else if (data.category == 2) {
        return { backgroundColor: DEFAULT_COLOR_1 }
      } else if (data.shape == 2) {
        return { backgroundColor: DEFAULT_COLOR_1, height: height }
      } else {
        return { backgroundColor: CATEGORY_1 }
      }
    }
  }

  barSize = data => {
    // console.warn(data)
    const height = 80
    if (DeviceInfo.isTablet()) {
      return [{ backgroundColor: "#007CC2", height: height }, 
      // data.shape && data.shape == 2 && { backgroundColor: DEFAULT_COLOR_1},
      data.category == 3 || data.category == 5?{ backgroundColor: DEFAULT_COLOR_4}:data.shape == 2?{ backgroundColor: DEFAULT_COLOR_1}:{}
    ]
    }
    return [{ backgroundColor: "#007CC2" }, 
    // data.shape && data.shape == 2 && { backgroundColor: DEFAULT_COLOR_1},
    data.category == 3 || data.category == 5? { backgroundColor: DEFAULT_COLOR_4 }:data.shape == 2? { backgroundColor: DEFAULT_COLOR_1}:{}
  ]
  }

  jumboTitle = data => {
    let title = data.title
    return (
      <View style={styles.navigationTitle}>
      <Text style={[styles.navigationTitleText, DeviceInfo.isTablet() && { marginLeft: 17, fontSize: 50 }]}>{title}</Text>
      </View>
    )
  }

  _Title = data => {
    var title = data.title
    if(Actions.currentScene == "_mainpile"){
      return(
        <TouchableOpacity style={[styles.navigationTitle]} onPress={() => this._AlertBackMain(data)}>
            <View>
              <Text ellipsizeMode={"tail"} numberOfLines={1} style={[styles.navigationTitleText, DeviceInfo.isTablet() && { marginLeft: 17, fontSize: 50 },
              {width: Dimensions.get("window").width > 1080 ? Dimensions.get("window").width * 50/100  :  Dimensions.get("window").width * 35/100}]}>{title}</Text>
            </View>
        </TouchableOpacity>
      )
    }else if(Actions.currentScene == '_projectlist' || Actions.currentScene == '_joblist'){
      return(
        <View style={{width:350}}>
            {/*<Text style={[styles.navigationTitleText, DeviceInfo.isTablet() && { marginLeft: 17, fontSize: 50 }]}>{''}</Text>*/}
            {<CustomNavBar color={data.category==3?DEFAULT_COLOR_4:data.category==2?DEFAULT_COLOR_1:CATEGORY_1}/>}
          </View>
      )
    }
    else{
      return(
        <View style={{width:350}}>
        <Text style={[styles.navigationTitleText, DeviceInfo.isTablet() && { marginLeft: 17, fontSize: 50 }]}>{''}</Text>
      </View>
      )
    }
    return (
      <View>
        {Actions.currentScene == "_mainpile" ? (
          <TouchableOpacity style={[styles.navigationTitle]} onPress={() => this._AlertBackMain(data)}>
            <View>
              <Text ellipsizeMode={"tail"} numberOfLines={1} style={[styles.navigationTitleText, DeviceInfo.isTablet() && { marginLeft: 17, fontSize: 50 },
              {width: Dimensions.get("window").width > 1080 ? Dimensions.get("window").width * 50/100  :  Dimensions.get("window").width * 39/100}]}>{title}</Text>
            </View>
          </TouchableOpacity>
        ) 
        // : Actions.currentScene == '_projectlist' || Actions.currentScene == '_joblist'? (
        //   <View style={{width:350}}>
        //     {/*<Text style={[styles.navigationTitleText, DeviceInfo.isTablet() && { marginLeft: 17, fontSize: 50 }]}>{''}</Text>*/}
        //     {<CustomNavBar color={data.category==3?DEFAULT_COLOR_4:data.category==2?DEFAULT_COLOR_1:CATEGORY_1}/>}
        //   </View>
        // )
        :
        (
          <View style={{width:350}}>
            <Text style={[styles.navigationTitleText, DeviceInfo.isTablet() && { marginLeft: 17, fontSize: 50 }]}>{''}</Text>
          </View>
        )}
      </View>
    )
  }

  _AlertBackMain(data) {
    if (data.fromnoti) {
    }

    if(data.category != 1 && data.category != 2 && data.pile_childs != null && data.pile_childs != undefined){
      var data1 = null
      for(var i = 0 ; i < data.pile_childs.length ; i++){
        if(data.pile_childs[i].pile_id != data.pileid){
          data1 = data.pile_childs[i]
        }
      }
      console.log("data1", data1)
      Alert.alert(I18n.t('mainpile.alert.goto'),
        '',
        [
          {text: I18n.t('mainpile.alert.pilepage'), onPress: () => {
            if(data.fromnoti){
              Actions.joblist()
            }else{
              this.props.ungetStepStatus2()
              // this.props.pileClear()
              // this.props.mainpileClear()
              Actions.projectlist({ result: ''})
            }
          }},
          {text: I18n.t('mainpile.alert.pile')+' '+data1.pile_no, onPress: () => {
            this.props.ungetStepStatus2()
            this.props.clearStatus()
            Actions.pop()
            Actions.mainpile({ title: data1.pile_no, pileid: data1.pile_id, jobid: data.jobid, category: data.category,viewType:0, finish:false, sp_parentid: data.sp_parentid, pile_childs:data.pile_childs})
          }},
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          }
          // {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
      );
    }else{
      Alert.alert("Back", I18n.t('mainpile.alert.backtopile'), [
        {
          text: "Cancel",
          onPress: () => {}
        },
        {
          text: "Ok",
          onPress: () => {
            if(data.fromnoti){
              Actions.joblist()
            }else{
              this.props.ungetStepStatus2()
              // this.props.pileClear()
              // this.props.mainpileClear()
              Actions.projectlist({ result: ''})
            }
          }
        }
      ])
    }
  }

  _pileDetailIcon = () => {
    return (
      <View style={{ marginRight: 5 }}>
        <Icon name="file-text" type="font-awesome" color="#6bacd5" size={20} />
      </View>
    )
  }

  _RightButtonPress = () => {
    Actions.refresh({ navBar: CustomNavBar })
  }
  _LeftButtonPress = () => {
    Actions.refresh({ navBar: SideBar })
  }

  _DrawerIcon = () => {
    if (this.props.noticount && this.props.noticount >= 1) {
      return (
        <View style={{ flexDirection: "row", paddingRight: 5 }}>
          <Icon name="bars" type="font-awesome" color="#FFF" size={DeviceInfo.isTablet() ? 50 : 26} />
          <View
            style={{
              marginLeft: -5,
              backgroundColor: "#F00",
              width: 17,
              height: 17,
              borderRadius: 13,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text style={{ color: "#FFF", fontSize: 12 }}>{this.props.noticount}</Text>
          </View>
        </View>
      )
    }
    return <Icon name="bars" type="font-awesome" color="#FFF" size={DeviceInfo.isTablet() ? 50 : 26} />
  }

  _xButton = () => {
    return (
      <View style={{ marginRight: 5 }}>
        <Icon name="close" type="evilicons" color="#FFF" onPress={() => Actions.pop()} style={{ marginRight: 10 }} size={DeviceInfo.isTablet() ? 50 : 26} />
      </View>
    )
  }

  _leftButton = () => <View />

  render() {
    return (
      
      <Router>
        <Stack key="root" navigationBarStyle={this.barSize} titleStyle={styles.titleText} renderTitle={this.jumboTitle}>
          <Scene key="splash" component={Splash} title="App" hideNavBar timeout={0} initial />
          <Scene key="login" component={Login} title="Login" hideNavBar onBack={() => {}} />

          <Scene
            ref="drawerRefs"
            key="drawer"
            drawer
            drawerWidth={300}
            drawerIcon={this._DrawerIcon}
            contentComponent={SideBar}
            renderTitle={this._Title}
            // renderRightButton={this._RightButton}
            navigationBarStyle={this._Navigation}
            onBack={() => {}}
            hideNavBar
          >
            <Scene
              key="joblist"
              component={JobList}
              onBack={() => console.log("BACK in router")}
              initial
              title="หน้าแรก"
            />
            <Scene key="projectlist" component={ProjectList}/>
            {<Scene key="mainpile" component={MainPile} />}
            <Scene
              key="userprofile"
              component={UserProfile}
              title="ข้อมูลส่วนตัว"
              renderRightButton={this._leftButton}
            />
            <Scene key="notification" component={Notification} renderRightButton={this._leftButton} />
            <Scene
              key="dashboard"
              component={Dashboard}
              title="Dashboard"
              renderRightButton={this._leftButton}
            />
          </Scene>

          {<Scene key="camearaRoll" component={CameraRoll} title="Camera Roll" hideNavBar={true} />}
          {<Scene key="scanQrCode" component={ScanQrCode} title="ScanQrCode" hideNavBar={true} />}
          {<Scene
            key="selfapprove"
            component={SelfApprove}
            renderBackButton={this._leftButton}
            renderRightButton={this._xButton}
            navigationBarStyle={this._Navigation}
          />}
          <Scene
            key="notidetail"
            component={NotificationDetail}
            renderBackButton={this._leftButton}
            renderRightButton={this._xButton}
          />
          <Scene
            key="jobdetail"
            component={JobDetail}
            renderBackButton={this._leftButton}
            renderRightButton={this._xButton}
            // renderTitle={this._Title}
          />
          <Scene
            key="piledetail"
            component={PileDetail}
            renderBackButton={this._leftButton}
            renderRightButton={this._xButton}
          />
          <Scene
            key="steeldetail"
            component={SteelDetail}
            renderBackButton={this._leftButton}
            renderRightButton={this._xButton}
            navigationBarStyle={this._Navigation}
          />
          <Scene key="liquidTestInsert" component={LiquidTestInsert} hideNavBar={true} />
          <Scene key="drillPileInsert" component={DrillPileInsert} hideNavBar={true} />
          <Scene key="tremieinsert" component={TremieInsert} hideNavBar />
          <Scene key="concreteregister" component={ConcreteRegister} hideNavBar />
          <Scene key="refreshPage" component={RefreshPage} title="refreshPage" hideNavBar={true} />
          <Scene
            key="slumpapprove"
            component={SlumpApprove}
            renderBackButton={this._leftButton}
            renderRightButton={this._xButton}
          />
          <Scene
            key="history"
            component={History}
            renderBackButton={this._leftButton}
            renderRightButton={this._xButton}
            title={I18n.t('drawer.history')}
          />
          <Scene
            key="note"
            component={Note}
            renderBackButton={this._leftButton}
            renderRightButton={this._xButton}
            title={I18n.t('drawer.note')}
          />
          <Scene key="dropconcrete" component={DropConcrete} hideNavBar />
        </Stack>
      </Router>
    )
  }
}

const mapStateToProps = state => {
  return {
    noticount: state.noti.count
  }
}
export default connect(mapStateToProps, actions)(App)


const styles = StyleSheet.create({
  titleText: {
    color: "#FFF",
    fontFamily: "THSarabunNew"
  },
  navigationTitle: {
    // backgroundColor: '#BABABA',
    // justifyContent: "center",
    // alignItems: "center"
  },
  navigationTitleText: {
    color: "#FFF",
    fontFamily: "THSarabunNew Bold",
    fontSize: 25,
    marginTop: 3
  }
})
