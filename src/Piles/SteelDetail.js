import React, { Component } from "react"
import { Alert, Text, View, StyleSheet, TouchableOpacity, BackHandler,ActivityIndicator, Linking } from "react-native"
import { connect } from "react-redux"
import { Container, Content } from "native-base"
import { Actions } from "react-native-router-flux"
import * as actions from "../Actions"
import TableView from "../Components/TableView"
import { MAIN_COLOR, SUB_COLOR, DEFAULT_COLOR_1, DEFAULT_COLOR_4,GRAY } from "../Constants/Color"
import { Icon } from "react-native-elements"
import I18n from "../../assets/languages/i18n"
import styles from "./styles/SteelDetail.style"



class SteelDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showImage: false,
      sectionDetail: {
        mainbar: [],
        spiral: []
      },
      checkconcretespacer: this.props.checkconcretespacer,
      checksteelcage: this.props.checksteelcage,
      image_concretespacer: this.props.image_concretespacer == null ? [] : this.props.image_concretespacer,
      image_steelcage: this.props.image_steelcage == null ? [] : this.props.image_steelcage,
      loading:false,
      randomlockstep7_casing:null,
      onpressimage: false,
      process1_success_2_random: null,
      // rbsheet:'',
      // rbsheetshow:false
    }
  }

  componentWillMount() {
    this.props.steelSecDetail({ pileid: this.props.pileid, sectionid: this.props.sectionid })
  }

  _backButton() {
    Actions.pop()
    return true
  }

  componentWillUnmount() {
    // BackHandler.removeEventListener("sdback")
  }
  componentDidMount() {
    // BackHandler.addEventListener("sdback", () => this._backButton())
    var pileMaster = null
    if (
      this.props.pileAll[this.props.pileid] &&
      this.props.pileAll[this.props.pileid][`${this.props.process}_${this.props.step}`]
    ) {
      pileMaster = this.props.pileAll[this.props.pileid][`${this.props.process}_${this.props.step}`].masterInfo
    }

    if(pileMaster &&pileMaster.Steelcage&&pileMaster.Steelcage.SectionList){
      pileMaster.Steelcage.SectionList.map(item => {
        // console.log('pileMaster.Steelcage.SectionList',item.rbsheet)
        if (item.section_id == this.props.sectionid) {
          var detail = {
            mainbar: item.mainbar,
            spiral: item.spiral,
            spacers: item.spacers,
            dowel: item.dowel,
            steelcage_no: item.steelcage_no,
            section_no: item.section_no,
            index:  this.props.index,
            se:item.se,
            startdate: item.startdate,
            enddate:item.enddate,
            section_no_only:item.section_no_only,
            // rbsheet:item.rbsheet
          }
          this.setState({ sectionDetail: detail } )
        }
      }) 
    }
   
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.action == "update") {
      this.setState(nextProps.value.data)
    }
    if(this.state.image_concretespacer||this.state.image_steelcage){
      this.setState({
        onpressimage:false
      })
    }
    console.log(this.props.process==7&&nextProps.randomlockstep7_casing != this.state.randomlockstep7_casing&&this.state.onpressimage==false ,nextProps.randomlockstep7_casing  ,this.state.randomlockstep7_casing,this.state.onpressimage,Actions.currentScene,nextProps.image_concretespacer,nextProps.image_steelcage)
    if(this.props.process==7&&nextProps.randomlockstep7_casing != this.state.randomlockstep7_casing&&this.state.onpressimage==false){
     this.setState({
       loading:false,
       randomlockstep7_casing:nextProps.randomlockstep7_casing
     },()=>{
      Actions.pop({
        refresh: {
          action: "update",
          value: {
            process: this.props.process,
            step: this.props.step,
            data: {
              sectionid: this.props.sectionid,
              checkconcretespacer: this.state.checkconcretespacer,
              checksteelcage: this.state.checksteelcage,
              image_concretespacer: this.state.image_concretespacer,
              image_steelcage: this.state.image_steelcage,
              startdate:this.state.startdate,
              enddate:this.state.enddate
            }
          }
        }
      })
     })
    }
    
    if(nextProps.error!=null){
      this.setState({
       loading:false
     })
    }
    // console.warn('test',nextProps.process1_success_2,nextProps.process1_success_2_random)
    if(nextProps.process1_success_2 == true && nextProps.process1_success_2_random != this.state.process1_success_2_random&&this.state.onpressimage==false&&this.props.process==1){
      this.setState({process1_success_2_random:nextProps.process1_success_2_random,loading:false},()=>{
        Actions.pop({
          refresh: {
            action: "update",
            value: {
              process: this.props.process,
              step: this.props.step,
              data: {
                sectionid: this.props.sectionid,
                checkconcretespacer: this.state.checkconcretespacer,
                checksteelcage: this.state.checksteelcage,
                image_concretespacer: this.state.image_concretespacer,
                image_steelcage: this.state.image_steelcage,
                startdate:this.state.startdate,
                enddate:this.state.enddate
              }
            }
          }
        })
      })

    }
  }

  closeButton() {
    Actions.pop()
  }

  

  async saveButton() {
    // if (this.props.process != 7) {
    //   console.warn("saveButton",this.props.process)
    //     Actions.pop({
    //       refresh: {
    //         action: "update",
    //         value: {
    //           process: this.props.process,
    //           step: this.props.step,
    //           data: {
    //             sectionid: this.props.sectionid,
    //             checkconcretespacer: this.state.checkconcretespacer,
    //             checksteelcage: this.state.checksteelcage,
    //             image_concretespacer: this.state.image_concretespacer,
    //             image_steelcage: this.state.image_steelcage,
    //             startdate:this.state.startdate,
    //             enddate:this.state.enddate
    //           }
    //         }
    //       }
    //     })
    //   }
        this.save(this.props.process, this.props.step)
        return true
  }

  save(process, step) {
        var dataSectionDetail = []
        dataSectionDetail.push({
          sectionId: this.props.sectionid,
          Checksteelcage: this.state.checksteelcage,
          CheckConcretespacer: this.state.checkconcretespacer,
          ImageSteelcage: this.state.image_steelcage,
          ImageConcretespacer: this.state.image_concretespacer,
        })

        var valueApi = {
          Language: I18n.locale,
          JobId: this.props.jobid,
          PileId: this.props.pileid,
          Page: step,
          SectionDetail: dataSectionDetail,
          latitude: this.props.lat,
          longitude: this.props.log
        }
        if (process == 1) {
          this.setState({loading:true},()=>{
            this.props.pileSaveStep01(valueApi)
          })
        } else if (process == 7) {
          console.log("steeldetil7",valueApi)
          this.setState({loading:true},()=>{
            this.props.pileSaveStep07Casing(valueApi)
          })
          
        }
  }

  onCameraRoll(keyname, photos, type,editflag) {
    // console.warn('editflag',editflag,editflag!==undefined && editflag!==null && editflag!== '')
    var temp = 0
    // if(editflag != undefined){
    //   temp = editflag
    // }else{
    //   temp = 1
    // }
    if(editflag == true){
      if(this.props.process == 1){
        if(this.props.shape == 1){
          temp = 1
        }else{
          temp = 0
        }
      }else{
        if(this.props.shape == 1){
          temp = 0
        }else{
          temp = 1
        }
      }
    }else{
      temp = 1
    }
    
    if (this.props.Edit_Flag == 1) {
      this.setState({
        onpressimage:true
      },()=>{
      Actions.camearaRoll({
        jobid: this.props.jobid,
        process: this.props.process,
        step: this.props.step,
        pileId: this.props.pileid,
        keyname: keyname,
        photos: photos,
        typeViewPhoto: type,
        shape: this.props.shape,
        category: this.props.category,
        Edit_Flag:temp,
        status:this.props.status
      })
    })
    } else {
      this.setState({
        onpressimage:true
      },()=>{
      Actions.camearaRoll({
        jobid: this.props.jobid,
        process: this.props.process,
        step: this.props.step,
        pileId: this.props.pileid,
        keyname: keyname,
        photos: photos,
        typeViewPhoto: "view",
        shape: this.props.shape,
        category: this.props.category,
        Edit_Flag:0,
        status:this.props.status
      })
    })
    }
  }

  _renderconcreted(){
    const spacers = this.state.sectionDetail.spacers
    if (spacers != "" && spacers != undefined) {
      // console.log(mainbar)
      return (
        <View style={[styles.box,{margin:10}]}>
          <TableView
            value={[
              I18n.t("mainpile.steeldetail.concretetable.position"),
              I18n.t("mainpile.steeldetail.concretetable.head"),
              I18n.t("mainpile.steeldetail.concretetable.center"),
              I18n.t("mainpile.steeldetail.concretetable.last")
            ]}
            head
            shape = {this.props.shape}
            category = {this.props.category}
          />
          <TableView
            value={[I18n.t("mainpile.steeldetail.concretetable.concrete"),spacers.top==null?'-':spacers.top, spacers.middle==null?'-':spacers.middle, spacers.bottom==null?'-':spacers.bottom]}
            last={true}
            category = {this.props.category}
          />
            
         
        </View>
      )
    }
  }

  _renderMainBar() {
    const mainbar = this.state.sectionDetail.mainbar
    if (mainbar != "" && mainbar != undefined) {
      // console.log(mainbar)
      return (
        <View style={styles.box}>
          <View style={styles.topic}>
            {this.props.category==3|| this.props.category == 5?<Text style={[styles.textTopic,{color:DEFAULT_COLOR_4}]}>Main Bar</Text>:this.props.shape == 2 ?<Text style={[styles.textTopic,{color:'#934b02'}]}>Main Bar</Text>:<Text style={styles.textTopic}>Main Bar</Text>}
          </View>
          <TableView
            value={[
              I18n.t("mainpile.steeldetail.mainbar.item"),
              I18n.t("mainpile.steeldetail.mainbar.quantity"),
              I18n.t("mainpile.steeldetail.mainbar.size"),
              I18n.t("mainpile.steeldetail.mainbar.length")
            ]}
            head
            shape = {this.props.shape}
            category = {this.props.category}
          />
          {mainbar.map((data, index) => {
            return (
              <TableView
                key={index}
                value={[data.item, parseFloat(data.qty).toFixed(1), data.type, parseFloat(data.length).toFixed(3)]}
                last={mainbar.length - 1 == index}
                category = {this.props.category}
              />
            )
          })}
        </View>
      )
    }
  }

  _renderSpiral() {
    const spiral = this.state.sectionDetail.spiral
    if (spiral != "" && spiral != undefined) {
      return (
        <View style={styles.box}>
          <View style={styles.topic}>
          {this.props.category==3|| this.props.category == 5?<Text style={[styles.textTopic,{color:DEFAULT_COLOR_4}]}>{this.props.shape == 1 ? "Spiral" : "Tie Bar"}</Text>:this.props.shape == 2 ?<Text style={[styles.textTopic,{color:'#934b02'}]}>{this.props.shape == 1 ? "Spiral" : "Tie Bar"}</Text>:<Text style={styles.textTopic}>{this.props.shape == 1 ? "Spiral" : "Tie Bar"}</Text>}
          </View>
          {this.props.shape == 1 ? (
            <View>
              <TableView
                value={[
                  I18n.t("mainpile.steeldetail.spiral.item"),
                  I18n.t("mainpile.steeldetail.spiral.depth"),
                  I18n.t("mainpile.steeldetail.spiral.size"),
                  I18n.t("mainpile.steeldetail.spiral.length")
                ]}
                head
                shape = {this.props.shape}
                category = {this.props.category}
              />
              {spiral.map((data, index) => {
                return (
                  <TableView
                    key={index}
                    value={[
                      data.item,
                      parseFloat(data.depth).toFixed(3) ,
                      data.type,
                      parseFloat(data.spacing).toFixed(3)
                    ]}
                    last={spiral.length - 1 == index}
                    category = {this.props.category}
                  />
                )
              })}
            </View>
          ) : (
            <View>
              <TableView
                value={[
                  I18n.t("mainpile.steeldetail.spiral_dwall.depth"),
                  I18n.t("mainpile.steeldetail.spiral_dwall.type"),
                  I18n.t("mainpile.steeldetail.spiral_dwall.space"),
                  I18n.t("mainpile.steeldetail.spiral_dwall.size"),
                  I18n.t("mainpile.steeldetail.spiral_dwall.length")
                ]}
                head
                shape = {this.props.shape}
                category = {this.props.category}
              />
              {spiral.map((data, index) => {
                return (
                  <TableView
                    key={index}
                    value={[
                      data.depth === 0.000 ? I18n.t('mainpile.10_3.l'):parseFloat(data.depth).toFixed(3),
                      data.typespiral,
                      data.spacing,
                      data.type,
                      data.legs
                    ]}
                    last={spiral.length - 1 == index}
                    category = {this.props.category}
                  />
                )
              })}
            </View>
          )}
        </View>
      )
    }
  }
  
  
  render() {
    const detail = this.state.sectionDetail //Detail
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, justifyContent: "center" }}>
          <ActivityIndicator size="large" color="#007CC2" />
        </View>
      )
    }
     
    return (
      <Container style={{backgroundColor:GRAY}}>
        <Content>
          <View>
            <View>
              {this.props.shape == 2 ? (
                <View>
                  <Row left={I18n.t("mainpile.steeldetail.drawingno")} right={detail.steelcage_no} />
                  <Row left={I18n.t("mainpile.steeldetail.no")} right={detail.section_no_only} />
                  <Row left={I18n.t('mainpile.steeldetail.se')} right={detail.se==''?'-':detail.se} />
                  <Row
                    left={
                      detail.section_no === 'โครงที่ 1' || detail.section_no === 'Cage No.1'
                        ? I18n.t("mainpile.steeldetail.dowel")
                        : I18n.t("mainpile.steeldetail.distance")
                    }
                    right={parseFloat(detail.dowel).toFixed(3)}
                  />
                </View>
              ) : (
                <View>
                  <Row left={I18n.t("mainpile.steeldetail.drawingno")} right={detail.steelcage_no} />
                  <Row left={I18n.t("mainpile.steeldetail.no")} right={detail.section_no_only} />
                  <Row
                    left={
                      detail.section_no === 'โครงที่ 1' || detail.section_no === 'Cage No.1'
                        ? I18n.t("mainpile.steeldetail.dowel")
                        : I18n.t("mainpile.steeldetail.distance")
                    }
                    right={parseFloat(detail.dowel).toFixed(3)}
                  />
                </View>
              )}
            </View>
            {this._renderMainBar()}
            {this._renderSpiral()}
            <View>
              <View style={styles.box}>
                <View style={styles.topic}>
                  <Text style={styles.textTopic}>{I18n.t("mainpile.steeldetail.steelcage")}</Text>
                </View>
                <View style={styles.imageDetail}>
                  <View style={{ width: "40%", flexDirection: "row", alignItems: "center", marginLeft: "10%" }}>
                    {this.state.checksteelcage ? (
                      <Icon
                        name="check"
                        reverse
                        color="#6dcc64"
                        size={15}
                        onPress={() => {
                          if (this.props.Edit_Flag == 1 && this.props.checkbox == 1) {
                            this.setState({ checksteelcage: !this.state.checksteelcage })
                          }
                        }}
                      />
                    ) : (
                      <Icon
                        name="check"
                        reverse
                        color="grey"
                        size={15}
                        onPress={() => {
                          if (this.props.Edit_Flag == 1 && this.props.checkbox == 1) {
                            this.setState({ checksteelcage: !this.state.checksteelcage })
                          }
                        }}
                      />
                    )}
                    <Text>{
                      // I18n.t("mainpile.steeldetail.approve")
                      this.props.process==1?I18n.t("mainpile.steeldetail.concreteapprove"):I18n.t("mainpile.steeldetail.steelaprove")
                    }</Text>
                  </View>
                  <View style={{ width: "50%", alignItems: "center" }}>
                    <TouchableOpacity
                      style={[styles.image,this.state.image_steelcage.length>0?{backgroundColor:'#6dcc64'}:{}]}
                      onPress={() => this.onCameraRoll("image_steelcage", this.state.image_steelcage, "edit")}
                    >
                      <Icon name="image" type="entypo" color="#FFF" />
                      <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
            <View>
              <View style={this.props.concretedisabled == false ? styles.box : styles.box_grey}>
                <View style={this.props.concretedisabled == false ? styles.topic : styles.topic_grey}>
                  <Text style={styles.textTopic}>{I18n.t("mainpile.steeldetail.concrete")}</Text>
                </View>
                {this._renderconcreted()}
                <View style={styles.imageDetail}>
                  <View style={{ width: "40%", flexDirection: "row", alignItems: "center", marginLeft: "10%" }}>
                    {this.state.checkconcretespacer ? (
                      <Icon
                        disabled={this.props.concretedisabled}
                        name="check"
                        reverse
                        color="#6dcc64"
                        size={15}
                        onPress={() => {
                          if (this.props.Edit_Flag == 1 && this.props.checkbox == 1) {
                            this.setState({ checkconcretespacer: !this.state.checkconcretespacer })
                          }
                        }}
                      />
                    ) : (
                      <Icon
                        disabled={this.props.concretedisabled}
                        name="check"
                        reverse
                        color="grey"
                        size={15}
                        onPress={() => {
                          if (this.props.Edit_Flag == 1 && this.props.checkbox == 1) {
                            this.setState({ checkconcretespacer: !this.state.checkconcretespacer })
                          }
                        }}
                      />
                    )}
                    <Text>{
                      // I18n.t("mainpile.steeldetail.approve")
                      I18n.t("mainpile.steeldetail.concreteapprove")
                    }</Text>
                  </View>
                  <View style={{ width: "50%", alignItems: "center" }}>
                    <TouchableOpacity
                      style={[styles.image,this.state.image_concretespacer.length>0?{backgroundColor:'#6dcc64'}:{}]}
                      // disabled={this.props.concretedisabled}
                      onPress={() =>
                        this.onCameraRoll(
                          "image_concretespacer",
                          this.state.image_concretespacer,
                          this.props.concretedisabled ? "view" : "edit",
                          true
                          // this.props.process == 7 && this.props.shape == 1?0:1
                        )
                      }
                    >
                      <Icon name="image" type="entypo" color="#FFF" />
                      <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
            {/* <View>
              <View style={ styles.box}>
                <View style={styles.topic }>
                  <Text style={styles.textTopic}>{I18n.t("mainpile.steeldetail.rbsheet.title")}</Text>
                </View>

                <View style={styles.imageDetail}>
                  <View style={{  alignItems: "center", paddingLeft: "10%" }}>
                    <TouchableOpacity
                      style={[styles.image]}
                      onPress={() =>{
                        console.log("rbsheet: " + this.state.sectionDetail.rbsheet)
                        Linking.canOpenURL(this.state.sectionDetail.rbsheet).then(supported => {
                          if (supported) {
                            Linking.openURL(this.state.sectionDetail.rbsheet)
                          } else {
                            console.log("Don't know how to open URI: " + this.state.sectionDetail.rbsheet)
                          }
                        })
                      }}
                    >
                      <Icon name="text-document" type="entypo" color="#FFF" />
                      <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.rbsheet.pdf")}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View> */}
          </View>
        </Content>
        <View>
          <View style={{ flexDirection: "column" }}>
            <View style={this.props.category==3 ? styles.second_super:this.props.shape == 1 ? styles.second : styles.second_dwall}>
              <TouchableOpacity style={styles.firstButton} onPress={() => this.closeButton()}>
                <Text style={styles.selectButton}>{I18n.t("mainpile.steeldetail.button.close")}</Text>
              </TouchableOpacity>
              {this.props.Edit_Flag == 1 ? (
                <TouchableOpacity
                  style={[
                    styles.secondButton,
                    this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                  ]}
                  onPress={() => this.saveButton()}
                >
                  <Text style={styles.selectButton}>{I18n.t("mainpile.steeldetail.button.save")}</Text>
                </TouchableOpacity>
              ) : (
                <View style={styles.secondButtonGrey}>
                  <Text style={styles.selectButton}>{I18n.t("mainpile.steeldetail.button.save")}</Text>
                </View>
              )}
            </View>
          </View>
        </View>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    error: state.pile.error,
    // sectionDetail: state.steelSecDetail.sectionDetail,
    pileAll: state.mainpile.pileAll || null,
    randomlockstep7_casing:state.pile.randomlockstep7_casing,
    process1_success_2:state.pile.process1_success_2,
    process1_success_2_random:state.pile.process1_success_2_random
  }
}

export default connect(mapStateToProps, actions)(SteelDetail)

class Row extends Component {
  render() {
    return (
      <View style={[styles.sub, this.props.main ? { backgroundColor: "#57c3e8" } : {}]}>
        <View style={styles.subtopic}>
          <Text style={styles.textSubTopic}>{this.props.left}</Text>
        </View>
        <View style={{ width: "60%" }}>
          <Text style={styles.textSubTopic}>{this.props.right}</Text>
        </View>
      </View>
    )
  }
}
