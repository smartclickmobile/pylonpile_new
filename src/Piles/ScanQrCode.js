import React, { Component } from "react"
import {
  AppRegistry,
  Dimensions,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  Image,
  TouchableOpacity,
  BackHandler
} from "react-native"
import { RNCamera } from "react-native-camera"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"
import I18n from "../../assets/languages/i18n"
import styles from "./styles/ScanQrCode.style"
import { Icon } from 'react-native-elements'

class ScanQrCode extends Component {
  constructor(props) {
    super(props)
    this.state = {
      qrCode: null
    }
  }
  componentDidMount() {
    BackHandler.addEventListener("back", () => this.onBack())
  }
  componentWillUnmount() {
    BackHandler.removeEventListener("back", () => this.onBack())
  }

  _onBack() {
    Actions.pop({
      refresh: {
        action: "update",
        value: {
          process: this.props.process,
          step: this.props.step,
          isapprove: false,
          approveBy: 3,
          ApprovedUserId: this.state.qrcode
        }
      }
    })
    return true
  }

  onBack() {
    Actions.pop()
    return true
  }

  async onBarCodeRead(e) {
    const url = e.data
    if(url != this.state.qrCode) {
      this.props.clearError()
      this.setState({ qrCode: url }, () => {
        var value = {
          Language: I18n.locale,
          JobId: this.props.jobid,
          PileId: this.props.pileid,
          ApproveType: 3,
          ApprovedUserId: url,
          LocationLat:this.props.lat,
          LocationLong:this.props.log
        }
        this.props.pileSaveApproveStep03(value)

        Actions.pop({
          refresh: {
            action: "update",
            value: {
              process: this.props.process,
              step: this.props.step,
              isapprove: false,
              approveBy: 3,
              ApprovedUserId: this.state.qrCode
            }
          }
        })
        return true
      })
      console.log("onBarCodeRead", url)

      // Alert.alert(url)
      // Linking.openURL(url).catch(err => {
      //   console.log('An error occurred', err);
      //   Alert.alert('Unable to open URL!', "", [
      //     {
      //       text: 'OK',
      //       onPress: () => this.setState({qrCode: null})
      //     }
      //   ]);
      // });
    }
  }

  onApproveQrCode() { }

  render() {
    return (
      <View style={{
        flex: 1,
        paddingTop: 10,
        backgroundColor: '#FFF',
      }}>
        <View style={styles.navigationTitle}>
          <TouchableOpacity activeOpacity={0.8} style={styles.buttonRight} onPress={() => Actions.pop()}>
            <Icon name="close" type="material-community" color="#000" size={40} />
          </TouchableOpacity>
        </View>
        <View style={styles.textView}>
          <Text style={{ fontSize: 30, color: '#000' }}> QR Code </Text>
        </View>
        <RNCamera
          ref={cam => {
            this.camera = cam
          }}
          onBarCodeRead={this.onBarCodeRead.bind(this)}
          style={styles.preview}
        />
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {}
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(ScanQrCode)
