import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  DatePickerAndroid,
  TimePickerAndroid,
  BackHandler
} from "react-native"
import { connect } from "react-redux"
import { Container, Content } from "native-base"
import { Actions } from "react-native-router-flux"
import TableView from "../Components/TableView"
import {
  MAIN_COLOR,
  SUB_COLOR,
  BASIC_COLOR,
  MENU_GREY_ITEM,
  DEFAULT_COLOR_4,
  DEFAULT_COLOR_1
} from "../Constants/Color"
import { Icon } from "react-native-elements"
import I18n from "../../assets/languages/i18n"
import ModalSelector from "react-native-modal-selector"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import styles from "./styles/TremieInsert.style"
import * as actions from "../Actions"
import moment from "moment"
import DateTimePicker from '@react-native-community/datetimepicker';
class ConcreteRegister extends Component {
  constructor(props) {
    super(props)
    this.state = {
      Edit_Flag: 1,
      approvetype: null,
      approveid: null,
      approveuser: [],
      piledetail: [],
      index: 1,
      strength: "",
      unit: "",
      qty: 0,
      slumpmin: "",
      slumpmax: "",
      concretebranddata: [{ key: 0, section: true, label: I18n.t('mainpile.concreteregister.selectconcrete') }],
      concretebrand: I18n.t('mainpile.concreteregister.selectconcrete'),
      concretebrandid: "",
      mixdata: [{ key: 0, section: true, label: I18n.t('mainpile.concreteregister.selectMix') }],
      mix: I18n.t('mainpile.concreteregister.selectMix'),
      mixid: "",
      truckno: "",
      truckconcretevolume: "",
      truckid: null,
      slump: "",
      slumpinvalid: false,
      truckarrival: "",
      truckdepart: "",
      reject: false,
      ticketimage: [],
      truckimage: [],
      slumpimage: [],
      approved: false,
      slumpcheck: false,
      lockconcrete: false,
      lockmix: false,
      appovestatus: 0,
      check10:false,
      status10:0,

      datevisibledate_truckarrival:false,
      datevisibletime_truckarrival:false,
      date_truckarrival:'',

      datevisibledate_truckdepart:false,
      datevisibletime_truckdepart:false,
      date_truckdepart:'',
    }
  }

  updateState(value) {
    // console.warn(value)
  }

  componentWillReceiveProps(nextProps) {
    // var pile = null
    // var pile4_0 = null
    // var pileMaster = null
    // if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["4_1"]) {
    //   pile = nextProps.pileAll[this.props.pileid]["4_1"].data
    //   pileMaster = nextProps.pileAll[this.props.pileid]["4_1"].masterInfo
    // }
    // console.warn(nextProps.value)
    if (nextProps.action == "update") {
      if (nextProps.value.approvetype != undefined) {
        this.setState({
          approvetype: nextProps.value.approvetype,
          approveid: nextProps.value.approveid,
          approved: true
        })
      }
      if (nextProps.value.data != undefined) {
        if (nextProps.value.data.ticketimage != undefined) {
          this.setState({ ticketimage: nextProps.value.data.ticketimage })
        }
        if (nextProps.value.data.slumpimage != undefined) {
          this.setState({ slumpimage: nextProps.value.data.slumpimage })
        }
        if (nextProps.value.data.truckimage != undefined) {
          this.setState({ truckimage: nextProps.value.data.truckimage })
        }
      }
    }

    

    // if (nextProps.error == null) {
    //   console.log('POP back')
    // }
    // else {
    //   console.log('ERROR ', console.log(nextProps.error))
    // }

    // if (nextProps.success == true) {
    //   Actions.pop({
    //     refresh: {
    //       action: "update",
    //       value: {
    //         process: this.props.process,
    //         step: this.props.step,
    //         data: {
    //           TruckNo: this.state.truckno,
    //           TruckArrivalTime: this.state.truckarrival,
    //           TruckDepartureTime: this.state.truckdepart,
    //           TruckConcreteVolume: this.state.truckconcretevolume,
    //           ConcreteSuplierId: this.state.concretebrandid,
    //           MixNoId: this.state.mixid,
    //           Slump: this.state.slump,
    //           ApproveType: 0,
    //           ApproveEmployeeId: 0,
    //           ApproveDate: "0",
    //           IsReject: this.state.reject
    //         }
    //       }
    //     }
    //   })
    // }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("ccback")
  }

  _handleBack() {
    console.log(Actions.currentScene, "BACK")
    if (Actions.currentScene == "concreteregister") {
      Actions.pop()
      return true
    }
    return false
  }

  componentDidMount() {
    BackHandler.addEventListener("ccback", () => this._handleBack())
    var pile = null
    var pileMaster = null
    var edit = null
    if (
      this.props.pileAll[this.props.pileid] &&
      this.props.pileAll[this.props.pileid]["9_1"]
    ) {
      pile = this.props.pileAll[this.props.pileid]["9_1"].data
      pileMaster = this.props.pileAll[this.props.pileid]["9_1"].masterInfo
      edit = this.props.pileAll[this.props.pileid]
    }
    console.log("appovestatus pile", this.props.appovestatus )
    if (pileMaster) {
      if (pileMaster.ConcreteBrandList) {
        let concretelist = [{ key: 0, section: true, label: I18n.t('mainpile.concreteregister.selectconcrete') }]
        pileMaster.ConcreteBrandList.map((data, index) => {
          concretelist.push({
            key: index + 1,
            label: data.Name,
            id: data.Id,
            mixlist: data.MixNoList || []
          })
          if (this.props.edit && this.props.data) {
            var brand = ""
            var mix = ""
            let mixdata = [{ key: 0, section: true, label: I18n.t('mainpile.concreteregister.selectMix') }]
            if (data.Id == this.props.data.ConcreteSuplierId) {
              brand = data.Name
              data.MixNoList.map((data2, index) => {
                mixdata.push({ key: index + 1, label: data2.No, id: data2.Id })
                if (data2.Id == this.props.data.MixNoId) {
                  mix = data2.No
                }
              })
              this.setState({
                concretebrand: brand,
                mix: mix,
                mixdata: mixdata
              })
            }
          }
          if (this.props.concreteid && this.props.mixid) {
            var brand = ""
            var brandid = ""
            var mix = ""
            var mixid = ""
            let mixdata = [{ key: 0, section: true, label: I18n.t('mainpile.concreteregister.selectMix') }]
            if (data.Id == this.props.concreteid) {
              brandid = data.Id
              brand = data.Name
              if(brandid!==""){
                mix = data.MixNoList[0].No
                mixid = data.MixNoList[0].Id
              }
              data.MixNoList.map((data2, index) => {
                mixdata.push({ key: index + 1, label: data2.No, id: data2.Id })
                if (data2.Id == this.props.mixid) {
                  mix = data2.No
                  mixid = data2.Id
                }
              })
              this.setState({
                concretebrandid: brandid,
                concretebrand: brand,
                mixid: mixid,
                mix: mix,
                mixdata: mixdata,
                lockconcrete: true,
                lockmix: true
              })
            }
          }
        })
        this.setState({ concretebranddata: concretelist })
      }

      if (pileMaster.ConcreteInfo) {
        console.log(pileMaster.ConcreteInfo)
        this.setState({
          qty: pileMaster.ConcreteInfo.ConcreteBom,
          slumpmax: pileMaster.ConcreteInfo.SlumpMax,
          slumpmin: pileMaster.ConcreteInfo.SlumpMin,
          strength: pileMaster.ConcreteInfo.Strength,
          unit: pileMaster.ConcreteInfo.Unit
        })
      }

      if (pileMaster.PileDetail) {
        this.setState({ piledetail: pileMaster.PileDetail })
      }

      if (pileMaster.ApprovedUser) {
        this.setState({ approveuser: pileMaster.ApprovedUser })
      }

      if (this.props.edit && this.props.data) {
        this.setState({
          truckid: this.props.data.Id,
          truckno: this.props.data.TruckNo,
          truckarrival: this.props.data.TruckArrivalTime  ? moment(this.props.data.TruckArrivalTime,'DD/MM/YYYY HH:mm').format('DD/MM/YYYY HH:mm'):"",
          truckdepart: this.props.data.TruckDepartureTime ? moment(this.props.data.TruckDepartureTime,'DD/MM/YYYY HH:mm').format('DD/MM/YYYY HH:mm'):"",
          truckconcretevolume: this.props.data.TruckConcreteVolume,
          concretebrandid: this.props.data.ConcreteSuplierId,
          mixid: this.props.data.MixNoId,
          slump: this.props.data.Slump || "",
          ticketimage: this.props.data.ImageTicket,
          truckimage: this.props.data.ImageTruck,
          slumpimage: this.props.data.ImageSlump,
          reject: this.props.data.IsReject,
          approvetype: this.props.data.ApproveType ,
          approved: this.props.data.ApproveType != null,
          approveid: this.props.data.ApproveEmployeeId,
          Edit_Flag: this.props.Edit_Flag,
          slumpcheck: this.props.data.Slump == null,
          appovestatus: this.props.appovestatus,
          check10:this.props.check10
        },()=>{
          // console.warn("approvetype",this.props.data)
          if(edit!=undefined){
            this.setState({status10:edit.step_status.Step10Status})
          }
          if(this.state.appovestatus==4){
            this.onChangeSlump(this.state.slump)
          }
          // console.warn("data reg",this.props.data)
        })
      }
    }

    if (this.props.TremieList != undefined) {
      this.setState({
        tremieimage: this.props.TremieList.image,
        tremie: this.props.TremieList.size,
        tremieid: this.props.TremieList.tremieid,
        tremielength: this.props.TremieList.length,
        last: this.props.TremieList.last,
        index: this.props.index
      })
    }
  }

  onApprove() {
    // console.warn(this.props.data.ApproveType)
    Actions.slumpapprove({
      process: this.props.process,
      step: this.props.step,
      jobid: this.props.jobid,
      pileid: this.props.pileid,
      approveuser: this.state.approveuser,
      piledetail: this.state.piledetail,
      truckno: this.state.truckno,
      concretebrand: this.state.concretebrand,
      mix: this.state.mix,
      strength: this.state.strength,
      unit: this.state.unit,
      truckconcretevolume: this.state.truckconcretevolume,
      slump: this.state.slump,
      truckarrival: this.state.truckarrival,
      truckdepart: this.state.truckdepart,
      slumpimage: this.state.slumpimage,
      lat: this.props.lat,
      log: this.props.log,
      category: this.props.category,
      check10:this.props.check10,
      // approveid: this.props.data.ApproveEmployeeId,
      ApproveEmployeeId: this.props.data != null && this.props.data.ApproveEmployeeId != null ? this.props.data.ApproveEmployeeId : null
    })
  }

  onCancelApprove(){
    // const {truckno} = this.props
    Alert.alert("", I18n.t('mainpile.concreteregister.needcancel'), [
      {
        text: "Cancel",
        // onPress: () => this.setState({ select_user: "", approveBy: "", ApprovedUserId: "" })
      },
      {
        text: "OK",
        onPress: () => {

          var value = {
            Language: I18n.locale,
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            TruckRegisterId: this.state.truckid,
            latitude: this.props.lat,
            longitude: this.props.log,
          }
          console.log("vale con" ,value)
          this.props.pileCancelSlumpApprove(value)
          Actions.pop({
            refresh: {
              action: 'refresh',
            }
          })
          // this.setState({submit:true})
        }
      }
    ],
    { cancelable: false })
  }

  selectConcreteBrand = data => {
    let mixdata = [{ key: 0, section: true, label: I18n.t('mainpile.concreteregister.selectMix') }]
    data.mixlist.map((data, index) => {
      mixdata.push({ key: index + 1, label: data.No, id: data.Id })
    })
    
    this.setState({
      concretebrand: data.label,
      concretebrandid: data.id,
      mixdata: mixdata,
      mix: data.mixlist[0].No,
      mixid:data.mixlist[0].Id
      // mix: I18n.t('mainpile.concreteregister.selectMix'),
      // mixid: ""
    })
  }

  selectMixNo = data => {
    this.setState({ mix: data.label, mixid: data.id })
  }

  closeButton() {
    Actions.pop()
  }

  onChangeTremieLength(length) {
    this.setState({ tremielength: length })
  }

  onCameraRoll(keyname, photos, type) {
    if (this.state.Edit_Flag == 1) {
      Actions.camearaRoll({
        jobid: this.props.jobid,
        process: this.props.process,
        step: this.props.step,
        pileId: this.props.pileid,
        keyname: keyname,
        photos: photos,
        typeViewPhoto: type,
        shape: this.props.shape,
        category: this.props.category,
        Edit_Flag:this.state.Edit_Flag
      })
    } else {
      Actions.camearaRoll({
        jobid: this.props.jobid,
        process: this.props.process,
        step: this.props.step,
        pileId: this.props.pileid,
        keyname: keyname,
        photos: photos,
        typeViewPhoto: "view",
        shape: this.props.shape,
        category: this.props.category,
        Edit_Flag:this.state.Edit_Flag
      })
    }
  }

  saveButton() {
    let truckarrival = moment(this.state.truckarrival, "DD/MM/YYYY HH:mm")
    let truckdepart = moment(this.state.truckdepart, "DD/MM/YYYY HH:mm")
    // Validate
    if (
      this.state.truckarrival == "" 
    ) {
     
      Alert.alert("", I18n.t("mainpile.concreteregister.errortruckarrival"), [
        {
          text: "OK"
        }
      ])
      return
    }
    if (
     
      this.state.concretebrandid == ""
    ) {
     
      Alert.alert("", I18n.t("mainpile.concreteregister.errorconcretebrand"), [
        {
          text: "OK"
        }
      ])
      return
    }
    if (
      this.state.mixid == ""
    ) {
     
      Alert.alert("", I18n.t("mainpile.concreteregister.errormixno"), [
        {
          text: "OK"
        }
      ])
      return
    }
    
    
    if (
      this.state.truckno == ""
    ) {
     
      Alert.alert("", I18n.t("mainpile.concreteregister.errortruckno"), [
        {
          text: "OK"
        }
      ])
      return
    }
    if (
      this.state.truckconcretevolume == "" ||this.state.truckconcretevolume ==null
    ) {
      
      Alert.alert("", I18n.t("mainpile.concreteregister.errorconcrete"), [
        {
          text: "OK"
        }
      ])
      return
    }

    if (this.state.slumpcheck == false && this.state.slump == "") {
      Alert.alert("", I18n.t("mainpile.concreteregister.no_slump"), [
        {
          text: "OK"
        }
      ])
      return
    }
    if(truckdepart >= truckarrival){
      Alert.alert("", I18n.t("mainpile.concreteregister.errortime"), [
        {
          text: "OK"
        }
      ])
      return
    }
    if((this.state.check10==true)&&(this.props.data.IsReject==false&&this.state.reject==true)){
      Alert.alert("", I18n.t("mainpile.concreteregister.warn3"), [
        {
          text: "OK"
        }
      ])
      return
    }
    Actions.pop()
    this.save()

    // if (this.props.edit && this.state.last && !this.props.allowlast && !this.props.TremieList.last) {
    //   Alert.alert("", "ไม่สามารถเลือกท่อสุดท้ายได้", [
    //     {
    //       text: "OK"
    //     }
    //   ])
    //   return
    // }
    // if (this.state.tremielength != "" && this.state.tremie != "") {
    //   Actions.pop({
    //     refresh: {
    //       action: "update",
    //       value: {
    //         process: this.props.process,
    //         step: this.props.step,
    //         data: {
    //           image: this.state.tremieimage,
    //           tremiesize: this.state.tremie,
    //           tremieid: this.state.tremieid,
    //           length: parseFloat(this.state.tremielength),
    //           last: this.state.last,
    //           edit: this.props.edit,
    //           index: this.props.index
    //         }
    //       }
    //     }
    //   })
    //   this.save()
    // } else {
    //   Alert.alert("", I18n.t("mainpile.4_1.error_nextstep"), [
    //     {
    //       text: "OK"
    //     }
    //   ])
    // }
  }

  save() {
    var valueApi = {
      Language: I18n.currentLocale(),
      latitude: this.props.lat,
      longitude: this.props.log,
      JobId: this.props.jobid,
      PileId: this.props.pileid,
      TruckRegisterId: this.state.truckid,
      TruckNo: this.state.truckno,
      TruckArrivalTime: moment(
        this.state.truckarrival,
        "DD/MM/YYYY HH:mm"
      ).format("YYYY-MM-DD HH:mm:ss"),
      TruckDepartureTime: moment(
        this.state.truckdepart,
        "DD/MM/YYYY HH:mm"
      ).format("YYYY-MM-DD HH:mm:ss"),
      TruckConcreteVolume: this.state.truckconcretevolume,
      ConcreteSuplierId: this.state.concretebrandid,
      ConcreteSuplierName:this.state.concretebrand,
      MixNoId: this.state.mixid,
      MixNoName:this.state.mix,
      Slump: this.state.slump,
      ticketimage: this.state.ticketimage,
      truckimage: this.state.truckimage,
      slumpimage: this.state.slumpimage,
      IsReject: this.state.reject,
      ApproveType: this.state.approvetype,
      ApproveEmployeeId: this.state.approveid,
      SlumpFlag: this.state.slumpcheck,
      No:this.props.index,
      latitude:this.props.lat,
      longitude:this.props.log
    }
    console.log("SAVE concrete register ", valueApi)
    if(this.state.status10==2&&this.props.data.IsReject==true&&this.state.reject==false){
      Alert.alert("", I18n.t("mainpile.concreteregister.warn2"), [
        {
          text: "OK"
        }
      ])
    }
    this.props.pileSaveStep9(valueApi)
  }

  onArriveSelect() {
    var now = moment().format("DD/MM/YYYY HH:mm")
    this.setState({ truckarrival: now })
  }

  onSlumpCheck() {
    this.setState({
      slumpcheck: !this.state.slumpcheck,
      slumpinvalid: false,
      slump: ""
    })
  }

  async onArriveSelectDate2() {
    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        date: new Date(moment().format("YYYY"), month, moment().format("D"))
      })
      if (action !== DatePickerAndroid.dismissedAction) {
        var date = new Date(year, month, day)
        date = moment(date).format("DD/MM/YYYY")
        try {
          const { action, hour, minute } = await TimePickerAndroid.open({
            is24Hour: false,
            hour: 23,
            minute: 0
          })
          if (action !== TimePickerAndroid.dismissedAction) {
            var second = moment().format("ss")
            var datepick = moment(
              date + " " + hour + ":" + minute + ":" + second,
              "DD/MM/YYYY HH:mm:ss"
            ).format("DD/MM/YYYY HH:mm")
            this.setState({ truckarrival: datepick })
          }
        } catch ({ code, message }) {
          // console.warn("Cannot open time picker", message)
        }
      }
    } catch ({ code, message }) {
      // console.warn("Cannot open date picker", message)
    }
  }
  onArriveSelectDate(date){
    let date2 = moment(date).format("DD/MM/YYYY HH:mm")
    this.setState({ truckarrival: date2 },()=>{console.log(this.state.truckdepart)})
  }
  onDepartSelect(date){
    let date2 = moment(date).format("DD/MM/YYYY HH:mm")
    this.setState({ truckdepart: date2 },()=>{console.log(this.state.truckdepart)})
  }


  async onDepartSelect2() {
    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        date: new Date(moment().format("YYYY"), month, moment().format("D"))
      })
      if (action !== DatePickerAndroid.dismissedAction) {
        var date = new Date(year, month, day)
        date = moment(date).format("DD/MM/YYYY")
        try {
          const { action, hour, minute } = await TimePickerAndroid.open({
            is24Hour: false,
            
          })
          if (action !== TimePickerAndroid.dismissedAction) {
            var second = moment().format("ss")
            var datepick = moment(
              date + " " + hour + ":" + minute + ":" + second,
              "DD/MM/YYYY HH:mm:ss"
            ).format("DD/MM/YYYY HH:mm")
            this.setState({ truckdepart: datepick })
          }
        } catch ({ code, message }) {
          // console.warn("Cannot open time picker", message)
        }
      }
    } catch ({ code, message }) {
      // console.warn("Cannot open date picker", message)
    }
  }

  onChangeSlump = value => {
    var invalid = true
    var slump = parseFloat(value)
    if (this.state.slumpmin <= slump && slump <= this.state.slumpmax) {
      invalid = false
    }

    if(this.state.check10==true&&this.state.approveid!=null){
      this.setState({ slump: value, slumpinvalid: false },()=>{
        if(this.state.approved == true){
          this.setState({ approved: !this.state.approved})
        }
      })
    }else{
      this.setState({ slump: value, slumpinvalid: invalid },()=>{
        if(this.state.approved == true){
          this.setState({ approved: !this.state.approved})
        }
      })
    }
   
  }

  onChangeFormatDecimal(data, decimal = 3) {
    if (data.split(".")[1]) {
      if (data.split(".")[1].length > decimal) {
        return parseFloat(data).toFixed(decimal)
      } else {
        return data
      }
    } else {
      return data
    }
  }
  onChangedate_truckarrival = (event, selectedDate) => {
    if(event.type == 'set'){
      var date = moment(selectedDate).format('DD-MM-YYYY')
      this.setState({date_truckarrival:date,datevisibledate_truckarrival:false,datevisibletime_truckarrival:true})
    }else{
      this.setState({datevisibledate_truckarrival: false,datevisibletime_truckarrival: false})
    }
  };
  onChangetime_truckarrival = (event, selectedDate) => {
    var time = moment(selectedDate).format('HH:mm')
    var datetime_str = this.state.date_truckarrival + time
    var datetime = moment(datetime_str, 'DD-MM-YYYY HH:mm')
    if(event.type == 'set'){
      this.setState({datevisibletime_truckarrival:false})
      this.onArriveSelectDate(datetime)
    }else{
      this.setState({datevisibledate_truckarrival: false,datevisibletime_truckarrival: false})
    }
  };

  onChangedate_truckdepart = (event, selectedDate) => {
    if(event.type == 'set'){
      var date = moment(selectedDate).format('DD-MM-YYYY')
      this.setState({date_truckdepart:date,datevisibledate_truckdepart:false,datevisibletime_truckdepart:true})
    }else{
      this.setState({datevisibledate_truckdepart: false,datevisibletime_truckdepart: false})
    }
  };
  onChangetime_truckdepart = (event, selectedDate) => {
    var time = moment(selectedDate).format('HH:mm')
    var datetime_str = this.state.date_truckdepart + time
    var datetime = moment(datetime_str, 'DD-MM-YYYY HH:mm')
    if(event.type == 'set'){
      this.setState({datevisibletime_truckdepart:false})
      this.onDepartSelect(datetime)
    }else{
      this.setState({datevisibledate_truckdepart: false,datevisibletime_truckdepart: false})
    }
  };
  render() {
    console.warn("Edit_Flag ",this.state.Edit_Flag ,"appovestatus",this.state.appovestatus)
    let currenttime  = new Date()
    let truckarrival = new Date(moment(this.state.truckarrival,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm'))
    let truckdepart = new Date(moment(this.state.truckdepart,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm'))
    return (
      <View style={styles.listContainer}>
        <View style={
          this.props.category == 3|| this.props.category == 5 ? [styles.listNavigation,{backgroundColor: DEFAULT_COLOR_4}]:this.props.shape == 1 ? styles.listNavigation : [styles.listNavigation, {backgroundColor:DEFAULT_COLOR_1}] 
        }>
          <Text style={styles.listNavigationText}>
            {I18n.t('mainpile.concreteregister.concreteregister')}
          </Text>
        </View>
        <View style={styles.listNavigationStep}>
          <Text style={styles.listNavigationTextStep}>
            {I18n.t('mainpile.concreteregister.carNo') + this.props.index || 1}
          </Text>
        </View>
        <ScrollView>
        {/* <KeyboardAwareScrollView
          scrollEnabled={true}
          resetScrollToCoords={{ x: 0, y: 0 }}
          enableOnAndroid={true}
          keyboardShouldPersistTaps="always"
        > */}
          <View style={styles.listRow}>
            <View style={styles.listTopicSub}>
              <Text style={styles.listTopicSubText}>{I18n.t('mainpile.concreteregister.carcome')}</Text>
              <View style={{ flexDirection: "row", justifyContent: "center" }}>
              {this.state.datevisibledate_truckarrival &&<DateTimePicker
                value={this.state.truckarrival!="" ? truckarrival  : currenttime}
                isVisible={this.state.datevisibledate_truckarrival}
                is24Hour={true}
                display='spinner'
                mode='date'
                onChange={this.onChangedate_truckarrival}
              />}
              {this.state.datevisibletime_truckarrival && <DateTimePicker
                value={this.state.truckarrival!="" ? truckarrival  : currenttime}
                isVisible={this.state.datevisibletime_truckarrival}
                is24Hour={true}
                display='spinner'
                mode='time'
                onChange={this.onChangetime_truckarrival}
              />}
                <TouchableOpacity
                  style={[styles.buttonBox, { width: "68%" }]}
                  onPress={() => {
                   
                    this.setState({datevisibledate_truckarrival:true})
                  }}
                  disabled={this.state.Edit_Flag == 0}
                >
                  <Text >{this.state.truckarrival}</Text>
                </TouchableOpacity>
                {/* <TextInput
                  value={this.state.truckarrival}
                  editable={false}
                  keyboardType="numeric"
                  underlineColorAndroid="transparent"
                  style={[styles.listTextbox, { width: "68%" }]}
                  /> */}
                <TouchableOpacity
                  style={styles.approveBox}
                  onPress={() => this.onArriveSelect()}
                  disabled={this.state.Edit_Flag == 0}
                >
                  <Text>{I18n.t('mainpile.concreteregister.come')}</Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.listTopicSub}>
              <Text style={styles.listTopicSubText}>{I18n.t('mainpile.concreteregister.concrete')}</Text>
              <View style={styles.listSelectedView}>
                <ModalSelector
                  data={this.state.concretebranddata}
                  onChange={this.selectConcreteBrand}
                  selectTextStyle={styles.textButton}
                  cancelText="Cancel"
                  disabled={
                    this.state.Edit_Flag == 0 
                    // || this.state.lockconcrete||this.state.appovestatus==2||this.state.appovestatus==3
                  }
                >
                  <TouchableOpacity
                    style={[
                      styles.button,
                      (this.state.Edit_Flag == 0 
                        // ||this.state.appovestatus==2||this.state.appovestatus==3||
                        // this.state.lockconcrete
                        ) 
                        && {
                        borderColor: MENU_GREY_ITEM
                      }
                    ]}
                  >
                    <View style={styles.buttonTextStyle}>
                      <Text
                        style={[
                          styles.buttonText,
                          (this.state.Edit_Flag == 0 
                            // ||this.state.appovestatus==2||this.state.appovestatus==3||
                            // this.state.lockconcrete
                            ) && {
                            color: MENU_GREY_ITEM
                          }
                        ]}
                      >
                        {this.state.concretebrand}
                      </Text>
                      <View
                        style={{
                          justifyContent: "flex-end",
                          flexDirection: "row"
                        }}
                      >
                        <Icon
                          name="arrow-drop-down"
                          type="MaterialIcons"
                          color={
                            this.state.Edit_Flag == 0 
                            // ||this.state.appovestatus==2||this.state.appovestatus==3|| this.state.lockconcrete
                              ? MENU_GREY_ITEM
                              : "#007CC2"
                          }
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                </ModalSelector>
              </View>
            </View>
            <View style={styles.listTopicSub}>
              <Text style={styles.listTopicSubText}>{I18n.t('mainpile.concreteregister.NoMix')}</Text>
              <View style={styles.listSelectedView}>
                <ModalSelector
                  data={this.state.mixdata}
                  onChange={this.selectMixNo}
                  selectTextStyle={styles.textButton}
                  cancelText="Cancel"
                  disabled={this.state.Edit_Flag == 0 
                    // || this.state.lockmix||this.state.appovestatus==2||this.state.appovestatus==3
                  }
                >
                  <TouchableOpacity
                    style={[
                      styles.button,
                      (this.state.Edit_Flag == 0 
                        // ||this.state.appovestatus==2||this.state.appovestatus==3|| this.state.lockmix
                        ) && {
                        borderColor: MENU_GREY_ITEM
                      }
                    ]}
                  >
                    <View style={styles.buttonTextStyle}>
                      <Text
                        style={[
                          styles.buttonText,
                          (this.state.Edit_Flag == 0 
                            // ||this.state.appovestatus==2||this.state.appovestatus==3|| this.state.lockmix
                            ) && {
                            color: MENU_GREY_ITEM
                          }
                        ]}
                      >
                        {this.state.mix}
                      </Text>
                      <View
                        style={{
                          justifyContent: "flex-end",
                          flexDirection: "row"
                        }}
                      >
                        <Icon
                          name="arrow-drop-down"
                          type="MaterialIcons"
                          color={
                            this.state.Edit_Flag == 0 
                            // ||this.state.appovestatus==2||this.state.appovestatus==3|| this.state.lockmix
                              ? MENU_GREY_ITEM
                              : "#007CC2"
                          }
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                </ModalSelector>
              </View>
            </View>

            <View style={styles.listTopicSub}>
              <Text style={styles.listTopicSubText}>Strength (ksc.)</Text>
              <TextInput
                value={this.state.strength}
                editable={false}
                keyboardType="numeric"
                underlineColorAndroid="transparent"
                style={[styles.listTextbox, { backgroundColor: "#ebf1f7" }]}
              />
            </View>

            <View style={styles.listTopicSub}>
              <Text style={styles.listTopicSubText}>Unit</Text>
              <TextInput
                value={this.state.unit}
                editable={false}
                keyboardType="numeric"
                underlineColorAndroid="transparent"
                style={[styles.listTextbox, { backgroundColor: "#ebf1f7" }]}
              />
            </View>
            <View style={{ marginTop: 10, flexDirection: "row" }}>
              <View style={{ width: "70%" }}>
                <Text>{I18n.t('mainpile.concreteregister.planconcrete')}</Text>
              </View>
              <Text>{this.state.qty.toFixed(2)}</Text>
            </View>
            <View style={{ marginTop: 10, flexDirection: "row" }}>
              <View style={{ width: "70%" }}>
                <Text>{I18n.t('mainpile.concreteregister.concretecol')}</Text>
              </View>
              <Text>
                {this.props.concretecumulative.toFixed(2)}
              </Text>
            </View>
            <View style={styles.listTopicSub}>
              <Text style={styles.listTopicSubText}>{I18n.t('mainpile.concreteregister.Nocar')}</Text>
              <TextInput
                
                editable={this.state.Edit_Flag != 0&&this.state.appovestatus!=3}
                value={this.state.truckno.toString()}
                underlineColorAndroid="transparent"
                style={[styles.listTextbox,this.state.appovestatus==3 &&{color:MENU_GREY_ITEM}]}
                onChangeText={data => {
                  if(this.state.check10==true){
                    Alert.alert('', I18n.t('mainpile.concreteregister.warn1'), [
                      {
                        text: 'OK',
                      }
                      ])
                  }else{
                    this.setState({ truckno: data })
                  }
                  
                }}
                onSubmitEditing={()=>{
                  this.truckconcretevolumeInput.focus()
                }}
              />
            </View>

            <View style={styles.listTopicSub}>
              <Text style={styles.listTopicSubText}>
                {I18n.t('notificationdetail.concrete_per')}
              </Text>
              <TextInput
                ref={(input) => { this.truckconcretevolumeInput = input; }}
                editable={this.state.Edit_Flag != 0&&this.state.appovestatus!=3}
                value={this.state.truckconcretevolume.toString()}
                keyboardType="numeric"
                underlineColorAndroid="transparent"
                style={[styles.listTextbox,this.state.appovestatus==3 &&{color:MENU_GREY_ITEM}]}
                onChangeText={data =>
                  {
                    if(this.state.check10==true){
                      Alert.alert('', I18n.t('mainpile.concreteregister.warn1'), [
                        {
                          text: 'OK',
                        }
                        ])
                    } else {
                      this.setState({
                        truckconcretevolume: this.onChangeFormatDecimal(
                          data.toString(),
                          2
                        )
                      })
                    }
                  }
                }
                onEndEditing={() =>
                  {
                    let truckconcretevolume =  parseFloat(Math.abs(this.state.truckconcretevolume)).toFixed(2)
                    // console.warn(isNaN(truckconcretevolume))
                    this.setState({
                      truckconcretevolume: this.state.truckconcretevolume
                        ? (isNaN(truckconcretevolume) == true ? "":parseFloat(Math.abs(this.state.truckconcretevolume)).toFixed(2))
                        : ""
                    })
                  }
                }
                onSubmitEditing={()=>{
                  if(this.state.slumpcheck ==false){
                    this.slumpInput.focus()
                  }
                  
                }}
              />
            </View>

            <View style={styles.checkView}>
              {this.state.slumpcheck == true ? (
                <Icon
                  disabled={this.state.Edit_Flag == 0 ||this.state.appovestatus==3}
                  name="check"
                  reverse
                  color="#6dcc64"
                  size={15}
                  onPress={() => this.onSlumpCheck()}
                />
              ) : (
                <Icon
                  disabled={this.state.Edit_Flag == 0||this.state.appovestatus==3}
                  name="check"
                  reverse
                  color="grey"
                  size={15}
                  onPress={() => this.onSlumpCheck()}
                />
              )}
              <Text>{I18n.t('mainpile.concreteregister.no_slump')}</Text>
            </View>
            {!this.state.slumpcheck && (
              <View style={styles.listTopicSub}>
                <Text style={styles.listTopicSubText}>
                {I18n.t('mainpile.concreteregister.slump')} ({this.state.slumpmin}-{this.state.slumpmax} cm.)
                </Text>
                <View
                  style={{ flexDirection: "row", justifyContent: "center" }}
                >
                  <TextInput
                    ref={(input) => { this.slumpInput = input; }}
                    editable={this.state.Edit_Flag != 0&&this.state.appovestatus!=3}
                    value={this.state.slump.toString()}
                    keyboardType="numeric"
                    underlineColorAndroid="transparent"
                    style={[
                      styles.listTextbox,
                      { width: "68%" },
                      this.state.slumpinvalid
                        ? { borderColor: "red", color: "red" }
                        : {},this.state.appovestatus==3 &&{color:MENU_GREY_ITEM}
                    ]}
                    onChangeText={this.onChangeSlump}
                  />
                  <TouchableOpacity
                    style={[
                      styles.approveBox,
                      this.state.slumpinvalid
                        ? {}
                        : { backgroundColor: MENU_GREY_ITEM },
                      this.state.approved ? { backgroundColor: "#6dcc64" } : {},this.state.appovestatus==3&&{ backgroundColor: MENU_GREY_ITEM }
                    ]}
                    disabled={
                      !this.state.slumpinvalid || this.state.Edit_Flag == 0||this.state.appovestatus==3
                      // this.state.appovestatus == (2 || 3)
                    }
                    onPress={() => this.onApprove()}
                  >
                    <Text>{I18n.t('mainpile.concreteregister.approve')}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            )}
            <View style={styles.listTopicSub}>
              <Text style={styles.listTopicSubText}>{I18n.t('mainpile.concreteregister.timecarout')}</Text>
              {this.state.datevisibledate_truckdepart &&<DateTimePicker
                value={this.state.truckdepart!="" ? truckdepart  : currenttime}
                isVisible={this.state.datevisibledate_truckdepart}
                is24Hour={true}
                display='spinner'
                mode='date'
                onChange={this.onChangedate_truckdepart}
              />}
              {this.state.datevisibletime_truckdepart && <DateTimePicker
                value={this.state.truckdepart!="" ? truckdepart  : currenttime}
                isVisible={this.state.datevisibletime_truckdepart}
                is24Hour={true}
                display='spinner'
                mode='time'
                onChange={this.onChangetime_truckdepart}
              />}
              <TouchableOpacity
                style={styles.buttonBox}
                onPress={() => this.setState({ datevisibledate_truckdepart: true })}
                disabled={this.state.Edit_Flag == 0}
              >
                <Text>{this.state.truckdepart}</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.listTopicSubRow}>
              <Text style={styles.listLabel}>{I18n.t('notificationdetail.img_concrete')}</Text>
              <View style={styles.buttonImage}>
                <TouchableOpacity
                  style={[styles.image,this.state.ticketimage.length>0?{backgroundColor:'#6dcc64'}:{}]}
                  onPress={() =>
                    this.onCameraRoll(
                      "ticketimage",
                      this.state.ticketimage,
                      "edit"
                    )
                  }
                >
                  <Icon name="image" type="entypo" color="#fff" />
                  <Text style={styles.imageText}>
                    {I18n.t("mainpile.liquidtestinsert.view")}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.listTopicSubRow}>
              <Text style={styles.listLabel}>{I18n.t('notificationdetail.img_nocar')}</Text>
              <View style={styles.buttonImage}>
                <TouchableOpacity
                  style={[styles.image,this.state.truckimage.length>0?{backgroundColor:'#6dcc64'}:{}]}
                  onPress={() =>
                    this.onCameraRoll(
                      "truckimage",
                      this.state.truckimage,
                      "edit"
                    )
                  }
                >
                  <Icon name="image" type="entypo" color="#fff" />
                  <Text style={styles.imageText}>
                    {I18n.t("mainpile.liquidtestinsert.view")}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.listTopicSubRow}>
              <Text style={styles.listLabel}>{I18n.t('notificationdetail.img_slump')}</Text>
              <View style={styles.buttonImage}>
                <TouchableOpacity
                  style={[styles.image,this.state.slumpimage.length>0?{backgroundColor:'#6dcc64'}:{}]}
                  onPress={() =>
                    this.onCameraRoll(
                      "slumpimage",
                      this.state.slumpimage,
                      "edit"
                    )
                  }
                >
                  <Icon name="image" type="entypo" color="#fff" />
                  <Text style={styles.imageText}>
                    {I18n.t("mainpile.liquidtestinsert.view")}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.lastView}>
              {this.state.reject == true ? (
                <Icon
                  disabled={this.state.Edit_Flag == 0||this.state.appovestatus==3}
                  name="check"
                  reverse
                  color="#6dcc64"
                  size={15}
                  
                  onPress={() => this.setState({ reject: !this.state.reject })}
                />
              ) : (
                <Icon
                  disabled={this.state.Edit_Flag == 0||this.state.appovestatus==3}
                  name="check"
                  reverse
                  color="grey"
                  size={15}
                  onPress={() => this.setState({ reject: !this.state.reject })}
                />
              )}
              <Text>{I18n.t("mainpile.concreteregister.noRejister")}</Text>
            </View>
           {this.props.appovestatus == 1 && this.props.Edit_Flag == 1 &&
            (<TouchableOpacity
              style={{
                margin: 10,
                backgroundColor: "#ff7a73",
                height: 50,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 5}}
              onPress={() => {
                this.props.clearError()
                this.onCancelApprove()
              }}
            >
              <Text style={styles.selectButton}>{I18n.t("mainpile.concreteregister.cancelAppove")}</Text>
            </TouchableOpacity>)
            }
          </View>
        {/* </KeyboardAwareScrollView> */}
        </ScrollView>
        {/* Bottom */}
        <View style={styles.buttonFixed}>
          <View style={styles.second}>
            <TouchableOpacity
              style={styles.firstButton}
              onPress={() => this.closeButton()}
            >
              <Text style={styles.selectButton}>
                {I18n.t("mainpile.steeldetail.button.close")}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.secondButton,
                this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1},
                (this.state.slumpinvalid && !this.state.approved)||(this.props.Edit_Flag!=1)
                  ? { backgroundColor: "#BABABA" }
                  : {}
              ]}
              onPress={() => this.saveButton()}
              disabled={(this.state.slumpinvalid && !this.state.approved)||(this.props.Edit_Flag!=1)}
            >
              <Text style={styles.selectButton}>
                {I18n.t("mainpile.steeldetail.button.save")}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll || null
  }
}
export default connect(mapStateToProps, actions)(ConcreteRegister)
