import React, { Component } from "react"
import {
  View,
  TextInput,
  Text,
  TouchableOpacity,
  ScrollView,
  Keyboard,
  Dimensions,
  ActivityIndicator
} from "react-native"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"
import { Icon } from "react-native-elements"
import firebase from "react-native-firebase"
import I18n from "../../assets/languages/i18n"
import {
  MAIN_COLOR,
  DEFAULT_COLOR_1,
  DEFAULT_COLOR_4,
  SUB_COLOR,
  MENU_GREY
} from "../Constants/Color"

class Note extends Component {
  constructor(props) {
    super(props)
    this.state = {
      note: '',
      bottom: 0,
      noteimage: [],
      location: null,
      loading: true,
      imagePress: false
    }
  }

  updateValue(text) {
    // const path = `jobid/${this.props.jobid}/pileid/${this.props.pileid}`
    // const ref = firebase.database().ref(path)
    // ref.update({ note: text })
    this.setState({ note: text })
  }

  keyboardWillShow(e) {
    console.log(e)
    this.setState({ bottom: e.endCoordinates.height })
  }

  keyboardWillHide(e) {
    this.setState({ bottom: 0 })
  }

  async componentDidMount() {
    // this.keyboardshow = Keyboard.addListener(
    //   "keyboardDidShow",
    //   this.keyboardWillShow.bind(this)
    // )
    // this.keyboardhide = Keyboard.addListener(
    //   "keyboardDidHide",
    //   this.keyboardWillHide.bind(this)
    // )
    // const path = `jobid/${this.props.jobid}/pileid/${this.props.pileid}`
    // const ref = firebase.database().ref(path)
    // ref.on("value", this.callback)
    // this.getLocation()
    // console.warn(this.props.process)
    await this.props.mainPileGetAllStorage()
    const data = {
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      processid: this.props.process
    }
    await this.props.getNote(data)
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.note != null &&
      nextProps.note.Detail != null &&
      this.state.imagePress == false &&
      this.props.process == nextProps.note.ProcessId
    ) {
      // console.warn("updare", this.state.note)
      // console.warn("updare", nextProps.note)
      // console.warn(nextProps.note)
      this.setState({
        note: nextProps.note.Detail,
        noteimage: nextProps.note.Images,
        loading: false
      })
    } else {
      this.setState({
        loading: false
      })
    }
    if (nextProps.image != null && this.state.imagePress == true) {
      // console.warn("noteimage state", this.state.noteimage)
      this.setState(
        { noteimage: nextProps.image.data.noteimage, imagePress: false },
        () => {
          // console.warn("noteimage state", this.state.noteimage.length)
        }
      )
    }
    // if (nextProps.image==null&&nextProps.note==null&&this.state.imagePress==false){
    //   this.setState({
    //     loading: false,
    //     note: null,
    //     noteimage: [],
    //     imagePress: false
    //   })
    // }

    if (this.props.category == 3|| this.props.category == 5) {
      Actions.refresh({
        navigationBarStyle: { backgroundColor: DEFAULT_COLOR_4 }
      })
    } else if (this.props.shape == 2) {
      Actions.refresh({
        navigationBarStyle: { backgroundColor: DEFAULT_COLOR_1 }
      })
    }
    // if (nextProps.image) {
    //   // console.warn("noteimage", nextProps.image.data)
    //   this.setState({ noteimage: nextProps.image.data.noteimage }, () => {
    //     console.warn("noteimage state", this.state.noteimage)
    //   })
    // }
    if (nextProps.pileAll!=null&& nextProps.pileAll.currentLocation !=null) {
      // console.warn(nextProps.pileAll)
      this.setState({ location: nextProps.pileAll.currentLocation },()=>{console.warn("locationnote",this.state.location.position)})
    }
  }

  componentWillUnmount() {
    // this.keyboardshow.remove()
    // this.keyboardhide.remove()
    // const path = `jobid/${this.props.jobid}/pileid/${this.props.pileid}`
    // const ref = firebase.database().ref(path)
    // ref.off()
  }

  saveButton() {
    const data = {
      language: I18n.currentLocale(),
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      processid: this.props.process,
      detail: this.state.note,
      noteimage: this.state.noteimage,
      lat:this.state.location.position.lat,
      log: this.state.location.position.log
    }
    // console.warn(data)
    this.props.saveNote(data)
    Actions.pop()
  }

  closeButton() {
    Actions.pop()
  }

  callback = data => {
    if (data.val()) {
      this.setState({ note: data.val().note })
    }
  }
  onCameraroll(keyname, photos, type) {
    // console.warn(this.state.note)
    // this.setState({loading:true},()=>{
    this.setState({ imagePress: true }, () => {
      Keyboard.dismiss()
      Actions.camearaRoll({
        jobid: this.props.jobid,
        process: this.props.process,
        pileId: this.props.pileid,
        keyname: keyname,
        photos: photos,
        typeViewPhoto: this.props.viewType ==true?"view":type,
        Edit_Flag: this.props.viewType ==true?"0":"1",
        shape: this.props.shape,
        category: this.props.category,
      
        // note:this.state.note
      })
    })
  }

  render() {
    //  console.warn("render", this.props.viewType)
    // console.warn(this.state.note)
    if (this.state.loading == true) {
      return (
        <View style={{ flex: 1 }}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      )
    }
    return (
      <View style={styles.listContainer}>
        <View>
          <View style={{ alignItems: "center" }}>
            <TouchableOpacity
              
              style={[
                styles.image,
                this.state.noteimage != undefined &&
                  this.state.noteimage.length > 0 && {
                    backgroundColor: "#6dcc64"
                  }
              ]}
              onPress={() => {
                this.onCameraroll("noteimage", this.state.noteimage, "edit")
              }}
            >
              <Icon name="image" type="entypo" color="#FFF" />
              <Text style={styles.imageText}>
                {I18n.t("mainpile.3_2.view")}
              </Text>
            </TouchableOpacity>
          </View>
          <ScrollView style={{ borderWidth: 0.5, margin: 10, height: "70%" }}>
            <TextInput
              // autoFocus
              editable={this.props.viewType==true ? false:true}
              multiline
              numberOfLines={20}
              value={this.state.note}
              onChangeText={text => this.updateValue(text)}
              style={{ textAlignVertical: "top", fontSize: 25, height: "100%" }}
            />
          </ScrollView>
        </View>

        <View style={[styles.bottom, { bottom: this.state.bottom }]}>
          <View style={styles.second}>
            <TouchableOpacity
              style={styles.firstButton}
              onPress={() => this.closeButton()}
            >
              <Text style={styles.selectButton}>
                {I18n.t("mainpile.steeldetail.button.close")}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
            disabled={this.props.viewType==true && true}
              style={[
                styles.secondButton,
                this.props.category == 3|| this.props.category == 5
                  ? { backgroundColor: DEFAULT_COLOR_4 }
                  : this.props.shape == 2
                    ? { backgroundColor: DEFAULT_COLOR_1 }
                    : {},this.props.viewType==true &&{ backgroundColor: "#BABABA" }
              ]}
              onPress={() => this.saveButton()}
            >
              <Text style={styles.selectButton}>
                {I18n.t("mainpile.steeldetail.button.save")}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = {
  listContainer: {
    flex: 1,
    backgroundColor: "#fff"
  },
  buttonFixed: {
    flexDirection: "column",
    flex: 1,
    position: "absolute",
    bottom: 0
  },
  bottom: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0
  },
  second: {
    height: 50,
    backgroundColor: MAIN_COLOR,
    flexDirection: "row"
  },
  firstButton: {
    width: "40%",
    backgroundColor: "#BABABA",
    justifyContent: "center",
    alignItems: "center"
  },
  secondButton: {
    width: "60%",
    justifyContent: "center",
    alignItems: "center"
  },
  selectButton: {
    color: "#FFF",
    fontSize: 20
  },
  image: {
    backgroundColor: SUB_COLOR,
    height: 40,
    width: "50%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10,
    marginTop: 10
  },
  imageText: {
    marginLeft: 10,
    color: "white"
  }
}

const mapStateToProps = state => {
  // console.log("mapStateToProps",state.mainpile.pileAll )
  return {
    note: state.note.note,
    error: state.note.error,
    pileAll: state.mainpile.pileAll
  }
}
export default connect(mapStateToProps, actions)(Note)
