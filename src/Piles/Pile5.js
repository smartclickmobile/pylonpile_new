import React, { Component } from "react"
import { View, Text, StyleSheet, TouchableOpacity,Alert,ActivityIndicator } from "react-native"
import StepIndicator from "react-native-step-indicator"
import { Container, Content } from "native-base"
import { MAIN_COLOR,MENU_GREY_ITEM,DEFAULT_COLOR_1, DEFAULT_COLOR_4 } from "../Constants/Color"
import Pile5_1 from "./Pile5_1"
import Pile5_2 from "./Pile5_2"
import Pile5_3 from "./Pile5_3"
import Pile5_4 from "./Pile5_4"
import Pile5_5 from "./Pile5_5"
import styles from './styles/Pile5.style'
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux'
import * as actions from '../Actions'
import I18n from '../../assets/languages/i18n'
import moment from 'moment'

class Pile5 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 5,
      step: 0,
      ref:4,
      error:null,
      data:[],
      location:{
        position:{
          lat:1,
          log:1,
        }
      },
      Edit_Flag:1,
      dataDepth:null,
      submit:false,
      status6:0,
      status5:0,
      status7:0,
      status1:0,
      checksavebutton:false,
      checkto7_super_random:'',
      checkto6_super_random:'',
      process5_success_1_random:null,
      process5_success_4_random:null,
      statuscolor:null,
      saveload:false,
      
      process5_deleterow:null,
      drillinglist_random:null,

      stepstatus:null,
      flag_to4: false,
      flag_to5: false
    }
  }
  componentDidMount(){
    this.props.getDrilling({
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      Language: I18n.locale
    })
    this.props.getDrillingChild({
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      Language: I18n.locale
    })
    if (this.props.stack) {
      // console.log('1234',this.props.stack[this.props.stack.length - 1].step);
      this.setState({step:this.props.stack[this.props.stack.length - 1].step - 1})
    }
  }
async componentWillReceiveProps(nextProps) {
  
  if(nextProps.step_status2_random != this.state.stepstatus && nextProps.processstatus == 5){
    this.setState({stepstatus:nextProps.step_status2_random},async()=>{
      console.warn('check state',this.props.shape,this.props.category,this.state.step,nextProps.step_status2.Step5Status)
      await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step5Status:nextProps.step_status2.Step5Status})
      this.props.mainPileGetStorage(this.props.pileid,'step_status')
      if(this.props.shape == 1){
        if(this.props.category == 5){
          if(this.state.step == 2){
            if(nextProps.step_status2.Step5Status == 2){
              var valuecheck ={
                Language:I18n.locale,
                JobId: this.props.jobid,
                PileId: this.props.pileid,
                lockstep:false
              }
              this.props.pileCheckStep06super(valuecheck)
            }else{
              Alert.alert("",I18n.t('mainpile.3_3.error_checkall'))
            }
          }
        }else if(this.props.category == 4){
          if(this.state.step == 2){
            if(nextProps.step_status2.Step5Status == 2){
              if(this.state.status1 == 2){
                this.onSetStack(6,0)
                this.props.onNextStep()
              }else{
                Alert.alert("",I18n.t('mainpile.lockprocess.process7_d'))
              }
              
            }else{
              Alert.alert("",I18n.t('mainpile.3_3.error_checkall'))
            }
          }
        }else{
          if(this.state.step == 2){
            if(nextProps.step_status2.Step5Status == 2){
                this.onSetStack(6,0)
                this.props.onNextStep()
            }else{
              Alert.alert("",I18n.t('mainpile.3_3.error_checkall'))
            }
          }
        }
        
      }else{
        if(this.state.step == 3 && this.props.category != 5){
          if(nextProps.step_status2.Step5Status == 2){
            if(this.props.category == 3 || this.props.category == 5){
              var valuecheck ={
                Language:I18n.locale,
                JobId: this.props.jobid,
                PileId: this.props.pileid,
                lockstep:false,
                process:5
              }
              this.props.pileCheckStep07super(valuecheck)
            }else{
              if(this.state.status1==2){
                this.onSetStack(7,0)
                this.props.onNextStep()
              }else{
                Alert.alert('', I18n.t('mainpile.lockprocess.process7_dw1'), [
                  {
                    text: 'OK'
                  }
                ])
              }
            }
          }else{
            Alert.alert("",I18n.t('mainpile.3_3.error_checkall'))
          }
      }else if(this.state.step == 3 && this.props.category == 5 && this.props.shape == 1){
        if(nextProps.step_status2.Step5Status == 2){
          var valuecheck ={
            Language:I18n.locale,
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            lockstep:false,
            process:5
          }
          this.props.pileCheckStep07super(valuecheck)
        }
      }else if(this.state.step == 4 && this.props.category == 5 && this.props.shape == 2){
        if(nextProps.step_status2.Step5Status == 2){
          var valuecheck ={
            Language:I18n.locale,
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            lockstep:false,
            process:5
          }
          this.props.pileCheckStep07super(valuecheck)
        }
      }
      }
    })
  }

  if(nextProps.drillinglist_random != this.state.drillinglist_random){
    this.setState({drillinglist_random:nextProps.drillinglist_random},()=>{
      if(nextProps.process5_deleterow != this.state.process5_deleterow){
        this.setState({process5_deleterow: nextProps.process5_deleterow},async()=>{
          var validate = true
            for (var i = 0; i <  this.props.drillinglist.length; i++) {
              if (this.props.drillinglist[i + 1]) {
                if (parseFloat(this.props.drillinglist[i].Depth) > parseFloat(this.props.drillinglist[i + 1].Depth)) {
                  validate = false
                }
              }else {
                if (validate == true) {
                  if (this.props.drillinglist[i - 1]) {
                    if (parseFloat(this.props.drillinglist[i].Depth) < parseFloat(this.props.drillinglist[i - 1].Depth)) {
                      validate = false
                    }
                  }
                }
              }
            }
            // console.warn('validate step51',this.props.drillinglist.length)
            if(this.props.drillinglist.length == 0){
              validate = false
            }else{
              var validate_depth = parseFloat(this.state.dataDepth.depth) - parseFloat(this.state.data['5_2'].masterInfo.PileInfo.Constant)
              if (parseFloat(this.props.drillinglist[this.props.drillinglist.length - 1].Depth) >= parseFloat(validate_depth) && validate == true) {
                validate = true
              }else {
                validate = false
              }
            }
    
            // console.warn('validate',validate)
            if(validate == false){
              await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step5Status:1})
              this.props.mainPileGetStorage(this.props.pileid,'step_status')
            }
        })
    
      }
    })
  }
  
  if (nextProps.pileAll[this.props.pileid] != undefined) {
    var edit = nextProps.pileAll[this.props.pileid]
    if (edit['5_0'] != undefined) {
      await this.setState({Edit_Flag: edit['5_0'].data.Edit_Flag})
    }

    if(edit != undefined){
      this.setState({status6:edit.step_status.Step6Status,status5:edit.step_status.Step5Status,status7:edit.step_status.Step7Status,status1:edit.step_status.Step1Status})

    }
  }
  console.log('nextprops setp5',edit['5_0'])
  await this.setState({
    error: nextProps.error,
    data: nextProps.pileAll[nextProps.pileid],
    location: nextProps.pileAll.currentLocation,
  })
  if (nextProps.hidden == false && this.state.saveload == false) {
    // console.warn('dataDepth',this.state.step,this.props.parent,this.refs['pile5_5'].getWrappedInstance().onGetDepth())
    if(this.state.step == 3 && this.props.parent == true){
      await this.setState({dataDepth: this.refs['pile5_5'].getWrappedInstance().onGetDepth()})
    }else{
      await this.setState({dataDepth: this.refs['pile5_3'].getWrappedInstance().onGetDepth()})
    }
  }
  // console.warn('ffff',nextProps.lockstepcheckto7_super)
  if(nextProps.checkto7_super == true && nextProps.checkto7_super_random != this.state.checkto7_super_random && (this.props.category == 3||this.props.category == 5) && nextProps.lockstepcheckto7_super == false && nextProps.checkto7_super_process == 5){
    this.setState({checkto7_super_random: nextProps.checkto7_super_random},()=>{
      // console.warn('ffff')
      console.warn('checkto7_super process5')
      this.onSetStack(7,0)
      this.props.onNextStep()
      setTimeout(()=>{this.setState({checksavebutton:false})},2000)
    })
  }
  if(nextProps.checkto6_super == true && nextProps.checkto6_super_random != this.state.checkto6_super_random && (this.props.category == 5||this.props.category == 5) && nextProps.lockstepcheckto6_super == false){
    this.setState({checkto6_super_random: nextProps.checkto6_super_random},()=>{
      // console.warn('ffff')
      this.onSetStack(6,0)
      this.props.onNextStep()
      setTimeout(()=>{this.setState({checksavebutton:false})},2000)
    })
  }
  if(nextProps.error != null){
    this.setState({checksavebutton:false,saveload:false})
  }
  if(nextProps.process5_success_1 == true && nextProps.process5_success_1_random != this.state.process5_success_1_random){
    this.setState({process5_success_1_random:nextProps.process5_success_1_random},async()=>{
      if(this.state.status5 == 2){
        // await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step5Status:2})
        // this.props.mainPileGetStorage(this.props.pileid,'step_status')
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process:5
        })
        if(this.state.flag_to4){
          this.onSetStack(5,3)
          this.setState({step: 3,saveload:false, flag_to4:false})
          // this.onSetStack(5,2)
          // this.setState({step: 2,saveload:false, flag_to4:false})
        }else if(this.state.flag_to5){
          this.onSetStack(5,4)
          this.setState({step: 4,saveload:false, flag_to5:false})
          // this.onSetStack(5,3)
          // this.setState({step: 3,saveload:false, flag_to4:false})
        }else{
          this.onSetStack(5,1)
          this.setState({step: 1,saveload:false})
        }
        
      }else{

        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process:5
        })
        if(this.state.flag_to4){
          this.onSetStack(5,3)
          this.setState({step: 3,saveload:false, flag_to4:false})
          // this.onSetStack(5,2)
          // this.setState({step: 2,saveload:false, flag_to4:false})
        }else if(this.state.flag_to5){
          this.onSetStack(5,4)
          this.setState({step: 4,saveload:false, flag_to5:false})
          // this.onSetStack(5,3)
          // this.setState({step: 3,saveload:false, flag_to4:false})
        }else{
          this.onSetStack(5,1)
          this.setState({step: 1,saveload:false})
        }
      }
    })

  }

  if(this.props.category == 2 && nextProps.process5_success_4 == true && nextProps.process5_success_4_random != this.state.process5_success_4_random){
    this.setState({process5_success_4_random:nextProps.process5_success_4_random},async()=>{
      this.props.getStepStatus2({
        jobid: this.props.jobid,
        pileid: this.props.pileid,
        process:5
      })
     
      this.setState({checksavebutton:false,saveload:false})
    })
  }
  if((this.props.category == 3 || this.props.category == 5) && nextProps.process5_success_4 == true && nextProps.process5_success_4_random != this.state.process5_success_4_random ){
    this.setState({process5_success_4_random:nextProps.process5_success_4_random},async()=>{
      this.props.getStepStatus2({
        jobid: this.props.jobid,
        pileid: this.props.pileid,
        process:5
      })
      this.setState({checksavebutton:false,saveload:false})
    })
  }
 
}

  initialStep(){
    this.setState({step:0})
  }

  updateState(value){
    if (value.step != null) {
      const ref = 'pile'+ value.process +'_'+ value.step
      if (this.refs[ref].getWrappedInstance().updateState(value)) {
          this.refs[ref].getWrappedInstance().updateState(value)
      }
    }else{
      console.log('step5',value)
    }
  }

  nextButton = async () => {
    switch (await this.state.step) {
      case 0:
        this.onSave(this.state.step)
        break
      case 1:
        this.onSave(this.state.step)
        break
      case 2:
        this.onSave(this.state.step)
        break
      case 3:
        this.onSave(this.state.step)
        break
      case 4:
        this.onSave(this.state.step)
        break
      default:
        break
    }
  }

  // async onSaveOld(step){
  //  console.warn('onSave step',step)
  //   await this.props.mainPileGetAllStorage()
  //   if (step == 0) {
  //     var value = {
  //       process:5,
  //       step:1,
  //       pile_id: this.props.pileid,
  //       shape: this.props.shape,
  //       drillinglist:this.props.drillinglist,
  //       category: this.props.category,
  //       parent:this.props.parent
  //     }
      
  //     if (this.state.Edit_Flag == 1) {

  //       if(this.props.category==5){
  //           console.log('test step == 0',step)
  //           this.onSetStack(5,1)
  //           this.setState({step: 1})
  //         return
  //       }
  //       this.props.mainpileAction_checkStepStatus(value)
  //         .then(async (data) =>{
  //           if ( this.state.error == null) {
  //             if (data.check == false) {
  //               Alert.alert('', data.errorText[0], [
  //                 {
  //                   text: 'OK'
  //                 }
  //               ])
  //             }else {
  //               var valueApi ={
  //                 Language:I18n.locale,
  //                 JobId: this.props.jobid,
  //                 PileId: this.props.pileid,
  //                 Page:1,
  //                 latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
  //                 longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
  //                 CheckPlummet:this.state.data['5_1'].data.CheckPlummet,
  //                 CheckWaterLevel:this.state.data['5_1'].data.CheckWaterLevel,
  //                 // ImageLengthSteelCarry: this.state.data['5_1'].data.ImageLengthSteelCarry,
  //                 ImageCheckPlummet: this.state.data['5_1'].data.ImageCheckPlummet,
  //                 ImageCheckWaterLevel: this.state.data['5_1'].data.ImageCheckWaterLevel,
  //                 // OverLifting: this.state.data["5_1"].data.OverLifting,
  //                 // Pcoring: this.state.data["5_1"].data.Pcoring,
  //                 // hanginglength: this.state.data["5_1"].data.HangingBarsLength
  //               }
  //               this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor})
  //               this.props.pileSaveStep05(valueApi)
               
  //             }} else {
  //             Alert.alert(this.state.error)
  //             this.setState({checksavebutton:false,saveload:false})
  //           }
  //         })
  //     }else {
  //       this.onSetStack(5,1)
  //       this.setState({step: 1})
  //     }


  //   }else if (step == 1) {
  //     if (this.state.Edit_Flag == 1) {
  //       var value = {
  //         process:5,
  //         step:2,
  //         pile_id: this.props.pileid,
  //         shape: this.props.shape,
  //         drillinglist:this.props.drillinglist,
  //         category: this.props.category,
  //         parent:this.props.parent
  //       }
  //       if(this.props.category==5){
  //         var value = {
  //           process:5,
  //           step:2,
  //           pile_id: this.props.pileid,
  //           shape: this.props.shape,
  //           drillinglist:this.props.drillinglist,
  //           category: this.props.category,
  //           parent:this.props.parent
  //           }
           
  //           if (this.state.Edit_Flag == 1) {
  //             var validate = true
  //             for (var i = 0; i <  this.props.drillinglist.length; i++) {
  //               if (this.props.drillinglist[i + 1]) {
  //                 if (parseFloat(this.props.drillinglist[i].Depth) > parseFloat(this.props.drillinglist[i + 1].Depth)) {
  //                   validate = false
  //                 }
  //               }else {
  //                 if (validate == true) {
  //                   if (this.props.drillinglist[i - 1]) {
  //                     if (parseFloat(this.props.drillinglist[i].Depth) < parseFloat(this.props.drillinglist[i - 1].Depth)) {
  //                       validate = false
  //                     }
  //                   }
  //                 }
  //               }
  //             }
  //             // console.warn('validate step51',this.props.drillinglist.length)
  //             if(this.props.drillinglist.length == 0){
  //               validate = false
  //             }else{
  //               var validate_depth = parseFloat(this.state.dataDepth.depth) - parseFloat(this.state.data['5_2'].masterInfo.PileInfo.Constant)
  //               if (parseFloat(this.props.drillinglist[this.props.drillinglist.length - 1].Depth) >= parseFloat(validate_depth) && validate == true) {
  //                 validate = true
  //               }else {
  //                 validate = false
  //               }
  //             }
      
  //             var check_validate = false
  //             if(this.props.category == 5){
  //               if(this.state.data['5_3'].data.CheckPlummet == true){
  //                 if(this.state.data['5_3'].data.CheckWaterLevel == true){
  //                   check_validate = true
  //                 }else{
  //                   Alert.alert('',I18n.t('alert.checkwater'))
  //                 }
  //               }else{
  //                 Alert.alert('',I18n.t('alert.checkplummet'))
  //               }
  //             }else{
  //               check_validate = true
  //             }
  //             if (validate == true && check_validate == true) {
  //               if(this.props.category == 5){
  //                 console.log('test this.props.category step 1')
  //                 var valueApi ={
  //                   Language:I18n.locale,
  //                   JobId: this.props.jobid,
  //                   PileId: this.props.pileid,
  //                   Page:1,
  //                   latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
  //                   longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
  //                   CheckPlummet:this.state.data['5_3'].data.CheckPlummet,
  //                   CheckWaterLevel:this.state.data['5_3'].data.CheckWaterLevel,
  //                   ImageCheckPlummet: this.state.data['5_3'].data.ImageCheckPlummet,
  //                   ImageCheckWaterLevel: this.state.data['5_3'].data.ImageCheckWaterLevel,
  //                 }
  //                 console.log('pile5_1',valueApi)
  //                 this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor,flag_to4:true})
  //                 this.props.pileSaveStep05(valueApi)
  //               }else{
  //                 Alert.alert(
  //                   '',
  //                   this.props.shape == 1 ? I18n.t('mainpile.6_2.changebucket') : I18n.t('mainpile.5_3.depthcom'),
  //                   [
  //                     {
  //                       text: 'Cancel', onPress: async () =>{this.setState({checksavebutton:false})}
  //                     },
  //                     {
  //                       text: 'OK', onPress: async () =>
  //                       {
  //                         if (this.state.Edit_Flag == 1) {
        
  //                             this.props.mainpileAction_checkStepStatus(value)
  //                               .then(async (data) =>
  //                               {
                                 
        
  //                                 if ( this.state.error == null) {
  //                                   if (data.check == false) {
  //                                     Alert.alert('', data.errorText[0], [
  //                                       {
  //                                         text: 'OK'
  //                                       }
  //                                     ])
  //                                     this.setState({checksavebutton:false,saveload:false})
  //                                   }else {
  //                                     if (data.size >= 1) {
  //                                       if (this.props.shape == 1) {
  //                                         var data5_0 = this.state.data['5_0']
  //                                         data5_0.data.Edit_Flag = 1
  //                                         await this.props.mainPileSetStorage(this.props.pileid,'5_0',data5_0)
                                          
  //                                         this.props.getStepStatus2({
  //                                           jobid: this.props.jobid,
  //                                           pileid: this.props.pileid,
  //                                           process:5
  //                                         })
                                          
  //                                         setTimeout(()=>{this.setState({checksavebutton:false})},2000)
  //                                       }else {
                                          
  //                                         this.props.getStepStatus2({
  //                                           jobid: this.props.jobid,
  //                                           pileid: this.props.pileid,
  //                                           process:5
  //                                         })
  //                                         console.log('test step = 13',step)
  //                                         setTimeout(()=>{
  //                                           this.onSetStack(5,2)
  //                                           this.setState({step: 2})
  //                                         },2000)
                                          
                                          
  //                                       }
  //                                     }else {
  //                                       Alert.alert('', I18n.t('mainpile.5_1.error_nextstep'), [
  //                                         {
  //                                           text: 'OK'
  //                                         }
  //                                       ])
  //                                       this.setState({checksavebutton:false})
  //                                     }
        
  //                                   }
  //                                 } else {
  //                                   Alert.alert(this.state.error)
  //                                   this.setState({checksavebutton:false,saveload:false})
  //                                 }
  //                               })
  //                           // }
  //                         }else {
  //                           if (this.props.shape == 1) {
  //                             this.onSetStack(6,0)
  //                             this.props.onNextStep()
  //                             setTimeout(()=>{this.setState({checksavebutton:false})},2000)
  //                           }else {
  //                             console.log('test step = 14',step)
  //                             this.onSetStack(5,2)
  //                             this.setState({step: 2})
  //                           }
  //                         }
  //                       }
  //                     }
  //                   ],
  //                   { cancelable: false }
  //                 )
  //               }
                
  //             }else {
  //               await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step5Status:1})
  //               this.props.mainPileGetStorage(this.props.pileid,'step_status')
  //               if(check_validate == true){
  //                 Alert.alert(I18n.t('mainpile.5_3.depthcolnotco'))
  //               }
  //               this.setState({checksavebutton:false})
  //             }
  //           }

  //         return
  //       }

  //       this.props.mainpileAction_checkStepStatus(value)
  //         .then(async (data) =>
  //         {

  //           if ( this.state.error == null) {
  //             if (data.check == false) {
  //               Alert.alert('', data.errorText[0], [
  //                 {
  //                   text: 'OK'
  //                 }
  //               ])
  //             }else {
              
  //                 this.setState({step: 2})
  //                 this.onSetStack(5,2)
               
  //             }
  //           } else {
  //             Alert.alert(this.state.error)
  //             this.setState({checksavebutton:false,saveload:false})
  //           }
  //         })
  //     }else {
  //       console.log('test step = 12',step)
  //       this.onSetStack(5,2)
  //       this.setState({step: 2})
  //     }


  //   }else if (step == 2 && this.props.category != 5) {
  //     if(this.props.shape == 1){
  //       this.setState({checksavebutton:true})
  //     }
  //     var value = {
  //     process:5,
  //     step:3,
  //     pile_id: this.props.pileid,
  //     shape: this.props.shape,
  //     drillinglist:this.props.drillinglist,
  //     category: this.props.category,
  //     parent:this.props.parent
  //     }
     
  //     if (this.state.Edit_Flag == 1) {
  //       var validate = true
  //       for (var i = 0; i <  this.props.drillinglist.length; i++) {
  //         if (this.props.drillinglist[i + 1]) {
  //           if (parseFloat(this.props.drillinglist[i].Depth) > parseFloat(this.props.drillinglist[i + 1].Depth)) {
  //             validate = false
  //           }
  //         }else {
  //           if (validate == true) {
  //             if (this.props.drillinglist[i - 1]) {
  //               if (parseFloat(this.props.drillinglist[i].Depth) < parseFloat(this.props.drillinglist[i - 1].Depth)) {
  //                 validate = false
  //               }
  //             }
  //           }
  //         }
  //       }
  //       if(this.props.drillinglist.length == 0){
  //         validate = false
  //       }else{
  //         var validate_depth = parseFloat(this.state.dataDepth.depth) - parseFloat(this.state.data['5_2'].masterInfo.PileInfo.Constant)
  //         if (parseFloat(this.props.drillinglist[this.props.drillinglist.length - 1].Depth) >= parseFloat(validate_depth) && validate == true) {
  //           validate = true
  //         }else {
  //           validate = false
  //         }
  //       }

  //       var check_validate = false
  //       if(this.props.category == 5){
  //         if(this.state.data['5_3'].data.CheckPlummet == true){
  //           if(this.state.data['5_3'].data.CheckWaterLevel == true){
  //             check_validate = true
  //           }else{
  //             Alert.alert('',I18n.t('alert.checkwater'))
  //           }
  //         }else{
  //           Alert.alert('',I18n.t('alert.checkplummet'))
  //         }
  //       }else{
  //         check_validate = true
  //       }

  //       if (validate == true && check_validate == true) {
  //         if(this.props.category == 4){
  //           if (this.state.Edit_Flag == 1) {
  //             this.props.mainpileAction_checkStepStatus(value)
  //               .then(async (data) =>
  //               {
                 

  //                 if ( this.state.error == null) {
  //                   if (data.check == false) {
  //                     Alert.alert('', data.errorText[0], [
  //                       {
  //                         text: 'OK'
  //                       }
  //                     ])
  //                     this.setState({checksavebutton:false,saveload:false})
  //                   }else {
  //                     if (data.size >= 1) {
  //                       if (this.props.shape == 1) {
  //                         var data5_0 = this.state.data['5_0']
  //                         data5_0.data.Edit_Flag = 1
  //                         await this.props.mainPileSetStorage(this.props.pileid,'5_0',data5_0)
                          
  //                         this.props.getStepStatus2({
  //                           jobid: this.props.jobid,
  //                           pileid: this.props.pileid,
  //                           process:5
  //                         })
                          
  //                         setTimeout(()=>{this.setState({checksavebutton:false})},2000)
  //                       }else {
                          
  //                         this.props.getStepStatus2({
  //                           jobid: this.props.jobid,
  //                           pileid: this.props.pileid,
  //                           process:5
  //                         })
  //                         setTimeout(()=>{
  //                           this.onSetStack(5,3)
  //                           this.setState({step: 3})
  //                         },2000)
                          
                          
  //                       }
  //                     }else {
  //                       Alert.alert('', I18n.t('mainpile.5_1.error_nextstep'), [
  //                         {
  //                           text: 'OK'
  //                         }
  //                       ])
  //                       this.setState({checksavebutton:false})
  //                     }

  //                   }
  //                 } else {
  //                   Alert.alert(this.state.error)
  //                   this.setState({checksavebutton:false,saveload:false})
  //                 }
  //               })
  //           // }
  //         }else {
  //           if (this.props.shape == 1) {
  //             this.onSetStack(6,0)
  //             this.props.onNextStep()
  //             setTimeout(()=>{this.setState({checksavebutton:false})},2000)
  //           }else {
  //             this.onSetStack(5,3)
  //             this.setState({step: 3})
  //           }
  //         }
  //         }else if(this.props.category == 5){
  //           var valueApi ={
  //             Language:I18n.locale,
  //             JobId: this.props.jobid,
  //             PileId: this.props.pileid,
  //             Page:1,
  //             latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
  //             longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
  //             CheckPlummet:this.state.data['5_3'].data.CheckPlummet,
  //             CheckWaterLevel:this.state.data['5_3'].data.CheckWaterLevel,
  //             ImageCheckPlummet: this.state.data['5_3'].data.ImageCheckPlummet,
  //             ImageCheckWaterLevel: this.state.data['5_3'].data.ImageCheckWaterLevel,
          
  //           }
  //           console.log('pile5_1',valueApi)
  //           this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor,flag_to4:true})
  //           this.props.pileSaveStep05(valueApi)
  //         }else{
  //           Alert.alert(
  //             '',
  //             this.props.shape == 1 ? I18n.t('mainpile.6_2.changebucket') : I18n.t('mainpile.5_3.depthcom'),
  //             [
  //               {
  //                 text: 'Cancel', onPress: async () =>{this.setState({checksavebutton:false})}
  //               },
  //               {
  //                 text: 'OK', onPress: async () =>
  //                 {
  //                   if (this.state.Edit_Flag == 1) {
  
  //                       this.props.mainpileAction_checkStepStatus(value)
  //                         .then(async (data) =>
  //                         {
                           
  
  //                           if ( this.state.error == null) {
  //                             if (data.check == false) {
  //                               Alert.alert('', data.errorText[0], [
  //                                 {
  //                                   text: 'OK'
  //                                 }
  //                               ])
  //                               this.setState({checksavebutton:false,saveload:false})
  //                             }else {
  //                               if (data.size >= 1) {
  //                                 if (this.props.shape == 1) {
  //                                   var data5_0 = this.state.data['5_0']
  //                                   data5_0.data.Edit_Flag = 1
  //                                   await this.props.mainPileSetStorage(this.props.pileid,'5_0',data5_0)
                                    
  //                                   this.props.getStepStatus2({
  //                                     jobid: this.props.jobid,
  //                                     pileid: this.props.pileid,
  //                                     process:5
  //                                   })
                                    
  //                                   setTimeout(()=>{this.setState({checksavebutton:false})},2000)
  //                                 }else {
                                    
  //                                   this.props.getStepStatus2({
  //                                     jobid: this.props.jobid,
  //                                     pileid: this.props.pileid,
  //                                     process:5
  //                                   })
  //                                   setTimeout(()=>{
  //                                     this.onSetStack(5,3)
  //                                     this.setState({step: 3})
  //                                   },2000)
                                    
                                    
  //                                 }
  //                               }else {
  //                                 Alert.alert('', I18n.t('mainpile.5_1.error_nextstep'), [
  //                                   {
  //                                     text: 'OK'
  //                                   }
  //                                 ])
  //                                 this.setState({checksavebutton:false})
  //                               }
  
  //                             }
  //                           } else {
  //                             Alert.alert(this.state.error)
  //                             this.setState({checksavebutton:false,saveload:false})
  //                           }
  //                         })
  //                     // }
  //                   }else {
  //                     if (this.props.shape == 1) {
  //                       this.onSetStack(6,0)
  //                       this.props.onNextStep()
  //                       setTimeout(()=>{this.setState({checksavebutton:false})},2000)
  //                     }else {
  //                       this.onSetStack(5,3)
  //                       this.setState({step: 3})
  //                     }
  //                   }
  //                 }
  //               }
  //             ],
  //             { cancelable: false }
  //           )
  //         }
          
  //       }else {
  //         await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step5Status:1})
  //         this.props.mainPileGetStorage(this.props.pileid,'step_status')
  //         if(check_validate == true){
  //           Alert.alert(I18n.t('mainpile.5_3.depthcolnotco'))

  //         }
  //         this.setState({checksavebutton:false})
  //       }
  //     }else {
  //       if (this.props.shape == 1) {
  //         this.onSetStack(6,0)
  //         this.props.onNextStep()
  //         setTimeout(()=>{this.setState({checksavebutton:false})},2000)
  //       }else {
  //         this.onSetStack(5,3)
  //         this.setState({step: 3})
  //       }
  //     }
  //   }else if (step == 3 && this.props.category != 5){
      
  //     this.setState({checksavebutton:true})
  //     if (this.state.Edit_Flag == 1) {
  //       var value = {
  //         process:5,
  //         step:4,
  //         pile_id: this.props.pileid,
  //         shape: this.props.shape,
  //         drillinglist:this.props.drillinglist,
  //         category: this.props.category,
  //         parent:this.props.parent
  //       }

  //       this.props.mainpileAction_checkStepStatus(value)
  //         .then(async (data) =>
  //         {
  //           var start1 = moment(this.state.data['5_4'].data.StartDrillingFluidDate,'DD/MM/YYYY HH:mm')
  //           var end1 = moment(this.state.data['5_4'].data.EndDrillingFluidDate,'DD/MM/YYYY HH:mm')
  //           if(start1>=end1){
  //             Alert.alert('', I18n.t('mainpile.liquidtestinsert.alert.error4'), [
  //               {
  //                 text: 'OK'
  //               }
  //             ])
  //             this.setState({checksavebutton:false})
  //           }else{
              
  //             if(this.state.data['5_4'].data.CheckInstallStopEnd == true){
  //               var start = moment(this.state.data['5_4'].data.StartStopEndDate,'DD/MM/YYYY HH:mm')
  //               var end = moment(this.state.data['5_4'].data.EndStopEndDate,'DD/MM/YYYY HH:mm')
  //               if(start>=end){
  //                 Alert.alert('', I18n.t('mainpile.liquidtestinsert.alert.error4'), [
  //                   {
  //                     text: 'OK'
  //                   }
  //                 ])
  //                 this.setState({checksavebutton:false})
  //               }else{
                  
  //               var valueApi = {
  //                 Language:I18n.locale,
  //                 JobId: this.props.jobid,
  //                 PileId: this.props.pileid,
  //                 Page:5,
  //                 latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
  //                 longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
  //                 StartDrillingFluidDate:moment(this.state.data['5_4'].data.StartDrillingFluidDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
  //                 EndDrillingFluidDate:moment(this.state.data['5_4'].data.EndDrillingFluidDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
  //                 StartStopEndDate:moment(this.state.data['5_4'].data.StartStopEndDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
  //                 EndStopEndDate:moment(this.state.data['5_4'].data.EndStopEndDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),  
  //                 CheckStopEnd:this.state.data['5_4'].data.CheckStopEnd,
  //                 CheckWaterStop:this.state.data['5_4'].data.CheckWaterStop,
  //                 CheckInstallStopEnd:this.state.data['5_4'].data.CheckInstallStopEnd,
  //                 CheckDrillingFluid:this.state.data['5_4'].data.CheckDrillingFluid,
  //                 ImageCheckStopEnd:this.state.data['5_4'].data.ImageCheckStopEnd,
  //                 ImageCheckWaterStop:this.state.data['5_4'].data.ImageCheckWaterStop,
  //                 ImageCheckInstallStopEnd:this.state.data['5_4'].data.ImageCheckInstallStopEnd,
  //                 ImageCheckDrillingFluid:this.state.data['5_4'].data.ImageCheckDrillingFluid,
  //                 WaterStopTypeId:this.state.data['5_4'].data.waterstopid,
  //                 WaterStopLength:this.state.data['5_4'].data.WaterStopLength,
  //                 WaterStopTypeName: this.state.data['5_4'].data.WaterStopTypeName,
  //                 category:this.props.category
  //               }
  //               console.log('pile5_4',valueApi)
                
    
  //               if ( this.state.error == null) {
  //                 if (data.check == false) {
  //                   Alert.alert('', data.errorText[0], [
  //                     {
  //                       text: 'OK'
  //                     }
  //                   ])
  //                   this.setState({checksavebutton:false,saveload:false})
  //                 }else {
  //                   if (data.size >= 1) {
  //                     // this.props.pileSaveStep05(valueApi)
  //                     var data5_0 = this.state.data['5_0']
  //                     data5_0.data.Edit_Flag = 1
  //                     await this.props.mainPileSetStorage(this.props.pileid,'5_0',data5_0)
    
  //                     var value = {
  //                       process: 7,
  //                       step: 0,
  //                       pile_id: this.props.pileid,
  //                       shape: this.props.shape
  //                     }
                     
  //                       // console.warn('save5 category',this.props.category)
  //                         if(this.props.category == 3){
                            
                           
  //                           this.props.pileSaveStep05(valueApi)
                           
                          
  //                         }else{
  //                           this.props.pileSaveStep05(valueApi)
  //                           this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor})
                           
  //                         }
                         
                     
    
  //                   }
  //                 }
  //               } else {
  //                 Alert.alert(this.state.error)
  //                 this.setState({checksavebutton:false,saveload:false})
  //               }
  //               }
  //             }else{
               
  //               var valueApi = {
  //                 Language:I18n.locale,
  //                 JobId: this.props.jobid,
  //                 PileId: this.props.pileid,
  //                 Page:5,
  //                 latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
  //                 longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
  //                 StartDrillingFluidDate:moment(this.state.data['5_4'].data.StartDrillingFluidDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
  //                 EndDrillingFluidDate:moment(this.state.data['5_4'].data.EndDrillingFluidDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
  //                 StartStopEndDate:moment(this.state.data['5_4'].data.StartStopEndDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
  //                 EndStopEndDate:moment(this.state.data['5_4'].data.EndStopEndDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),  
  //                 CheckStopEnd:this.state.data['5_4'].data.CheckStopEnd,
  //                 CheckWaterStop:this.state.data['5_4'].data.CheckWaterStop,
  //                 CheckInstallStopEnd:this.state.data['5_4'].data.CheckInstallStopEnd,
  //                 CheckDrillingFluid:this.state.data['5_4'].data.CheckDrillingFluid,
  //                 ImageCheckStopEnd:this.state.data['5_4'].data.ImageCheckStopEnd,
  //                 ImageCheckWaterStop:this.state.data['5_4'].data.ImageCheckWaterStop,
  //                 ImageCheckInstallStopEnd:this.state.data['5_4'].data.ImageCheckInstallStopEnd,
  //                 ImageCheckDrillingFluid:this.state.data['5_4'].data.ImageCheckDrillingFluid,
  //                 WaterStopTypeId:this.state.data['5_4'].data.waterstopid,
  //                 WaterStopLength:this.state.data['5_4'].data.WaterStopLength,
  //                 WaterStopTypeName: this.state.data['5_4'].data.WaterStopTypeName,
  //                 category:this.props.category
  //               }
  //               console.log('pile5_4',valueApi)
                
    
  //               if ( this.state.error == null) {
  //                 if (data.check == false) {
  //                   Alert.alert('', data.errorText[0], [
  //                     {
  //                       text: 'OK'
  //                     }
  //                   ])
  //                   this.setState({checksavebutton:false})
  //                 }else {
  //                   if (data.size >= 1) {
  //                     // this.props.pileSaveStep05(valueApi)
  //                     var data5_0 = this.state.data['5_0']
  //                     data5_0.data.Edit_Flag = 1
  //                     await this.props.mainPileSetStorage(this.props.pileid,'5_0',data5_0)
    
  //                     var value = {
  //                       process: 7,
  //                       step: 0,
  //                       pile_id: this.props.pileid,
  //                       shape: this.props.shape
  //                     }
                      
  //                         if(this.props.category == 3){
                           
  //                           this.props.pileSaveStep05(valueApi)
                            
  //                         }else{
  //                           this.props.pileSaveStep05(valueApi)
  //                           this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor})
                            
  //                         }
                        
    
  //                   }
  //                 }
  //               } else {
  //                 Alert.alert(this.state.error)
  //                 this.setState({checksavebutton:false,saveload:false})
  //               }
  //             }
                
  //             }
  //         })
        
  //     }else {
  //       this.onSetStack(7,0)
  //       this.props.onNextStep()
  //       setTimeout(()=>{this.setState({checksavebutton:false})},2000)
  //     }
    
  //   }else if(step == 2 && this.props.category == 5){
  //       if (this.state.Edit_Flag == 1) {
  //         var validate = true
  //         for (var i = 0; i <  this.props.drillinglistChild.length; i++) {
  //           if (this.props.drillinglistChild[i + 1]) {
  //             if (parseFloat(this.props.drillinglistChild[i].Depth) > parseFloat(this.props.drillinglistChild[i + 1].Depth)) {
  //               validate = false
  //             }
  //           }else {
  //             if (validate == true) {
  //               if (this.props.drillinglistChild[i - 1]) {
  //                 if (parseFloat(this.props.drillinglistChild[i].Depth) < parseFloat(this.props.drillinglistChild[i - 1].Depth)) {
  //                   validate = false
  //                 }
  //               }
  //             }
  //           }
  //         }
  //         // console.warn('validate step51',this.props.drillinglistChild.length)
  //         if(this.props.drillinglistChild.length == 0){
  //           validate = false
  //         }else{
  //           var validate_depth = parseFloat(this.state.dataDepth.depth) - parseFloat(this.state.data['5_2'].masterInfo.PileInfo.Constant)
  //           // console.warn('validate_depth: ',validate_depth,' Depth: ',parseFloat(this.props.drillinglistChild[this.props.drillinglistChild.length - 1].Depth))
  //           if (parseFloat(this.props.drillinglistChild[this.props.drillinglistChild.length - 1].Depth) >= parseFloat(validate_depth) && validate == true) {
  //             validate = true
  //           }else {
  //             validate = false
  //           }
  //         }
  
  //         var check_validate = false
  //         if(this.props.category == 5){
  //           if(this.state.data['5_5'].data.CheckPlummet == true){
  //             if(this.state.data['5_5'].data.CheckWaterLevel == true){
  //               check_validate = true
  //             }else{
  //               Alert.alert('',I18n.t('alert.checkwater'))
  //             }
  //           }else{
  //             Alert.alert('',I18n.t('alert.checkplummet'))
  //           }
  //         }else{
  //           check_validate = true
  //         }
  //         // console.log('validate step51',this.state.dataDepth)
  
  //         if (validate == true && check_validate == true) {
            
  //             var valueApi ={
  //               Language:I18n.locale,
  //               JobId: this.props.jobid,
  //               PileId: this.props.pileid,
  //               Page:6,
  //               latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
  //               longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
  //               CheckPlummet:this.state.data['5_5'].data.CheckPlummet,
  //               CheckWaterLevel:this.state.data['5_5'].data.CheckWaterLevel,
  //               // ImageLengthSteelCarry: this.state.data['5_1'].data.ImageLengthSteelCarry,
  //               ImageCheckPlummet: this.state.data['5_5'].data.ImageCheckPlummet,
  //               ImageCheckWaterLevel: this.state.data['5_5'].data.ImageCheckWaterLevel,
  //               // OverLifting: this.state.data["5_1"].data.OverLifting,
  //               // Pcoring: this.state.data["5_1"].data.Pcoring,
  //               // hanginglength: this.state.data["5_1"].data.HangingBarsLength
  //             }
  //             this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor,flag_to4:false,flag_to5:true})
  //             // console.warn('step 3 category 5',valueApi)
  //             this.props.pileSaveStep05(valueApi)
  //         }else {
  //           await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step5Status:1})
  //           this.props.mainPileGetStorage(this.props.pileid,'step_status')
  //           if(check_validate == true){
  //             Alert.alert(I18n.t('mainpile.5_3.depthcolnotco'))
  
  //           }
  //           this.setState({checksavebutton:false})
  //         }
  //       }else {
  //         if (this.props.shape == 1) {
  //           this.onSetStack(6,0)
  //           this.props.onNextStep()
  //           setTimeout(()=>{this.setState({checksavebutton:false})},2000)
  //         }else {
  //           this.onSetStack(5,3)
  //           this.setState({step: 3})
  //         }
  //       }
      
  //   }else if(step == 3 && this.props.category == 5){
  //     this.setState({checksavebutton:true})
  //     if (this.state.Edit_Flag == 1) {
  //       var value = {
  //         process:5,
  //         step:4,
  //         pile_id: this.props.pileid,
  //         shape: this.props.shape,
  //         drillinglist:this.props.drillinglist,
  //         category: this.props.category,
  //         parent:this.props.parent
  //       }

  //       this.props.mainpileAction_checkStepStatus(value)
  //         .then(async (data) =>
  //         {
  //           var start1 = moment(this.state.data['5_4'].data.StartDrillingFluidDate,'DD/MM/YYYY HH:mm')
  //           var end1 = moment(this.state.data['5_4'].data.EndDrillingFluidDate,'DD/MM/YYYY HH:mm')
  //           if(start1>=end1){
  //             Alert.alert('', I18n.t('mainpile.liquidtestinsert.alert.error4'), [
  //               {
  //                 text: 'OK'
  //               }
  //             ])
  //             this.setState({checksavebutton:false})
  //           }else{
              
  //             if(this.state.data['5_4'].data.CheckInstallStopEnd == true){
  //               var start = moment(this.state.data['5_4'].data.StartStopEndDate,'DD/MM/YYYY HH:mm')
  //               var end = moment(this.state.data['5_4'].data.EndStopEndDate,'DD/MM/YYYY HH:mm')
  //               if(start>=end){
  //                 Alert.alert('', I18n.t('mainpile.liquidtestinsert.alert.error4'), [
  //                   {
  //                     text: 'OK'
  //                   }
  //                 ])
  //                 this.setState({checksavebutton:false})
  //               }else{
                  
  //               var valueApi = {
  //                 Language:I18n.locale,
  //                 JobId: this.props.jobid,
  //                 PileId: this.props.pileid,
  //                 Page:5,
  //                 latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
  //                 longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
  //                 StartDrillingFluidDate:moment(this.state.data['5_4'].data.StartDrillingFluidDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
  //                 EndDrillingFluidDate:moment(this.state.data['5_4'].data.EndDrillingFluidDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
  //                 StartStopEndDate:moment(this.state.data['5_4'].data.StartStopEndDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
  //                 EndStopEndDate:moment(this.state.data['5_4'].data.EndStopEndDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),  
  //                 CheckStopEnd:this.state.data['5_4'].data.CheckStopEnd,
  //                 CheckWaterStop:this.state.data['5_4'].data.CheckWaterStop,
  //                 CheckInstallStopEnd:this.state.data['5_4'].data.CheckInstallStopEnd,
  //                 CheckDrillingFluid:this.state.data['5_4'].data.CheckDrillingFluid,
  //                 ImageCheckStopEnd:this.state.data['5_4'].data.ImageCheckStopEnd,
  //                 ImageCheckWaterStop:this.state.data['5_4'].data.ImageCheckWaterStop,
  //                 ImageCheckInstallStopEnd:this.state.data['5_4'].data.ImageCheckInstallStopEnd,
  //                 ImageCheckDrillingFluid:this.state.data['5_4'].data.ImageCheckDrillingFluid,
  //                 WaterStopTypeId:this.state.data['5_4'].data.waterstopid,
  //                 WaterStopLength:this.state.data['5_4'].data.WaterStopLength,
  //                 WaterStopTypeName: this.state.data['5_4'].data.WaterStopTypeName,
  //                 category:this.props.category
  //               }
  //               console.log('pile5_4',valueApi)
                
    
  //               if ( this.state.error == null) {
  //                 if (data.check == false) {
  //                   Alert.alert('', data.errorText[0], [
  //                     {
  //                       text: 'OK'
  //                     }
  //                   ])
  //                   this.setState({checksavebutton:false,saveload:false})
  //                 }else {
  //                   if (data.size >= 1) {
  //                     // this.props.pileSaveStep05(valueApi)
  //                     var data5_0 = this.state.data['5_0']
  //                     data5_0.data.Edit_Flag = 1
  //                     await this.props.mainPileSetStorage(this.props.pileid,'5_0',data5_0)
    
  //                     var value = {
  //                       process: 7,
  //                       step: 0,
  //                       pile_id: this.props.pileid,
  //                       shape: this.props.shape
  //                     }
                     
  //                       // console.warn('save5 category',this.props.category)
  //                         if(this.props.category == 3){
                            
                           
  //                           this.props.pileSaveStep05(valueApi)
                           
                          
  //                         }else{
  //                           this.props.pileSaveStep05(valueApi)
  //                           this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor})
                           
  //                         }
                         
                     
    
  //                   }
  //                 }
  //               } else {
  //                 Alert.alert(this.state.error)
  //                 this.setState({checksavebutton:false,saveload:false})
  //               }
  //               }
  //             }else{
               
  //               var valueApi = {
  //                 Language:I18n.locale,
  //                 JobId: this.props.jobid,
  //                 PileId: this.props.pileid,
  //                 Page:5,
  //                 latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
  //                 longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
  //                 StartDrillingFluidDate:moment(this.state.data['5_4'].data.StartDrillingFluidDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
  //                 EndDrillingFluidDate:moment(this.state.data['5_4'].data.EndDrillingFluidDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
  //                 StartStopEndDate:moment(this.state.data['5_4'].data.StartStopEndDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
  //                 EndStopEndDate:moment(this.state.data['5_4'].data.EndStopEndDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),  
  //                 CheckStopEnd:this.state.data['5_4'].data.CheckStopEnd,
  //                 CheckWaterStop:this.state.data['5_4'].data.CheckWaterStop,
  //                 CheckInstallStopEnd:this.state.data['5_4'].data.CheckInstallStopEnd,
  //                 CheckDrillingFluid:this.state.data['5_4'].data.CheckDrillingFluid,
  //                 ImageCheckStopEnd:this.state.data['5_4'].data.ImageCheckStopEnd,
  //                 ImageCheckWaterStop:this.state.data['5_4'].data.ImageCheckWaterStop,
  //                 ImageCheckInstallStopEnd:this.state.data['5_4'].data.ImageCheckInstallStopEnd,
  //                 ImageCheckDrillingFluid:this.state.data['5_4'].data.ImageCheckDrillingFluid,
  //                 WaterStopTypeId:this.state.data['5_4'].data.waterstopid,
  //                 WaterStopLength:this.state.data['5_4'].data.WaterStopLength,
  //                 WaterStopTypeName: this.state.data['5_4'].data.WaterStopTypeName,
  //                 category:this.props.category
  //               }
  //               console.log('pile5_4',valueApi)
                
    
  //               if ( this.state.error == null) {
  //                 if (data.check == false) {
  //                   Alert.alert('', data.errorText[0], [
  //                     {
  //                       text: 'OK'
  //                     }
  //                   ])
  //                   this.setState({checksavebutton:false})
  //                 }else {
  //                   if (data.size >= 1) {
  //                     // this.props.pileSaveStep05(valueApi)
  //                     var data5_0 = this.state.data['5_0']
  //                     data5_0.data.Edit_Flag = 1
  //                     await this.props.mainPileSetStorage(this.props.pileid,'5_0',data5_0)
    
  //                     var value = {
  //                       process: 7,
  //                       step: 0,
  //                       pile_id: this.props.pileid,
  //                       shape: this.props.shape
  //                     }
                      
  //                         if(this.props.category == 3){
                           
  //                           this.props.pileSaveStep05(valueApi)
                            
  //                         }else{
  //                           this.props.pileSaveStep05(valueApi)
  //                           this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor})
                            
  //                         }
                        
    
  //                   }
  //                 }
  //               } else {
  //                 Alert.alert(this.state.error)
  //                 this.setState({checksavebutton:false,saveload:false})
  //               }
  //             }
                
  //             }
  //         })
        
  //     }else {
  //       this.onSetStack(7,0)
  //       this.props.onNextStep()
  //       setTimeout(()=>{this.setState({checksavebutton:false})},2000)
  //     }
  //   }
    
  // }

  async onSave(step){
  console.warn('onSave step',step)
  var value,valueApi = null
  const {category} = this.props 
    await this.props.mainPileGetAllStorage()
    if(step == 0){
      console.log('step == 0')
      if(this.state.Edit_Flag !== 1){
        this.onSetStack(5,1)
        this.setState({step: 1})
        return 
      }
      // if(category==5){
      //   console.log('test step == 0',step)
      //   value = this.prepareData(0,1)
      //   this.props.mainpileAction_checkStepStatus(value)
      //     .then(async (data) =>
      //     {
      //       if ( this.state.error != null) return  this.setState({checksavebutton:false,saveload:false},()=> this.alertText(this.state.error,true))
      //       if(!data.check) return this.setState({checksavebutton:false,saveload:false},()=> this.alertText(data.errorText[0]))
      //       this.onSetStack(5,1)
      //       this.setState({step: 1})
      //     })
        
      // return
      // }
      value = this.prepareData(0,1)
      this.props.mainpileAction_checkStepStatus(value)
        .then(async (data) =>{
          if ( this.state.error != null) return  this.setState({checksavebutton:false,saveload:false},()=> this.alertText(this.state.error,true))
          if(!data) return
          if(!data.check) return this.setState({checksavebutton:false,saveload:false},()=> this.alertText(data.errorText[0]))
          // valueApi = category==5 ? this.prepareData(2,1,'5_1',1): this.prepareData(1,1,'5_1',1)
          valueApi = this.prepareData(1,1,'5_1',1)
          this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor})
          console.log('valueApi step 5',valueApi)
          this.props.pileSaveStep05(valueApi)
        })
    }else if(step == 1){
      if(this.props.shape == 1){
        this.setState({checksavebutton:true})
      }
      if(this.state.Edit_Flag !== 1){
        this.onSetStack(5,2)
        this.setState({step: 2})
        return 
      }
      value = this.prepareData(0,2)
      // if(this.props.category==5){
      //   console.log('test step == 1',step)
      //  let validate = this.checkDrilllist()
      //  let check_validate = this.checkPlummetAndWaterLevel('5_3')
      //   if(!validate||!check_validate){
      //     await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step5Status:1})
      //     this.props.mainPileGetStorage(this.props.pileid,'step_status')
      //     if(check_validate == true){
      //       Alert.alert(I18n.t('mainpile.5_3.depthcolnotco'))
      //     }
      //     this.setState({checksavebutton:false})
      //   return
      //   }
      //   valueApi = this.prepareData(1,2,'5_3',1)
      //   console.log('pile5_2',valueApi)
      //   this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor,flag_to4:true})
      //   this.props.pileSaveStep05(valueApi)
      //  return
      // }
      
      this.props.mainpileAction_checkStepStatus(value).then(async (data) =>{
        if ( this.state.error != null) return  this.setState({checksavebutton:false,saveload:false},()=> this.alertText(this.state.error,true))
        if(!data.check) return this.setState({checksavebutton:false,saveload:false},()=> this.alertText(data.errorText[0]))
        this.setState({checksavebutton:false})
        this.setState({step: 2})
        this.onSetStack(5,2)
      })
    }else if(step == 2){
      if(this.props.shape == 1) this.setState({checksavebutton:true})
      var value = this.prepareData(0,3)
      if(this.state.Edit_Flag !== 1){
        if (this.props.shape == 1) {
          this.onSetStack(6,0)
          this.props.onNextStep()
          setTimeout(()=>{this.setState({checksavebutton:false})},2000)
        }else {
          this.onSetStack(5,3)
          this.setState({step: 3})
        }
        return 
      }
    
      if(this.props.category == 1||this.props.category == 2||this.props.category == 3){
        let validate = this.checkDrilllist()
        if(!validate){
          await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step5Status:1})
          this.props.mainPileGetStorage(this.props.pileid,'step_status')
          Alert.alert(I18n.t('mainpile.5_3.depthcolnotco'))
          this.setState({checksavebutton:false})
        return
        }
      }


      if(this.props.category == 4){
        let validate = this.checkDrilllist()
        let check_validate = this.checkPlummetAndWaterLevel('5_3')
       
        if(!validate||!check_validate){
          await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step5Status:1})
          this.props.mainPileGetStorage(this.props.pileid,'step_status')
          if(check_validate == true){
            Alert.alert(I18n.t('mainpile.5_3.depthcolnotco'))
          }
          this.setState({checksavebutton:false})
        return
        }
        this.props.mainpileAction_checkStepStatus(value).then(async (data) =>{
          if ( this.state.error != null) return  this.setState({checksavebutton:false,saveload:false},()=> this.alertText(this.state.error,true))
          if (!data.check) return this.setState({checksavebutton:false,saveload:false},()=> this.alertText(data.errorText[0]))
          if (data.size < 1) return this.setState({checksavebutton:false,saveload:false},()=>{this.alertText(I18n.t('mainpile.5_1.error_nextstep'))})
          var data5_0 = this.state.data['5_0']
          data5_0.data.Edit_Flag = 1
          await this.props.mainPileSetStorage(this.props.pileid,'5_0',data5_0)
          this.props.getStepStatus2({
            jobid: this.props.jobid,
            pileid: this.props.pileid,
            process:5
          })
          setTimeout(()=>{this.setState({checksavebutton:false})},2000)
          })
        return
      }
      if(this.props.category==5){
      console.log('test step == 2 category==5',step)
       let validate = this.checkDrilllist()
       let check_validate = this.checkPlummetAndWaterLevel('5_3')
        if(!validate||!check_validate){
          await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step5Status:1})
          this.props.mainPileGetStorage(this.props.pileid,'step_status')
          if(check_validate == true){
            Alert.alert(I18n.t('mainpile.5_3.depthcolnotco'))
          }
          this.setState({checksavebutton:false})
        return
        }
        // valueApi =  this.prepareData(1,2,'5_3',1)
        valueApi = category==5 ? this.prepareData(2,2,'5_3',1): this.prepareData(1,2,'5_3',1)
        console.log('pile5_2',valueApi)
        this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor,flag_to4:true})
        this.props.pileSaveStep05(valueApi)
       return
      }
      // if(this.props.category==5){
      //   let validate = this.checkDrilllistChild()
      //   let check_validate = this.checkPlummetAndWaterLevel('5_5')
      //   if(!validate||!check_validate){
      //     await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step5Status:1})
      //     this.props.mainPileGetStorage(this.props.pileid,'step_status')
      //     if(check_validate == true){
      //       Alert.alert(I18n.t('mainpile.5_3.depthcolnotco'))
      //     }
      //     this.setState({checksavebutton:false})
      //     return
      //   }
      //   var valueApi = this.prepareData(1,4,'5_5',6)

      //   this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor,flag_to4:false,flag_to5:true})
      //   this.props.pileSaveStep05(valueApi)
      //   return
      // }
      Alert.alert('',this.props.shape == 1 ? I18n.t('mainpile.6_2.changebucket') : I18n.t('mainpile.5_3.depthcom'),
        [
          {
            text: 'Cancel', onPress: async () =>{this.setState({checksavebutton:false})}
          },
          { text: 'OK', onPress: async () => {
              this.props.mainpileAction_checkStepStatus(value).then(async (data) => {
                console.log('this.state.error 1',this.state.error)
                if ( this.state.error != null) return  this.setState({checksavebutton:false,saveload:false},()=> this.alertText(this.state.error,true))
                console.log('!data.check 2',data.errorText[0],!data.check)
                if (!data.check) return this.setState({checksavebutton:false,saveload:false},()=> this.alertText(data.errorText[0]))
                console.log('data.size < 1 3')
                if (data.size < 1) return this.setState({checksavebutton:false,saveload:false},()=>{this.alertText(I18n.t('mainpile.5_1.error_nextstep'))})
                console.log('data.size < 1 after')
                if (this.props.shape !== 1){
                  this.props.getStepStatus2({
                    jobid: this.props.jobid,
                    pileid: this.props.pileid,
                    process:5
                  })
                  setTimeout(()=>{
                    this.setState({checksavebutton:false})
                    this.onSetStack(5,3)
                    this.setState({step: 3})
                  },2000)
                  return
                }
                var data5_0 = this.state.data['5_0']
                data5_0.data.Edit_Flag = 1
                await this.props.mainPileSetStorage(this.props.pileid,'5_0',data5_0)
                this.props.getStepStatus2({ jobid: this.props.jobid, pileid: this.props.pileid, process:5 })
                setTimeout(()=>{this.setState({checksavebutton:false})},2000)
              })
            }
          }
        ],
        { cancelable: false }
      )
    }else if(step == 3){
      this.setState({checksavebutton:true})
      if (this.state.Edit_Flag !== 1) {
        this.onSetStack(7,0)
        this.props.onNextStep()
        setTimeout(()=>{this.setState({checksavebutton:false})},2000)
        return
      }
      value = this.prepareData(0,4)
      if(this.props.category==5){
        let validate = this.checkDrilllistChild()
        let check_validate = this.checkPlummetAndWaterLevel('5_5')
        if(!validate||!check_validate){
          await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step5Status:1})
          this.props.mainPileGetStorage(this.props.pileid,'step_status')
          if(check_validate == true){
            Alert.alert(I18n.t('mainpile.5_3.depthcolnotco'))
          }
          this.setState({checksavebutton:false})
          return
        }
        // var valueApi = this.prepareData(1,4,'5_5',6)
        var valueApi = category==5 ? this.prepareData(2,4,'5_5',6): this.prepareData(1,4,'5_5',6)
        this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor,flag_to4:false,flag_to5:true,checksavebutton:false})
        this.props.pileSaveStep05(valueApi)
        return
      }
      this.props.mainpileAction_checkStepStatus(value).then(async (data) => {
        var start1 = moment(this.state.data['5_4'].data.StartDrillingFluidDate,'DD/MM/YYYY HH:mm')
        var end1 = moment(this.state.data['5_4'].data.EndDrillingFluidDate,'DD/MM/YYYY HH:mm')
        if( start1>=end1) return this.setState({checksavebutton:false,saveload:false},()=> this.alertText(I18n.t('mainpile.liquidtestinsert.alert.error4')))
        if(this.state.data['5_4'].data.CheckInstallStopEnd == true){
          var start = moment(this.state.data['5_4'].data.StartStopEndDate,'DD/MM/YYYY HH:mm')
          var end = moment(this.state.data['5_4'].data.EndStopEndDate,'DD/MM/YYYY HH:mm')
          if(start>=end) return this.setState({checksavebutton:false,saveload:false},()=> this.alertText(I18n.t('mainpile.liquidtestinsert.alert.error4')))
         
          if ( this.state.error != null) return  this.setState({checksavebutton:false,saveload:false},()=> this.alertText(this.state.error,true))
          if (!data.check) return this.setState({checksavebutton:false,saveload:false},()=> this.alertText(data.errorText[0]))
          if (data.size < 1) return this.setState({checksavebutton:false,saveload:false},()=>{this.alertText(I18n.t('mainpile.5_1.error_nextstep'))})
          var data5_0 = this.state.data['5_0']
          data5_0.data.Edit_Flag = 1
          await this.props.mainPileSetStorage(this.props.pileid,'5_0',data5_0)
          var valueApi = {
            Language:I18n.locale,
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            Page:5,
            latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
            longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
            StartDrillingFluidDate:moment(this.state.data['5_4'].data.StartDrillingFluidDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
            EndDrillingFluidDate:moment(this.state.data['5_4'].data.EndDrillingFluidDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
            StartStopEndDate:moment(this.state.data['5_4'].data.StartStopEndDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
            EndStopEndDate:moment(this.state.data['5_4'].data.EndStopEndDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),  
            CheckStopEnd:this.state.data['5_4'].data.CheckStopEnd,
            CheckWaterStop:this.state.data['5_4'].data.CheckWaterStop,
            CheckInstallStopEnd:this.state.data['5_4'].data.CheckInstallStopEnd,
            CheckDrillingFluid:this.state.data['5_4'].data.CheckDrillingFluid,
            ImageCheckStopEnd:this.state.data['5_4'].data.ImageCheckStopEnd,
            ImageCheckWaterStop:this.state.data['5_4'].data.ImageCheckWaterStop,
            ImageCheckInstallStopEnd:this.state.data['5_4'].data.ImageCheckInstallStopEnd,
            ImageCheckDrillingFluid:this.state.data['5_4'].data.ImageCheckDrillingFluid,
            WaterStopTypeId:this.state.data['5_4'].data.waterstopid,
            WaterStopLength:this.state.data['5_4'].data.WaterStopLength,
            WaterStopTypeName: this.state.data['5_4'].data.WaterStopTypeName,
            category:this.props.category
          }
          if(this.props.category == 3) return  this.props.pileSaveStep05(valueApi)
          this.props.pileSaveStep05(valueApi)
          this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor})

        }else{
          var valueApi = {
            Language:I18n.locale,
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            Page:5,
            latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
            longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
            StartDrillingFluidDate:moment(this.state.data['5_4'].data.StartDrillingFluidDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
            EndDrillingFluidDate:moment(this.state.data['5_4'].data.EndDrillingFluidDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
            StartStopEndDate:moment(this.state.data['5_4'].data.StartStopEndDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
            EndStopEndDate:moment(this.state.data['5_4'].data.EndStopEndDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),  
            CheckStopEnd:this.state.data['5_4'].data.CheckStopEnd,
            CheckWaterStop:this.state.data['5_4'].data.CheckWaterStop,
            CheckInstallStopEnd:this.state.data['5_4'].data.CheckInstallStopEnd,
            CheckDrillingFluid:this.state.data['5_4'].data.CheckDrillingFluid,
            ImageCheckStopEnd:this.state.data['5_4'].data.ImageCheckStopEnd,
            ImageCheckWaterStop:this.state.data['5_4'].data.ImageCheckWaterStop,
            ImageCheckInstallStopEnd:this.state.data['5_4'].data.ImageCheckInstallStopEnd,
            ImageCheckDrillingFluid:this.state.data['5_4'].data.ImageCheckDrillingFluid,
            WaterStopTypeId:this.state.data['5_4'].data.waterstopid,
            WaterStopLength:this.state.data['5_4'].data.WaterStopLength,
            WaterStopTypeName: this.state.data['5_4'].data.WaterStopTypeName,
            category:this.props.category
          }
          console.log('pile5_4',valueApi)
          if ( this.state.error != null) return  this.setState({checksavebutton:false,saveload:false},()=> this.alertText(this.state.error,true))
          if (!data.check) return this.setState({checksavebutton:false,saveload:false},()=> this.alertText(data.errorText[0]))
          if (data.size < 1) return this.setState({checksavebutton:false,saveload:false},()=>{this.alertText(I18n.t('mainpile.5_1.error_nextstep'))})
          var data5_0 = this.state.data['5_0']
          data5_0.data.Edit_Flag = 1
          await this.props.mainPileSetStorage(this.props.pileid,'5_0',data5_0)
          if(this.props.category == 3) return  this.props.pileSaveStep05(valueApi)
          this.props.pileSaveStep05(valueApi)
          this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor})
        }
      })
    }else if(step == 4){
      this.setState({checksavebutton:true})
      if (this.state.Edit_Flag !== 1) {
        this.onSetStack(7,0)
        this.props.onNextStep()
        setTimeout(()=>{this.setState({checksavebutton:false})},2000)
        return
      }
      value = this.prepareData(0,4)
      if(this.props.category==5){
        this.props.mainpileAction_checkStepStatus(value).then(async (data) => {
          var start1 = moment(this.state.data['5_4'].data.StartDrillingFluidDate,'DD/MM/YYYY HH:mm')
          var end1 = moment(this.state.data['5_4'].data.EndDrillingFluidDate,'DD/MM/YYYY HH:mm')
          if( start1>=end1) return this.setState({checksavebutton:false,saveload:false},()=> this.alertText(I18n.t('mainpile.liquidtestinsert.alert.error4')))
          if(this.state.data['5_4'].data.CheckInstallStopEnd == true){
            var start = moment(this.state.data['5_4'].data.StartStopEndDate,'DD/MM/YYYY HH:mm')
            var end = moment(this.state.data['5_4'].data.EndStopEndDate,'DD/MM/YYYY HH:mm')
            if(start>=end) return this.setState({checksavebutton:false,saveload:false},()=> this.alertText(I18n.t('mainpile.liquidtestinsert.alert.error4')))
           
            if ( this.state.error != null) return  this.setState({checksavebutton:false,saveload:false},()=> this.alertText(this.state.error,true))
            if (!data.check) return this.setState({checksavebutton:false,saveload:false},()=> this.alertText(data.errorText[0]))
            if (data.size < 1) return this.setState({checksavebutton:false,saveload:false},()=>{this.alertText(I18n.t('mainpile.5_1.error_nextstep'))})
            var data5_0 = this.state.data['5_0']
            data5_0.data.Edit_Flag = 1
            await this.props.mainPileSetStorage(this.props.pileid,'5_0',data5_0)
            var valueApi = {
              Language:I18n.locale,
              JobId: this.props.jobid,
              PileId: this.props.pileid,
              Page:5,
              latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
              longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
              StartDrillingFluidDate:moment(this.state.data['5_4'].data.StartDrillingFluidDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
              EndDrillingFluidDate:moment(this.state.data['5_4'].data.EndDrillingFluidDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
              StartStopEndDate:moment(this.state.data['5_4'].data.StartStopEndDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
              EndStopEndDate:moment(this.state.data['5_4'].data.EndStopEndDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),  
              CheckStopEnd:this.state.data['5_4'].data.CheckStopEnd,
              CheckWaterStop:this.state.data['5_4'].data.CheckWaterStop,
              CheckInstallStopEnd:this.state.data['5_4'].data.CheckInstallStopEnd,
              CheckDrillingFluid:this.state.data['5_4'].data.CheckDrillingFluid,
              ImageCheckStopEnd:this.state.data['5_4'].data.ImageCheckStopEnd,
              ImageCheckWaterStop:this.state.data['5_4'].data.ImageCheckWaterStop,
              ImageCheckInstallStopEnd:this.state.data['5_4'].data.ImageCheckInstallStopEnd,
              ImageCheckDrillingFluid:this.state.data['5_4'].data.ImageCheckDrillingFluid,
              WaterStopTypeId:this.state.data['5_4'].data.waterstopid,
              WaterStopLength:this.state.data['5_4'].data.WaterStopLength,
              WaterStopTypeName: this.state.data['5_4'].data.WaterStopTypeName,
              category:this.props.category
            }
            if(this.props.category == 3) return  this.props.pileSaveStep05(valueApi)
            this.props.pileSaveStep05(valueApi)
            this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor})
  
          }else{
            var valueApi = {
              Language:I18n.locale,
              JobId: this.props.jobid,
              PileId: this.props.pileid,
              Page:5,
              latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
              longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
              StartDrillingFluidDate:moment(this.state.data['5_4'].data.StartDrillingFluidDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
              EndDrillingFluidDate:moment(this.state.data['5_4'].data.EndDrillingFluidDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
              StartStopEndDate:moment(this.state.data['5_4'].data.StartStopEndDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),
              EndStopEndDate:moment(this.state.data['5_4'].data.EndStopEndDate,'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"),  
              CheckStopEnd:this.state.data['5_4'].data.CheckStopEnd,
              CheckWaterStop:this.state.data['5_4'].data.CheckWaterStop,
              CheckInstallStopEnd:this.state.data['5_4'].data.CheckInstallStopEnd,
              CheckDrillingFluid:this.state.data['5_4'].data.CheckDrillingFluid,
              ImageCheckStopEnd:this.state.data['5_4'].data.ImageCheckStopEnd,
              ImageCheckWaterStop:this.state.data['5_4'].data.ImageCheckWaterStop,
              ImageCheckInstallStopEnd:this.state.data['5_4'].data.ImageCheckInstallStopEnd,
              ImageCheckDrillingFluid:this.state.data['5_4'].data.ImageCheckDrillingFluid,
              WaterStopTypeId:this.state.data['5_4'].data.waterstopid,
              WaterStopLength:this.state.data['5_4'].data.WaterStopLength,
              WaterStopTypeName: this.state.data['5_4'].data.WaterStopTypeName,
              category:this.props.category
            }
            console.log('pile5_4',valueApi)
            if ( this.state.error != null) return  this.setState({checksavebutton:false,saveload:false},()=> this.alertText(this.state.error,true))
            if (!data.check) return this.setState({checksavebutton:false,saveload:false},()=> this.alertText(data.errorText[0]))
            if (data.size < 1) return this.setState({checksavebutton:false,saveload:false},()=>{this.alertText(I18n.t('mainpile.5_1.error_nextstep'))})
            var data5_0 = this.state.data['5_0']
            data5_0.data.Edit_Flag = 1
            await this.props.mainPileSetStorage(this.props.pileid,'5_0',data5_0)
            if(this.props.category == 3) return  this.props.pileSaveStep05(valueApi)
            this.props.pileSaveStep05(valueApi)
            this.setState({saveload:this.state.status1==2?true:false,statuscolor:data.statuscolor})
          }
        })
      }
      
    }
    console.log('step == ',step)
  }


  prepareData(type,step,stepname,page){
    let baseData0 = { 
      process:5,
      pile_id: this.props.pileid,
      shape: this.props.shape,
      drillinglist:this.props.drillinglist,
      category: this.props.category,
      parent:this.props.parent
    }
    let baseData1 = {
      Language:I18n.locale,
      JobId: this.props.jobid,
      PileId: this.props.pileid,
      category: this.props.category,
      latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
      longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
      
    }

    switch(type) {
      case 0:
        return {
          step:step,
          ...baseData0
        }
      case 1:
        return {
          Page:page,
          CheckPlummet:this.state.data[stepname].data.CheckPlummet,
          CheckWaterLevel:this.state.data[stepname].data.CheckWaterLevel,
          ImageCheckPlummet: this.state.data[stepname].data.ImageCheckPlummet,
          ImageCheckWaterLevel: this.state.data[stepname].data.ImageCheckWaterLevel,
          OverLifting: this.state.data[stepname].data.OverLifting,
          Pcoring: this.state.data[stepname].data.Pcoring,
          ...baseData1
        }
      case 2:
        return {
          Page:page,
          CheckPlummet:this.state.data[stepname].data.CheckPlummet,
          CheckWaterLevel:this.state.data[stepname].data.CheckWaterLevel,
          ImageCheckPlummet: this.state.data[stepname].data.ImageCheckPlummet,
          ImageCheckWaterLevel: this.state.data[stepname].data.ImageCheckWaterLevel,
          OverLifting: this.state.data['5_1'].data.OverLifting,
          Pcoring: this.state.data['5_1'].data.Pcoring,
          ...baseData1
        } 
      default:
        return null;        
      }

  }
  alertText(text,header){
    if(header) return Alert.alert(text)
    return Alert.alert('', text, [{text: 'OK'}])
  }
  checkDrilllist(){
    var validate = true
      for (var i = 0; i <  this.props.drillinglist.length; i++) {
        if (this.props.drillinglist[i + 1]) {
          if (parseFloat(this.props.drillinglist[i].Depth) > parseFloat(this.props.drillinglist[i + 1].Depth)) {
            validate = false
          }
        }else {
          if (validate == true) {
            if (this.props.drillinglist[i - 1]) {
              if (parseFloat(this.props.drillinglist[i].Depth) < parseFloat(this.props.drillinglist[i - 1].Depth)) {
                validate = false
              }
            }
          }
        }
      }
      if(this.props.drillinglist.length == 0){
          validate = false
        }else{
          var validate_depth = parseFloat(this.state.dataDepth.depth) - parseFloat(this.state.data['5_2'].masterInfo.PileInfo.Constant)
          if (parseFloat(this.props.drillinglist[this.props.drillinglist.length - 1].Depth) >= parseFloat(validate_depth) && validate == true) {
            validate = true
          }else {
            validate = false
          }
        }
      return validate
  }
  checkPlummetAndWaterLevel(step){
    var check_validate = false
    if(this.props.category == 5){
      if(this.state.data[step].data.CheckPlummet == true){
        if(this.state.data[step].data.CheckWaterLevel == true){
          check_validate = true
        }else{
          Alert.alert('',I18n.t('alert.checkwater'))
        }
      }else{
        Alert.alert('',I18n.t('alert.checkplummet'))
      }
    }else{
      check_validate = true
    }
    return check_validate
  }
  checkDrilllistChild(){
    var validate = true
    for (var i = 0; i <  this.props.drillinglistChild.length; i++) {
      if (this.props.drillinglistChild[i + 1]) {
        if (parseFloat(this.props.drillinglistChild[i].Depth) > parseFloat(this.props.drillinglistChild[i + 1].Depth)) {
          validate = false
        }
      }else {
        if (validate == true) {
          if (this.props.drillinglistChild[i - 1]) {
            if (parseFloat(this.props.drillinglistChild[i].Depth) < parseFloat(this.props.drillinglistChild[i - 1].Depth)) {
              validate = false
            }
          }
        }
      }
    }
    if(this.props.drillinglistChild.length == 0){
      validate = false
    }else{
      var validate_depth = parseFloat(this.state.dataDepth.depth) - parseFloat(this.state.data['5_2'].masterInfo.PileInfo.Constant)
      if (parseFloat(this.props.drillinglistChild[this.props.drillinglistChild.length - 1].Depth) >= parseFloat(validate_depth) && validate == true) {
        validate = true
      }else {
        validate = false
      }
    }
    return validate
  }

  onBlock(step){
    // console.warn('onBlock',step)
    if (step == 2 && this.props.category != 5) {
        var value = {
          process: 5,
          step: 3,
          pile_id: this.props.pileid,
          shape: this.props.shape
        }
        this.props.mainpileAction_checkStepStatus(value).then(async (data) => {
          console.log(data)
          if (data.size >= 2) {
            this.setState({step: step})
            this.onSetStack(5,step)
          }
        })
    }else if (step == 3 && this.props.category != 5) {
      var value = {
        process: 5,
        step: 4,
        pile_id: this.props.pileid,
        shape: this.props.shape
      }
      this.props.mainpileAction_checkStepStatus(value).then(async (data) => {
        console.log(data)
        if (data.size >= 1) {
          var validate = true
          for (var i = 0; i <  this.state.data['5_3'].data.DrillingList.length; i++) {
            if (this.state.data['5_3'].data.DrillingList[i + 1] != undefined ) {
              if (this.state.data['5_3'].data.DrillingList[i].Depth > this.state.data['5_3'].data.DrillingList[i + 1].Depth) {
                validate = false
              }
            }else {
              if (validate == true) {
                if (this.state.data['5_3'].data.DrillingList[i - 1] != undefined) {
                  if (this.state.data['5_3'].data.DrillingList[i].Depth < this.state.data['5_3'].data.DrillingList[i - 1].Depth) {
                    validate = false
                  }
                }
              }
            }
          }
          console.log(this.state.data)
          var validate_depth = parseFloat(this.state.data['5_2'].masterInfo.PileInfo.Depth) - parseFloat(this.state.data['5_2'].masterInfo.PileInfo.Constant)
          if (parseFloat(this.state.data['5_3'].data.DrillingList[this.state.data['5_3'].data.DrillingList.length - 1].Depth) >= parseFloat(validate_depth) && validate == true) {
            validate = true
          }else {
            validate = false
          }
          if (validate == true) {
            this.setState({step: step})
            this.onSetStack(5,step)
          }
        }
      })
    }else if(step == 3 && this.props.category == 5){
      var value = {
        process: 5,
        step: 4,
        pile_id: this.props.pileid,
        shape: this.props.shape
      }
      this.props.mainpileAction_checkStepStatus(value).then(async (data) => {
        console.log(data)
        if (data.size >= 1) {
          var validate = true
          for (var i = 0; i <  this.state.data['5_3'].data.DrillingList.length; i++) {
            if (this.state.data['5_3'].data.DrillingList[i + 1] != undefined ) {
              if (this.state.data['5_3'].data.DrillingList[i].Depth > this.state.data['5_3'].data.DrillingList[i + 1].Depth) {
                validate = false
              }
            }else {
              if (validate == true) {
                if (this.state.data['5_3'].data.DrillingList[i - 1] != undefined) {
                  if (this.state.data['5_3'].data.DrillingList[i].Depth < this.state.data['5_3'].data.DrillingList[i - 1].Depth) {
                    validate = false
                  }
                }
              }
            }
          }
          console.log(this.state.data)
          var validate_depth = parseFloat(this.state.data['5_2'].masterInfo.PileInfo.Depth) - parseFloat(this.state.data['5_2'].masterInfo.PileInfo.Constant)
          if (parseFloat(this.state.data['5_3'].data.DrillingList[this.state.data['5_3'].data.DrillingList.length - 1].Depth) >= parseFloat(validate_depth) && validate == true) {
            validate = true
          }else {
            validate = false
          }
          if (validate == true) {
            this.setState({step: step})
            this.onSetStack(5,step)
          }
        }
      })
    }else {
      console.log(step)
      this.setState({step: step})
      this.onSetStack(5,step)
    }

  }

  async onSetStack(pro,step) {
    var data = {
      process: pro,
      step: step + 1
    }
    await this.props.setStack(data)
    await this.props.mainPileGetAllStorage()
  }
  setStep(step){
    return this.setState({step:step})
  }

  deleteButton = () => {
    this.props.setStack({
      process: 5,
      step: 1
    })
    const data = {
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      processid: this.state.process,
      lat: this.state.location == undefined ? 1 : this.state.location.position.lat,
      log: this.state.location == undefined ? 1 : this.state.location.position.log,
    }
    var temp = 0
    if(this.props.shape == 1 && this.props.category != 4){
      temp = this.state.status6
    }else{
      temp = this.state.status7
    }
    if(temp == 0){
      Alert.alert("Warning", I18n.t("mainpile.button.confirmdelete"), [
        { text: "Cancel" },
        { text: "OK", onPress: () => this.props.pileDelete(data) }
      ])
    }else{
      if(this.props.shape == 1 && this.props.category != 4){
        Alert.alert("", I18n.t('alert.error_delete5'), [
          {
            text: "OK"
          }
        ])
      }else{
        Alert.alert("", I18n.t('alert.error_delete6'), [
          {
            text: "OK"
          }
        ])
      }
      
    }
  }

  _renderStepFooter() {
    var index = 3
    if(this.props.category == 5){
      index = 5
    }else{
      if(this.props.shape == 1){
        index = 3
      }else{
        index = 4
      }
    }
    if (this.state.step == 2) {
      return (
        <View>
          <View style={{ flexDirection: "column" }}>
            <View style={{borderWidth: 1 }}>
              <StepIndicator stepCount={index} onPress={step => {
                if (this.refs['pile5_1'].getWrappedInstance().state.loading != true) {
                  this.onBlock(step)
                }
              }} currentPosition={this.state.step} />
            </View>
            {
              this.state.Edit_Flag == 1 ?
              <View style={[styles.second,
                this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
              ]}>
                <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}
                // disabled={this.state.status6 == 0 ? false:true}
                >
                  <Text style={styles.selectButton}>{I18n.t('mainpile.button.delete')}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.secondButton,
                  this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                ]} onPress={this.nextButton}
                disabled={this.state.checksavebutton}
                >
                  <Text style={styles.selectButton}>{this.state.Edit_Flag == 1 ? I18n.t('mainpile.button.next'):I18n.t('mainpile.button.continue')}</Text>
                </TouchableOpacity>
              </View>
              :
              <View/>
            }
          </View>
        </View>
      )
    }else {
      return (
        <View>
          <View style={{ flexDirection: "column" }}>
            <View style={{borderWidth: 1 }}>
              <StepIndicator stepCount={index} onPress={step => {
                if (this.refs['pile5_1'].getWrappedInstance().state.loading != true) {
                  this.onBlock(step)
                }
              }} currentPosition={this.state.step} />
            </View>
            {
              this.state.Edit_Flag == 1 ?
              <View style={[styles.second,
                this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
              ]}>
              <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}>
                <Text style={styles.selectButton}>{I18n.t('mainpile.button.delete')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.secondButton,
                this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
              ]} onPress={this.nextButton}
              disabled={this.state.checksavebutton}
              >
                <Text style={styles.selectButton}>{this.state.Edit_Flag == 1 ? I18n.t('mainpile.button.next'):I18n.t('mainpile.button.continue')}</Text>
              </TouchableOpacity>
            </View>
              :
              <View/>
            }
          </View>
        </View>
      )
    }

  }

  render() {
    if (this.props.hidden) {
      return null
    }
    console.warn('this.props.parent 5_0',!(this.state.step == 0))
    var index_step_3 = 3
    var index_step_4 = 4
    if(this.props.category == 5){
      index_step_3 = 4
      index_step_4 = 3
    }
    return (
      <View style={{ flex: 1 }}>
        {
          this.state.saveload?<Content><ActivityIndicator size="large" color="#007CC2" /></Content>:<View/>
        }
        {!this.state.saveload && this.props.category != 5?
        <Pile5_3 parent={this.props.parent} ref='pile5_3' hidden={!(this.state.step == 2)} jobid={this.props.jobid} pileid={this.props.pileid} shape={this.props.shape} pile5Type={'head'} category={this.props.category} sp_parentid={this.props.sp_parentid}/>
        :<View/>
        }
        {!this.state.saveload && this.props.category != 5?
        <Pile5_5 parent={this.props.parent} ref='pile5_5' hidden={!(this.state.step == index_step_4)} jobid={this.props.jobid} pileid={this.props.pileid} shape={this.props.shape} pile5Type={'head'} category={this.props.category} sp_parentid={this.props.sp_parentid}/>
        :<View/>
        }

        {!this.state.saveload?
          <Content>
          <Pile5_1 parent={this.props.parent} ref='pile5_1' hidden={!(this.state.step == 0)} jobid={this.props.jobid} pileid={this.props.pileid} onRefresh={this.props.onRefresh} shape={this.props.shape} category={this.props.category} pile={this.props.pile}/>
          <Pile5_2 parent={this.props.parent} ref='pile5_2' hidden={!(this.state.step == 1)} jobid={this.props.jobid} pileid={this.props.pileid} shape={this.props.shape} category={this.props.category}/>
          <Pile5_3 parent={this.props.parent} ref='pile5_3' hidden={!(this.state.step == 2)} jobid={this.props.jobid} pileid={this.props.pileid} shape={this.props.shape} pile5Type={'content'} category={this.props.category} sp_parentid={this.props.sp_parentid}/>
          
          <Pile5_5 parent={this.props.parent} ref='pile5_5' hidden={!(this.state.step == index_step_4)} jobid={this.props.jobid} pileid={this.props.pileid} shape={this.props.shape} pile5Type={'content'} category={this.props.category} sp_parentid={this.props.sp_parentid}/>
          
          <Pile5_4 parent={this.props.parent} ref='pile5_4' hidden={!(this.state.step == index_step_3)} jobid={this.props.jobid} pileid={this.props.pileid} shape={this.props.shape} category={this.props.category}/>

        </Content>:<View/>
        }
        {this._renderStepFooter()}
      </View>
    )
  }
}
const mapStateToProps = state => {
  // console.log('mapStateToProps',state);
  return {
    error: state.pile.error,
    pileAll: state.mainpile.pileAll || null,
    stack: state.mainpile.stack_item,
    drillingfluidslist:state.drillingFluids.drillingfluidslist,
    drillinglist:state.drillinglist.drillinglist,
    drillinglistChild: state.drillinglist.drillinglistChild,
    
    checkto7_super: state.pile.checkto7_super,
    checkto7_super_random: state.pile.checkto7_super_random,
    checkto7_super_process:state.pile.checkto7_super_process,

    process5_success_1:state.pile.process5_success_1,
    process5_success_1_random:state.pile.process5_success_1_random,
    process5_success_4:state.pile.process5_success_4,
    process5_success_4_random:state.pile.process5_success_4_random,
    lockstep7_8: state.pile.lockstep7_8,
    lockstepcheckto7_super:state.pile.lockstepcheckto7_super,
    process5_deleterow:state.pile.process5_deleterow,
    drillinglist_random:state.drillinglist.drillinglist_random,

    step_status2_random:state.mainpile.step_status2_random,
    step_status2:state.mainpile.step_status2,
    processstatus:state.mainpile.process,

    checkto6_super: state.pile.checkto6_super,
    checkto6_super_random: state.pile.checkto6_super_random,
    lockstepcheckto6_super:state.pile.lockstepcheckto6_super,
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(Pile5)
