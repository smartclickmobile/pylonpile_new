import React, { Component } from "react"
import { View, Text, TouchableOpacity, TextInput } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"
import I18n from "../../assets/languages/i18n"
import styles from "./styles/Pile7_2.style"
import { MENU_GREY_ITEM,MAIN_COLOR } from "../Constants/Color"
import RNTooltips from "react-native-tooltips"
import { Icon } from "react-native-elements"

class Pile7_2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 7,
      step: 2,
      distance: '',
      distanceCalled: false,
      pco: '',
      pcoCalled: false,
      metal: '',
      cutoff: this.props.pile.cutoff || 0,
      topcasing: 0,
      Edit_Flag: 1,
      visible: false,
      status7:0,

      hanginglength:'',
      images_hanging:[],
    }
  }

  async componentWillReceiveProps(nextProps) {
    this.setState({visible:false})
    var pile = null
    var pileMaster = null
    var pile3_3 = null
    
    var pile7 = null
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["7_2"]) {
      pile = nextProps.pileAll[this.props.pileid]["7_2"].data
      pileMaster = nextProps.pileAll[this.props.pileid]["7_2"].masterInfo
      pile5 = nextProps.pileAll[this.props.pileid]["5_1"].data
    }
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["7_0"]) {
      pile7 = nextProps.pileAll[this.props.pileid]["7_0"].data
      this.setState({status7:nextProps.pileAll[this.props.pileid].step_status.Step7Status})
    }

    if(nextProps.pileAll != undefined && nextProps.pileAll != "") {
      this.setState({ location: nextProps.pileAll.currentLocation })
    }
    if(this.props.shape != 1){
      if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["3_5"]) {
      
        pile3_3 = nextProps.pileAll[this.props.pileid]["3_5"].data
      }
    }else{
      if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["3_3"]) {
      
        pile3_3 = nextProps.pileAll[this.props.pileid]["3_3"].data
      }
    }
    
    if(pile3_3) {
      if(pile3_3.top != null && pile3_3.top != undefined) {
        this.setState({ topcasing: parseFloat(pile3_3.top) })
      }
    }
 
    if (pile7) {
      console.log('pile7.Edit_Flag ',pile7.Edit_Flag )
      this.setState({ Edit_Flag: pile7.Edit_Flag })
    }

    if(pile){
      console.log('7_2 data test',pile)
      if (pile.ImageLengthSteelCarry != "" && pile.ImageLengthSteelCarry != undefined && pile.ImageLengthSteelCarry.length >0 ) {
        
        await this.setState({images_hanging: pile.ImageLengthSteelCarry})
      }else if(pile.ImageLengthSteelCarry == null || pile.ImageLengthSteelCarry.length == 0) {
        this.setState({images_hanging: []})
      }
      if (pile.HangingBarsLength != "" && pile.HangingBarsLength != undefined&&isNaN(parseFloat(pile.HangingBarsLength).toFixed(3))==false) {
        await this.setState({hanginglength: parseFloat(pile.HangingBarsLength).toFixed(3)})
      }else{
        this.setState({hanginglength: ''})
      }
      
    }
    if(pile5){
      let check1 = pile5.OverLifting!==''&&pile5.OverLifting!==null
      let check2 = pile5.Pcoring!==''&&pile5.Pcoring!==null
      if(check1&&check2){
        this.setState({distance: parseFloat(pile5.OverLifting).toFixed(3),pco: parseFloat(pile5.Pcoring).toFixed(3)},()=>{
          this.onCalculate()
        })
      }else{
        if(check1) this.setState({distance: parseFloat(pile5.OverLifting).toFixed(3)})
        if(check2) this.setState({pco: parseFloat(pile5.Pcoring).toFixed(3)})
      }
    }
  
    
  }



  async updateState(value) {
    console.log('updateState step7_2',value)
    if (value.data.images_hanging != undefined) {
      await this.setState({images_hanging:value.data.images_hanging})
      this.saveLocal()
   }
  }

  onCalculate(type, value) {
    switch(type) {
      case "distance":
        if(value != '' &&  value != undefined&&value>=0){
          var temp = this.state.topcasing - this.state.cutoff - parseFloat(value) - parseFloat(this.state.pco)
          this.setState({ metal: isNaN(temp.toFixed(3))?'':temp.toFixed(3), distance: this.onChangeFormatDecimal(value),visible:false }, () => {
          })
        }else{
          this.setState({ metal: '', distance: '',visible:false }, () => {})
        }
        break
      case "pco":
        if(value != '' &&  value != undefined&&value>=0){
          var temp = this.state.topcasing - this.state.cutoff - parseFloat(this.state.distance) - parseFloat(value)
          this.setState({ metal: isNaN(temp.toFixed(3))?'':temp.toFixed(3), pco: this.onChangeFormatDecimal(value),visible:false }, () => {
          })
        }else{
          this.setState({ metal: '', pco: '',visible:false }, () => {})
        }
        break
      default:
        let metal = (this.state.topcasing - this.state.cutoff - parseFloat(this.state.distance) - parseFloat(this.state.pco)).toFixed(3)
        this.setState({
            metal:metal
          },() => {})
        break
    }
  }

  onCameraRoll(keyname,photos,type){
    if (this.state.Edit_Flag == 1) {
      Actions.camearaRoll({status:this.state.status7,jobid:this.props.jobid,process:this.state.process,step:this.state.step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:type,shape:this.props.shape,category:this.props.category,Edit_Flag:this.state.Edit_Flag})
    }else {
      Actions.camearaRoll({status:this.state.status7,jobid:this.props.jobid,process:this.state.process,step:this.state.step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:'view',shape:this.props.shape,category:this.props.category,Edit_Flag:this.state.Edit_Flag})
    }
  }

  onChangeFormatDecimal(data) {
    if(data.split(".")[1]) {
      if(data.split(".")[1].length > 3) {
        return parseFloat(data).toFixed(3)
      } else {
        return data
      }
    } else {
      return data
    }
  }

  saveLocal() {
    var value = {
      process: 2,
      step: 7,
      pile_id: this.props.pileid,
      data: {
        OverLifting: this.state.distance,
        Pcoring: this.state.pco,
        Metal: this.state.metal,
        HangingBarsLength: this.state.hanginglength,
        ImageLengthSteelCarry: this.state.images_hanging,
      }
    }
    this.props.mainPileSetStorage(this.props.pileid, "7_2", value)
  }

  render() {
    if(this.props.hidden) {
      return null
    }
    let flag_edit = this.state.Edit_Flag !=1 ? false :true
    console.warn('7_2 category',this.props.category)
    return (
      <View>
      <RNTooltips
      text={this.props.shape!=1?I18n.t('tooltip.7_2dw'):I18n.t('tooltip.7_2')}
      textSize={16}
      visible={this.state.visible}
      reference={this.main}
      tintColor="#007CC2"
    />
        {/* <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t("mainpile.7_2.topic")}</Text>
        </View>
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.7_2.length")}</Text>
          </View>
          <View style={styles.selectedView}>
            <View style={[styles.button, { height: 50, padding: 0 }, this.state.Edit_Flag == 0 && { borderColor: MENU_GREY_ITEM }]}>
              <TextInput
                editable={this.state.Edit_Flag == 1}
                keyboardType="numeric"
                value={this.state.distance}
                editable={false}
                onChangeText={distance => this.onCalculate("distance", distance)}
                underlineColorAndroid="transparent"
                style={this.state.Edit_Flag == 1 ? styles.textInputStyle : styles.textInputStyleGrey}
                onEndEditing={() => this.setState({ distance: this.state.distance ? parseFloat(this.state.distance).toFixed(3) : '',visible:false })}
              />
            </View>
          </View>
        </View>
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.7_2.height")}</Text>
          </View>
          <View style={styles.selectedView}>
            <View style={[styles.button, { height: 50, padding: 0 }, this.state.Edit_Flag == 0 && { borderColor: MENU_GREY_ITEM }]}>
              <TextInput
                editable={this.state.Edit_Flag == 1}
                keyboardType="numeric"
                value={this.state.pco.toString()}
                editable={false}
                onChangeText={pco => this.onCalculate("pco", pco)}
                underlineColorAndroid="transparent"
                style={this.state.Edit_Flag == 1 ? styles.textInputStyle : styles.textInputStyleGrey}
                onEndEditing={() => this.setState({ pco: this.state.pco ? parseFloat(this.state.pco).toFixed(3) : "",visible:false })}
              />
            </View>
          </View>
        </View>
        <View>
          <View style={[styles.boxTopic,{flexDirection:"row"}]}>
            <Text>{I18n.t("mainpile.7_2.maxlength")+" "}</Text>
            <Icon
            name="ios-information-circle"
            type="ionicon"
            color="#517fa4"
            onPress={() => {
              this.setState(
                {
                  visible: true,
                  dismiss: false,
                  reference:this.main
                })
            }}
          />
          </View>
          <View style={styles.selectedView} >
            <View style={[styles.button, { height: 50, padding: 0, borderColor: MENU_GREY_ITEM }]} ref={ref => {
              this.main = ref
            }}>
              <TextInput
                keyboardType="numeric"
                value={this.state.metal}
                onChangeText={metal => this.setState({ metal })}
                underlineColorAndroid="transparent"
                style={styles.textInputStyleGrey}
                editable={false}
              />
            </View>
          </View>
        </View> */}
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t("mainpile.5_2.topic")}</Text>
        </View>
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.7_2.length")}</Text>
          </View>
          <View style={styles.selectedView}>
            {/* <View style={[styles.button, { height: 50, padding: 0 }, { borderColor: flag_edit==false ?MENU_GREY_ITEM:MAIN_COLOR }]}> */}
            <View style={[styles.button, { height: 50, padding: 0 }, { borderColor: MENU_GREY_ITEM}]}>
              <TextInput
                ref={(input) => { this.distanceInput = input; }}
                // editable={flag_edit}
                editable={false}
                keyboardType="numeric"
                value={this.state.distance.toString()}
                onChangeText={distance => this.onCalculate("distance", distance)}
                underlineColorAndroid="transparent"
                // style={flag_edit ? styles.textInputStyle : styles.textInputStyleGrey}
                style={styles.textInputStyleGrey}
                placeholder={I18n.t("mainpile.7_2.length")}
                onEndEditing={() => {
                  this.setState({distance: isNaN(parseFloat(this.state.distance))?'':parseFloat(this.state.distance).toFixed(3)},()=>{
                    this.saveLocal()
                    
                  })
                }}
                onSubmitEditing={()=>{
                  this.pcoInput.focus()
                }}
              />
            </View>
          </View>
        </View>
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.7_2.height")}</Text>
          </View>
          <View style={styles.selectedView}>
            {/* <View style={[styles.button, { height: 50, padding: 0 },   { borderColor: flag_edit==false ?MENU_GREY_ITEM:MAIN_COLOR }]}> */}
            <View style={[styles.button, { height: 50, padding: 0 }, { borderColor: MENU_GREY_ITEM}]}>
              <TextInput
                ref={(input) => { this.pcoInput = input; }}
                // editable={flag_edit}
                editable={false}
                keyboardType="numeric"
                value={this.state.pco.toString()}
                onChangeText={pco => this.onCalculate("pco", pco)}
                underlineColorAndroid="transparent"
                // style={[flag_edit ? styles.textInputStyle : styles.textInputStyleGrey]}
                style={styles.textInputStyleGrey}
                placeholder={I18n.t("mainpile.7_2.height_holder")}
                onEndEditing={() => 
                  {
                    this.setState({pco: isNaN(parseFloat(this.state.pco))?'':parseFloat(this.state.pco).toFixed(3)},()=>{
                      this.saveLocal()

                      this.hanginglengthInput.focus()
                    })
                    
                  }
                }
                onSubmitEditing={()=>{
                  this.hanginglengthInput.focus()
                }}
              />
            </View>
          </View>
        </View>
        <View>
          <View style={[styles.boxTopic,{flexDirection:"row"}]}>
            {/* <Text>{I18n.t(this.props.category!=1&&this.props.category!=4?'mainpile.7_2.maxlength_super':"mainpile.7_2.maxlength")+" "}</Text> */}
            <Text>{I18n.t((this.props.category!=1&&this.props.category!=4)?"mainpile.7_2.maxlength_super":"mainpile.7_2.maxlength")+" "}</Text>
            <Icon
            name="ios-information-circle"
            type="ionicon"
            color="#517fa4"
            onPress={() => {
              this.setState(
                {
                  visible: true,
                  // dismiss: false,
                  reference:this.main
                },()=>{this.setState({visible: false})})
            }}
          />
          </View>
          <View style={styles.selectedView} >
            <View style={[styles.button, { height: 50, padding: 0, borderColor: MENU_GREY_ITEM }]} ref={ref => {
              this.main = ref
            }}>
              <TextInput
                keyboardType="numeric"
                value={this.state.metal}
                onChangeText={metal => this.setState({ metal })}
                underlineColorAndroid="transparent"
                style={styles.textInputStyleGrey}
                editable={false}
              />
            </View>
          </View>
          <View style={[styles.boxTopic,{justifyContent:this.props.category!=1&&this.props.category!=4 ?'center':'flex-start',
          alignItems:this.props.category!=1&&this.props.category!=4 ? 'flex-start':'center',
          flexDirection:this.props.category!=1&&this.props.category!=4 ?'column':'row'
          },]}>
            {/* <Text>{I18n.t("mainpile.5_1.hangingbar")}</Text> */}
            {/* <Text style={{fontSize:11}}>{I18n.t("mainpile.7_2.hangingbarlength_super")}</Text> */}
            <Text >{I18n.t((this.props.category!=1&&this.props.category!=4)?"mainpile.7_2.hangingbarlength_super1":"mainpile.5_1.hangingbar")+" "}</Text>
            <View style={styles.buttonImage}>
                <TouchableOpacity
                  style={[styles.image,this.state.images_hanging.length>0?{backgroundColor:'#6dcc64'}:{}]}
                  onPress={() => this.onCameraRoll("images_hanging", this.state.images_hanging,flag_edit?'edit':'view')}
                >
                  <Icon name="image" type="entypo" color="#FFF" />
                  <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                </TouchableOpacity>
            </View>
          </View>
          <View style={styles.selectedView} >
            <View style={[styles.button, { height: 50, padding: 0 }, { borderColor: flag_edit==false ?MENU_GREY_ITEM:MAIN_COLOR }]}>
              <TextInput
                ref={(input) => { this.hanginglengthInput = input; }}
                editable={flag_edit}
                keyboardType="numeric"
                value={this.state.hanginglength.toString()}
                onChangeText={hanginglength => this.setState({ hanginglength })}
                underlineColorAndroid="transparent"
                style={flag_edit ? styles.textInputStyle : styles.textInputStyleGrey}
                // placeholder={I18n.t("mainpile.5_1.hangingbarlength")}
                // placeholder={I18n.t("mainpile.7_2.hangingbarlength_super")}
                placeholder={I18n.t((this.props.category!=1&&this.props.category!=4)?"mainpile.7_2.hangingbarlength_super2":"mainpile.5_1.hangingbarlength")}
                onEndEditing={()=> {
                  console.warn('isNaN(parseFloat(this.state.hanginglength))',isNaN(parseFloat(this.state.hanginglength)))
                  this.setState({hanginglength:isNaN(parseFloat(this.state.hanginglength))?'':  parseFloat(this.state.hanginglength).toFixed(3) })
                  this.saveLocal()
                }}
              />
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(Pile7_2)
