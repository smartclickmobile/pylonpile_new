import React, {Component} from "react"
import {
  Alert,
  Text,
  View,
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  BackHandler,
  DatePickerAndroid,
  TimePickerAndroid,
} from "react-native"
import {connect} from "react-redux"
import {Container, Content} from "native-base"
import {Actions} from 'react-native-router-flux'
import * as actions from "../Actions"
import TableView from "../Components/TableView"
import {
  MAIN_COLOR,
  SUB_COLOR,
  BASIC_COLOR,
  BLUE_COLOR,
  MENU_GREY_ITEM,
  BUTTON_COLOR,
  DEFAULT_COLOR_1,
  DEFAULT_COLOR_4
} from "../Constants/Color"
import {Icon} from "react-native-elements"
import I18n from '../../assets/languages/i18n'
import ModalSelector from "react-native-modal-selector"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import styles from './styles/DrillPileInsert.style'
import moment from 'moment'
import DateTimePicker from '@react-native-community/datetimepicker';
class DrillPileInsert extends Component {
  constructor(props) {
    super(props)
    this.state = {
      driver:'',
      machine:'',
      drilltype:'',
      drilltool:'',
      powerpack:'',
      MachineCraneId:'',
      DriverId:'',
      DrillTypeId:'',
      DrillToolId:'',
      PowerPackId:'',
      machineData:[{ key: 0, section: true, label: I18n.t("mainpile.2_1.machineselect") }],
      driverData:[{ key: 0, section: true, label: I18n.t("mainpile.2_1.driverselect")  }],
      drillTypeData:[{ key: 0, section: true, label: this.props.shape==1?I18n.t('mainpile.drillpileinsert.selectdrill1'): I18n.t('mainpile.drillpileinsert_dw.selectdrill1') }],
      drillToolData:[{ key: 0, section: true, label: this.props.shape==1?I18n.t('mainpile.drillpileinsert.selectdrilltool'): I18n.t('mainpile.drillpileinsert_dw.selectdrilltool') }],
      powerPackData:[{ key: 0, section: true, label:  this.props.shape==1?I18n.t('mainpile.drillpileinsert.selectpowerpack'): I18n.t('mainpile.drillpileinsert_dw.selectpowerpack') }],
      StartDrilling:'',
      EndDrilling:'',
      IsBreak:false,
      IsBreakDisable:false,
      Edit_Flag:1,
      Depth:'',
      pile:[],
      drilltoolDisable:true,
      powerpackDisable:true,
      Images:[],
      No:1,
      PowerPackId:'',
      DrillingToolType:'',
      DrillingTool:'',
      PowerPack:'',
      Crane:'',
      dataMaster:'',
      dataValue:'',
      catch:false,
      Type:'',
      notstopDisable:false,
      status5:0,
      status6:0,
      CrossFlag:'',
      AfterDepth:'',
      BeforeDepth:'',
      ChangeDrillingFluidAfterDepth:'',
      ChangeDrillingFluidBeforeDepth:'',
      status8:0,
      status10:0,
      text:'',

      BucketCuttingEdgeWidth:'',
      ImageBucketCuttingEdgeWidths:[],

      datevisibledate:false,
      datevisibletime:false,
      date:'',

      percentageCheckDepth:''
    }
  }
  // this.setState({load: false}, () => {
  //
  // })
  async componentWillReceiveProps(nextProps){
    if(nextProps.action == "update"){
      this.setState(nextProps.value.data)
    }
    if (nextProps.status == 1 && this.state.catch == false) {
      this.setState({catch:true})
      console.log('this.state.No',this.state.No)
      
    }
  }
  componentWillMount(){
    this.props.clearError()
    // console.warn('shape',this.props.shape)
    if(this.props.shape == 1){
      this.setState({text:'mainpile.drillpileinsert'})
    }else{
      this.setState({text:'mainpile.drillpileinsert_dw'})
    }
  }

  async componentDidMount() {
      BackHandler.addEventListener("back", this._onBack)
      var pile = null
      var pile5_0 = null
      var pile6_0 = null
      var pile6_1 = null
      var pile6_2 = null
      var pileMaster = null
      var pile_page = ''
      if (this.props.item != null) {
         this.setState({pile:this.props.item})
      }
      if(this.props.child == false){
        pile_page = '5_3'
      }else{
        pile_page = '5_5'
      }
      // console.warn('pile_page',pile_page)
      if(this.props.pileAll[this.props.pileid] && this.props.pileAll[this.props.pileid][pile_page]) {
        pile = this.props.pileAll[this.props.pileid][pile_page].data
        pileMaster = this.props.pileAll[this.props.pileid][pile_page].masterInfo
        console.log('pileMaster',pileMaster)
        this.setState({dataMaster:pileMaster,dataValue:pile})

      if(this.props.pileAll[this.props.pileid] && this.props.pileAll[this.props.pileid]['5_0']) {
        pile5_0 = this.props.pileAll[this.props.pileid]['5_0'].data
        // console.warn('status',this.props.pileAll[this.props.pileid].step_status)
        this.setState({status5:this.props.pileAll[this.props.pileid].step_status.Step5Status,status6:this.props.pileAll[this.props.pileid].step_status.Step6Status,status8:this.props.pileAll[this.props.pileid].step_status.Step8Status,status10:this.props.pileAll[this.props.pileid].step_status.Step10Status})
      }
      if(this.props.pileAll[this.props.pileid] && this.props.pileAll[this.props.pileid]['6_1']) {
        pile6_1 = this.props.pileAll[this.props.pileid]['6_1'].data
        this.setState({CrossFlag:pile6_1.CrossFlag,ChangeDrillingFluidAfterDepth:pile6_1.ChangeDrillingFluidAfterDepth,ChangeDrillingFluidBeforeDepth:pile6_1.ChangeDrillingFluidBeforeDepth})
      }
      if(this.props.pileAll[this.props.pileid] && this.props.pileAll[this.props.pileid]['6_2']) {
        pile6_2 = this.props.pileAll[this.props.pileid]['6_2'].data
        this.setState({AfterDepth:pile6_2.AfterDepth,BeforeDepth:pile6_2.BeforeDepth})
      }
      
      if (pile) {
        var drillinglist_list = []
        if(this.props.child == true){
          drillinglist_list = this.props.drillinglistChild
        }else{
          drillinglist_list = this.props.drillinglist
        }
        if (drillinglist_list != undefined) {
          if (drillinglist_list.length > 0 ) {
            console.log(drillinglist_list[drillinglist_list.length - 1].PowerPack)
             await this.setState({
               IsBreak:drillinglist_list[drillinglist_list.length - 1].IsBreak,
               No:drillinglist_list.length + 1,
               MachineCraneId:drillinglist_list[drillinglist_list.length - 1].Crane.itemid,
               DriverId:drillinglist_list[drillinglist_list.length - 1].DriverId,
               DrillTypeId:drillinglist_list[drillinglist_list.length - 1].DrillingToolType == null ? '' : drillinglist_list[drillinglist_list.length - 1].DrillingToolType.Id,
               DrillToolId:drillinglist_list[drillinglist_list.length - 1].DrillingTool == null ? '' : drillinglist_list[drillinglist_list.length - 1].DrillingTool.itemid,
               DrillingToolType:drillinglist_list[drillinglist_list.length - 1].DrillingToolType,
               DrillingTool:drillinglist_list[drillinglist_list.length - 1].DrillingTool,
               Crane:drillinglist_list[drillinglist_list.length - 1].Crane,
               PowerPack:drillinglist_list[drillinglist_list.length - 1].PowerPack,
               PowerPackId:drillinglist_list[drillinglist_list.length - 1].PowerPack == null ? '' : drillinglist_list[drillinglist_list.length - 1].PowerPack.itemid,
             })
             drillinglist_list.map((data) =>{
               if (data.IsBreak == true && this.state.IsBreakDisable) {
                 this.setState({IsBreakDisable:true,})
               }
             })
          }else {
            // this.setState({IsBreakDisable:false})
          }
        }
        if (this.props.DrillingListData != undefined && Actions.currentScene == 'drillPileInsert') {
          // console.warn('test',this.props.DrillingListData.drilltool)
           this.setState({
            StartDrilling:moment(this.props.DrillingListData.StartDrilling,'DD/MM/YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'),
            EndDrilling:this.props.DrillingListData.EndDrilling!=''?moment(this.props.DrillingListData.EndDrilling,'DD/MM/YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'):this.props.DrillingListData.EndDrilling,
            driver:this.props.DrillingListData.driver,
            machine:this.props.DrillingListData.Crane.machine_no,
            drilltype:this.props.DrillingListData.DrillingToolType == null ? '' : this.props.DrillingListData.DrillingToolType.Name,
            drilltool:this.props.DrillingListData.drilltool == undefined?'':this.props.DrillingListData.drilltool,
            powerpack:this.props.DrillingListData.powerpack,
            MachineCraneId:this.props.DrillingListData.Crane.itemid,
            DriverId:this.props.DrillingListData.DriverId,
            DrillTypeId:this.props.DrillingListData.DrillingToolType == null ? '' : this.props.DrillingListData.DrillingToolType.Id,
            DrillToolId:this.props.DrillingListData.DrillingTool == null ? '' : this.props.DrillingListData.DrillingTool.itemid,
            PowerPackId:this.props.DrillingListData.PowerPack == null ? '' : this.props.DrillingListData.PowerPack.itemid,
            IsBreak:this.props.DrillingListData.IsBreak,
            // Depth:isNaN(parseFloat(this.props.DrillingListData.Depth).toFixed(3)) || this.props.DrillingListData.Depth == 0 ? '0.000' : parseFloat(this.props.DrillingListData.Depth).toFixed(3),
            Depth: this.props.DrillingListData.Depth === null? '':this.props.DrillingListData.Depth == 0 ? '0.000' : parseFloat(this.props.DrillingListData.Depth).toFixed(3),
            drilltoolDisable:this.props.DrillingListData.drilltoolDisable,
            Images:this.props.DrillingListData.Images == undefined ? this.props.DrillingListData.Images : this.props.DrillingListData.Images,
            ImageBucketCuttingEdgeWidths: this.props.DrillingListData.ImageBucketCuttingEdgeWidths == undefined ? [] : this.props.DrillingListData.ImageBucketCuttingEdgeWidths,
            BucketCuttingEdgeWidth:this.props.DrillingListData.BucketCuttingEdgeWidth == null ? '' : parseFloat(this.props.DrillingListData.BucketCuttingEdgeWidth).toFixed(3),
            No:this.props.DrillingListData.No,
            DrillingToolType:this.props.DrillingListData.DrillingToolType,
            DrillingTool:this.props.DrillingListData.DrillingTool,
            Crane:this.props.DrillingListData.Crane,
            PowerPack:this.props.DrillingListData.PowerPack,
            Edit_Flag:this.props.Edit_Flag,
            notstopDisable:this.props.DrillingListData.EndDrilling==''?false:this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling == ''?true:false
          })
        }
      }


      if (pile && Actions.currentScene == 'drillPileInsert') {
        // console.warn('pileMaster.DrillingToolTypeList',pileMaster.DrillingToolTypeList)
        if ( this.state.drillTypeData != pileMaster.DrillingToolTypeList && pileMaster.DrillingToolTypeList != '' && this.state.drillTypeData.length == 1) {
          let list = [{ key: 0, section: true, label: I18n.t(this.state.text+'.selectdrill1') }]
          for (var i = 0; i < pileMaster.DrillingToolTypeList.length; i++) {
            if (this.state.DrillTypeId == pileMaster.DrillingToolTypeList[i].Id  ) {
              this.setState({DrillTypeId:pileMaster.DrillingToolTypeList[i].Id,drilltype:pileMaster.DrillingToolTypeList[i].Name})
            }
            await list.push({
              key: pileMaster.DrillingToolTypeList[i].Id,
              label: pileMaster.DrillingToolTypeList[i].Name,
              index:i,
            })
          }
          await this.setState({drillTypeData:list})
        }

        if ( this.state.drillToolData != pileMaster.DrillingToolList && pileMaster.DrillingToolList != '' && this.state.drillToolData.length == 1) {
          let list = [{ key: 0, section: true, label: I18n.t(this.state.text+'.selectdrilltool') }]
          for (var i = 0; i < pileMaster.DrillingToolList.length; i++) {
            if (this.state.DrillToolId == pileMaster.DrillingToolList[i].itemid) {
              this.setState({
                DrillToolId: pileMaster.DrillingToolList[i].itemid,
                drilltool: pileMaster.DrillingToolList[i].machine_no,
                powerpackDisable: pileMaster.DrillingToolList[i].powerpack_flag == true ? false : true,
              })
            }
            await list.push({
              key: pileMaster.DrillingToolList[i].itemid,
              label: pileMaster.DrillingToolList[i].machine_no,
              powerpack_flag:pileMaster.DrillingToolList[i].powerpack_flag,
              index:i,
            })
          }
          await this.setState({drillToolData:list})
        }

        if ( this.state.machineData != pileMaster.CraneList && pileMaster.CraneList != '' && this.state.machineData.length == 1) {
          let list = [{ key: 0, section: true, label: I18n.t("mainpile.2_1.machineselect") }]
          for (var i = 0; i < pileMaster.CraneList.length; i++) {
              if (pileMaster.CraneList[i].itemid == this.state.MachineCraneId) {
                
                this.setState({
                  drilltoolDisable: pileMaster.CraneList[i].drill_flag == true ? false : true,
                  machine:pileMaster.CraneList[i].machine_no,
                  MachineId: pileMaster.CraneList[i].itemid,
                })
              }
            await list.push({
              key: pileMaster.CraneList[i].itemid,
              label: pileMaster.CraneList[i].machine_no,
              typeid: pileMaster.CraneList[i].itemtype_id,
              drill_flag : pileMaster.CraneList[i].drill_flag,
              index: i,
            })
          }
          await this.setState({machineData:list})
          // console.log(this.state.machineData);
        }
        //       driverData.push({ key: index + 1, label: driver.firstname + " " + driver.lastname, id: driver.employee_id })
        if (this.state.driverData != pileMaster.DriverList && pileMaster.DriverList != '' && this.state.driverData.length == 1 ) {
          let list = [{ key: 0, section: true, label: I18n.t("mainpile.2_1.driverselect")  }]
          for (var i = 0; i < pileMaster.DriverList.length; i++) {
            if (pileMaster.DriverList[i].employee_id == this.state.DriverId) {
              this.setState({
                driver:(pileMaster.DriverList[i].nickname && pileMaster.DriverList[i].nickname + "-") + pileMaster.DriverList[i].firstname + " " + pileMaster.DriverList[i].lastname,
                DriverId:pileMaster.DriverList[i].employee_id
              })
            }
             await list.push({
              key: pileMaster.DriverList[i].employee_id,
              label: (pileMaster.DriverList[i].nickname && pileMaster.DriverList[i].nickname + "-") + pileMaster.DriverList[i].firstname + " " + pileMaster.DriverList[i].lastname
            })
          }
          await this.setState({driverData:list})
        }
        if (this.state.pile.shape == 2 && this.state.powerPackData != pileMaster.PowerPackList && pileMaster.PowerPackList != '' && this.state.powerPackData.length == 1) {
          let list = [{ key: 0,section: true,label: I18n.t(this.state.text+".selectpowerpack")}]
          for (var i = 0; i < pileMaster.PowerPackList.length; i++) {
            if (this.state.PowerPackId == pileMaster.PowerPackList[i].itemid) {
              this.setState({PowerPackId: pileMaster.PowerPackList[i].itemid, powerpack: pileMaster.PowerPackList[i].machine_no})
            }
            await list.push({key: pileMaster.PowerPackList[i].itemid, label: pileMaster.PowerPackList[i].machine_no, index: i})
          }
          await this.setState({powerPackData: list})
        }
        if(pileMaster&&pileMaster.PileInfo &&pileMaster.PileInfo.PercentageCheckDepth&&this.props.Depthneed){
          let goal = parseFloat(this.props.Depthneed.toString()).toFixed(3)
         let value =(1+pileMaster.PileInfo.PercentageCheckDepth)*goal
          console.log('pileMaster.PileInfo',value)
          this.setState({percentageCheckDepth: value})
        }
      }
    }

  }
  componentWillUnmount() {
    BackHandler.removeEventListener("back", this._onBack)
  }
  _onBack(){
    Actions.pop()
    return true
  }

  selectedDrillType =  async drilltype => {
    await this.setState({ drilltype: drilltype.label,DrillTypeId:drilltype.key,DrillingToolType:this.state.dataMaster.DrillingToolTypeList[drilltype.index]})
  }
  selectedMachine =  async machine => {
    if( machine.label == this.state.machine){

    }else{
      console.log('selectedMachine',machine)
      await this.setState({ 
        machine: machine.label,
        MachineCraneId:machine.key, 
        drilltoolDisable: machine.drill_flag == true ? false : true,
        Crane:this.state.dataMaster.CraneList[machine.index],
        drilltool:'',
        DrillToolId:'',
        powerpackDisable:true,
        powerpack:'',
        PowerPackId: ''
      },()=>{
        // console.warn(this.state.drilltoolDisable)
        // if(this.state.drilltoolDisable == true){
        //   this.setState({drillToolData:[{ key: 0, section: true, label: I18n.t(this.state.text+'.selectdrilltool')  }],drilltool:'',DrillToolId:''})
        // }
  
      })
    }
    
  }

  selectedDriver = async driver => {
    await this.setState({ driver: driver.label,DriverId: driver.key })
  }
  selectedDrillTool = async drilltool => {
    if(drilltool.label == this.state.drilltool){

    }else{
      await this.setState({ 
        drilltool: drilltool.label,
        DrillToolId: drilltool.key,
        DrillingTool:this.state.dataMaster.DrillingToolList[drilltool.index],
        powerpackDisable: drilltool.powerpack_flag == true ? false : true ,
        powerpack:'',
        PowerPackId: ''
      })

    }
  }

  selectedPowerPack= async powerpack => {
    await this.setState({ powerpack: powerpack.label,PowerPackId: powerpack.key,PowerPack:this.state.dataMaster.PowerPackList[powerpack.index] })
   
  }

  onChangeFormatDecimal(data){
    if (data.split(".")[1]) {
      if (data.split(".")[1].length > 3) {
        return parseFloat(data).toFixed(3)
      }else {
        return data
      }
    }else {
      return data
    }
  }

  onCameraRoll(keyname,photos,type){
    console.log(photos)
    if (this.state.Edit_Flag == 1) {
      Actions.camearaRoll({jobid:this.props.jobid,process:this.state.process,step:this.state.step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:type,shape:this.props.shape,category: this.props.category,Edit_Flag:this.state.Edit_Flag})
    }else {
      Actions.camearaRoll({jobid:this.props.jobid,process:this.state.process,step:this.state.step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:'view',shape:this.props.shape, category: this.props.category,Edit_Flag:this.state.Edit_Flag})
    }
  }

  setTime(type){
    let bucketcutting_flag = true
    let bucketcutting_count = 0
    let bucketcutting_index = ''
    for(var f = 0 ; f < this.props.DrillingListlist.length ; f++){
      if(this.props.DrillingListlist[f].DrillingToolType.Id == 2){
        bucketcutting_count++
        bucketcutting_index = f
      }
    }
    if(bucketcutting_count == 0){
      bucketcutting_flag = true
    }else if(bucketcutting_count == 1){
      if(this.props.index == bucketcutting_index){
        bucketcutting_flag = true
      }else{
        bucketcutting_flag = false
      }
    }else{
      bucketcutting_flag = false
    }
    if (this.state.drilltype == '') {
      Alert.alert(I18n.t(this.state.text+'.selectdrill'))
    }else if(
      this.state.ImageBucketCuttingEdgeWidths.length == 0 && 
      this.state.DrillTypeId == 2 && 
      bucketcutting_flag == true &&
      (this.props.category == 1 || this.props.category == 4 || this.props.category == 5)
      ){
        Alert.alert(I18n.t(this.state.text+'.error_bucketcutting_image'))
    }else if(
      this.state.BucketCuttingEdgeWidth == '' && 
      this.state.DrillTypeId == 2 && 
      bucketcutting_flag == true &&
      (this.props.category == 1 || this.props.category == 4 || this.props.category == 5)
      ){
        Alert.alert(I18n.t(this.state.text+'.error_bucketcutting'))
    }else if(this.state.machine == ''){
      Alert.alert(I18n.t(this.state.text+'.selectMachine'))
    }
    else if(this.props.shape == 2 || this.props.shape == 3){
      if(this.state.drilltoolDisable == false){
        if(this.state.drilltool == ''){
          Alert.alert(I18n.t(this.state.text+'.selectdrilltool1'))
        }else if(this.state.powerpack == '' && !this.state.powerpackDisable){
          Alert.alert(I18n.t(this.state.text+'.selectpowerpack'))
        }
        else if (this.state.driver == '') {
          Alert.alert(I18n.t(this.state.text+'.selectdriver'))
        }else {
          var title = ''
          if (type == 'start') {
            title = I18n.t(this.state.text+'.starttime')
          }else {
            title = I18n.t(this.state.text+'.endtime')
          }
          Alert.alert('', title, [
            {
              text: 'Cancel'
            },
            {
              text: 'OK',
              onPress: ()=>{
                var now = moment().format("DD/MM/YYYY HH:mm")
                if (type == 'start') {
                  this.setState({StartDrilling:now})
                }else {
                  if (this.state.EndDrilling == '') {
                    this.setState({EndDrilling:now})
                  }
                }
              }
            }
          ])
        }
      } else if(this.state.powerpack == ''&& !this.state.powerpackDisable){
        Alert.alert(I18n.t(this.state.text+'.selectpowerpack'))
      }
      else if (this.state.driver == '') {
        Alert.alert(I18n.t(this.state.text+'.selectdriver'))
      }
      else {
        var title = ''
        if (type == 'start') {
          title = I18n.t(this.state.text+'.starttime')
        }else {
          title = I18n.t(this.state.text+'.endtime')
        }
        Alert.alert('', title, [
          {
            text: 'Cancel'
          },
          {
            text: 'OK',
            onPress: ()=>{
              var now = moment().format("DD/MM/YYYY HH:mm")
              if (type == 'start') {
                this.setState({StartDrilling:now})
              }else {
                if (this.state.EndDrilling == '') {
                  this.setState({EndDrilling:now})
                }
              }
            }
          }
        ])
      }
    }
    else if(this.state.drilltool == '' && this.state.drilltoolDisable == false){
      Alert.alert(I18n.t(this.state.text+'.selectdrilltool'))
    }
    else if (this.state.driver == '') {
      Alert.alert(I18n.t(this.state.text+'.selectdriver'))
    }
    else {
      var title = ''
      if (type == 'start') {
        title = I18n.t(this.state.text+'.starttime')
      }else {
        title = I18n.t(this.state.text+'.endtime')
      }
      Alert.alert('', title, [
        {
          text: 'Cancel'
        },
        {
          text: 'OK',
          onPress: ()=>{
            var now = moment().format("DD/MM/YYYY HH:mm")
            if (type == 'start') {
              this.setState({StartDrilling:now})
            }else {
              if (this.state.EndDrilling == '') {
                this.setState({EndDrilling:now})
              }
            }
          }
        }
      ])
    }
  }

  async selectedIsBreak(){
    if (this.state.Edit_Flag == 1 && this.state.IsBreakDisable == false  && this.state.EndDrilling != '') {
      await this.setState({ IsBreak: !this.state.IsBreak })
    }
  }

  setCalendar2(date,type){
    let date2 = moment(date).format("DD/MM/YYYY HH:mm")
    // let hour = date.getHours()
    // let minute = date.getMinutes()
    if (type == 'start') {
      this.onCalDate(date2,type)
    }else {
      this.onCalDate(date2,type)
    }
  }

  async setCalendar(type){
    var month =  moment().format('M')
    month = month != 1 ? month : parseInt(month) - 1
    try {
      const {action, year, month, day} = await DatePickerAndroid.open({
        // Use `new Date()` for current date.
        // May 25 2020. Month 0 is January.
        date: new Date(moment().format('YYYY'),month,moment().format('D'))
      })
      if (action !== DatePickerAndroid.dismissedAction) {
        // Selected year, month (0-11), day
        var date = new Date(year, month, day)
        date = moment(date).format("DD/MM/YYYY")

        try {
            const {action, hour, minute} = await TimePickerAndroid.open({
              is24Hour: true,
            })
            if (action !== TimePickerAndroid.dismissedAction) {
              // Selected hour (0-23), minute (0-59) datavalue

              if (type == 'start') {
                // this.setState({StartDrilling:date +' '+moment(hour+':'+minute,'HH:mm').format("HH:mm:ss")}
                this.onCalDate(date +' '+moment(hour+':'+minute,'HH:mm').format("HH:mm:ss"),type)

              }else {
                // this.setState({EndDrilling:date +' '+moment(hour+':'+minute,'HH:mm').format("HH:mm:ss")}
                this.onCalDate(date +' '+moment(hour+':'+minute,'HH:mm').format("HH:mm:ss"),type)
              }
            }
          } catch ({code, message}) {
            // console.warn('Cannot open time picker', message)
          }
      }
    } catch ({code, message}) {
      // console.warn('Cannot open date picker', message)
    }
  }

  onCalDate(date,type){
    if(this.props.DrillingListData != undefined){
    if (this.props.DrillingListData.length == this.props.DrillingListData.No && this.props.DrillingListData.No != 1 ) {
      // console.warn('test1')
      var index = (parseInt(this.props.DrillingListData.No) - 1) - 1
      if (type == 'start') {
          var a = moment(date,'DD-MM-YYYY HH:mm:ss')
          var b = moment(this.props.DrillingListData[index].StartDrilling,'DD-MM-YYYY HH:mm:ss')
          var check = false

          //after
          if (a.diff(b, 'years',true) >= 0 && a.diff(b, 'months',true) >=0 && b.diff(a, 'days',true) <= 0 && b.diff(a, 'hours',true) <= 0 && b.diff(a, 'minutes',true) <= 0) {
            if (this.state.EndDrilling != '') {
              var c = moment(this.state.EndDrilling,'DD-MM-YYYY HH:mm:ss')
              //before
              if (a.diff(c, 'years',true) <= 0 && a.diff(c, 'months',true) <=0 && c.diff(a, 'days',true) >= 0 && c.diff(a, 'hours',true) >= 0 && c.diff(a, 'minutes',true) >= 0) {
                check = true
                this.setState({StartDrilling:date})
              }
            }else {
              check = true
              this.setState({StartDrilling:date})
            }
          }

          if (check == false) {
            Alert.alert('', I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'), [
              {
                text: 'OK'
              }
            ])
          }
        }else {
          var a = moment(date,'DD-MM-YYYY HH:mm:ss')
          var b = moment(this.state.StartDrilling,'DD-MM-YYYY HH:mm:ss')
          var check = false

          if (a.diff(b, 'years',true) >= 0 && a.diff(b, 'months',true) >=0 && b.diff(a, 'days',true) <= 0 && b.diff(a, 'hours',true) <= 0 && b.diff(a, 'minutes',true) <= 0) {
              check = true
              this.setState({EndDrilling:date})
          }
          if (check == false) {
            Alert.alert('', I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'), [
              {
                text: 'OK'
              }
            ])
          }
        }
      // this.props.DrillingListData.No - 1
    }else if(this.props.DrillingListData.No != 1 && this.props.DrillingListData.No != this.props.DrillingListlist[this.props.DrillingListlist.length-1].No){
      // console.warn('test2')
      var index = (parseInt(this.props.DrillingListData.No) - 1) - 1
      var indexPlus = (parseInt(this.props.DrillingListData.No) - 1) + 1
      if (type == 'start') {
          var a = moment(date,'DD-MM-YYYY HH:mm:ss')
          var b = moment(this.props.DrillingListlist[index].StartDrilling,'DD-MM-YYYY HH:mm:ss')
          var check = false
          var error = []
          //after
          if(a < moment(this.state.EndDrilling,'DD-MM-YYYY HH:mm:ss')){
            if (a>b) {
              var c = moment(this.props.DrillingListlist[index].EndDrilling,'DD-MM-YYYY HH:mm:ss')
              // console.log(c);

              if (a>c) {

                var d = moment(this.props.DrillingListlist[indexPlus].StartDrilling,'DD-MM-YYYY HH:mm:ss')
                //before
                if (a<d) {
                  var e = moment(this.props.DrillingListlist[indexPlus].EndDrilling,'DD-MM-YYYY HH:mm:ss')
                  if (a<e) {

                    if (this.state.EndDrilling != '') {
                      var f = moment(this.state.EndDrilling,'DD-MM-YYYY HH:mm:ss')
                      if (a<f) {
                        check = true
                        this.setState({StartDrilling:date})
                      }
                    }else {
                      check = true
                      this.setState({StartDrilling:date})
                    }
                  }
                }
              }else{
                error.push(I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[index].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'))
              }
          }else{
            error.push(I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[index].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'))
          }
          }else{
            error.push(I18n.t(this.state.text+'.startendnotmat'))
          }
          

          if (check == false) {
            Alert.alert('', error[0], [
              {
                text: 'OK'
              }
            ])
          }
        }else {
          var a = moment(date,'DD-MM-YYYY HH:mm:ss')
          
          var check = false
          var error = []
          //after
          if(a > moment(this.state.StartDrilling,'DD-MM-YYYY HH:mm:ss')){
              //before
                var d = moment(this.props.DrillingListlist[indexPlus].StartDrilling,'DD-MM-YYYY HH:mm:ss')
                if (a < moment(this.props.DrillingListlist[indexPlus].StartDrilling,'DD-MM-YYYY HH:mm:ss')) {
                  var e = moment(this.props.DrillingListlist[indexPlus].EndDrilling,'DD-MM-YYYY HH:mm:ss')
                  // console.warn('sss',this.props.DrillingListlist[indexPlus].EndDrilling)
                  if(this.props.DrillingListlist[indexPlus].EndDrilling != '' && !this.state.notstopDisable){
                    if (a<e) {
                      if (this.state.StartDrilling != '') {
                        var f = moment(this.state.StartDrilling,'DD-MM-YYYY HH:mm:ss')
                        if (a>f) {
                          check = true
                          this.setState({EndDrilling:date})
                        }
                      }else {
                        check = true
                        this.setState({EndDrilling:date})
                      }
                    }else{
                      // error.push('testtesttest')
                      error.push(I18n.t(this.state.text+'.enddatefail')+` `+I18n.t(this.state.text+'.before')+moment(this.props.DrillingListlist[indexPlus].StartDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'))
                    }
                  }else{
                    // console.warn('null')
                    check=true
                    this.setState({EndDrilling:date})
                  }
                  
                }else{
                  error.push(I18n.t(this.state.text+'.enddatefail')+` `+I18n.t(this.state.text+'.before')+moment(this.props.DrillingListlist[indexPlus].StartDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'))
                }
              
          }else{
            error.push(I18n.t(this.state.text+'.startendnotmat'))
          }
          

          if (check == false) {
            Alert.alert('', error[0], [
              {
                text: 'OK'
              }
            ])
          }
        }

      // this.props.DrillingListData.No - 1
      // this.props.DrillingListData.No + 1
    }else if(this.props.DrillingListData.No == 1 && this.props.DrillingListData.length > 1){
      // console.warn('test3')
      var index = parseInt(this.props.DrillingListData.No)
      if (type == 'start') {
          var a = moment(date,'DD-MM-YYYY HH:mm:ss')
          var b = moment(this.props.DrillingListData[index].StartDrilling,'DD-MM-YYYY HH:mm:ss')
          var check = false

          if (a.diff(b, 'years',true) <= 0 && a.diff(b, 'months',true) <=0 && b.diff(a, 'days',true) >= 0 && b.diff(a, 'hours',true) >= 0 && b.diff(a, 'minutes',true) >= 0) {
            if (this.state.EndDrilling != '') {
              var c = moment(this.state.EndDrilling,'DD-MM-YYYY HH:mm:ss')
              if (a.diff(c, 'years',true) <= 0 && a.diff(c, 'months',true) <=0 && c.diff(a, 'days',true) >= 0 && c.diff(a, 'hours',true) >= 0 && c.diff(a, 'minutes',true) >= 0) {
                check = true
                this.setState({StartDrilling:date})
              }
            }else {
              check = true
              this.setState({StartDrilling:date})
            }
          }

          if (check == false) {
            Alert.alert('test3', I18n.t(this.state.text+'.startdatefail'), [
              {
                text: 'OK'
              }
            ])
          }
        }else {
          var a = moment(date,'DD-MM-YYYY HH:mm:ss')
          var b = moment(this.state.StartDrilling,'DD-MM-YYYY HH:mm:ss')
          var check = false

          if (a.diff(b, 'years',true) >= 0 && a.diff(b, 'months',true) >=0 && b.diff(a, 'days',true) <= 0 && b.diff(a, 'hours',true) <= 0 && b.diff(a, 'minutes',true) <= 0) {
            var c = moment(this.props.DrillingListData[index].StartDrilling,'DD-MM-YYYY HH:mm:ss')

            if (a.diff(c, 'years',true) <= 0 && a.diff(c, 'months',true) <=0 && c.diff(a, 'days',true) >= 0 && c.diff(a, 'hours',true) >= 0 && c.diff(a, 'minutes',true) >= 0) {
              check = true
              console.log(date)
              this.setState({EndDrilling:date})
            }
          }
          if (check == false) {
            Alert.alert('', I18n.t(this.state.text+'.startendnotmat'), [
              {
                text: 'OK'
              }
            ])
          }
        }
      }else if(this.props.index == 0 && this.props.DrillingListlist.length>1){
        // console.warn('test4')
        var indexPlus = (parseInt(this.props.DrillingListData.No) - 1) + 1
        var error = []
        if(type == 'start'){
          var check = false
          var a = moment(date,'DD-MM-YYYY HH:mm:ss')
          if(a < moment(this.state.EndDrilling,'DD-MM-YYYY HH:mm:ss')){ 
            check = true
            this.setState({StartDrilling:date})
          }else{
            error.push(I18n.t(this.state.text+'.startendnotmat'))
          }

          if (check == false) {
            Alert.alert('', error[0], [
              {
                text: 'OK'
              }
            ])
          }
        }else{
          var a = moment(date,'DD-MM-YYYY HH:mm:ss')
          var b = moment(this.props.DrillingListlist[indexPlus].StartDrilling,'DD-MM-YYYY HH:mm:ss')
          var check = false
          if(a > moment(this.state.StartDrilling,'DD-MM-YYYY HH:mm:ss')){
            if(a < b){
              check = true
              this.setState({EndDrilling:date})
            }else{
              error.push(I18n.t(this.state.text+'.enddatefail')+` `+I18n.t(this.state.text+'.before')+moment(this.props.DrillingListlist[indexPlus].StartDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'))
            }
            
          }else{
            error.push(I18n.t(this.state.text+'.startendnotmat'))
          }

          if (check == false) {
            Alert.alert('', error[0], [
              {
                text: 'OK'
              }
            ])
          }
          
        }
        }else if(this.props.index == 0 && this.props.DrillingListlist.length==1){
          // console.warn('test7')
          var indexPlus = (parseInt(this.props.DrillingListData.No) - 1) + 1
          var error = []
          if(type == 'start'){
            if(this.state.EndDrilling != ''){
              var check = false
              var a = moment(date,'DD-MM-YYYY HH:mm:ss')
              if(a < moment(this.state.EndDrilling,'DD-MM-YYYY HH:mm:ss')){ 
                check = true
                this.setState({StartDrilling:date})
              }else{
                error.push(I18n.t(this.state.text+'.startendnotmat'))
              }
              if (check == false) {
                Alert.alert('', error[0], [
                  {
                    text: 'OK'
                  }
                ])
              }
            }else{
              this.setState({StartDrilling:date})
            }
            
  
            
          }else{
            var a = moment(date,'DD-MM-YYYY HH:mm:ss')
           
            var check = false
            if(a > moment(this.state.StartDrilling,'DD-MM-YYYY HH:mm:ss')){
              check = true
                this.setState({EndDrilling:date})
              
            }else{
              error.push(I18n.t(this.state.text+'.startendnotmat'))
            }
  
            if (check == false) {
              Alert.alert('', error[0], [
                {
                  text: 'OK'
                }
              ])
            }
            
          }
      }else if(this.props.index == this.props.DrillingListlist.length-1){
        // console.warn('test6')
        var check = false
        var index = (parseInt(this.props.DrillingListData.No) - 1) - 1
        var a = moment(date,'DD-MM-YYYY HH:mm:ss')
        var b = moment(this.props.DrillingListlist[index].EndDrilling,'DD-MM-YYYY HH:mm:ss')
        var error = []
        if(type == 'start'){
          if(this.state.EndDrilling != ''){
            if(a < moment(this.state.EndDrilling,'DD-MM-YYYY HH:mm:ss')){
              if(a>b){
                check = true
                this.setState({StartDrilling:date})
              }else{
                error.push(I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[index].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'))
              }
            }else{
              error.push(I18n.t(this.state.text+'.startendnotmat'))
            }
  
            if (check == false) {
              Alert.alert('', error[0], [
                {
                  text: 'OK'
                }
              ])
            }
          }else{
            if(a>b){
              check = true
              this.setState({StartDrilling:date})
            }else{
              error.push(I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[index].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'))
            }
            if (check == false) {
              Alert.alert('', error[0], [
                {
                  text: 'OK'
                }
              ])
            }
          }
          
        }else{
          var a = moment(date,'DD-MM-YYYY HH:mm:ss')
          var check = false
          if(a > moment(this.state.StartDrilling,'DD-MM-YYYY HH:mm:ss')){
            check = true
            this.setState({EndDrilling:date})
          }else{
            error.push(I18n.t(this.state.text+'.startendnotmat'))
          }

          if (check == false) {
            Alert.alert('', error[0], [
              {
                text: 'OK'
              }
            ])
          }
          
        }
        
      }else {
        // console.warn('test5')
        if (type == 'start') {
          this.setState({StartDrilling:date})
        }else {
          this.setState({EndDrilling:date})
        }

      }
    }else{
      // console.warn('test8')
      if(this.state.Type == 'start'){
        if(this.props.DrillingListlist.length > 0){
          var a = moment(date,'DD-MM-YYYY HH:mm:ss')
          var b = moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].StartDrilling,'DD-MM-YYYY HH:mm:ss')
          var c = moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,'DD-MM-YYYY HH:mm:ss')
          if(this.state.EndDrilling != ''){
            var b = moment(this.state.EndDrilling,'DD-MM-YYYY HH:mm:ss')
            if(a < b){
              if(c<a){
                this.setState({StartDrilling:date})
              }else{
                Alert.alert('', I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'), [
                  {
                    text: 'OK'
                  }
                ])
              }
            }else{
              Alert.alert('', I18n.t(this.state.text+'.startendnotmat'), [
                {
                  text: 'OK'
                }
              ])
            }
            
          }else{
            if(c<a){
              this.setState({StartDrilling:date})
            }else{
              Alert.alert('', I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'), [
                {
                  text: 'OK'
                }
              ])
            }
          }
          
        }else{
          this.setState({StartDrilling:date})
        }
      }else{
        if(this.state.StartDrilling<date){
          this.setState({EndDrilling:date})
        }else{
          Alert.alert('', I18n.t(this.state.text+'.startendnotmat'), [
            {
              text: 'OK'
            }
          ])
        }

      }
    }

  }


  checkdepthalert=()=>{
    return new Promise((resolve, reject) => {
      Alert.alert('',I18n.t(this.state.text+'.error_depth3'),[
        {
          text: I18n.t(this.state.text+'.depth3_yes'),onPress:()=>{
            resolve('YES')
          }
        },
        {
          text: I18n.t(this.state.text+'.depth3_no'),onPress:()=>{
            resolve('NO')
          }
        }
      ],{cancelable:false}) 
    })
   
  }

 async saveButton(){

    let validate = false
    let depthfail = false
    let notstop = false
    let index = 0
    let depthfailedit=false
    let bucketcutting_flag = true
    let bucketcutting_count = 0
    let bucketcutting_index = ''
    let depthfail5_3 = true
    let check_depth_process5_parant = false
    let check_depth_process5_child = false

    if(this.props.index == undefined){
      if(this.props.DrillingListlist.length == 0){
        bucketcutting_flag = true
      }else{
        var bucket_check = 0
         for(var f = 0 ; f < this.props.DrillingListlist.length ; f++){
            if(this.props.DrillingListlist[f].DrillingToolType.Id == 2){
              bucket_check++
            }
          }
          if(bucket_check > 0){
            bucketcutting_flag = false
          }else{
            bucketcutting_flag = true
          }
      }
    }else{
      for(var f = 0 ; f < this.props.index ; f++){
        var bucket_check = 0
        if(this.props.DrillingListlist[f].DrillingToolType.Id == 2){
          bucket_check++
        }
      }
      if(bucket_check > 0){
        bucketcutting_flag = false
      }else{
        bucketcutting_flag = true
      }
    }
    // console.warn('bucketcutting_flag',bucketcutting_count,this.props.index,bucketcutting_index,this.props.DrillingListlist.length)
    if (this.state.Depth !== ''&&parseFloat(this.state.Depth).toFixed(3) > this.state.percentageCheckDepth) {
      let con = true

      let DepthAlert = await this.checkdepthalert().then((a,b)=>{
       
          return a
      })
     
      console.log('DepthAlert',DepthAlert)
      if(DepthAlert=='YES'){
        return
      }
    }
    if (this.state.drilltype == '') {
      Alert.alert(I18n.t(this.state.text+'.selectdrill'))
    }else if(
      this.state.ImageBucketCuttingEdgeWidths.length == 0 && 
      this.state.DrillTypeId == 2 && 
      bucketcutting_flag == true &&
      (this.props.category == 1 || this.props.category == 4 || this.props.category == 5)
      ){
        Alert.alert(I18n.t(this.state.text+'.error_bucketcutting_image'))
    }else if(
      this.state.BucketCuttingEdgeWidth == '' && 
      this.state.DrillTypeId == 2 && 
      bucketcutting_flag == true &&
      (this.props.category == 1 || this.props.category == 4 || this.props.category == 5)
      ){
        Alert.alert(I18n.t(this.state.text+'.error_bucketcutting'))
    }else if(this.state.machine == ''){
      Alert.alert(I18n.t(this.state.text+'.selectMachine'))
    }else if(this.props.shape == 2 || this.props.shape == 3){
      if(this.state.drilltoolDisable == false){
        if(this.state.drilltool == ''){
          Alert.alert(I18n.t(this.state.text+'.selectdrilltool1'))
        }else if(this.state.powerpack == ''&& !this.state.powerpackDisable){
          Alert.alert(I18n.t(this.state.text+'.selectpowerpack'))
        }
        else if (this.state.driver == '') {
          Alert.alert(I18n.t(this.state.text+'.selectdriver'))
        }else {
          // console.warn('depth kaa 1',this.props.category,this.props.child,parseFloat(this.state.Depth),this.props.drillinglistChild)
          if(this.props.category == 5 && this.props.child == false){
            if(this.props.DrillingListlist.length == this.props.index + 1){
              if(this.props.drillinglistChild.length > 0){
                if(this.props.drillinglistChild[0].Depth != ''){
                  if(parseFloat(this.state.Depth) < this.props.drillinglistChild[0].Depth){
                    check_depth_process5_parant = true
                  }
                }else{
                  check_depth_process5_parant = true
                }
              }else{
                check_depth_process5_parant = true
              }
            }else{
              check_depth_process5_parant = true
            }
          }else{
            check_depth_process5_parant = true
          }
          // console.warn('test jah 1')
          if(this.props.category == 5 && this.props.child == true){
            if(this.props.drillinglist.length > 0){
              if(this.props.index == 0 || this.props.drillinglistChild.length == 0){
                if(this.state.Depth > this.props.drillinglist[this.props.drillinglist.length-1].Depth){
                  check_depth_process5_child = true
                }else{
                  depthfail5_3 = false
                }
              }else{
                check_depth_process5_child = true
              }
              
            }else{
              check_depth_process5_child = true
            }
            
          }else{
            check_depth_process5_child = true
          }

          if (this.props.DrillingListlist.length > 0 && this.state.EndDrilling != '') {
            // if (this.state.Depth !== ''&&parseFloat(this.state.Depth).toFixed(3) > this.state.percentageCheckDepth) {
            //   console.log('this.state.Depth',parseFloat(this.state.Depth).toFixed(3) , this.state.percentageCheckDepth)
            //   Alert.alert(I18n.t(this.state.text+'.error_depth3'),[
            //     {
            //       text: I18n.t(this.state.text+'depth3_yes')
            //     },
            //     {
            //       text: I18n.t(this.state.text+'depth3_no')
            //     }
            //   ])
            //   return
            // }
            if(this.state.Depth != '' && this.state.Depth != null && this.state.Depth != 0){
              var start = moment(this.state.StartDrilling,"DD/MM/YYYY HH:mm")
                  var end1 = moment(this.state.EndDrilling,"DD/MM/YYYY HH:mm")
              if( start < end1){
              

              if (this.props.DrillingListData != undefined) {
                for (var i = 0; i < this.props.DrillingListlist.length; i++) {
                  if (this.props.DrillingListData.No == this.props.DrillingListlist[i].No) {
                    index = i
                  }
                }
                if (this.props.DrillingListlist[index + 1] !== undefined  ) {
                  if (parseFloat(this.state.Depth) <= this.props.DrillingListlist[this.props.DrillingListlist.length - 1].Depth ) {
                    if (parseFloat(this.state.Depth) <= this.props.DrillingListlist[index + 1].Depth && index > 0) {
                      if(this.props.DrillingListlist[index - 1] != undefined){
                        if (parseFloat(this.state.Depth) >= this.props.DrillingListlist[index - 1].Depth) {
                          
                            validate = true
                         
                        }else{
                          depthfail = true
                        }
                      }else{
                          validate = true
                       
                      }
                      
                    }else if (parseFloat(this.state.Depth) <= this.props.DrillingListlist[index + 1].Depth) {
                      
                
                        validate = true
                     
                    }else{
                      depthfail = true
                    }
                  }else{
                    depthfail = true
                    
                  }
                }else {
                  if(this.props.DrillingListlist[index - 1] != undefined){
                    if (parseFloat(this.state.Depth) >= this.props.DrillingListlist[index - 1].Depth) {
                      
                      if(this.state.status5 == 2 && this.state.status8 !=0){
                        var length8=0
                        for(var i=0;i<this.props.tremielist.length;i++){
                          length8 += this.props.tremielist[i].Length
                        }
                        if(this.state.Depth >= length8){
                          validate = true
                        }else{
                          depthfailedit = true
                        }
                      }else{
                        validate = true
                      }
                    }else{
                      depthfail = true
                    }
                  }else{
                    
                    if(this.state.status5 == 2 && this.state.status8 !=0){
                      var length8=0
                      for(var i=0;i<this.props.tremielist.length;i++){
                        length8 += this.props.tremielist[i].Length
                      }
                      if(this.state.Depth >= length8){
                        validate = true
                      }else{
                        depthfailedit = true
                      }
                    }else{
                      validate = true
                    }
                  }
                  
                }
        
              }else {
                if (this.props.DrillingListlist.length > 0) {
                  if (parseFloat(this.state.Depth) > this.props.DrillingListlist[this.props.DrillingListlist.length - 1].Depth ) {
                    
                    if(this.state.status5 == 2 && this.state.status8 !=0){
                      var length8=0
                      for(var i=0;i<this.props.tremielist.length;i++){
                        length8 += this.props.tremielist[i].Length
                      }
                      if(this.state.Depth >= length8){
                        validate = true
                      }else{
                        depthfailedit = true
                      }
                    }else{
                      validate = true
                    }
                    // console.warn('tremielist1',this.state.Depth-0.3>=length8)
                  }
                }
              }
            }
          }
            
          }else {
            if(this.state.EndDrilling != ''){
              var start = moment(this.state.StartDrilling,"DD/MM/YYYY HH:mm")
              var end1 = moment(this.state.EndDrilling,"DD/MM/YYYY HH:mm")
                if( start < end1){
                  if(this.state.EndDrilling != ''){
                    if(this.state.Depth != ''){
                      
                      if(this.state.status5 == 2 && this.state.status8 !=0){
                        var length8=0
                        for(var i=0;i<this.props.tremielist.length;i++){
                          length8 += this.props.tremielist[i].Length
                        }
                        if(this.state.Depth >= length8){
                          validate = true
                        }else{
                          depthfailedit = true
                        }
                      }else{
                        validate = true
                      }
                    }
                  }else{
                      validate = true
                  }
                }
            }else{
                validate = true
            }
          }
          // console.warn('check_depth_process5_parant 1', check_depth_process5_parant,validate)
          if (
            this.state.drilltype != '' && 
            this.state.driver != '' && 
            validate == true && 
            this.state.StartDrilling != '' &&
            check_depth_process5_parant == true &&
            check_depth_process5_child == true
            ) {
            if (this.state.Edit_Flag == 1 ) {
              if(this.props.DrillPileInsertType == 'add'){
                if(this.props.DrillingListlist.length > 0){
                  var start1 = moment(this.state.StartDrilling,"DD/MM/YYYY HH:mm")
                  var end2 = moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,"DD/MM/YYYY HH:mm")
                  var start2 = moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].StartDrilling,"DD/MM/YYYY HH:mm")
                  if(start1 > start2 && start1 > end2){                    
                    Actions.pop({refresh:{action:'update', value: {
                      process: this.props.process,
                      step: this.props.step,
                      data: {
                        StartDrilling:this.state.StartDrilling,
                        EndDrilling:this.state.EndDrilling,
                        driver:this.state.driver,
                        machine:this.state.machine,
                        drilltype:this.state.drilltype,
                        drilltool:this.state.drilltool,
                        ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                        BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                        MachineCraneId:this.state.MachineCraneId,
                        DriverId:this.state.DriverId,
                        DrillTypeId:this.state.DrillTypeId,
                        DrillToolId:this.state.DrillToolId,
                        IsBreak:this.state.IsBreak,
                        Depth:this.state.Depth,
                        drilltoolDisable:this.state.drilltoolDisable,
                        Images:this.state.Images,
                        No:this.state.No,
                        DrillingToolType:this.state.DrillingToolType,
                        Crane:this.state.Crane,
                        DrillPileInsertType:this.props.DrillPileInsertType,
                        PowerPack:this.state.PowerPack,
                        PowerPackId:this.state.PowerPackId,
                        powerpack:this.state.powerpack,
                      }
                    }}})
                    this.saveStep05_Row()
                  }else{
                    Alert.alert('', I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'), [
                      {
                        text: 'OK'
                      }
                    ])
                  }
                }else{
                  Actions.pop({refresh:{action:'update', value: {
                    process: this.props.process,
                    step: this.props.step,
                    data: {
                      StartDrilling:this.state.StartDrilling,
                      EndDrilling:this.state.EndDrilling,
                      driver:this.state.driver,
                      machine:this.state.machine,
                      drilltype:this.state.drilltype,
                      drilltool:this.state.drilltool,
                      ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                      BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                      MachineCraneId:this.state.MachineCraneId,
                      DriverId:this.state.DriverId,
                      DrillTypeId:this.state.DrillTypeId,
                      DrillToolId:this.state.DrillToolId,
                      IsBreak:this.state.IsBreak,
                      Depth:this.state.Depth,
                      drilltoolDisable:this.state.drilltoolDisable,
                      Images:this.state.Images,
                      No:this.state.No,
                      DrillingToolType:this.state.DrillingToolType,
                      Crane:this.state.Crane,
                      DrillPileInsertType:this.props.DrillPileInsertType,
                      PowerPack:this.state.PowerPack,
                      PowerPackId:this.state.PowerPackId,
                      powerpack:this.state.powerpack,
                    }
                  }}})
                  this.saveStep05_Row()
                }
              }else if(this.props.DrillPileInsertType == 'edit'){
                if(this.props.index>0){
                  var start1 = moment(this.state.StartDrilling,"DD/MM/YYYY HH:mm")
                  var end2 = moment(this.props.DrillingListlist[this.props.index-1].EndDrilling,"DD/MM/YYYY HH:mm")
                  var start2 = moment(this.props.DrillingListlist[this.props.index-1].StartDrilling,"DD/MM/YYYY HH:mm")
                  if(start1 > start2 && start1 > end2){                    
                    Actions.pop({refresh:{action:'update', value: {
                      process: this.props.process,
                      step: this.props.step,
                      data: {
                        StartDrilling:this.state.StartDrilling,
                        EndDrilling:this.state.EndDrilling,
                        driver:this.state.driver,
                        machine:this.state.machine,
                        drilltype:this.state.drilltype,
                        drilltool:this.state.drilltool,
                        ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                        BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                        MachineCraneId:this.state.MachineCraneId,
                        DriverId:this.state.DriverId,
                        DrillTypeId:this.state.DrillTypeId,
                        DrillToolId:this.state.DrillToolId,
                        IsBreak:this.state.IsBreak,
                        Depth:this.state.Depth,
                        drilltoolDisable:this.state.drilltoolDisable,
                        Images:this.state.Images,
                        No:this.state.No,
                        DrillingToolType:this.state.DrillingToolType,
                        Crane:this.state.Crane,
                        DrillPileInsertType:this.props.DrillPileInsertType,
                        PowerPack:this.state.PowerPack,
                        PowerPackId:this.state.PowerPackId,
                        powerpack:this.state.powerpack,
                      }
                    }}})
                    this.saveStep05_Row()
                  }else{
                    Alert.alert('', I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'), [
                      {
                        text: 'OK'
                      }
                    ])
                  }
                }else{
                    Actions.pop({refresh:{action:'update', value: {
                      process: this.props.process,
                      step: this.props.step,
                      data: {
                        StartDrilling:this.state.StartDrilling,
                        EndDrilling:this.state.EndDrilling,
                        driver:this.state.driver,
                        machine:this.state.machine,
                        drilltype:this.state.drilltype,
                        drilltool:this.state.drilltool,
                        ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                        BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                        MachineCraneId:this.state.MachineCraneId,
                        DriverId:this.state.DriverId,
                        DrillTypeId:this.state.DrillTypeId,
                        DrillToolId:this.state.DrillToolId,
                        IsBreak:this.state.IsBreak,
                        Depth:this.state.Depth,
                        drilltoolDisable:this.state.drilltoolDisable,
                        Images:this.state.Images,
                        No:this.state.No,
                        DrillingToolType:this.state.DrillingToolType,
                        Crane:this.state.Crane,
                        DrillPileInsertType:this.props.DrillPileInsertType,
                        PowerPack:this.state.PowerPack,
                        PowerPackId:this.state.PowerPackId,
                        powerpack:this.state.powerpack,
                      }
                    }}})
                    this.saveStep05_Row()
                }
                

              }
              
              
            }else{
              if (this.state.Edit_Flag == 1) {
                if(this.state.StartDrilling > this.props.DrillingListlist[this.props.DrillingListlist.length-1].StartDrilling && this.state.StartDrilling > this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling){
                Actions.pop({refresh:{action:'update', value: {
                  process: this.props.process,
                  step: this.props.step,
                  data: {
                    StartDrilling:this.state.StartDrilling,
                    EndDrilling:this.state.EndDrilling,
                    driver:this.state.driver,
                    machine:this.state.machine,
                    drilltype:this.state.drilltype,
                    drilltool:this.state.drilltool,
                    ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                    BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                    MachineCraneId:this.state.MachineCraneId,
                    DriverId:this.state.DriverId,
                    DrillTypeId:this.state.DrillTypeId,
                    DrillToolId:this.state.DrillToolId,
                    IsBreak:this.state.IsBreak,
                    Depth:this.state.Depth,
                    drilltoolDisable:this.state.drilltoolDisable,
                    Images:this.state.Images,
                    No:this.state.No,
                    DrillingToolType:this.state.DrillingToolType,
                    Crane:this.state.Crane,
                    DrillPileInsertType:this.props.DrillPileInsertType,
                    PowerPack:this.state.PowerPack,
                    PowerPackId:this.state.PowerPackId,
                    powerpack:this.state.powerpack,
                  }
                }}})
              }else{
                Alert.alert('', I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'), [
                  {
                    text: 'OK'
                  }
                ])
              }
              }else {
                Actions.pop()
              }
            }
          }else {
            if(this.state.Depth == ''){
              Alert.alert('', this.props.shape != 2?I18n.t(this.state.text+'.alertdepth'):I18n.t(this.state.text+'.alertdepthbw'), [
                {
                  text: 'OK'
                }
              ])
            }else{
              if(this.state.StartDrilling >= this.state.EndDrilling){
                Alert.alert('', I18n.t(this.state.text+'.startendnotmat'), [
                  {
                    text: 'OK'
                  }
                ])
              }else{
                if(notstop){
                  Actions.pop({refresh:{action:'update', value: {
                    process: this.props.process,
                    step: this.props.step,
                    data: {
                      StartDrilling:this.state.StartDrilling,
                      EndDrilling:this.state.EndDrilling,
                      driver:this.state.driver,
                      machine:this.state.machine,
                      drilltype:this.state.drilltype,
                      drilltool:this.state.drilltool,
                      ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                      BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                      MachineCraneId:this.state.MachineCraneId,
                      DriverId:this.state.DriverId,
                      DrillTypeId:this.state.DrillTypeId,
                      DrillToolId:this.state.DrillToolId,
                      IsBreak:this.state.IsBreak,
                      Depth:this.state.Depth,
                      drilltoolDisable:this.state.drilltoolDisable,
                      Images:this.state.Images,
                      No:this.state.No,
                      DrillingToolType:this.state.DrillingToolType,
                      Crane:this.state.Crane,
                      DrillPileInsertType:this.props.DrillPileInsertType,
                      PowerPack:this.state.PowerPack,
                      PowerPackId:this.state.PowerPackId,
                      powerpack:this.state.powerpack,
                    }
                  }}})
                  this.saveStep05_Row()
                }else{
                  if(check_depth_process5_parant == false){
                    Alert.alert('', I18n.t(this.state.text+'.depthfail_pileleg'), [
                      {
                        text: 'OK'
                      }
                    ])
                  }else{
                    if(depthfail5_3 == false){
                      Alert.alert('', I18n.t(this.state.text+'.depthfail_dwall'), [
                        {
                          text: 'OK'
                        }
                      ])
                    }else{
                      if(depthfailedit){
                        Alert.alert('', I18n.t(this.state.text+'.error_depth'), [
                          {
                            text: 'OK'
                          }
                        ])
                      }else{
                        Alert.alert('', I18n.t(this.state.text+'.depthfail'), [
                          {
                            text: 'OK'
                          }
                        ])
                      }
                    }
                  }
                  
                  
                }
              }
          }
        }
        }
      }else if(this.state.powerpack == ''&& !this.state.powerpackDisable){
        Alert.alert(I18n.t(this.state.text+'.selectpowerpack'))
      }
      else if (this.state.driver == '') {
        Alert.alert(I18n.t(this.state.text+'.selectdriver'))
      }
      else {
        // console.warn('depth kaa 2',this.props.category,this.props.child,parseFloat(this.state.Depth),this.props.drillinglistChild)
        if(this.props.category == 5 && this.props.child == false){
          if(this.props.DrillingListlist.length == this.props.index + 1){
            if(this.props.drillinglistChild.length > 0){
              if(this.props.drillinglistChild[0].Depth != ''){
                if(parseFloat(this.state.Depth) < this.props.drillinglistChild[0].Depth){
                  check_depth_process5_parant = true
                }
              }else{
                check_depth_process5_parant = true
              }
            }else{
              check_depth_process5_parant = true
            }
          }else{
            check_depth_process5_parant = true
          }
        }else{
          check_depth_process5_parant = true
        }
        // console.warn('test jah 2')
        if(this.props.category == 5 && this.props.child == true){
          if(this.props.drillinglist.length > 0){
            if(this.props.index == 0 || this.props.drillinglistChild.length == 0){
              if(this.state.Depth > this.props.drillinglist[this.props.drillinglist.length-1].Depth){
                check_depth_process5_child = true
              }else{
                depthfail5_3 = false
              }
            }else{
              check_depth_process5_child = true
            }
            
          }else{
            check_depth_process5_child = true
          }
          
        }else{
          check_depth_process5_child = true
        }
        if (this.props.DrillingListlist.length > 0 && this.state.EndDrilling != '') {
          if(this.state.Depth != '' && this.state.Depth != null && this.state.Depth != 0){
            var start = moment(this.state.StartDrilling,"DD/MM/YYYY HH:mm")
                var end1 = moment(this.state.EndDrilling,"DD/MM/YYYY HH:mm")
            if( start < end1){
              

            if (this.props.DrillingListData != undefined) {
              for (var i = 0; i < this.props.DrillingListlist.length; i++) {
                if (this.props.DrillingListData.No == this.props.DrillingListlist[i].No) {
                  index = i
                }
              }
              if (this.props.DrillingListlist[index + 1] !== undefined  ) {
                if (parseFloat(this.state.Depth) <= this.props.DrillingListlist[this.props.DrillingListlist.length - 1].Depth ) {
                  if (parseFloat(this.state.Depth) <= this.props.DrillingListlist[index + 1].Depth && index > 0) {
                    if(this.props.DrillingListlist[index - 1] != undefined){
                      if (parseFloat(this.state.Depth) >= this.props.DrillingListlist[index - 1].Depth) {
                       
                          validate = true
                        
                      }else{
                        depthfail = true
                      }
                    }else{
                     
                        validate = true
                     
                    }
                    
                  }else if (parseFloat(this.state.Depth) <= this.props.DrillingListlist[index + 1].Depth) {
                  
                      validate = true
                    
                  }else{
                    depthfail = true
                  }
                }else{
                  depthfail = true
                  
                }
              }else {
                if(this.props.DrillingListlist[index - 1] != undefined){
                  if (parseFloat(this.state.Depth) >= this.props.DrillingListlist[index - 1].Depth) {
                    if(this.state.status5 == 2 && this.state.status8 !=0){
                      var length8=0
                      for(var i=0;i<this.props.tremielist.length;i++){
                        length8 += this.props.tremielist[i].Length
                      }
                      if(this.state.Depth >= length8){
                        validate = true
                      }else{
                        depthfailedit = true
                      }
                    }else{
                      validate = true
                    }
                    // console.warn('tremielist2',this.props.tremielist)
                  }else{
                    depthfail = true
                  }
                }else{
                  if(this.state.status5 == 2 && this.state.status8 !=0){
                    var length8=0
                    for(var i=0;i<this.props.tremielist.length;i++){
                      length8 += this.props.tremielist[i].Length
                    }
                    if(this.state.Depth >= length8){
                      validate = true
                    }else{
                      depthfailedit = true
                    }
                  }else{
                    validate = true
                  }
                  // console.warn('tremielist2',this.props.tremielist)
                }
                
              }
      
            }else {
              if (this.props.DrillingListlist.length > 0) {
                if (parseFloat(this.state.Depth) > this.props.DrillingListlist[this.props.DrillingListlist.length - 1].Depth ) {
                  if(this.state.status5 == 2 && this.state.status8 !=0){
                    var length8=0
                    for(var i=0;i<this.props.tremielist.length;i++){
                      length8 += this.props.tremielist[i].Length
                    }
                    if(this.state.Depth >= length8){
                      validate = true
                    }else{
                      depthfailedit = true
                    }
                  }else{
                    validate = true
                  }
                  // console.warn('tremielist2',this.props.tremielist)
                }
              }
            }
          }
        }
          
        }else {
          if(this.state.EndDrilling != ''){
            var start = moment(this.state.StartDrilling,"DD/MM/YYYY HH:mm")
            var end1 = moment(this.state.EndDrilling,"DD/MM/YYYY HH:mm")
              if( start < end1){
                if(this.state.EndDrilling != ''){
                  if(this.state.Depth != ''){
                    if(this.state.status5 == 2 && this.state.status8 !=0){
                      var length8=0
                      for(var i=0;i<this.props.tremielist.length;i++){
                        length8 += this.props.tremielist[i].Length
                      }
                      if(this.state.Depth >= length8){
                        validate = true
                      }else{
                        depthfailedit = true
                      }
                    }else{
                        validate = true
                    }
                    // console.warn('tremielist2',this.props.tremielist)
                  }
                }else{
                  if(this.state.status5 == 2 && this.state.status8 !=0){
                    var length8=0
                    for(var i=0;i<this.props.tremielist.length;i++){
                      length8 += this.props.tremielist[i].Length
                    }
                    if(this.state.Depth >= length8){
                      validate = true
                    }else{
                      depthfailedit = true
                    }
                  }else{
                    validate = true
                  }
                  // console.warn('tremielist2',this.props.tremielist)
                }
              }
          }else{
          
              validate = true
            
          }
          
          
         
        }
        // console.warn('check_depth_process5_parant 2', check_depth_process5_parant,validate)
        if (
          this.state.drilltype != '' && 
          this.state.driver != '' && 
          validate == true && 
          this.state.StartDrilling != ''&&
          check_depth_process5_parant == true &&
          check_depth_process5_child == true
          ) {
          if (this.state.Edit_Flag == 1 ) {
            if(this.props.DrillPileInsertType == 'add'){
              if(this.props.DrillingListlist.length > 0){
                var start1 = moment(this.state.StartDrilling,"DD/MM/YYYY HH:mm")
                var end2 = moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,"DD/MM/YYYY HH:mm")
                var start2 = moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].StartDrilling,"DD/MM/YYYY HH:mm")
                if(start1 > start2 && start1 > end2){
                    Actions.pop({refresh:{action:'update', value: {
                    process: this.props.process,
                    step: this.props.step,
                    data: {
                      StartDrilling:this.state.StartDrilling,
                      EndDrilling:this.state.EndDrilling,
                      driver:this.state.driver,
                      machine:this.state.machine,
                      drilltype:this.state.drilltype,
                      drilltool:this.state.drilltool,
                      ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                      BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                      MachineCraneId:this.state.MachineCraneId,
                      DriverId:this.state.DriverId,
                      DrillTypeId:this.state.DrillTypeId,
                      DrillToolId:this.state.DrillToolId,
                      IsBreak:this.state.IsBreak,
                      Depth:this.state.Depth,
                      drilltoolDisable:this.state.drilltoolDisable,
                      Images:this.state.Images,
                      No:this.state.No,
                      DrillingToolType:this.state.DrillingToolType,
                      Crane:this.state.Crane,
                      DrillPileInsertType:this.props.DrillPileInsertType,
                      PowerPack:this.state.PowerPack,
                      PowerPackId:this.state.PowerPackId,
                      powerpack:this.state.powerpack,
                    }
                  }}})
                  this.saveStep05_Row()
                }else{
                  Alert.alert('', I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'), [
                    {
                      text: 'OK'
                    }
                  ])
                }
              }else{
                Actions.pop({refresh:{action:'update', value: {
                  process: this.props.process,
                  step: this.props.step,
                  data: {
                    StartDrilling:this.state.StartDrilling,
                    EndDrilling:this.state.EndDrilling,
                    driver:this.state.driver,
                    machine:this.state.machine,
                    drilltype:this.state.drilltype,
                    drilltool:this.state.drilltool,
                    ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                    BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                    MachineCraneId:this.state.MachineCraneId,
                    DriverId:this.state.DriverId,
                    DrillTypeId:this.state.DrillTypeId,
                    DrillToolId:this.state.DrillToolId,
                    IsBreak:this.state.IsBreak,
                    Depth:this.state.Depth,
                    drilltoolDisable:this.state.drilltoolDisable,
                    Images:this.state.Images,
                    No:this.state.No,
                    DrillingToolType:this.state.DrillingToolType,
                    Crane:this.state.Crane,
                    DrillPileInsertType:this.props.DrillPileInsertType,
                    PowerPack:this.state.PowerPack,
                    PowerPackId:this.state.PowerPackId,
                    powerpack:this.state.powerpack,
                  }
                }}})
                this.saveStep05_Row()
              }
              
            }else if(this.props.DrillPileInsertType == 'edit'){
              if(this.props.index>0){
                var start1 = moment(this.state.StartDrilling,"DD/MM/YYYY HH:mm")
                var end2 = moment(this.props.DrillingListlist[this.props.index-1].EndDrilling,"DD/MM/YYYY HH:mm")
                var start2 = moment(this.props.DrillingListlist[this.props.index-1].StartDrilling,"DD/MM/YYYY HH:mm")
                if(start1 > start2 && start1 > end2){                  
                  Actions.pop({refresh:{action:'update', value: {
                    process: this.props.process,
                    step: this.props.step,
                    data: {
                      StartDrilling:this.state.StartDrilling,
                      EndDrilling:this.state.EndDrilling,
                      driver:this.state.driver,
                      machine:this.state.machine,
                      drilltype:this.state.drilltype,
                      drilltool:this.state.drilltool,
                      ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                      BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                      MachineCraneId:this.state.MachineCraneId,
                      DriverId:this.state.DriverId,
                      DrillTypeId:this.state.DrillTypeId,
                      DrillToolId:this.state.DrillToolId,
                      IsBreak:this.state.IsBreak,
                      Depth:this.state.Depth,
                      drilltoolDisable:this.state.drilltoolDisable,
                      Images:this.state.Images,
                      No:this.state.No,
                      DrillingToolType:this.state.DrillingToolType,
                      Crane:this.state.Crane,
                      DrillPileInsertType:this.props.DrillPileInsertType,
                      PowerPack:this.state.PowerPack,
                      PowerPackId:this.state.PowerPackId,
                      powerpack:this.state.powerpack,
                    }
                  }}})
                  this.saveStep05_Row()
                }else{
                  Alert.alert('', I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'), [
                    {
                      text: 'OK'
                    }
                  ])
                }
              }else{
                  Actions.pop({refresh:{action:'update', value: {
                    process: this.props.process,
                    step: this.props.step,
                    data: {
                      StartDrilling:this.state.StartDrilling,
                      EndDrilling:this.state.EndDrilling,
                      driver:this.state.driver,
                      machine:this.state.machine,
                      drilltype:this.state.drilltype,
                      drilltool:this.state.drilltool,
                      ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                      BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                      MachineCraneId:this.state.MachineCraneId,
                      DriverId:this.state.DriverId,
                      DrillTypeId:this.state.DrillTypeId,
                      DrillToolId:this.state.DrillToolId,
                      IsBreak:this.state.IsBreak,
                      Depth:this.state.Depth,
                      drilltoolDisable:this.state.drilltoolDisable,
                      Images:this.state.Images,
                      No:this.state.No,
                      DrillingToolType:this.state.DrillingToolType,
                      Crane:this.state.Crane,
                      DrillPileInsertType:this.props.DrillPileInsertType,
                      PowerPack:this.state.PowerPack,
                      PowerPackId:this.state.PowerPackId,
                      powerpack:this.state.powerpack,
                    }
                  }}})
                  this.saveStep05_Row()
              }

            }
          }else{
            if (this.state.Edit_Flag == 1) {
              if(this.state.StartDrilling > this.props.DrillingListlist[this.props.DrillingListlist.length-1].StartDrilling && this.state.StartDrilling > this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling){

              Actions.pop({refresh:{action:'update', value: {
                process: this.props.process,
                step: this.props.step,
                data: {
                  StartDrilling:this.state.StartDrilling,
                  EndDrilling:this.state.EndDrilling,
                  driver:this.state.driver,
                  machine:this.state.machine,
                  drilltype:this.state.drilltype,
                  drilltool:this.state.drilltool,
                  ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                  BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                  MachineCraneId:this.state.MachineCraneId,
                  DriverId:this.state.DriverId,
                  DrillTypeId:this.state.DrillTypeId,
                  DrillToolId:this.state.DrillToolId,
                  IsBreak:this.state.IsBreak,
                  Depth:this.state.Depth,
                  drilltoolDisable:this.state.drilltoolDisable,
                  Images:this.state.Images,
                  No:this.state.No,
                  DrillingToolType:this.state.DrillingToolType,
                  Crane:this.state.Crane,
                  DrillPileInsertType:this.props.DrillPileInsertType,
                  PowerPack:this.state.PowerPack,
                  PowerPackId:this.state.PowerPackId,
                  powerpack:this.state.powerpack,
                }
              }}})
            }else{
              Alert.alert('', I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'), [
                {
                  text: 'OK'
                }
              ])
            }
            }else {
              Actions.pop()
            }
          }
        }else {
          if(this.state.Depth == ''){
            Alert.alert('', this.props.shape != 2?I18n.t(this.state.text+'.alertdepth'):I18n.t(this.state.text+'.alertdepthbw'), [
              {
                text: 'OK'
              }
            ])
          }else{
            if(this.state.StartDrilling >= this.state.EndDrilling){
              Alert.alert('', I18n.t(this.state.text+'.startendnotmat'), [
                {
                  text: 'OK'
                }
              ])
            }else {
              if(notstop){
              
                Actions.pop({refresh:{action:'update', value: {
                  process: this.props.process,
                  step: this.props.step,
                  data: {
                    StartDrilling:this.state.StartDrilling,
                    EndDrilling:this.state.EndDrilling,
                    driver:this.state.driver,
                    machine:this.state.machine,
                    drilltype:this.state.drilltype,
                    drilltool:this.state.drilltool,
                    ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                    BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                    MachineCraneId:this.state.MachineCraneId,
                    DriverId:this.state.DriverId,
                    DrillTypeId:this.state.DrillTypeId,
                    DrillToolId:this.state.DrillToolId,
                    IsBreak:this.state.IsBreak,
                    Depth:this.state.Depth,
                    drilltoolDisable:this.state.drilltoolDisable,
                    Images:this.state.Images,
                    No:this.state.No,
                    DrillingToolType:this.state.DrillingToolType,
                    Crane:this.state.Crane,
                    DrillPileInsertType:this.props.DrillPileInsertType,
                    PowerPack:this.state.PowerPack,
                    PowerPackId:this.state.PowerPackId,
                    powerpack:this.state.powerpack,
                  }
                }}})
                this.saveStep05_Row()
              }else{
                if(check_depth_process5_parant == false){
                  Alert.alert('', I18n.t(this.state.text+'.depthfail_pileleg'), [
                    {
                      text: 'OK'
                    }
                  ])
                }else{
                  if(depthfail5_3 == false){
                    Alert.alert('', I18n.t(this.state.text+'.depthfail_dwall'), [
                      {
                        text: 'OK'
                      }
                    ])
                  }else{
                    if(depthfailedit){
                      Alert.alert('', I18n.t(this.state.text+'.error_depth'), [
                        {
                          text: 'OK'
                        }
                      ])
                    }else{
                      Alert.alert('', I18n.t(this.state.text+'.depthfail'), [
                        {
                          text: 'OK'
                        }
                      ])
                    }
                  }
                }
                
              }
            }
        }
      }
      
      }

    }else if(this.state.drilltool == '' && this.state.drilltoolDisable == false){
      Alert.alert(I18n.t(this.state.text+'.selectdrilltool'))
    }else if (this.state.driver == '') {
      Alert.alert(I18n.t(this.state.text+'.selectdriver'))
    }else{
      // console.warn('savesave')
      // console.warn('depth kaa 3',this.props.category,this.props.child,parseFloat(this.state.Depth),this.props.drillinglistChild)
      if(this.props.category == 5 && this.props.child == false){
        if(this.props.DrillingListlist.length == this.props.index + 1){
          if(this.props.drillinglistChild.length > 0){
            if(this.props.drillinglistChild[0].Depth != ''){
              if(parseFloat(this.state.Depth) < this.props.drillinglistChild[0].Depth){
                check_depth_process5_parant = true
              }
            }else{
              check_depth_process5_parant = true
            }
          }else{
            check_depth_process5_parant = true
          }
        }else{
          check_depth_process5_parant = true
        }
      }else{
        check_depth_process5_parant = true
      }
      // console.warn('test jah 3')
      if(this.props.category == 5 && this.props.child == true){
        if(this.props.drillinglist.length > 0){
          if(this.props.index == 0 || this.props.drillinglistChild.length == 0){
            if(this.state.Depth > this.props.drillinglist[this.props.drillinglist.length-1].Depth){
              check_depth_process5_child = true
            }else{
              depthfail5_3 = false
            }
          }else{
            check_depth_process5_child = true
          }
          
        }else{
          check_depth_process5_child = true
        }
        
      }else{
        check_depth_process5_child = true
      }
      if (this.props.DrillingListlist.length > 0 && this.state.EndDrilling != '') {
        if(this.state.Depth != '' && this.state.Depth != null && this.state.Depth != 0){
          var start = moment(this.state.StartDrilling,"DD/MM/YYYY HH:mm")
              var end1 = moment(this.state.EndDrilling,"DD/MM/YYYY HH:mm")
          if( start < end1){
            
          if (this.props.DrillingListData != undefined) {
            if(this.state.notstopDisable){
              // console.warn('qqq')
              validate = true
            }else{
            for (var i = 0; i < this.props.DrillingListlist.length; i++) {
              if (this.props.DrillingListData.No == this.props.DrillingListlist[i].No) {
                index = i
              }
            }
            if (this.props.DrillingListlist[index + 1] !== undefined  ) {
              if (parseFloat(this.state.Depth) <= this.props.DrillingListlist[this.props.DrillingListlist.length - 1].Depth ) {
                if (parseFloat(this.state.Depth) <= this.props.DrillingListlist[index + 1].Depth && index > 0) {
                  if(this.props.DrillingListlist[index - 1] != undefined){
                    if (parseFloat(this.state.Depth) >= this.props.DrillingListlist[index - 1].Depth) {
                        validate = true
                    }else{
                      depthfail = true
                    }
                  }else{
                      validate = true

                  }
                  
                }else if (parseFloat(this.state.Depth) <= this.props.DrillingListlist[index + 1].Depth) {
                    validate = true

                }else{
                  depthfail = true
                }
              }else{
                depthfail = true
              }
            }else {
              // console.warn('last')
              if(this.props.DrillingListlist[index - 1] != undefined){
                if (parseFloat(this.state.Depth) >= this.props.DrillingListlist[index - 1].Depth) {
                  if(this.props.category == 4){
                    if(this.state.status5 == 2 && this.state.status8 !=0){
                      var length8=0
                      for(var i=0;i<this.props.tremielist.length;i++){
                        length8 += this.props.tremielist[i].Length
                      }
                      if(this.state.Depth >= length8){
                        validate = true
                      }else{
                        depthfailedit = true
                      }
                    }else{
                      validate = true
                    }
                  }else{
                    if(this.state.status5==2 && this.state.status6 != 0){
                      // console.warn('6 ทำแล้ว 1',this.state.CrossFlag,parseFloat(this.props.Depthneed).toFixed(3)-0.5,parseFloat(this.state.Depth).toFixed(3),parseFloat(this.state.AfterDepth).toFixed(3),parseFloat(this.state.ChangeDrillingFluidAfterDepth).toFixed(3))
                      if(this.state.CrossFlag == false){
                        if(this.state.AfterDepth == ''){
                          validate = true
                        }else{
                          if(parseFloat(this.props.Depthneed).toFixed(3)-0.5<=parseFloat(this.state.Depth).toFixed(3) && parseFloat(this.state.Depth).toFixed(3)<=parseFloat(this.state.AfterDepth).toFixed(3) && parseFloat(this.state.Depth).toFixed(3)>=parseFloat(this.state.BeforeDepth).toFixed(3)){
                            validate = true
                          }else{
                            depthfailedit = true
                          }
                        }
                      }else{
                        if(this.state.ChangeDrillingFluidAfterDepth == ''){
                          validate = true
                        }else{
                          if(parseFloat(this.props.Depthneed).toFixed(3)-0.5<=parseFloat(this.state.Depth).toFixed(3) && parseFloat(this.state.Depth).toFixed(3)<=parseFloat(this.state.ChangeDrillingFluidAfterDepth).toFixed(3) ){
                            validate = true
                          }else{
                            depthfailedit = true
                          }
                        }
                      }
  
                    }else{
                      validate = true
                    }
                  }
                  
                  
                }else{
                  depthfail = true
                }
              }else{
                if(this.props.category == 4){
                  if(this.state.status5 == 2 && this.state.status8 !=0){
                    var length8=0
                    for(var i=0;i<this.props.tremielist.length;i++){
                      length8 += this.props.tremielist[i].Length
                    }
                    if(this.state.Depth >= length8){
                      validate = true
                    }else{
                      depthfailedit = true
                    }
                  }else{
                    validate = true
                  }
                }else{
                  if(this.state.status5==2 && this.state.status6 != 0){
                    // console.warn('6 ทำแล้ว 2',this.state.CrossFlag,parseFloat(this.props.Depthneed).toFixed(3)-0.5,parseFloat(this.state.Depth).toFixed(3),parseFloat(this.state.AfterDepth).toFixed(3),parseFloat(this.state.ChangeDrillingFluidAfterDepth).toFixed(3))
                    if(this.state.CrossFlag == false){
                      if(this.state.AfterDepth == ''){
                        validate = true
                      }else{
                        if(parseFloat(this.props.Depthneed).toFixed(3)-0.5<=parseFloat(this.state.Depth).toFixed(3) && parseFloat(this.state.Depth).toFixed(3)<=parseFloat(this.state.AfterDepth).toFixed(3) && parseFloat(this.state.Depth).toFixed(3)>=parseFloat(this.state.BeforeDepth).toFixed(3)){
                          validate = true
                        }else{
                          depthfailedit = true
                        }
                      }
                    }else{
                      if(this.state.ChangeDrillingFluidAfterDepth == ''){
                        validate = true
                      }else{
                        if(parseFloat(this.props.Depthneed).toFixed(3)-0.5<=parseFloat(this.state.Depth).toFixed(3) && parseFloat(this.state.Depth).toFixed(3)<=parseFloat(this.state.ChangeDrillingFluidAfterDepth).toFixed(3) ){
                          validate = true
                        }else{
                          depthfailedit = true
                        }
                      }
                    }
  
                  }else{
                    validate = true
                  }
                }
                
              }
              
            }
          }
          }else {
            if (this.props.DrillingListlist.length > 0) {
              if (parseFloat(this.state.Depth) > this.props.DrillingListlist[this.props.DrillingListlist.length - 1].Depth ) {
                if(this.props.category == 4){
                  if(this.state.status5 == 2 && this.state.status8 !=0){
                    var length8=0
                    for(var i=0;i<this.props.tremielist.length;i++){
                      length8 += this.props.tremielist[i].Length
                    }
                    if(this.state.Depth >= length8){
                      validate = true
                    }else{
                      depthfailedit = true
                    }
                  }else{
                    validate = true
                  }
                }else{
                  if(this.state.status5==2 && this.state.status6 != 0){
                    // console.warn('6 ทำแล้ว 3',this.state.CrossFlag,parseFloat(this.props.Depthneed).toFixed(3)-0.5,parseFloat(this.state.Depth).toFixed(3),parseFloat(this.state.AfterDepth).toFixed(3),parseFloat(this.state.ChangeDrillingFluidAfterDepth).toFixed(3))
                    if(this.state.CrossFlag == false){
                      if(this.state.AfterDepth == ''){
                        validate = true
                      }else{
                        if(parseFloat(this.props.Depthneed).toFixed(3)-0.5<=parseFloat(this.state.Depth).toFixed(3) && parseFloat(this.state.Depth).toFixed(3)<=parseFloat(this.state.AfterDepth).toFixed(3) && parseFloat(this.state.Depth).toFixed(3)>=parseFloat(this.state.BeforeDepth).toFixed(3)){
                          validate = true
                        }else{
                          depthfailedit = true
                        }
                      }
                    }else{
                      if(this.state.ChangeDrillingFluidAfterDepth == ''){
                        validate = true
                      }else{
                        if(parseFloat(this.props.Depthneed).toFixed(3)-0.5<=parseFloat(this.state.Depth).toFixed(3) && parseFloat(this.state.Depth).toFixed(3)<=parseFloat(this.state.ChangeDrillingFluidAfterDepth).toFixed(3) ){
                          validate = true
                        }else{
                          depthfailedit = true
                        }
                      }
                    }
  
                  }else{
                    validate = true
                  }
                }
                
              }
            }
          }
        }
      }
        
      }else {
        if(this.state.EndDrilling != ''){
          var start = moment(this.state.StartDrilling,"DD/MM/YYYY HH:mm")
          var end1 = moment(this.state.EndDrilling,"DD/MM/YYYY HH:mm")
            if( start < end1){
              if(this.state.EndDrilling != ''){
                if(this.state.Depth != ''){
                  if(this.props.category == 4){
                    if(this.state.status5 == 2 && this.state.status8 !=0){
                      var length8=0
                      for(var i=0;i<this.props.tremielist.length;i++){
                        length8 += this.props.tremielist[i].Length
                      }
                      if(this.state.Depth >= length8){
                        validate = true
                      }else{
                        depthfailedit = true
                      }
                    }else{
                      validate = true
                    }
                  }else{
                    if(this.state.status5==2 && this.state.status6 != 0){
                      // console.warn('6 ทำแล้ว 4',this.state.CrossFlag,parseFloat(this.props.Depthneed).toFixed(3)-0.5,parseFloat(this.state.Depth).toFixed(3),parseFloat(this.state.AfterDepth).toFixed(3),parseFloat(this.state.ChangeDrillingFluidAfterDepth).toFixed(3))
                      if(this.state.CrossFlag == false){
                        if(this.state.AfterDepth == ''){
                          validate = true
                        }else{
                          if(parseFloat(this.props.Depthneed).toFixed(3)-0.5<=parseFloat(this.state.Depth).toFixed(3) && parseFloat(this.state.Depth).toFixed(3)<=parseFloat(this.state.AfterDepth).toFixed(3) && parseFloat(this.state.Depth).toFixed(3)>=parseFloat(this.state.BeforeDepth).toFixed(3)){
                            validate = true
                          }else{
                            depthfailedit = true
                          }
                        }
                      }else{
                        if(this.state.ChangeDrillingFluidAfterDepth == ''){
                          validate = true
                        }else{
                          if(parseFloat(this.props.Depthneed).toFixed(3)-0.5<=parseFloat(this.state.Depth).toFixed(3) && parseFloat(this.state.Depth).toFixed(3)<=parseFloat(this.state.ChangeDrillingFluidAfterDepth).toFixed(3) ){
                            validate = true
                          }else{
                            depthfailedit = true
                          }
                        }
                      }
  
                    }else{
                      // console.warn('test jah 5')
                      if(this.props.category == 5 && this.props.child == true){
                        if(this.props.drillinglist.length > 0){
                          if(this.state.Depth > this.props.drillinglist[this.props.drillinglist.length-1].Depth){
                            validate = true
                          }else{
                            depthfail5_3 = false
                          }
                        }else{
                          validate = true
                        }
                      }else{
                        validate = true
                      }
                    }
                  }
                  
                }
              }else{
                if(this.props.category == 4){
                  if(this.state.status5 == 2 && this.state.status8 !=0){
                    var length8=0
                    for(var i=0;i<this.props.tremielist.length;i++){
                      length8 += this.props.tremielist[i].Length
                    }
                    if(this.state.Depth >= length8){
                      validate = true
                    }else{
                      depthfailedit = true
                    }
                  }else{
                    validate = true
                  }
                }else{
                  if(this.state.status5==2 && this.state.status6 != 0){
                    // console.warn('6 ทำแล้ว 5',this.state.CrossFlag,parseFloat(this.props.Depthneed).toFixed(3)-0.5,parseFloat(this.state.Depth).toFixed(3),parseFloat(this.state.AfterDepth).toFixed(3),parseFloat(this.state.ChangeDrillingFluidAfterDepth).toFixed(3))
                    if(this.state.CrossFlag == false){
                      if(this.state.AfterDepth == ''){
                        validate = true
                      }else{
                        if(parseFloat(this.props.Depthneed).toFixed(3)-0.5<=parseFloat(this.state.Depth).toFixed(3) && parseFloat(this.state.Depth).toFixed(3)<=parseFloat(this.state.AfterDepth).toFixed(3) && parseFloat(this.state.Depth).toFixed(3)>=parseFloat(this.state.BeforeDepth).toFixed(3)){
                          validate = true
                        }else{
                          depthfailedit = true
                        }
                      }
                    }else{
                      if(this.state.ChangeDrillingFluidAfterDepth == ''){
                        validate = true
                      }else{
                        if(parseFloat(this.props.Depthneed).toFixed(3)-0.5<=parseFloat(this.state.Depth).toFixed(3) && parseFloat(this.state.Depth).toFixed(3)<=parseFloat(this.state.ChangeDrillingFluidAfterDepth).toFixed(3) ){
                          validate = true
                        }else{
                          depthfailedit = true
                        }
                      }
                    }
  
                  }else{
                    validate = true
                  }
                }
                

              }
            }
        }else{
            validate = true
        }
      }
      // console.warn('check_depth_process5_parant 3', check_depth_process5_parant,validate)
      if (
        this.state.drilltype != '' && 
        this.state.driver != '' && 
        validate == true && 
        this.state.StartDrilling != '' &&
        check_depth_process5_parant == true &&
        check_depth_process5_child == true
        ) {
        if (this.state.Edit_Flag == 1 ) {
          if(this.props.DrillPileInsertType == 'add'){
            if(this.props.DrillingListlist.length > 0){
              var start1 = moment(this.state.StartDrilling,"DD/MM/YYYY HH:mm")
              var end2 = moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,"DD/MM/YYYY HH:mm")
              var start2 = moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].StartDrilling,"DD/MM/YYYY HH:mm")
              if(start1 > start2 && start1 > end2){
                Actions.pop({refresh:{action:'update', value: {
                  process: this.props.process,
                  step: this.props.step,
                  data: {
                    StartDrilling:this.state.StartDrilling,
                    EndDrilling:this.state.EndDrilling,
                    driver:this.state.driver,
                    machine:this.state.machine,
                    drilltype:this.state.drilltype,
                    drilltool:this.state.drilltool,
                    ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                    BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                    MachineCraneId:this.state.MachineCraneId,
                    DriverId:this.state.DriverId,
                    DrillTypeId:this.state.DrillTypeId,
                    DrillToolId:this.state.DrillToolId,
                    IsBreak:this.state.IsBreak,
                    Depth:this.state.Depth,
                    drilltoolDisable:this.state.drilltoolDisable,
                    Images:this.state.Images,
                    No:this.state.No,
                    DrillingToolType:this.state.DrillingToolType,
                    Crane:this.state.Crane,
                    DrillPileInsertType:this.props.DrillPileInsertType,
                    PowerPack:this.state.PowerPack,
                    PowerPackId:this.state.PowerPackId,
                    powerpack:this.state.powerpack,
                  }
                }}})
                this.saveStep05_Row()
              }else{
                Alert.alert('', I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'), [
                  {
                    text: 'OK'
                  }
                ])
              }
            }else{
              Actions.pop({refresh:{action:'update', value: {
                process: this.props.process,
                step: this.props.step,
                data: {
                  StartDrilling:this.state.StartDrilling,
                  EndDrilling:this.state.EndDrilling,
                  driver:this.state.driver,
                  machine:this.state.machine,
                  drilltype:this.state.drilltype,
                  drilltool:this.state.drilltool,
                  ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                  BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                  MachineCraneId:this.state.MachineCraneId,
                  DriverId:this.state.DriverId,
                  DrillTypeId:this.state.DrillTypeId,
                  DrillToolId:this.state.DrillToolId,
                  IsBreak:this.state.IsBreak,
                  Depth:this.state.Depth,
                  drilltoolDisable:this.state.drilltoolDisable,
                  Images:this.state.Images,
                  No:this.state.No,
                  DrillingToolType:this.state.DrillingToolType,
                  Crane:this.state.Crane,
                  DrillPileInsertType:this.props.DrillPileInsertType,
                  PowerPack:this.state.PowerPack,
                  PowerPackId:this.state.PowerPackId,
                  powerpack:this.state.powerpack,
                }
              }}})
              this.saveStep05_Row()
            }
          }else if(this.props.DrillPileInsertType == 'edit'){
            if(this.props.index>0){
              var start1 = moment(this.state.StartDrilling,"DD/MM/YYYY HH:mm")
              var end2 = moment(this.props.DrillingListlist[this.props.index-1].EndDrilling,"DD/MM/YYYY HH:mm")
              var start2 = moment(this.props.DrillingListlist[this.props.index-1].StartDrilling,"DD/MM/YYYY HH:mm")
              if(start1 > start2 && start1 > end2){                
                Actions.pop({refresh:{action:'update', value: {
                  process: this.props.process,
                  step: this.props.step,
                  data: {
                    StartDrilling:this.state.StartDrilling,
                    EndDrilling:this.state.EndDrilling,
                    driver:this.state.driver,
                    machine:this.state.machine,
                    drilltype:this.state.drilltype,
                    drilltool:this.state.drilltool,
                    ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                    BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                    MachineCraneId:this.state.MachineCraneId,
                    DriverId:this.state.DriverId,
                    DrillTypeId:this.state.DrillTypeId,
                    DrillToolId:this.state.DrillToolId,
                    IsBreak:this.state.IsBreak,
                    Depth:this.state.Depth,
                    drilltoolDisable:this.state.drilltoolDisable,
                    Images:this.state.Images,
                    No:this.state.No,
                    DrillingToolType:this.state.DrillingToolType,
                    Crane:this.state.Crane,
                    DrillPileInsertType:this.props.DrillPileInsertType,
                    PowerPack:this.state.PowerPack,
                    PowerPackId:this.state.PowerPackId,
                    powerpack:this.state.powerpack,
                  }
                }}})
                this.saveStep05_Row()
              }else{
                Alert.alert('', I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'), [
                  {
                    text: 'OK'
                  }
                ])
              }
            }else{
                Actions.pop({refresh:{action:'update', value: {
                  process: this.props.process,
                  step: this.props.step,
                  data: {
                    StartDrilling:this.state.StartDrilling,
                    EndDrilling:this.state.EndDrilling,
                    driver:this.state.driver,
                    machine:this.state.machine,
                    drilltype:this.state.drilltype,
                    drilltool:this.state.drilltool,
                    ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                    BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                    MachineCraneId:this.state.MachineCraneId,
                    DriverId:this.state.DriverId,
                    DrillTypeId:this.state.DrillTypeId,
                    DrillToolId:this.state.DrillToolId,
                    IsBreak:this.state.IsBreak,
                    Depth:this.state.Depth,
                    drilltoolDisable:this.state.drilltoolDisable,
                    Images:this.state.Images,
                    No:this.state.No,
                    DrillingToolType:this.state.DrillingToolType,
                    Crane:this.state.Crane,
                    DrillPileInsertType:this.props.DrillPileInsertType,
                    PowerPack:this.state.PowerPack,
                    PowerPackId:this.state.PowerPackId,
                    powerpack:this.state.powerpack,
                  }
                }}})
                this.saveStep05_Row()
            }

          }
        }else{
          if (this.state.Edit_Flag == 1) {
            if(this.state.StartDrilling > this.props.DrillingListlist[this.props.DrillingListlist.length-1].StartDrilling && this.state.StartDrilling > this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling){

            Actions.pop({refresh:{action:'update', value: {
              process: this.props.process,
              step: this.props.step,
              data: {
                StartDrilling:this.state.StartDrilling,
                EndDrilling:this.state.EndDrilling,
                driver:this.state.driver,
                machine:this.state.machine,
                drilltype:this.state.drilltype,
                drilltool:this.state.drilltool,
                ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                MachineCraneId:this.state.MachineCraneId,
                DriverId:this.state.DriverId,
                DrillTypeId:this.state.DrillTypeId,
                DrillToolId:this.state.DrillToolId,
                IsBreak:this.state.IsBreak,
                Depth:this.state.Depth,
                drilltoolDisable:this.state.drilltoolDisable,
                Images:this.state.Images,
                No:this.state.No,
                DrillingToolType:this.state.DrillingToolType,
                Crane:this.state.Crane,
                DrillPileInsertType:this.props.DrillPileInsertType,
                PowerPack:this.state.PowerPack,
                PowerPackId:this.state.PowerPackId,
                powerpack:this.state.powerpack,
              }
            }}})
          }else{
            Alert.alert('', I18n.t(this.state.text+'.startdatefail')+` `+I18n.t(this.state.text+'.after')+moment(this.props.DrillingListlist[this.props.DrillingListlist.length-1].EndDrilling,'DD-MM-YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm'), [
              {
                text: 'OK'
              }
            ])
          }
          }else {
            Actions.pop()
          }
        }
      }else {
        if(this.state.Depth == ''){
          Alert.alert('', this.props.shape != 2?I18n.t(this.state.text+'.alertdepth'):I18n.t(this.state.text+'.alertdepthbw'), [
            {
              text: 'OK'
            }
          ])
        }else{
          if(this.state.StartDrilling >= this.state.EndDrilling){
            Alert.alert('', I18n.t(this.state.text+'.startendnotmat'), [
              {
                text: 'OK'
              }
            ])
          }else{
            // console.warn('notstop',notstop)
            if(notstop){
            
              Actions.pop({refresh:{action:'update', value: {
                process: this.props.process,
                step: this.props.step,
                data: {
                  StartDrilling:this.state.StartDrilling,
                  EndDrilling:this.state.EndDrilling,
                  driver:this.state.driver,
                  machine:this.state.machine,
                  drilltype:this.state.drilltype,
                  drilltool:this.state.drilltool,
                  ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
                  BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
                  MachineCraneId:this.state.MachineCraneId,
                  DriverId:this.state.DriverId,
                  DrillTypeId:this.state.DrillTypeId,
                  DrillToolId:this.state.DrillToolId,
                  IsBreak:this.state.IsBreak,
                  Depth:this.state.Depth,
                  drilltoolDisable:this.state.drilltoolDisable,
                  Images:this.state.Images,
                  No:this.state.No,
                  DrillingToolType:this.state.DrillingToolType,
                  Crane:this.state.Crane,
                  DrillPileInsertType:this.props.DrillPileInsertType,
                  PowerPack:this.state.PowerPack,
                  PowerPackId:this.state.PowerPackId,
                  powerpack:this.state.powerpack,
                }
              }}})
              this.saveStep05_Row()
            }else{
              if(check_depth_process5_parant == false){
                Alert.alert('', I18n.t(this.state.text+'.depthfail_pileleg'), [
                  {
                    text: 'OK'
                  }
                ])
              }else{
                if(depthfail5_3 == false){
                  Alert.alert('', I18n.t(this.state.text+'.depthfail_dwall'), [
                    {
                      text: 'OK'
                    }
                  ])
                }else{
                  if(depthfailedit){
                    if(this.props.category == 4){
                      Alert.alert('', I18n.t(this.state.text+'.error_depth'), [
                        {
                          text: 'OK'
                        }
                      ])
                    }else if(this.props.category == 1){
                      Alert.alert('', I18n.t(this.state.text+'.error_depth1'), [
                        {
                          text: 'OK'
                        }
                      ])
                    }else  {
                      Alert.alert('', I18n.t(this.state.text+'.error_depth2'), [
                        {
                          text: 'OK'
                        }
                      ])
                    }
                    
                  }else{
                    Alert.alert('', I18n.t(this.state.text+'.depthfail'), [
                      {
                        text: 'OK'
                      }
                    ])
                  }
                }
              }
              
            }
          }
        
      }
    }
    }
    
  }

  saveStep05_Row(){
    if(this.props.DrillPileInsertType == 'add'){
       var drillingIdd = null
       var Noo = this.props.count+1
    }else{
      var drillingIdd = this.state.No
      var Noo = this.state.No
    }
    var valueApi = {
      Language:I18n.locale,
      JobId: this.props.jobid,
      PileId: this.props.pileid,
      StartDrilling:this.state.StartDrilling,
      EndDrilling:this.state.EndDrilling == '' ? '' : this.state.EndDrilling,
      driver:this.state.driver,
      machine:this.state.machine,
      drilltype:this.state.drilltype,
      drilltool:this.state.drilltool,
      MachineId:this.state.MachineCraneId,
      DriverId:this.state.DriverId,
      DrillingToolTypeId:this.state.DrillTypeId,
      DrillingToolTypeName:this.state.drilltype,
      DrillingToolId:this.state.DrillToolId == undefined ? '' :this.state.DrillToolId,
      DrillingToolName: this.state.DrillToolId == undefined ? '' : this.state.drilltool,
      IsBreak:this.state.IsBreak,
      Depth:this.state.Depth == '' ? 0 : parseFloat(this.state.Depth),
      drilltoolDisable:this.state.drilltoolDisable,
      Images:this.state.Images,
      No:Noo,
      latitude:this.props.lat,
      longitude:this.props.log,
      PowerPackId:this.state.PowerPackId,
      PowerPackName: this.state.powerpack,
      drillingId:drillingIdd,
      index:this.props.index,
      ImageBucketCuttingEdgeWidths:this.state.ImageBucketCuttingEdgeWidths,
      BucketCuttingEdgeWidth:this.state.BucketCuttingEdgeWidth,
    }
    // console.log('pile drilling',valueApi)
    this.props.clearError()
    ///////////
    if(this.props.child == true){
      this.props.pileSaveStep05DrillingChild(valueApi)
    }else{
      this.props.pileSaveStep05Drilling(valueApi)
    }
    
  }

  renderlistenerButton(){
    if (this.state.StartDrilling == '' && this.state.EndDrilling == '') {
      return(
        <TouchableOpacity activeOpacity={0.8} onPress={() => this.setTime('start')} style={styles.listButtonCenter}>
          <View style={styles.buttonStart}>
            <Image source={require('../../assets/image/icon_play.png')}/>
          </View>
        </TouchableOpacity>
      )
    }else if (this.state.StartDrilling != '' && this.state.EndDrilling == '' ) {
      return(
        <TouchableOpacity activeOpacity={0.8} onPress={() => this.setTime('end')} style={styles.listButtonCenter}>
          <View style={styles.buttonStop}>
            <Image source={require('../../assets/image/icon_stop.png')}/>
          </View>
        </TouchableOpacity>
      )
    }
  }

  renderTime(){
    if (this.props.DrillingListData == undefined) {
      return(
      <View style={[styles.listTime, styles.listTimeLeft, styles.listTimeBorderBefore]}>
        {
          this.state.StartDrilling == '' ? <Text></Text> :
          <View style={styles.listTimeRow}>
            <Text>{this.state.StartDrilling != '' ? I18n.t(this.state.text+'.start') + this.state.StartDrilling : ''}</Text>
            {this.state.notstopDisable == false?
            <TouchableOpacity activeOpacity={0.8} onPress={()=>this.setState({Type:'start',datevisibledate:true})}>
            {
              this.state.Edit_Flag == 1 ?
              <View style={styles.buttonEdit}>
                <Image source={require('../../assets/image/icon_edit.png')}/>
                <Text style={styles.buttonEditText}>{I18n.t(this.state.text+'.edit')}</Text>
              </View>
              :
              <View/>
            }
            </TouchableOpacity>
            :
              <View/>
            }
          </View>
        }
        
        {
          this.state.EndDrilling == '' ? <Text></Text> :
          <View style={styles.listTimeRow}>
          <Text>{this.state.EndDrilling != '' ? I18n.t(this.state.text+'.end') + this.state.EndDrilling : ''}</Text>
          {this.state.notstopDisable == false?
          <TouchableOpacity activeOpacity={0.8} onPress={()=>this.setState({Type:'end',datevisibledate:true})}>
            {
              this.state.Edit_Flag == 1 ?
              <View style={styles.buttonEdit}>
                <Image source={require('../../assets/image/icon_edit.png')}/>
                <Text style={styles.buttonEditText}>{I18n.t(this.state.text+'.edit')}</Text>
              </View>
              :
              <View/>
            }
            </TouchableOpacity>
            :<View/>
          }
          </View>
        }
        {this.renderlistenerButton()}

      </View>)
    }else if(this.props.DrillingListData != undefined){
        return(
        <View style={[styles.listTime, styles.listTimeLeft, styles.listTimeBorderBeforeAfter]}>
          <View style={styles.listTimeRow}>
            {
              this.state.StartDrilling == '' ? <Text></Text> :
              <Text>{this.state.StartDrilling != '' ? I18n.t(this.state.text+'.start') + this.state.StartDrilling : ''}</Text>
            }
            {
              this.state.StartDrilling == '' && this.props.DrillingListData.StartDrilling == '' ? <View/>
              :
              <TouchableOpacity activeOpacity={0.8} onPress={()=>this.setState({Type:'start',datevisibledate:true})}>
                {
                  this.state.Edit_Flag == 1 ?
                  <View style={styles.buttonEdit}>
                    <Image source={require('../../assets/image/icon_edit.png')}/>
                    <Text style={styles.buttonEditText}>{I18n.t(this.state.text+'.edit')}</Text>
                  </View>
                  :
                  <View/>
                }
              </TouchableOpacity>
            }
          </View>
          <View style={styles.listTimeRow}>
            {
              this.state.EndDrilling == '' ? <Text></Text> :
              <Text>{this.state.EndDrilling != '' ? I18n.t(this.state.text+'.end') + this.state.EndDrilling : ''}</Text>
            }
            {
              this.state.EndDrilling == '' && this.props.DrillingListData.EndDrilling == '' ? <View/>
              :
              <TouchableOpacity activeOpacity={0.8} onPress={()=>this.setState({Type:'end',datevisibledate:true})}>
                {
                  this.state.Edit_Flag == 1 ?
                  <View style={styles.buttonEdit}>
                    <Image source={require('../../assets/image/icon_edit.png')}/>
                    <Text style={styles.buttonEditText}>{I18n.t(this.state.text+'.edit')}</Text>
                  </View>
                  :
                  <View/>
                }
              </TouchableOpacity>
            }
          </View>
          {this.renderlistenerButton()}

        </View>)
      }
  }
  getTime(){
    if(this.state.Type == 'start'){
      if(this.state.StartDrilling != ''){
        return new Date(moment(this.state.StartDrilling,'DD/MM/YYYY HH:mm:ss').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        return new Date()
      }
    }else{
      if(this.state.EndDrilling != ''){
        return new Date(moment(this.state.EndDrilling,'DD/MM/YYYY HH:mm:ss').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        return new Date()
      }
    }
  }
  onChangedate = (event, selectedDate) => {
    if(event.type == 'set'){
      var date = moment(selectedDate).format('DD-MM-YYYY')
      
      this.setState({date:date,datevisibledate:false,datevisibletime:true})
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };
  onChangetime = (event, selectedDate) => {
    var time = moment(selectedDate).format('HH:mm')
    var datetime_str = this.state.date + time
    var datetime = moment(datetime_str, 'DD-MM-YYYY HH:mm')
    if(event.type == 'set'){
      this.setState({datevisibletime:false})
      this.setCalendar2(datetime,this.state.Type)
    }
  };
  render() {
    var temp = ''
    if(this.state.pile.shape==1){
      temp = I18n.t(this.state.text+'.selectdrilltool')
    }else{
      temp = I18n.t(this.state.text+'.selectdrilltool_dw')
    }
    return (<View style={styles.listContainer}>
      <View style={[
        this.props.category == 3|| this.props.category == 5 ? [styles.listNavigation,{backgroundColor: DEFAULT_COLOR_4}] : this.props.shape == 1 ? styles.listNavigation : [styles.listNavigation, {backgroundColor:DEFAULT_COLOR_1}]    
      ]}>
        <Text style={styles.listNavigationText}>{I18n.t(this.state.text+'.drillpileinsert')}</Text>
      </View>
      <ScrollView>
      {/* <KeyboardAwareScrollView scrollEnabled={true} resetScrollToCoords={{ x: 0, y: 0 }} enableOnAndroid ={true}> */}
      <View style={styles.listRow}>
        <View style={styles.listTopicSub}>
          <Text style={styles.listTopicSubText}>{I18n.t(this.state.text+'.drill')}</Text>
          <View style={styles.listSelectedView}>
            {/* <DateTimePicker
            date={this.getTime()}
                isVisible={this.state.datevisible}
                onConfirm={(date) => { 
                  this.setState({ datevisible: false },
                    this.setCalendar2(date,this.state.Type)
                  )
                  console.warn(date)
                }}
                onCancel={() => this.setState({ datevisible: false })}
                datePickerModeAndroid="spinner"
                timePickerModeAndroid= "spinner"
                mode='datetime'
              /> */}
              {this.state.datevisibledate &&<DateTimePicker
                value={this.getTime()}
                isVisible={this.state.datevisibledate}
                is24Hour={true}
                display='spinner'
                mode='date'
                onChange={this.onChangedate}
              />}
              {this.state.datevisibletime&&this.state.datevisibledate==false  && <DateTimePicker
                value={this.getTime()}
                isVisible={this.state.datevisibletime}
                is24Hour={true}
                display='spinner'
                mode='time'
                onChange={this.onChangetime}
              />}
            <ModalSelector
              data={this.state.drillTypeData}
              onChange={this.selectedDrillType}
              selectTextStyle={styles.textButton}
              disabled={(this.state.StartDrilling != '' && this.state.Edit_Flag == 0) || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))  ? true : false}
              cancelText="Cancel">
              <TouchableOpacity style={(this.state.StartDrilling != '' && this.state.Edit_Flag == 0) || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? [styles.button, { borderColor: MENU_GREY_ITEM }] : styles.button}>
                <View style={styles.buttonTextStyle}>
                  <Text style={(this.state.StartDrilling != '' && this.state.Edit_Flag == 0) || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? [styles.buttonText ,{ color: MENU_GREY_ITEM }]:styles.buttonText}>{this.state.drilltype || I18n.t(this.state.text+'.selectdrill1')}</Text>
                  <View style={styles.listSelectBotton}>
                    <Icon name='arrow-drop-down' type='MaterialIcons' color={(this.state.StartDrilling != '' && this.state.Edit_Flag == 0) || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? MENU_GREY_ITEM : '#007CC2' }></Icon>
                  </View>
                </View>
              </TouchableOpacity>
            </ModalSelector>
          </View>
        </View>
        <View style={styles.listTopicSub}>
          <Text style={styles.listTopicSubText}>{I18n.t(this.state.text+'.drillsize')}</Text>
          <TextInput value={isNaN (parseFloat(this.state.pile.size).toFixed(2))?this.state.pile.size:parseFloat(this.state.pile.size).toFixed(2)} editable={false} keyboardType={"numeric"} underlineColorAndroid='transparent' style={[styles.listTextbox, styles.textboxDisable]}/>
        </View>
        {(this.props.category == 1 || this.props.category == 4 || this.props.category == 5) && this.state.DrillTypeId == 2 && <View style={[styles.listTopicSub,{flexDirection:"row",justifyContent:'center',alignItems:'center'}]}>
            <Text style={[styles.listTopicSubText,{flex:1}]}>{I18n.t(this.state.text+'.bucketcutting')}</Text>
            <View style={styles.buttonImage}>
                <TouchableOpacity
                  style={[styles.image,this.state.ImageBucketCuttingEdgeWidths.length>0?{backgroundColor:'#6dcc64'}:{}]}
                  onPress={() => this.onCameraRoll("ImageBucketCuttingEdgeWidths", this.state.ImageBucketCuttingEdgeWidths,'edit')}
                >
                  <Icon name="image" type="entypo" color="#FFF" />
                  <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                </TouchableOpacity>
            </View>
          </View>}
          {(this.props.category == 1 || this.props.category == 4 || this.props.category == 5) && this.state.DrillTypeId == 2 && <View style={styles.listTopicSub}>
            <View style={[styles.button, { height: 50, padding: 0 }, this.state.Edit_Flag == 0 && { borderColor: MENU_GREY_ITEM }]}>
              <TextInput
                ref={(input) => { this.BucketCuttingEdgeWidthInput = input; }}
                keyboardType="numeric"
                value={this.state.BucketCuttingEdgeWidth.toString()}
                onChangeText={BucketCuttingEdgeWidth => this.setState({ BucketCuttingEdgeWidth })}
                underlineColorAndroid="transparent"
                style={this.state.Edit_Flag == 1 ? styles.textInputStyle : styles.textInputStyleGrey}
                placeholder={I18n.t(this.state.text+'.bucketcutting')}
                onEndEditing={()=> {
                  this.setState({BucketCuttingEdgeWidth:isNaN(parseFloat(this.state.BucketCuttingEdgeWidth))?'':  parseFloat(this.state.BucketCuttingEdgeWidth).toFixed(3) },()=>{
                    // this.DepthInput.focus()
                    if(this.state.status8 == 0 || this.state.status10 == 0){
                      this.DepthInput.focus()
                    }else{
                      this.Depth2Input.focus()
                    }
                  })
                }}
              />
            </View>
          </View>}
        <View style={styles.listTopicSub}>
          <Text style={styles.listTopicSubText}>{I18n.t(this.state.text+'.machine')}</Text>
          <View style={styles.listSelectedView}>
            <ModalSelector
              data={this.state.machineData}
              onChange={this.selectedMachine}
              selectTextStyle={styles.textButton}
              disabled={(this.state.StartDrilling != '' && this.state.Edit_Flag == 0) || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? true : false}
              cancelText="Cancel">
              <TouchableOpacity style={(this.state.StartDrilling != '' && this.state.Edit_Flag == 0) || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? [styles.button, { borderColor: MENU_GREY_ITEM }] : styles.button}>
                <View style={styles.buttonTextStyle}>
                  <Text style={(this.state.StartDrilling != '' && this.state.Edit_Flag == 0) || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? [styles.buttonText ,{ color: MENU_GREY_ITEM }]:styles.buttonText}>{this.state.machine || I18n.t("mainpile.2_1.machineselect")}</Text>
                  <View style={styles.listSelectBotton}>
                    <Icon name='arrow-drop-down' type='MaterialIcons' color={(this.state.StartDrilling != '' && this.state.Edit_Flag == 0) || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? MENU_GREY_ITEM : '#007CC2' }></Icon>
                  </View>
                </View>
              </TouchableOpacity>
            </ModalSelector>
          </View>
        </View>
        <View style={styles.listTopicSub}>
          <Text style={styles.listTopicSubText}>{this.state.pile.shape == 1 ? I18n.t(this.state.text+'.drilltoolbp') : I18n.t(this.state.text+'.drilltooldw') }</Text>
          <View style={styles.listSelectedView}>
            <ModalSelector
              data={this.state.drillToolData}
              onChange={this.selectedDrillTool}
              selectTextStyle={styles.textButton}
              cancelText="Cancel"
              disabled={this.state.drilltoolDisable == true || ((this.state.StartDrilling != '' && this.state.Edit_Flag == 0) || this.state.notstopDisable )? true : false}
              >
              <TouchableOpacity style={[styles.button,this.state.drilltoolDisable || ((this.state.StartDrilling != '' && this.state.Edit_Flag == 0) || this.state.notstopDisable) || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? { borderColor: MENU_GREY_ITEM } : {}]}>
                <View style={styles.buttonTextStyle}>
                  <Text style={[styles.buttonText,this.state.drilltoolDisable || ((this.state.StartDrilling != '' && this.state.Edit_Flag == 0 )|| this.state.notstopDisable)|| (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? { color: MENU_GREY_ITEM } : {}]}>
                    {this.state.drilltool=='' ?temp:this.state.drilltool}</Text>
                  <View style={styles.listSelectBotton}>
                    <Icon
                      name="arrow-drop-down"
                      type="MaterialIcons"
                      color={this.state.drilltoolDisable || ((this.state.StartDrilling != '' && this.state.Edit_Flag == 0) || this.state.notstopDisable) || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? MENU_GREY_ITEM : "#007CC2"}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </ModalSelector>
          </View>
        </View>
        {
          this.state.pile.shape == 1 ?    
          <View/>
          :
          <View style={styles.listTopicSub}>
            <Text style={styles.listTopicSubText}>PowerPack</Text>
            <View style={styles.listSelectedView}>
              <ModalSelector
                data={this.state.powerPackData}
                onChange={this.selectedPowerPack}
                selectTextStyle={styles.textButton}
                cancelText="Cancel"
                disabled={this.state.powerpackDisable == true || this.state.Edit_Flag == 0 || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? true : false }
                >
                <TouchableOpacity style={[styles.button,this.state.powerpackDisable || this.state.Edit_Flag == 0 || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? { borderColor: MENU_GREY_ITEM } : {}]}>
                  <View style={styles.buttonTextStyle}>
                    <Text style={[styles.buttonText,this.state.powerpackDisable || this.state.Edit_Flag == 0 || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? { color: MENU_GREY_ITEM } : {}]}>
                      {this.state.powerpack || I18n.t(this.state.text+'.selectpowerpack')}</Text>
                    <View style={styles.listSelectBotton}>
                      <Icon
                        name="arrow-drop-down"
                        type="MaterialIcons"
                        color={this.state.powerpackDisable || this.state.Edit_Flag == 0 || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? MENU_GREY_ITEM : "#007CC2"}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </ModalSelector>
            </View>
          </View>
        }
        <View style={styles.listTopicSub}>
          <Text style={styles.listTopicSubText}>{I18n.t(this.state.text+'.driver')}</Text>
          <View style={styles.listSelectedView}>
            <ModalSelector
              data={this.state.driverData}
              onChange={this.selectedDriver}
              selectTextStyle={styles.textButton}
              disabled={(this.state.StartDrilling != '' && this.state.Edit_Flag == 0 ) || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? true : false}
              cancelText="Cancel">
              <TouchableOpacity style={(this.state.StartDrilling != '' && this.state.Edit_Flag == 0) || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? [styles.button, { borderColor: MENU_GREY_ITEM }] : styles.button}>
                <View style={styles.buttonTextStyle}>
                  <Text style={(this.state.StartDrilling != '' && this.state.Edit_Flag == 0 ) || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? [styles.buttonText ,{ color: MENU_GREY_ITEM }]:styles.buttonText}>{this.state.driver || I18n.t("mainpile.2_1.driverselect")}</Text>
                  <View style={styles.listSelectBotton}>
                    <Icon name='arrow-drop-down' type='MaterialIcons' color={(this.state.StartDrilling != '' && this.state.Edit_Flag == 0) || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? MENU_GREY_ITEM : '#007CC2' }></Icon>
                  </View>
                </View>
              </TouchableOpacity>
            </ModalSelector>
          </View>
        </View>
        {this.renderTime()}
        <View style={styles.listCheckBox}>
          {
            this.state.IsBreak == true
              ?
              <Icon name="check" reverse={true} color="#6dcc64" size={15} onPress={() => this.selectedIsBreak()} disabled={this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))}/>
              :
              <Icon name="check" reverse={true} color="grey" size={15} onPress={() => this.selectedIsBreak()} disabled={this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))} />
          }
          <Text>{I18n.t('mainpile.5_3.pileb')}</Text>
        </View>
        {
          this.state.status8 == 0 || this.state.status10 == 0
          ?
          <View style={styles.listTopicSub}>
            <Text style={styles.listTopicSubText}>{this.props.shape == 1 ||this.props.category == 4 ? I18n.t(this.state.text+'.depthbp') : I18n.t(this.state.text+'.depthdw')}</Text>
            <TextInput 
              ref={(input) => { this.DepthInput = input; }}
              value={isNaN(parseFloat(this.state.Depth)) ? '' :this.state.Depth.toString()}
              editable={(this.state.StartDrilling != '' && this.state.EndDrilling != '' && this.state.Edit_Flag == 1 && !this.state.notstopDisable) && (this.state.status8 == 0 || this.state.status10 == 0 )   ? true : false}
              onChangeText={(value) => this.setState({Depth:this.onChangeFormatDecimal(value)})}
              keyboardType="numeric"
              underlineColorAndroid='transparent'
              onEndEditing={()=> {
                this.setState({Depth:this.state.Depth  ? parseFloat(this.state.Depth).toFixed(3) : ''})
              }}
              onSubmitEditing={()=> this.setState({Depth:this.state.Depth  ? parseFloat(this.state.Depth).toFixed(3) : ''})}
              style={[styles.listTextbox, this.state.EndDrilling == '' || this.state.Edit_Flag == 0 || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? styles.textboxDisable :
              [styles.button,styles.buttonText]]}/>
          </View>
          :
          <TouchableOpacity style={styles.listTopicSub} onPress={()=> Alert.alert('',I18n.t('mainpile.6_0.error'))}>
            <Text style={styles.listTopicSubText}>{this.props.category == 1 ||this.props.category == 4 ? I18n.t(this.state.text+'.depthbp') : I18n.t(this.state.text+'.depthdw')}</Text>
            <TextInput 
              ref={(input) => { this.Depth2Input = input; }}
              value={isNaN(parseFloat(this.state.Depth)) ? '' :this.state.Depth.toString()}
              editable={(this.state.StartDrilling != '' && this.state.EndDrilling != '' && this.state.Edit_Flag == 1 && !this.state.notstopDisable) && (this.state.status8 == 0 || this.state.status10 == 0 )   ? true : false}
              onChangeText={(value) => this.setState({Depth:this.onChangeFormatDecimal(value)})}
              keyboardType="numeric"
              underlineColorAndroid='transparent'
              onEndEditing={()=> {
                this.setState({Depth:this.state.Depth  ? parseFloat(this.state.Depth).toFixed(3) : ''})
              }}
              onSubmitEditing={()=> this.setState({Depth:this.state.Depth  ? parseFloat(this.state.Depth).toFixed(3) : ''})}
              style={[styles.listTextbox, this.state.EndDrilling == '' || this.state.Edit_Flag == 0 || this.state.notstopDisable || (this.state.status8 != 0 && this.state.status10 != 0 && (this.state.pile.shape != 1||this.props.category==4))? styles.textboxDisable :
              [styles.button,styles.buttonText]]}/>
          </TouchableOpacity>
        }
        
        <View style={[styles.buttonImage, styles.buttonCenterImage]}>
          <TouchableOpacity
            style={[styles.image,this.state.Images.length>0?{backgroundColor:'#6dcc64'}:{}]}
            onPress={() => this.onCameraRoll("Images", this.state.Images,'edit')}
          >
            <Icon name="image" type="entypo" color="#fff" />
            <Text style={styles.imageText}>{I18n.t("mainpile.liquidtestinsert.view")}</Text>
          </TouchableOpacity>
        </View>
      </View>
      {/* </KeyboardAwareScrollView> */}
      </ScrollView>
      <View style={styles.buttonFixed}>
          <View style={[styles.second,
            this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
          ]}>
          <TouchableOpacity  style={styles.firstButton} onPress={this.deleteButton} onPress={() => Actions.pop()}>
            <Text style={styles.selectButton}>{I18n.t('mainpile.steeldetail.button.close')}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.secondButton,
            this.props.Edit_Flag!=1?{backgroundColor:'#BABABA'}:this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
          ]} onPress={()=> this.saveButton()} disabled={this.props.Edit_Flag == 0}>
            <Text style={styles.selectButton}>{I18n.t('mainpile.steeldetail.button.save')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>)
  }
}

const mapStateToProps = state => {
  return {
      pileAll: state.mainpile.pileAll || null,
      item: state.pile.item,
      status:state.pile.status,
      drillinglist: state.drillinglist.drillinglist,
      drillinglistChild: state.drillinglist.drillinglistChild,
      tremielist: state.tremie.tremielist,
      
  }
}

export default connect(mapStateToProps, actions)(DrillPileInsert)
