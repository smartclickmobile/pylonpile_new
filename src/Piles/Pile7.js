import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity
} from "react-native"
import { connect } from "react-redux"
import { Container, Content } from "native-base"
import StepIndicator from "react-native-step-indicator"
import * as actions from "../Actions"
import Pile7_1 from "./Pile7_1"
import Pile7_2 from "./Pile7_2"
import Pile7_3 from "./Pile7_3"
import styles from "./styles/Pile7.style"
import I18n from "../../assets/languages/i18n"
import { MAIN_COLOR,DEFAULT_COLOR_1, DEFAULT_COLOR_4 } from "../Constants/Color"
import moment from "moment"
class Pile7 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      step: 0,
      process: 7,
      Edit_Flag: 1,
      error: null,
      data: [],
      location: {
        position: {
          lat: 1,
          log: 1
        }
      },
      status8:0,
      random7_1:null,
      onPress:false,
      Super:false,
      checksavebutton:false,
      Step7Status:0,
      stepstatus:null
    }
  }
  componentDidMount(){
    if (this.props.stack) {
      this.setState({step:this.props.stack[this.props.stack.length - 1].step - 1})
    }
  }
  // async componentDidUpdate(prvProps){
  //   if (this.props.pileAll[this.props.pileid] != undefined &&this.props.pileAll[this.props.pileid]!==prvProps.pileAll[this.props.pileid]) {
  //     var edit = this.props.pileAll[this.props.pileid]
  //     if (edit["7_0"] != undefined) {
  //       await this.setState({ Edit_Flag: edit["7_0"].data.Edit_Flag })
  //     }
  //     if(edit != undefined){
  //       this.setState({status8:edit.step_status.Step8Status})
  //     }
  //   }
  //   if(this.props.step_status2_random!=null&&this.props.step_status2_random!==prvProps.step_status2_random){
  //     if(this.props.processstatus==7){
  //       await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step7Status: nextProps.step_status2.Step7Status })
  //       this.props.mainPileGetStorage(this.props.pileid, "step_status")
  //       let step = this.state.step 
  //       if(this.props.category==5&&this.props.parent==false){
  //         step = 2
  //       }

  //     }
  //   }
  //   if(this.props.pile7_pass8==true&&this.state.onPress==false&&this.state.Super==true){
  //     this.setState({
  //       Super:false
  //     },()=>{
  //       this.props.onNextStep()
  //       this.onSetStack(8,0)
  //       setTimeout(()=>{this.setState({checksavebutton:false})},2000)
  //     })
  //   }
  // }
  async componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps7',nextProps.step_status2_random , this.state.stepstatus , nextProps.processstatus == 7)
    if(nextProps.step_status2_random != this.state.stepstatus && nextProps.processstatus == 7){
      this.setState({stepstatus:nextProps.step_status2_random},async()=>{
        console.warn('yes7',nextProps.step_status2.Step7Status,this.state.step)
        await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step7Status: nextProps.step_status2.Step7Status })
        this.props.mainPileGetStorage(this.props.pileid, "step_status")
        let step =this.state.step 
        if(this.props.category==5&&this.props.parent==false){
          step = 2
        }
        if(step==2){
          if(nextProps.step_status2.Step7Status==2){
            if(this.props.category==3||this.props.category==5){
              var valueApi = {
                Language: I18n.currentLocale(),
                JobId: this.props.jobid,
                PileId: this.props.pileid,
                lockstep:false
              }
              console.log("7_3 checksuper", valueApi)
              this.setState({
                onPress:false,Super:true
              },()=>{
                this.props.pileSaveStep07super(valueApi)
              })
              
            }else{
              this.setState({
                onPress:false
              },async ()=>{
                console.warn('change to 8',this.state.Step7Status)
                await this.onSetStack(8,0)
                this.props.onNextStep()
                setTimeout(()=>{this.setState({checksavebutton:false})},2000)
              })
              
            }
          }else{
            setTimeout(()=>{this.setState({checksavebutton:false})},2000)
            // Alert.alert('', I18n.t('mainpile.3_3.error_checkall'), [
            //   {
            //     text: 'OK'
            //   }
            // ])
          }
          
        }else{
          // this.onSetStack(7,0)
        }
         
      })
    }
    if (nextProps.pileAll[this.props.pileid] != undefined) {
      var edit = nextProps.pileAll[this.props.pileid]
      if (edit["7_0"] != undefined) {
        await this.setState({ Edit_Flag: edit["7_0"].data.Edit_Flag })
      }
      if(edit != undefined){
        this.setState({status8:edit.step_status.Step8Status})
      }
    }
    console.warn("componentWillReceiveProps7 part2",nextProps.pile7,this.state.random7_1!=nextProps.random7_1,this.state.onPress)
    if(nextProps.pile7==true&&this.state.random7_1!=nextProps.random7_1&&this.state.onPress==true){
      if(nextProps.save7page==1){
        // console.warn("pile7",nextProps.pile7)
        this.setState({random7_1:nextProps.random7_1,step:1,onPress:false})
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process:7
        })
        this.onSetStack(7,0)
        setTimeout(()=>{this.setState({checksavebutton:false})},2000)
      }else if(nextProps.save7page==2){
        if(this.props.category==5&&this.props.parent==false){
          this.setState({random7_1:nextProps.random7_1})
          this.props.getStepStatus2({
            jobid: this.props.jobid,
            pileid: this.props.pileid,
            process:7
          })
          setTimeout(()=>{this.setState({checksavebutton:false})},2000)
        }else{
          this.setState({random7_1:nextProps.random7_1,step:2,onPress:false})
          this.props.getStepStatus2({
            jobid: this.props.jobid,
            pileid: this.props.pileid,
            process:7
          })
          this.onSetStack(7,1)
          setTimeout(()=>{this.setState({checksavebutton:false})},2000)
        }
      
      }else if(nextProps.save7page==3){
        // console.warn("pile7",nextProps.pile7)
        this.setState({random7_1:nextProps.random7_1})
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process:7
        })
        setTimeout(()=>{this.setState({checksavebutton:false})},2000)
      }
      
    }
    console.log(nextProps.pile7_pass8,this.state.Super)
    if(nextProps.pile7_pass8==true&&this.state.onPress==false&&this.state.Super==true){
      this.setState({
        Super:false
      },()=>{
        this.props.onNextStep()
        this.onSetStack(8,0)
        setTimeout(()=>{this.setState({checksavebutton:false})},2000)
      })
    }

   
    await this.setState({
      error: nextProps.error,
      data: nextProps.pileAll[nextProps.pileid],
      location: nextProps.pileAll.currentLocation
    })
  }



  async onSetStack(pro,step) {
    // console.warn("onSetStack")
    var data = {
      process: pro,
      step: step + 1
    }
    await this.props.setStack(data)
    await this.props.mainPileGetAllStorage()
  }

  _renderStepFooter() {
    var flag = true
    if(this.props.category == 5){
      if(this.props.parent == false){
        flag = true
      }else{
        flag = false
      }
    }else{
      flag = false
    }
    return (
      <View>
        <View style={{ flexDirection: "column" }}>
          <View style={{ borderWidth: 1 }}>
            <StepIndicator
              stepCount={flag?2:3}
              onPress={step => {
                console.log(step)
                this.setState({ step: step })
                this.onSetStack(7,step)
              }}
              currentPosition={this.state.step}
            />
          </View>
          {
            this.state.Edit_Flag == 1 ?
            <View style={[styles.second,
              this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
            ]}>
              <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}  disabled={this.state.checksavebutton}>
                <Text style={styles.selectButton}>{I18n.t("mainpile.button.delete")}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.secondButton,
                this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
              ]} onPress={this.nextButton}  disabled={this.state.checksavebutton}>
                <Text style={styles.selectButton}>{I18n.t("mainpile.button.next")}</Text>
              </TouchableOpacity>
            </View>
            :
            <View/>
          }
        </View>
      </View>
    )
  }

  deleteButton = () => {
    this.props.setStack({
      process: 7,
      step: 1
    })
    const data = {
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      processid: this.state.process,
      lat:this.state.location!=undefined ? this.state.location.position.lat:1,
      log:this.state.location!=undefined ? this.state.location.position.log:1
    }
    if(this.state.status8 == 0){
      Alert.alert("Warning", I18n.t("mainpile.button.confirmdelete"), [
        { text: "Cancel" },
        { text: "OK", onPress: () => this.props.pileDelete(data) }
      ])
    }else{
      Alert.alert("", I18n.t('alert.error_delete7'), [
        {
          text: "OK"
        }
      ])
    }
    
  }

  nextButton = () => {
    switch (this.state.step) {
      case 0:
        this.onSave(this.state.step)
        break
      case 1:
        this.onSave(this.state.step)
        break
      case 2:
        this.onSave(this.state.step)
        break
      default:
        break
    }
  }

  async onSave(step) {
    this.setState({checksavebutton:true})
    await this.props.mainPileGetAllStorage()
    if(this.props.category == 5 && step == 1 && this.props.parent == false){
      step = 2
    }
    console.warn('onSave step',step)
    if (step == 0) {
      var value = {
        process: 7,
        step: 1,
        pile_id: this.props.pileid,
        category:this.props.category ,
        sp_parentid:this.props.sp_parentid,
        parent:this.props.parent
      }
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value).then(async data => {console.log("Status1:",data)
        

          var valueApi = {
            Language: I18n.currentLocale(),
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            category:this.props.category ,
            Page: 1,
            MachineId: this.state.data["7_1"].data.MachineId,
            MachineName:this.state.data["7_1"].data.MachineName,
            DriverId: this.state.data["7_1"].data.DriverId,
            DriverName: this.state.data["7_1"].data.DriverName,
            latitude: this.state.location!=undefined ? this.state.location.position.lat:1,
            longitude: this.state.location!=undefined ? this.state.location.position.log:1,
            
          }
          
          if (this.state.error == null) {
            if (data.check == false) {
              Alert.alert("", data.errorText[0], [
                {
                  text: "OK"
                }
              ])
              this.setState({checksavebutton:false})
            } else {
              this.setState({onPress:true,Step7Status:data.statuscolor},()=>{
                this.props.pileSaveStep07(valueApi)
              })
              

               if(this.props.startdate != '' & this.props.startdate != null){
                this.setState({ startdate: moment(this.props.startdate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY HH:mm")})
              }else{
                this.setState({ startdate: moment().format("DD/MM/YYYY HH:mm")})
              }
            }
          } else {
            
            Alert.alert(this.state.error)
            this.setState({checksavebutton:false})
          }
        })
      }
      
    } else if (step == 1) {
      var value = {
        process: 7,
        step: 2,
        pile_id: this.props.pileid,
        category:this.props.category ,
        sp_parentid:this.props.sp_parentid,
        parent:this.props.parent
      }
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value).then(async data => {console.log("Status1:",data)
          var valueApi = {
            Language: I18n.currentLocale(),
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            Page: 2,
            ImageLengthSteelCarry: this.state.data['7_2'].data.ImageLengthSteelCarry,
            // OverLifting: this.state.data["7_2"].data.OverLifting,
            // Pcoring: this.state.data["7_2"].data.Pcoring,
            latitude: this.state.location!=undefined ? this.state.location.position.lat:"",
            longitude: this.state.location!=undefined ? this.state.location.position.log:"",
            hanginglength: this.state.data["7_2"].data.HangingBarsLength
          }
          
          if (this.state.error == null) {
            if (data.check == false) {
              Alert.alert("", data.errorText[0], [
                {
                  text: "OK"
                }
              ])
              this.setState({checksavebutton:false})
            } else {
              this.setState({onPress:true,Step7Status:data.statuscolor},()=>{
                console.log(valueApi)
                this.props.pileSaveStep07(valueApi)
              })
              

               if(this.props.startdate != '' & this.props.startdate != null){
                this.setState({ startdate: moment(this.props.startdate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY HH:mm")})
              }else{
                this.setState({ startdate: moment().format("DD/MM/YYYY HH:mm")})
              }
            }
          } else {
            
            Alert.alert(this.state.error)
            this.setState({checksavebutton:false})
          }
        })
      }
        
              // this.setState({ step: 2 })
              // this.onSetStack(7,1)
              // setTimeout(()=>{this.setState({checksavebutton:false})},2000)
         
      
    } else if (step ==2 ){
      var value = {
        process: 7,
        step: 3,
        pile_id: this.props.pileid,
        shape: this.props.shape,
        category:this.props.category ,
        sp_parentid:this.props.sp_parentid,
        parent:this.props.parent
      }
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value).then(async data => {
          console.log("Status3:",data) 
          console.warn('data.statuscolor',data.statuscolor)
          if (this.state.error == null) {
            if (data.check != false) {
              let checkstart = this.state.data["7_3"].startdate === '' ||this.state.data["7_3"].startdate ===  null||this.state.data["7_3"].startdate === undefined ? false : true 
              let checkend = this.state.data["7_3"].enddate === '' ||this.state.data["7_3"].enddate ===  null||this.state.data["7_3"].enddate === undefined ? false : true 
              let end = checkend ? this.state.data["7_3"].enddate:moment().format("DD/MM/YYYY HH:mm")
              var valueend  = {
                process: 7,
                step: 3,
                pile_id: this.props.pileid,
                data: {
                  SectionDetail: this.state.data["7_3"].data.SectionDetail,
                  concretedisabled: this.state.data["7_3"].data.concretedisabled,
                  local: this.state.data["7_3"].data.local,
                  shape: this.state.data["7_3"].data.shape,
                  startdate: this.state.data["7_3"].data.startdate,
                  enddate: this.state.data["7_3"].data.enddate
                },
                startdate:this.state.data["7_3"].startdate,
                enddate:end,

              }
              await this.props.mainPileSetStorage(this.props.pileid,'7_3',valueend)

              if(checkstart==false){
                if(this.props.startdate > this.props.enddate) return this.setState({checksavebutton:false},()=>{
                  Alert.alert("", I18n.t("mainpile.4_1.error_step"), [{text: "OK"}])
                })
                let valueApi = {
                  Language: I18n.currentLocale(),
                  JobId: this.props.jobid,
                  PileId: this.props.pileid,
                  Page: this.props.category == 5 && this.props.parent == false ? 2:3,
                  latitude: this.state.location!=undefined ? this.state.location.position.lat:"",
                  longitude: this.state.location!=undefined ? this.state.location.position.log:"",
                  startdate: moment(this.props.startdate,"DD/MM/YYYY HH:mm:ss").format("YYYY-MM-DD HH:mm:ss"),
                  enddate: moment(this.props.enddate,"DD/MM/YYYY HH:mm:ss").format("YYYY-MM-DD HH:mm:ss"),
                  category:this.props.category ,
                  sp_parentid:this.props.sp_parentid,
                  parent:this.props.parent
                }
                if (this.state.error == null) {
                  if(data.check == false) {
                    Alert.alert("", data.errorText[0], [
                      {
                        text: "OK"
                      }
                    ])
                    this.setState({checksavebutton:false})
                  }else{
                    this.setState({onPress:true,Step7Status:data.statuscolor},()=>{
                      this.props.pileSaveStep07(valueApi)
                    })
                  }
                } else {
                  Alert.alert(this.state.error)
                  this.setState({checksavebutton:false})
                }
              }else{
                let start = moment(this.state.data["7_3"].startdate,"DD/MM/YYYY HH:mm")
                let end2 = moment(end,"DD/MM/YYYY HH:mm")
                console.log('start <= end2',start <= end2)
                if(start <= end2){
                  let valueApi1 = {
                    Language: I18n.currentLocale(),
                    JobId: this.props.jobid,
                    PileId: this.props.pileid,
                    Page:  this.props.category == 5 && this.props.parent == false ? 2:3,
                    latitude: this.state.location!=undefined ? this.state.location.position.lat:1,
                    longitude: this.state.location!=undefined ? this.state.location.position.log:1,
                    startdate: moment(this.state.data["7_3"].startdate,"DD/MM/YYYY HH:mm").format("YYYY-MM-DD HH:mm:ss"),
                    enddate: moment(end,"DD/MM/YYYY HH:mm").format("YYYY-MM-DD HH:mm:ss"),
                    category:this.props.category ,
                    sp_parentid:this.props.sp_parentid,
                    parent:this.props.parent
                  }
                  console.log('start <= end2 error',data.check)
                  if (this.state.error == null) {
                    if (data.check == false) {
                      Alert.alert("", data.errorText[0], [
                        {
                          text: "OK"
                        }
                      ])
                      this.setState({checksavebutton:false})
                    } else {
                      this.setState({onPress:true,Step7Status:data.statuscolor},()=>{
                        this.props.pileSaveStep07(valueApi1)
                      })

                    }
                  } else {
                    Alert.alert(this.state.error)
                    this.setState({checksavebutton:false})
                  }
                } else{
                  Alert.alert("", I18n.t("mainpile.liquidtestinsert.alert.error4"), [
                    {
                      text: "OK"
                    }
                  ])
                  this.setState({checksavebutton:false})
                }

              }
            }else{
              Alert.alert("", data.errorText[0], [
                {
                  text: "OK"
                }
              ])
              this.setState({checksavebutton:false})
            }
          }else{
            Alert.alert(this.state.error)
            this.setState({checksavebutton:false})
          }
        })
      }
    }
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    var step_index = 2
    if(this.props.category == 5){
      if(this.props.parent == false){
        step_index = 1
      }
    }

    var flag = true
    if(this.props.category == 5){
      if(this.props.parent == false){
        flag = false
      }
    }
    return (
      <View style={{ flex: 1 }}>
        <Content>
          <Pile7_1 ref="pile7_1" hidden={!(this.state.step == 0)} jobid={this.props.jobid} pileid={this.props.pileid} shape={this.props.shape} onRefresh={this.props.onRefresh} />
          {flag && <Pile7_2
            ref="pile7_2"
            hidden={!(this.state.step == 1)}
            jobid={this.props.jobid}
            pileid={this.props.pileid}
            pile={this.props.pile}
            shape={this.props.shape}
            category={this.props.category} 
          />}
          <Pile7_3 
            ref="pile7_3" 
            hidden={!(this.state.step == step_index)} 
            jobid={this.props.jobid} 
            pileid={this.props.pileid} 
            shape={this.props.shape} 
            category={this.props.category} 
            startdate={this.props.startdate}
            startdatedefault={this.state.startdate}
            enddate={this.props.enddate}/>
        </Content>
        {this._renderStepFooter()}
      </View>
    )
  }

  updateState(value) {
    const ref = "pile" + value.process + "_" + value.step
    if (this.refs[ref].getWrappedInstance()) {
      this.refs[ref].getWrappedInstance().updateState(value)
    }
  }
  initialStep(){
    this.setState({step:0})
  }
}

const mapStateToProps = state => {
  return {
    error: state.pile.error,
    pileAll: state.mainpile.pileAll || null,
    stack: state.mainpile.stack_item,
    pile7:state.pile.pile7,
    random7_1:state.pile.random7_1,
    save7page:state.pile.save7page,
    pile7_pass8:state.pile.pile7_pass8,

    step_status2_random:state.mainpile.step_status2_random,
    step_status2:state.mainpile.step_status2,
    processstatus:state.mainpile.process
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(Pile7)
