import { Platform, StyleSheet } from "react-native"
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM } from "../../Constants/Color"

export default StyleSheet.create({
  topic: {
    marginTop: 10,
    marginLeft: 10
  },
  topicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  boxTopic: {
    marginLeft: 10,
    marginTop: 10
  },
  buttonImage: {
    width: "40%",
    alignItems: "center",
    alignSelf:'center',
    marginLeft: 10,
    marginTop:10
  },
  selectedView: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10
  },
  button: {
    borderWidth: 2,
    borderColor: MENU_GREY_ITEM,
    borderRadius: 10,
    padding: 5,
    backgroundColor: "#FFF"
  },
  textInputStyleGrey: {
    color: MENU_GREY_ITEM,
    fontFamily: "THSarabunNewBold",
    fontSize: 18,
    height: 50,
    width: "100%",
    textAlign: "center"
  },
  textInputStyle: {
    color: MAIN_COLOR,
    fontFamily: "THSarabunNewBold",
    fontSize: 18,
    height: 50,
    width: "100%",
    textAlign: "center"
  },
  buttonBox: {
    borderWidth: 2,
    borderColor: "#cccccc",
    borderRadius: 20,
    height: 40,
    marginTop: 5,
    justifyContent: "center",
    alignItems: 'center'
  },
  approveBox: {
    borderWidth: 1,
    borderColor: "#cccccc",
    backgroundColor: SUB_COLOR,
    borderRadius: 20,
    height: 40,
    width: "28%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5,
    marginLeft: 5
  },
  image: {
    backgroundColor: SUB_COLOR,
    height: 40,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10
  },
  imageDetail: {
    flexDirection: "row",
    alignItems: "center"
  },
  imageText: {
    marginLeft: 10,
    color: "white"
  },
  listTopicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
})
