import {Platform, StyleSheet} from 'react-native';

export default StyleSheet.create({
  second: {
    height: 50,
    flexDirection: "row"
  },
  firstButton: {
    width: "40%",
    backgroundColor: "#BABABA",
    justifyContent: "center",
    alignItems: "center"
  },
  secondButton: {
    width: "60%",
    justifyContent: "center",
    alignItems: "center"
  },
  secondButtonGrey: {
    width: "60%",
    backgroundColor: "#BABABA",
    justifyContent: "center",
    alignItems: "center"
  },
  selectButton: {
    color: "#FFF",
    fontSize: 20
  }
});
