import {Platform, StyleSheet} from 'react-native';
import {
  MAIN_COLOR,
  SUB_COLOR,
  BASIC_COLOR,
  BLUE_COLOR,
  MENU_GREY_ITEM,
  BUTTON_COLOR
} from "../../Constants/Color"

export default StyleSheet.create({
  listContainer: {
    flex: 1,
    backgroundColor: '#fff'
  },
  listNavigation: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: MAIN_COLOR,
    height: 60
  },
  listNavigationText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold'
  },
  listRow: {
    flexDirection: 'column',
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 60
  },
  listTopic: {},
  listTopicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  listTopicSub: {
    marginTop: 10
  },
  listTopicSubText: {},
  listTopicSubRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 20
  },
  listTextbox: {
    borderWidth: 2,
    borderColor: "#cccccc",
    borderRadius: 20,
    height: 40,
    marginTop: 5,
    color: BASIC_COLOR,
    textAlign: "center"
  },
  listTextboxFlex: {
    flex: 1
  },
  textboxDisable: {
    backgroundColor: '#ddeef0',
    color: "#686968"
  },
  listSelectedView: {
    marginTop: 5
  },
  listSelectBotton: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  listCheckBox: {
    flexDirection: "row",
    alignItems: "center",
    borderColor: MENU_GREY_ITEM
  },
  listButton: {
    width: 120,
    height: 30,
    borderRadius: 40,
    backgroundColor: BLUE_COLOR,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15
  },
  listButtonStyle: {
    justifyContent: "flex-start",
    alignItems: "center",
    flexDirection: "row"
  },
  listButtonText: {
    color: BUTTON_COLOR,
    fontFamily: "THSarabunNew",
    fontWeight: "bold",
    fontSize: 14
  },
  listButtonIcon: {
    position: "absolute",
    right: 0,
    marginRight: 10
  },
  listLabel: {
    flex: 2,
    fontFamily: "THSarabunNew",
    fontWeight: "bold",
    fontSize: 14,
    color: BASIC_COLOR
  },
  listButtonImage: {
    backgroundColor: '#2fa8df',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    height: 40,
    marginLeft: 10
  },
  listButtonCenter: {
    alignSelf: 'center',
    marginBottom: 10
  },
  listTime: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },
  listTimeLeft: {
    alignItems: 'flex-start'
  },
  listTimeRow: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    marginBottom: 10
  },
  listTimeCol: {
    flexDirection: 'column'
  },
  listTimeBorderBefore: {
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#ccc',
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 10
  },
  listTimeBorderBeforeAfter: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#ccc',
    paddingTop: 10,
    marginBottom: 10
  },
  buttonEdit: {
    borderWidth: 1,
    borderColor: '#3b95cd',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 5
  },
  buttonEditText: {
    color: '#3b95cd'
  },
  buttonStart: {
    backgroundColor: '#2d91ce',
    width: 80,
    height: 35,
    borderRadius: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    marginTop: 10
  },
  buttonStop: {
    backgroundColor: '#b6b7b6',
    width: 80,
    height: 35,
    borderRadius: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    marginTop: 10
  },
  buttonCenter: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonCenterImage: {
    width: 200,
    alignItems: 'center',
    alignSelf: 'center'
  },
  buttonImageView: {
    backgroundColor: '#cccbca'
  },
  button: {
    borderWidth: 2,
    borderColor: MAIN_COLOR,
    borderRadius: 10,
    padding: 5,
    backgroundColor: '#FFF'
  },
  buttonText: {
    color: MAIN_COLOR,
    fontFamily: 'THSarabunNewBold',
    fontSize: 20
  },
  buttonTextStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  buttonFixed: {
    flexDirection: 'column',
    // flex: 1,
    // position: 'absolute',
    // bottom: 0
  },
  second: {
    height: 50,
    backgroundColor: MAIN_COLOR,
    flexDirection: 'row'
  },
  firstButton: {
    width: '40%',
    backgroundColor: '#BABABA',
    justifyContent: 'center',
    alignItems: 'center'
  },
  secondButton: {
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  selectButton: {
    color: '#FFF',
    fontSize: 20
  },
  image: {
    backgroundColor: SUB_COLOR,
    height: 40,
    width: "80%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10,
    marginTop: 10
  },
  imageText: {
    marginLeft: 10,
    color: "white"
  },
  buttonDelete: {
    borderWidth: 1,
    borderColor: '#3b95cd',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10
  },
  buttonDeleteText: {
    color: '#3b95cd'
  },
  textInputStyle: {
    color: MAIN_COLOR,
    fontFamily: "THSarabunNewBold",
    fontSize: 20,
    height: 50,
    width: "100%",
    textAlign: "center"
  },
  textInputStyleGrey: {
    color: MENU_GREY_ITEM,
    fontFamily: "THSarabunNewBold",
    fontSize: 20,
    height: 50,
    width: "100%",
    textAlign: "center"
  },
  buttonImage: {
    width: "50%",
    alignItems: "center"
  },
});
