import {Platform, StyleSheet} from 'react-native';
import { MAIN_COLOR, MENU_GREY } from "../../Constants/Color"

export default StyleSheet.create({
  topic: {
    marginTop: 10,
    marginLeft: 10
  },
  topicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  subTopic: {
    flexDirection: "row",
    height: 'auto',
    minHeight: 30,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
  },
  box: {
    marginTop: 10,
    backgroundColor: "#FFF",
    paddingBottom: 10
  },
  imageText: {
    marginLeft: 10,
    color: "white"
  },
  imageDetail: {
    flexDirection: "row",
    alignItems: "center"
  },
  buttonBox: {
    borderWidth: 2,
    borderColor: "#cccccc",
    borderRadius: 20,
    height: 40,
    marginTop: 5,
    justifyContent: "center",
    alignItems: 'center'
  },
  textTopic: {
    marginLeft: 12,
    color: "#007CC2",
    fontSize: 20,
    fontFamily: "THSarabunNewBold",
    // fontWeight: "bold"
  },
});
