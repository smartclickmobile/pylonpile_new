import {Platform, StyleSheet} from 'react-native';
import { MAIN_COLOR } from '../../Constants/Color'

export default StyleSheet.create({
  topic: {
    marginTop: 10,
    marginLeft: 10
  },
  topicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  slide: {
    borderWidth: 1,
    width: 320
  },
  title: {

  },
  button: {
    borderWidth: 2,
    borderColor: MAIN_COLOR,
    borderRadius: 10,
    padding: 5,
    backgroundColor: '#FFF'
  },
  buttonText: {
    color: MAIN_COLOR,
    fontFamily: 'THSarabunNewBold',
    fontSize: 20
  },
  buttonTextStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  }
});
