import { Platform, StyleSheet } from "react-native"
import {
  MAIN_COLOR,
  SUB_COLOR,
  BASIC_COLOR,
  MENU_GREY_ITEM,
  BUTTON_COLOR,
  BLUE_COLOR
} from "../../Constants/Color"
import DeviceInfo from 'react-native-device-info'
import I18n from "../../../assets/languages/i18n"
export default StyleSheet.create({
  listContainer: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 10
  },
  listTopic: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10
  },
  listTopicText: {
    fontSize: I18n.locale == 'th' ?20:16,
    color: MAIN_COLOR
  },
  listButton: {
    width: DeviceInfo.isTablet() ? 130 : 100,
    height: 40,
    borderRadius: 40,
    backgroundColor: BLUE_COLOR,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15
  },
  listButtonLength: {
    width: 130,
    height: 40,
    borderRadius: 40,
    backgroundColor: '#ebf1f7',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15
  },
  listButtonText: {
    color: BUTTON_COLOR,
    fontFamily: "THSarabunNew",
    fontWeight: DeviceInfo.isTablet() ? "bold" : "normal",
    fontSize: 14
  },
  listButtonTextLength: {
    fontFamily: "THSarabunNew",
    fontWeight: "bold",
    fontSize: 14
  },
  lisTable: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    width: '100%'
  },
  listTableDetail: {
    marginTop: 80
  },
  listTableDetailText: {
    fontFamily: "THSarabunNew",
    fontSize: 20,
    lineHeight: 30,
    textAlign: 'center',
    color: '#000'
  },
  textHighligh: {
    color: BLUE_COLOR
  },
  tableCol: {
    flexDirection: 'column'
  },
  tableHeadGroup: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#daeef8',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#636361',
  },
  tableHead: {
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: 80,
    height: 85,
    padding: 10,
    borderRightWidth: 1,
    borderColor: '#636361'
  },
  tableNo: {
    width: 50
  },
  tableStep: {
    width: 150
  },
  tableHeadTool: {
    width: 125
  },
  tableContentGroup: {
    flexDirection: 'row'
  },
  tableContent: {
    flex: 1,
    // width: 80,
    minHeight: 10,
    flexWrap: 'wrap',
    padding: 10,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#636361',
    alignItems: 'center',
    justifyContent: 'center',
    alignContent:'center'
  },
  tableContentCenter: {
    alignItems: 'center'
  },
  tableContentTool: {
    width: 125,
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap'
  },
  buttonDelete: {
    borderWidth: 1,
    borderColor: '#3b95cd',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    height: 30
  },
  buttonDeleteText: {
    color: '#3b95cd'
  },
  buttonEdit: {
    borderWidth: 1,
    borderColor: '#3b95cd',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 5,
    height: 30
  },
  buttonEditText: {
    color: '#3b95cd'
  },
})
