import {Dimensions, Platform, StyleSheet} from 'react-native'
import {
  MENU_BACKGROUND,
  MENU_GREY_ITEM,
  MENU_GREY,
  MENU_GREY_BORDER,
  MENU_ORANGE,
  MENU_ORANGE_ITEM,
  MENU_ORANGE_BORDER,
  MENU_GREEN,
  MENU_GREEN_ITEM,
  MENU_GREEN_BORDER
} from "../../Constants/Color"
import DeviceInfo from 'react-native-device-info'

const sliderWidth = Dimensions.get("window").width
const itemWidth = sliderWidth * 0.7

export default StyleSheet.create({
  main: {
    height: DeviceInfo.isTablet() ? 50 : 30,
    backgroundColor: "#027AAF",
    justifyContent: "center"
  },
  textMain: {
    marginLeft: 10,
    color: "#FFF",
    // fontSize: 15,
    fontSize: DeviceInfo.isTablet() ? 21 : 14,
  },
  topic: {
    height: DeviceInfo.isTablet() ? 70 : 50,
    borderWidth: 1,
    borderColor: "white",
    flexDirection: "row"
    // backgroundColor: '#039cd2'
  },
  slide: {
    width: itemWidth,
    height: DeviceInfo.isTablet() ? 60 : 30,
    backgroundColor: "green",
    borderRadius: 3,
    flexDirection: "row"
  },
  menuBorder: {
    height: DeviceInfo.isTablet() ? 70 : 40,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: MENU_BACKGROUND,
    flexDirection: "row"
  },
  leftMenu: {
    width: itemWidth * 0.2,
    borderWidth: 0.5,
    borderColor: MENU_GREY_BORDER,
    borderBottomLeftRadius: 3,
    borderTopLeftRadius: 3,
    backgroundColor: MENU_GREY_ITEM,
    alignItems: "center",
    justifyContent: "center"
  },
  centerMenu: {
    width: itemWidth * 0.6,
    backgroundColor: MENU_GREY,
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    borderColor: MENU_GREY_BORDER,
    alignItems: "center",
    justifyContent: "center"
  },
  rightMenu: {
    alignItems: "flex-end",
    justifyContent: "flex-end",
    width: itemWidth * 0.2,
    borderWidth: 0.5,
    borderColor: MENU_GREY_BORDER,
    borderBottomRightRadius: 3,
    borderTopRightRadius: 3,
    backgroundColor: MENU_GREY_ITEM,
    alignItems: "center",
    justifyContent: "center"
  },
  textTopic: {
    color: "#FFF",
    fontSize: DeviceInfo.isTablet() ? 20 : 14,
    textAlign: 'center'
  },
  topicView: {
    backgroundColor: "#039cd2",
    width: "33%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: '0.5%'
  },
  textStyle: {
    fontSize: DeviceInfo.isTablet() ? 25 : 14,
    color: '#000'
  }
})
