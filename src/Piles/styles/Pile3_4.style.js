import {Platform, StyleSheet} from 'react-native';
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM, MENU_GREY_BORDER, MENU_BACKGROUND,MENU_GREEN } from "../../Constants/Color"

export default StyleSheet.create({
  topic: {
    marginTop: 10,
    marginLeft: 10
    // alignItems: "center"
  },
  topicText: {
    fontSize: 17,
    color: MAIN_COLOR
  },
  subTopic: {
    fontSize: 15,
    fontWeight: "bold",
    color: "black",
    marginLeft: 10
  },
  box: {
    height: "auto",
    minHeight: 30,
    flexDirection: "row",
    borderTopWidth: 0.5,
    borderColor: "black"
  },
  leftBox: {
    backgroundColor: MENU_GREY_ITEM,
    width: "45%",
    justifyContent: "center"
  },
  rightBox: {
    width: "55%",
    justifyContent: "center"
  },
  textBox: {
    marginLeft: 10,
    textAlignVertical: "center"
  },
  topicBox: {
    borderTopWidth: 0.5
  },
  button: {
    margin: 10,
    backgroundColor: MAIN_COLOR,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
  button_gray: {
    margin: 10,
    backgroundColor: MENU_GREY_ITEM,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
  button_green: {
    margin: 10,
    backgroundColor: MENU_GREEN,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
  textButton: {
    color: "#FFF",
    fontSize: 22,
    fontFamily: "THSarabunNew",
    fontWeight: "bold"
  },
  imageText: {
    marginLeft: 10,
    color: "white"
  },
  image: {
    backgroundColor: SUB_COLOR,
    height: 40,
    width: "50%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10
  },
});
