import {Platform, StyleSheet} from 'react-native';
import { DEFAULT_COLOR_1,MENU_GREY_ITEM } from "../../Constants/Color"

export default StyleSheet.create({
  second: {
    height: 50,
    flexDirection: "row"
  },
  firstButton: {
    width: "40%",
    backgroundColor: "#BABABA",
    justifyContent: "center",
    alignItems: "center"
  },
  secondButton: {
    width: "60%",
    justifyContent: "center",
    alignItems: "center"
  },
  secondButtonGrey: {
    width: "60%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: MENU_GREY_ITEM
  },
  selectButton: {
    color: "#FFF",
    fontSize: 20
  }
});
