import {Platform, StyleSheet} from 'react-native';
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM } from "../../Constants/Color"

export default StyleSheet.create({
  topic: {
    marginTop: 10,
    marginLeft: 10
  },
  topicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  boxTopic: {
    marginLeft: 10,
    marginTop: 10
  },
  boxColor: {
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: SUB_COLOR,
    margin: 10
  },
  selectedView: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10
  },
  button: {
    borderWidth: 2,
    borderColor: MAIN_COLOR,
    borderRadius: 10,
    padding: 5,
    backgroundColor: "#FFF"
  },
  buttonText: {
    color: MAIN_COLOR,
    fontFamily: "THSarabunNewBold",
    fontSize: 20
  },
  buttonTextStyle: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  textInputStyle: {
    color: MAIN_COLOR,
    fontFamily: "THSarabunNewBold",
    fontSize: 20,
    height: 50,
    width: "100%",
    textAlign: "center"
  },
  textInputStyleGrey: {
    color: MENU_GREY_ITEM,
    fontFamily: "THSarabunNewBold",
    fontSize: 20,
    height: 50,
    width: "100%",
    textAlign: "center"
  }
});
