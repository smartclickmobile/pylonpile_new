import { Platform, StyleSheet } from "react-native"
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM } from "../../Constants/Color"

export default StyleSheet.create({
  topic: {
    marginTop: 10,
    marginLeft: 10
  },
  topicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  checkbox: {
    marginTop: 10,
    flexDirection: "row",
    // justifyContent: "center",
    alignItems: "center",
    width: "100%",
    borderColor: MENU_GREY_ITEM,
    paddingBottom: 10,
  },
  checkboxText: {
    marginLeft: 5
  },
  inputView: {
    borderWidth: 0.8,
    width: '70%',
    marginLeft: 20,
    height: 40
  }
})
