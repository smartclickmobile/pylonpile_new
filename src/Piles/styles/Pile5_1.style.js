import {Platform, StyleSheet} from 'react-native';
import { MAIN_COLOR, SUB_COLOR, BASIC_COLOR, MENU_GREY_ITEM, BUTTON_COLOR, BLUE_COLOR } from "../../Constants/Color"

export default StyleSheet.create({
  listContainer: {
    flex: 1,
    padding: 10
  },
  listTopic: {

  },
  topic: {
    marginTop: 10,
    marginLeft: 10
  },
  topicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  boxTopic: {
    // marginLeft: 10,
    marginTop: 10
  },
  textInputStyleGrey: {
    color: MENU_GREY_ITEM,
    fontFamily: "THSarabunNewBold",
    fontSize: 20,
    height: 50,
    width: "100%",
    textAlign: "center"
  },
  listTopicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  listTopicSub: {
    marginTop: 10
  },
  listTopicSubText: {
  },
  selectedView: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10
  },
  button: {
    borderWidth: 2,
    borderColor: MAIN_COLOR,
    borderRadius: 10,
    padding: 5,
    backgroundColor: "#FFF"
  },
  textInputStyleGrey: {
    color: MENU_GREY_ITEM,
    fontFamily: "THSarabunNewBold",
    fontSize: 20,
    height: 50,
    width: "100%",
    textAlign: "center"
  },
  textInputStyle: {
    color: MAIN_COLOR,
    fontFamily: "THSarabunNewBold",
    fontSize: 20,
    height: 50,
    width: "100%",
    textAlign: "center"
  },
  listButton: {
    width: 120,
    height: 30,
    borderRadius: 40,
    backgroundColor: BLUE_COLOR,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15
  },
  listButtonView:{
    backgroundColor: '#eceeee',
    justifyContent: 'center'
  },
  listButtonText: {
    color: BASIC_COLOR,
    fontFamily: "THSarabunNew",
    fontWeight: "bold",
    fontSize: 14,
  },
  imageDetail: {
    flexDirection: "row",
    alignItems: "center"
  },
  image: {
    backgroundColor: SUB_COLOR,
    height: 40,
    width: "80%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10
  },
  imageText: {
    marginLeft: 10,
    color: "white"
  },
  buttonImage: {
    width: "50%",
    alignItems: "center"
  },
  listCheckbox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 15
  },
  listCheckboxBase: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderColor: MENU_GREY_ITEM,
  }
});
