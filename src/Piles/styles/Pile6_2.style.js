import {Platform, StyleSheet} from 'react-native';
import { MAIN_COLOR, SUB_COLOR, BASIC_COLOR, MENU_GREY_ITEM, BUTTON_COLOR, BLUE_COLOR } from "../../Constants/Color"

export default StyleSheet.create({
  listContainer: {
    flex: 1,
    padding: 10
  },
  listTopic: {

  },
  listTopicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  listTopicRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  listTopicSub: {
    marginTop: 10
  },
  listTopicSubText: {

  },
  listTextbox:{
    borderWidth: 2,
    borderColor: "#cccccc",
    borderRadius: 20,
    height: 40,
    marginTop: 5,
    color: BASIC_COLOR,
    textAlign: "center",
    width:'100%'
  },
  textboxDisable: {
    backgroundColor: '#ddeef0',
    color: "#686968"
  },
  listSelectedView: {
    marginTop: 5
  },
  listButton: {
    width: 120,
    height: 30,
    borderRadius: 40,
    backgroundColor: BLUE_COLOR,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15
  },
  listButtonText: {
    color: BASIC_COLOR,
    fontFamily: "THSarabunNew",
    fontWeight: "bold",
    fontSize: 14
  },
  buttonBox: {
    borderWidth: 2,
    borderColor: "#cccccc",
    borderRadius: 20,
    height: 40,
    marginTop: 5,
    justifyContent: "center",
    alignItems: 'center'
  },
  approveBox: {
    borderWidth: 1,
    borderColor: "#cccccc",
    backgroundColor: SUB_COLOR,
    borderRadius: 20,
    height: 40,
    width: "28%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5,
    marginLeft: 5
  },
  imageDetail: {
    flexDirection: "row",
    alignItems: "center"
  },
  image: {
    backgroundColor: SUB_COLOR,
    height: 40,
    width: "80%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10
  },
  imageText: {
    marginLeft: 10,
    color: "white"
  },
  buttonImage: {
    width: "46%",
    alignItems: "flex-end",
    marginRight:20
  },
  icon: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: "54%",
    borderColor: MENU_GREY_ITEM,
  }
});
