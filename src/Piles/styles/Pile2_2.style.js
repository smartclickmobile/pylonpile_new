import {Platform, StyleSheet} from 'react-native';
import { MAIN_COLOR, SUB_COLOR } from "../../Constants/Color"

export default StyleSheet.create({
  topic: {
    marginTop: 10,
    marginLeft: 10
  },
  topicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  form: {
    marginTop: 20,
    alignItems: "center"
  },
  topicBox: {
    backgroundColor: "#FFF",
    height: 40,
    justifyContent: "center"
  },
  textTopicBox: {
    marginLeft: 12,
    color: "#007CC2",
    fontSize: 20,
    fontFamily: "THSarabunNewBold",
    fontWeight: "bold"
  },
  box: {
    marginTop: 10,
    backgroundColor: "#FFF",
    paddingBottom: 10,
    borderWidth: 0.5,
    marginLeft: 5,
    marginRight: 5
  },
  buttonBox: {
    borderWidth: 2,
    borderColor: "#cccccc",
    borderRadius: 20,
    height: 40,
    marginTop: 5,
    justifyContent: "center",
    alignItems: 'center'
  },
  approveBox: {
    borderWidth: 1,
    borderColor: "#cccccc",
    backgroundColor: SUB_COLOR,
    borderRadius: 20,
    height: 40,
    width: "28%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5,
    marginLeft: 5
  },
  imageDetail: {
    flexDirection: "row",
    alignItems: "center"
  },
  image: {
    backgroundColor: SUB_COLOR,
    height: 40,
    width: "80%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10
  },
  imageText: {
    marginLeft: 10,
    color: "white"
  }
});
