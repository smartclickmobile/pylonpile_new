import {Platform, StyleSheet} from 'react-native';
import { MAIN_COLOR, SUB_COLOR, BASIC_COLOR, MENU_GREY_ITEM, BUTTON_COLOR, BLUE_COLOR } from "../../Constants/Color"

export default StyleSheet.create({
  topic: {
    marginTop: 10,
    marginLeft: 10
  },
  topicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  boxTopic: {
    marginLeft: 10,
    marginTop: 10
  },
  boxColor: {
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: SUB_COLOR,
    margin: 10
  },
  listContainer: {
    flex: 1,
    padding: 10
  },
  listTopic: {

  },
  listTopicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  listTopicSub: {
    marginTop: 10
  },
  listTopicSubText: {
  },
  listButton: {
    width: 120,
    height: 30,
    borderRadius: 40,
    backgroundColor: BLUE_COLOR,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15
  },
  listButtonView:{
    backgroundColor: '#eceeee',
    justifyContent: 'center'
  },
  listButtonText: {
    color: BASIC_COLOR,
    fontFamily: "THSarabunNew",
    fontWeight: "bold",
    fontSize: 14,
  },
  imageDetail: {
    flexDirection: "row",
    alignItems: "center"
  },
  image: {
    backgroundColor: SUB_COLOR,
    height: 40,
    width: "80%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10
  },
  imageText: {
    marginLeft: 10,
    color: "white"
  },
  buttonImage: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-end"
  },
  listCheckbox: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 15
  },
  listCheckboxBase: {
    flexDirection: "row",
    flex: 1.5,
    justifyContent: "center",
    alignItems: "center",
    borderColor: MENU_GREY_ITEM
  },
  textFlex: {
    flex: 1
  },

  listTime: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },
  listTimeLeft: {
    alignItems: 'flex-start'
  },
  buttonBox: {
    borderWidth: 2,
    borderColor: "#cccccc",
    borderRadius: 20,
    height: 40,
    marginTop: 5,
    justifyContent: "center",
    alignItems: 'center'
  },
  listTimeRow: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%'
  },
  rowStart: {
    marginBottom: 10
  },
  listTimeCol: {
    flexDirection: 'column',
    alignItems: 'center'
  },
  listTimeBorderBefore: {
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 1,
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 10
  },
  listTimeBorderBeforeAfter: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#ccc',
    paddingTop: 10,
    marginBottom: 10
  },
  textInputStyleGrey: {
    color: MENU_GREY_ITEM,
    fontFamily: "THSarabunNewBold",
    fontSize: 16,
    height: 40,
    width: "100%",
    textAlign: "center"
  },
  textInputStyle: {
    color: MAIN_COLOR,
    fontFamily: "THSarabunNewBold",
    fontSize: 16,
    height: 40,
    width: "100%",
    textAlign: "center"
  },
  buttonEdit: {
    borderWidth: 1,
    borderColor: '#3b95cd',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 5
  },
  buttonEditText: {
    color: '#3b95cd'
  },
  buttonStart: {
    backgroundColor: '#2d91ce',
    width: 80,
    height: 35,
    borderRadius: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15
  },
  buttonStop: {
    backgroundColor: '#b6b7b6',
    width: 80,
    height: 35,
    borderRadius: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15
  },
  buttonCenter: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonCenterImage: {
    width: 200,
    alignItems: 'center',
    alignSelf: 'center'
  },
  buttonImageView: {
    backgroundColor: '#cccbca'
  },
  button: {
    borderWidth: 2,
    borderColor: MAIN_COLOR,
    borderRadius: 10,
    padding: 10,
    paddingHorizontal:20,
    backgroundColor: '#FFF'
  },
  buttonText: {
    color: MAIN_COLOR,
    fontFamily: 'THSarabunNewBold',
    fontSize: 14
  },
  buttonTextStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  buttonFixed: {
    flexDirection: 'column',
    flex: 1,
    position: 'absolute',
    bottom: 0
  },
  second: {
    height: 50,
    backgroundColor: MAIN_COLOR,
    flexDirection: 'row'
  },
  firstButton: {
    width: '40%',
    backgroundColor: '#BABABA',
    justifyContent: 'center',
    alignItems: 'center'
  },
  secondButton: {
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  selectButton: {
    color: '#FFF',
    fontSize: 20
  },
  image: {
    backgroundColor: SUB_COLOR,
    height: 40,
    width: "80%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10,
    marginTop: 10
  },
  imageText: {
    marginLeft: 10,
    color: "white",
    fontSize: 10
  },
  buttonDelete: {
    borderWidth: 1,
    borderColor: '#3b95cd',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10
  },
  buttonDeleteText: {
    color: '#3b95cd'
  }
});
