import {Platform, StyleSheet} from 'react-native'
import { MAIN_COLOR } from '../../Constants/Color'

export default StyleSheet.create({
  second: {
    height: 50,
    // backgroundColor: MAIN_COLOR,
    flexDirection: 'row'
  },
  firstButton: {
    width: '40%',
    backgroundColor: '#BABABA',
    justifyContent: 'center',
    alignItems: 'center'
  },
  secondButton: {
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  selectButton: {
    color: '#FFF',
    fontSize: 20
  }
})