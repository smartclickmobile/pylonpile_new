import {Platform, StyleSheet} from 'react-native';
import { MAIN_COLOR, SUB_COLOR, DEFAULT_COLOR_1, DEFAULT_COLOR_4 } from "../../Constants/Color"

export default StyleSheet.create({
  topic: {
    backgroundColor: "#FFF",
    height: 40,
    justifyContent: "center"
  },
  approveBox: {
    borderWidth: 1,
    borderColor: "#cccccc",
    backgroundColor: SUB_COLOR,
    borderRadius: 20,
    height: 40,
    width: "28%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5,
    marginLeft: 5
  },
  buttonBox: {
    borderWidth: 2,
    borderColor: "#cccccc",
    borderRadius: 20,
    height: 40,
    marginTop: 5,
    justifyContent: "center",
    alignItems: 'center'
  },
  topic_grey: {
    backgroundColor: "#CDD2D2",
    height: 40,
    justifyContent: "center"
  },
  textTopic: {
    marginLeft: 12,
    color: "#007CC2",
    fontSize: 20,
    fontFamily: "THSarabunNewBold",
    fontWeight: "bold"
  },
  sub: {
    backgroundColor: "#FFF",
    marginTop: 2,
    flexDirection: "row",
    height: "auto",
    alignItems: "center",
    paddingTop: 2,
    paddingBottom: 2
  },
  subtopic: {
    width: "40%"
  },
  textSubTopic: {
    marginLeft: 12,
    fontFamily: "THSarabunNewBold",
    fontSize: 15
  },
  box: {
    marginTop: 10,
    backgroundColor: "#FFF",
    paddingBottom: 10
  },
  box_grey: {
    marginTop: 10,
    backgroundColor: "#CDD2D2",
    paddingBottom: 10
  },
  imageDetail: {
    flexDirection: "row",
    alignItems: "center"
  },
  image: {
    backgroundColor: SUB_COLOR,
    height: 40,
    width: "80%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10
  },
  imageText: {
    marginLeft: 10,
    color: "white"
  },

  second: {
    height: 50,
    backgroundColor: MAIN_COLOR,
    flexDirection: 'row'
  },
  second_dwall: {
    height: 50,
    backgroundColor: DEFAULT_COLOR_1,
    flexDirection: 'row'
  },
  second_super:{
    height: 50,
    backgroundColor: DEFAULT_COLOR_4,
    flexDirection: 'row'
  },
  firstButton: {
    width: '40%',
    backgroundColor: '#BABABA',
    justifyContent: 'center',
    alignItems: 'center'
  },
  secondButton: {
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  selectButton: {
    color: '#FFF',
    fontSize: 20
  },
  secondButtonGrey: {
    width: "60%",
    backgroundColor: "#BABABA",
    justifyContent: "center",
    alignItems: "center"
  },
});
