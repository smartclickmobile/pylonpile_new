import {Platform, StyleSheet} from 'react-native';
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM } from "../../Constants/Color"

export default StyleSheet.create({
  boxTopic: {
    borderBottomWidth: 0.5,
    paddingBottom: 10
  },
  topic: {
    marginTop: 10,
    marginLeft: 10
  },
  boxValue: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    flexDirection: "row",
    height: 40,
    justifyContent: "center"
    // alignItems: 'center'
  },
  valueLeft: {
    width: "40%",
    height: 40,
    justifyContent: "center"
  },
  valueRight: {
    width: "60%",
    height: 40,
    justifyContent: "center",
    backgroundColor: SUB_COLOR
  },
  valueRightText: {
    marginLeft: 5,
    textAlign: 'right'
  },
  image: {
    backgroundColor: SUB_COLOR,
    height: 40,
    width: "50%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10
  },
  imageText: {
    marginLeft: 10,
    color: "white"
  },
  header: {
    margin: 10,
    backgroundColor: "#ff7a73",
    height: 40,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  }
});
