import {Platform, StyleSheet} from 'react-native';
import {
  MAIN_COLOR,
  SUB_COLOR,
  BASIC_COLOR,
  MENU_BACKGROUND,
  MENU_GREY_ITEM,
  BUTTON_COLOR,
  BLUE_COLOR
} from "../../Constants/Color"

export default StyleSheet.create({
  listContainer: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 10
  },
  listTopic: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10
  },
  listTopicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  listTopicSub: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 10
  },
  listTopicSubText: {},
  listTopicSubTextModal: {
    color: '#fff'
  },
  listTextbox: {
    borderWidth: 2,
    borderColor: "#cccccc",
    borderRadius: 20,
    minWidth: 100,
    height: 40,
    marginTop: 5,
    color: BASIC_COLOR,
    textAlign: "center"
  },
  listTopicSub2: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10
  },
  listTopicSubText2: {
    fontFamily: "THSarabunNew",
    fontWeight: "bold",
    fontSize: 14,
    color: BASIC_COLOR
  },
  listTextboxModal: {
    backgroundColor: '#fff'
  },
  textboxDisable: {
    backgroundColor: '#ddeef0',
    color: "#686968"
  },
  listButton: {
    width: 130,
    height: 40,
    borderRadius: 40,
    backgroundColor: BLUE_COLOR,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15
  },
  listButtonText: {
    color: BUTTON_COLOR,
    fontFamily: "THSarabunNew",
    fontWeight: "bold",
    fontSize: 14
  },
  listTable: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  listTableDetail: {
    marginTop: 100
  },
  listTableDetailText: {
    fontFamily: "THSarabunNew",
    fontSize: 20,
    textAlign: 'center'
  },
  textHighligh: {
    color: BLUE_COLOR
  },
  tableView: {
    flexDirection: 'column',
    flex: 1
  },
  tableViewHeadGroup: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#daeef8',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#636361'
  },
  tableViewHead: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: 80,
    height: 55,
    padding: 10,
    borderRightWidth: 1,
    borderColor: '#636361'
  },
  tableViewHeadText: {
    textAlign: 'center',
    color: '#000'
  },
  tableViewContentGroup: {
    flexDirection: 'row'
  },
  tableViewContent: {
    flex: 1,
    width: 80,
    minHeight: 30,
    flexWrap: 'wrap',
    padding: 10,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#636361'
  },
  tableViewContentText: {
    textAlign: 'center',
    color: '#000'
  },
  tableViewSM: {
    width: 50
  },
  tableViewMD: {
    width: 130
  },
  tableViewLG: {
    width: 200
  },
  tableViewTool: {
    width: 120
  },
  tableAction: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  buttonEdit: {
    borderWidth: 1,
    borderColor: '#3b95cd',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 5,
    marginRight: 5,
    height: 30
  },
  buttonEditText: {
    color: '#3b95cd'
  },
  buttonStart: {
    backgroundColor: '#2d91ce',
    width: 45,
    height: 30,
    borderRadius: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft: 2
  },
  buttonStop: {
    backgroundColor: '#b6b7b6',
    width: 45,
    height: 30,
    borderRadius: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft: 2
  },
  button: {
    borderWidth: 1,
    borderRadius: 10,
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: "#FFF"
  },
  buttonText: {
    color: BASIC_COLOR
  },
  buttonTextStyle: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  buttonSelectorIcon: {
    justifyContent: "flex-end",
    flexDirection: "row"
  },
  modalContainer: {
    backgroundColor: 'rgba(0,0,0,0.8)',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalCloseButton: {
    position: 'absolute',
    right: 20,
    top: 20
  },
  modalDetail: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonDelete: {
    borderWidth: 1,
    borderColor: '#3b95cd',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    height: 30
  },
  buttonDeleteText: {
    color: '#3b95cd'
  },


//-- new table --//
  tableRow: {
    flexDirection: 'row'
  },
  tableCol: {
    flexDirection: 'column',
    // width: '25%'
  },
  tableHead: {
    backgroundColor: '#daeef8',
    // width: 90, 
    // width: '25%',
    minHeight: 60,
    alignItems: 'center',
    padding: 10,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#636361',
  },
  tableHeadText: {
    textAlign: 'center',
    color: '#000'
  },
  tableContent: {
    // width: 90,
    flex: 1,
    alignItems: 'center',
    flexWrap: 'wrap',
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#636361'
  },
  tableContentText: {
    textAlign: 'center',
    color: '#000'
  },
  tableContentCol: {
    flexDirection: 'column',
    flex: 1,
    width: '100%',
    height: '100%'
  },
  tableContentArea: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    padding: 2,
    minHeight: 60
  },
  tableContentBorder: {
    borderBottomWidth: 1,
    borderColor: '#636361'
  },
  tabelExtra: {
    width: 120
  },
  scrollData: {
    flex: 1,
    width: '100%'
  },
  scrollLeft: {
    flex: 1,
    width: '100%'
  },
  listCheckbox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 15
  },
  listCheckboxBase: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderColor: MENU_GREY_ITEM,
  },
  image: {
    backgroundColor: SUB_COLOR,
    height: 40,
    width: "80%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10
  },
  imageText: {
    marginLeft: 10,
    color: "white"
  },
  buttonImage: {
    width: "50%",
    alignItems: "center"
  },
});
