import { Platform, StyleSheet } from "react-native"
import { MAIN_COLOR, SUB_COLOR, BASIC_COLOR, MENU_GREY_ITEM, MENU_GREEN } from "../../Constants/Color"

export default StyleSheet.create({
  listContainer: {
    flex: 1,
    backgroundColor: "#fff"
  },
  listNavigation: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: MAIN_COLOR,
    height: 60
  },
  listNavigationStep: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#BABABA",
    height: 40
  },
  listNavigationTextStep: {
    color: "#000",
    fontSize: 15,
    fontWeight: "bold"
  },
  listNavigationText: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold"
  },
  listRow: {
    flexDirection: "column",
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 60,
    // backgroundColor: 'red'
  },
  listTopicNumber: {
    backgroundColor: "#cccccc",
    height: 40,
    marginTop: 5
  },
  listTopicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  listTopicSub: {
    marginTop: 10
  },
  listTopicSubText: {},
  listTopicSubRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 20
  },
  listTextbox: {
    borderWidth: 2,
    borderColor: "#cccccc",
    borderRadius: 20,
    height: 40,
    marginTop: 5,
    color: BASIC_COLOR,
    textAlign: "center"
  },
  listicondatebox:{
    borderWidth: 2,
    borderColor: "#cccccc",
    borderRadius: 20,
    height: 40,
    marginTop: 5,
    // color: BASIC_COLOR,
    // textAlign: "center"
  },
  listTextboxFlex: {
    flex: 1
  },
  textboxDisable: {
    backgroundColor: "#ddeef0",
    color: "#686968"
  },
  listSelectedView: {
    marginTop: 5
  },
  listButton: {
    borderWidth: 2,
    borderColor: MAIN_COLOR,
    borderRadius: 20,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 10
  },
  listButtonStyle: {
    justifyContent: "flex-start",
    alignItems: "center",
    flexDirection: "row"
  },
  listButtonText: {
    color: MAIN_COLOR,
    fontFamily: "THSarabunNew",
    fontWeight: "bold",
    fontSize: 14
  },
  listButtonIcon: {
    position: "absolute",
    right: 0,
    marginRight: 10
  },
  listLabel: {
    flex: 2
  },
  listButtonImage: {
    backgroundColor: "#2fa8df",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    height: 40,
    marginLeft: 10
  },
  buttonImageView: {
    backgroundColor: "#cccbca"
  },
  button: {
    borderWidth: 2,
    borderColor: MAIN_COLOR,
    borderRadius: 10,
    padding: 5,
    backgroundColor: "#FFF"
  },
  buttonText: {
    color: MAIN_COLOR,
    fontFamily: "THSarabunNewBold",
    fontSize: 20
  },
  buttonTextStyle: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  buttonFixed: {
    flexDirection: "column",
    // flex: 1,
    // position: "absolute",
    // bottom: 0
  },
  second: {
    height: 50,
    backgroundColor: MAIN_COLOR,
    flexDirection: "row"
  },
  firstButton: {
    width: "40%",
    backgroundColor: "#BABABA",
    justifyContent: "center",
    alignItems: "center"
  },
  secondButton: {
    width: "60%",
    justifyContent: "center",
    alignItems: "center"
  },
  selectButton: {
    color: "#FFF",
    fontSize: 20
  },
  imageDetail: {
    flexDirection: "row",
    alignItems: "center"
  },
  image: {
    backgroundColor: SUB_COLOR,
    height: 40,
    width: "80%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10
  },
  imageText: {
    marginLeft: 10,
    color: "white"
  },
  buttonImage: {
    flex: 2,
    alignItems: "center"
  },
  lastView: {
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    borderColor: MENU_GREY_ITEM,
    paddingBottom: 10
  },
  approveBox: {
    borderWidth: 1,
    borderColor: "#cccccc",
    backgroundColor: SUB_COLOR,
    borderRadius: 20,
    height: 40,
    width: "28%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5,
    marginLeft: 5
  },
  buttonBox: {
    borderWidth: 2,
    borderColor: "#cccccc",
    borderRadius: 20,
    height: 40,
    marginTop: 5,
    justifyContent: "center",
    alignItems: 'center'
  },
  buttonStyle: {
    height: 40,
    width: '90%',
    backgroundColor: '#f7a81d',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  topicText: {
    color: '#2fa8df',
    fontSize: 18,
    marginLeft: 10
  },
  dropDownTopic: { 
    borderTopWidth: 1, 
    borderRightWidth: 1, 
    borderLeftWidth: 1, 
    height: 50, 
    justifyContent: 'center', 
    alignItems: 'center',
    backgroundColor: MENU_GREEN
   },
   checkView: {
    marginTop: 10,
    flexDirection: "row",
    // justifyContent: "center",
    alignItems: "center",
    width: "100%",
    borderColor: MENU_GREY_ITEM,
    paddingBottom: 10
  },
  button2: {
    margin: 10,
    backgroundColor: MAIN_COLOR,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
})
