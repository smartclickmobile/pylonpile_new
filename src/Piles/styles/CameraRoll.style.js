import {Dimensions, Platform, StyleSheet} from 'react-native'
import {MAIN_COLOR} from "../../Constants/Color"

const {height, width} = Dimensions.get('window')
export default StyleSheet.create({
  //---------- start navigation style ----------//
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  navigation: {
    paddingTop: Platform.OS === 'ios'
      ? 25
      : 10,
    backgroundColor: 'transparent',
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1
  },
  navigationBlue: {
    backgroundColor: '#007cc2',
    borderBottomWidth: 1,
    borderColor: '#848789'
  },
  buttonLeft: {
    position: 'absolute',
    top: 4,
    left: 30
  },
  buttonLeftResize: {
    width: 25,
    height: 25
  },
  buttonOption: {
    marginTop: 14,
    marginLeft: -5
  },
  buttonRight: {
    position: 'absolute',
    top: 0,
    right: 20
  },
  buttonText: {
    fontFamily: 'Prompt-Medium',
    fontSize: 16,
    color: '#c37f6f'
  },
  navigationTitle: {
    flexDirection: 'row',
    justifyContent: 'center',
    minHeight: 30
  },
  navigationTitleRow: {
    flexDirection: 'row'
  },
  navigationTitleText: {
    color: '#fff',
    fontSize: 20,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10
  },
  //---------- end navigation style ----------//

  //---------- start camera roll style ----------//
  cameraRollContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  cameraRollRow: {
    maxWidth: '100%',
    flex: 1,
    minWidth: Dimensions.get('window').width-20,
    marginTop: 80,
    paddingLeft: 10,
    paddingRight: 10
  },
  cameraRollList: {
    width: (Dimensions.get('window').width-50)/3,
    height: (Dimensions.get('window').width-50)/3,
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: '#bababa',
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 10
  },
  cameraRollImg: {
    width: (Dimensions.get('window').width-50)/3,
    height: (Dimensions.get('window').width-50)/3
  },
  cameraRollImgBorderGallary: {
    width: (Dimensions.get('window').width-50)/3,
    height: (Dimensions.get('window').width-50)/3,
    borderTopWidth: 2,
    borderRightWidth: 2,
    borderBottomWidth: 2,
    borderColor: '#bababa',
  },
  cameraRollImgBorder: {
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 10,
    borderTopWidth: 2,
    borderRightWidth: 2,
    borderBottomWidth: 2,
    borderColor: '#bababa'
  },
  cameraButtonGroup: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: '100%',
    // paddingBottom: 50,
    // flex: 0.4,
    // marginTop: 20,
    backgroundColor: 'transparent',
    // borderWidth: 1,
    // borderColor: 'blue'
  },
  cameraRollButton: {
    width: 28,
    height: 28,
    borderRadius: 8,
    borderWidth: 2,
    borderColor: '#fff'
  },
  previewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '92%',
    backgroundColor: '#fff'
  },
  preview: {
    flex: 1,
    width: '100%',
    height: '92%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: 'rgba(0,0,0,0.9)'
  },
  previewImg: {
    width: '100%',
    height: '100%',
    // resizeMode: 'contain'
  },
  previewCropImg: {
    width: 350,
    height: 350,
    borderRadius: 350,
    position: 'absolute'
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40
  },
  //---------- end camera roll style ----------//
  // cameraRollContainer: {
  //   width: 600,
  //   maxWidth: '100%'
  // },
  cameraButtonPosition: {
    // flex: 1,
    width: width,
    height: width / 3 * 4,
    justifyContent: 'flex-end',
    alignItems: 'center',
    // backgroundColor: 'transparent'
  },
  camratio: {
    // flex: 1,
    width: width,
    height: width / 3 * 4,
    // justifyContent: 'flex-end',
    // alignItems: 'center'
  },
  buttonDelete: {
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 1
  },
  buttonAdd: {
    justifyContent: 'center',
    alignItems: 'center'
  },

  buttonFixed: {
    flexDirection: 'column',
    flex: 1,
    position: 'absolute',
    bottom: 0
  },
  second: {
    height: 50,
    // backgroundColor: MAIN_COLOR,
    flexDirection: 'row'
  },
  firstButton: {
    width: '40%',
    backgroundColor: '#BABABA',
    justifyContent: 'center',
    alignItems: 'center'
  },
  secondButton: {
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  selectButton: {
    color: '#FFF',
    fontSize: 20
  },
  preview2: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  saveimage:{
    height:'8%',
    width:'100%',
    alignContent:'center'
  }
})
