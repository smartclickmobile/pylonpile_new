import {Platform, StyleSheet} from 'react-native';
import { MENU_GREY_ITEM, MAIN_COLOR } from "../../Constants/Color"

export default StyleSheet.create({
  topic: {
    marginTop: 10,
    marginLeft: 10,
    alignItems: "center"
  },
  topicText: {
    fontSize: 30,
    color: MAIN_COLOR
  },
  scroll: {
    flex: 1,
    margin: 10,
    paddingTop: 30,
    paddingBottom: 30,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 20,
    backgroundColor: "#C0C0C0"
  },
  scrollText: {
    color: "black",
    fontSize: 14,
    lineHeight: 30
  },
  textBold: {
    fontWeight: "bold",
    fontSize: 16,
    textAlign: "center",
    paddingBottom: 20
  },
  acceptStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    borderColor: MENU_GREY_ITEM,
    paddingBottom: 10
  },
  button: {
    margin: 10,
    backgroundColor: MAIN_COLOR,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
  textButton: {
    color: "#FFF",
    fontSize: 22,
    fontFamily: "THSarabunNew",
    fontWeight: "bold"
  },
  textCheck: {
    color: "black"
  }
});
