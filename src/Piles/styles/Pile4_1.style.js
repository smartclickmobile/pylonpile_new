import {Platform, StyleSheet} from 'react-native';
import {
  MAIN_COLOR,
  SUB_COLOR,
  BASIC_COLOR,
  MENU_GREY_ITEM,
  BUTTON_COLOR,
  BLUE_COLOR
} from "../../Constants/Color"

export default StyleSheet.create({
  listContainer: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 10
  },
  listTopic: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10
  },
  listTopicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  listButton: {
    width: 130,
    height: 40,
    borderRadius: 40,
    backgroundColor: BLUE_COLOR,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15
  },
  listButtonText: {
    color: BUTTON_COLOR,
    fontFamily: "THSarabunNew",
    fontWeight: "bold",
    fontSize: 14
  },
  listTable: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  listTableDetail: {
    marginTop: 80
  },
  listTableDetailText: {
    fontFamily: "THSarabunNew",
    fontSize: 20,
    lineHeight: 30,
    textAlign: 'center',
    color: '#000'
  },
  textHighligh: {
    color: BLUE_COLOR
  },
  tableView: {
    flexDirection: 'column',
    flex: 1
  },
  tableViewHeadGroup: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#daeef8',
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#636361',
    flex: 1,
    height: 40
  },
  tableViewHead: {
    width: 100,
    height: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    borderRightWidth: 1,
    borderColor: '#636361'
  },
  tableViewHeadText: {
    textAlign: 'center',
    color: '#000'
  },
  tableViewContentGroup: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    flex: 1,
    minHeight: 40
  },
  tableViewContent: {
    width: 100,
    height: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    borderRightWidth: 1,
    borderColor: '#636361'
  },
  tableViewStep: {
    width: 100,
    borderWidth: 1,
    borderColor: 'red'
  },
  tableViewContentText: {
    textAlign: 'center',
    color: '#000'
  },
  tableViewContentStepText: {
    color: '#000'
  },
  buttonDelete: {
    borderWidth: 1,
    borderColor: '#3b95cd',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    height: 30
  },
  buttonDeleteText: {
    color: '#3b95cd'
  },
  buttonEdit: {
    borderWidth: 1,
    borderColor: '#3b95cd',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 5,
    marginRight: 5,
    height: 30
  },
  buttonEditText: {
    color: '#3b95cd'
  },
  tableAction: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  tableViewMD: {
    width: 180
  },

  tableCol: {
    flexDirection: 'column'
  },
  tableHeadGroup: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#daeef8',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#636361'
  },
  tableNo: {
    width: 50
  },
  tableStep: {
    width: 150
  },
  tableHeadTool: {
    width: 125
  },
  tableContentGroup: {
    flexDirection: 'row'
  },
  tableContentCol: {
    flexDirection: 'column',
    flex: 1,
  },
  tableContentCenter: {
    alignItems: 'center'
  },
  tableContentTool: {
    width: 125,
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap'
  },

  //-- new table --//
  tableRow: {
    flexDirection: 'row'
  },
  tableCol: {
    flexDirection: 'column',
    // width: '25%'
  },
  tableHead: {
    backgroundColor: '#daeef8',
    // width: 90, 
    // width: '25%',
    minHeight: 50,
    alignItems: 'center',
    padding: 10,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#636361',
  },
  tableHeadText: {
    textAlign: 'center',
    color: '#000'
  },
  tableContent: {
    // width: 90,
    height: 100,
    flex: 1,
    alignItems: 'center',
    flexWrap: 'wrap',
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#636361'
  },
  tableContentText: {
    textAlign: 'center',
    color: '#000'
  },
  tableContentCol: {
    flexDirection: 'column',
    flex: 1,
    width: '100%',
    height: '100%'
    
  },
  tableContentArea: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    padding: 5,
    minHeight: 70
  },
  tableContentBorder: {
    borderBottomWidth: 1,
    borderColor: '#636361'
  },
  tabelExtra: {
    width: 120
  },
  scrollData: {
    flex: 1,
    width: '100%'
  },
  scrollLeft: {
    flex: 1,
    width: '100%'
  }
});
