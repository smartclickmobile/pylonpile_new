import {Platform, StyleSheet} from 'react-native';
import { MAIN_COLOR, SUB_COLOR, BASIC_COLOR, MENU_GREY_ITEM } from "../../Constants/Color"

export default StyleSheet.create({
  listContainer: {
    flex: 1,
    padding: 10
  },
  listTopic: {

  },
  listTopicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  listTopicSub: {
    marginTop: 10
  },
  listTopicSubText: {

  },
  listSelectedView: {
    marginTop: 5
  },
  listButton: {
    borderWidth: 2,
    borderColor: MAIN_COLOR,
    borderRadius: 20,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 10
  },
  listButtonStyle: {
    justifyContent: "flex-start",
    alignItems: "center",
    flexDirection: "row"
  },
  listButtonText: {
    color: MAIN_COLOR,
    fontFamily: "THSarabunNew",
    fontWeight: "bold",
    fontSize: 14
  },
  listButtonIcon: {
    position: "absolute",
    right: 0
  },
  button: {
    borderWidth: 2,
    borderColor: MAIN_COLOR,
    borderRadius: 10,
    padding: 5,
    backgroundColor: '#FFF'
  },
  buttonText: {
    color: MAIN_COLOR,
    fontFamily: 'THSarabunNewBold',
    fontSize: 18
  },
  buttonTextStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  buttonSelectorIcon: {
    justifyContent: 'flex-end',
    flexDirection: 'row'
  },
  icon: {
    flexDirection: "row",
    // justifyContent: "center",
    alignItems: "center",
    borderColor: MENU_GREY_ITEM,
  }
});
