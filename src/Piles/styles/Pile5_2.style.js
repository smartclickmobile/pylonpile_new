import {Platform, StyleSheet} from 'react-native';
import { MAIN_COLOR, SUB_COLOR, BASIC_COLOR, MENU_GREY_ITEM, BUTTON_COLOR, BLUE_COLOR } from "../../Constants/Color"

export default StyleSheet.create({
  listContainer: {
    flex: 1,
    padding: 10
  },
  listTopic: {

  },
  listTopicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  listTopicSub: {
    marginTop: 10
  },
  listTopicSubText: {
    fontFamily: "THSarabunNew",
    fontWeight: "bold",
    fontSize: 14,
    color: BASIC_COLOR
  },
  listInfo:{
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center'
  },
  listInfoCol:{
    flexDirection: 'column'
  },
  listButton: {
    width: 120,
    height: 30,
    borderRadius: 40,
    backgroundColor: BLUE_COLOR,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15
  },
  listButtonView:{
    backgroundColor: '#eceeee',
    justifyContent: 'center'
  },
  listButtonText: {
    color: BASIC_COLOR,
    fontFamily: "THSarabunNew",
    fontWeight: "bold",
    fontSize: 14,
  },
  listTextbox:{
    borderWidth: 2,
    borderColor: "#cccccc",
    borderRadius: 10,
    width: 100,
    height: 40,
    marginTop: 5,
    color: BASIC_COLOR,
    textAlign: "center"
  },
  textboxDisable:{
    backgroundColor: '#ddeef0',
    color: "#686968"
  },
  textboxDepth:{
    marginTop: 60
  },
  textboxDepthDwall: {
    marginTop: 15
  },
  textboxLevel:{
    marginTop: 30
  },
  textboxLevelDwall: {
    marginTop: 20
  },
  textboxCutOff: {
    marginTop: 20
  }
});
