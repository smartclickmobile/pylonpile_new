import { Platform, StyleSheet } from "react-native"
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM } from "../../Constants/Color"

export default StyleSheet.create({
  selectedView: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10
  },
  button: {
    borderWidth: 2,
    borderColor: MAIN_COLOR,
    borderRadius: 10,
    padding: 5,
    backgroundColor: "#FFF"
  },
  buttonText: {
    color: MAIN_COLOR,
    fontFamily: "THSarabunNewBold",
    fontSize: 20
  },
  buttonTextStyle: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  topic: {
    marginTop: 10,
    marginLeft: 10
  },
  topicText: {
    fontSize: 20,
    color: MAIN_COLOR
  }
})
