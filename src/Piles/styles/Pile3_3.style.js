import {Platform, StyleSheet} from 'react-native'
import { MAIN_COLOR, SUB_COLOR } from "../../Constants/Color"

export default StyleSheet.create({
  topic: {
    marginTop: 10,
    marginLeft: 10
  },
  imageDetail: {
    flexDirection: "row",
    alignItems: "center"
  },
  approveBox: {
    borderWidth: 1,
    borderColor: "#cccccc",
    backgroundColor: SUB_COLOR,
    borderRadius: 20,
    height: 40,
    width: "28%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5,
    marginLeft: 5
  },
  buttonBox: {
    borderWidth: 2,
    borderColor: "#cccccc",
    borderRadius: 20,
    height: 40,
    marginTop: 5,
    justifyContent: "center",
    alignItems: 'center'
  },
  topicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  boxTopic: {
    marginLeft: 10,
    marginTop: 10
  },
  selectedView: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10
  },
  button: {
    borderWidth: 2,
    borderColor: MAIN_COLOR,
    borderRadius: 10,
    backgroundColor: "#FFF",
    height: 50,
    padding: 0,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonText: {
    color: MAIN_COLOR,
    fontFamily: "THSarabunNewBold",
    fontSize: 20,
    // height: 50,
    width: "100%",
    textAlign: "center",
    padding:0
  },
  buttonTextStyle: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
})
