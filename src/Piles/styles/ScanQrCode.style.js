import { Platform, StyleSheet, Dimensions } from 'react-native'
const { height, width } = Dimensions.get('window')
export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row'
  },
  preview: {
    // width: width,
    // height: width
    flex: 1
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40
  },
  buttonRight: {
    position: 'absolute',
    top: 0,
    right: 20
  },
  navigationTitle: {
    flexDirection: 'row',
    justifyContent: 'center',
    // minHeight: 30,
  },
  buttonLeftResize: {
    width: 25,
    height: 25
  },
  textView: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
