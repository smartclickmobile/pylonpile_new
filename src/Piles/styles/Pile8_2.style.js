import {Platform, StyleSheet} from 'react-native'
import {
  MAIN_COLOR,
  SUB_COLOR,
  BASIC_COLOR,
  MENU_GREY_ITEM,
  BUTTON_COLOR,
  BLUE_COLOR
} from "../../Constants/Color"

export default StyleSheet.create({
  listContainer: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 10
  },
  listTopic: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10
  },
  listTopicText: {
    fontSize: 20,
    color: MAIN_COLOR
  },
  listButton: {
    width: 130,
    height: 40,
    borderRadius: 40,
    backgroundColor: BLUE_COLOR,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15
  },
  listButtonLength: {
    width: 130,
    height: 40,
    borderRadius: 40,
    backgroundColor: '#ebf1f7',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15
  },
  listButtonText: {
    color: BUTTON_COLOR,
    fontFamily: "THSarabunNew",
    fontWeight: "bold",
    fontSize: 14
  },
  listButtonTextLength: {
    fontFamily: "THSarabunNew",
    fontWeight: "bold",
    fontSize: 14
  },
  lisTable: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    width: '100%'
  },
  listTableDetail: {
    marginTop: 80
  },
  listTableDetailText: {
    fontFamily: "THSarabunNew",
    fontSize: 20,
    lineHeight: 30,
    textAlign: 'center',
    color: '#000'
  },
  textHighligh: {
    color: BLUE_COLOR
  },
  tableView: {
    flexDirection: 'column',
    flex: 1
  },
  tableViewHeadGroup: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#daeef8',
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#636361',
    flex: 1,
    height: 40
  },
  tableViewHead: {
    width: 100,
    height: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    borderRightWidth: 1,
    borderColor: '#636361'
  },
  tableViewHeadText: {
    textAlign: 'center',
    color: '#000'
  },
  tableViewContentGroup: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    flex: 1,
    minHeight: 40
  },
  tableViewContent: {
    width: 100,
    height: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    borderRightWidth: 1,
    borderColor: '#636361'
  },
  tableViewStep: {
    width: 100,
    borderWidth: 1,
    borderColor: 'red'
  },
  tableViewContentText: {
    textAlign: 'center',
    color: '#000'
  },
  tableViewContentStepText: {
    color: '#000'
  },
  buttonDelete: {
    borderWidth: 1,
    borderColor: '#3b95cd',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10
  },
  buttonDeleteText: {
    color: '#3b95cd'
  },
  buttonEdit: {
    borderWidth: 1,
    borderColor: '#3b95cd',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 5
  },
  buttonEditText: {
    color: '#3b95cd'
  },
  tableAction: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  tableViewMD: {
    width: 180
  },


  tableCol: {
    flexDirection: 'column',
    // width: '100%'
  },
  tableHeadGroup: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#daeef8',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#636361',
    width: '100%'
  },
  tableHead: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // width: 100,
    height: 40,
    padding: 10,
    borderRightWidth: 1,
    borderColor: '#636361'
  },
  tableHeadTool: {
    width: 180
  },
  tableContentGroup: {
    flexDirection: 'row'
  },
  tableContent: {
    flex: 1,
    width: 100,
    minHeight: 30,
    flexWrap: 'wrap',
    padding: 10,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#636361'
  },
  tableContentCenter: {
    alignItems: 'center'
  },
  tableContentTool: {
    width: 180,
    flexDirection: 'row',
    justifyContent: 'space-around',
    flexWrap: 'wrap'
  }
})
