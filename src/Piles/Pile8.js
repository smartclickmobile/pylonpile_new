import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import {Container, Content} from 'native-base';
import StepIndicator from 'react-native-step-indicator';
import * as actions from '../Actions';
import Pile8_1 from './Pile8_1';
import Pile8_2 from './Pile8_2';
import Pile8_3 from './Pile8_3';
import styles from './styles/Pile8.style';
import I18n from '../../assets/languages/i18n';
import {MAIN_COLOR, DEFAULT_COLOR_1, DEFAULT_COLOR_4} from '../Constants/Color';
import moment from 'moment';
class Pile8 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 0,
      process: 8,
      Edit_Flag: 1,
      error: null,
      data: [],
      location: {
        position: {
          lat: 1,
          log: 1,
        },
      },
      startdate: '',
      enddate: '',
      status10: 0,
      checksavebutton: false,
      onPress: false,
      random8: null,
      Step8Status: 0,
      stepstatus: null,
      Parentcheck: false,
    };
  }
  componentDidMount() {
    // console.log(this.props.stack)
    if (this.props.stack) {
      this.setState({
        step: this.props.stack[this.props.stack.length - 1].step - 1,
      });
    }
    this.props.getTremieList({
      pileid: this.props.pileid,
      jobid: this.props.jobid,
    });
  }
  async componentWillReceiveProps(nextProps) {
    if (
      nextProps.step_status2_random != this.state.stepstatus &&
      nextProps.processstatus == 8
    ) {
      this.setState({stepstatus: nextProps.step_status2_random}, async () => {
        // console.warn('yes6',nextProps.step_status2.Step2Status)
        await this.props.mainPileSetStorage(this.props.pileid, 'step_status', {
          Step8Status: nextProps.step_status2.Step8Status,
        });
        this.props.mainPileGetStorage(this.props.pileid, 'step_status');
      });
    }
    // console.warn( 'nextProps')
    if (
      nextProps.pileAll[this.props.pileid] &&
      nextProps.pileAll[this.props.pileid]['9_1'] &&
      (this.props.category == 5 || this.props.category == 3)
    ) {
      // console.warn( 'nextProps',nextProps.pileAll[this.props.pileid]['9_1'].masterInfo )
      let pileMaster = nextProps.pileAll[this.props.pileid]['9_1'].masterInfo;
      if (pileMaster) {
        if (pileMaster.PileDetail) {
          // console.warn('PileDetail',pileMaster.PileDetail)
          this.setState({
            Parentcheck:
              pileMaster.PileDetail.pile_id == pileMaster.PileDetail.parent_id
                ? true
                : false,
          });
        }
      }
    }
    if (nextProps.pileAll[this.props.pileid] != undefined) {
      var edit = nextProps.pileAll[this.props.pileid];
      if (edit['8_0'] != undefined) {
        console.log('edit[8_0].data', edit['8_0'].data);
        await this.setState({Edit_Flag: edit['8_0'].data.Edit_Flag});
      }

      if (edit != undefined) {
        this.setState({status10: edit.step_status.Step10Status});
      }
    }

    if (
      nextProps.pile8 == true &&
      this.state.random8 != nextProps.random8 &&
      this.state.onPress == true
    ) {
      if (nextProps.save8page == 1) {
        this.setState({random8: nextProps.random8, step: 1, onPress: false});

        // if (this.state.Step8Status == 2) {
        //   await this.props.mainPileSetStorage(
        //     this.props.pileid,
        //     "step_status",
        //     { Step8Status: 2 }
        //   )
        // } else {
        //   await this.props.mainPileSetStorage(
        //     this.props.pileid,
        //     "step_status",
        //     { Step8Status: 1 }
        //   )
        // }
        // this.props.mainPileGetStorage(this.props.pileid, "step_status")
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process: 8,
        });
        this.onSetStack(8, 0);
        setTimeout(() => {
          this.setState({checksavebutton: false});
        }, 2000);
      } else if (nextProps.save8page == 3) {
        this.setState({random8: nextProps.random8, step: 3, onPress: false});

        // if (this.state.Step8Status == 2) {
        //   await this.props.mainPileSetStorage(
        //     this.props.pileid,
        //     "step_status",
        //     { Step8Status: 2 }
        //   )
        // } else {
        //   // console.warn(nextProps.random8)
        //   await this.props.mainPileSetStorage(
        //     this.props.pileid,
        //     "step_status",
        //     { Step8Status: 1 }
        //   )
        // }
        // this.props.mainPileGetStorage(this.props.pileid, "step_status")
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process: 8,
        });
        this.props.onNextStep();
        this.onSetStack(9, 1);
        setTimeout(() => {
          this.setState({checksavebutton: false});
        }, 2000);
      }
    }

    await this.setState({
      error: nextProps.error,
      data: nextProps.pileAll[nextProps.pileid],
      location: nextProps.pileAll.currentLocation,
      checksavebutton: false,
    });
  }

  async onSetStack(pro, step) {
    var data = {
      process: pro,
      step: step + 1,
    };
    await this.props.setStack(data);
    await this.props.mainPileGetAllStorage();
  }

  nextButton = () => {
    switch (this.state.step) {
      case 0:
        this.onSave(this.state.step);
        break;
      case 1:
        this.onSave(this.state.step);
        break;
      case 2:
        this.onSave(this.state.step);
        break;
      default:
        break;
    }
  };

  async onSave(step) {
    this.setState({checksavebutton: true});
    await this.props.mainPileGetAllStorage();
    if (step == 0) {
      var value = {
        process: 8,
        step: 1,
        pile_id: this.props.pileid,
        haslast: this.props.haslast,
        shape: this.props.shape,
        category: this.props.category,
      };
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value).then(async (data) => {
          var valueApi = {
            Language: I18n.currentLocale(),
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            Page: 1,
            shape: this.props.shape,
            category: this.props.category,
            MachineId: this.state.data['8_1'].data.MachineId,
            MachineName: this.state.data['8_1'].data.MachineName,
            DriverId: this.state.data['8_1'].data.DriverId,
            DriverName: this.state.data['8_1'].data.DriverName,
            TremieTypeId: this.state.data['8_1'].data.TremieTypeId,
            TremieTypeName: this.state.data['8_1'].data.TremieTypeName,
            latitude:
              this.state.location != undefined
                ? this.state.location.position.lat
                : 1,
            longitude:
              this.state.location != undefined
                ? this.state.location.position.log
                : 1,
          };

          if (this.state.error == null) {
            if (data.check == false) {
              Alert.alert('', data.errorText[0], [
                {
                  text: 'OK',
                },
              ]);
              this.setState({checksavebutton: false});
            } else {
              this.setState(
                {onPress: true, Step8Status: data.statuscolor},
                async () => {
                  await this.props.pileSaveStep08(valueApi);
                },
              );

              if (
                (this.props.startdate != '') &
                (this.props.startdate != null)
              ) {
                this.setState({
                  step: 1,
                  startdate: moment(
                    this.props.startdate,
                    'DD/MM/YYYY HH:mm:ss',
                  ).format('DD/MM/YYYY HH:mm'),
                });
              } else {
                this.setState({
                  step: 1,
                  startdate: moment().format('DD/MM/YYYY HH:mm'),
                });
              }
            }
          } else {
            Alert.alert(this.state.error);
            this.setState({checksavebutton: false});
          }
        });
      }
    } else if (step == 1) {
      var value = {
        process: 8,
        step: 2,
        pile_id: this.props.pileid,
        haslast: this.props.haslast,
      };
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value).then(async (data) => {
          // this.props.mainPileGetStorage(this.props.pileid, "step_status")

          if (this.state.error == null) {
            if (data.check == false) {
              Alert.alert('', data.errorText[0], [
                {
                  text: 'OK',
                },
              ]);
              this.setState({checksavebutton: false});
            } else {
              // this.setState({onPress:true},()=>{
              //   // this.props.pileSaveStep07(valueApi)
              // })
              // await this.props.mainPileSetStorage(
              //   this.props.pileid,
              //   "step_status",
              //   { Step8Status: data.statuscolor }
              // )
              this.props.getStepStatus2({
                jobid: this.props.jobid,
                pileid: this.props.pileid,
                process: 8,
              });
              // console.warn('enddate',this.props.enddate)
              if ((this.props.enddate != '') & (this.props.enddate != null)) {
                this.setState({
                  step: 2,
                  enddate: moment(this.props.enddate).format(
                    'DD/MM/YYYY HH:mm',
                  ),
                });
              } else {
                this.setState(
                  {
                    step: 2,
                    enddate: moment().format('DD/MM/YYYY HH:mm'),
                  },
                  () => {
                    // console.warn('date',this.state.enddate)
                  },
                );
              }
              // this.setState({ step: 2 })
              this.onSetStack(8, 1);
              setTimeout(() => {
                this.setState({checksavebutton: false});
              }, 2000);
            }
          } else {
            Alert.alert(this.state.error);
            this.setState({checksavebutton: false});
          }
        });
      }
    } else if (step == 2) {
      var value = {
        process: 8,
        step: 3,
        pile_id: this.props.pileid,
        haslast: this.props.haslast,
        category: this.props.category,
      };
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value).then(async (data) => {
          var imagefile = [];
          if (
            this.state.data['8_3'].data.foamimage != null &&
            this.state.data['8_3'].data.foamimage.length > 0
          ) {
            for (
              var i = 0;
              i < this.state.data['8_3'].data.foamimage.length;
              i++
            ) {
              imagefile.push(this.state.data['8_3'].data.foamimage[i]);
            }
          }
          // console.warn(this.state.data["8_3"].enddate)
          if (this.state.data['8_3'].enddate != '') {
            var end = this.state.data['8_3'].enddate;
            var valueend = {
              process: 8,
              step: 3,
              pile_id: this.props.pileid,
              data: {
                checkfoam: this.state.data['8_3'].data.checkfoam,
                foamimage: this.state.data['8_3'].data.foamimage,
              },
              startdate: this.state.data['8_3'].startdate,
              enddate: end,
            };
            await this.props.mainPileSetStorage(
              this.props.pileid,
              '8_3',
              valueend,
            );
          } else {
            var end = moment().format('DD/MM/YYYY HH:mm');
            var valueend = {
              process: 8,
              step: 3,
              pile_id: this.props.pileid,
              data: {
                checkfoam: this.state.data['8_3'].data.checkfoam,
                foamimage: this.state.data['8_3'].data.foamimage,
              },
              startdate: this.state.data['8_3'].startdate,
              enddate: end,
            };
            await this.props.mainPileSetStorage(
              this.props.pileid,
              '8_3',
              valueend,
            );
          }
          var start = moment(
            this.state.data['8_3'].startdate,
            'DD/MM/YYYY HH:mm',
          );
          var end1 = moment(end, 'DD/MM/YYYY HH:mm');
          console.warn(this.state.data['8_3'].startdate);
          if (this.state.data['8_3'].startdate != undefined) {
            if (start <= end1) {
              var valueApi = {
                Language: I18n.currentLocale(),
                JobId: this.props.jobid,
                PileId: this.props.pileid,
                Page: 3,
                AddFoam: this.state.data['8_3'].data.checkfoam,
                FoamImage: imagefile,
                latitude:
                  this.state.location != undefined
                    ? this.state.location.position.lat
                    : 1,
                longitude:
                  this.state.location != undefined
                    ? this.state.location.position.log
                    : 1,
                startdate: moment(
                  this.state.data['8_3'].startdate,
                  'DD/MM/YYYY HH:mm',
                ).format('YYYY-MM-DD HH:mm:ss'),
                enddate: moment(end, 'DD/MM/YYYY HH:mm').format(
                  'YYYY-MM-DD HH:mm:ss',
                ),
              };

              if (this.state.error == null) {
                if (data.check == false) {
                  Alert.alert('', data.errorText[0], [
                    {
                      text: 'OK',
                    },
                  ]);
                  this.setState({checksavebutton: false});
                } else {
                  this.setState(
                    {onPress: true, Step8Status: data.statuscolor},
                    async () => {
                      console.warn(
                        'props date1',
                        this.state.data['8_3'].startdate,
                        end,
                      );
                      await this.props.pileSaveStep08(valueApi);
                    },
                  );
                }
              } else {
                Alert.alert(this.state.error);
                this.setState({checksavebutton: false});
              }
            } else {
              Alert.alert(
                '',
                I18n.t('mainpile.liquidtestinsert.alert.error4'),
                [
                  {
                    text: 'OK',
                  },
                ],
              );
              this.setState({checksavebutton: false});
            }
          } else {
            if (this.props.startdate <= this.props.enddate) {
              // await this.props.mainPileSetStorage(
              //   this.props.pileid,
              //   "step_status",
              //   { Step8Status: data.statuscolor }
              // )
              // this.props.mainPileGetStorage(this.props.pileid, "step_status")
              var valueApi = {
                Language: I18n.currentLocale(),
                JobId: this.props.jobid,
                PileId: this.props.pileid,
                Page: 3,
                AddFoam: this.state.data['8_3'].data.checkfoam,
                FoamImage: imagefile,
                latitude:
                  this.state.location != undefined
                    ? this.state.location.position.lat
                    : 1,
                longitude:
                  this.state.location != undefined
                    ? this.state.location.position.log
                    : 1,
                startdate: moment(
                  this.props.startdate,
                  'DD/MM/YYYY HH:mm:ss',
                ).format('YYYY-MM-DD HH:mm:ss'),
                enddate: moment(
                  this.props.enddate,
                  'DD/MM/YYYY HH:mm:ss',
                ).format('YYYY-MM-DD HH:mm:ss'),
              };

              if (this.state.error == null) {
                if (data.check == false) {
                  Alert.alert('', data.errorText[0], [
                    {
                      text: 'OK',
                    },
                  ]);
                  this.setState({checksavebutton: false});
                } else {
                  this.setState(
                    {onPress: true, Step8Status: data.statuscolor},
                    async () => {
                      console.warn(
                        'props date',
                        this.props.startdate,
                        this.props.enddate,
                      );
                      await this.props.pileSaveStep08(valueApi);
                    },
                  );
                }
              } else {
                Alert.alert(this.state.error);
                this.setState({checksavebutton: false});
              }
            } else {
              Alert.alert('', I18n.t('mainpile.8_1.error_step'), [
                {
                  text: 'OK',
                },
              ]);
              this.setState({checksavebutton: false});
            }
          }
        });
      }
    }
  }

  _renderStepFooter() {

    return (
      <View>
        <View style={{flexDirection: 'column'}}>
          <View style={{borderWidth: 1}}>
            <StepIndicator
              stepCount={3}
              onPress={(step) => {
                this.setState({step: step});
                this.onSetStack(8, step);
              }}
              currentPosition={this.state.step}
            />
          </View>
          {(this.state.Edit_Flag == 1 &&(this.props.category != 3 && this.props.category != 5)) ||
          (this.state.Edit_Flag == 1 &&( this.props.category == 3 || this.props.category == 5) &&this.state.Parentcheck == true) ? (
            <View
              style={[
                styles.second,
                this.props.category == 3 || this.props.category == 5
                  ? {backgroundColor: DEFAULT_COLOR_4}
                  : this.props.shape == 1
                  ? {backgroundColor: MAIN_COLOR}
                  : {backgroundColor: DEFAULT_COLOR_1},
              ]}>
              <TouchableOpacity
                style={styles.firstButton}
                onPress={this.deleteButton}
                disabled={this.state.checksavebutton}>
                <Text style={styles.selectButton}>
                  {I18n.t('mainpile.button.delete')}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.secondButton,
                  this.props.category == 3 || this.props.category == 5
                    ? {backgroundColor: DEFAULT_COLOR_4}
                    : this.props.shape == 1
                    ? {backgroundColor: MAIN_COLOR}
                    : {backgroundColor: DEFAULT_COLOR_1},
                ]}
                onPress={this.nextButton}
                disabled={this.state.checksavebutton}>
                <Text style={styles.selectButton}>
                  {I18n.t('mainpile.button.next')}
                </Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View />
          )}
        </View>
      </View>
    );
  }

  deleteButton = () => {
    this.props.setStack({
      process: 8,
      step: 1,
    });
    const data = {
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      processid: this.state.process,
      lat:
        this.state.location != undefined ? this.state.location.position.lat : 1,
      log:
        this.state.location != undefined ? this.state.location.position.log : 1,
    };
    if (this.state.status10 == 0) {
      Alert.alert('Warning', I18n.t('mainpile.button.confirmdelete'), [
        {text: 'Cancel'},
        {text: 'OK', onPress: () => this.props.pileDelete(data)},
      ]);
    } else {
      Alert.alert('', I18n.t('alert.error_delete8'), [
        {
          text: 'OK',
        },
      ]);
    }
  };

  updateState(value) {
    const ref = 'pile' + value.process + '_' + value.step;
    if (this.refs[ref].getWrappedInstance()) {
      this.refs[ref].getWrappedInstance().updateState(value);
    }
  }
  initialStep() {
    this.setState({step: 0});
  }

  render() {
    if (this.props.hidden) {
      return null;
    }

    return (
      <View style={{flex: 1}}>
        <Content>
          <Pile8_1
            ref="pile8_1"
            hidden={!(this.state.step == 0)}
            jobid={this.props.jobid}
            pileid={this.props.pileid}
            shape={this.props.shape}
            onRefresh={this.props.onRefresh}
            category={this.props.category}
          />
          <Pile8_2
            ref="pile8_2"
            hidden={!(this.state.step == 1)}
            jobid={this.props.jobid}
            pileid={this.props.pileid}
            shape={this.props.shape}
            category={this.props.category}
          />
          <Pile8_3
            ref="pile8_3"
            hidden={!(this.state.step == 2)}
            jobid={this.props.jobid}
            pileid={this.props.pileid}
            shape={this.props.shape}
            category={this.props.category}
            startdate={this.props.startdate}
            startdatedefault={this.state.startdate}
            enddate={this.props.enddate}
            enddatedefault={this.state.enddate}
          />
        </Content>
        {this._renderStepFooter()}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    error: state.pile.error,
    pileAll: state.mainpile.pileAll || null,
    haslast: state.tremie.haslast,
    stack: state.mainpile.stack_item,
    random8: state.pile.random8,
    pile8: state.pile.pile8,
    save8page: state.pile.save8page,

    step_status2_random: state.mainpile.step_status2_random,
    step_status2: state.mainpile.step_status2,
    processstatus: state.mainpile.process,
  };
};

export default connect(mapStateToProps, actions, null, {withRef: true})(Pile8);
