import React, { Component } from "react"
import { View, Text, TouchableOpacity, TextInput ,Alert} from "react-native"
import ModalSelector from "react-native-modal-selector"
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM } from "../Constants/Color"
import { Icon } from "react-native-elements"
import * as actions from "../Actions"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import I18n from "../../assets/languages/i18n"
import styles from "./styles/Pile10_1.style"

class Pile10_1 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      foremanset: "",
      foremansetid: "",
      foreman: "",
      foremanid: "",
      foremansetdata: [{ key: 0, section: true, label: I18n.t("mainpile.10_1.foremansetselect") }],
      foremandata: [{ key: 0, section: true, label: I18n.t("mainpile.10_1.foremanselect") }],
      overcast:'',
      process: 10,
      step: 1,
      Edit_Flag: 1
    }
  }

  updateState(value) {
    console.log("Update state from 10_1")
  }

  componentDidMount() {
    if (this.props.onRefresh === "refresh") {
      Actions.refresh({
        action: "start"
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    // console.warn('NEXT PROPS 10-1')
    var pile = null
    var pileMaster = null
    var pile10 = null
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["10_1"]) {
      pile = nextProps.pileAll[this.props.pileid]["10_1"].data
      pileMaster = nextProps.pileAll[this.props.pileid]["10_1"].masterInfo
      pile10 = nextProps.pileAll[this.props.pileid]["10_0"].data
      console.log("pile 10_2",pile)
    }
    if(pile){ 
      // console.warn('Overcast',pile.Overcast)    
      if(pile.Overcast!==undefined&&pile.Overcast!==null){
        
        this.setState({ overcast:  isNaN(parseFloat(pile.Overcast.toString() ))?'':parseFloat(pile.Overcast.toString() ).toFixed(2) })
      }
    }
    if (pileMaster) {
      if (pileMaster.ForemanSet) {
        let foremanset = [{ key: 0, section: true, label: I18n.t("mainpile.10_1.foremansetselect") }]
        pileMaster.ForemanSet.map((data, index) => {
          foremanset.push({ key: index + 1, label: data.Foreman, id: data.Id })
          if (pile && pile.ForemanSet && pile.ForemanSet.Id) {
            if (pile.ForemanSet.Id == data.Id) {
              console.log("pile.ForemanSet.Id",pile.ForemanSet.Id)
              this.setState({ foremanset: data.Foreman, foremansetid: data.Id })
            }
          }
          if(pile.ForemanSet==null){
            this.setState({ foremanset: "", foremansetid: "" })
          }
        })
        this.setState({ foremansetdata: foremanset })
      }
      if (pileMaster.Foreman) {
        let foreman = [{ key: 0, section: true, label: I18n.t("mainpile.10_1.foremanselect") }]
        pileMaster.Foreman.map((data, index) => {
          let name = (data.nickname && data.nickname + "-") + data.firstname + " " + data.lastname
          foreman.push({ key: index + 1, label: name, id: data.employee_id })
          if (pile && pile.ForemanId) {
            if (pile.ForemanId == data.employee_id) {
              console.log("pile.ForemanId",pile.ForemanId)
              this.setState({ foreman: name, foremanid: data.employee_id })
            }
          }
          if(pile.ForemanId==null){
            this.setState({ foreman: "", foremanid: "" })
          }
        })
        this.setState({ foremandata: foreman })
      }

    }
   
    // if(pile){
    //   this.setState({ foremansetid: pile.ForemanId})
    //   if(foremanid:pile.ForemanSet.Id ){
    //     this.setState({ foremanid:pile.ForemanSet.Id})
    //   }
      
    // }
    if(pile10) {
      this.setState({ Edit_Flag: pile10.Edit_Flag })
    }
  }

  selectForemanSet = data => {
    this.setState({ foremanset: data.label, foremansetid: data.id }, () => this.saveLocal())
  }

  selectForeman = data => {
    this.setState({ foreman: data.label, foremanid: data.id }, () => this.saveLocal())
  }

  saveLocal() {
    var value = {
      process: 10,
      step: 1,
      pile_id: this.props.pileid,
      data: {
        ForemanId: this.state.foremanid,
        ForemanName: this.state.foreman,
        ForemanSet: {
          Id: this.state.foremansetid,
          Foreman: this.state.foremanset
        },
        Overcast:this.state.overcast
      }
    }
    this.props.mainPileSetStorage(this.props.pileid, "10_1", value)
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    console.log('this.props.category==4||this.props.category==1',this.props.category==4||this.props.category==1,this.props.category)
    let check = this.props.concreterecord.length
    return (
      <View>
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t("mainpile.10_1.topic")}</Text>
        </View>
        <View style={{ marginTop: 20 }}>
          <View style={styles.boxTopic}>
            <Text style={{ marginLeft: 10 }}>{I18n.t("mainpile.10_1.foremanset")}</Text>
          </View>
          <View style={styles.selectedView}>
            <ModalSelector
              data={this.state.foremansetdata}
              onChange={this.selectForemanSet}
              selectTextStyle={styles.textButton}
              cancelText="Cancel"
              disabled={this.state.Edit_Flag == 0}
            >
              <TouchableOpacity style={[styles.button, this.state.Edit_Flag == 0 && { borderColor: MENU_GREY_ITEM }]} disabled={this.state.Edit_Flag == 0}>
                <View style={styles.buttonTextStyle}>
                <Text style={[styles.buttonText, this.state.Edit_Flag == 0 && { color: MENU_GREY_ITEM }]}>
                    {this.state.foremanset || I18n.t("mainpile.10_1.foremansetselect")}
                  </Text>
                  <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                    <Icon name="arrow-drop-down" type="MaterialIcons" color={this.state.Edit_Flag == 0 ? MENU_GREY_ITEM : "#007CC2" } />
                  </View>
                </View>
              </TouchableOpacity>
            </ModalSelector>
          </View>
        </View>
        <View style={{ marginTop: 10 }}>
          <View style={styles.boxTopic}>
            <Text style={{ marginLeft: 10 }}>{I18n.t("mainpile.10_1.foreman")}</Text>
          </View>
          <View style={styles.selectedView}>
            <ModalSelector
              data={this.state.foremandata}
              onChange={this.selectForeman}
              selectTextStyle={styles.textButton}
              cancelText="Cancel"
              disabled={this.state.Edit_Flag == 0}
            >
              <TouchableOpacity style={[styles.button, this.state.Edit_Flag == 0 && { borderColor: MENU_GREY_ITEM }]} disabled={this.state.Edit_Flag == 0}>
                <View style={styles.buttonTextStyle}>
                <Text style={[styles.buttonText, this.state.Edit_Flag == 0 && { color: MENU_GREY_ITEM }]}>{this.state.foreman || I18n.t("mainpile.10_1.foremanselect")}</Text>
                  <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                    <Icon name="arrow-drop-down" type="MaterialIcons" color={this.state.Edit_Flag == 0 ? MENU_GREY_ITEM : "#007CC2" } />
                  </View>
                </View>
              </TouchableOpacity>
            </ModalSelector>
          </View>
        </View>
        <View style={{ marginTop: 10 }}>
          <View style={styles.boxTopic}>
            <Text  style={{ marginLeft: 10 }}>{this.props.category!==4&&this.props.category!==1? I18n.t("mainpile.10_1.overcast_D"):I18n.t("mainpile.10_1.overcast_B")}</Text>
          </View>
          <View style={styles.selectedView}>
            <View style={[styles.button, { height: 50, padding: 0 }, this.state.Edit_Flag==0 && { borderColor: MENU_GREY_ITEM }]}>
              <TextInput
                editable={this.state.Edit_Flag == 1}
                keyboardType="numeric"
                placeholder={this.props.category!==4&&this.props.category!==1?I18n.t("mainpile.10_1.overcast_p"): I18n.t("mainpile.10_1.overcast_bp")}
                value={this.state.overcast}
                onChangeText={text => {
                  if(check>0){
                    Alert.alert("", I18n.t("alert.errorovercast4"), [
                      {
                        text: "OK"
                      }
                    ])
                    return
                  }else{
                    this.setState({overcast:text})}}
                  }
                  
                underlineColorAndroid="transparent"
                style={[styles.buttonText,{ textAlign: "center"}, this.state.Edit_Flag == 0 && { color: MENU_GREY_ITEM }]}
                onEndEditing={() => {
                  this.setState({overcast: isNaN(parseFloat(this.state.overcast))?'':parseFloat(this.state.overcast).toFixed(2)},()=>{
                    this.saveLocal()
                  })
                  
                }
              }
              />
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll,
    concreterecord: state.concrete.concreterecord,
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(Pile10_1)
