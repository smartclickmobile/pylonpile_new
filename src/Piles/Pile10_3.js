import React, { Component } from "react"
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Image,
  ActivityIndicator,
  Alert
} from "react-native"
import ModalSelector from "react-native-modal-selector"
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM } from "../Constants/Color"
import { Icon } from "react-native-elements"
import * as actions from "../Actions"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import I18n from "../../assets/languages/i18n"
import styles from "./styles/Pile10_3.style"
import moment from "moment"
import naturalSort from "javascript-natural-sort"

class Pile10_3 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      concrete: 0,
      concretecumulative: 0,
      process: 10,
      step: 3,
      location: {
        position: {
          lat: 1,
          log: 1
        }
      },
      trucklist: ["5"],
      slumpmin: 0,
      slumpmax: 0,
      loading: false,
      Edit_Flag: 1,
      trigger: false,
      sort: 1,
      Parentid: null,
      Parentno: null,
      Pileid: null,
      Parentcheck: false,
      CheckOvercast:false
    }
  }

  updateState(value) {}

  async componentDidMount() {
    this.props.getConcreteList({
      pileid: this.props.pileid,
      jobid: this.props.jobid
    })
    this.props.getConcreteRecord({
      pileid: this.props.pileid,
      jobid: this.props.jobid
    })



    var pile10 = null
    var pile10_1 =null
    if (
      this.props.pileAll[this.props.pileid] &&
      this.props.pileAll[this.props.pileid]["10_0"]
    ) {
      pile10 = this.props.pileAll[this.props.pileid]["10_0"].data
      pile10_1 = this.props.pileAll[this.props.pileid]["10_1"].data
      console.log("pile10_1",pile10_1)
    }
    if (pile10) {
      // console.log
      this.setState({ Edit_Flag: pile10.Edit_Flag })
    }
    if (pile10_1) {
      // console.warn('pile10_1',pile10_1.Overcast)
      if(pile10_1.Overcast!==undefined&&pile10_1.Overcast!==null&&pile10_1.Overcast!==''){
        this.setState({ CheckOvercast: true },()=>{
          // console.warn('Overcast',this.state.Overcast)
        })
      }else{
        this.setState({ CheckOvercast: false },()=>{
          // console.warn('Overcast',this.state.Overcast)
        })
      }
      
    }
    // await this.props.concreterecord
    // this.setState({ trigger: false },()=>{
    //   if (this.props.concreterecord) {
    //     const rowLen = this.props.concreterecord.length
    //     this.props.concreterecord.map((item, i) => {
    //       if (rowLen>0 && rowLen === i + 1&&(item.EndConcreting==null||item.EndConcreting=="")) {
    //         // last one
    //         this.setState({ trigger: true })
    //         console.warn("this.state.trigger", this.state.trigger,item)
    //       }else{
    //         this.setState({ trigger: false })

    //       }
    //     })
    //   }console.warn("triggerfalse", this.state.trigger)
    // })

    //   console.log("concreterecord trigger", this.state.trigger)

    //   if(this.props.concreterecord==null||this.props.concreterecord.length==0){
    //     console.log("concreterecord trigger", this.state.trigger)
    //     this.setState({ trigger: false })
    // }
    // console.log("this.state.trigger", this.state.trigger)
    // if(data!=null){
    //   if(data.endconcrete==null||data.endconcrete==""){
    //     this.setState({trigger:true})
    //   }else{
    //     this.setState({trigger:false})
    //   }}
  }

  async componentWillReceiveProps(nextProps) {
    var pile10 = null
    var pile10_1 =null
    var pileMaster = null
    if (
      nextProps.pileAll[this.props.pileid] &&
      nextProps.pileAll[this.props.pileid]["10_0"]
    ) {
      pile10 = nextProps.pileAll[this.props.pileid]["10_0"].data
      pile10_1 = nextProps.pileAll[this.props.pileid]["10_1"].data
      pileMaster = nextProps.pileAll[this.props.pileid]["9_1"].masterInfo
    }
    if (pile10) {
      this.setState({ Edit_Flag: pile10.Edit_Flag })
    }
    if (pile10_1) {
   
      if(pile10_1.Overcast!==undefined&&pile10_1.Overcast!==null&&pile10_1.Overcast!==''){
        this.setState({ CheckOvercast: true },()=>{
          // console.warn('will Overcast',this.state.CheckOvercast)
        })
      }else{
        this.setState({ CheckOvercast: false },()=>{
          // console.warn('will Overcast',this.state.CheckOvercast)
        })
      }
      
    }
    if (pileMaster) {
      if (pileMaster.PileDetail) {
        this.setState({
          Parentid: pileMaster.PileDetail.parent_id,
          Parentno: pileMaster.PileDetail.parent_no,
          Pileid: pileMaster.PileDetail.pile_id,
          Parentcheck:
            pileMaster.PileDetail.pile_id == pileMaster.PileDetail.parent_id
              ? true
              : false
        })
      }
    }
    // await this.props.concreterecord
    // if (this.props.concreterecord) {
    //   const rowLen = this.props.concreterecord.length
    //   this.props.concreterecord.map((item, i) => {
    //     if (rowLen === i + 1&&(item.EndConcreting==null||item.EndConcreting=="")) {
    //       // last one
    //       this.setState({ trigger: true })
    //       console.log("this.state.trigger2", this.state.trigger,item)
    //     }else{
    //       this.setState({ trigger: false })

    //     }
    //   })
    //   console.log(this.props.concreterecord)
    // }
    // console.log("triggerfalse", this.state.trigger)
    // if(this.props.concreterecord==null||this.props.concreterecord.length==0){
    //   console.log("concreterecord trigger", this.state.trigger)
    //   this.setState({ trigger: false })
    // }
    if (nextProps.pileAll != undefined && nextProps.pileAll != "") {
      this.setState({ location: nextProps.pileAll.currentLocation }, () => {
        console.log("triggerfalse", this.state.location)
      })
    }
    // await this.props.concreterecord
    // if (this.props.concreterecord) {
    //   const rowLen = this.props.concreterecord.length
    //   this.props.concreterecord.map((rank, i) => {
    //     if (rowLen === i + 1) {
    //       // last one
    //       this.setState({ trigger: true })
    //       console.log("this.state.trigger", this.state.trigger)
    //     } else {
    //       // not last one
    //       this.setState({ trigger: false })
    //     }
    //   })
    // }
  }

  regisButton2() {
    // let getpreconlast = EndConcreting

    Actions.concreteregister({
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      process: 9,
      step: 1,
      Edit_Flag: this.state.Edit_Flag,
      shape: this.props.shape,
      lat:
        this.state.location == undefined ? 1 : this.state.location.position.lat,
      log:
        this.state.location == undefined ? 1 : this.state.location.position.log,
      concretecumulative: this.props.concretevolume,
      index: this.props.concretelist.length + 1,
      concreteid: this.props.concreteid,
      mixid: this.props.mixid,
      category: this.props.category
    })
  }

  // calculateValue() {
  //   var volume = 0
  //   this.state.trucklist.map((data, index) => {
  //     if(data.TruckConcreteVolume) {
  //       volume += data.TruckConcreteVolume
  //     }
  //   })
  //   this.setState({ concretecumulative: volume })
  // }

  // saveLocal() {
  //   var value = {
  //     process: 9,
  //     step: 1,
  //     pile_id: this.props.pileid,
  //     data: {
  //       DriverName: this.state.driver,
  //       DriverId: this.state.driverid,
  //       MachineName: this.state.machine,
  //       MachineId: this.state.machineid
  //     }
  //   }
  //   this.props.mainPileSetStorage(this.props.pileid, "9_1", value)
  // }

  async regisButton() {
    let PreTruckEnd = ""
    let Predepth = ""
    let Precutsink = ""
    let PreTruck = ""
    let PredepthArr = []
    let PredepthArrStop = false
    let StopPourconVol=0
    let pretruckcheckstopbetween =false
    let index =this.props.concreterecord.length +1
    if(this.state.CheckOvercast!=true){
      let text = I18n.t("alert.errorovercast1")
      // if(this.props.category==4||this.props.category==1){
      //   text  = I18n.t("alert.errorovercast2")  
      // }
      Alert.alert("", text, [
        {
          text: "OK"
        }
      ])
      return
    }

    if (this.props.concreterecord.length != 0) {
      // console.warn(this.props.concreterecord[this.props.concreterecord.length - 1])
      if (
        this.props.concreterecord[this.props.concreterecord.length - 1]
          .EndConcreting != null &&
        this.props.concreterecord[this.props.concreterecord.length - 1]
          .EndConcreting != ""
      ) {
        PreTruckEnd = this.props.concreterecord[
          this.props.concreterecord.length - 1
        ].EndConcreting
      } else {
        Alert.alert("", I18n.t("alert.error10_3"), [
          {
            text: "OK"
          }
        ])
        return
      }
      console.warn(('length - 1'),
        this.props.concreterecord[this.props.concreterecord.length - 1]
      )
      if (
        this.props.concreterecord[this.props.concreterecord.length - 1] !=
          null &&
        this.props.concreterecord[this.props.concreterecord.length - 1] != ""
      ) {
        PreTruck = this.props.concreterecord[
          this.props.concreterecord.length - 1
        ]
      }

      // if (this.props.category == 5) {
      
      for (let i = this.props.concreterecord.length - 1; i >= 0; i--) {
        // console.warn("i", i, this.props.concreterecord[i])
        if (
          Predepth == "" &&
          this.props.concreterecord[i].Depth != "" &&
          this.props.concreterecord[i].Depth != null &&
          // this.props.category == 5&&
          PredepthArrStop == false&&
          this.props.concreterecord[i].StopPouringConcrete!=true
        ) {
          // console.warn('PredepthArrStop',PredepthArrStop,'i',i,this.props.concreterecord[i])
          Predepth = this.props.concreterecord[i].Depth
          PredepthArrStop = true
        } else {
          if (PredepthArrStop != true&&
            this.props.concreterecord[i].StopPouringConcrete!=true) {
            PredepthArr.push(this.props.concreterecord[i])
          }
        }

        
       

        if (
          this.props.category == 5 &&
          Precutsink == "" &&
          this.props.concreterecord[i].TremyCutSinkAfter != null
        ) {
          // console.warn("Precutsink", this.props.concreterecord[i].Precutsink)
          Precutsink = this.props.concreterecord[i].TremyCutSinkAfter
        }
      }
      console.warn('concreterecord',this.props.concreterecord )
      // if(this.props.concreterecord.length >0){

      // }
      // }
      // console.warn('this.props.concreterecord[this.props.concreterecord.length - 1]',this.props.concreterecord[this.props.concreterecord.length - 1].StopPouringConcrete)
      if(this.props.concreterecord&&
        this.props.concreterecord.length >0&&
        this.props.concreterecord[this.props.concreterecord.length - 1]!=undefined && 
        this.props.concreterecord[this.props.concreterecord.length - 1].StopPouringConcrete==true){
        
        pretruckcheckstopbetween=true
        index = this.props.concreterecord[this.props.concreterecord.length - 1].NoText
        for (let i = this.props.concreterecord.length - 1; this.props.concreterecord[i]!=undefined &&this.props.concreterecord[i].StopPouringConcrete==true; i--) {
          console.warn('StopPouringConcrete',this.props.concreterecord[i].StopPouringConcrete==true)
          // StopPourArr.push( this.props.concreterecord[i].ConcreteCalculateVolumn)
          StopPourconVol +=this.props.concreterecord[i].ConcreteCalculateVolumn
        }
      }else if(
        this.props.concreterecord.length >1&&
        this.props.concreterecord[this.props.concreterecord.length - 1].NoText!=undefined &&
        this.props.concreterecord[this.props.concreterecord.length - 1].StopPouringConcrete!=true &&
        this.props.concreterecord[this.props.concreterecord.length - 2]!=undefined && 
        this.props.concreterecord[this.props.concreterecord.length - 2].StopPouringConcrete==true ){
          let oldindex = this.props.concreterecord[this.props.concreterecord.length - 1].NoText
          let cut  = oldindex.split(' ')
          index = Number.parseInt(cut[0], 10) +1
          console.warn('cut',cut,index)
        }else if(
          this.props.concreterecord.length >1&&
          this.props.concreterecord[this.props.concreterecord.length - 2]!=undefined && 
          this.props.concreterecord[this.props.concreterecord.length - 2].StopPouringConcrete!=true ){
            let oldindex = this.props.concreterecord[this.props.concreterecord.length - 1].NoText
            index =Number.parseInt(oldindex, 10) +1
          }
    }
    // if(this.props.concreterecord.length >0){
    //   index = this.props.concreterecord[this.props.concreterecord.length - 1].NoText
    // }
    // for (let i = this.props.concreterecord.length - 1; i >= 0; i--) {
    //   if(this.props.concreterecord[i].StopPouringConcrete==true){
    //     // StopPouringConcrete:item.StopPouringConcrete,
    //     // ConcreteCalculateVolumn: item.ConcreteCalculateVolumn,
        
    //   }
    // }


    // console.warn("StopPourArr", StopPourArr,'pretruckcheckstopbetween',pretruckcheckstopbetween)
    Actions.dropconcrete({
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      process: this.state.process,
      step: this.state.step,
      Edit_Flag: "1",
      shape: this.props.shape,
      lat:
        this.state.location == undefined ? 1 : this.state.location.position.lat,
      log:
        this.state.location == undefined ? 1 : this.state.location.position.log,
      index:index,
      concretecumulative: this.props.concretecumulative,
      tremiecutcumulative: this.props.tremiecutcumulative,
      tremiecutcumulativelength: this.props.tremiecutcumulativelength,
      lastdepth: this.props.lastdepthArr[this.props.concreterecord.length],
      lastdepthconcrete: this.props.lastdepthconcreteArr[
        this.props.concreterecord.length
      ],
      category: this.props.category,
      PreTruckEnd: PreTruckEnd,
      newesttruck: true,
      Predepth: Predepth,
      Precutsink: Precutsink,
      PreTruck: PreTruck,
      PredepthArr: PredepthArr,
      StopPourconVol:StopPourconVol , 
      pretruckcheckstopbetween:pretruckcheckstopbetween
    })
  }

  /**
   * tremiepipeinstall
   * tremiepipelength
   * cuttremiepipe
   * tremiepipecutlength
   * remainingtremiepiplength
   * tremiepipeembeddedlength
   */

  onEditData(id, data, concrete, tremie, tremielength,key) {
    // console.warn("10_3",data)
    let newesttruck = false
    console.warn('onEditData id',id,data)
    if (key == this.props.concreterecord.length-1) {

      newesttruck = true
    }

    let Predepth = ""
    let Precutsink = ""
    let PreTruck = ""
    let PredepthArr = []
    let PredepthArrStop = false
    let StopPourconVol=0
    let pretruckcheckstopbetween =false
    let dropNo=id
    if (this.props.concreterecord.length != 0) {
      console.warn(
        this.props.concreterecord[this.props.concreterecord.length - 1]
      )
      if (
        this.props.concreterecord[this.props.concreterecord.length - 1] !=
          null &&
        this.props.concreterecord[this.props.concreterecord.length - 1] != ""
      ) {
        PreTruck = this.props.concreterecord[
          this.props.concreterecord.length - 1
        ]
      }

      for (let i = id - 2; i >= 0; i--) {
        console.warn("i", i, id - 2, id, this.props.concreterecord[i])
        if (
          Predepth == "" &&
          this.props.concreterecord[i].Depth != "" &&
          this.props.concreterecord[i].Depth != null &&
          this.props.concreterecord[i].StopPouringConcrete!=true

        ) {
          console.warn(
            "PredepthArrStop",
            PredepthArrStop,
            "i",
            i,
            this.props.concreterecord[i]
          )
          Predepth = this.props.concreterecord[i].Depth
          PredepthArrStop = true
        } else {
          if (PredepthArrStop != true&&
            this.props.concreterecord[i].StopPouringConcrete!=true) {
            PredepthArr.push(this.props.concreterecord[i])
          }
        }


        if (
          Precutsink == "" &&
          this.props.concreterecord[i].TremyCutSinkAfter != null
        ) {
          console.warn("Precutsink", this.props.concreterecord[i].Precutsink)
          Precutsink = this.props.concreterecord[i].TremyCutSinkAfter
        }
      }
      if(this.props.concreterecord.length >= 2&&id>=2){
        if( this.props.concreterecord[id - 2].StopPouringConcrete==true){
          pretruckcheckstopbetween=true
          for (let i = id - 2; i >=0&&this.props.concreterecord[i].StopPouringConcrete==true; i--) {
            // StopPourArr.push( this.props.concreterecord[i].ConcreteCalculateVolumn)
            StopPourconVol +=this.props.concreterecord[i].ConcreteCalculateVolumn
          }
        }
      }
      if(this.props.concreterecord[key]&&
        this.props.concreterecord[key].ConcreteCalculateVolumn&&
        this.props.concreterecord[key].StopPouringConcrete==false){
          pretruckcheckstopbetween=true
      }
      
    }

    if(newesttruck==true&&this.props.concreterecord&&this.props.concreterecord[key-1]&&this.props.concreterecord[key-1].StopPouringConcrete==true){

      let oldindex = this.props.concreterecord[key].NoText
      console.warn('oldindex',oldindex)
      let cut  = oldindex.split(' ')
      dropNo = Number.parseInt(cut[0], 10) +1
      console.warn('cut',cut,dropNo)
    }
    console.warn("PredepthArr", data)
    Actions.dropconcrete({
      edit: true,
      data: data,
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      process: this.state.process,
      step: this.state.step,
      Edit_Flag: "1",
      shape: this.props.shape,
      lat:
        this.state.location == undefined ? 1 : this.state.location.position.lat,
      log:
        this.state.location == undefined ? 1 : this.state.location.position.log,
      index: id,
      dropNo:dropNo,
      concretecumulative: concrete,
      tremiecutcumulative: tremie,
      tremiecutcumulativelength: tremielength,
      lastdepth: this.props.lastdepthArr[id],
      lastdepthconcrete: this.props.lastdepthconcreteArr[id],
      category: this.props.category,
      newesttruck: newesttruck,
      Predepth: Predepth,
      Precutsink: Precutsink,
      PreTruck: PreTruck,
      PredepthArr: PredepthArr,
      StopPourconVol:StopPourconVol , 
      pretruckcheckstopbetween:pretruckcheckstopbetween
    })
  }
  onViewData(id, data, concrete, tremie, tremielength) {
    // console.log(concrete, tremie, tremielength)
    let newesttruck = false
    if (id == this.props.concreterecord.length) {
      newesttruck = true
    }
    let Predepth = ""
    let Precutsink = ""
    let PreTruck = ""
    let PredepthArr = []
    let PredepthArrStop = false
    let StopPourconVol=0
    let pretruckcheckstopbetween =false
    if (this.props.concreterecord.length != 0) {
      console.warn(
        this.props.concreterecord[this.props.concreterecord.length - 1]
      )
      if (
        this.props.concreterecord[this.props.concreterecord.length - 1] !=
          null &&
        this.props.concreterecord[this.props.concreterecord.length - 1] != ""
      ) {
        PreTruck = this.props.concreterecord[
          this.props.concreterecord.length - 1
        ]
      }

      for (let i = id - 2; i >= 0; i--) {
        console.warn("i", i, id - 2, id, this.props.concreterecord[i])
        if (
          Predepth == "" &&
          this.props.concreterecord[i].Depth != "" &&
          this.props.concreterecord[i].Depth != null&&
          this.props.concreterecord[i].StopPouringConcrete!=true
          //  &&
          // this.props.category == 5
        ) {
          console.warn(
            "PredepthArrStop",
            PredepthArrStop,
            "i",
            i,
            this.props.concreterecord[i]
          )
          Predepth = this.props.concreterecord[i].Depth
          PredepthArrStop = true
        } else {
          if (PredepthArrStop != true&&
            this.props.concreterecord[i].StopPouringConcrete!=true) {
            PredepthArr.push(this.props.concreterecord[i])
          }
        }

        // if (
        //   Predepth == "" &&
        //   this.props.concreterecord[i].Depth != "" &&
        //   this.props.concreterecord[i].Depth != null &&
        //   this.props.category != 5
        // ) {
        //   Predepth = this.props.concreterecord[i].Depth
        // }
        
        if (
          Precutsink == "" &&
          this.props.concreterecord[i].TremyCutSinkAfter != null
        ) {
          console.warn("Precutsink", this.props.concreterecord[i].Precutsink)
          Precutsink = this.props.concreterecord[i].TremyCutSinkAfter
        }
      }
      if(this.props.concreterecord.length >= 2&&id>=2){
        // console.warn('this.props.concreterecord[id - 2]',this.props.concreterecord[id - 2].StopPouringConcrete)
        if( this.props.concreterecord[id - 2].StopPouringConcrete==true){
          // pretruckcheckstopbetween=true
            console.warn('this.props.concreterecord[i].StopPouringConcrete==true',)
          
          //   for (let i =2 ; i<0; i--){
          //   console.warn('test',i)
          // }
            for (let i = id - 2;i >=0&&this.props.concreterecord[i].StopPouringConcrete==true; i--) {
            console.warn('this.props.concreterecord[id - 2]',i,this.props.concreterecord[i].StopPouringConcrete==true)

            StopPourconVol +=this.props.concreterecord[i].ConcreteCalculateVolumn
          
          }


        }
      }
    }

    Actions.dropconcrete({
      edit: true,
      data: data,
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      process: this.state.process,
      step: this.state.step,
      Edit_Flag: "0",
      shape: this.props.shape,
      lat:
        this.state.location == undefined ? 1 : this.state.location.position.lat,
      log:
        this.state.location == undefined ? 1 : this.state.location.position.log,
      index: id,
      concretecumulative: concrete,
      tremiecutcumulative: tremie,
      tremiecutcumulativelength: tremielength,
      lastdepth: this.props.lastdepthArr[id],
      lastdepthconcrete: this.props.lastdepthconcreteArr[id],
      category: this.props.category,
      newesttruck: newesttruck,
      Predepth: Predepth,
      Precutsink: Precutsink,
      PreTruck: PreTruck,
      PredepthArr: PredepthArr,
      StopPourconVol:StopPourconVol , 
      pretruckcheckstopbetween:pretruckcheckstopbetween
    })
  }

  deleteRow(key) {
    Alert.alert(
      "",
      I18n.t("mainpile.10_3.confirmdelect"),
      [
        {
          text: "Cancel"
        },
        {
          text: "OK",
          onPress: () => {
            this.props.removeConcreteRecord({
              Language: I18n.currentLocale(),
              JobId: this.props.jobid,
              PileId: this.props.pileid,
              ConcreteRecordId: key,
              latitude:
                this.state.location != undefined
                  ? this.state.location.position.lat
                  : 1,
              longitude:
                this.state.location != undefined
                  ? this.state.location.position.log
                  : 1
            })
          }
        }
      ],
      { cancelable: false }
    )
  }

  render() {
    if (this.props.hidden) {
      return null
    }

    return (
      <View>
        <View style={styles.listContainer}>
          <View style={styles.listTopic}>
            <Text
              style={[
                styles.listTopicText,
                { fontSize: I18n.locale == "th" ? 20 : 18 }
              ]}
            >
              {I18n.t("mainpile.10_3.table_add")}{" "}
              {this.props.break && (
                <Text style={{ color: "red" }}>
                  ({I18n.t("mainpile.5_3.pileb")})
                </Text>
              )}
            </Text>

            {this.state.Edit_Flag == 1 ? (
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => this.regisButton2()}
                disabled={
                  this.state.Edit_Flag == 0 ||
                  ((this.props.category == 3 || this.props.category == 5) &&
                    this.state.Parentcheck == false)
                }
              >
                <View
                  style={[
                    styles.listButton,
                    (this.state.Edit_Flag == 0 ||
                      ((this.props.category == 3 || this.props.category == 5) &&
                        this.state.Parentcheck == false)) && {
                      backgroundColor: MENU_GREY_ITEM
                    }
                  ]}
                >
                  <Image
                    source={require("../../assets/image/icon_add.png")}
                    style={{ width: 20 }}
                    resizeMode={"contain"}
                  />
                  <Text style={styles.listButtonText}>
                    {I18n.t("mainpile.9_1.register")}
                  </Text>
                </View>
              </TouchableOpacity>
            ) : null}
            {this.state.Edit_Flag == 1 ? (
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => this.regisButton()}
                disabled={
                  this.props.lasttruck ||
                  this.state.Edit_Flag == 0 ||
                  ((this.props.category == 3 || this.props.category == 5) &&
                    this.state.Parentcheck == false)
                }
              >
                <View
                  style={[
                    styles.listButton,
                    (this.props.lasttruck ||
                      ((this.props.category == 3 || this.props.category == 5) &&
                        this.state.Parentcheck == false)) && {
                      backgroundColor: MENU_GREY_ITEM
                    }
                  ]}
                >
                  <Image
                    source={require("../../assets/image/icon_add.png")}
                    style={{ width: 20 }}
                    resizeMode={"contain"}
                  />
                  <Text style={styles.listButtonText}>
                    {I18n.t("mainpile.10_3.add_data")}
                  </Text>
                </View>
              </TouchableOpacity>
            ) : null}
          </View>
          {this.props.concreterecord.length > 0 ? (
            <View>
              <View
                style={{
                  marginTop: 10,
                  marginBottom: 10,
                  alignItems: "center",
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              >
                <Icon
                  raised
                  reverse
                  type="font-awesome"
                  color="#007CC2"
                  name={
                    this.state.sort == 3
                      ? "sort-alpha-asc"
                      : this.state.sort == 1
                      ? "sort-numeric-asc"
                      : "sort-numeric-desc"
                  }
                  onPress={() => {
                    if (this.state.sort == 1) {
                      this.setState({ sort: 2 }, () => {
                        console.warn(this.state.sort)
                      })
                    } else if (this.state.sort == 2) {
                      this.props.concreterecord.reverse()
                      this.setState({ sort: 3 }, () => {
                        console.warn(this.state.sort)
                      })
                    } else {
                      this.setState({ sort: 1 }, () => {
                        console.warn(this.state.sort)
                      })
                    }
                  }}
                  size={30}
                />
              </View>
              {this.renderTable()}
            </View>
          ) : (
            <View style={styles.listTableDetail}>
              <Text style={styles.listTableDetailText}>
                {I18n.t("mainpile.10_3.no_data")}
                {I18n.locale == "th" ? "\n" : ""}
                {I18n.locale == "th" ? (
                  <Text style={styles.textHighligh}>
                    {I18n.t("mainpile.10_3.add_data")}
                  </Text>
                ) : (
                  ""
                )}
                {I18n.locale == "th"
                  ? I18n.t("mainpile.10_3.add_data_test")
                  : ""}
              </Text>
            </View>
          )}
        </View>
        {/**
        <View style={[styles.listTopic, { marginTop: 10 }]}>
          <Text style={[styles.listTopicText, { color: "black" }]}>คอนกรีตสะสมที่ใช้จริง</Text>
          <View style={styles.listButtonLength}>
            <Text style={styles.listButtonTextLength}>{this.props.concretevolume}</Text>
          </View>
        </View>
            */}
      </View>
    )
  }

  renderTable() {
    const modaldata = [
      {
        key: 0,
        section: true,
        label: I18n.t("mainpile.9_1.selector"),
        action: "head"
      },
      {
        key: 1,
        label: I18n.t("mainpile.9_1.edit"),
        action: "edit"
      },
      {
        key: 2,
        label: I18n.t("mainpile.9_1.delete"),
        action: "delete"
      }
    ]
    const modaldata2 = [
      {
        key: 0,
        section: true,
        label: I18n.t("mainpile.9_1.selector"),
        action: "head"
      },
      {
        key: 1,
        label: I18n.t("mainpile.9_1.see"),
        action: "view"
      }
    ]
    const modaldata3 = [
      {
        key: 0,
        section: true,
        label: I18n.t("mainpile.9_1.selector"),
        action: "head"
      },
      {
        key: 1,
        label: I18n.t("mainpile.9_1.edit"),
        action: "edit"
      }
    ]
    var concreterecord = this.props.concreterecord
    // console.warn(concreterecord)
    // var indexcount = this.props.indexcount

    // 1 mean sort by number 1to9# so it doesn't need to be sort
    // 2 mean sort by number 9to1#
    // 3 mean sort by name
    if (this.state.sort == 3) {
      var concretetemp = []
      concreterecord.map((data, index) => {
        data.index = index + 1
        concretetemp.push(data)
      })
      // console.warn(concretetemp)

      concreterecord = concretetemp.sort((a, b) => {
        return naturalSort(
          a.ConcreteTruckRegister.TruckNo,
          b.ConcreteTruckRegister.TruckNo
        )
      })
    } else if (this.state.sort == 2) {
      // let concreterecord_re = this.props.concreterecord
      concreterecord = this.props.concreterecord.reverse()
      console.log("desc", this.props.concreterecord)
    } else {
      concreterecord = this.props.concreterecord
      console.log("aes", this.props.concreterecord)
    }
    this.props.getConcreteList10({
      language: I18n.currentLocale(),
      pileid: this.props.pileid,
      jobid: this.props.jobid
    })
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, justifyContent: "center" }}>
          <ActivityIndicator size="large" color="#007CC2" />
        </View>
      )
    }

    return (
      <View style={styles.lisTable}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={styles.tableCol}>
            <View style={styles.tableHeadGroup}>
              <View
                style={[
                  styles.tableHead,
                  { width: 90, height: I18n.locale == "th" ? 85 : 110 }
                ]}
              >
                <Text style={{ textAlign: "center" }}>
                  {I18n.t("mainpile.9_1.no")}
                </Text>
                <Text style={{ textAlign: "center", color: "green" }}>
                  {I18n.t("mainpile.9_1.carNo")}
                </Text>
              </View>
              <View
                style={[
                  styles.tableHead,
                  { width: 120, height: I18n.locale == "th" ? 85 : 110 }
                ]}
              >
                <Text style={{ textAlign: "center" }}>
                  {I18n.t("mainpile.10_3.concretecol")}
                </Text>
              </View>
              <View
                style={[
                  styles.tableHead,
                  { width: 120, height: I18n.locale == "th" ? 90 : 110 }
                ]}
              >
                <Text
                  style={{
                    textAlign: "center",
                    height: I18n.locale == "th" ? 85 : 105
                  }}
                >
                  {I18n.t("mainpile.10_3.depth")}
                  {this.props.category == 1||this.props.category ==4
                    ? I18n.t("mainpile.DropConcrete.bp")
                    : I18n.t("mainpile.DropConcrete.dw")}{" "}
                  {I18n.t("mainpile.DropConcrete.m")}
                </Text>
              </View>
              <View
                style={[
                  styles.tableHead,
                  { width: 100, height: I18n.locale == "th" ? 85 : 110 }
                ]}
              >
                <Text style={{ textAlign: "center" }}>
                  {I18n.t("mainpile.10_3.cutamount")}{" "}
                  {I18n.t("mainpile.DropConcrete.ton")}
                </Text>
              </View>
              <View
                style={[
                  styles.tableHead,
                  {
                    width: 240,
                    padding: 0,
                    height: I18n.locale == "th" ? 85 : 110
                  }
                ]}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    alignItems: "center",
                    borderBottomColor: "#636361",
                    borderBottomWidth: 1
                  }}
                >
                  <Text style={{ flex: 1, textAlign: "center" }}>
                    {I18n.t("mainpile.10_3.length1")}
                  </Text>
                </View>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View
                    style={{
                      flex: 1,
                      borderRightWidth: 1,
                      borderRightColor: "#636361"
                    }}
                  >
                    <Text
                      style={{ flex: 1, textAlign: "center", marginTop: 15 }}
                    >
                      {I18n.t("mainpile.10_3.cut")}
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      borderRightWidth: 1,
                      borderRightColor: "#636361"
                    }}
                  >
                    <Text
                      style={{ flex: 1, textAlign: "center", marginTop: 15 }}
                    >
                      {I18n.t("mainpile.10_3.l")}
                    </Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text
                      style={{ flex: 1, textAlign: "center", marginTop: 15 }}
                    >
                      {I18n.t("mainpile.10_3.aftercut")}
                    </Text>
                  </View>
                </View>
              </View>
            </View>

            {concreterecord.map((item, key) => {
              var truck = item.ConcreteTruckRegister || []
              // console.warn("item",key)
              let PreTruckEnd = ""
              let PostTruckStart = ""
              for (let i = 0; i < concreterecord.length; i++) {
                if (item.No == this.props.concreterecord[i].No) {
                  if (
                    this.props.concreterecord[i - 1] != null &&
                    this.props.concreterecord[i - 1].EndConcreting != null &&
                    this.props.concreterecord[i - 1].EndConcreting != ""
                  ) {
                    PreTruckEnd = this.props.concreterecord[i - 1].EndConcreting
                    console.warn("PreTruckEnd", PreTruckEnd)
                  }
                  if (
                    this.props.concreterecord[i + 1] != null &&
                    this.props.concreterecord[i + 1].EndConcreting != null &&
                    this.props.concreterecord[i + 1].StartConcreting != ""
                  ) {
                    PostTruckStart = this.props.concreterecord[i + 1]
                      .StartConcreting
                    console.warn("PostTruckStart", PostTruckStart)
                  }
                }
              }
              var newest = false

              if (key + 1 == this.props.concreterecord.length) {
                var newest = true
              }
              console.warn(
                "data item",
                item
              )
              // if(concreterecord.length-1==key&&item.EndConcreting==""&&this.state.trigger!=true){

              //   this.setState({trigger:true})
              // }
              // console.warn("item.Id==this.props.concreterecord.length",newest,key+1==this.props.concreterecord.length,)
              const data = {
                id: item.Id,
                truckno: truck.TruckNo,
                truckid: truck.Id,
                concretevolume: truck.TruckConcreteVolume,
                depth: item.Depth,
                startconcrete: item.StartConcreting,
                endconcrete: item.EndConcreting,
                tremiecutcount: item.TremieCutCount || 0,
                imageconcrete: item.ImageConcrete,
                imagelasttruck: item.ImageConcreteLastTruckRecord,
                IsLastTruck: item.IsLastTruck,
                LastTruckCheckPVCandPlummet: item.LastTruckCheckPVCandPlummet,
                LastTruckPlummetConcreteHeight:
                  item.LastTruckPlummetConcreteHeight || 0,
                LastTruckPVCConcreteHeight:
                  item.LastTruckPVCConcreteHeight || 0,
                LastTruckConcreteVolume: item.LastTruckConcreteVolume,
                TremyCutLength: item.TremyCutLength || 0,
                TremyCutSinkAfter: item.TremyCutSinkAfter || 0,
                TremyCutSinkBefore: item.TremyCutSinkBefore || 0,
                TremyCutLeft: item.TremyCutLeft || 0,
                TruckArrivalTime: truck.TruckArrivalTime,
                PreTruckEnd: PreTruckEnd,
                PostTruckStart: PostTruckStart,
                RemainConcreteVolumn:item.RemainConcreteVolumn,
                WidthConcrete:item.WidthConcrete,
                LengthConcrete:item.LengthConcrete,
                HeightConcrete:item.HeightConcrete,

                ImageConcreteRecordDump:item.ImageConcreteRecordDump,
                RemainConcrete: item.RemainConcrete,
                RemainConcretePileNo: item.RemainConcretePileNo,

                StopPouringConcrete:item.StopPouringConcrete,
                ConcreteCalculateVolumn: item.ConcreteCalculateVolumn,
                itemindex:item.NoText
              }
              // console.log("pile10_3data",data.endconcrete)

              return (
                <ModalSelector
                  data={
                    this.state.Edit_Flag == 0
                      ? modaldata2
                      : this.props.category == 3 || this.props.category == 5
                      ? this.state.Parentcheck == true
                        ? newest == true
                          ? modaldata
                          : modaldata3
                        : modaldata2
                      : newest == true
                      ? modaldata
                      : modaldata3
                  }
                  cancelText="Cancel"
                  onModalClose={option => {
                    if (option.action == "edit") {
                      this.onEditData(
                        item.NoText,
                        data,
                        this.props.concretecumulativeArr[key],
                        this.props.tremiecutcumulativeArr[key],
                        this.props.tremiecutcumulativelengthArr[key],
                        key
                      )
                    } else if (option.action == "delete") {
                      this.deleteRow(item.Id)
                    } else if (option.action == "view") {
                      this.onViewData(
                        item.NoText,
                        data,
                        this.props.concretecumulativeArr[key],
                        this.props.tremiecutcumulativeArr[key],
                        this.props.tremiecutcumulativelengthArr[key]
                      )
                    }
                  }}
                  key={key}
                  // disabled={this.state.Edit_Flag == 0}
                >
                  <View key={key} style={styles.tableContentGroup}>
                    <View style={[styles.tableContent, { width: 90 }]}>
                      <Text style={{ textAlign: "center" }}>
                        {item.NoText}
                      </Text>
                      <Text style={{ textAlign: "center", color: "green" }}>
                        {truck.TruckNo}
                      </Text>
                    </View>
                    <View
                      style={[
                        styles.tableContent,
                        { width: 120 },
                        styles.tableContentCenter
                      ]}
                    >
                      <Text>{ parseFloat(this.props.concretecumulativeArr[key + 1]).toFixed(2)}</Text>
                     
                    </View>
                    <View
                      style={[
                        styles.tableContent,
                        { width: 120 },
                        styles.tableContentCenter
                      ]}
                    >
                      <Text>{item.Depth == null ? "-" : item.Depth}</Text>
                    </View>
                    <View
                      style={[
                        styles.tableContent,
                        { width: 100 },
                        styles.tableContentCenter
                      ]}
                    >
                      <Text>{item.TremieCutCount || "0"}</Text>
                    </View>
                    <View
                      style={[
                        styles.tableContent,
                        { width: 240, flexDirection: "row", padding: 0 },
                        styles.tableContentCenter
                      ]}
                    >
                      <View
                        style={{
                          flex: 1,
                          borderRightWidth: 1,
                          borderRightColor: "#636361"
                        }}
                      >
                        <Text
                          style={{
                            flex: 1,
                            textAlign: "center",
                            marginTop: 20
                          }}
                        >
                          {item.TremyCutLength || "0"}
                        </Text>
                      </View>
                      <View
                        style={{
                          flex: 1,
                          borderRightWidth: 1,
                          borderRightColor: "#636361"
                        }}
                      >
                        <Text
                          style={{
                            flex: 1,
                            textAlign: "center",
                            marginTop: 20
                          }}
                        >
                          {item.TremyCutLeft || "0"}
                        </Text>
                      </View>
                      <View style={{ flex: 1 }}>
                        <Text
                          style={{
                            flex: 1,
                            textAlign: "center",
                            marginTop: 20
                          }}
                        >
                          {item.TremyCutLength > 0
                            ? item.TremyCutSinkAfter
                            : "-"}
                        </Text>
                      </View>
                    </View>
                  </View>
                </ModalSelector>
              )
            })}
          </View>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll,
    pile9success: state.pile.pile9success,
    concreterecord: state.concrete.concreterecord,
    tremiecut: state.concrete.tremiecut,
    tremieleft: state.concrete.tremieleft,
    tremiedeep: state.concrete.tremiedeep,
    concretecumulative: state.concrete.concretecumulative,
    tremiecutcumulative: state.concrete.tremiecutcumulative,
    tremiecutcumulativelength: state.concrete.tremiecutcumulativelength,
    break: state.concrete.break,
    concretecumulativeArr: state.concrete.concretecumulativeArr,
    tremiecutcumulativeArr: state.concrete.tremiecutcumulativeArr,
    tremiecutcumulativelengthArr: state.concrete.tremiecutcumulativelengthArr,
    concretelist: state.concrete.concretelist,
    concretevolume: state.concrete.concretevolume,
    concreteid: state.concrete.concreteid,
    mixid: state.concrete.mixid,
    lasttruck: state.concrete.lasttruck,
    lastdepthArr: state.concrete.lastdepthArr,
    lastdepthconcreteArr: state.concrete.lastdepthconcreteArr
  }
}

export default connect(mapStateToProps, actions, null, { withRef: true })(
  Pile10_3
)
