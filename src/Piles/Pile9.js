import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity
} from "react-native"
import { connect } from "react-redux"
import { Container, Content } from "native-base"
import StepIndicator from "react-native-step-indicator"
import * as actions from "../Actions"
import Pile9_1 from "./Pile9_1"
import styles from "./styles/Pile8.style"
import I18n from "../../assets/languages/i18n"
import {
  MAIN_COLOR,
  DEFAULT_COLOR_1,
  DEFAULT_COLOR_4
} from "../Constants/Color"
class Pile9 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      step: 0,
      process: 9,
      Edit_Flag: 1,
      error: null,
      data: [],
      location: {
        position: {
          lat: 1,
          log: 1
        }
      },
      concreterecord: [],
      status10: 0,
      status8: 0,
      checksavebutton: false,
      stepstatus: null,
      Parentcheck: false
    }
  }
  componentDidMount() {
    // console.log(this.props.stack)
    if (this.props.stack) {
      this.setState({
        step: this.props.stack[this.props.stack.length - 1].step - 1
      })
    }
    this.props.getConcreteRecord({
      pileid: this.props.pileid,
      jobid: this.props.jobid
    })
  }
  async componentWillReceiveProps(nextProps) {
    let pileMaster = null
    if (
      nextProps.step_status2_random != this.state.stepstatus &&
      nextProps.processstatus == 9
    ) {
      this.setState({ stepstatus: nextProps.step_status2_random }, async () => {
        // console.warn('yes6',nextProps.step_status2.Step2Status)
        await this.props.mainPileSetStorage(this.props.pileid, "step_status", {
          Step9Status: nextProps.step_status2.Step9Status
        })
        this.props.mainPileGetStorage(this.props.pileid, "step_status")
      })
    }
    if (nextProps.pileAll[this.props.pileid] != undefined) {
      var edit = nextProps.pileAll[this.props.pileid]
      pileMaster = nextProps.pileAll[this.props.pileid]["9_1"].masterInfo
      if (edit["9_0"] != undefined) {
        await this.setState({ Edit_Flag: edit["9_0"].data.Edit_Flag })
      }
    }
    if (pileMaster) {
      if (pileMaster.PileDetail) {
        this.setState({
          Parentcheck:
            pileMaster.PileDetail.pile_id == pileMaster.PileDetail.parent_id
              ? true
              : false
        })
      }
    }

    if (edit != undefined) {
      this.setState({
        status10: edit.step_status.Step10Status,
        status8: edit.step_status.Step8Status
      })
    }

    await this.setState(
      {
        error: nextProps.error,
        data: nextProps.pileAll[nextProps.pileid],
        location: nextProps.pileAll.currentLocation,
        checksavebutton: false
      },
      () => {
        // console.warn("checkPareant",this.state.data['9_1'].masterInfo.PileDetail)
      }
    )
    if (nextProps.concreterecord != undefined) {
      this.setState({ concreterecord: nextProps.concreterecord })
    }
  }

  async onSetStack(pro, step) {
    var data = {
      process: pro,
      step: step + 1
    }
    await this.props.setStack(data)
    await this.props.mainPileGetAllStorage()
  }

  nextButton = () => {
    this.onSave()
  }

  async onSave() {
    // var statuscolor = 1
    // if (this.props.concretelist.length > 0) {
    //   statuscolor = 2
    // }
    this.setState({ checksavebutton: true })
    var statuscolor = this.props.status
    // console.warn(statuscolor)

    // await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step9Status: statuscolor })

    // this.props.mainPileGetStorage(this.props.pileid, "step_status")
    this.props.getStepStatus2({
      jobid: this.props.jobid,
      pileid: this.props.pileid,
      process: 9
    })
    if (this.state.status8 == 2) {
      this.props.onNextStep()
      this.onSetStack(10, 0)
    } else {
      Alert.alert("", I18n.t("mainpile.lockprocess.process10_dw"), [
        {
          text: "OK"
        }
      ])
    }

    setTimeout(() => {
      this.setState({ checksavebutton: false })
    }, 2000)

    /** 
    if (statuscolor == 2) {
      this.props.onNextStep()
      this.onSetStack(10,0)
    } else {
      Alert.alert("", I18n.t("mainpile.8_1.error_nextstep"), [
        {
          text: "OK"
        }
      ])
    }
    */
    // await this.props.mainPileGetAllStorage()
    // if (step == 0) {
    //   var value = {
    //     process: this.state.process,
    //     step: 1,
    //     pile_id: this.props.pileid
    //   }
    //   if (this.state.Edit_Flag == 1) {
    //     this.props.mainpileAction_checkStepStatus(value).then(async data => {
    //       await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step8Status: data.statuscolor })
    //       this.props.mainPileGetStorage(this.props.pileid, "step_status")

    //       var valueApi = {
    //         Language: I18n.currentLocale(),
    //         JobId: this.props.jobid,
    //         PileId: this.props.pileid,
    //         Page: 1,
    //         MachineId: this.state.data["8_1"].data.MachineId,
    //         DriverId: this.state.data["8_1"].data.DriverId,
    //         latitude: this.state.location.position.lat,
    //         longitude: this.state.location.position.log
    //       }
    //       this.props.pileSaveStep08(valueApi)

    //       if (this.state.error == null) {
    //         if (data.check == false) {
    //           Alert.alert("", I18n.t("mainpile.8_1.error_nextstep"), [
    //             {
    //               text: "OK"
    //             }
    //           ])
    //         } else {
    //           this.setState({ step: 1 })
    //           this.onSetStack(1)
    //         }
    //       } else {
    //         Alert.alert(this.state.error)
    //       }
    //     })
    //   }
    // } else if (step == 1) {
    //   var value = {
    //     process: 8,
    //     step: 2,
    //     pile_id: this.props.pileid
    //   }
    //   if (this.state.Edit_Flag == 1) {
    //     this.props.mainpileAction_checkStepStatus(value).then(async data => {
    //       await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step8Status: data.statuscolor })
    //       // this.props.mainPileGetStorage(this.props.pileid, "step_status")

    //       if (this.state.error == null) {
    //         if (data.check == false) {
    //           Alert.alert("", I18n.t("mainpile.8_1.error_nextstep"), [
    //             {
    //               text: "OK"
    //             }
    //           ])
    //         } else {
    //           this.setState({ step: 2 })
    //           this.onSetStack(2)
    //         }
    //       } else {
    //         Alert.alert(this.state.error)
    //       }
    //     })
    //   }
    // } else if (step == 2) {
    //   var value = {
    //     process: 8,
    //     step: 3,
    //     pile_id: this.props.pileid
    //   }
    //   if (this.state.Edit_Flag == 1) {
    //     this.props.mainpileAction_checkStepStatus(value).then(async data => {
    //       await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step8Status: data.statuscolor })
    //       this.props.mainPileGetStorage(this.props.pileid, "step_status")

    //       var imagefile = []
    //       if (this.state.data["8_3"].data.foamimage != null && this.state.data["8_3"].data.foamimage.length > 0) {
    //         for (var i = 0; i < this.state.data["8_3"].data.foamimage.length; i++) {
    //           imagefile.push(this.state.data["8_3"].data.foamimage[i])
    //         }
    //       }

    //       var valueApi = {
    //         Language: I18n.currentLocale(),
    //         JobId: this.props.jobid,
    //         PileId: this.props.pileid,
    //         Page: 3,
    //         AddFoam: this.state.data["8_3"].data.checkfoam,
    //         FoamImage: imagefile,
    //         latitude: this.state.location.position.lat,
    //         longitude: this.state.location.position.log
    //       }
    //       this.props.pileSaveStep08(valueApi)

    //       if (this.state.error == null) {
    //         if (data.check == false) {
    //           Alert.alert("", I18n.t("mainpile.8_1.error_nextstep"), [
    //             {
    //               text: "OK"
    //             }
    //           ])
    //         } else {
    //           this.props.onNextStep()
    //           this.onSetStack(3)
    //         }
    //       } else {
    //         Alert.alert(this.state.error)
    //       }
    //     })
    //   }
    // }
  }

  _renderStepFooter() {
    return (
      <View>
        <View style={{ flexDirection: "column" }}>
          {/* <View style={{ borderWidth: 1 }}>
            <StepIndicator
              stepCount={3}
              onPress={step => {
                this.setState({ step: step })
                this.onSetStack(step)
              }}
              currentPosition={this.state.step}
            />
          </View>
            */}
          {this.state.Edit_Flag == 1 ? (
            <View style={styles.second}>
              <TouchableOpacity
                style={styles.firstButton}
                onPress={this.deleteButton}
                disabled={
                  this.state.checksavebutton ||
                  (this.props.category == 3 && this.state.Parentcheck == false)
                }
                // disabled={this.state.concreterecord.length == 0 ? false:true}
              >
                <Text style={styles.selectButton}>
                  {I18n.t("mainpile.button.delete")}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.secondButton,
                 ( this.props.category == 3|| this.props.category == 5)
                    ? this.state.Parentcheck == false
                      ? {
                          backgroundColor: "#BABABA"
                        }
                      : {
                          backgroundColor: DEFAULT_COLOR_4
                        }
                    : this.props.shape == 1
                      ? { backgroundColor: MAIN_COLOR }
                      : { backgroundColor: DEFAULT_COLOR_1 }
                ]}
                onPress={this.nextButton}
                disabled={
                  this.state.checksavebutton ||
                  (this.props.category == 3 && this.state.Parentcheck == false)
                }
              >
                <Text style={styles.selectButton}>
                  {I18n.t("mainpile.button.next")}
                </Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View />
          )}
        </View>
      </View>
    )
  }

  deleteButton = () => {
    this.props.setStack({
      process: 9,
      step: 1
    })
    const data = {
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      processid: this.state.process,
      lat:
        this.state.location != undefined ? this.state.location.position.lat : 1,
      log:
        this.state.location != undefined ? this.state.location.position.log : 1
    }

    if (this.state.status10 == 0) {
      Alert.alert("Warning", I18n.t("mainpile.button.confirmdelete"), [
        { text: "Cancel" },
        { text: "OK", onPress: () => this.props.pileDelete(data) }
      ])
    } else {
      Alert.alert("", I18n.t("alert.error_delete8"), [
        {
          text: "OK"
        }
      ])
    }
  }

  updateState(value) {
    const ref = "pile" + value.process + "_" + value.step
    if (this.refs[ref].getWrappedInstance()) {
      this.refs[ref].getWrappedInstance().updateState(value)
    }
  }
  initialStep() {
    this.setState({ step: 0 })
  }

  render() {
    if (this.props.hidden) {
      return null
    }

    return (
      <View style={{ flex: 1 }}>
        <Content>
          <Pile9_1
            ref="pile9_1"
            hidden={!(this.state.step == 0)}
            jobid={this.props.jobid}
            pileid={this.props.pileid}
            shape={this.props.shape}
            onRefresh={this.props.onRefresh}
            category={this.props.category}
          />
        </Content>
        {this._renderStepFooter()}
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    error: state.pile.error,
    pileAll: state.mainpile.pileAll || null,
    concretelist: state.concrete.concretelist,
    status: state.concrete.status,
    stack: state.mainpile.stack_item,
    concreterecord: state.concrete.concreterecord,

    step_status2_random: state.mainpile.step_status2_random,
    step_status2: state.mainpile.step_status2,
    processstatus: state.mainpile.process
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(Pile9)
