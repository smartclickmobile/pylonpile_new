import React, { Component } from "react"
import { View, Text, TouchableOpacity, TextInput, ScrollView ,Linking} from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"
import I18n from "../../assets/languages/i18n"
import styles from "./styles/Pile7_3.style"
import { MENU_GREY } from "../Constants/Color"
import { Icon } from "react-native-elements"
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from "moment"
class Pile7_3 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 7,
      step: 3,
      steelCageNo: "-",
      steelCages: [],
      steelCages1: [],
      Edit_Flag: 1,
      shape: 0,
      location: {
        position: {
          lat: 1,
          log: 1
        }
      },
      datevisibletime:false,
      datevisibledate:false,
      startdate:'',
      enddate:'',
      type:'',
      datestart:'',
      timestart:'',
      dateend:'',
      timeend:'',
      set:false,
      Step7Status:0,
      rbsheet:''
    }
  }

  componentWillReceiveProps(nextProps) {
    var pile = null
    var pileMaster = null
    var pileMaster1 = null
    var pile7 = null
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["7_3"]) {
      pile = nextProps.pileAll[this.props.pileid]["7_3"].data
      pileMaster = nextProps.pileAll[this.props.pileid]["7_3"].masterInfo
      this.setState({Step7Status:nextProps.pileAll[this.props.pileid].step_status.Step7Status})
    }
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["7_0"]) {
      pile7 = nextProps.pileAll[this.props.pileid]["7_0"].data
    }

    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["1_2"]) {
      pileMaster1 = nextProps.pileAll[this.props.pileid]["1_2"].data
    }

    if (nextProps.pileAll != undefined && nextProps.pileAll != "") {
      this.setState({ location: nextProps.pileAll.currentLocation })
    }
    
    if(!this.state.set){
      if(this.props.startdatedefault != '' && this.props.startdatedefault != null){
        if(this.props.enddate != '' && this.props.enddate != null){
          this.setState({
            datestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
            timestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("HH:mm"),
            dateend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
            timeend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("HH:mm"),
            startdate:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
            enddate:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm")
          })
        }else{
          this.setState({
            datestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
            timestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("HH:mm"),
            dateend:'',
            timeend:'',
            startdate:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
            enddate:''
          })
        }
      }else{
        if(this.props.startdate != '' && this.props.startdate != null){
          this.setState({
            datestart:moment(this.props.startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
            timestart:moment(this.props.startdate,"DD/MM/YYYY HH:mm").format("HH:mm"),
            startdate:moment(this.props.startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
          })
        }
        if(this.props.enddate != '' && this.props.enddate != null){
          this.setState({
            dateend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
            timeend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("HH:mm"),
            enddate:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm")
          })
        }else{
          this.setState({
            dateend:'',
            timeend:''
          })
        }
      }
      
    }
    if(nextProps.pileAll[this.props.pileid]&&nextProps.pileAll[this.props.pileid]['7_3'].startdate != null && nextProps.pileAll[this.props.pileid]['7_3'].startdate != ''){
      this.setState({
        datestart:moment(nextProps.pileAll[this.props.pileid]['7_3'].startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
        timestart:moment(nextProps.pileAll[this.props.pileid]['7_3'].startdate,"DD/MM/YYYY HH:mm").format("HH:mm"),
        startdate:moment(nextProps.pileAll[this.props.pileid]['7_3'].startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
      })
    }
    if(nextProps.pileAll[this.props.pileid]&&nextProps.pileAll[this.props.pileid]['7_3'].enddate != null && nextProps.pileAll[this.props.pileid]['7_3'].enddate != ''){
      this.setState({
        dateend:moment(nextProps.pileAll[this.props.pileid]['7_3'].enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
        timeend:moment(nextProps.pileAll[this.props.pileid]['7_3'].enddate,"DD/MM/YYYY HH:mm").format("HH:mm"),
        enddate:moment(nextProps.pileAll[this.props.pileid]['7_3'].enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
      })
    }
    
    if (pileMaster) {
      console.log('pile7 test1',pileMaster.Steelcage.SectionList)
      if (pileMaster.Steelcage && this.state.steelCages.length == 0) {
        this.setState({
          steelCageNo: pileMaster.Steelcage.steelcage_no,
          steelCages: pileMaster.Steelcage.SectionList,
          local: false
        })
      }
      if (pileMaster.Shape) {
        this.setState({ shape: pileMaster.Shape })
      }
    }
    if(pileMaster &&pileMaster.rbsheet){
      console.warn('pileMaster.rbsheet',pileMaster.rbsheet)
      this.setState({rbsheet:pileMaster.rbsheet})

    }

    // if (pileMaster) {
    //   if (pileMaster.Steelcage && pileMaster.Steelcage.SectionList) {
    //     var steelcage_temp = []
    //     pileMaster.Steelcage.SectionList.map((data, index) => {
    //       var data_temp = data
    //       if (pile && pile.SectionDetail) {
    //         var value = pile.SectionDetail[pile.SectionDetail.length - index - 1]

    //         data_temp.checksteelcage =  value.checksteelcage
    //         data_temp.image_steelcage = value.image_steelcage
    //         data_temp.checkconcretespacer = value.checkconcretespacer
    //         data_temp.image_concretespacer = value.image_concretespacer
    //         // image_steelcage: data.image_steelcage,
    //       }
    //       steelcage_temp.push(data_temp)
    //     })
    //     this.setState({ steelCages: steelcage_temp })
    //   }
    // }

    // if (pileMaster) {
    //   if (pileMaster.Steelcage && pileMaster.Steelcage.SectionList) {
    //     this.setState({ steelCages: pileMaster.Steelcage.SectionList })
    //   }
    // }

    if (pileMaster1) {
      if (pileMaster1.SectionDetail) {
        // console.log(pileMaster1.SectionDetail)
        console.log('pile7 test2',pileMaster1.SectionDetail)
        this.setState({ steelCages1: pileMaster1.SectionDetail })
      }
    }

    if (pile) {
      // console.warn(pile)
      if (pile.local) {
        console.log('pile7 test3',pile.local)
        this.setState({ steelCages: pile.SectionDetail, local: true })
      }
    
    }
    if (pile7) {
      this.setState({ Edit_Flag: pile7.Edit_Flag })
    }
  }

  updateState(value) {
    // console.log(value)
    if (value.data.sectionid > 0) {
      let steelCages = this.state.steelCages
      for (var i in steelCages) {
        if (value.data.sectionid == steelCages[i].section_id) {
          steelCages[i].checkconcretespacer = value.data.checkconcretespacer
          steelCages[i].checksteelcage = value.data.checksteelcage
          steelCages[i].image_concretespacer = value.data.image_concretespacer
          steelCages[i].image_steelcage = value.data.image_steelcage
          steelCages[i].startdate = value.data.startdate
          steelCages[i].endtdate = value.data.enddate
        }
      }
      this.setState({ steelCages, status: 1 }, () => {
        this.saveLocal("7_3")
      })
    } else {
      this.setState(value.data, () => {
        this.saveLocal("7_3")
      })
    }
  }
  async mixdate(){
    if( this.state.datestart != '' && this.state.timestart != ''){
      await this.setState({startdate:this.state.datestart+' '+this.state.timestart})
     }
     if(this.state.dateend != '' && this.state.timeend != ''){
       await this.setState({enddate:this.state.dateend+' '+this.state.timeend})
     }
  }
  async saveLocal(process_step) {
    await this.mixdate()
    // console.warn(this.state.startdate)
    var value = {
      process: 7,
      step: 3,
      pile_id: this.props.pileid,
      data: {
        SectionDetail: this.state.steelCages,
        concretedisabled: this.state.shape == 1,
        local: true,
        shape: this.state.shape,
        startdate: this.state.steelCages.startdate,
        enddate: this.state.steelCages.enddate
      },
      startdate:this.state.startdate,
      enddate:this.state.enddate
    }
    console.log("save Local 7_3", value)
    this.props.mainPileSetStorage(this.props.pileid, process_step, value)
  }

  iconPressed({ sectionid, checksteelcage, checkconcretespacer, image_steelcage, image_concretespacer }) {
    console.log("shape check",this.props.shape)
    Actions.steeldetail({
      process: this.state.process,
      step: this.state.step,
      title: I18n.t("mainpile.steeldetail.title"),
      pileid: this.props.pileid,
      sectionid: sectionid,
      checksteelcage: checksteelcage,
      checkconcretespacer: checkconcretespacer,
      concretedisabled: this.props.shape == 1,
      image_steelcage: image_steelcage,
      image_concretespacer: image_concretespacer,
      jobid: this.props.jobid,
      Edit_Flag: this.state.Edit_Flag,
      lat: this.state.location == undefined ? 1: this.state.location.position.lat,
      log: this.state.location == undefined ? 1: this.state.location.position.log,
      shape: this.props.shape,
      category: this.props.category,
      startdate : this.props.startdate,
      checkbox:this.state.Step7Status==2?0:this.state.Edit_Flag,
      status:this.state.Step7Status
    })
  }

  PhotoPress_concretespacer({ sectionid, checksteelcage, checkconcretespacer, image_steelcage, image_concretespacer }) {
    this.onCameraRoll(
      "image_concretespacer",
      image_concretespacer,
      this.state.shape == 1 ? "view" : "edit",
      sectionid,
      checksteelcage,
      checkconcretespacer,
      image_steelcage,
      image_concretespacer
    )
  }
  PhotoPress_steelcage({ sectionid, checksteelcage, checkconcretespacer, image_steelcage, image_concretespacer }) {
    this.onCameraRoll(
      "image_steelcage",
      image_steelcage,
      "edit",
      sectionid,
      checksteelcage,
      checkconcretespacer,
      image_steelcage,
      image_concretespacer
    )
  }

  onCameraRoll(
    keyname,
    photos,
    type,
    sectionid,
    checksteelcage,
    checkconcretespacer,
    image_steelcage,
    image_concretespacer
  ) {
    if (this.state.Edit_Flag == 1) {
      Actions.camearaRoll({
        jobid: this.props.jobid,
        process: this.state.process,
        step: this.state.step,
        pileId: this.props.pileid,
        keyname: keyname,
        photos: photos,
        typeViewPhoto: "view",
        sectionid,
        checksteelcage,
        checkconcretespacer,
        image_steelcage,
        image_concretespacer,
        shape: this.props.shape,
        category:this.props.category,
        Edit_Flag:0
      })
    } else {
      Actions.camearaRoll({
        jobid: this.props.jobid,
        process: this.state.process,
        step: this.state.step,
        pileId: this.props.pileid,
        keyname: keyname,
        photos: photos,
        typeViewPhoto: "view",
        sectionid,
        checksteelcage,
        checkconcretespacer,
        image_steelcage,
        image_concretespacer,
        shape: this.props.shape,
        category:this.props.category,
        Edit_Flag:0
      })
    }
  }

  _renderTable() {
    return this.state.steelCages.slice(0).reverse().map((sec, index) => {
      let imgconcrete = sec.image_concretespacer
      let checkconcrete = sec.checkconcretespacer
      let lastitem = false
      // console.log(index)
      if (this.state.steelCages1 && (this.state.steelCages1.length > index) && this.props.shape == 1) {
        this.state.steelCages1.map((sec1, index1) => {
          if(sec1.sectionId == undefined){
            if (sec1.section_id == sec.section_id) {
                imgconcrete = this.state.steelCages1[index1].image_concretespacer
                checkconcrete = this.state.steelCages1[index1].checkconcretespacer
            }
          }else{
            if (sec1.sectionId == sec.section_id) {   
                imgconcrete = this.state.steelCages1[index1].ImageConcretespacer
                checkconcrete = this.state.steelCages1[index1].CheckConcretespacer
            }
          }
          
        })
        // imgconcrete = this.state.steelCages1[this.state.steelCages1.length - 1 - index].image_concretespacer
        // checkconcrete = this.state.steelCages1[this.state.steelCages1.length - 1 - index].checkconcretespacer
      }
      if(this.state.steelCages1.length-1==index){
        lastitem=true
      }
      console.warn('steelCages master',this.state.steelCages1)
      console.warn('steelCages value info',this.state.steelCages)
      return (
        <Table
          name={sec.section_no}
          se={sec.se}
          checksteelcage={sec.checksteelcage}
          // checkconcretespacer={sec.checkconcretespacer}
          checkconcretespacer={checkconcrete}
          concretedisabled={this.state.shape == 1}
          // image_concretespacer={sec.image_concretespacer}
          image_concretespacer={imgconcrete}
          image_steelcage={sec.image_steelcage}
          key={index}
          firstitem={index==0?true:false}
          lastitem={lastitem}
          shape = {this.props.shape}
          PhotoPress_concretespacer={() =>
            this.PhotoPress_concretespacer({
              sectionid: sec.section_id,
              checksteelcage: sec.checksteelcage,
              checkconcretespacer: checkconcrete,
              image_steelcage: sec.image_steelcage,
              image_concretespacer: imgconcrete
            })
          }
          PhotoPress_steelcage={() =>
            this.PhotoPress_steelcage({
              sectionid: sec.section_id,
              checksteelcage: sec.checksteelcage,
              checkconcretespacer: checkconcrete,
              image_steelcage: sec.image_steelcage,
              image_concretespacer: imgconcrete
            })
          }
          iconPressed={() =>
            this.iconPressed({
              sectionid: sec.section_id,
              checksteelcage: sec.checksteelcage,
              checkconcretespacer: checkconcrete,
              image_steelcage: sec.image_steelcage,
              image_concretespacer: imgconcrete
            })
          }
        />
      )
    })
  }
  onSelectDate(date){
    let date2 = moment(date).format("DD/MM/YYYY")
    if(this.state.type=='start'){
      this.setState({ datestart: date2 ,set:true,datevisibledate: false})
      this.saveLocal('7_3')
    }else if(this.state.type=='end'){
      this.setState({ dateend: date2 ,set:true,datevisibledate: false})
      this.saveLocal('7_3')
    }
  }
  onSelectTime(date){
    let date2 = moment(date).format("HH:mm")
    if(this.state.type=='start'){
      this.setState({ timestart: date2 ,set:true,datevisibletime: false})
      this.saveLocal('7_3')
    }else if(this.state.type=='end'){
      this.setState({ timeend: date2 ,set:true,datevisibletime: false})
      this.saveLocal('7_3')
    }
  }
  getDate(){
    if(this.state.type == 'start'){
      if(this.state.startdate != ''){
        return new Date(moment(this.state.startdate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        if(this.props.startdate != '' && this.props.startdate != undefined && this.props.startdate != null){
          return new Date(moment(this.props.startdate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
        }else{
          return new Date()
        }
      }
    }else{
      if(this.state.enddate != ''){
        return new Date(moment(this.state.enddate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        if(this.props.enddate != '' && this.props.enddate != undefined && this.props.enddate != null){
          return new Date(moment(this.props.enddate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
        }else{
          return new Date()
        }
      }
    }
  }
  onChangedate = (event, selectedDate) => {
    if(event.type == 'set'){
      this.onSelectDate(selectedDate)
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };
  onChangetime = (event, selectedDate) => {
    if(event.type == 'set'){
      this.onSelectTime(selectedDate)
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };
  render() {
    if (this.props.hidden) {
      return null
    }
    return (
      <View>
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t("mainpile.7_3.topic")}</Text>
        </View>
        <View style={styles.subTopic}>
          <View style={{ width: "40%", justifyContent: "center" }}>
            <Text style={{ fontWeight: "bold" }}>{I18n.t("mainpile.7_3.drawingno")}</Text>
          </View>
          <View style={{ width: "40%", backgroundColor: MENU_GREY, justifyContent: "center" }}>
            <Text style={{ marginLeft: 10 }}>{this.state.steelCageNo || "-"}</Text>
          </View>
          <TouchableOpacity style={[
              { width: "15%", backgroundColor: "#6bacd5", justifyContent: 'center',marginLeft: 10 ,borderRadius:3},
              this.state.rbsheet==''&&{backgroundColor:'grey'}]} 
            onPress={() =>{
                        if(this.state.rbsheet=='')return
                        console.log("rbsheet: " + this.state.rbsheet)
                        Linking.openURL(this.state.rbsheet)
                        // Linking.canOpenURL(this.state.rbsheet).then(supported => {
                        //   if (supported) {
                        //     Linking.openURL(this.state.rbsheet)
                        //   } else {
                        //     console.log("Don't know how to open URI: " + this.state.rbsheet)
                        //   }
                        // })
                      }}>
              <Icon name="file-text" type='font-awesome' color='#fff' size={20}/>
            </TouchableOpacity>
        </View>
        <View>
          <Text>{ /*JSON.stringify(this.state.steelCages) */}</Text>
          <ScrollView style={{ marginTop: 20 }}>{this._renderTable()}</ScrollView>
        </View>
        {this.state.datevisibledate &&<DateTimePicker
          value={this.getDate()}
          isVisible={this.state.datevisibledate}
          is24Hour={true}
          display='spinner'
          mode='date'
          onChange={this.onChangedate}
          />}
        {this.state.datevisibletime && <DateTimePicker
          value={this.getDate()}
          isVisible={this.state.datevisibletime}
          is24Hour={true}
          display='spinner'
          mode='time'
          onChange={this.onChangetime}
        />}
        {/*testtttttttt*/}
        <View style={[styles.box ,{marginBottom:5}]}>
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t('startenddate.startenddate')}{I18n.t('mainpile.step.7')}</Text>
        </View>
        <View>
        <View style={[styles.imageDetail,{marginLeft:20,marginRight:20,marginBottom:15}]}>
        <Text style={{width:'25%'}}>{I18n.t('startenddate.start')}</Text>
          <TouchableOpacity
            style={[styles.buttonBox, { width: "35%" }]}
            onPress={() => {
              this.setState({datevisibledate:true,type:'start'})
            }}
            disabled={this.state.Edit_Flag == 0}
          >
            <Text>{this.state.datestart}</Text>
          </TouchableOpacity>
          <Text style={{width:'10%',textAlign:'center'}}>{I18n.t('startenddate.time')}</Text>
          <TouchableOpacity
            style={[styles.buttonBox, { width: "30%" }]}
            onPress={() => {
              this.setState({datevisibletime:true,type:'start'})
            }}
            disabled={this.state.Edit_Flag == 0}
          >
            <Text>{this.state.timestart}</Text>
          </TouchableOpacity>
          {/*<TouchableOpacity
            style={styles.approveBox}
            onPress={() => this.onnowdate('start')}
            disabled={this.state.Edit_Flag == 0}
          >
            <Text style={{color:'#fff'}}>เริ่ม</Text>
          </TouchableOpacity>*/}
        </View>
        <View style={[styles.imageDetail,{marginLeft:20,marginRight:20,marginBottom:15}]}>
        <Text style={{width:'25%'}}>{I18n.t('startenddate.end')}</Text>
        <TouchableOpacity
            style={[styles.buttonBox, { width: "35%" }]}
            onPress={() => {
              this.setState({datevisibledate:true,type:'end'})
            }}
            disabled={this.state.Edit_Flag == 0}
          >
            <Text>{this.state.dateend}</Text>
          </TouchableOpacity>
          <Text style={{width:'10%',textAlign:'center'}}>{I18n.t('startenddate.time')}</Text>
          <TouchableOpacity
            style={[styles.buttonBox, { width: "30%" }]}
            onPress={() => {
              this.setState({datevisibletime:true,type:'end'})
            }}
            disabled={this.state.Edit_Flag == 0}
          >
            <Text>{this.state.timeend}</Text>
          </TouchableOpacity>
          {/*<TouchableOpacity
            style={styles.approveBox}
            onPress={() => this.onnowdate('end')}
            disabled={this.state.Edit_Flag == 0}
          >
            <Text style={{color:'#fff'}}>สิ้นสุด</Text>
          </TouchableOpacity>*/}
        </View>
      </View>
      </View>
      {/*testtttttttt*/}
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(Pile7_3)


class Table extends Component {
  render() {
    return (
      <View style={{ flexDirection: "row", height: 70, borderTopWidth: 0.3, borderBottomWidth: 0.2 }}>
        <View style={{ width: "35%", borderRightWidth: 0.2, justifyContent: "center" }}>
        {this.props.shape != 2?<Text style={{textAlign:'center'}}>{this.props.name}</Text>
          :
          this.props.se == '' || this.props.se == null?<Text style={{textAlign:'center'}}>{this.props.name}</Text>
          :
          <Text style={{textAlign:'center'}}>{this.props.name} ({this.props.se})</Text>
          }
            {this.props.lastitem&&<Text style={{textAlign:'center',color:"#6dcc64"}}>{I18n.t("mainpile.7_3.firstitem")}</Text>}
            {this.props.firstitem&&<Text style={{textAlign:'center',color:"#6dcc64"}}>{I18n.t("mainpile.7_3.lastitem")}</Text>}
        </View>
        <View style={{ width: "25%", borderRightWidth: 0.2, alignItems: "center" }}>
          <Text style={{ fontSize: 12, marginTop: 2 }}>{I18n.t("mainpile.7_3.steelcage")}</Text>
          <View style={{ flexDirection: "row" }}>
            {this.props.checksteelcage ? (
              <Icon name="check" reverse color="#6dcc64" size={15} />
            ) : (
              <Icon name="check" reverse color="grey" size={15} />
            )}
            <TouchableOpacity style={styles.button} onPress={() => this.props.PhotoPress_steelcage()}>
              {this.props.image_steelcage && this.props.image_steelcage.length > 0 ? (
                <Icon name="image" type="font-awesome" reverse color="#6dcc64" size={15} />
              ) : (
                <Icon name="image" type="font-awesome" reverse color="grey" size={15} />
              )}
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={[
            { width: "25%", borderRightWidth: 0.2, alignItems: "center" },
            this.props.concretedisabled ? { backgroundColor: "#CDD2D2" } : {}
          ]}
        >
          <Text style={{ fontSize: 12, marginTop: 2 }}>{I18n.t("mainpile.7_3.concrete")}</Text>
          <View style={{ flexDirection: "row" }}>
            {this.props.checkconcretespacer ? (
              <Icon name="check" reverse color="#6dcc64" size={15} />
            ) : (
              <Icon name="check" reverse color="grey" size={15} />
            )}
            <TouchableOpacity
              style={styles.button}
              onPress={() => this.props.PhotoPress_concretespacer()}
              // disabled={this.props.concretedisabled}
            >
              {this.props.image_concretespacer && this.props.image_concretespacer.length > 0 ? (
                <Icon name="image" type="font-awesome" reverse color="#6dcc64" size={15} />
              ) : (
                <Icon name="image" type="font-awesome" reverse color="grey" size={15} />
              )}
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ width: "15%", alignItems: "center", justifyContent: "center" }}>
          <Icon
            name="file-text"
            type="font-awesome"
            reverse
            color="#6bacd5"
            size={20}
            onPress={() => this.props.iconPressed()}
            iconPressed={this.props.iconPressed}
          />
        </View>
        
      </View>
    )
  }
}
