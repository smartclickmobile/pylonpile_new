import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity
} from "react-native"
import { connect } from "react-redux"
import { Container, Content } from "native-base"
import StepIndicator from "react-native-step-indicator"
import * as actions from "../Actions"
import Pile11_1 from "./Pile11_1"
import Pile11_2 from "./Pile11_2"
import Pile11_3 from "./Pile11_3"
import styles from "./styles/Pile8.style"
import I18n from "../../assets/languages/i18n"
import { Actions } from "react-native-router-flux"
import {
  MAIN_COLOR,
  DEFAULT_COLOR_1,
  DEFAULT_COLOR_4
} from "../Constants/Color"
import moment from "moment"

class Pile11 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      step: 0,
      process: 11,
      Edit_Flag: 1,
      error: null,
      data: [],
      location: {
        position: {
          lat: 1,
          log: 1
        }
      },
      startdate: "",
      onPress: false,
      random11: null,
      Step11Status: 0,
      stepstatus: null
    }
  }
  componentDidMount() {
    // console.log(this.props.stack)
    if (this.props.stack) {
      this.setState({
        step: this.props.stack[this.props.stack.length - 1].step - 1
      })
    }
  }
  async componentWillReceiveProps(nextProps) {
    if (
      nextProps.step_status2_random != this.state.stepstatus &&
      nextProps.processstatus == 11
    ) {
      this.setState({ stepstatus: nextProps.step_status2_random }, async () => {
        // console.warn('yes6',nextProps.step_status2.Step2Status)
        await this.props.mainPileSetStorage(this.props.pileid, "step_status", {
          Step11Status: nextProps.step_status2.Step11Status
        })
        this.props.mainPileGetStorage(this.props.pileid, "step_status")

        if (this.state.step == 2) {
          if (nextProps.step_status2.Step11Status == 2) {
            if (this.props.category != 4) {
              var value = {
                process: 4,
                step: 1,
                pile_id: this.props.pileid,
                drillingFluids: this.props.drillingfluidslist,
                shape: this.props.shape
              }
              // let check4=true
              this.props
                .mainpileAction_checkStepStatus(value)
                .then(async data => {
                  console.warn("data.check", data.check)
                  if (data.statuscolor != 2) {
                    Alert.alert("", I18n.t("mainpile.11_3.process4notyet"), [
                      {
                        text: "OK"
                      }
                    ])
                  } else {
                    // console.warn("data.ทำงานเสร็จสิ้น", data.statuscolor)
                    Alert.alert(
                      I18n.t("mainpile.10_3.finish"),
                      I18n.t("mainpile.10_3.backtopile"),
                      [
                        { text: "Cancel" },
                        // { text: "OK", onPress: () => Actions.projectlist({title: "", project: this.props.project, jobid: this.props.jobid}) }result: ''
                        {
                          text: "OK",
                          onPress: () => Actions.projectlist({ result: "" })
                        }
                      ]
                    )
                    // this.setState({ step: 3 })
                    // this.onSetStack(11, 2)
                  }
                })
            } else {
              Alert.alert(
                I18n.t("mainpile.10_3.finish"),
                I18n.t("mainpile.10_3.backtopile"),
                [
                  { text: "Cancel" },
                  // { text: "OK", onPress: () => Actions.projectlist({title: "", project: this.props.project, jobid: this.props.jobid}) }result: ''
                  {
                    text: "OK",
                    onPress: () => Actions.projectlist({ result: "" })
                  }
                ]
              )
            }
          }
        }
      })
    }
    if (nextProps.pileAll[this.props.pileid] != undefined) {
      var edit = nextProps.pileAll[this.props.pileid]
      if (edit["11_0"] != undefined) {
        await this.setState({ Edit_Flag: edit["11_0"].data.Edit_Flag })
      }
    }
    if (
      nextProps.pile11 == true &&
      this.state.random11 != nextProps.random11 &&
      this.state.onPress == true
    ) {
      if (nextProps.save11page == 1) {
        // console.warn(nextProps.random11)
        this.setState({ random11: nextProps.random11, step: 1, onPress: false })
        // if(this.state.Step11Status==2){
        //   await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step11Status: 2 })
        // }else{
        //   await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step11Status: 1 })
        // }

        // this.props.mainPileGetStorage(this.props.pileid, "step_status")
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process: 11
        })
        this.onSetStack(11, 0)
      } else if (nextProps.save11page == 2) {
        this.setState({
          save11page: nextProps.random11,
          step: 2,
          onPress: false
        })
        // if(this.state.Step11Status==2){
        //   await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step11Status: 2 })
        // }else{
        //   await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step11Status: 1 })
        // }

        // this.props.mainPileGetStorage(this.props.pileid, "step_status")
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process: 11
        })
        this.onSetStack(11, 1)
      } else if (nextProps.save11page == 3) {
        this.setState({ save11page: nextProps.random11, onPress: false })
        // if(this.state.Step11Status==2){
        //   await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step11Status: 2 })
        // }else{
        //   await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step11Status: 1 })
        // }

        // this.props.mainPileGetStorage(this.props.pileid, "step_status")
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process: 11
        })
      }
    }
    await this.setState({
      error: nextProps.error,
      data: nextProps.pileAll[nextProps.pileid],
      location: nextProps.pileAll.currentLocation
    })
  }

  async onSetStack(pro, step) {
    var data = {
      process: pro,
      step: step + 1
    }
    // console.log("onSetStack_11_3",data)
    await this.props.setStack(data)
    await this.props.mainPileGetAllStorage()
  }

  nextButton = () => {
    switch (this.state.step) {
      case 0:
        this.onSave(this.state.step)
        break
      case 1:
        this.onSave(this.state.step)
        break
      case 2:
        this.onSave(this.state.step)
        break
      default:
        break
    }
  }

  async onSave(step) {
    console.log(step)
    await this.props.mainPileGetAllStorage()
    if (step == 0) {
      var value = {
        process: this.state.process,
        step: 1,
        pile_id: this.props.pileid
      }
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value).then(async data => {
          // console.log("dataStep11 page1", data)

          var valueApi = {
            Language: I18n.currentLocale(),
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            Page: 1,
            MachineId:
              (this.state.data["11_1"].data.Machine &&
                this.state.data["11_1"].data.Machine.itemid) ||
              "",
            MachineName: this.state.data["11_1"].data.MachineName,
            VibroId:
              (this.state.data["11_1"].data.Vibro &&
                this.state.data["11_1"].data.Vibro.itemid) ||
              "",
            VibroName: this.state.data["11_1"].data.VibroName,
            DriverId: this.state.data["11_1"].data.DriverId || "",
            DriverName: this.state.data["11_1"].data.DriverName,
            latitude:
              this.state.location != undefined
                ? this.state.location.position.lat
                : 1,
            longitude:
              this.state.location != undefined
                ? this.state.location.position.log
                : 1
          }
          // this.props.pileSaveStep11(valueApi)

          if (this.state.error == null) {
            if (data.check == false) {
              Alert.alert("", data.errorText[0], [
                {
                  text: "OK"
                }
              ])
            } else {
              this.setState(
                { onPress: true, Step11Status: data.statuscolor },
                async () => {
                  await this.props.pileSaveStep11(valueApi)
                }
              )
              // this.props.pileSaveStep11(valueApi)
              if (
                (this.props.startdate != "") &
                (this.props.startdate != null)
              ) {
                this.setState({
                  startdate: moment(
                    this.props.startdate,
                    "DD/MM/YYYY HH:mm:ss"
                  ).format("DD/MM/YYYY HH:mm")
                })
              } else {
                this.setState({
                  startdate: moment().format("DD/MM/YYYY HH:mm")
                })
              }
            }
          } else {
            Alert.alert(this.state.error)
          }
        })
      }
    } else if (step == 1) {
      var value = {
        process: this.state.process,
        step: 2,
        pile_id: this.props.pileid
      }
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value).then(async data => {
          console.log("dataStep11 page2", data)
          if (data.check != false) {
            if (this.state.data["11_2"].enddate != "") {
              var end = this.state.data["11_2"].enddate
              var valueend = {
                process: 11,
                step: 2,
                pile_id: this.props.pileid,
                data: {
                  CheckPlummet: this.state.data["11_2"].data.CheckPlummet,
                  CheckWaterLevel: this.state.data["11_2"].data.CheckWaterLevel,
                  ImagePlummet: this.state.data["11_2"].data.ImagePlummet,
                  ImageWaterLevel: this.state.data["11_2"].data.ImageWaterLevel
                },
                startdate: this.state.data["11_2"].startdate,
                enddate: end
              }
              await this.props.mainPileSetStorage(
                this.props.pileid,
                "11_2",
                valueend
              )
            } else {
              var end = moment().format("DD/MM/YYYY HH:mm")
              var valueend = {
                process: 11,
                step: 2,
                pile_id: this.props.pileid,
                data: {
                  CheckPlummet: this.state.data["11_2"].data.CheckPlummet,
                  CheckWaterLevel: this.state.data["11_2"].data.CheckWaterLevel,
                  ImagePlummet: this.state.data["11_2"].data.ImagePlummet,
                  ImageWaterLevel: this.state.data["11_2"].data.ImageWaterLevel
                },
                startdate: this.state.data["11_2"].startdate,
                enddate: end
              }
              await this.props.mainPileSetStorage(
                this.props.pileid,
                "11_2",
                valueend
              )
            }
            var start = moment(
              this.state.data["11_2"].startdate,
              "DD/MM/YYYY HH:mm"
            )
            var end1 = moment(end, "DD/MM/YYYY HH:mm")
            if (this.state.data["11_2"].startdate != undefined) {
              if (start <= end1) {
                var valueApi = {
                  Language: I18n.currentLocale(),
                  JobId: this.props.jobid,
                  PileId: this.props.pileid,
                  Page: 2,
                  latitude:
                    this.state.location != undefined
                      ? this.state.location.position.lat
                      : 1,
                  longitude:
                    this.state.location != undefined
                      ? this.state.location.position.log
                      : 1,
                  CheckPlummet: this.state.data["11_2"].data.CheckPlummet,
                  CheckWaterLevel: this.state.data["11_2"].data.CheckWaterLevel,
                  ImagePlummet: this.state.data["11_2"].data.ImagePlummet,
                  ImageWaterLevel: this.state.data["11_2"].data.ImageWaterLevel,
                  startdate: moment(
                    this.state.data["11_2"].startdate,
                    "DD/MM/YYYY HH:mm"
                  ).format("YYYY-MM-DD HH:mm:ss"),
                  enddate: moment(end, "DD/MM/YYYY HH:mm").format(
                    "YYYY-MM-DD HH:mm:ss"
                  )
                }
                console.log(
                  "valueApi update11",
                  this.state.data["11_2"].data.ImagePlummet
                )
                // this.props.pileSaveStep11(valueApi)

                if (this.state.error == null) {
                  if (data.check == false) {
                    Alert.alert("", data.errorText[0], [
                      {
                        text: "OK"
                      }
                    ])
                  } else {
                    this.setState(
                      {
                        onPress: true,
                        Step11Status: data.statuscolor,
                        step: 2,
                        startdate: moment().format("DD/MM/YYYY HH:mm:ss")
                      },
                      async () => {
                        await this.props.pileSaveStep11(valueApi)
                      }
                    )
                  }
                } else {
                  Alert.alert(this.state.error)
                }
              } else {
                Alert.alert(
                  "",
                  I18n.t("mainpile.liquidtestinsert.alert.error4"),
                  [
                    {
                      text: "OK"
                    }
                  ]
                )
              }
            } else {
              if (this.props.startdate <= this.props.enddate) {
                var valueApi = {
                  Language: I18n.currentLocale(),
                  JobId: this.props.jobid,
                  PileId: this.props.pileid,
                  Page: 2,
                  latitude:
                    this.state.location != undefined
                      ? this.state.location.position.lat
                      : 1,
                  longitude:
                    this.state.location != undefined
                      ? this.state.location.position.log
                      : 1,
                  CheckPlummet: this.state.data["11_2"].data.CheckPlummet,
                  CheckWaterLevel: this.state.data["11_2"].data.CheckWaterLevel,
                  ImagePlummet: this.state.data["11_2"].data.ImagePlummet,
                  ImageWaterLevel: this.state.data["11_2"].data.ImageWaterLevel,
                  startdate: moment(
                    this.props.startdate,
                    "DD/MM/YYYY HH:mm:ss"
                  ).format("DD-MM-YYYY HH:mm:ss"),
                  enddate: moment(
                    this.props.enddate,
                    "DD/MM/YYYY HH:mm:ss"
                  ).format("DD-MM-YYYY HH:mm:ss")
                }
                console.log(
                  "valueApi update11",
                  this.state.data["11_2"].data.ImagePlummet
                )
                // this.props.pileSaveStep11(valueApi)

                if (this.state.error == null) {
                  if (data.check == false) {
                    Alert.alert("", data.errorText[0], [
                      {
                        text: "OK"
                      }
                    ])
                  } else {
                    this.setState(
                      {
                        onPress: true,
                        Step11Status: data.statuscolor,
                        step: 2,
                        startdate: moment().format("DD/MM/YYYY HH:mm:ss")
                      },
                      async () => {
                        await this.props.pileSaveStep11(valueApi)
                      }
                    )
                  }
                } else {
                  Alert.alert(this.state.error)
                }
              } else {
                Alert.alert("", data.errorText[0], [
                  {
                    text: "OK"
                  }
                ])
              }
            }
          } else {
            Alert.alert("", data.errorText[0], [
              {
                text: "OK"
              }
            ])
          }
        })
      }
    } else if (step == 2) {
      var value = {
        process: this.state.process,
        step: 3,
        pile_id: this.props.pileid
      }
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value).then(async data => {
          console.log("dataStep11 page3", data)
          // await this.props.mainPileSetStorage(
          //   this.props.pileid,
          //   "step_status",
          //   { Step11Status: data.statuscolor }
          // )
          // this.props.mainPileGetStorage(this.props.pileid, "step_status")

          var valueApi = {
            Language: I18n.currentLocale(),
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            Page: 3,
            latitude:
              this.state.location != undefined
                ? this.state.location.position.lat
                : 1,
            longitude:
              this.state.location != undefined
                ? this.state.location.position.log
                : 1,
            ConcreteLevelBeforeWithdraw: this.state.data["11_3"].data
              .ConcreteLevelBeforeWithdraw,
            ConcreteLevelAfterWithdraw: this.state.data["11_3"].data
              .ConcreteLevelAfterWithdraw,
            ConcreteBleeding: this.state.data["11_3"].data.ConcreteBleeding
          }
          console.log(
            "this.props.pileAll[this.props.pileid]",
            this.state.data["4_0"].data
          )

          if (this.state.error == null) {
            if (data.check == false) {
              Alert.alert("", data.errorText[0], [
                {
                  text: "OK"
                }
              ])
            } else {
              this.setState(
                {
                  onPress: true,
                  Step11Status: data.statuscolor,
                  step: 2,
                  startdate: moment().format("DD/MM/YYYY HH:mm:ss")
                },
                async () => {
                  await this.props.pileSaveStep11(valueApi)
                }
              )
            }
          } else {
            Alert.alert(this.state.error)
          }
        })
      }
    }
  }

  deleteButton = () => {
    this.props.setStack({
      process: 11,
      step: 1
    })
    const data = {
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      processid: this.state.process,
      lat:
        this.state.location != undefined ? this.state.location.position.lat : 1,
      log:
        this.state.location != undefined ? this.state.location.position.log : 1
    }
    Alert.alert("Warning", I18n.t("mainpile.button.confirmdelete"), [
      { text: "Cancel" },
      { text: "OK", onPress: () => this.props.pileDelete(data) }
    ])
  }

  _renderStepFooter() {
    return (
      <View>
        <View style={{ flexDirection: "column" }}>
          <View style={{ borderWidth: 1 }}>
            <StepIndicator
              stepCount={3}
              onPress={step => {
                this.setState({ step: step })
                this.onSetStack(11, step)
              }}
              currentPosition={this.state.step}
            />
          </View>
          {this.state.Edit_Flag == 1 ? (
            <View style={styles.second}>
              <TouchableOpacity
                style={styles.firstButton}
                onPress={this.deleteButton}
              >
                <Text style={styles.selectButton}>
                  {I18n.t("mainpile.button.delete")}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.secondButton,
                  this.props.category == 3 || this.props.category == 5
                    ? { backgroundColor: DEFAULT_COLOR_4 }
                    : this.props.shape == 1
                    ? { backgroundColor: MAIN_COLOR }
                    : { backgroundColor: DEFAULT_COLOR_1 }
                ]}
                onPress={this.nextButton}
              >
                <Text style={styles.selectButton}>
                  {I18n.t("mainpile.button.next")}
                </Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View />
          )}
        </View>
      </View>
    )
  }

  updateState(value) {
    const ref = "pile" + value.process + "_" + value.step
    if (this.refs[ref].getWrappedInstance()) {
      this.refs[ref].getWrappedInstance().updateState(value)
    }
  }
  initialStep() {
    this.setState({ step: 0 })
  }

  render() {
    if (this.props.hidden) {
      return null
    }

    return (
      <View style={{ flex: 1 }}>
        <Content>
          <Pile11_1
            ref="pile11_1"
            hidden={!(this.state.step == 0)}
            jobid={this.props.jobid}
            pileid={this.props.pileid}
            shape={this.props.shape}
            onRefresh={this.props.onRefresh}
          />
          <Pile11_2
            ref="pile11_2"
            hidden={!(this.state.step == 1)}
            jobid={this.props.jobid}
            pileid={this.props.pileid}
            shape={this.props.shape}
            category={this.props.category}
            startdatedefault={this.state.startdate}
            startdate={this.props.startdate}
            enddate={this.props.enddate}
          />
          <Pile11_3
            ref="pile11_3"
            hidden={!(this.state.step == 2)}
            jobid={this.props.jobid}
            pileid={this.props.pileid}
            shape={this.props.shape}
          />
        </Content>
        {this._renderStepFooter()}
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    error: state.pile.error,
    pileAll: state.mainpile.pileAll || null,
    stack: state.mainpile.stack_item,
    drillingfluidslist: state.drillingFluids.drillingfluidslist,
    random11: state.pile.random11,
    pile11: state.pile.pile11,
    save11page: state.pile.save11page,

    step_status2_random: state.mainpile.step_status2_random,
    step_status2: state.mainpile.step_status2,
    processstatus: state.mainpile.process
  }
}

export default connect(
  mapStateToProps,
  actions,
  null,
  { withRef: true }
)(Pile11)
