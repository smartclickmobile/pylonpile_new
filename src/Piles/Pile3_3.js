import React, { Component } from "react"
import { View, Text, StyleSheet, TouchableOpacity, TextInput, Alert } from "react-native"
import { MAIN_COLOR, SUB_COLOR,MENU_GREY_ITEM } from "../Constants/Color"
import ModalSelector from "react-native-modal-selector"
import { Icon } from "react-native-elements"
import I18n from "../../assets/languages/i18n"
import { Extra } from "../Controller/API"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"
import styles from './styles/Pile3_3.style'
import moment from "moment"
import DateTimePicker from '@react-native-community/datetimepicker';
class Pile3_3 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      top: "",
      ground: "",
      process:3,
      step:3,
      Edit_Flag:1,
      datevisibletime:false,
      datevisibledate:false,
      startdate:'',
      enddate:'',
      type:'',
      datestart:'',
      timestart:'',
      dateend:'',
      timeend:'',
      set:false,
      ApprovedDate:'',
      editground:false,
      status11:0,
      status3:0,
      status4:0,
      status5:0,
      statusAll:[],
      top1:false,
      groundold:''
    }
  }

  async componentWillMount() {
    this.setState({
      datestart: moment(this.props.startdate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY"),
      dateend: moment().format("DD/MM/YYYY"),
      timeend: moment().format("HH:mm:ss"),
      timestart: moment(this.props.startdate,"DD/MM/YYYY HH:mm:ss").format("HH:mm:ss"),
    })
  }
  componentDidMount(){
    console.log('net',)
  }

  async componentWillReceiveProps(nextProps){
    var pile = null
    var pile3_0 = null
    var pile3_4 = null
    if (nextProps.pileAll[nextProps.pileid] != undefined) {
      var edit = nextProps.pileAll[nextProps.pileid]
      if(edit != undefined){
        this.setState({
          status11:edit.step_status.Step11Status,
          status3:edit.step_status.Step3Status,
          status4:edit.step_status.Step4Status,
          statusAll:edit.step_status,
          status5:edit.step_status.Step5Status
        })
      }
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['3_3']) {
      pile = nextProps.pileAll[this.props.pileid]['3_3'].data
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['3_0']) {
      pile3_0 = nextProps.pileAll[this.props.pileid]['3_0'].data
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["3_4"]){
      pile3_4 = nextProps.pileAll[this.props.pileid]["3_4"].data
    }
    if(pile3_4){
      if(pile3_4.ApprovedDate != null && pile3_4.ApprovedDate != undefined && pile3_4.ApprovedDate != ''){
        this.setState({ApprovedDate:pile3_4.ApprovedDate})
      }
    }
    if (pile) {
      if(pile.top != null && pile.top != undefined && this.state.top == ''){
        await this.setState({top:isNaN(parseFloat(pile.top))?'': (parseFloat(pile.top).toFixed(3)).toString()})
      }else if (pile.top == null) {
        this.setState({top:""})
      }
      if(pile.ground != null && pile.ground != undefined && this.state.ground == ''){
        await this.setState({ground:isNaN(parseFloat(pile.ground))?'':(parseFloat(pile.ground).toFixed(3)).toString(),groundold:pile.ground.toString()})
      }else if (pile.ground == null) {
        this.setState({ground:""})
      }
      if(!this.state.set){
        if(this.props.startdatedefault != '' && this.props.startdatedefault != null){
          if(this.props.enddate != '' && this.props.enddate != null){
            this.setState({
              datestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
              timestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("HH:mm"),
              dateend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
              timeend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("HH:mm"),
              startdate:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
              enddate:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm")
            })
          }else{
            this.setState({
              datestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
              timestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("HH:mm"),
              dateend:'',
              timeend:'',
              startdate:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
              enddate:''
            })
          }
        }else{
          if(this.props.startdate != '' && this.props.startdate != null){
            this.setState({
              datestart:moment(this.props.startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
              timestart:moment(this.props.startdate,"DD/MM/YYYY HH:mm").format("HH:mm"),
              startdate:moment(this.props.startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
            })
          }else{
            this.setState({
              datestart:'',
              timestart:''
            })
          }
          if(this.props.enddate != '' && this.props.enddate != null){
            this.setState({
              dateend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
              timeend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("HH:mm"),
              enddate:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm")
            })
          }else{
            this.setState({
              dateend:'',
              timeend:''
            })
          }
        }
        
      }
      if(nextProps.pileAll[this.props.pileid]['3_3'].startdate != null && nextProps.pileAll[this.props.pileid]['3_3'].startdate != ''){
        this.setState({
          datestart:moment(nextProps.pileAll[this.props.pileid]['3_3'].startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
          timestart:moment(nextProps.pileAll[this.props.pileid]['3_3'].startdate,"DD/MM/YYYY HH:mm").format("HH:mm"),
          startdate:moment(nextProps.pileAll[this.props.pileid]['3_3'].startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
        })
      }
      if(nextProps.pileAll[this.props.pileid]['3_3'].enddate != null && nextProps.pileAll[this.props.pileid]['3_3'].enddate != ''){
        this.setState({
          dateend:moment(nextProps.pileAll[this.props.pileid]['3_3'].enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
          timeend:moment(nextProps.pileAll[this.props.pileid]['3_3'].enddate,"DD/MM/YYYY HH:mm").format("HH:mm"),
          enddate:moment(nextProps.pileAll[this.props.pileid]['3_3'].enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
        })
      }
      
    }

    if (pile3_0) {
      // if (pile3_0.Edit_Flag != null && pile3_0.Edit_Flag != undefined && pile3_0.Edit_Flag != '') {
        this.setState({Edit_Flag:pile3_0.Edit_Flag})
      // }
    }
  }

  getState(){
    var data = {
      top: this.state.top,
      ground: this.state.ground,
    }
    return data
  }
  async mixdate(){
    if( this.state.datestart != '' && this.state.timestart != ''){
     await this.setState({startdate:this.state.datestart+' '+this.state.timestart})
    }
    if(this.state.dateend != '' && this.state.timeend != ''){
      await this.setState({enddate:this.state.dateend+' '+this.state.timeend})
    }
  }
  async saveLocal(process_step){
    // console.warn('editground',this.state.ground)
    await this.mixdate()
    var value = {
    process:3,
    step:3,
    pile_id: this.props.pileid,
    data:{
      top:this.state.top,
      ground:this.state.ground,
      },
    startdate: this.state.startdate,
    enddate: this.state.enddate,
    editground:this.state.editground
    }
    await this.props.mainPileSetStorage(this.props.pileid,process_step,value)
  }
  async selectedGround(ground){
    if(this.state.status11 != 0){
      await this.setState({ ground: this.onChangeFormatDecimal(ground), editground:true})
    }else{
      await this.setState({ ground: this.onChangeFormatDecimal(ground)})
    }
    
  }
  async selectedTop(top) {
    await this.setState({ top: this.onChangeFormatDecimal(top)})
  }

  onChangeFormatDecimal(data){
    if (data.split(".")[1]) {
      if (data.split(".")[1].length > 3) {
        return parseFloat(data).toFixed(3)
      }else {
        return data
      }
    }else {
      return data
    }
  }
  onSelectDate(date){
    let date2 = moment(date).format("DD/MM/YYYY")
    if(this.state.type=='start'){
      this.setState({ datestart: date2 ,set:true,datevisibledate: false})
      this.saveLocal('3_3')
    }else if(this.state.type=='end'){
      this.setState({ dateend: date2 ,set:true,datevisibledate: false})
      this.saveLocal('3_3')
    }
  }
  onSelectTime(date){
    let date2 = moment(date).format("HH:mm")
    if(this.state.type=='start'){
      this.setState({ timestart: date2 ,set:true,datevisibletime: false})
      this.saveLocal('3_3')
    }else if(this.state.type=='end'){
      this.setState({ timeend: date2 ,set:true,datevisibletime: false})
      this.saveLocal('3_3')
    }
  }
  getDate(){
    if(this.state.type == 'start'){
      if(this.state.startdate != ''){
        return new Date(moment(this.state.startdate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        if(this.props.startdate != '' && this.props.startdate != undefined && this.props.startdate != null){
          return new Date(moment(this.props.startdate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
        }else{
          return new Date()
        }
      }
    }else{
      if(this.state.enddate != ''){
        return new Date(moment(this.state.enddate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        if(this.props.enddate != '' && this.props.enddate != undefined && this.props.enddate != null){
          return new Date(moment(this.props.enddate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
        }else{
          return new Date()
        }
      }
    }
  }

  edittop(){
    // console.warn('edittop')
    var temp = ''
    if(I18n.locale=='th'){
      temp = '"'
    }
    
    if( this.state.status5 !=0){
      var index = ''
      if(this.state.statusAll.Step11Status != 0){
        Alert.alert('', I18n.t('mainpile.3_3.warn1')+I18n.t('mainpile.step.11')+temp+I18n.t('mainpile.3_3.warn2'), [
          {text: 'OK'}
        ])
      }else if(this.state.statusAll.Step10Status != 0){
        Alert.alert('', I18n.t('mainpile.3_3.warn1')+I18n.t('mainpile.step.10')+temp+I18n.t('mainpile.3_3.warn2'), [
          {text: 'OK'}
        ])
      }else if(this.state.statusAll.Step8Status != 0){
        Alert.alert('', I18n.t('mainpile.3_3.warn1')+I18n.t('mainpile.step.8')+temp+I18n.t('mainpile.3_3.warn2'), [
          {text: 'OK'}
        ])
      }else if(this.state.statusAll.Step7Status != 0){
        Alert.alert('', I18n.t('mainpile.3_3.warn1')+I18n.t('mainpile.step.7')+temp+I18n.t('mainpile.3_3.warn2'), [
          {text: 'OK'}
        ])
      }else if(this.state.statusAll.Step6Status != 0){
        Alert.alert('', I18n.t('mainpile.3_3.warn1')+I18n.t('mainpile.step.6')+temp+I18n.t('mainpile.3_3.warn2'), [
          {text: 'OK'}
        ])
      }else if(this.state.statusAll.Step5Status != 0){
        Alert.alert('', I18n.t('mainpile.3_3.warn4')+I18n.t('mainpile.step.5')+temp+I18n.t('mainpile.3_3.warn5dw'), [
          {text: 'OK'}
        ])
      }
      
      this.setState({top1:true})
      // Alert.alert('', I18n.t('mainpile.3_3.warn1')+index+I18n.t('mainpile.3_3.warn2'), [
      //   { text: "Cancel" },
      //   {
      //     text: 'OK',
      //   }
      //   ])
    }else{
      // console.warn('else')
      this.setState({top:this.state.top ? parseFloat(this.state.top).toFixed(3) : ''})
      this.saveLocal('3_3')
    }
    
     
  }
  editgroundfunc(){
    if(this.state.status5 != 0 ){
      // console.warn('check',parseFloat(this.state.ground).toFixed(4) , this.state.groundold)
      if(parseFloat(this.state.ground).toFixed(3) != parseFloat(this.state.groundold).toFixed(3)){
        Alert.alert('', I18n.t('mainpile.3_3.warn3'), [
          { 
            text: "Cancel",
            onPress: ()=>{
              this.setState({ground:this.state.groundold ? parseFloat(this.state.groundold).toFixed(3) : ''})
              this.saveLocal('3_3')
            }
          },
          {
            text: 'OK',
            onPress: ()=>{ 
              this.setState({ground:this.state.ground ? parseFloat(this.state.ground).toFixed(3) : ''})
              this.saveLocal('3_3')
            }
          }
          ])
      }else{
        this.setState({ground:this.state.ground ? parseFloat(this.state.ground).toFixed(3) : ''})
        this.saveLocal('3_3')
      }
    }else{
      this.setState({ground:this.state.ground ? parseFloat(this.state.ground).toFixed(3) : ''})
      this.saveLocal('3_3')
    }
  }

  onChangedate = (event, selectedDate) => {
    if(event.type == 'set'){
      this.onSelectDate(selectedDate)
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };
  onChangetime = (event, selectedDate) => {
    if(event.type == 'set'){
      this.onSelectTime(selectedDate)
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };
  render() {
    if (this.props.hidden) {
      return null
    }
    // console.warn('top',this.state.top1)
    return (
      <View>
        <View style={styles.topic}>
          <Text style={styles.topicText}>{this.props.shape == 1 ? I18n.t("mainpile.3_3.position_bp") : I18n.t("mainpile.3_3.position_dw") }</Text>
        </View>

        <View>
          <View style={styles.boxTopic}>
            <Text>{this.props.shape == 1 ? I18n.t("mainpile.3_3.top") : I18n.t("mainpile.3_3.top_dwall") }</Text>
          </View>
          {
            // this.state.Edit_Flag == 1 && this.state.ApprovedDate == ''?
            this.state.Edit_Flag == 1 && this.props.waitApprove == false?
            <View style={styles.selectedView}>
              <View style={styles.button}>
                {this.state.status5 !=0?
                  <TouchableOpacity onPress={()=>this.edittop()} style={{width:'100%'}}>
                    <TextInput
                    editable={false}
                    keyboardType="numeric"
                    onChangeText={(top) => this.state.top1?this.state.top:this.selectedTop(top)}
                    underlineColorAndroid="transparent"
                    value={this.state.top}
                    style={styles.buttonText}
                    placeholder={this.props.shape == 1 ? I18n.t("mainpile.3_3.top") : I18n.t("mainpile.3_3.top_dwall") }
                    // onFocus={()=> this.edittop()}
                    // onEndEditing={()=> {
                    //   this.edittop()
                    // }}
                  />
                  </TouchableOpacity>
                  :
                  <TextInput
                  editable={true}
                  keyboardType="numeric"
                  onChangeText={(top) => this.state.top1?this.state.top:this.selectedTop(top)}
                  underlineColorAndroid="transparent"
                  value={this.state.top}
                  style={styles.buttonText}
                  placeholder={this.props.shape == 1 ? I18n.t("mainpile.3_3.top") : I18n.t("mainpile.3_3.top_dwall") }
                  onFocus={()=> this.edittop()}
                  onEndEditing={()=> {
                    this.edittop()
                  }}
                />}
              </View>
            </View>
            :
            <View style={styles.selectedView}>
              <TouchableOpacity disabled={true} style={[styles.button, { borderColor: MENU_GREY_ITEM }]}>
                <View style={styles.buttonTextStyle}>
                  <Text style={[styles.buttonText ,{ color: MENU_GREY_ITEM }]}>
                    {this.state.top ? isNaN(this.state.top)  ? '' : parseFloat(this.state.top).toFixed(3) :''}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          }
        </View>

        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.3_3.ground")}</Text>
          </View>
            {
              // this.state.Edit_Flag == 1 && this.state.ApprovedDate == '' ?
              this.state.Edit_Flag == 1 && this.props.waitApprove == false?
              <View style={styles.selectedView}>
                <View style={styles.button}>
                  <TextInput
                    editable={true}
                    keyboardType="numeric"
                    onChangeText={(ground) => this.selectedGround(ground)}
                    underlineColorAndroid="transparent"
                    value={this.state.ground}
                    style={styles.buttonText}
                    placeholder={I18n.t("mainpile.3_3.ground")}
                    onEndEditing={()=> this.editgroundfunc()}
                  />
                </View>
              </View>
              :
              <View style={styles.selectedView}>
                <TouchableOpacity disabled={true} style={[styles.button, { borderColor: MENU_GREY_ITEM }]}>
                  <View style={styles.buttonTextStyle}>
                    <Text style={[styles.buttonText ,{ color: MENU_GREY_ITEM }]}>
                      {this.state.ground ? isNaN(this.state.ground)  ? '' : parseFloat(this.state.ground).toFixed(3) :''}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            }
          </View>
          {this.state.datevisibledate &&<DateTimePicker
          value={this.getDate()}
          isVisible={this.state.datevisibledate}
          is24Hour={true}
          display='spinner'
          mode='date'
          onChange={this.onChangedate}
          />}
          {this.state.datevisibletime && <DateTimePicker
            value={this.getDate()}
            isVisible={this.state.datevisibletime}
            is24Hour={true}
            display='spinner'
            mode='time'
            onChange={this.onChangetime}
          />}
          <View>
            <View style={styles.topic}>
              <Text style={[styles.topicText]}>{I18n.t('startenddate.startenddate')}{I18n.t('mainpile.step.3')}</Text>
            </View>
            <View style={[styles.imageDetail,{marginLeft:20,marginRight:20,marginBottom:15}]}>
            <Text style={{width:'25%'}}>{I18n.t('startenddate.start')}</Text>
            <TouchableOpacity
              style={[styles.buttonBox, { width: "35%" }]}
              onPress={() => {
                this.setState({datevisibledate:true,type:'start'})
              }}
              disabled={this.state.Edit_Flag == 0 || this.props.waitApprove}
            >
              <Text>{this.state.datestart}</Text>
            </TouchableOpacity>
            <Text style={{width:'10%',textAlign:'center'}}>{I18n.t('startenddate.time')}</Text>
            <TouchableOpacity
              style={[styles.buttonBox, { width: "30%" }]}
              onPress={() => {
                this.setState({datevisibletime:true,type:'start'})
              }}
              disabled={this.state.Edit_Flag == 0 || this.props.waitApprove}
            >
              <Text>{this.state.timestart}</Text>
            </TouchableOpacity>
            {/*<TouchableOpacity
              style={styles.approveBox}
              onPress={() => this.onnowdate('start')}
              disabled={this.state.Edit_Flag == 0}
            >
              <Text style={{color:'#fff'}}>เริ่ม</Text>
            </TouchableOpacity>*/}
          </View>
          <View style={[styles.imageDetail,{marginLeft:20,marginRight:20,marginBottom:15}]}>
          <Text style={{width:'25%'}}>{I18n.t('startenddate.end')}</Text>
          <TouchableOpacity
              style={[styles.buttonBox, { width: "35%" }]}
              onPress={() => {
                this.setState({datevisibledate:true,type:'end'})
              }}
              disabled={this.state.Edit_Flag == 0 || this.props.waitApprove}
            >
              <Text>{this.state.dateend}</Text>
            </TouchableOpacity>
            <Text style={{width:'10%',textAlign:'center'}}>{I18n.t('startenddate.time')}</Text>
            <TouchableOpacity
              style={[styles.buttonBox, { width: "30%" }]}
              onPress={() => {
                this.setState({datevisibletime:true,type:'end'})
              }}
              disabled={this.state.Edit_Flag == 0 || this.props.waitApprove}
            >
              <Text>{this.state.timeend}</Text>
            </TouchableOpacity>
            {/*<TouchableOpacity
              style={styles.approveBox}
              onPress={() => this.onnowdate('end')}
              disabled={this.state.Edit_Flag == 0}
            >
              <Text style={{color:'#fff'}}>สิ้นสุด</Text>
            </TouchableOpacity>*/}
          </View>
          </View>
        </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll || null,
  }
}

export default connect(mapStateToProps, actions, null, { withRef: true })(Pile3_3)
