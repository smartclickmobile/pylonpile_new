import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity
} from "react-native"
import { Container, Content } from "native-base"
import { Icon } from "react-native-elements"
import Carousel from "react-native-snap-carousel"
import ModalSelector from "react-native-modal-selector"
import { MAIN_COLOR, SUB_COLOR, BASIC_COLOR, MENU_GREY_ITEM, BUTTON_COLOR, BLUE_COLOR } from "../Constants/Color"
import { GroupEmployee } from "../Controller/API"
import I18n from "../../assets/languages/i18n"
import { Extra } from "../Controller/API"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"
import styles from './styles/Pile6_2.style'
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from "moment"
import RNTooltips from "react-native-tooltips"
class Pile6_2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 6,
      step: 2,
      Edit_Flag: 1,
      BucketChanged: false,
      BucketSize:'',
      WaitingTimeMinute: 30,
      BeforeDepth: '',
      AfterDepth: '',
      ImageFiles: [],
      Depth:'',
      DepthLast:'',
      datevisibletime:false,
      datevisibledate:false,
      startdate:'',
      enddate:'',
      type:'',
      datestart:'',
      timestart:'',
      dateend:'',
      timeend:'',
      visible: false,
      visible2: false, 
      set:false,
      CrossFlag:false,
      visible3:false,
      status8:0,
      status10:0,
      CrossFlagold:false,
      status6:0,
      check:false,
      checkcheck:false,

      WaitingTimeMinute_old: 30,
      BeforeDepth_old: '',
      AfterDepth_old: '',
    }
  }

  componentWillMount() {
   
  }

  async componentWillReceiveProps(nextProps) {
    // console.warn('AfterDepth6_2 ',nextProps.pileAll[this.props.pileid]["6_2"].data.AfterDepth)
    var pile = null
    var pile5_3 = null
    var pile5_5 = null
    var pile6_0 = null
    var pileMaster = null
    var pileMaster5_2 = null
    var pile6_1 = null
    var pile3_5 = null
    this.setState({visible2:false,visible:false,visible3:false})
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["6_2"]) {
       pile = nextProps.pileAll[this.props.pileid]["6_2"].data
       pileMaster = nextProps.pileAll[this.props.pileid]['6_2'].masterInfo
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['6_0']) {
      pile6_0 = nextProps.pileAll[this.props.pileid]['6_0'].data
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['6_1']) {
      pile6_1 = nextProps.pileAll[this.props.pileid]['6_1'].data
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['8_0']){
      this.setState({status8:nextProps.pileAll[this.props.pileid].step_status.Step8Status,status10:nextProps.pileAll[this.props.pileid].step_status.Step10Status,status6:nextProps.pileAll[this.props.pileid].step_status.Step6Status})
    }
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["5_2"]) {
      pileMaster5_2 = nextProps.pileAll[this.props.pileid]['5_2'].masterInfo

      if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['3_3'] && nextProps.pileAll[this.props.pileid]['3_5']) {
        pile3_3 = nextProps.pileAll[this.props.pileid]['3_3'].data
        pile3_5 = nextProps.pileAll[this.props.pileid]['3_5'].data
        if (pile3_3) {
          
          if (pile3_3.top) {
            if(this.props.category == 5){
              console.warn('pile3_3.top',pile3_3.top,pileMaster5_2.PileInfo.PileTipChild)
              let cal = pile3_3.top - pileMaster5_2.PileInfo.PileTipChild
              await this.setState({Depth: cal.toString()})
            }else{
              let cal = pile3_3.top - pileMaster5_2.PileInfo.PileTip
              await this.setState({Depth: cal.toString()})
            }
          }
        }
      }
    }
    // console.warn('image',pile.Images)
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["5_3"] && nextProps.pileAll[this.props.pileid]["5_5"]) {
      pile5_3 = nextProps.pileAll[this.props.pileid]['5_3'].data
      pile5_5 = nextProps.pileAll[this.props.pileid]['5_5'].data
      // console.log('pile53',pile5_5.DrillingList[pile5_5.DrillingList.length - 1].Depth)
      if(this.props.category == 5){
        if (pile5_5.DrillingList[pile5_5.DrillingList.length - 1] != undefined) {
          await this.setState({DepthLast:pile5_5.DrillingList[pile5_5.DrillingList.length - 1].Depth})
        }
      }else{
        if (pile5_3.DrillingList[pile5_3.DrillingList.length - 1] != undefined) {
          await this.setState({DepthLast:pile5_3.DrillingList[pile5_3.DrillingList.length - 1].Depth})
        }
      }
      
    }
    if (pile && nextProps.hidden == false) {
      if(pile.BucketChanged != null && pile.BucketChanged != undefined){
        await this.setState({BucketChanged: pile.BucketChanged})
      }else if (pile.BucketChanged == false) {
        this.setState({BucketChanged: false})
      }
      if(pile.Images != null && pile.Images != undefined && pile.Images != ''){
        await this.setState({ImageFiles: pile.Images})
      }else if (pile.Images) {
        this.setState({ImageFiles:[]})
      }
      if(pile.WaitingTimeMinute != null && pile.WaitingTimeMinute != undefined){
        await this.setState({WaitingTimeMinute: pile.WaitingTimeMinute.toString(),WaitingTimeMinute_old: pile.WaitingTimeMinute.toString()})
      }else if (pile.WaitingTimeMinute) {
        this.setState({WaitingTimeMinute: "30"})
      }
      if(pile.BeforeDepth != null && pile.BeforeDepth != undefined && this.state.BeforeDepth != pile.BeforeDepth.toString() ){
        await this.setState({BeforeDepth: pile.BeforeDepth.toString(),BeforeDepth_old: pile.BeforeDepth.toString()})
        this.onBucketSize()
      }else if (pile.BeforeDepth == "") {
         this.setState({BeforeDepth: "",BucketSize:""})
      }
      if(pile.AfterDepth != null && pile.AfterDepth != undefined && this.state.AfterDepth != pile.AfterDepth.toString() ){
        await this.setState({AfterDepth:pile.AfterDepth.toString(),AfterDepth_old:pile.AfterDepth.toString()})
      }else if (pile.AfterDepth == "") {
        this.setState({AfterDepth:''})
      }
      if(!this.state.set){
        if(this.props.startdatedefault != '' && this.props.startdatedefault != null){
          if(this.props.enddate != '' && this.props.enddate != null){
            this.setState({
              datestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
              timestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("HH:mm"),
              dateend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
              timeend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("HH:mm"),
              startdate:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
              enddate:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm")
            })
          }else{
            this.setState({
              datestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
              timestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("HH:mm"),
              dateend:'',
              timeend:'',
              startdate:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
              enddate:''
            })
          }
        }else{
          if(this.props.startdate != '' && this.props.startdate != null){
            this.setState({
              datestart:moment(this.props.startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
              timestart:moment(this.props.startdate,"DD/MM/YYYY HH:mm").format("HH:mm"),
              startdate:moment(this.props.startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
            })
          }
          if(this.props.enddate != '' && this.props.enddate != null){
            this.setState({
              dateend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
              timeend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("HH:mm"),
              enddate:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm")
            })
          }else{
            this.setState({
              dateend:'',
              timeend:''
            })
          }
        }
        
      }
      if(nextProps.pileAll[this.props.pileid]['6_2'].startdate != null && nextProps.pileAll[this.props.pileid]['6_2'].startdate != ''){
        this.setState({
          datestart:moment(nextProps.pileAll[this.props.pileid]['6_2'].startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
          timestart:moment(nextProps.pileAll[this.props.pileid]['6_2'].startdate,"DD/MM/YYYY HH:mm").format("HH:mm"),
          startdate:moment(nextProps.pileAll[this.props.pileid]['6_2'].startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
        })
      }
      if(nextProps.pileAll[this.props.pileid]['6_2'].enddate != null && nextProps.pileAll[this.props.pileid]['6_2'].enddate != ''){
        this.setState({
          dateend:moment(nextProps.pileAll[this.props.pileid]['6_2'].enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
          timeend:moment(nextProps.pileAll[this.props.pileid]['6_2'].enddate,"DD/MM/YYYY HH:mm").format("HH:mm"),
          enddate:moment(nextProps.pileAll[this.props.pileid]['6_2'].enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
        })
      }
      
      
    }
    if (pile6_0) {
      // if (pile3_0.Edit_Flag != null && pile3_0.Edit_Flag != undefined && pile3_0.Edit_Flag != '') {
        this.setState({Edit_Flag:pile6_0.Edit_Flag})
      // }
    }
    if(pile6_1){
      this.setState({CrossFlag:pile6_1.CrossFlag,CrossFlagold:pile6_1.CrossFlag},async()=>{
        if(this.state.CrossFlag == true && this.state.checkcheck== false){
          var value = {
            process:6,
            step:2,
            pile_id: this.props.pileid,
            data:{
              BucketChanged: null,
              WaitingTimeMinute: null,
              BeforeDepth: null,
              AfterDepth: null,
              Images: null
            },
            startdate: null,
            enddate: null
          }
          await this.props.mainPileSetStorage(this.props.pileid,'6_2',value)
          this.setState({checkcheck:true})
          // console.warn('ok')
        }else{
          this.setState({checkcheck:false})
        }
      })
    }
    
    // if(this.state.status6 == 2){
    //   console.warn('test2',nextProps.pileAll[this.props.pileid]["6_1"].data.ChangeDrillingFluidBeforeDepth,this.state.check)
    
    //     if(nextProps.pileAll[this.props.pileid]["6_1"].data.ChangeDrillingFluidBeforeDepth != '' && nextProps.pileAll[this.props.pileid]["6_1"].data.ChangeDrillingFluidBeforeDepth != null && nextProps.pileAll[this.props.pileid]["6_1"].data.ChangeDrillingFluidAfterDepth != '' && nextProps.pileAll[this.props.pileid]["6_1"].data.ChangeDrillingFluidAfterDepth != null && this.state.check == false){
    //       // console.warn('yes')
    //       if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["6_1"]) {
    //         this.setState({
    //           BeforeDepth:parseFloat(nextProps.pileAll[this.props.pileid]["6_1"].data.ChangeDrillingFluidBeforeDepth).toFixed(3),
    //           AfterDepth:parseFloat(nextProps.pileAll[this.props.pileid]["6_1"].data.ChangeDrillingFluidAfterDepth).toFixed(3),
    //           BucketChanged:false
    //         }, ()=>{
    //           // await this.onBucketSize()
    //           // this.saveLocal('6_2')
    //         })
    //         this.onBucketSize()
    //     }
    //     }
        
    // }
  }

  async updateState(value) {
    if (value.data.ImageFiles != undefined) {
      await this.setState({ImageFiles:value.data.ImageFiles})
      this.saveLocal('6_2')
    }
  }

  // onSumDepth(data){
  //   var sum = 0
  //   if (data != null) {
  //     for (var i = 0; i < data.length; i++) {
  //       sum = parseFloat(sum) + parseFloat(data[i].Depth == '' ? 0 : data[i].Depth )
  //     }
  //     this.setState({Depth:isNaN(sum) == true ? 0 : sum})
  //     this.onBucketSize()
  //   }
  // }
  getDate(){
    if(this.state.type == 'start'){
      if(this.state.startdate != ''){
        return new Date(moment(this.state.startdate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        if(this.props.startdate != '' && this.props.startdate != undefined && this.props.startdate != null){
          return new Date(moment(this.props.startdate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
        }else{
          return new Date()
        }
      }
    }else{
      if(this.state.enddate != ''){
        return new Date(moment(this.state.enddate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        if(this.props.enddate != '' && this.props.enddate != undefined && this.props.enddate != null){
          return new Date(moment(this.props.enddate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
        }else{
          return new Date()
        }
      }
    }
  }
  onBucketSize(){
    if (this.state.BeforeDepth != '') {
      var diff =  parseFloat(this.state.DepthLast) - parseFloat(this.state.BeforeDepth)
      if (diff > 0 ) {
        this.setState({BucketSize:isNaN(diff) == true ? '' : parseFloat(diff).toFixed(3)})
      }else {
        this.setState({BucketSize:parseFloat(diff).toFixed(3)})
      }
    }else  {
      this.setState({BucketSize:''})
    }
  }
  async mixdate(){
    if( this.state.datestart != '' && this.state.timestart != ''){
     await this.setState({startdate:this.state.datestart+' '+this.state.timestart})
    }
    if(this.state.dateend != '' && this.state.timeend != ''){
      await this.setState({enddate:this.state.dateend+' '+this.state.timeend})
    }
  }
  async saveLocal(process_step,data){
    await this.mixdate()
    // console.warn('saveLocal2')
    var value = {
      process:6,
      step:2,
      pile_id: this.props.pileid,
      data:{
        BucketChanged: this.state.BucketChanged,
        WaitingTimeMinute: this.state.WaitingTimeMinute,
        BeforeDepth: this.state.BeforeDepth,
        AfterDepth: this.state.AfterDepth,
        Images: this.state.ImageFiles,
        WaitingTimeMinute_old: this.state.WaitingTimeMinute_old,
        BeforeDepth_old: this.state.BeforeDepth_old,
        AfterDepth_old: this.state.AfterDepth_old,
      },
      startdate: this.state.startdate,
      enddate: this.state.enddate
    }
    console.log(value)
    await this.props.mainPileSetStorage(this.props.pileid,process_step,value)
  }

  onCameraRoll(keyname,photos,type){
    if (this.state.Edit_Flag == 1) {
      Actions.camearaRoll({jobid:this.props.jobid,process:this.state.process,step:this.state.step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:type,shape:this.props.shape,category:this.props.category,Edit_Flag:this.state.Edit_Flag})
    }else {
      Actions.camearaRoll({jobid:this.props.jobid,process:this.state.process,step:this.state.step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:'view',shape:this.props.shape,category:this.props.category,Edit_Flag:this.state.Edit_Flag})
    }
  }

  async selecteBucketChanged(){
    if (this.state.Edit_Flag == 1) {
      await this.setState({ BucketChanged: !this.state.BucketChanged ,check:true})
      this.saveLocal('6_2')
    }
  }
  async selectedWaitingTimeMinute(WaitingTimeMinute) {
    await this.setState({ WaitingTimeMinute: WaitingTimeMinute,visible: false,visible2: false,visible3:false,check:true})
  }
  async selectedBeforeDepth(BeforeDepth) {
    this.setState({ BeforeDepth: this.onChangeFormatDecimal(BeforeDepth),visible: false,visible2: false,visible3:false,check:true},() =>{
      this.onBucketSize()
    })
  }
  async selectedAfterDepth(AfterDepth) {
     this.setState({ AfterDepth: this.onChangeFormatDecimal(AfterDepth),visible: false,visible2: false,visible3:false,check:true},()=>{
     })
  }
  onChangeFormatDecimal(data){
    if (data.split(".")[1]) {
      if (data.split(".")[1].length > 3) {
        return parseFloat(data).toFixed(3)
      }else {
        return data
      }
    }else {
      return data
    }
  }
  onSelectDate(date){
    let date2 = moment(date).format("DD/MM/YYYY")
    if(this.state.type=='start'){
      this.setState({ datestart: date2 ,visible: false,visible2: false, set:true,visible3:false,datevisibledate: false})
      this.saveLocal('6_2')
    }else if(this.state.type=='end'){
      this.setState({ dateend: date2 ,visible: false,visible2: false, set:true,visible3:false,datevisibledate: false})
      this.saveLocal('6_2')
    }
  }
  onSelectTime(date){
    let date2 = moment(date).format("HH:mm")
    if(this.state.type=='start'){
      this.setState({ timestart: date2 ,visible: false,visible2: false, set:true,visible3:false,datevisibletime: false})
      this.saveLocal('6_2')
    }else if(this.state.type=='end'){
      this.setState({ timeend: date2 ,visible: false,visible2: false, set:true,visible3:false,datevisibletime: false})
      this.saveLocal('6_2')
    }
  }
  onChangedate = (event, selectedDate) => {
    if(event.type == 'set'){
      this.onSelectDate(selectedDate)
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };
  onChangetime = (event, selectedDate) => {
    if(event.type == 'set'){
      this.onSelectTime(selectedDate)
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };
  render() {
    if (this.props.hidden) {
      return null
    }
    // console.warn('6_2')
    return (
      <View style={styles.listContainer}>
        <RNTooltips
          text={I18n.t('tooltip.6_21')}
          textSize={16}
          visible={this.state.visible}
          reference={this.main3}
          tintColor="#007CC2"
        />
        <RNTooltips
          text={this.props.category==5?I18n.t('tooltip.6_23_DW'):(this.state.DepthLast>=this.state.Depth?I18n.t('tooltip.6_22'):I18n.t('tooltip.6_23'))}
          textSize={16}
          visible={this.state.visible2}
          reference={this.main4}
          tintColor="#007CC2"
        />
        <RNTooltips
          // text={this.state.DepthLast>=this.state.Depth?I18n.t('tooltip.6_23_1'):I18n.t('tooltip.6_23_2')}
          text={this.props.category==5? I18n.t('tooltip.6_23_1_DW'):I18n.t('tooltip.6_23_1')}
          textSize={16}
          visible={this.state.visible3}
          reference={this.main5}
          tintColor="#007CC2"
        />

        <View style={styles.listTopic}>
          <Text style={styles.listTopicText} >{I18n.t('mainpile.6_1.crosstitle')} *</Text>
        </View>

        <View style={styles.listTopicRow}>
          <View style={[styles.icon]}>
            {this.state.BucketChanged == true? (
              <Icon
              
                name="check"
                reverse
                color="#6dcc64"
                size={15}
                onPress={() => !this.state.CrossFlag ?this.selecteBucketChanged():{}}
              />
            ) : (
              <Icon
                name="check"
                reverse
                color="grey"
                size={15}
                onPress={() =>  !this.state.CrossFlag ?this.selecteBucketChanged():{}}
              />
            )}
            <Text>{I18n.t('mainpile.6_2.changebucket')}</Text>
          </View>
          <View style={[styles.buttonImage]} >
            <TouchableOpacity
              style={[styles.image,this.state.ImageFiles.length>0?{backgroundColor:'#6dcc64'}:{}]}
              onPress={() =>  !this.state.CrossFlag?this.onCameraRoll("ImageFiles", this.state.ImageFiles,'edit'):{}}
            >
              <Icon name="image" type="entypo" color="#FFF" />
              <Text style={[styles.imageText,{fontSize:I18n.locale=='en'?12:12}]}>{I18n.t("mainpile.steeldetail.view")}</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.listTopicSub} >
          <Text style={styles.listTopicSubText}>{I18n.t('mainpile.6_2.time')}</Text>
          <TextInput
            editable={this.state.Edit_Flag == 1 && !this.state.CrossFlag ? true : false}
            keyboardType="numeric"
            underlineColorAndroid='transparent'
            style={this.state.Edit_Flag == 1 && !this.state.CrossFlag ? styles.listTextbox : [styles.listTextbox, styles.textboxDisable]}
            onChangeText={(WaitingTimeMinute) => this.selectedWaitingTimeMinute(WaitingTimeMinute)}
            value={this.state.WaitingTimeMinute.toString()}
            onEndEditing={()=>{
              this.saveLocal('6_2')
            }}
          />
        </View>
        <View 
        style={[styles.listTopicSub]}>
          <View style={{flexDirection:'row'}}>
            <Text style={styles.listTopicSubText}>{this.props.category == 5 ? I18n.t('mainpile.6_2.depth_dw'):I18n.t('mainpile.6_2.depth')}</Text>
           {/* <Icon
              name="ios-information-circle"
              type="ionicon"
              color="#517fa4"
              onPress={() => {
                this.setState({visible: true,visible2:false, visible3:false})
              }}
            />*/}
          </View>
          <View style={{flexDirection: "row",
          // justifyContent: "center",
          // alignItems: "center",
          borderColor: MENU_GREY_ITEM,}}  ref={ref => {
            this.main3 = ref
          }}>
            <TextInput
              editable={this.state.Edit_Flag == 1 && !this.state.CrossFlag ? true : false}
              keyboardType="numeric"
              underlineColorAndroid='transparent'
              style={this.state.Edit_Flag == 1 && !this.state.CrossFlag ? styles.listTextbox : [styles.listTextbox, styles.textboxDisable]}
              onChangeText={(BeforeDepth) => this.selectedBeforeDepth(BeforeDepth)}
              value={this.state.BeforeDepth.toString()}
              onEndEditing={()=> {
                this.setState({BeforeDepth:this.state.BeforeDepth  ? parseFloat(this.state.BeforeDepth).toFixed(3) : '',visible2:false,visible:false,visible3:false})
                this.saveLocal('6_2')
              }}
            /> 
          </View>
        </View>
        <View style={styles.listTopicSub} >
        <View style={{flexDirection:'row'}}>
        <Text style={styles.listTopicSubText}>{I18n.t('mainpile.6_2.crossSize')}</Text>
        <Icon 
              name="ios-information-circle"
              type="ionicon"
              color="#517fa4"
              onPress={() => {
                this.setState({visible: false,visible2:false, visible3:true})
              }}
            />
        </View>
        <View style={{flexDirection: "row",
        // justifyContent: "center",
        // alignItems: "center",
        borderColor: MENU_GREY_ITEM,}}  ref={ref => {
          this.main5 = ref
        }}>
          <TextInput
            value={isNaN(this.state.BucketSize) ? '' : this.state.BucketSize }
            editable = {false}
            keyboardType="numeric"
            underlineColorAndroid='transparent'
            style={[styles.listTextbox, styles.textboxDisable]}
          />
          </View>
        </View>

        <View style={styles.listTopicSub} >
          <Text style={styles.listTopicSubText}>{this.state.DepthLast>=this.state.Depth?(this.props.category == 5 ? I18n.t('mainpile.6_2.depth1_dw'):I18n.t('mainpile.6_2.depth1')):(this.props.category == 5 ? I18n.t('mainpile.6_2.depthmust_dw'):I18n.t('mainpile.6_2.depthmust'))}</Text>
          <TextInput
            value={isNaN(this.state.Depth) ? '' :this.state.DepthLast>=this.state.Depth?parseFloat(this.state.DepthLast.toString()).toFixed(3):parseFloat(this.state.Depth.toString()).toFixed(3)}
            editable = {false}
            keyboardType="numeric"
            underlineColorAndroid='transparent'
            style={[styles.listTextbox, styles.textboxDisable]}
          />
        </View>
        <View style={styles.listTopicSub}>
          <View style={{flexDirection:'row'}}>
            <Text style={styles.listTopicSubText}>{this.props.category == 5 ? I18n.t('mainpile.6_2.depth3_dw'):I18n.t('mainpile.6_2.depth3')}</Text>
            <Icon
            name="ios-information-circle"
            type="ionicon"
            color="#517fa4"
            onPress={() => {
              this.setState({visible2: true, visible:false,visible3:false})
            }}
          />
          </View>
          <View style={{flexDirection: "row",
          // justifyContent: "center",
          // alignItems: "center",
          borderColor: MENU_GREY_ITEM,}}  ref={ref => {
            this.main4 = ref
          }}>
          {
            this.state.status10 == 0?
            <TextInput
            editable={this.state.Edit_Flag == 1 && !this.state.CrossFlag&& this.state.status10 == 0? true : false}
            keyboardType="numeric"
            underlineColorAndroid='transparent'
            style={this.state.Edit_Flag == 1 && !this.state.CrossFlag&& this.state.status10 == 0? styles.listTextbox : [styles.listTextbox, styles.textboxDisable]}
            onChangeText={(AfterDepth) => this.selectedAfterDepth(AfterDepth)}
            value={this.state.AfterDepth.toString()}
            onEndEditing={()=> {
                this.setState({AfterDepth:this.state.AfterDepth  ? parseFloat(this.state.AfterDepth).toFixed(3) : '',visible2:false,visible:false,visible3:false})
                this.saveLocal('6_2')
            }}
          />:
          <TouchableOpacity disabled={this.state.Edit_Flag == 0} style={{width:'100%'}} onPress={()=> {
            // if(this.state.Edit_Flag == 1){
              Alert.alert('', this.props.category == 5 ? I18n.t('mainpile.6_0.error2'):I18n.t('mainpile.6_0.error'), [
                {
                  text: 'OK'
                }
              ])
            // }
            
          }}>
          <TextInput
            editable={this.state.Edit_Flag == 1 && !this.state.CrossFlag&& this.state.status10 == 0? true : false}
            keyboardType="numeric"
            underlineColorAndroid='transparent'
            style={this.state.Edit_Flag == 1 && !this.state.CrossFlag? styles.listTextbox : [styles.listTextbox, styles.textboxDisable]}
            onChangeText={(AfterDepth) => this.selectedAfterDepth(AfterDepth)}
            value={this.state.AfterDepth.toString()}
            onEndEditing={()=> {
                this.setState({AfterDepth:this.state.AfterDepth  ? parseFloat(this.state.AfterDepth).toFixed(3) : '',visible2:false,visible:false,visible3:false})
                this.saveLocal('6_2')
            }}
          />
          </TouchableOpacity>
          }
          
          </View>
        </View>

        {this.state.datevisibledate &&<DateTimePicker
          value={this.getDate()}
          isVisible={this.state.datevisibledate}
          is24Hour={true}
          display='spinner'
          mode='date'
          onChange={this.onChangedate}
          />}
        {this.state.datevisibletime && <DateTimePicker
          value={this.getDate()}
          isVisible={this.state.datevisibletime}
          is24Hour={true}
          display='spinner'
          mode='time'
          onChange={this.onChangetime}
        />}

        <View style={[styles.listTopic,{marginTop:10}]}>
          <Text style={styles.listTopicText}>{I18n.t('startenddate.startenddate')}{I18n.t('mainpile.step.6_1')}</Text>
        </View>
        <View style={[styles.imageDetail,{marginLeft:20,marginRight:20,marginBottom:15}]}>
        <Text style={{width:'25%'}}>{I18n.t('startenddate.start')}</Text>
        <TouchableOpacity 
          style={[styles.buttonBox, { width: "35%" }]}
          onPress={() => {
            !this.state.CrossFlag?
            this.setState({datevisibledate:true,type:'start'}):{}
          }}
          disabled={this.state.Edit_Flag == 1 && !this.state.CrossFlag? false : true}
        >
          <Text>{this.state.datestart}</Text>
        </TouchableOpacity>
        <Text style={{width:'10%',textAlign:'center'}}>{I18n.t('startenddate.time')}</Text>
        <TouchableOpacity
          style={[styles.buttonBox, { width: "30%" }]}
          onPress={() => {
            !this.state.CrossFlag?
            this.setState({datevisibletime:true,type:'start'}):{}
          }}
          disabled={this.state.Edit_Flag == 1 && !this.state.CrossFlag? false : true}
        >
          <Text>{this.state.timestart}</Text>
        </TouchableOpacity>
        {/*<TouchableOpacity
          style={styles.approveBox}
          onPress={() => this.onnowdate('start')}
          disabled={this.state.Edit_Flag == 0}
        >
          <Text style={{color:'#fff'}}>เริ่ม</Text>
        </TouchableOpacity>*/}
      </View>
      <View style={[styles.imageDetail,{marginLeft:20,marginRight:20,marginBottom:15}]} >
      <Text style={{width:'25%'}}>{I18n.t('startenddate.end')}</Text>
      <TouchableOpacity
          style={[styles.buttonBox, { width: "35%" }]}
          onPress={() => {
            !this.state.CrossFlag?
            this.setState({datevisibledate:true,type:'end'}):{}
          }}
          disabled={this.state.Edit_Flag == 1 && !this.state.CrossFlag? false : true}
        >
          <Text>{this.state.dateend}</Text>
        </TouchableOpacity>
        <Text style={{width:'10%',textAlign:'center'}}>{I18n.t('startenddate.time')}</Text>
        <TouchableOpacity
          style={[styles.buttonBox, { width: "30%" }]}
          onPress={() => {
            !this.state.CrossFlag?
            this.setState({datevisibletime:true,type:'end'}):{}
          }}
          disabled={this.state.Edit_Flag == 1 && !this.state.CrossFlag? false : true}
        >
          <Text>{this.state.timeend}</Text>
        </TouchableOpacity>
        {/*<TouchableOpacity
          style={styles.approveBox}
          onPress={() => this.onnowdate('end')}
          disabled={this.state.Edit_Flag == 0}
        >
          <Text style={{color:'#fff'}}>สิ้นสุด</Text>
        </TouchableOpacity>*/}
          </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll || null
   }
}

export default connect(mapStateToProps, actions, null, { withRef: true })(Pile6_2)
