  import React, { Component } from "react"
import { View, Text, StyleSheet, TouchableOpacity,Alert } from "react-native"
import StepIndicator from "react-native-step-indicator"
import { Container, Content } from "native-base"
import { MAIN_COLOR,DEFAULT_COLOR_1, DEFAULT_COLOR_4 } from "../Constants/Color"
import Pile4_1 from "./Pile4_1"
import styles from './styles/Pile4.style'
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux'
import * as actions from '../Actions'
import I18n from '../../assets/languages/i18n'

class Pile4 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 4,
      step: 0,
      ref:1,
      error:null,
      data:[],
      location:{
        position:{
          lat:1,
          log:1,
        }
      },
      Edit_Flag:1,
      status5:0,
      checksavebutton:false,
      stepstatus:null,
      checkto5_super_random:null
    }
  }
  componentDidMount(){
    // console.log(this.props.stack)
    this.props.getDrillingFluids({
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      Language: I18n.locale
    })
    if (this.props.stack) {
      this.setState({step:this.props.stack[this.props.stack.length - 1].step - 1})
    }
  }
  async componentWillReceiveProps(nextProps){
    if(nextProps.step_status2_random != this.state.stepstatus && nextProps.processstatus == 4){
      this.setState({stepstatus:nextProps.step_status2_random},async()=>{
        // console.warn('yes',nextProps.step_status2.Step2Status)
        await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step4Status: nextProps.step_status2.Step4Status })
        this.props.mainPileGetStorage(this.props.pileid, "step_status")
      })
    }
    if(nextProps.checkto5_super == true && nextProps.checkto5_super_random != this.state.checkto5_super_random && (this.props.category == 3||this.props.category == 5) && nextProps.lockstepcheckto5_super == false){
      this.setState({checkto5_super_random: nextProps.checkto5_super_random},()=>{
        this.props.onNextStep()
      this.onSetStack(5,0)
      // setTimeout(()=>{this.setState({checksavebutton:false})},2000)

      })
    }
    if (nextProps.pileAll[this.props.pileid] != undefined) {
      var edit = nextProps.pileAll[this.props.pileid]
      if (edit['4_0'] != undefined) {
          await this.setState({Edit_Flag:edit['4_0'].data.Edit_Flag})
      }

      if(edit != undefined){
        this.setState({status5:edit.step_status.Step5Status})
      }
    }
    await this.setState({error:nextProps.error,data:nextProps.pileAll[nextProps.pileid],location:nextProps.pileAll.currentLocation})
  }

  updateState(value){
    if (value.step != null) {
      const ref = 'pile'+ value.process +'_'+ value.step
      if (this.refs[ref].getWrappedInstance().updateState(value) != undefined) {
        this.refs[ref].getWrappedInstance().updateState(value)
      }
    }else{
      console.log(value)
    }
  }

  setStep(step){
    return this.setState({step:step})
  }
  initialStep(){
    this.setState({step:0})
  }

  _renderStepFooter() {
    return (
      <View>
        <View style={styles.buttonFooterGroup}>
          {
            this.state.Edit_Flag == 1 ?
            <View style={[styles.second,
              this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
            ]}>
              <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton} 
              // disabled={this.state.checksavebutton}
              >
                <Text style={styles.selectButton}>{I18n.t('mainpile.button.delete')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.secondButton,
                this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
              ]} onPress={this.nextButton}
              disabled={this.state.checksavebutton}
              >
                <Text style={styles.selectButton}>{this.state.Edit_Flag == 1 ? I18n.t('mainpile.button.next'):I18n.t('mainpile.button.continue')}</Text>
              </TouchableOpacity>
            </View>
            :
            <View/>
          }
        </View>
      </View>
    )
  }

async onSave(step){
  // this.setState({checksavebutton:true})

  const text = "mainpile.lockprocess.process" + 5 + "_" + (this.props.shape == 1 ? "bp" : "dw")
  if(this.props.drillingfluidslist.length>0){
    await this.props.mainPileGetAllStorage()
    if (step == 0) {
      var value = {
        process:4,
        step:1,
        pile_id: this.props.pileid,
        drillingFluids:this.props.drillingfluidslist,
        shape:this.props.shape
      }
      // await this.props.mainPileSetStorage(this.props.pileid,'3_1',value)
      if (this.state.Edit_Flag == 1) {
        var value0 = {
          process: 5,
          step: 0,
          pile_id: this.props.pileid,
          shape: this.props.shape,
          drillingFluids:this.props.drillingfluidslist,
          category:this.props.category
        }
        this.props.mainpileAction_checkStepStatus(value0).then(data => {
          console.log('test lock',data.check )
          if (data.check == true) {
            this.props.mainpileAction_checkStepStatus(value)
              .then(async (data) =>
              {
                // if (this.state.data['4_0'].data.Status != 2) {
                //   await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step4Status:data.statuscolor})
                //   this.props.mainPileGetStorage(this.props.pileid,'step_status')
                // }else{
                //   await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step4Status:data.statuscolor})
                //   this.props.mainPileGetStorage(this.props.pileid,'step_status')
                // }
                this.props.getStepStatus2({
                  jobid: this.props.jobid,
                  pileid: this.props.pileid,
                  process:4
                })
                if ( this.state.error == null) {
                  if (data.check == false) {
                    Alert.alert('', I18n.t('mainpile.4_1.error_nextstep'), [
                      {
                        text: 'OK'
                      }
                    ])
                    this.setState({checksavebutton:false})
                  }else {
                    if(this.props.category == 3 || this.props.category == 5){
                      var data_check = {
                        Language:I18n.locale,
                        JobId: this.props.jobid,
                        PileId: this.props.pileid,
                      }

                      this.props.checkstepofsuperstructure5(data_check)
                    }else{
                      this.props.onNextStep()
                      this.onSetStack(5,0)
                      // setTimeout(()=>{this.setState({checksavebutton:false})},2000)
                    }
                    
                  }
                } else {
                  
                  Alert.alert(this.state.error)
                  this.setState({checksavebutton:false})
                }
              })
          }else{
            console.log('test lock')
            const text1 = "mainpile.lockprocess.process" + 5 + "_" + "dw"
            if(this.props.category == 5){
              Alert.alert("", I18n.t(text1), [
                {
                  text: "OK",
                }
              ])
              this.setState({checksavebutton:false})
            }else{
              Alert.alert("", I18n.t(text), [
                {
                  text: "OK",
                }
              ])
              this.setState({checksavebutton:false})
            }
           
          }
        })
      }
    
  }
  }else{
    Alert.alert("", I18n.t('mainpile.lockprocess.process4'), [
                {
                  text: "OK",
                }
              ])
              this.setState({checksavebutton:false})
  }
  
}
async onSetStack(pro,step) {
  var data = {
    process: pro,
    step: step + 1
  }
  await this.props.setStack(data)
  await this.props.mainPileGetAllStorage()
}
  nextButton = async () => {
    switch (await this.state.step) {
      case 0:
        this.onSave(this.state.step)
        break
      default:
        break
    }
  }

  deleteButton = () => {
    const data = {
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      processid: this.state.process,
      lat: this.state.location == undefined ? 1 : this.state.location.position.lat,
      log: this.state.location == undefined ? 1 : this.state.location.position.log,
    }
    if(this.state.status5 == 0){
      Alert.alert("Warning", I18n.t("mainpile.button.confirmdelete"), [
        { text: "Cancel" },
        { text: "OK", onPress: () => this.props.pileDelete(data) }
      ])
    }else{
      Alert.alert("", I18n.t('alert.error_delete4'), [
        {
          text: "OK"
        }
      ])
    }
    
  }


  render() {
    if (this.props.hidden) {
      return null
    }
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1,height:'90%',bottom:0}}>
          <Pile4_1 
              ref='pile4_1' 
              hidden={!(this.state.step == 0)} 
              jobid={this.props.jobid} 
              pileid={this.props.pileid} 
              onRefresh={this.props.onRefresh} 
              shape={this.props.shape} 
              pile4Type={'content'}
              category = {this.props.category}
              />
        </View>
        {/* <Content> */}
          {/* <Pile4_1 ref='pile4_1' hidden={!(this.state.step == 0)} jobid={this.props.jobid} pileid={this.props.pileid} onRefresh={this.props.onRefresh} shape={this.props.shape} pile4Type={'content'}/> */}
        {/* </Content> */}
        {/* <View style={{height:'15%'}}> */}
        {this._renderStepFooter()}
        {/* </View> */}
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    error: state.pile.error,
    pileAll: state.mainpile.pileAll || null,
    stack: state.mainpile.stack_item,
    drillingfluidslist: state.drillingFluids.drillingfluidslist,

    step_status2_random:state.mainpile.step_status2_random,
    step_status2:state.mainpile.step_status2,
    processstatus:state.mainpile.process,

    checkto5_super: state.pile.checkto5_super,
    checkto5_super_random: state.pile.checkto5_super_random,
    lockstepcheckto5_super:state.pile.lockstepcheckto5_super,

  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(Pile4)
