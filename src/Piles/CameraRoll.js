import React, { Component } from 'react'
import { Actions } from 'react-native-router-flux'
import {
  Alert,
  BackHandler,
  FlatList,
  Image,
  KeyboardAvoidingView,
  Text,
  TouchableOpacity,
  View,
  Platform,
  Dimensions,
  ScrollView,
  StyleSheet,
  ImageBackground,
  ActivityIndicator,
  Slider,
  ToastAndroid,
  Clipboard,
  PermissionsAndroid,
} from 'react-native'
import CameraRoll2 from "@react-native-community/cameraroll";
import ModalSelector from 'react-native-modal-selector'
import { connect } from 'react-redux'
import Camera, { RNCamera } from 'react-native-camera'
import * as actions from '../Actions'
import I18n from '../../assets/languages/i18n'
import styles from './styles/CameraRoll.style'
import ImageZoom from "react-native-image-pan-zoom"
import moment from 'moment'
import RNFetchBlob from  "rn-fetch-blob"
import FastImage from 'react-native-fast-image'
import { MENU_ORANGE_BORDER, MAIN_COLOR, DEFAULT_COLOR_1, DEFAULT_COLOR_4,CATEGORY_1 } from '../Constants/Color'
import DeviceInfo from "react-native-device-info"
import { Icon } from "react-native-elements"
import RNFS from 'react-native-fs'
import Sound from 'react-native-sound'
import ImageResizer from 'react-native-image-resizer'
import Share, {ShareSheet, Button} from 'react-native-share'
import FusedLocation from 'react-native-fused-location'
const sound = new Sound(require('../../assets/sound/camera_shutter.mp3'), Sound.MAIN_BUNDLE, (error) => {
  if (error) {
    console.log('failed to load the sound', error)
    return
  }
  // loaded successfully
  console.log('duration in seconds: ' + sound.getDuration() + 'number of channels: ' + sound.getNumberOfChannels())
})


class CameraRoll extends Component {
  constructor(props) {
    super(props)

    this.state = {
      display: '',
      image: '',
      photos: [],
      photosLib: [],
      type: RNCamera.Constants.Type.back,
      loading: true,
      flash: false,
      depth: 0.5,
      focus: true,
      zoom: 0,
      loadingpic: false,
      Edit_Flag: 1,
      visible:false,
      base64img:''
      // note:null
    }
  }

  
  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButton)
    this.getPhotosLib()
    this.initialPhoto()
    
    var processTitle = ''
    if(this.props.process == 1) {
      processTitle = 'SteelCageSection'
    } else if(this.props.process == 2) {
      processTitle = 'TwoAxisVerticalCheck'
    } else if(this.props.process == 3) {
      if(this.props.keyname == 'Image_ReadingCoordinate') {
        processTitle = 'PileSurvey'
      } else {
        processTitle = 'PileSurveyDiff'
      }
    }
    const bundleid = DeviceInfo.getBundleId()
    if (Platform.OS == "android") {
      RNFetchBlob.fs
        .exists( RNFS.ExternalDirectoryPath + '/data/' + bundleid + '/Upload/' + processTitle)
        .then((exist) => {
          if (exist == false) {
            RNFetchBlob.fs
              .mkdir(
                RNFS.ExternalDirectoryPath + '/data/' + bundleid + '/Upload/' + processTitle
              )
              .then((data) => {
                console.log("folder create");
              })
              .catch((err) => {
                console.log("no folder create", err);
              });
          } else {
            console.log("folder created");
          }
        })
        .catch(() => {});

        let dirs = RNFetchBlob.fs.dirs

        RNFetchBlob.fs
        .exists( dirs.DCIMDir+'/Pylon/Download/')
        .then((exist) => {
          if (exist == false) {
            RNFetchBlob.fs
              .mkdir(
                dirs.DCIMDir+'/Pylon/Download/'
              )
              .then((data) => {
                console.log(" dirs.DCIMDir folder create");
              })
              .catch((err) => {
                console.log(" dirs.DCIMDir no folder create", err);
              });
          } else {
            console.log(" dirs.DCIMDir folder created");
          }
        })
        .catch(() => {});
    }

    // RNFetchBlob.fs.exists('/data/data/' + bundleid + '/Upload/' + processTitle)
    //   .then((exist) => {
    //     if(exist == false) {
    //       RNFetchBlob.fs.mkdir('/data/data/' + bundleid + '/Upload/' + processTitle).then((data) => { }).catch((err) => { })
    //     }
    //   })
    //   .catch(() => { })
  }

  

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButton)
  }
  componentDidMount() {
    console.log('componentDidMount',this.props.Edit_Flag)
    this.saveLocation()
    if((this.props.Edit_Flag!==null)&&(this.props.Edit_Flag!=="")){
      console.log("Edit_flag cameraRoll",this.props.Edit_Flag)
      this.setState({Edit_Flag:this.props.Edit_Flag})
    }
  }
  async saveLocation() {
    console.log('save local')
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
          title: 'App needs to access your location',
          message: 'App needs access to your location ' +
          'so we can let our app be even more awesome.'
          }
      )
      if (granted) {
        FusedLocation.setLocationPriority(FusedLocation.Constants.HIGH_ACCURACY)
        const location = await FusedLocation.getFusedLocation()
        this.props.setlocation(location)
        var data
        if(location == undefined){
          let lat = this.props.location_lat
          let log = this.props.location_log
           data = {
            position:{
              lat:lat,
              log:log
            }
          }
        }else{
          let lat = location.latitude
          let log = location.longitude
          data = {
            position:{
              lat:lat,
              log:log
            }
          }
        }
       
        
        await this.props.mainPileSetStorageNoneRedux('currentLocation',data)

      }else{
        // console.warn('granted')
      }
  }

  async initialPhoto() {
    var photos = []
    if(this.props.typeViewPhoto == 'edit') {
      photos.push({ image: 'etc' })
    }
    if(this.props.photos != undefined) {
      if(this.props.photos.length > 0) {
        for(var i in this.props.photos) {
          photos.push(this.props.photos[i])
        }
      }
    }
    this.setState({ photos })
  }


  async resizeImage(path){
    console.log('resize path',path)
    let extPath = RNFS.mkdir(RNFS.ExternalStorageDirectoryPath + '/Pylon/Images/resize')
    ImageResizer.createResizedImage(path, 2000, 2000, 'JPEG', 100,0,'/storage/emulated/0/Pylon/Images/resize').then((response) => {
      console.log('response resize',response)
    }).catch((err) => {
      
    })
  }

  saveImage(base64, type, timestamp) {
    var processTitle = ''
    if(this.props.process == 1) {
      processTitle = 'SteelCageSection'
    } else if(this.props.process == 2) {
      processTitle = 'TwoAxisVerticalCheck'
    } else if(this.props.process == 3) {
      console.log(this.props.keyname)
      if(this.props.keyname == 'Image_ReadingCoordinate') {
        processTitle = 'PileSurvey'
      } else {
        processTitle = 'PileSurveyDiff'
      }
    }
    else {
      processTitle = 'etc'
    }
    // let extPath = RNFS.mkdir(RNFS.ExternalStorageDirectoryPath + '/Pylon/Images/')
    // const bundleid = DeviceInfo.getBundleId()
    // let extPath = RNFS.mkdir( RNFS.ExternalDirectoryPath + '/data/' + bundleid + '/Upload/' + processTitle)
    let extPath = RNFS.mkdir( RNFS.ExternalDirectoryPath + '/Pylon/Images/')
    
    // const NEWPATH = RNFS.ExternalStorageDirectoryPath + '/Pylon/Images/' + processTitle + '_' + this.props.jobid + '_' + this.props.pileId + '_' + this.props.process + '_' + this.state.photos.length + '' + Math.floor(Math.random() * (999 - 100 + 1) + 100) + '.jpeg'
    const NEWPATH = RNFS.ExternalDirectoryPath + '/Pylon/Images/' + processTitle + '_' + this.props.jobid + '_' + this.props.pileId + '_' + this.props.process + '_' + this.state.photos.length + '' + Math.floor(Math.random() * (999 - 100 + 1) + 100) + '.jpeg'
    RNFS.writeFile(NEWPATH, base64, 'base64')
    .then(() => {
      // CameraRoll2.saveToCameraRoll('file://' + NEWPATH, 'photo')
      if(type == 'camera') {
        this.state.photos.push({ image: 'file://' + NEWPATH, timestamp: timestamp, type: type })
        this.state.photosLib.push({ image: 'file://' + NEWPATH, timestamp: timestamp, type: type })
      } else {
        const time = this.formatDate(timestamp, 'gallery')
        this.state.photos.push({ image: 'file://' + NEWPATH, timestamp: time, type: type })
      }
      this.setState({ display: ''})
    })
    console.log(NEWPATH)
  }

  async savePhotos(path, type, timestamp) {
    var processTitle = ''
    if(this.props.process == 1) {
      processTitle = 'SteelCageSection'
    } else if(this.props.process == 2) {
      processTitle = 'TwoAxisVerticalCheck'
    } else if(this.props.process == 3) {
      console.log(this.props.keyname)
      if(this.props.keyname == 'Image_ReadingCoordinate') {
        processTitle = 'PileSurvey'
      } else {
        processTitle = 'PileSurveyDiff'
      }
    }
    else {
      processTitle = 'etc'
    }
    const bundleid = DeviceInfo.getBundleId()
    let extPath = RNFS.mkdir( RNFS.ExternalDirectoryPath + '/data/' + bundleid + '/Upload/' + processTitle)
    
    // /data/user/0/com.pylonpile/Upload/
    var new_path = ''
    // new_path = '/data/data/' + bundleid + '/Upload/' + processTitle + '/' + this.props.jobid + '_' + this.props.pileId + '_' + this.props.process + '_' + this.state.photos.length + '' + Math.floor(Math.random() * (999 - 100 + 1) + 100) + '.jpeg'
    new_path = RNFS.ExternalDirectoryPath + '/data/' + bundleid + '/Upload/' + processTitle + '/' + this.props.jobid + '_' + this.props.pileId + '_' + this.props.process + '_' + this.state.photos.length + '' + Math.floor(Math.random() * (999 - 100 + 1) + 100) + '.jpeg'
    console.log('newpath', new_path)
    console.log('doc', RNFS.DocumentDirectoryPath)
    console.log('Ext', RNFS.ExternalDirectoryPath)
    console.log('EXT STR', RNFS.ExternalStorageDirectoryPath)
    // const NEWPATH = RNFS.ExternalStorageDirectoryPath + 'Pylon/' + processTitle + '/' + this.props.jobid + '_' + this.props.pileId + '_' + this.props.process + '_' + this.state.photos.length + '' + Math.floor(Math.random() * (999 - 100 + 1) + 100) + '.jpeg'
    const NEWPATH = RNFS.ExternalDirectoryPath + '/data/' + bundleid + '/Upload/' + processTitle + '/' + this.props.jobid + '_' + this.props.pileId + '_' + this.props.process + '_' + this.state.photos.length + '' + Math.floor(Math.random() * (999 - 100 + 1) + 100) + '.jpeg'
    let imageBlob = await RNFetchBlob.fs.readFile(path, 'base64')

    await RNFetchBlob.fs.createFile(new_path, imageBlob, 'base64')
      .then((result) => console.log('REUSLT: ', result))
      .catch((error) => {
        console.log(error.message)
        RNFetchBlob.fs.writeFile(new_path, imageBlob, 'base64')
          .then(() => { })
      })
      
    /** RESIZER
    ImageResizer.createResizedImage(path, 600, 600, 'JPEG', 100, 0).then(async (response) => {
      console.log(response)
      // response.uri is the URI of the new image that can now be displayed, uploaded...
      // response.path is the path of the new image
      // response.name is the name of the new image with the extension
      // response.size is the size of the new image
      console.log(response.size)
      console.log(response.name)
      console.log(response.path)
      console.log(response.uri)

      const imageBlob = await RNFetchBlob.fs.readFile(response.path, 'base64')
      await RNFetchBlob.fs.createFile(new_path, imageBlob, 'base64')
        .then((result) => console.log(result))
        .catch((error) => {
          RNFetchBlob.fs.writeFile(new_path, imageBlob, 'base64')
            .then(() => { })
        })

      if(type == 'camera') {
        await this.state.photos.push({ image: 'file://' + new_path, timestamp: timestamp, type: type })
        await this.state.photosLib.push({ image: 'file://' + new_path, timestamp: timestamp, type: type })
      } else {
        const time = this.formatDate(timestamp, 'gallery')
        await this.state.photos.push({ image: 'file://' + new_path, timestamp: time, type: type })
      }
    }).catch((err) => {
      // Oops, something went wrong. Check that the filename is correct and
      // inspect err to get more details.
    })
 */
    // let extPath2 = RNFS.mkdir(RNFS.ExternalStorageDirectoryPath + '/Pylon/Images/resize')
    let extPath2 = RNFS.mkdir( RNFS.ExternalDirectoryPath + '/Pylon/Images/resize')
  
    // ImageResizer.createResizedImage(new_path, 2000, 2000, 'JPEG', 80,0,'/storage/emulated/0/Pylon/Images/resize').then((response) => {
    ImageResizer.createResizedImage(new_path, 2000, 2000, 'JPEG', 80,0,RNFS.ExternalDirectoryPath + '/Pylon/Images/resize').then((response) => {
      if(type == 'camera') {
        this.state.photos.push({ image: 'file://' + response.path, timestamp: timestamp, type: type })
        this.state.photosLib.push({ image: 'file://' + response.path, timestamp: timestamp, type: type })
      } else {
        const time = this.formatDate(timestamp, 'gallery')
        console.warn('Tiem save', time)
        this.state.photos.push({ image: 'file://' + response.path, timestamp: time, type: type })
      }
      this.setState({ display: "" })
      // delete img
      RNFS.exists(new_path)
      .then( (result) => {
          console.log("file exists: ", result);

          if(result){
            return RNFS.unlink(new_path)
              .then(() => {
                console.log('FILE DELETED');
              })
              // `unlink` will throw an error, if the item to unlink does not exist
              .catch((err) => {
                console.log(err.message);
              });
          }

        })
        .catch((err) => {
          console.log(err.message);
        });
    }).catch((err) => {
      
    })
    // setTimeout(() => {
    //   this.setState({ display: "" })
    // }, 1000)
    
  }



  removeIndex0() {
    if(this.state.display != '') {
      this.setState({ display: '' })
    } else {
      var array = this.state.photos
      array.splice(array, 1)
      this.saveLocal(array)
    }
  }

  async saveLocal(data) {
    var data = {}
    data[this.props.keyname] = this.state.photos
    if(this.props.sectionid != undefined) {
      if(this.props.keyname == 'image_concretespacer') {
        data['sectionid'] = this.props.sectionid
        data['checksteelcage'] = this.props.checksteelcage
        data['checkconcretespacer'] = this.props.checkconcretespacer
        data['image_steelcage'] = this.props.image_steelcage
        data['image_concretespacer'] = this.state.photos
      }if(this.props.keyname == 'images_hanging') {
        console.log('saveLocal camera')
      } else {
        data['sectionid'] = this.props.sectionid
        data['checksteelcage'] = this.props.checksteelcage
        data['checkconcretespacer'] = this.props.checkconcretespacer
        data['image_steelcage'] = this.state.photos
        data['image_concretespacer'] = this.props.image_concretespacer
      }

    }
    if(this.props.typeViewPhoto == 'edit'&&this.props.keyname!="noteimage") {
      console.log('saveLocal camera pop', 
      {
        process: this.props.process,
        step: this.props.step,
        data: data,
      })
      Actions.pop({
        refresh: {
          action: 'update',
          value: {
            process: this.props.process,
            step: this.props.step,
            data: data,
          }
        }
      })
    }else if(this.props.keyname=="noteimage"){
      Actions.pop({
        refresh: {
          action: 'update',
          image: {
            data: data,
          },
          // note: this.state.note
        }
      })
    }else{
      Actions.pop()
    }
  }

  async onDeleteItem(id) {
    var array = this.state.photos
    var index = 0
    for(var i = 0; i < this.state.photos.length; i++) {
      if(this.state.photos[i].image == id) {
        console.log('image', this.state.photos[i].image)
        index = i
        let pathDel = this.state.photos[i].image
        RNFetchBlob.fs.unlink(pathDel.replace('file:///', ''))
          .then((d) => {
            const bundleid = DeviceInfo.getBundleId()
            RNFetchBlob.fs.ls('/data/data/' + bundleid + '/Upload/')
              // files will an array contains filenames
              .then((files) => {
                console.log(files)
              })
          })
          .catch((err) => { console.log('error_delete_image', err) })
      }
    }
    array.splice(index, 1)
    this.setState({ photos: array })
  }

  getPhotosLib() {
    // this.setState({photosLib:[]});
    CameraRoll2.getPhotos({ first: 999, assetType: 'Photos' }).then(r => {
      for(var i = 0; i < r.edges.length; i++) {
        console.log('CameraRoll2',r.edges[i].node.image)
        this.state.photosLib.push({ image: r.edges[i].node.image.uri, timestamp: r.edges[i].node.timestamp })
      }
    }).catch((error) => {
      console.error(error)
    })
  }

  getPhotoUrl(url) {
    this.setState({ image: url })
  }

  onBackButton() {
    Actions.pop()
    return true
  }

  // onPreviewClose() {
  //   this.setState({image: ''});
  // }

  async onPreviewContinue() {
    console.log(this.state.image)
    const imageBlob = await RNFetchBlob.fs.readFile(this.state.image, 'base64')
    Actions.pop({
      refresh: {
        imageBlob
      }
    })
  }

  onTakePhoto() {
    const options = {}
    var now = moment().format("DD-MM-YYYY HH:mm:ss")
    // this.camera.capture().then((data) => this.savePhotos(data.path,'camera',now)).catch(err => console.error(err))
    if(this.camera) {
      const options = { quality: 0.5,fixOrientation: false }
      // const data = await this.camera.takePictureAsync(options)
      // console.warn(data)
      // console.warn("URI", data.uri)
      // console.warn("PATH", data.path)
      // this.savePhotos(data.uri, "camera", now)
      try {
        this.camera
          .takePictureAsync(options)
          .then(data => {
            this.savePhotos(data.uri, "camera", now)
          })
          .catch(error => {
            console.log(error)
          })
      }
      catch(e) {
        // console.warn(e)
      }
    }

  }

  onPhoto() {
    if(!this.state.flash){
    this.playsound()
    }
    let now = moment().format("DD-MM-YYYY HH:mm:ss")
    const options = { fixOrientation: true, quality:0.7}
    
    if (this.camera) {
      if(this.state.flash){
        setTimeout(()=>{this.playsound()},500)
        
        }
      this.camera.takePictureAsync(options)
      .then(data => {
        console.log('onPhoto',data)
        // this.setState({ display: ''})
        const uri = data.uri
        // BASE64
        const filepath = uri.split('//')[1]
        console.log('uri: ', uri)
        RNFS.readFile(filepath, 'base64')
        .then(base => {
          const base64 = `data:image/jpeg;base64,${base}`
          console.log('BASE64: ',base)
          this.saveImage(base, "camera", now)
          
        })  
      })
    }
    // this.setState({ display: "" })
  }

  
  getbase64img(image){
    // console.warn(image.split("?")[0])
    if(image.split(":")[0] != 'file'){
      let imagePath = null
      const { config, fs } = RNFetchBlob
      RNFetchBlob.config({
        fileCache: true
      })
        .fetch("GET", image.split("?")[0])
        .then(resp => {
          imagePath = resp.path()
          return resp.readFile("base64")
        })
        .then(base64Data => {
          // console.warn(base64Data)
          this.setState({base64img:base64Data})
          return fs.unlink(imagePath)
        })
    }else{
      let url = image
      let filepath = url.split('//')[1]
      // console.warn('base4',filepath)
        RNFS.readFile(filepath, 'base64')
        .then(base => {
          this.setState({base64img:base})
        }) 
    }
    
    this.setState({ display: 'preview', image: image })
  }

  renderPhotoSelect() {
    let index = 0
    const data = [
      {
        key: index++,
        label: I18n.t("mainpile.cameraRoll.camera"),
        action: "camera"
      }, {
        key: index++,
        label: I18n.t("mainpile.cameraRoll.library"),
        action: "photoLibrary"
      }
    ]
    return (<View style={styles.container}>
      <View style={[styles.navigation, 
        this.props.category== 3|| this.props.category == 5 ?{backgroundColor:DEFAULT_COLOR_4}:this.props.shape == 1 ? styles.navigationBlue : { backgroundColor: MENU_ORANGE_BORDER }
      ]}>
        <View style={styles.navigationTitle}>
          <Text style={styles.navigationTitleText}>{I18n.t('mainpile.cameraRoll.Photo')}</Text>
        </View>
      </View>

      <FlatList extraData={this.state}
        style={styles.cameraRollRow} data={this.state.photos} numColumns={this.numColumns()} keyExtractor={item => item.image}
        renderItem={({ item }) => {
          switch(item.image) {
            case 'etc':
              return (<View style={[styles.cameraRollList, { justifyContent: 'center'}]}>
                <ModalSelector cancelText="Cancel" animationType="fade" data={data} onChange={(option) => {
                  if(option.action == "camera") {
                    this.setState({ display: "camera" })
                  } else if(option.action == "photoLibrary") {
                    this.setState({ display: "photoLibrary" })
                  }
                }}>
                  <View style={styles.buttonAdd}>
                    <Image source={require('../../assets/image/icon_plus.png')} />
                  </View>
                </ModalSelector>
              </View>)
            default:
              if(this.props.typeViewPhoto == 'edit') {
                var temp = 0
                if(this.props.process == 1 || this.props.process == 3 || this.props.process == 7){
                  if(this.props.process == 7){
                    if(this.props.shape == 2){
                      if(this.props.status == 2){
                       
                        if(this.props.keyname == 'image_concretespacer'){
                          if(this.state.photos.length > 2){
                            temp = 1
                          }else{
                            temp = 0
                          }
                        }else{
                          temp = 1
                        }
                        if(this.props.keyname == 'images_hanging'){
                          if(this.props.status == 2){
                            if(this.state.photos.length > 2){
                              temp = 1
                            }else{
                              temp = 0
                            }
                          }else{
                            temp = 1
                          }
                        }else{
                          temp = 1
                        }
                     
                      }else{
                        temp = 1
                      }
                    }else if(this.props.keyname == 'images_hanging'){
                      if(this.props.status == 2){
                        if(this.state.photos.length > 2){
                          temp = 1
                        }else{
                          temp = 0
                        }
                      }else{
                        temp = 1
                      }
                    }else{
                      temp = 1
                    }
                    
                  }else{
                    if(this.props.status == 2){
                      if(this.state.photos.length > 2){
                        temp = 1
                      }else{
                        temp = 0
                      }
                    }else{
                      temp = 1
                    }
                  }
                  
                }else{
                  temp = 1
                }
                // console.warn('check temp', this.props.process,this.props.status,this.props.keyname,temp)
                console.log('item.image',item.image)
                return (<View style={styles.cameraRollList}>

                  {
                    temp == 1?
                      <TouchableOpacity style={styles.buttonDelete} onPress={() => {
                        this.onDeleteItem(item.image)
                      }}>
                        <Image source={require('../../assets/image/icon_delete.png')} onPress={() => {
                          this.onDeleteItem(item.image)
                        }} />
                      </TouchableOpacity>
                      :<View/>
                  }
                 
                  <TouchableOpacity style={styles.cameraRollImgBorderGallary}
                    onPress={() => {
                      this.getbase64img(item.image)
                      
                    }}>
                    <View style={styles.cameraRollImgBorderGallary} accessible={true}>
                      <FastImage
                        style={styles.cameraRollImg}
                        source={{
                          uri: item.image.search('http') == 0 ? item.imagethumbnail + '?' + Math.random() : item.image,
                          priority: FastImage.priority.low,
                        }}
                        resizeMode={FastImage.resizeMode.cover}
                      />
                    </View>
                  </TouchableOpacity>
                </View>)
              } else {
                // console.log(' item.image', item)
                return (<View style={styles.cameraRollList}>
                  <TouchableOpacity style={styles.cameraRollImgBorderGallary} onPress={() => this.getbase64img(item.image)}>
                    <View style={styles.cameraRollImgBorderGallary}>
                      <FastImage
                        style={styles.cameraRollImg}
                        source={{
                          uri: item.image.search('http') == 0 ? item.imagethumbnail + '?' + Math.random() : item.image,
                          priority: FastImage.priority.low,
                        }}
                        resizeMode={FastImage.resizeMode.cover}
                      />

                    </View>
                  </TouchableOpacity>
                </View>)
              }
          }
        }} />

      <View style={styles.buttonFixed}>
        <View style={[styles.second, 
          // this.props.category == 3 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
        ]}>
          <TouchableOpacity style={styles.firstButton} onPress={this.onBackButton.bind(this)}>
            <Text style={styles.selectButton}>{I18n.t('mainpile.steeldetail.button.back')}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.secondButton, 
           this.state.Edit_Flag != 0 ?   (this.props.category == 3 || this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}) :({backgroundColor: "#BABABA"})
          ]} 
          onPress={this.removeIndex0.bind(this)}
          disabled={this.state.Edit_Flag == 0}>
            <Text style={styles.selectButton}>{I18n.t('mainpile.steeldetail.button.save')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>)
  }


  formatDate(date, type) {
    if(type != 'camera') {
      var day = moment.unix(date)
      var date2 = moment(day) 
      if(date== null||date== ''||date== 0 || moment(date2).isValid()==false){
        date2=  new Date()
      }
 
      return  moment(date2).format("DD-MM-YYYY HH:mm:ss")
    } else {
      return date
    }
  }
  playsound(){
    
    sound.play((success) => {
      if (success) {
        console.log('successfully finished playing') 
        // setTimeout(()=>{this.setState({ display: ''})},300)
        
      } else {
        console.log('playback failed due to audio decoding errors')
        // reset the player to its uninitialized state (android only)
        // this is the only option to recover after an error occured and use the player again
        sound.reset()
      }
    })
  }
  extention(filename){
    return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
  }
  downloadimage(url){
    // console.warn(url)
    if(url.split(":")[0] == 'https'){
      var date = new Date()
      var ext = this.extention(url)
      let filetype = ext[0]
      let dirs = RNFetchBlob.fs.dirs
     
      ext = "."+ext[0]
      let filename = Math.floor(date.getTime() + date.getSeconds() / 2)+ext
      const { config, fs } = RNFetchBlob
      const bundleid = DeviceInfo.getBundleId()
      let options = {
        // fileCache: true,
        
        // appendExt :filetype,
        // path: RNFS.ExternalDirectoryPath+ '/Pylon/'+Math.floor(date.getTime() + date.getSeconds() / 2)+ext,
        // path:  RNFS.ExternalDirectoryPath + '/data/' + bundleid +'/Download/' +filename,
        path : dirs.DCIMDir +'/Pylon/Download/'+filename,
        addAndroidDownloads : {
          notification : true,
          useDownloadManager: true,
          // title : filename,
          // mime : 'application/'+filetype,
          // path:  RNFS.ExternalDirectoryPath + '/Pylon/'+Math.floor(date.getTime() + date.getSeconds() / 2)+ext,
          // path:  RNFS.ExternalStorageDirectoryPath + '/Pylon/'+Math.floor(date.getTime() + date.getSeconds() / 2)+ext,
          // path:  RNFS.ExternalDirectoryPath + '/data/' + bundleid +'/Download/' +Math.floor(date.getTime() + date.getSeconds() / 2)+ext,
          // path:  RNFS.ExternalDirectoryPath + '/data/' + bundleid +'/Download/' +filename,
          path : dirs.DCIMDir  +'/Pylon/Download/'+ filename,
          description : 'Image',
          
        }
      }


      RNFetchBlob.config(options).fetch('GET', url).then((resp) => {
        console.log('resp.path()',resp.path())
        ToastAndroid.show('Success Downloaded', ToastAndroid.SHORT)
        }).catch((err) => {
          console.log('downloading error',err)
        })
    }else if(url.split(":")[0] == 'file'){
        let now = moment().format("DD-MM-YYYY HH:mm:ss")
        // BASE64
        const filepath = url.split('//')[1]
        RNFS.readFile(filepath, 'base64')
        .then(base => {
          var processTitle = ''
          if(this.props.process == 1) {
            processTitle = 'SteelCageSection'
          } else if(this.props.process == 2) {
            processTitle = 'TwoAxisVerticalCheck'
          } else if(this.props.process == 3) {
            if(this.props.keyname == 'Image_ReadingCoordinate') {
              processTitle = 'PileSurvey'
            } else {
              processTitle = 'PileSurveyDiff'
            }
          }
          // console.warn('BASE64: ',base)
          // this.saveImage(base, "camera", now)
          let extPath = RNFS.mkdir(RNFS.ExternalStorageDirectoryPath + '/Pylon/')
          const NEWPATH = RNFS.ExternalStorageDirectoryPath + '/Pylon/' + processTitle + '_' + this.props.jobid + '_' + this.props.pileId + '_' + this.props.process + '_' + this.state.photos.length + '' + Math.floor(Math.random() * (999 - 100 + 1) + 100) +'(download)'+ '.jpeg'
          RNFS.writeFile(NEWPATH, base, 'base64')
          .then(() => {
            ToastAndroid.show('Success Downloaded', ToastAndroid.SHORT)
          })
          
        }) 
      
    }else{
      // console.warn(url.split(":")[0])
    }
    
  
  }
  shareImage(url){
    // console.warn(url)

  }
  onCancel() {
    console.log("CANCEL")
    this.setState({visible:false})
  }
  onOpen() {
    console.log("OPEN")
    this.setState({visible:true})
  }
  renderPhotoLibrary() {
    let index = 0
    const data = [
      {
        key: index++,
        label: 'Take Photo',
        action: "camera"
      }, {
        key: index++,
        label: 'Choose from Library',
        action: "photoLibrary"
      }
    ]
    console.log(this.state.photosLib)
    return (<View style={styles.container}>
      <View style={[styles.navigation, 
        this.props.category==3|| this.props.category == 5?{backgroundColor:DEFAULT_COLOR_4}:this.props.shape == 1 ? styles.navigationBlue : { backgroundColor: MENU_ORANGE_BORDER }
      ]}>
        <View style={styles.navigationTitle}>
          <TouchableOpacity activeOpacity={0.8} style={styles.buttonRight} onPress={this.removeIndex0.bind(this)}>
            <Image source={require('../../assets/image/icon_cancel.png')} style={styles.buttonLeftResize} />
          </TouchableOpacity>
          <Text style={styles.navigationTitleText}>{I18n.t('mainpile.cameraRoll.Photo')}</Text>
        </View>
      </View>
      <FlatList style={styles.cameraRollRow}
        data={this.state.photosLib}
        numColumns={this.numColumns()}
        keyExtractor={item => item.image} renderItem={({ item }) => {
          return (<TouchableOpacity onPress={() => {
            this.savePhotos(item.image, 'gallery', item.timestamp)
          }}>
            <View style={styles.cameraRollImgBorder}>
              <FastImage
                style={styles.cameraRollImg}
                source={{
                  uri: item.image.search('http') == 0 ? item.image + '?' + Math.random() : item.image,
                  priority: FastImage.priority.low,
                }}
                resizeMode={FastImage.resizeMode.cover}
              />
            </View>
          </TouchableOpacity>)

        }} />
    </View>)
  }

  renderCamera() {
    return (
      <View style={{
        flex: 1,
        paddingTop: 10,
        backgroundColor: '#000',
      }}>
        <View style={styles.navigationTitle}>
          <TouchableOpacity activeOpacity={0.8} style={styles.buttonRight} onPress={() => this.setState({ display: '' })}>
            <Image source={require('../../assets/image/icon_cancel.png')} style={styles.buttonLeftResize} />
          </TouchableOpacity>
        </View>
        <RNCamera ref={(cam) => {
          this.camera = cam
        }}
          style={{ flex: 1 }}
          type={this.state.type}
          zoom={this.state.zoom}
          ration="4:3"
          flashMode={this.state.flash ? RNCamera.Constants.FlashMode.on : RNCamera.Constants.FlashMode.off}
          autoFocus={this.state.focus ? RNCamera.Constants.AutoFocus.on : RNCamera.Constants.AutoFocus.off}
        />
        <View style={{ alignItems: 'center', marginTop: 10, flexDirection: 'row' }}>
          <View style={{ width: '33.33%' }}>
            <Slider
            style={{ width: '100%', alignSelf: 'flex-end', backgroundColor: '#FFF' }}
            onValueChange={(zoom) => this.setState({zoom})}
            step={0.1}
          />
          </View>
          <View style={{ width: '33.33%', alignItems: 'center' }}>
            <TouchableOpacity activeOpacity={0.8} 
            onPress={ () => {
              
              this.onPhoto()
            }}>
              <Image source={require('../../assets/image/icon_snap.png')} />
            </TouchableOpacity>
          </View>
          <View style={{ width: '33.33%' }}>
          { this.state.flash ?
            <Icon
              reverse
              raised
              name="flash"
              type="material-community"
              color="#007CC2"
              size={20}
              onPress={() => this.setState({ flash: !this.state.flash })}
            />
            :
            <Icon
              reverse
              raised
              name="flash-off"
              type="material-community"
              color="#007CC2"
              size={20}
              onPress={() => this.setState({ flash: !this.state.flash })}
            />
          }
          </View>

        </View>
      </View>
    )
  }

  numColumns() {
    return 3
    var {
      height,
      width
    } = Dimensions.get('window')
    if(width < height) {
      if(width >= 360 && height >= 640) {
        return 3
      } else {
        return 3
      }
    } else {
      if(width >= 640 && height >= 360) {
        return 5
      } else {
        return 1
      }
    }
  }

  renderPreview() {
    const width = Dimensions.get("window").width
    const height = Dimensions.get("window").height
    let shareOptions = {
      title: "",
      message: "",
      url: "data:image/png;base64,"+this.state.base64img,
    }
    return (
      <View style={styles.container}>
        <View style={[styles.navigation, 
          this.props.category==3|| this.props.category == 5?{backgroundColor:DEFAULT_COLOR_4}:this.props.shape == 1 ? styles.navigationBlue : { backgroundColor: MENU_ORANGE_BORDER }
        ]}>
          <View style={styles.navigationTitle}>
            <TouchableOpacity activeOpacity={0.8} style={styles.buttonRight} onPress={() => this.setState({ display: '', loading: true })}>
              <Image source={require('../../assets/image/icon_cancel.png')} style={styles.buttonLeftResize} />
            </TouchableOpacity>
          
            <Text style={styles.navigationTitleText}>{I18n.t('mainpile.cameraRoll.Photo')}</Text>
          </View>
        </View>
      
      <View style={styles.previewContainer}>
        <View style={styles.preview}>
          <ImageZoom cropWidth={width}
            cropHeight={height}
            imageWidth={350}
            imageHeight={350}
            panToMove={"true"}
            pinchToZoom={"true"}
          >
            <FastImage
              style={styles.previewImg}
              source={{
                uri: this.state.image.search('http') == 0 ? this.state.image + '?' + Math.random() : this.state.image,
                priority: FastImage.priority.low,
              }}
              resizeMode={FastImage.resizeMode.contain}
            />
            <View style={styles.previewCropImg}></View>
            {/* {
            this.state.loading == true ?
            <View style={styles.previewCropImg}>
              <ActivityIndicator size="large" color="#007CC2" />
            </View>
            :
            <View/>
          } */}
        </ImageZoom>
        </View>
        {/* <Text style={styles.capture} onPress={() => this.onPreviewContinue()}>[Continue]</Text> */}
      </View>
      
      <View style={[styles.saveimage,{flexDirection:'row',backgroundColor:'rgba(0,0,0,0.9)'}]}>
        <TouchableOpacity style={[{flex:1,marginLeft:5,justifyContent:'center'}, 
          this.props.category==3|| this.props.category == 5?{backgroundColor:DEFAULT_COLOR_4}:this.props.shape == 1 ? { backgroundColor: MAIN_COLOR } : { backgroundColor: MENU_ORANGE_BORDER }]}
          onPress={()=>
            this.downloadimage(this.state.image.split("?")[0])
          }>
            <Text style={[styles.navigationTitleText,{textAlign:'center'}]}>{I18n.t('mainpile.cameraRoll.download')}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[{flex:1,marginLeft:5,marginRight:5,justifyContent:'center'}, 
          this.props.category==3|| this.props.category == 5?{backgroundColor:DEFAULT_COLOR_4}:this.props.shape == 1 ? { backgroundColor: MAIN_COLOR } : { backgroundColor: MENU_ORANGE_BORDER }]}
          onPress={()=>
            Share.open(shareOptions)
          }>
            <Text style={[styles.navigationTitleText,{textAlign:'center'}]}>{I18n.t('mainpile.cameraRoll.share')}</Text>
        </TouchableOpacity>
      </View>
     
    </View>)
  }

  render() {
    if(this.state.display == '') {
      return (this.renderPhotoSelect())
    } else if(this.state.display == 'camera') {
      return (this.renderCamera())
    } else if(this.state.display == 'photoLibrary') {
      return (this.renderPhotoLibrary())
    }
    else if(this.state.display == 'preview') {
      return (this.renderPreview())
    }
  }

}

const mapStateToProps = state => {
  return {
    location_lat: state.location.location_lat,
    location_log:state.location.location_log,
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(CameraRoll)
