import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity
} from "react-native"
import { connect } from "react-redux"
import { Container, Content } from "native-base"
import StepIndicator from "react-native-step-indicator"
import * as actions from "../Actions"
import Pile10_1 from "./Pile10_1"
import Pile10_2 from "./Pile10_2"
import Pile10_3 from "./Pile10_3"
import styles from "./styles/Pile8.style"
import { Actions } from "react-native-router-flux"
import I18n from "../../assets/languages/i18n"
import {
  MAIN_COLOR,
  DEFAULT_COLOR_1,
  DEFAULT_COLOR_4
} from "../Constants/Color"
class Pile10 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      step: 0,
      process: 10,
      Edit_Flag: 1,
      error: null,
      data: [],
      location: {
        position: {
          lat: 1,
          log: 1
        }
      },
      concrete: 0,
      statusCheck: false,
      concreterecord: null,
      status11: 0,
      status4: 0,
      checksavebutton: false,
      onPress: false,
      random10: null,
      Step10Status: 0,
      step_status: null,
      count: 0,
      stepstatus: null,
      Parentid: null,
      Parentno: null,
      Pileid: null,
      Parentcheck: false
    }
  }
  componentDidMount() {
    // console.log(this.props.stack)

    if (this.props.stack) {
      this.setState({
        step: this.props.stack[this.props.stack.length - 1].step - 1
      })
    }
  }
  async componentWillReceiveProps(nextProps) {
    // console.warn("confirm 4", nextProps.status)
    if (
      nextProps.step_status2_random != this.state.stepstatus &&
      nextProps.processstatus == 10
    ) {
      this.setState({ stepstatus: nextProps.step_status2_random }, async () => {
        console.warn("yes6", nextProps.step_status2.Step10Status)
        await this.props.mainPileSetStorage(this.props.pileid, "step_status", {
          Step10Status: nextProps.step_status2.Step10Status
        })
        this.props.mainPileGetStorage(this.props.pileid, "step_status")
      })
    }
    var pileMaster = null
    if (
      nextProps.pileAll[this.props.pileid] &&
      nextProps.pileAll[this.props.pileid]["9_1"]
    ) {
      pileMaster = nextProps.pileAll[this.props.pileid]["9_1"].masterInfo
    }

    if (pileMaster) {
      if (pileMaster.PileDetail) {
        // console.warn(pileMaster.PileDetail.pile_id,pileMaster.PileDetail.parent_id)
        this.setState({
          Parentid: pileMaster.PileDetail.parent_id,
          Parentno: pileMaster.PileDetail.parent_no,
          Pileid: pileMaster.PileDetail.pile_id,
          Parentcheck:
            pileMaster.PileDetail.pile_id == pileMaster.PileDetail.parent_id
              ? true
              : false
        })
      }
      if (pileMaster.ConcreteInfo) {
        this.setState({
          concrete: pileMaster.ConcreteInfo.ConcreteBom || 0
        })
      }
      if (nextProps.pileAll[this.props.pileid] != undefined) {
        var edit = nextProps.pileAll[this.props.pileid]
        if (edit != undefined) {
          this.setState({
            status11: edit.step_status.Step11Status,
            status4: edit.step_status.Step4Status
          })
        }
        if (edit["10_0"] != undefined) {
          await this.setState({ Edit_Flag: edit["10_0"].data.Edit_Flag })
        }
      }
      if (nextProps.concreterecord && this.state.statusCheck == true) {
        this.setState({ concreterecord: nextProps.concreterecord })
      }
    }
    if (
      nextProps.pile10 == true &&
      this.state.random10 != nextProps.random10 &&
      this.state.onPress == true
    ) {
      console.log("nextProps.save10page ", nextProps.save10page)
      if (nextProps.save10page == 1) {
        // console.warn(nextProps.random11)
        this.setState({ random10: nextProps.random10, step: 1, onPress: false })
        // if (this.state.Step10Status == 2) {
        //   await this.props.mainPileSetStorage(
        //     this.props.pileid,
        //     "step_status",
        //     { Step10Status: 2 }
        //   )
        // } else {
        //   await this.props.mainPileSetStorage(
        //     this.props.pileid,
        //     "step_status",
        //     { Step10Status: 1 }
        //   )
        // }

        // this.props.mainPileGetStorage(this.props.pileid, "step_status")
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process: 10
        })
        this.onSetStack(10, 0)
        setTimeout(() => {
          this.setState({ checksavebutton: false })
        }, 2000)
      } else if (nextProps.save10page == 2) {
        this.setState({ random10: nextProps.random10, step: 2, onPress: false })

        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process: 10
        })

        this.onSetStack(10, 1)
        setTimeout(() => {
          this.setState({ checksavebutton: false })
        }, 2000)
      } else if (nextProps.save10page == 3) {
        console.log("save10page 10_3", nextProps.random10)

        this.setState(
          {
            random10: nextProps.random10,
            onPress: false,
            statusCheck: true
          },
          async () => {
            await this.props.getStepStatus({
              pileid: this.props.pileid,
              jobid: this.props.jobid
            })
          }
        )
      }
    }
    if (nextProps.confirm == true && this.state.count < 1) {
      this.setState({ count: this.state.count + 1 })
      if (this.props.shape == 1) {
        console.warn("confirm", nextProps.confirm)
        this.setState(
          {
            concreterecord: null
            // step: 3
          },
          async () => {
            var valueApi3 = {
              Language: I18n.currentLocale(),
              JobId: this.props.jobid,
              PileId: this.props.pileid
            }
            await this.props
              .pileSaveStep10_3confirmToNext(valueApi3)
              .then(response => {
                if (response.status == true) {
                  this.props.onNextStep()
                  this.onSetStack(11, 0)
                } else {
                  Alert.alert(
                    "ERROR",
                    response.Message,
                    [
                      {
                        text: "OK"
                      }
                    ],
                    { cancelable: false }
                  )
                  // this.onSetStack(10, 0)
                }
              })
            setTimeout(() => {
              this.setState({ checksavebutton: false })
            }, 2000)
          }
        )
      } else {
        // this.setState({ step_status: nextProps.step_status,statusCheck:false })
        var value = {
          process: 4,
          step: 1,
          pile_id: this.props.pileid,
          drillingFluids: this.props.drillingfluidslist,
          shape: this.props.shape
        }

        this.props.mainpileAction_checkStepStatus(value).then(async data => {
          console.warn("swet1", data)
          if (data.check4_success != true) {
            Alert.alert("", I18n.t("mainpile.11_3.process4notyet"), [
              {
                text: "OK"
              }
            ])
            this.setState({ checksavebutton: false })
          } else {
            Alert.alert(
              I18n.t("mainpile.10_3.finish"),
              I18n.t("mainpile.10_3.backtopile"),
              [
                { text: "Cancel" },

                {
                  text: "OK",
                  onPress: () =>
                    Actions.projectlist({
                      result: ""
                    })
                }
              ],
              { cancelable: false }
            )
            // this.setState({ step: 3 })
            // this.onSetStack(10, 2)
            setTimeout(() => {
              this.setState({ checksavebutton: false })
            }, 2000)
          }
        })
      }
    }
    // console.log("step_status 10_3",nextProps.step_status)
    // if (
    //   nextProps.step_status != this.state.step_status &&
    //   this.state.statusCheck == true
    // ) {
    // if(this.state.count<2){
    //   this.setState({count:this.state.count+1})
    // }else{
    // setTimeout(()=>{

    // this.setState({ step_status: nextProps.step_status.Step10Status,statusCheck:false },()=>{
    //   console.warn("step_status 10_3", nextProps.step_status)
    //   if (nextProps.step_status.Step10Status == 2) {
    //     if (this.props.shape == 1) {
    //       this.setState(
    //         {
    //           concreterecord: null
    //         },
    //         () => {
    //           this.props.onNextStep()
    //           this.onSetStack(11, 0)
    //           setTimeout(() => {
    //             this.setState({ checksavebutton: false })
    //           }, 2000)
    //         }
    //       )
    //     } else {
    //       // this.setState({ step_status: nextProps.step_status,statusCheck:false })
    //       var value = {
    //         process: 4,
    //         step: 1,
    //         pile_id: this.props.pileid,
    //         drillingFluids: this.props.drillingfluidslist,
    //         shape: this.props.shape
    //       }

    //       this.props.mainpileAction_checkStepStatus(value).then(async data => {
    //         console.warn("swet1", data)
    //         if (data.check4_success != true) {
    //           Alert.alert("", I18n.t("mainpile.11_3.process4notyet"), [
    //             {
    //               text: "OK"
    //             }
    //           ])
    //           this.setState({ checksavebutton: false })
    //         } else {
    //           Alert.alert(
    //             I18n.t("mainpile.10_3.finish"),
    //             I18n.t("mainpile.10_3.backtopile"),
    //             [
    //               { text: "Cancel" },

    //               {
    //                 text: "OK",
    //                 onPress: () =>
    //                   Actions.projectlist({
    //                     result: ""
    //                   })
    //               }
    //             ],
    //             { cancelable: false }
    //           )
    //           // this.setState({ step: 3 })
    //           // this.onSetStack(10, 2)
    //           setTimeout(() => {
    //             this.setState({ checksavebutton: false })
    //           }, 2000)
    //         }
    //       })
    //     }
    //   } else {
    //     this.setState({ step_status: nextProps.step_status,statusCheck:false })
    //     Alert.alert("test2", I18n.t("mainpile.8_1.error_nextstep"), [
    //       {
    //         text: "OK"
    //       }
    //     ])
    //     //       // }
    //     //       //   })
    //     //       // }
    //     //     })
    //   }
    // })
    // },2000)
    // }

    // }

    await this.setState({
      error: nextProps.error,
      data: nextProps.pileAll[nextProps.pileid],
      location: nextProps.pileAll.currentLocation,
      checksavebutton: false
    })
  }

  async onSetStack(pro, step) {
    var data = {
      process: pro,
      step: step + 1
    }
    await this.props.setStack(data)
    await this.props.mainPileGetAllStorage()
  }

  nextButton = () => {
    if (
      (this.props.category == 3 || this.props.category == 5) &&
      this.state.Parentcheck == false
    ) {
      Alert.alert(
        "",
        I18n.t("alert.errorParentcheck10") + this.state.Parentno,
        [
          {
            text: "OK"
          }
        ],
        { cancelable: false }
      )
      return
    }
    switch (this.state.step) {
      case 0:
        this.onSave(this.state.step)
        break
      case 1:
        this.onSave(this.state.step)
        break
      case 2:
        this.onSave(this.state.step)
        break
      default:
        break
    }
  }

  async onSave(step) {
    // var statuscolor = 1
    // if (this.props.concretelist.length > 0) {
    //   statuscolor = 2
    // }

    // await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step9Status: statuscolor })

    // this.props.mainPileGetStorage(this.props.pileid, "step_status")

    // if (statuscolor == 2) {
    //   this.props.onNextStep()
    //   this.onSetStack(1)
    // } else {
    //   Alert.alert("", I18n.t("mainpile.8_1.error_nextstep"), [
    //     {
    //       text: "OK"
    //     }
    //   ])
    // }

    this.setState({ checksavebutton: true })
    await this.props.mainPileGetAllStorage()
    if (step == 0) {
      var value = {
        process: this.state.process,
        step: 1,
        pile_id: this.props.pileid,
        category: this.props.category
      }
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value).then(async data => {
          var valueApi = {
            Language: I18n.currentLocale(),
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            Page: 1,
            foremanid: this.state.data["10_1"].data.ForemanId,
            ForemanName: this.state.data["10_1"].data.ForemanName,
            foremansetid:
              this.state.data["10_1"].data.ForemanSet != undefined ||
              this.state.data["10_1"].data.ForemanSet != null
                ? this.state.data["10_1"].data.ForemanSet.Id
                : null,
            ForemanSetName:
              this.state.data["10_1"].data.ForemanSet != undefined ||
              this.state.data["10_1"].data.ForemanSet != null
                ? this.state.data["10_1"].data.ForemanSet.Foreman
                : null,
            Overcast:
              this.state.data["10_1"].data.Overcast != undefined ||
              this.state.data["10_1"].data.Overcast != null
                ? this.state.data["10_1"].data.Overcast
                : null,
            latitude:
              this.state.location != undefined
                ? this.state.location.position.lat
                : 1,
            longitude:
              this.state.location != undefined
                ? this.state.location.position.log
                : 1
          }

          console.log("foremansetid valueApi", valueApi)

          if (this.state.error == null) {
            if (data.check == false) {
              this.setState({ checksavebutton: false }, () => {
                Alert.alert("", data.errorText[0], [
                  {
                    text: "OK"
                  }
                ])
              })
            } else {
              this.setState(
                { onPress: true, Step10Status: data.statuscolor },
                async () => {
                  await this.props.pileSaveStep10(valueApi)
                }
              )
            }
          } else {
            Alert.alert(this.state.error)
            this.setState({ checksavebutton: false })
          }
        })
      }
    } else if (step == 1) {
      var value = {
        process: this.state.process,
        step: 2,
        pile_id: this.props.pileid
      }
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value).then(async data => {
          // await this.props.mainPileSetStorage(
          //   this.props.pileid,
          //   "step_status",
          //   { Step10Status: data.statuscolor }
          // )
          // this.props.mainPileGetStorage(this.props.pileid, "step_status")

          var valueApi = {
            Language: I18n.currentLocale(),
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            Page: 2,
            Weather: this.state.data["10_2"].data.Weather,
            IsFine: this.state.data["10_2"].data.IsFine,
            IsCloudy: this.state.data["10_2"].data.IsCloudy,
            IsRainy: this.state.data["10_2"].data.IsRainy,
            IsHail: this.state.data["10_2"].data.IsHail,
            IsWindy: this.state.data["10_2"].data.IsWindy,
            latitude:
              this.state.location != undefined
                ? this.state.location.position.lat
                : 1,
            longitude:
              this.state.location != undefined
                ? this.state.location.position.log
                : 1
          }
          console.log("Weather step10_2", valueApi)

          if (this.state.error == null) {
            if (data.check == false) {
              Alert.alert("", data.errorText[0], [
                {
                  text: "OK"
                }
              ])
              this.setState({ checksavebutton: false })
            } else {
              this.setState(
                { onPress: true, Step10Status: data.statuscolor },
                async () => {
                  await this.props.pileSaveStep10(valueApi)
                }
              )
            }
          } else {
            Alert.alert(this.state.error)
            this.setState({ checksavebutton: false })
          }
        })
      }
    } else if (step == 2) {
      // var statuscolor = this.props.status
      var go = I18n.t("alert.concretetruckyes")
      var stop = I18n.t("alert.concretetruckno")
      var conti = this.props.concretetruck.length
      // console.warn(
      //   this.props.concretetruck.length,
      //   this.props.concreterecord.length
      // )
      // console.warn("statuscolor", statuscolor)
      var value = {
        process: this.state.process,
        step: 3,
        pile_id: this.props.pileid
      }
      // console.warn(
      //   "concretecumulativeArr",this.props.concreterecord[this.props.concreterecord.length-1].IsLastTruck==true
      // )
      // this.props.getConcreteList10({
      //   language: I18n.currentLocale(),
      //   pileid: this.props.pileid,
      //   jobid: this.props.jobid
      // })
      // console.warn(this.state.trucklength)
      // console.log
      // await statetorage(this.props.pileid, "step_status", { Step10Status: statuscolor })

      // this.props.mainPileGetStorage(this.props.pileid, "step_status")
      this.props.mainpileAction_checkStepStatus(value).then(async data => {
        console.warn("checkstep10 : ", data, data.check)
        if (this.state.error == null) {
          if (data.check == false) {
            Alert.alert("", data.errorText[0], [
              {
                text: "OK"
              }
            ])
            this.setState({ checksavebutton: false })
          } else if (this.props.concreterecord.length != 0) {
            if (
              this.props.concreterecord[this.props.concreterecord.length - 1]
                .IsLastTruck == true &&
              this.props.concretecumulativeArr[
                this.props.concretecumulativeArr.length - 1
              ] < this.state.concrete &&
              conti == 0
            ) {
              Alert.alert(
                "",
                I18n.t("alert.errorconcrete"),
                [
                  {
                    text: I18n.t("alert.errorconcreteno"),
                    onPress: async () => {
                      var valueApi2 = {
                        Language: I18n.currentLocale(),
                        JobId: this.props.jobid,
                        PileId: this.props.pileid,
                        confirm: true
                      }

                      this.setState({ onPress: true, count: 0 }, async () => {
                        await this.props.pileSaveStep10_3confirm(valueApi2)
                      })
                    }
                  },
                  {
                    text: I18n.t("alert.errorconcreteyes"),
                    onPress: async () => {
                      var valueApi2 = {
                        Language: I18n.currentLocale(),
                        JobId: this.props.jobid,
                        PileId: this.props.pileid,
                        confirm: false
                      }

                      await this.props
                        .pileSaveStep10_3confirm(valueApi2)
                        .then(async () => {
                          setTimeout(async () => {
                            await this.props.getStepStatus({
                              pileid: this.props.pileid,
                              jobid: this.props.jobid
                            })
                          }, 1000)
                          this.setState({ checksavebutton: false })
                        })
                    }
                  }
                ],
                { cancelable: false }
              )
            } else {
              if (
                conti != 0 &&
                this.props.concreterecord[this.props.concreterecord.length - 1]
                  .IsLastTruck == true
              ) {
                Alert.alert(
                  "",
                  I18n.t("alert.concretetrucklength"),
                  [
                    {
                      text: go,
                      onPress: async () => {
                        var valueApi2 = {
                          Language: I18n.currentLocale(),
                          JobId: this.props.jobid,
                          PileId: this.props.pileid,
                          confirm: true
                        }

                        this.setState({ onPress: true, count: 0 }, async () => {
                          await this.props.pileSaveStep10_3confirm(valueApi2)
                        })
                      }
                    },

                    {
                      text: stop,
                      onPress: async () => {
                        var valueApi2 = {
                          Language: I18n.currentLocale(),
                          JobId: this.props.jobid,
                          PileId: this.props.pileid,
                          confirm: false
                        }
                        // console.warn("confirm")

                        await this.props
                          .pileSaveStep10_3confirm(valueApi2)
                          .then(async () => {
                            setTimeout(async () => {
                              await this.props.getStepStatus({
                                pileid: this.props.pileid,
                                jobid: this.props.jobid
                              })
                            }, 1000)
                          })
                        this.setState({ checksavebutton: false })
                      }
                    }
                  ],
                  { cancelable: false }
                )
              } else {
                if (
                  this.props.concreterecord[
                    this.props.concreterecord.length - 1
                  ].IsLastTruck != true
                ) {
                  Alert.alert("", I18n.t("alert.error10_3_last"), [
                    {
                      text: "OK"
                    }
                  ])
                  this.setState({ checksavebutton: false })
                  return
                }
                this.props.getStepStatus({
                  pileid: this.props.pileid,
                  jobid: this.props.jobid
                })

                // if (statuscolor == 2) {
                if (this.props.shape == 1) {
                  var valueApi3 = {
                    Language: I18n.currentLocale(),
                    JobId: this.props.jobid,
                    PileId: this.props.pileid
                  }
                  await this.props
                    .pileSaveStep10_3confirmToNext(valueApi3)
                    .then(response => {
                      if (response.status == true) {
                        this.props.onNextStep()
                        this.onSetStack(11, 0)
                      } else {
                        Alert.alert(
                          "ERROR",
                          response.Message,
                          [
                            {
                              text: "OK"
                            }
                          ],
                          { cancelable: false }
                        )
                        this.onSetStack(10, 0)
                      }
                    })

                  setTimeout(() => {
                    this.setState({ checksavebutton: false })
                  }, 2000)
                } else {
                  var value = {
                    process: 4,
                    step: 1,
                    pile_id: this.props.pileid,
                    drillingFluids: this.props.drillingfluidslist,
                    shape: this.props.shape
                  }
                  // let check4=true
                  this.props
                    .mainpileAction_checkStepStatus(value)
                    .then(async data => {
                      console.warn("swet3", data)
                      if (data.check4_success != true) {
                        // console.warn("data.statuscolor", data.statuscolor)

                        Alert.alert(
                          "",
                          I18n.t("mainpile.11_3.process4notyet"),
                          [
                            {
                              text: "OK"
                            }
                          ]
                        )
                        this.setState({ checksavebutton: false })
                      } else {
                        // console.warn("data.ทำงานเสร็จสิ้น", data.statuscolor)
                        Alert.alert(
                          I18n.t("mainpile.10_3.finish"),
                          I18n.t("mainpile.10_3.backtopile"),
                          [
                            { text: "Cancel" },
                            // { text: "OK", onPress: () => Actions.projectlist({title: "", project: this.props.project, jobid: this.props.jobid}) }result: ''
                            {
                              text: "OK",
                              onPress: () => Actions.projectlist({ result: "" })
                            }
                          ],
                          { cancelable: false }
                        )
                        this.setState({ step: 3 })
                        this.onSetStack(10, 2)
                        setTimeout(() => {
                          this.setState({ checksavebutton: false })
                        }, 2000)
                      }
                    })
                  // Alert.alert(I18n.t('mainpile.10_3.finish'), I18n.t('mainpile.10_3.backtopile'), [
                  //   { text: 'Cancel' },
                  //   { text: "OK", onPress: () => Actions.projectlist({title: "", project: this.props.project, jobid: this.props.jobid}) }
                  // ])
                  // this.setState({ step: 3 })
                  // this.onSetStack(10,2)
                }
              }
              //   else {
              //     Alert.alert("error1", I18n.t("mainpile.8_1.error_nextstep"), [
              //       {
              //         text: "OK"
              //       }
              //     ])
              //     this.setState({ checksavebutton: false })
              //   }
              // }
            }
          } else {
            Alert.alert("", I18n.t("alert.error10_3_last"), [
              {
                text: "OK"
              }
            ])
            this.setState({ checksavebutton: false })
          }
        } else {
          Alert.alert(this.state.error)
          this.setState({ checksavebutton: false })
        }
      })
    }
  }

  deleteButton = () => {
    if (
      (this.props.category == 3 || this.props.category == 5) &&
      this.state.Parentcheck == false
    ) {
      Alert.alert(
        "",
        I18n.t("alert.errorParentcheck10") + this.state.Parentno,
        [
          {
            text: "OK"
          }
        ],
        { cancelable: false }
      )
      return
    }
    this.props.setStack({
      process: 10,
      step: 1
    })
    const data = {
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      processid: this.state.process,
      lat:
        this.state.location != undefined ? this.state.location.position.lat : 1,
      log:
        this.state.location != undefined ? this.state.location.position.log : 1
    }
    if (this.state.status11 == 0) {
      Alert.alert("Warning", I18n.t("mainpile.button.confirmdelete"), [
        { text: "Cancel" },
        { text: "OK", onPress: () => this.props.pileDelete(data) }
      ])
    } else {
      Alert.alert("", I18n.t("alert.error_delete10"), [
        {
          text: "OK"
        }
      ])
    }
  }

  _renderStepFooter() {
    return (
      <View>
        <View style={{ flexDirection: "column" }}>
          <View style={{ borderWidth: 1 }}>
            <StepIndicator
              stepCount={3}
              onPress={step => {
                this.setState({ step: step })
                this.onSetStack(10, step)
              }}
              currentPosition={this.state.step}
            />
          </View>
          {this.state.Edit_Flag == 1 ? (
            <View style={styles.second}>
              <TouchableOpacity
                style={styles.firstButton}
                onPress={this.deleteButton}
                disabled={
                  this.state.checksavebutton ||
                  ((this.props.category == 3 || this.props.category == 5) &&
                    this.state.Parentcheck == false)
                }
              >
                <Text style={styles.selectButton}>
                  {I18n.t("mainpile.button.delete")}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.secondButton,
                  this.props.category == 3 || this.props.category == 5
                    ? this.state.Parentcheck == false
                      ? {
                          backgroundColor: "#BABABA"
                        }
                      : {
                          backgroundColor: DEFAULT_COLOR_4
                        }
                    : this.props.shape == 1
                    ? { backgroundColor: MAIN_COLOR }
                    : { backgroundColor: DEFAULT_COLOR_1 }
                ]}
                onPress={this.nextButton}
                disabled={
                  this.state.checksavebutton ||
                  ((this.props.category == 3 || this.props.category == 5) &&
                    this.state.Parentcheck == false)
                }
              >
                <Text style={styles.selectButton}>
                  {I18n.t("mainpile.button.next")}
                </Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View />
          )}
        </View>
      </View>
    )
  }

  updateState(value) {
    const ref = "pile" + value.process + "_" + value.step
    if (this.refs[ref].getWrappedInstance()) {
      this.refs[ref].getWrappedInstance().updateState(value)
    }
  }
  initialStep() {
    this.setState({ step: 0 })
  }

  render() {
    if (this.props.hidden) {
      return null
    }

    return (
      <View style={{ flex: 1 }}>
        <Content>
          <Pile10_1
            ref="pile10_1"
            hidden={!(this.state.step == 0)}
            jobid={this.props.jobid}
            pileid={this.props.pileid}
            shape={this.props.shape}
            category={this.props.category}
            onRefresh={this.props.onRefresh}
          />
          <Pile10_2
            ref="pile10_2"
            hidden={!(this.state.step == 1)}
            jobid={this.props.jobid}
            pileid={this.props.pileid}
            shape={this.props.shape}
          />
          <Pile10_3
            ref="pile10_3"
            hidden={!(this.state.step == 2)}
            jobid={this.props.jobid}
            pileid={this.props.pileid}
            shape={this.props.shape}
            category={this.props.category}
          />
        </Content>
        {this._renderStepFooter()}
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    error: state.pile.error,
    pileAll: state.mainpile.pileAll || null,
    status: state.concrete.status10,
    stack: state.mainpile.stack_item,
    drillingfluidslist: state.drillingFluids.drillingfluidslist,
    lasttruck: state.concrete.lasttruck,
    concretetruck: state.concrete.concretelist10,
    concreterecord: state.concrete.concreterecord,
    concretelist: state.concrete.concretelist,
    concretecumulativeArr: state.concrete.concretecumulativeArr,
    random10: state.pile.random10,
    pile10: state.pile.pile10,
    save10page: state.pile.save10page,
    step_status: state.mainpile.step_status,
    confirm: state.pile.confirm,

    step_status2_random: state.mainpile.step_status2_random,
    step_status2: state.mainpile.step_status2,
    processstatus: state.mainpile.process
  }
}

export default connect(mapStateToProps, actions, null, { withRef: true })(
  Pile10
)
