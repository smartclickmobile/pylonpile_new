import React, { Component } from "react"
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Image,
  ActivityIndicator,
  Alert
} from "react-native"
import ModalSelector from "react-native-modal-selector"
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM } from "../Constants/Color"
import { Icon } from "react-native-elements"
import * as actions from "../Actions"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import I18n from "../../assets/languages/i18n"
import styles from "./styles/Pile9_1.style"
import moment from "moment"
import naturalSort from "javascript-natural-sort"
import { DEFAULT_COLOR_4 } from "../Constants/Color"
class Pile9_1 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      concrete: 0,
      concretecumulative: 0,
      process: 9,
      step: 1,
      location: {
        position: {
          lat: 1,
          log: 1
        }
      },
      trucklist: ["5"],
      slumpmin: 0,
      slumpmax: 0,
      loading: false,
      Edit_Flag: 1,
      sort: 1,
      concreterecord: [],
      Parentid: null,
      Parentno: null,
      Pileid: null,
      Parentcheck: false
    }
  }

  updateState(value) {}

  componentDidMount() {
    console.log("pileAll", this.props.pileAll)
    this.props.getConcreteList({
      pileid: this.props.pileid,
      jobid: this.props.jobid
    })
    this.props.getConcreteRecord({
      pileid: this.props.pileid,
      jobid: this.props.jobid
    })
    if (this.props.onRefresh === "refresh") {
      Actions.refresh({
        action: "start"
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    var pile = null
    var pileMaster = null
    var pile9 = null
    if (
      nextProps.pileAll[this.props.pileid] &&
      nextProps.pileAll[this.props.pileid]["9_1"]
    ) {
      pile = nextProps.pileAll[this.props.pileid]["9_1"].data
      pileMaster = nextProps.pileAll[this.props.pileid]["9_1"].masterInfo
      pile9 = nextProps.pileAll[this.props.pileid]["9_0"].data
      // console.log(nextProps.pileAll)
      console.log("pileMaster_", pileMaster)
    }

    if (nextProps.pileAll != undefined && nextProps.pileAll != "") {
      this.setState({ location: nextProps.pileAll.currentLocation })
    }

    if (pile) {
      console.log("data step 9 : ", pile)
      if (pile.ConcreteTruckRegisterList) {
        // console.warn('DATA FROM API !')
        // this.props.pileClear()
        // console.warn('CLEAR')
        this.setState(
          { trucklist: pile.ConcreteTruckRegisterList, loading: false },
          () => this.calculateValue()
        )
      }
    }

    if (pileMaster) {
      if (pileMaster.PileDetail) {
        this.setState({
          Parentid: pileMaster.PileDetail.parent_id,
          Parentno: pileMaster.PileDetail.parent_no,
          Pileid: pileMaster.PileDetail.pile_id,
          Parentcheck:
            pileMaster.PileDetail.pile_id == pileMaster.PileDetail.parent_id
              ? true
              : false
        })
      }

      if (pileMaster.ConcreteInfo) {
        this.setState({
          slumpmin: pileMaster.ConcreteInfo.SlumpMin || 0,
          slumpmax: pileMaster.ConcreteInfo.SlumpMax || 0,
          concrete: pileMaster.ConcreteInfo.ConcreteBom || 0,
          loading: false
        })
      }
    }

    if (pile9) {
      this.setState({ Edit_Flag: pile9.Edit_Flag })
    }
    if (nextProps.concreterecord != undefined) {
      this.setState({ concreterecord: nextProps.concreterecord }, () => {
        console.log("nextProps.concreterecord", nextProps.concreterecord)
      })
    }

    // if (nextProps.pile9success == true) {
    //   // console.warn('get value')
    //   this.props.pileValueInfo({ jobid: this.props.jobid, pileid: this.props.pileid })
    // }
  }

  calculateValue() {
    var volume = 0
    this.state.trucklist.map((data, index) => {
      if (data.TruckConcreteVolume) {
        volume += data.TruckConcreteVolume
      }
    })
    this.setState({ concretecumulative: volume })
  }

  saveLocal() {
    var value = {
      process: 9,
      step: 1,
      pile_id: this.props.pileid,
      data: {
        DriverName: this.state.driver,
        DriverId: this.state.driverid,
        MachineName: this.state.machine,
        MachineId: this.state.machineid
      }
    }
    this.props.mainPileSetStorage(this.props.pileid, "9_1", value)
  }

  regisButton() {
    console.warn(
      "category+Parentcheck",
      this.props.category,
      this.state.Parentcheck
    )

    Actions.concreteregister({
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      process: this.state.process,
      step: this.state.step,
      Edit_Flag: this.state.Edit_Flag,
      shape: this.props.shape,
      lat:
        this.state.location == undefined ? 1 : this.state.location.position.lat,
      log:
        this.state.location == undefined ? 1 : this.state.location.position.log,
      concretecumulative: this.props.concretevolume,
      index: this.props.concretelist.length + 1,
      concreteid: this.props.concreteid,
      mixid: this.props.mixid,
      category: this.props.category
    })
  }

  onEditData(id, index, ApproveStatus) {
    let check10 = false
    if (this.state.concreterecord.length != 0) {
      this.state.concreterecord.map(data => {
        console.log(
          "check10concreterecord",
          data.ConcreteTruckRegister.Id,
          this.props.concretelist[index].Id
        )
        if (
          data.ConcreteTruckRegister.Id == this.props.concretelist[index].Id
        ) {
          check10 = true
        }
      })
    }
    console.log("check10", check10)
    Actions.concreteregister({
      data: this.props.concretelist[index],
      edit: true,
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      process: this.state.process,
      step: this.state.step,
      Edit_Flag: "1",
      shape: this.props.shape,
      lat:
        this.state.location == undefined ? 1 : this.state.location.position.lat,
      log:
        this.state.location == undefined ? 1 : this.state.location.position.log,
      concretecumulative: this.props.concretevolume,
      index: index + 1,
      appovestatus: ApproveStatus,
      category: this.props.category,
      check10: check10
    })
  }
  onViewData(id, index, ApproveStatus) {
    Actions.concreteregister({
      data: this.props.concretelist[index],
      edit: true,
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      process: this.state.process,
      step: this.state.step,
      Edit_Flag: "0",
      shape: this.props.shape,
      lat:
        this.state.location == undefined ? 1 : this.state.location.position.lat,
      log:
        this.state.location == undefined ? 1 : this.state.location.position.log,
      concretecumulative: this.props.concretevolume,
      index: index + 1,
      appovestatus: ApproveStatus,
      category: this.props.category
    })
  }

  deleteRow(key, index) {
    Alert.alert(
      "",
      I18n.t("mainpile.9_1.confirmdelect"),
      [
        {
          text: "Cancel"
        },
        {
          text: "OK",
          onPress: () => {
            this.props.removeConcreteList({
              Language: I18n.currentLocale(),
              JobId: this.props.jobid,
              PileId: this.props.pileid,
              TruckRegisterId: key,
              latitude:
                this.state.location != undefined
                  ? this.state.location.position.lat
                  : 1,
              longitude:
                this.state.location != undefined
                  ? this.state.location.position.log
                  : 1
            })
          }
        }
      ],
      { cancelable: false }
    )
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    return (
      <View style={styles.listContainer}>
        <View style={styles.listTopic}>
          <Text
            style={[
              styles.listTopicText,
              { fontSize: I18n.locale == "th" ? 20 : 18 }
            ]}
          >
            {I18n.t("mainpile.9_1.tableregister")}
          </Text>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => this.regisButton()}
            disabled={
              this.state.disabled ||
              this.state.Edit_Flag == 0 ||
              ((this.props.category == 3||this.props.category == 5 )&& this.state.Parentcheck == false)
            }
          >
            <View
              style={[
                styles.listButton,
                (this.props.disabled ||
                  this.state.Edit_Flag == 0 ||
                  ((this.props.category == 3|| this.props.category == 5) &&
                    this.state.Parentcheck == false)) && {
                  backgroundColor: MENU_GREY_ITEM
                }
              ]}
            >
              <Image source={require("../../assets/image/icon_add.png")} />
              <Text style={styles.listButtonText}>
                {I18n.t("mainpile.9_1.register")}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={[styles.listTopic, { marginTop: 10 }]}>
          <Text
            style={[
              styles.listTopicText,
              { color: "black", fontSize: I18n.locale == "th" ? 20 : 16 }
            ]}
          >
            {I18n.t("mainpile.concreteregister.planconcrete")}
          </Text>
          <View style={styles.listButtonLength}>
            <Text style={styles.listButtonTextLength}>
              {this.state.concrete.toFixed(2)}
            </Text>
          </View>
        </View>
        <View style={[styles.listTopic, { marginTop: 10 }]}>
          <Text
            style={[
              styles.listTopicText,
              { color: "black", fontSize: I18n.locale == "th" ? 20 : 16 }
            ]}
          >
            {I18n.t("mainpile.9_1.concretecol")}
          </Text>
          <View style={styles.listButtonLength}>
            <Text style={styles.listButtonTextLength}>
              {this.props.concretevolume.toFixed(2)}
            </Text>
          </View>
        </View>
        {this.props.concretelist.length > 0 ? (
          <View>
            <View
              style={{
                marginTop: 10,
                marginBottom: 10,
                alignItems: "center",
                flexDirection: "row",
                justifyContent: "center"
              }}
            >
              <Icon
                raised
                reverse
                type="font-awesome"
                color="#007CC2"
                name={
                  this.state.sort == 3
                    ? "sort-alpha-asc"
                    : this.state.sort == 1
                      ? "sort-numeric-asc"
                      : "sort-numeric-desc"
                }
                onPress={() => {
                  if (this.state.sort == 1) {
                    this.setState({ sort: 2 })
                  } else if (this.state.sort == 2) {
                    this.setState({ sort: 3 })
                  } else {
                    this.setState({ sort: 1 })
                  }
                }}
                size={30}
              />
            </View>
            {this.renderTable()}
          </View>
        ) : (
          <View style={styles.listTableDetail}>
            <Text style={styles.listTableDetailText}>
              {I18n.t("mainpile.9_1.no_data")}
              {I18n.locale == "th" ? "\n" : ""}
              {I18n.locale == "th" ? (
                <Text style={styles.textHighligh}>
                  {I18n.t("mainpile.9_1.add_data")}
                </Text>
              ) : (
                ""
              )}
              {I18n.locale == "th" ? I18n.t("mainpile.9_1.add_data_test") : ""}
            </Text>
          </View>
        )}
      </View>
    )
  }

  renderTable() {
    let data = null
    let Parent =
      (this.props.category == 3||this.props.category == 5)
        ? this.state.Parentcheck == true
          ? true
          : false
        : true
    console.warn(Parent)
    if (this.state.Edit_Flag != 0 && Parent == true) {
      data = [
        {
          key: 0,
          section: true,
          label: I18n.t("mainpile.9_1.selector"),
          action: "head"
        },
        {
          key: 1,
          label: I18n.t("mainpile.9_1.edit"),
          action: "edit"
        },
        {
          key: 2,
          label: I18n.t("mainpile.9_1.delete"),
          action: "delete"
        }
      ]
    } else {
      data = [
        {
          key: 0,
          section: true,
          label: I18n.t("mainpile.9_1.selector"),
          action: "head"
        },
        {
          key: 1,
          label: I18n.t("mainpile.9_1.see"),
          action: "view"
        }
      ]
    }

    const row1 = 120
    const row2 = 120
    const row3 = 100
    const row4 = 150
    const row5 = 90
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, justifyContent: "center" }}>
          <ActivityIndicator size="large" color="#007CC2" />
        </View>
      )
    }
    var concretelist = this.props.concretelist.slice(0)
    var indexcount = this.props.indexcount
    // True mean sort by name
    // False mean sort by number# so it doesn't need to be sort
    if (this.state.sort == 3) {
      var concretetemp = []
      concretelist.map((data, index) => {
        data.index = index + 1
        concretetemp.push(data)
      })
      console.log(concretetemp)

      concretelist = concretetemp.sort((a, b) => {
        return naturalSort(a.TruckNo, b.TruckNo)
      })
    } else if (this.state.sort == 2) {
      concretelist = this.props.concretelist.reverse()
    } else {
      concretelist = this.props.concretelist
    }
    return (
      <View style={styles.lisTable}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={styles.tableCol}>
            <View style={styles.tableHeadGroup}>
              <View
                style={[
                  styles.tableHead,
                  {
                    width: row1,
                    borderLeftWidth: 1,
                    height: I18n.locale == "th" ? 60 : 90
                  }
                ]}
              >
                <Text style={{ textAlign: "center" }}>
                  {I18n.t("mainpile.9_1.no")}
                </Text>
                <Text style={{ textAlign: "center", color: "green" }}>
                  {I18n.t("mainpile.9_1.carNo")}
                </Text>
              </View>
              <View
                style={[
                  styles.tableHead,
                  { width: row2, height: I18n.locale == "th" ? 60 : 90 }
                ]}
              >
                <Text style={{ textAlign: "center" }}>
                  {I18n.t("mainpile.9_1.concrete")}
                </Text>
              </View>
              <View
                style={[
                  styles.tableHead,
                  { width: row3, height: I18n.locale == "th" ? 60 : 90 }
                ]}
              >
                <Text style={{ textAlign: "center" }}>
                  {I18n.t("mainpile.9_1.slump")}
                </Text>
                <Text>
                  ({this.state.slumpmin}-{this.state.slumpmax})
                </Text>
              </View>
              <View
                style={[
                  styles.tableHead,
                  { width: row4, height: I18n.locale == "th" ? 60 : 90 }
                ]}
              >
                <Text style={{ textAlign: "center" }}>
                  {I18n.t("mainpile.9_1.carplant") + "/"}
                </Text>
                <Text style={{ textAlign: "center" }}>
                  {I18n.t("mainpile.9_1.carsite")}
                </Text>
              </View>
              <View
                style={[
                  styles.tableHead,
                  { width: row5, height: I18n.locale == "th" ? 60 : 90 }
                ]}
              >
                <Text style={{ textAlign: "center" }}>
                  {I18n.t("mainpile.9_1.approve")}
                </Text>
              </View>
            </View>

            {concretelist.map((item, key) => {
              var arrival = moment(item.TruckArrivalTime, [
                "DD/MM/YYYY HH:mm:ss",
                "MM-DD-YYYY HH:mm:ss"
              ])
              var depart = moment(item.TruckDepartureTime, [
                "DD/MM/YYYY HH:mm:ss",
                "MM-DD-YYYY HH:mm:ss"
              ])
              arrival = arrival.isValid()
                ? arrival.format("DD/MM/YYYY HH:mm")
                : "-"
              depart = depart.isValid()
                ? depart.format("DD/MM/YYYY HH:mm")
                : "-"
              return (
                <ModalSelector
                  data={data}
                  cancelText="Cancel"
                  onModalClose={option => {
                    if (option.action == "edit") {
                      this.onEditData(item.Id, key, item.ApproveStatus)
                    } else if (option.action == "delete") {
                      this.deleteRow(item.Id, key)
                    } else if (option.action == "view") {
                      this.onViewData(item.Id, key, item.ApproveStatus)
                    }
                  }}
                  key={key}
                  // disabled={this.state.Edit_Flag == 0}
                >
                  <View key={key} style={styles.tableContentGroup}>
                    <View
                      style={[
                        styles.tableContent,
                        { width: row1, borderLeftWidth: 1 },
                        key + 1 == concretelist.length && {
                          borderBottomWidth: 1
                        },
                        item.IsReject && { backgroundColor: "#ff7a73" },
                        item.Local && { backgroundColor: "#F7F6BD" }
                      ]}
                    >
                      <Text style={{ textAlign: "center" }}>
                        {item.index || key + 1}
                      </Text>
                      <Text style={{ textAlign: "center", color: "green" }}>
                        {item.TruckNo}
                      </Text>
                    </View>
                    <View
                      style={[
                        styles.tableContent,
                        { width: row2 },
                        styles.tableContentCenter,
                        item.Local && { backgroundColor: "#F7F6BD" }
                      ]}
                    >
                      <Text>{item.TruckConcreteVolume}</Text>
                    </View>
                    <View
                      style={[
                        styles.tableContent,
                        { width: row3 },
                        styles.tableContentCenter,
                        item.Local && { backgroundColor: "#F7F6BD" }
                      ]}
                    >
                      <Text>{item.Slump || "-"}</Text>
                    </View>
                    <View
                      style={[
                        styles.tableContent,
                        { width: row4 },
                        styles.tableContentCenter,
                        item.Local && { backgroundColor: "#F7F6BD" }
                      ]}
                    >
                      <Text>{depart}</Text>
                      <Text>{arrival}</Text>
                    </View>
                    <View
                      style={[
                        styles.tableContent,
                        { width: row5 },
                        styles.tableContentCenter,
                        item.Local && { backgroundColor: "#F7F6BD" }
                      ]}
                    >
                      {item.ApproveStatus == 0 && (
                        <Icon name="check" reverse color="grey" size={15} />
                      )}
                      {(item.ApproveStatus == 1 || item.ApproveStatus == 4) && (
                        <Icon name="check" reverse color="#f77721" size={15} />
                      )}
                      {item.ApproveStatus == 2 && (
                        <Icon name="check" reverse color="#6dcc64" size={15} />
                      )}
                      {item.ApproveStatus == 3 && (
                        <Icon name="check" reverse color="red" size={15} />
                      )}
                      {item.ApproveStatus == null && (
                        <Icon name="check" reverse color="grey" size={15} />
                      )}
                    </View>
                  </View>
                </ModalSelector>
              )
            })}
          </View>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll,
    pile9success: state.pile.pile9success,
    concretelist: state.concrete.concretelist,
    concretevolume: state.concrete.concretevolume,
    indexcount: state.concrete.index,
    concreteid: state.concrete.concreteid,
    mixid: state.concrete.mixid,
    concreterecord: state.concrete.concreterecord
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(
  Pile9_1
)
