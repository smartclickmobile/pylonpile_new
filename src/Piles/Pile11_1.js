import React, { Component } from "react"
import { View, Text, TouchableOpacity, TextInput } from "react-native"
import ModalSelector from "react-native-modal-selector"
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM } from "../Constants/Color"
import { Icon } from "react-native-elements"
import * as actions from "../Actions"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import I18n from "../../assets/languages/i18n"
import styles from "./styles/Pile10_1.style"

class Pile11_1 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      Edit_Flag: 1,
      foremanset: "",
      foremansetid: "",
      foreman: "",
      foremanid: "",
      foremansetdata: [{ key: 0, section: true, label: I18n.t("mainpile.10_1.foremansetselect") }],
      foremandata: [{ key: 0, section: true, label: I18n.t("mainpile.10_1.foremanselect") }],
      process: 11,
      step: 1,
      machine: "",
      machineid: "",
      machinedata: [{ key: 0, section: true, label: I18n.t("mainpile.11_1.machineselect") }],
      driver: "",
      driverid: "",
      driverdata: [{ key: 0, section: true, label: I18n.t("mainpile.11_1.driverselect") }],
      vibro: "",
      vibroid: "",
      vibrodata: [{ key: 0, section: true, label: I18n.t("mainpile.11_1.vibroselect") }],
      vibroenable: false
    }
  }

  updateState(value) {
    console.log("Update state from 10_1")
  }

  componentDidMount() {
    if (this.props.onRefresh === "refresh") {
      Actions.refresh({
        action: "start"
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    // console.warn('NEXT PROPS 10-1')
    var pile = null
    var pileMaster = null
    var pile11 = null
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["11_1"]) {
      pile = nextProps.pileAll[this.props.pileid]["11_1"].data
      pileMaster = nextProps.pileAll[this.props.pileid]["11_1"].masterInfo
    }

    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["11_0"]) {
      pile11 = nextProps.pileAll[this.props.pileid]["11_0"].data
    }

    if (pileMaster) {
      if (pileMaster.CraneList) {
        let machinedata = [{ key: 0, section: true, label: I18n.t("mainpile.11_1.machineselect") }]
        pileMaster.CraneList.map((data, index) => {
          machinedata.push({ key: index + 1, label: data.machine_no, id: data.itemid, vibro: data.vibro_flag})
          if (pile && pile.Machine && pile.Machine.itemid) {
            if (pile.Machine.itemid == data.itemid) {
              this.setState({ machine: data.machine_no, machineid: data.itemid, vibroenable: data.vibro_flag })
            }
          }

          /**
           * If value don't have anything set value to nothing
           */
          // Value from API
          if (pile.hasOwnProperty('Machine') && pile.Machine == null) {
            this.setState({ machine: "", machineid: "" })
          }
          // Value from local
          if (pile.hasOwnProperty('MachineName') && pile.MachineName == null) {
            this.setState({ machine: "", machineid: "" })
          }
        })
        this.setState({ machinedata })
      }
      if (pileMaster.VibroList) {
        let vibrodata = [{ key: 0, section: true, label: I18n.t("mainpile.11_1.vibroselect") }]
        pileMaster.VibroList.map((data, index) => {
          vibrodata.push({ key: index + 1, label: data.machine_no, id: data.itemid })
          if (pile && pile.Vibro && pile.Vibro.itemid) {
            if (pile.Vibro.itemid == data.itemid) {
              this.setState({ vibro: data.machine_no, vibroid: data.itemid })
            }
          }
          if (pile.Vibro == null) {
            this.setState({ vibro: "", vibroid: "" })
          }
        })
        this.setState({ vibrodata })
      }

      if (pileMaster.DriverList) {
        let driverdata = [{ key: 0, section: true, label: I18n.t("mainpile.11_1.driverselect") }]
        pileMaster.DriverList.map((data, index) => {
          let name = (data.nickname && data.nickname + "-") + data.firstname + " " + data.lastname
          driverdata.push({ key: index + 1, label: name, id: data.employee_id })
          if (pile && pile.DriverId) {

            if (pile.DriverId == data.employee_id) {
              this.setState({ driver: name, driverid: data.employee_id })
            }
          }
          if (pile.DriverId == null) {
            this.setState({ driver: "", driverid: "" })
          }
        })
        this.setState({ driverdata })
      }
    }

    if (pile11) {
      this.setState({ Edit_Flag: pile11.Edit_Flag })
    }
  }

   selectMachine = async data => {
   await this.setState({ machine: data.label, machineid: data.id, vibroenable: data.vibro }, () => {
      if(this.state.vibroenable == false){
        this.setState({vibrodata:[{ key: 0, section: true, label: I18n.t("mainpile.11_1.vibroselect") }],vibro:'',vibroid:''})
      }
      
    })
    this.saveLocal()
  }

  selectVibro = data => {
    this.setState({ vibro: data.label, vibroid: data.id }, () => this.saveLocal())
  }

  selectDriver = data => {
    this.setState({ driver: data.label, driverid: data.id }, () => this.saveLocal())
  }

  saveLocal() {
    var value = {
      process: 11,
      step: 1,
      pile_id: this.props.pileid,
      data: {
        Machine: {
          itemid: this.state.machineid,
          vibro_flag: this.state.vibroenable
        },
        MachineName:this.state.machine,
        Vibro: {
          itemid: this.state.vibroid
        },
        VibroName:this.state.vibro,
        DriverId: this.state.driverid,
        DriverName:this.state.driver
      }
    }
    console.warn('saveLocal',value)
    this.props.mainPileSetStorage(this.props.pileid, "11_1", value)
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    return (
      <View>
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t("mainpile.11_1.topic")}</Text>
        </View>
        <View style={{ marginTop: 20 }}>
          <View style={styles.boxTopic}>
            <Text style={{ marginLeft: 10 }}>{I18n.t("mainpile.11_1.machine")}</Text>
          </View>
          <View style={styles.selectedView}>
            <ModalSelector
              data={this.state.machinedata}
              onChange={this.selectMachine}
              selectTextStyle={styles.textButton}
              cancelText="Cancel"
              disabled={this.state.Edit_Flag == 0}
            >
              <TouchableOpacity style={[styles.button, this.state.Edit_Flag == 0 && { borderColor: MENU_GREY_ITEM }]} disabled={this.state.Edit_Flag == 0}>
                <View style={styles.buttonTextStyle}>
                  <Text style={[styles.buttonText, this.state.Edit_Flag == 0 && { color: MENU_GREY_ITEM }]}>
                    {this.state.machine || I18n.t("mainpile.11_1.machineselect")}
                  </Text>
                  <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                    <Icon name="arrow-drop-down" type="MaterialIcons" color={this.state.Edit_Flag == 0 ? MENU_GREY_ITEM : "#007CC2" } />
                  </View>
                </View>
              </TouchableOpacity>
            </ModalSelector>
          </View>
        </View>
        <View style={{ marginTop: 10 }}>
          <View style={styles.boxTopic}>
            <Text style={{ marginLeft: 10 }}>{I18n.t("mainpile.11_1.vibro")}</Text>
          </View>
          <View style={styles.selectedView}>
            <ModalSelector
              data={this.state.vibrodata}
              onChange={this.selectVibro}
              selectTextStyle={styles.textButton}
              cancelText="Cancel"
              disabled={this.state.Edit_Flag == 0||!this.state.vibroenable}
            >
              <TouchableOpacity style={[styles.button, (this.state.Edit_Flag == 0||!this.state.vibroenable) && { borderColor: MENU_GREY_ITEM }]} disabled={ this.state.Edit_Flag == 0||!this.state.vibroenable}>
                <View style={styles.buttonTextStyle}>
                  <Text style={[styles.buttonText,  (this.state.Edit_Flag == 0||!this.state.vibroenable) && { color: MENU_GREY_ITEM }]}>{this.state.vibro==''? I18n.t("mainpile.11_1.vibroselect"):this.state.vibro}</Text>
                  <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                  <Icon name="arrow-drop-down" type="MaterialIcons" color={(this.state.Edit_Flag == 0||!this.state.vibroenable) ? MENU_GREY_ITEM : "#007CC2" } />
                  </View>
                </View>
              </TouchableOpacity>
            </ModalSelector>
          </View>
        </View>
        <View style={{ marginTop: 10 }}>
          <View style={styles.boxTopic}>
            <Text style={{ marginLeft: 10 }}>{I18n.t("mainpile.11_1.driver")}</Text>
          </View>
          <View style={styles.selectedView}>
            <ModalSelector
              data={this.state.driverdata}
              onChange={this.selectDriver}
              selectTextStyle={styles.textButton}
              cancelText="Cancel"
              disabled={this.state.Edit_Flag == 0}
            >
              <TouchableOpacity style={[styles.button, this.state.Edit_Flag == 0 && { borderColor: MENU_GREY_ITEM }]} disabled={this.state.Edit_Flag == 0}>
                <View style={styles.buttonTextStyle}>
                  <Text style={[styles.buttonText, this.state.Edit_Flag == 0 && { color: MENU_GREY_ITEM }]}>{this.state.driver || I18n.t("mainpile.11_1.driverselect")}</Text>
                  <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                  <Icon name="arrow-drop-down" type="MaterialIcons" color={this.state.Edit_Flag == 0 ? MENU_GREY_ITEM : "#007CC2" } />
                  </View>
                </View>
              </TouchableOpacity>
            </ModalSelector>
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(Pile11_1)
