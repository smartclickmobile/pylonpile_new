import React, { Component } from "react"
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Alert
} from "react-native"
import {
  MAIN_COLOR,
  SUB_COLOR,
  MENU_GREY_ITEM,
  MENU_GREY_BORDER,
  MENU_BACKGROUND
} from "../Constants/Color"
import { Container, Content } from "native-base"
import ModalSelector from "react-native-modal-selector"
import { Icon } from "react-native-elements"
import I18n from "../../assets/languages/i18n"
import { Extra } from "../Controller/API"
import TableView from "../Components/TableView"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"

class SlumpApprove extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      approvedata: [{ key: 0, section: true, label: this.props.check10==true?I18n.t('mainpile.SlumpApprove.approvelist1'):I18n.t('mainpile.SlumpApprove.approvelist') }],
      approveid: null,
      approvetype: null,
      approved: false,
      approve: false,
      appovestatus:0
    }
  }
  _renderTopic() {
    return (
      <View style={styles.topic}>
        <Text style={styles.topicText}>{this.props.check10 == true ?I18n.t('mainpile.SlumpApprove.topicack'):I18n.t('mainpile.SlumpApprove.topicapprove')}</Text>
      </View>
    )
  }

  _xButton = () => {
    return (
      <View style={{ marginRight: 5 }}>
        <Icon
          name="close"
          type="evilicons"
          color="#FFF"
          onPress={() => this.props.authLogout()}
          style={{ marginRight: 10 }}
        />
      </View>
    )
  }

  // updateState(value) {
  //   console.warn(value)
  //   this.setState({
  //     approved: true,
  //     approvetype: value.approveBy,
  //     approveid: value.ApprovedUserId
  //   })
  // }

  componentDidMount() {
    const { approveuser } = this.props
    
    var approve = [{ key: 0, section: true, label:this.props.check10 == true ?I18n.t('mainpile.SlumpApprove.approvelist1'): I18n.t('mainpile.SlumpApprove.approvelist') }]
    console.warn("ApproveEmployeeId",this.props.ApproveEmployeeId)
    if(this.props.check10==true&&this.props.ApproveEmployeeId !=null){
      approveuser.map((data, index) => {
        if(data.employee_id==this.props.ApproveEmployeeId){
          approve.push({
            key: index + 1,
            id: data.employee_id,
            label: `${data.nickname}-${data.firstname} ${data.lastname}`
          })
        }
      })
    }else{
      approveuser.map((data, index) => {
        approve.push({
          key: index + 1,
          id: data.employee_id,
          label: `${data.nickname}-${data.firstname} ${data.lastname}`
        })
      })
    }
    
    this.setState({ approvedata: approve })
    console.warn("check10",this.props.check10)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error == null) {
      this.setState({ data: nextProps.data })
    }
    if (nextProps.action && nextProps.action == "update") {
      this.setState(
        {
          approved: true,
          approvetype: nextProps.value.approveBy,
          approveid: nextProps.value.ApprovedUserId
        },
        () => this.updateApprove()
      )
    }
    
  }

  updateApprove() {
    Actions.pop({
      refresh: {
        action: "update",
        value: {
          approvetype: this.state.approvetype,
          approveid: this.state.approveid
        }
      }
    })
  }
  onCancelApprove(){
    const {truckno} = this.props
    Alert.alert("", I18n.t('mainpile.SlumpApprove.confirmdelete'), [
      {
        text: "Cancel",
        // onPress: () => this.setState({ select_user: "", approveBy: "", ApprovedUserId: "" })
      },
      {
        text: "OK",
        onPress: () => {

          var value = {
            Language: I18n.locale,
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            TruckRegisterId: truckno,
            latitude: this.props.lat,
            longitude: this.props.log,
          }
          this.props.pileCancelSlumpApprove(value)
          // this.setState({submit:true})
        }
      }
    ],
    { cancelable: false })
  }

  selectApprove = data => {
    Alert.alert(
      "",
      (this.props.check10==true ? I18n.t('mainpile.3_4.acceptbyheader'):I18n.t('mainpile.3_4.by')) + " "+data.label,
      [
        {
          text: "Cancel",
          onPress: () =>
            this.setState({
              select_user: "",
              approveBy: "",
              ApprovedUserId: ""
            })
        },
        {
          text: "OK",
          onPress: () => {
            this.setState(
              {
                approveuser: data.label,
                approveid: data.id,
                approved: true,
                approvetype: 1
              },
              () => this.updateApprove()
            )
          }
        }
      ],
      { cancelable: false }
    )
  }

  _renderBasicInformation() {
    const piledetail = this.props.piledetail || []
    return (
      <View style={{ marginTop: 15 }}>
        <View style={styles.topicBox}>
          <Text style={styles.subTopic}>{I18n.t('notificationdetail.information')}</Text>
        </View>
        <View style={{ marginTop: 5 }}>
          <BoxView left={I18n.t('notificationdetail.projectName')} right={piledetail.job_name} />
          <BoxView left={I18n.t('notificationdetail.pileNo')} right={piledetail.pile_no} />
          <BoxView left={I18n.t('notificationdetail.diameter')} right={piledetail.size} />
          <BoxView
            left={I18n.t('notificationdetail.cutoff')}
            right={parseFloat(piledetail.cutoff).toFixed(3)}
          />
          <BoxView
            left={I18n.t('piledetail.piletip')}
            right={parseFloat(piledetail.piletip).toFixed(3)}
            last={!piledetail.tgw}
          />
          {piledetail.tgw && (
            <BoxView
              left="Top Guild Wall"
              right={parseFloat(piledetail.tgw).toFixed(3)}
              last
            />
          )}
        </View>
      </View>
    )
  }

  _renderConcreteInformation() {
    const {
      truckno,
      concretebrand,
      mix,
      strength,
      unit,
      truckconcretevolume,
      slump,
      truckarrival,
      truckdepart,
      slumpimage
    } = this.props

    return (
      <View style={{ marginTop: 15 }}>
        <View style={styles.topicBox}>
          <Text style={styles.subTopic}>{I18n.t('mainpile.SlumpApprove.inforegister')}</Text>
        </View>
        <View style={{ marginTop: 5 }}>
          <BoxView left={I18n.t('mainpile.concreteregister.Nocar')} right={truckno} />
          <BoxView left={I18n.t('mainpile.concreteregister.concrete')} right={concretebrand} />
          <BoxView left="Mix No." right={mix} />
          <BoxView left="Concrete Strength (ksc)" right={strength} />
          <BoxView left="Unit" right={unit} />
          <BoxView
            left={I18n.t('notificationdetail.concrete_per')}
            right={truckconcretevolume}
          />
          <BoxViewSlum left={I18n.t('notificationdetail.slump')} right={slump} />
          <BoxView left={I18n.t('mainpile.SlumpApprove.carcomesite')} right={truckarrival} />
          <BoxView left={I18n.t('mainpile.SlumpApprove.caroutplant')} right={truckdepart} last />
        </View>
      </View>
    )
  }

  _renderButton() {
    return (
      <View>
        {this.state.approved || this.state.approve ? (
          <View style={styles.button_gray}>
            <Text style={styles.textButton}>{this.props.check10 == true ? I18n.t('mainpile.3_4.acceptonline'):I18n.t('mainpile.3_4.approveonline')}</Text>
          </View>
        ) : (
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.refs.Modal_ApprovUser.open()}
          >
            <Text style={styles.textButton}>{this.props.check10 == true ? I18n.t('mainpile.3_4.acceptonline'):I18n.t('mainpile.3_4.approveonline')}</Text>
          </TouchableOpacity>
        )}
        {this.state.approved || this.state.approve ? (
          <View style={styles.button_gray}>
            <Text style={styles.textButton}>{this.props.check10 == true ? I18n.t('mainpile.3_4.acceptself'):I18n.t('mainpile.3_4.approveself')}</Text>
          </View>
        ) : (
          <TouchableOpacity
            style={styles.button}
            onPress={() =>
              Actions.selfapprove({
                process: this.props.process,
                step: this.props.step,
                data: false,
                jobid: this.props.jobid,
                pileid: this.props.pileid,
                category: this.props.shape,
                check10:this.props.check10
              })
            }
          >
            <Text style={styles.textButton}>{this.props.check10 == true ? I18n.t('mainpile.3_4.acceptself'):I18n.t('mainpile.3_4.approveself')}</Text>
          </TouchableOpacity>
        )}
        {this.state.approved || this.state.approve ? (
          <View style={styles.button_gray}>
            <Text style={styles.textButton}>{this.props.check10 == true ? I18n.t('mainpile.3_4.acceptqr'):I18n.t('mainpile.3_4.approveqr')}</Text>
          </View>
        ) : (
          <TouchableOpacity
            style={styles.button}
            onPress={() =>
              Actions.scanQrCode({
                process: this.props.process,
                step: this.props.step,
                data: false,
                jobid: this.props.jobid,
                pileid: this.props.pileid
              })
            }
          >
            <Text style={styles.textButton}>{this.props.check10 == true ? I18n.t('mainpile.3_4.acceptqr'):I18n.t('mainpile.3_4.approveqr')}</Text>
          </TouchableOpacity>
        )}
        {/*{this.state.appovestatus == 1  ? (
        <View style={styles.button_gray}>
          <Text style={styles.textButton}>ยกเลิกอนุมัติ</Text>
        </View>
        ) :(
          <TouchableOpacity
            style={styles.button}
            onPress={() =>{
              this.props.clearError()
              this.onCancelApprove
            }
          }
          >
            <Text style={styles.textButton}>ยกเลิกอนุมัติ</Text>
          </TouchableOpacity>
        )}*/}
      </View>
    )
  }

  renderModal() {
    return (
      <ModalSelector
        data={this.state.approvedata}
        onChange={this.selectApprove}
        selectTextStyle={styles.textButton}
        style={{ borderWidth: 0, height: 0, width: 0 }}
        cancelText="Cancel"
        ref="Modal_ApprovUser"
      />
    )
  }

  onCameraRoll(keyname, photos, type) {
    Actions.camearaRoll({
      process: 0,
      step: 0,
      pileId: 0,
      keyname: keyname,
      photos: photos,
      typeViewPhoto: type,
      category: this.props.category,
      Edit_Flag:this.state.Edit_Flag
    })
  }

  onConfirmPress(flag) {
    this.props.notiConfirm(this.state.data.Id, flag)
    // Actions.pop()
    // setTimeout(()=> Actions.refresh({ force: true }), 500)
    Actions.pop({
      refresh: {
        value: {
          force: true
        }
      }
    })
    setTimeout(() => Actions.refresh({ value: { force: false } }), 500)
  }

  render() {
    return (
      <Container>
        <Content>
          {this._renderTopic()}
          {this._renderBasicInformation()}
          {this._renderConcreteInformation()}
          {this._renderButton()}
          {this.renderModal()}
        </Content>
      </Container>
    )
  }
}

class BoxView extends Component {
  render() {
    return (
      <View
        style={[styles.box, this.props.last ? { borderBottomWidth: 0.5 } : {}]}
      >
        <View style={styles.leftBox}>
          <Text style={styles.textBox}>{this.props.left}</Text>
        </View>
        <View style={styles.rightBox}>
          <Text style={styles.textBox}>{this.props.right}</Text>
        </View>
      </View>
    )
  }
}
class BoxViewSlum extends Component {
  render() {
    return (
      <View
        style={[
          styles.Slumbox,
          this.props.last ? { borderBottomWidth: 0.5 } : {}
        ]}
      >
        <View style={styles.leftBox}>
          <Text style={styles.textSlumBox}>{this.props.left}</Text>
        </View>
        <View style={styles.rightBox}>
          <Text style={styles.textSlumBox}>{this.props.right}</Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  topic: {
    marginTop: 10,
    marginLeft: 10,
    alignItems: "center"
  },
  topicText: {
    fontSize: 25,
    color: MAIN_COLOR
  },
  subTopic: {
    fontSize: 15,
    fontWeight: "bold",
    color: "black",
    marginLeft: 10
  },
  box: {
    height: "auto",
    minHeight: 30,
    flexDirection: "row",
    borderTopWidth: 0.5,
    borderColor: "black"
  },
  leftBox: {
    backgroundColor: MENU_GREY_ITEM,
    width: "40%",
    justifyContent: "center"
  },
  rightBox: {
    width: "60%",
    justifyContent: "center"
  },
  textBox: {
    marginLeft: 10,
    textAlignVertical: "center"
  },
  topicBox: {
    borderTopWidth: 0.5
  },
  button: {
    margin: 10,
    backgroundColor: MAIN_COLOR,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
  textButton: {
    color: "#FFF",
    fontSize: 22,
    fontFamily: "THSarabunNew",
    fontWeight: "bold"
  },
  picButton: {
    backgroundColor: SUB_COLOR,
    height: 40,
    width: "50%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10
  },
  textButton: {
    marginLeft: 10,
    color: "white"
  },
  Slumbox: {
    height: "auto",
    minHeight: 30,
    flexDirection: "row",
    borderWidth: 2,
    borderColor: "red"
  },
  textSlumBox: {
    marginLeft: 10,
    textAlignVertical: "center",
    color: "red"
  },
  button_gray: {
    margin: 10,
    backgroundColor: MENU_GREY_ITEM,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  }
})

const mapStateToProps = state => {
  return {
    error: state.noti.error,
    data: state.noti.notidetail
  }
}

export default connect(mapStateToProps, actions)(SlumpApprove)
