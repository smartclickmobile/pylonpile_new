import React, {Component} from "react"
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Alert,
  Image,
  ScrollView,
  Modal,
  ActivityIndicator
} from "react-native"
import {
  MAIN_COLOR,
  SUB_COLOR,
  BASIC_COLOR,
  MENU_BACKGROUND,
  MENU_GREY_ITEM,
  BUTTON_COLOR,
  BLUE_COLOR
} from "../Constants/Color"
import ModalSelector from "react-native-modal-selector"
import {Icon} from "react-native-elements"
import I18n from "../../assets/languages/i18n"
import {Extra} from "../Controller/API"
import TableView from "../Components/TableView"
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import * as actions from '../Actions';
import styles from './styles/Pile5_3.style'
import moment from 'moment'
var data = [
  {
    key: 0,
    section: true,
    label: I18n.t('mainpile.4_1.selector'),
    action: "head"
  },
  {
    key: 1,
    label: I18n.t('mainpile.4_1.edit'),
    action: "edit"
  }, {
    key: 2,
    label: I18n.t('mainpile.4_1.delete'),
    action: 'delete'
  }
]

class Pile5_5 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 5,
      step: 3,
      DrillingList:[],
      location:{
        position:{
          lat:1,
          log:1,
        }
      },
      StartDrilling:'',
      modalVisible:false,
      titleModal:'',
      typeModel:0,
      indexList:0,
      value:null,
      sumTotal:0,
      drillTypeData:[{ key: 0, section: true, label: ''  }],
      drilltype:'',
      DrillTypeId:'',
      dataMaster:null,
      deleteDisable:false,
      dataMasterPile5_2:null,
      Edit_Flag:1,
      depthRow:'',
      data:'',
      loading:false,
      status6:0,
      status7:0,
      text:'',
      images_water:[],
      images_plummet:[],
      checkplummet:false,
      checkwater:false,
      parent:true,

      pile5_1:null,
      pile5_5:null
    }
  }

  componentWillMount() {
    // console.warn('shape',this.props.shape)
    this.props.getDrillingChild({
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      Language: I18n.locale
    })
    if(this.props.shape == 1){
      this.setState({text:'mainpile.5_3'})
    }else{
      this.setState({text:'mainpile.5_3_dw'})
    }
  }

  componentWillUpdate(nextProps, nextState){
    if (this.state.DrillingList != nextState.drillinglist && this.state.deleteDisable == false) {
      this.onSumDepth(nextState.drillinglist)
    }
  }

  async updateState(value) {
    // console.warn('updateState 5_5',value.data.images_water,value.data.images_plummet)
    var listDrillingList = []
    var dataCrane = {}
    var dataDrillType = {}
    var index = value.data.No - 1
    if (value.data.DrillPileInsertType == 'edit') {
      listDrillingList = this.state.DrillingList
      if (listDrillingList[index] == undefined) {
        listDrillingList.push({
          DrillTypeId: value.data.DrillTypeId,
          DrillToolId:value.data.DrillToolId,
          DriverId: value.data.DriverId,
          EndDrilling:value.data.EndDrilling,
          IsBreak: value.data.IsBreak,
          MachineCraneId: value.data.MachineCraneId,
          StartDrilling: value.data.StartDrilling,
          drilltype: value.data.drilltype,
          drilltool: value.data.drilltool,
          driver: value.data.driver,
          machine: value.data.machine,
          Depth: value.data.Depth,
          Images : value.data.Images,
          No:value.data.No,
          DrillingToolType:value.data.DrillingToolType,
          DrillingTool:value.data.DrillingTool,
          Crane:value.data.Crane,
          PowerPack:value.data.PowerPack,
          PowerPackId:value.data.PowerPackId,
          powerpack:value.data.powerpack,
        })
      }else {
        listDrillingList[index].DrillTypeId = value.data.DrillTypeId
        listDrillingList[index].DrillToolId = value.data.DrillToolId
        listDrillingList[index].DriverId = value.data.DriverId
        listDrillingList[index].EndDrilling = value.data.EndDrilling
        listDrillingList[index].IsBreak = value.data.IsBreak
        listDrillingList[index].MachineCraneId = value.data.MachineCraneId
        listDrillingList[index].StartDrilling = value.data.StartDrilling
        listDrillingList[index].drilltype = value.data.drilltype
        listDrillingList[index].drilltool = value.data.drilltool
        listDrillingList[index].driver = value.data.driver
        listDrillingList[index].machine = value.data.machine
        listDrillingList[index].Depth = value.data.Depth
        listDrillingList[index].Images = value.data.Images
        listDrillingList[index].No = value.data.No
        listDrillingList[index].DrillingToolType = value.data.DrillingToolType
        listDrillingList[index].DrillingTool = value.data.DrillingTool
        listDrillingList[index].Crane = value.data.Crane
        listDrillingList[index].PowerPack = value.data.PowerPack
        listDrillingList[index].PowerPackId = value.data.PowerPackId
        listDrillingList[index].powerpack = value.data.powerpack
      }
    }else {
      if (this.state.DrillingList.length > 0) {
        listDrillingList = this.state.DrillingList
      }
      listDrillingList.push({
        DrillTypeId: value.data.DrillTypeId,
        DrillToolId:value.data.DrillToolId,
        DriverId: value.data.DriverId,
        EndDrilling:value.data.EndDrilling,
        IsBreak: value.data.IsBreak,
        MachineCraneId: value.data.MachineCraneId,
        StartDrilling: value.data.StartDrilling,
        drilltype: value.data.drilltype,
        drilltool: value.data.drilltool,
        driver: value.data.driver,
        machine: value.data.machine,
        Depth: value.data.Depth,
        Images : value.data.Images,
        No:value.data.No,
        DrillingToolType:value.data.DrillingToolType,
        DrillingTool:value.data.DrillingTool,
        Crane:value.data.Crane,
        PowerPack:value.data.PowerPack,
        PowerPackId:value.data.PowerPackId,
        powerpack:value.data.powerpack,
      })
    }
    this.setState({DrillingList:listDrillingList,loading:true},() =>{
      
      this.onSumDepth(this.state.DrillingList)
      setTimeout(() => {
          this.setState({loading:false})
          this.saveLocal('5_5',listDrillingList)
          // console.warn('DrillingList 2',listDrillingList,this.props.category,this.props.drillinglistChild.length,this.state.checkplummet,this.state.checkwater)
          if(this.props.category == 5 && this.props.drillinglistChild.length == 1 && this.state.checkplummet && this.state.checkwater){
            var valueApi ={
              Language:I18n.locale,
              JobId: this.props.jobid,
              PileId: this.props.pileid,
              Page:6,
              latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
              longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
              CheckPlummet:this.state.checkplummet,
              CheckWaterLevel:this.state.checkwater,
              ImageLengthSteelCarry: this.state.pile5_1.ImageLengthSteelCarry,
              ImageCheckPlummet: this.state.images_plummet,
              ImageCheckWaterLevel: this.state.images_water,
              OverLifting: this.state.pile5_1.OverLifting,
              Pcoring: this.state.pile5_1.Pcoring,
              hanginglength: this.state.pile5_1.HangingBarsLength,
              flag_notnext:true
            }
            this.props.pileSaveStep05(valueApi)
          }
      }, 3000)
    })
    
    if (value.data.images_water != undefined) {
      await this.setState({images_water:value.data.images_water})
      this.saveLocal('5_5')
   }
   if (value.data.images_plummet != undefined) {
      await this.setState({images_plummet:value.data.images_plummet})
      this.saveLocal('5_5')
   }
  }

  async componentWillReceiveProps(nextProps) {
    if(this.props.parent != undefined){
      this.setState({parent: this.props.parent})
    }
    var pile = null
    var pile5_0 = null
    var pileMaster = null
    var pile5_2 = null
    var pile5_1 = null
  //  if(nextProps.drillinglistChild!= null){ this.saveLocal('5_5',this.props.drillinglistChild)}
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['5_5']) {
      pile = nextProps.pileAll[this.props.pileid]['5_5'].data
      pileMaster = nextProps.pileAll[this.props.pileid]['5_5'].masterInfo
      this.setState({
        dataMaster:pileMaster,
        data:nextProps.pileAll[this.props.pileid],
        status6:nextProps.pileAll[this.props.pileid].step_status.Step6Status,
        status7:nextProps.pileAll[this.props.pileid].step_status.Step7Status,
        pile5_5:pile
      })
      // console.warn('nextProps',nextProps.pileAll[this.props.pileid].step_status.Step6Status)
    }
    if(nextProps.pileAll[this.props.pileid]['5_1']) {
      pile5_1 = nextProps.pileAll[this.props.pileid]['5_1'].data
      this.setState({pile5_1:pile5_1})
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['5_0']) {
      pile5_0 = nextProps.pileAll[this.props.pileid]['5_0'].data
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['5_2']) {
      let dataMasterPile5_2 = nextProps.pileAll[this.props.pileid]['5_2'].masterInfo
      this.setState({dataMasterPile5_2:dataMasterPile5_2},()=>{
        if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['3_3']) {
          pile3_3 = nextProps.pileAll[this.props.pileid]['3_3'].data
          if (pile3_3) {
            var data = dataMasterPile5_2
            if (pile3_3.ground) {
              data.GroundLevel = pile3_3.ground
            }
            if (pile3_3.top) {
              dataMasterPile5_2.TopCasingLevel = pile3_3.top
              dataMasterPile5_2.Depth =  pile3_3.top - data.PileTip
            }
            this.setState({dataMasterPile5_2:data})
          }
        }
      })
    }
    if(nextProps.pileAll != undefined && nextProps.pileAll != '') {
      this.setState({location:nextProps.pileAll.currentLocation})
    }
    if(this.props.category == 5){
      // console.warn('parent',this.props.parent)
      if(this.props.parent == false){
        this.setState({Edit_Flag:0})
      }else{
        this.setState({Edit_Flag:pile5_0.Edit_Flag})
      }
    }else{
      if (pile5_0) {
        // if (pile3_0.Edit_Flag != null && pile3_0.Edit_Flag != undefined && pile3_0.Edit_Flag != '') {
          this.setState({Edit_Flag:pile5_0.Edit_Flag})
        // }
      }
    }
    if (pile) {
      
      if (pile.DrillingList != null && this.props.hidden == false) {
        if (pile.DrillingList.length > 0 && this.state.DrillingList != pile.DrillingList) {
          this.setState({DrillingList:pile.DrillingList},()=> {console.warn('DrillingList 1',pile.DrillingList)})
        }
      }
      if (pile.CheckPlummet != null && pile.CheckPlummet != undefined) {
        await this.setState({checkplummet: pile.CheckPlummet})
      }else if(pile.CheckPlummet == null) {
        this.setState({checkplummet: false})
      }
      if (pile.ImageCheckPlummet != "" && pile.ImageCheckPlummet != undefined && pile.ImageCheckPlummet.length >0 ) {
        await this.setState({images_plummet: pile.ImageCheckPlummet})
      }else if(pile.ImageCheckPlummet == null || pile.ImageCheckPlummet.length == 0) {
        this.setState({images_plummet: []})
      }
      
      if (pile.CheckWaterLevel != null && pile.CheckWaterLevel != undefined) {
        await this.setState({checkwater: pile.CheckWaterLevel})
      }else if(pile.CheckWaterLevel == null) {
        this.setState({checkwater: false})
      }
      if (pile.ImageCheckWaterLevel != "" && pile.ImageCheckWaterLevel != undefined && pile.ImageCheckWaterLevel.length >0 ) {
        await this.setState({images_water: pile.ImageCheckWaterLevel})
      }else if(pile.ImageCheckWaterLevel == null || pile.ImageCheckWaterLevel.length == 0) {
        this.setState({images_water: []})
      }
      if (pile.CheckWaterLevel == false && pile.CheckWaterLevel == false) {
        await this.setState({loading: false})
      }
    }

    if (pileMaster && nextProps.hidden == false) {
      if ( this.state.drillTypeData != pileMaster.DrillingToolTypeList && pileMaster.DrillingToolTypeList != '' && this.state.drillTypeData.length == 1 && this.state.drilltype =='' ) {
        let list = [{ key: 0, section: true, label: I18n.t(this.state.text+'.drillselect') }]
        for (var i = 0; i < pileMaster.DrillingToolTypeList.length; i++) {
          await list.push({
            key: pileMaster.DrillingToolTypeList[i].Id,
            label: pileMaster.DrillingToolTypeList[i].Name,
          })
        }
        // console.warn('pileMaster DrillingToolTypeList',pileMaster.DrillingToolTypeList)
        await this.setState({drillTypeData:list})
      }
    }
    
  }

  onSumDepth(data){
    // console.warn('onSumDepth',data)
    // if (data.length > 0) {
    //   var index = data.length - 1
    //   this.setState({sumTotal:data[index].Depth})
    // }else {
    //   this.setState({sumTotal:0})
    // }
  }

  setTime(type,index){
    var title = ''
    if (type == 'start') {
      title = I18n.t(this.state.text+'.startdrill')
    }else {
      title = I18n.t(this.state.text+'.enddrill')
    }
    Alert.alert('', title, [
      {
        text: 'Cancel'
      },
      {
        text: 'OK',
        onPress: () =>{
          var now = moment().format("DD-MM-YYYY HH:mm:ss")
          var listDrillingList = []
          listDrillingList = this.props.drillinglistChild
          if (type == 'start') {
            listDrillingList[index].StartDrilling = now
            this.setState({DrillingList:listDrillingList},()=>{console.warn('DrillingList 3',listDrillingList)})
            this.saveLocal('5_5',listDrillingList)
          }else {
              listDrillingList[index].EndDrilling = now
              this.setState({DrillingList:listDrillingList},()=>{console.warn('DrillingList 4',listDrillingList)})
              if (listDrillingList[index].Depth == '') {
                  // this.onOpenModal('Depth',index)
              }
          }
        }
      }
    ])
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  selectedDrillType(drilltype,key){
    this.setState({ drilltype: drilltype.label,DrillTypeId:drilltype.key  })

    var listDrillingList = []
    listDrillingList = this.props.drillinglistChild
    listDrillingList[key].DrillingToolType.Name = drilltype.label
    listDrillingList[key].DrillingToolType.Id = drilltype.key
    this.setState({DrillingList:listDrillingList,value:null},()=>{console.warn('DrillingList 5',listDrillingList)})
    this.saveLocal('5_5',listDrillingList)
  }

  onAddData(){
    if(this.props.category == 5){
      if(this.state.checkplummet == true){
        if(this.state.checkwater == true){
          if(this.props.drillinglist.length > 0){
            var validate_depth = parseFloat(this.state.dataMasterPile5_2.PileInfo.Depth) - parseFloat(dataMasterPile5_2.PileInfo.Constant)
            if(this.props.drillinglist[this.props.drillinglist.length - 1].Depth >= validate_depth){
              var flag_add = true
              for (var i = 0; i < this.props.drillinglistChild.length; i++) {
                if (this.props.drillinglistChild[i].EndDrilling == '') {
                  flag_add = false
                }
              }
              // console.warn('status6',this.state.status6)
              var statuscheck = null
              if(this.props.shape == 1){
                statuscheck = this.state.status6
              }else{
                statuscheck = this.state.status7
              }
              if(statuscheck == 0){
                if (flag_add == true) {
                var step = this.state.step
                if(this.props.category == 5){
                  step = '5'
                }
                  Actions.drillPileInsert({
                    jobid: this.props.jobid,
                    pileid: this.props.pileid,
                    process: this.state.process,
                    step: step,
                    Edit_Flag: '1',
                    lat:this.state.location == undefined ? 1 : this.state.location.position.lat ,
                    log:this.state.location == undefined ? 1 : this.state.location.position.log ,
                    DrillPileInsertType:'add',
                    shape:this.props.shape,
                    category: this.props.category,
                    count: this.props.drillinglistChild.length,
                    DrillingListlist:this.props.drillinglistChild,
                    Depthneed:this.state.dataMasterPile5_2.PileInfo.DepthChild,
                    child:true
                  })
                }else {
                  Alert.alert(I18n.t(this.state.text+'.error'), I18n.t(this.state.text+'.dontadd'), [
                    {
                      text: "OK",
                    }
                  ],
                  { cancelable: false })
                }
              }else{
                if(this.props.shape == 1){
                  Alert.alert(I18n.t(this.state.text+'.error'), I18n.t(this.state.text+'.dontadd1'), [
                    {
                      text: "OK",
                    }
                  ],
                  { cancelable: false })
                }else{
                  Alert.alert(I18n.t(this.state.text+'.error'), I18n.t(this.state.text+'.dontadd2'), [
                    {
                      text: "OK",
                    }
                  ],
                  { cancelable: false })
                }
                
              }
            }else{
              Alert.alert(I18n.t('mainpile.5_3.depthcolnotco_dwall'))
            }
          }else {
            Alert.alert(I18n.t('mainpile.5_3.depthcolnotco_dwall'))
          }

        }else{
          Alert.alert('',I18n.t('alert.checkwater'))
        }
      }else{
        Alert.alert('',I18n.t('alert.checkplummet'))
      }

    }else{
      var flag_add = true
      for (var i = 0; i < this.props.drillinglistChild.length; i++) {
        if (this.props.drillinglistChild[i].EndDrilling == '') {
          flag_add = false
        }
      }
      // console.warn('status6',this.state.status6)
      var statuscheck = null
      if(this.props.shape == 1){
        statuscheck = this.state.status6
      }else{
        statuscheck = this.state.status7
      }
      if(statuscheck == 0){
        if (flag_add == true) {
          var step = this.state.step
          if(this.props.category == 5){
            step = '5'
          }
          Actions.drillPileInsert({
            jobid: this.props.jobid,
            pileid: this.props.pileid,
            process: this.state.process,
            step: step,
            Edit_Flag: '1',
            lat:this.state.location == undefined ? 1 : this.state.location.position.lat ,
            log:this.state.location == undefined ? 1 : this.state.location.position.log ,
            DrillPileInsertType:'add',
            shape:this.props.shape,
            category: this.props.category,
            count: this.props.drillinglistChild.length,
            DrillingListlist:this.props.drillinglistChild,
            Depthneed:this.state.dataMasterPile5_2.PileInfo.DepthChild,
            child:true
          })
        }else {
          Alert.alert(I18n.t(this.state.text+'.error'), I18n.t(this.state.text+'.dontadd'), [
            {
              text: "OK",
            }
          ],
          { cancelable: false })
        }
      }else{
        if(this.props.shape == 1){
          Alert.alert(I18n.t(this.state.text+'.error'), I18n.t(this.state.text+'.dontadd1'), [
            {
              text: "OK",
            }
          ],
          { cancelable: false })
        }else{
          Alert.alert(I18n.t(this.state.text+'.error'), I18n.t(this.state.text+'.dontadd2'), [
            {
              text: "OK",
            }
          ],
          { cancelable: false })
        }
        
      }
    }
    
    
  }

  onEditData(index){
    var list = this.props.drillinglistChild
    list[index].StartDrilling = list[index].StartDrilling
    list[index].EndDrilling = list[index].EndDrilling

    this.setState({DrillingList:list},()=>{console.warn('DrillingList 6',list)})
    var step = this.state.step
    if(this.props.category == 5){
      step = '5'
    }
    Actions.drillPileInsert({
      jobid: this.props.jobid,
      pileid: this.props.pileid,
      process: this.state.process,
      step: step,
      Edit_Flag: this.state.Edit_Flag,
      lat:this.state.location == undefined ? 1 : this.state.location.position.lat ,
      log:this.state.location == undefined ? 1 : this.state.location.position.log ,
      DrillingListData:this.props.drillinglistChild[index],
      DrillPileInsertType: 'edit',
      shape:this.props.shape,
      category: this.props.category,
      DrillingListlist:this.props.drillinglistChild,
      index:index,
      Depthneed:this.state.dataMasterPile5_2.PileInfo.DepthChild,
      child:true
    })
    console.log('drillinglist step5',this.props.drillinglistChild[index])

  }

  onOpenModal(title,index){
    this.setModalVisible(!this.state.modalVisible)
    if (title == 'Depth') {
      this.setState({titleModal:I18n.t(this.state.text+'.depthbp'),typeModel:1,indexList:index})
    }
  }
  onCloseModal(){
    this.setModalVisible(!this.state.modalVisible)
    var listDrillingList = []
    listDrillingList = this.props.drillinglistChild
    if (this.state.typeModel == 1) {
      listDrillingList[this.state.indexList].Depth = this.state.value == null ? this.state.depthRow : this.state.value
      this.setState({DrillingList:listDrillingList,value:null},()=>{console.warn('DrillingList 7',listDrillingList)})
      this.onSumDepth(this.props.drillinglistChild)
      this.saveLocal('5_5',listDrillingList)
    }
  }
  changeValue(data){
    this.setState({value:data,depthRow:data})
  }

  onSaveApiPile5(){
    var dataDrillingList = []
    var dataImageFiles = []

    for (var i = 0; i < this.state.data['5_5'].data.DrillingList.length; i++) {
      dataImageFiles = []
      if (this.state.data['5_5'].data.DrillingList[i].Images != null && this.state.data['5_5'].data.DrillingList[i].Images.length > 0 ) {
        for (var j = 0; j < this.state.data['5_5'].data.DrillingList[i].Images.length; j++) {
          dataImageFiles.push(this.state.data['5_5'].data.DrillingList[i].Images[j])
        }
      }
      dataDrillingList.push({
        No: this.state.data['5_5'].data.DrillingList[i].No,
        DrillingToolTypeId: this.state.data['5_5'].data.DrillingList[i].DrillingToolType.Id,
        MachineId: this.state.data['5_5'].data.DrillingList[i].Crane.itemid,
        DrillingToolId: this.state.data['5_5'].data.DrillingList[i].DrillingTool == undefined ? '' : this.state.data['5_5'].data.DrillingList[i].DrillingTool.itemid,
        PowerPackId: this.state.data['5_5'].data.DrillingList[i].PowerPack == undefined ? '' : this.state.data['5_5'].data.DrillingList[i].PowerPack.itemid ,
        DriverId: this.state.data['5_5'].data.DrillingList[i].DriverId,
        StartDrilling:moment(this.state.data['5_5'].data.DrillingList[i].StartDrilling,'DD-MM-YYYY HH:mm:ss').format("MM-DD-YYYY HH:mm:ss"),
        EndDrilling:moment(this.state.data['5_5'].data.DrillingList[i].EndDrilling,'DD-MM-YYYY HH:mm:ss').format("MM-DD-YYYY HH:mm:ss"),
        Depth: this.state.data['5_5'].data.DrillingList[i].Depth,
        IsBreak: this.state.data['5_5'].data.DrillingList[i].IsBreak,
        Images: dataImageFiles,
      })
    }

    var valueApi ={
      Language:I18n.locale,
      JobId: this.props.jobid,
      PileId: this.props.pileid,
      Page:4,
      latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
      longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
      DrillingList:dataDrillingList,
    }
    console.log('pile5_5',valueApi)
    this.props.pileSaveStep05(valueApi)
  }

  deleteRow(index){
    // console.log('deleteRow',this.props.drillinglistChild[index+1].Depth)
    Alert.alert("", I18n.t(this.state.text+'.confirmsave'), [
      {
        text: "Cancel",
      },
      {
        text: "OK",
        onPress: () => {
          var position = index + 1
          var depth = ''
          if (position == this.props.drillinglistChild.length) {
            depth = this.props.drillinglistChild[index].Depth
            if (depth !== '') {
              this.onDeleteItem(this.props.drillinglistChild[index].No)
            }
            this.onSumDepth(this.props.drillinglistChild)
            this.saveLocal('5_5',this.props.drillinglistChild)
          }else {
            Alert.alert(I18n.t(this.state.text+'.error'), I18n.t(this.state.text+'.dontsave'), [{ text: "OK" }])
          }
        }
      }
    ],
    { cancelable: false })

  }

  onDeleteItem(index){
    var valueApi ={
      Language:I18n.locale,
      JobId: this.props.jobid,
      PileId: this.props.pileid,
      latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
      longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
      No:index
    }
    // console.log('pile5Delete',valueApi);
    this.props.pile5DeleteRowChild(valueApi)
  }

  onGetDepth(){
    if (this.state.dataMasterPile5_2) {
      var data = {
        depth:this.state.dataMasterPile5_2.PileInfo.DepthChild,
        depthSum:this.state.sumTotal,
      }
      return data
    }
  }

  onScroll =(data) =>{
    let offset = 0
    var currentOffsety = data.nativeEvent.contentOffset.y
    var currentOffsetx = data.nativeEvent.contentOffset.x
    // var direction = currentOffset > this.offset ? 'down' : 'up'
    // offset = currentOffset
    this.refs.scrollView.scrollTo({x: currentOffsetx, y: 0, animated: true})
    // console.log(currentOffsety)
    // console.log(currentOffsetx)
  }

  async saveLocal(process_step,data){
    var value = {
      process:5,
      step:5,
      pile_id: this.props.pileid,
      data:{
        CheckPlummet:this.state.checkplummet,
        CheckWaterLevel:this.state.checkwater,
        ImageCheckPlummet: this.state.images_plummet,
        ImageCheckWaterLevel: this.state.images_water,
        DrillingList:data,
      }
    }
    console.log('saveLocal 53',value);
    await this.props.mainPileSetStorage(this.props.pileid,process_step,value)
  }

  _renderTablePreview() {

    return (<View style={styles.lisTable}>
      <ScrollView horizontal={true}>
        <View style={styles.tableView}>
          <View style={styles.tableViewHeadGroup}>
              <View style={[styles.tableViewHead, styles.tableViewSM]}>
                <Text style={styles.tableViewHeadText}>{I18n.t(this.state.text+'.no')}</Text>
              </View>
            <View style={styles.tableViewHead}>
              <Text style={styles.tableViewHeadText}>{I18n.t(this.state.text+'.drill')}</Text>
            </View>
            <View style={styles.tableViewHead}>
              <Text style={styles.tableViewHeadText}>{I18n.t(this.state.text+'.drillsize')}</Text>
            </View>
            <View style={[styles.tableViewHead,styles.tableViewMD]}>
              <Text style={styles.tableViewHeadText}>{I18n.t(this.state.text+'.startdate')}</Text>
            </View>
            <View style={[styles.tableViewHead, styles.tableViewMD]}>
              <Text style={styles.tableViewHeadText}>{I18n.t(this.state.text+'.enddate')}</Text>
            </View>
            <View style={[styles.tableViewHead, styles.tableViewLG]}>
              <Text style={styles.tableViewHeadText}>{I18n.t(this.state.text+'.depthbp')}</Text>
            </View>
            <View style={styles.tableViewHead}>
              <Text style={styles.tableViewHeadText}>{I18n.t(this.state.text+'.pileb')}</Text>
            </View>
            <View style={styles.tableViewHead}>
              <Text style={styles.tableViewHeadText}>{I18n.t('cameraRoll.Photo')}</Text>
            </View>
          </View>

          {/* {
            this.state.DrillingList.map((item, key) => {
              return (<View style={styles.tableViewContentGroup}>
                <View style={styles.tableViewContent}>
                  <Text>1</Text>
                </View>

                <View style={styles.tableViewContent}>
                  <ModalSelector
                    data={this.state.surveyData}
                    onChange={survey => this.setState({ survey: survey.label })}
                    selectTextStyle={styles.textButton}
                    cancelText="Cancel">
                    <TouchableOpacity style={styles.button}>
                      <View style={styles.buttonTextStyle}>
                        <Text style={styles.buttonText}>สว่าน</Text>
                        <View style={styles.buttonSelectorIcon}>
                          <Icon name="arrow-drop-down" type="MaterialIcons"/>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </ModalSelector>
                </View>
                <TouchableOpacity onPress={() => {
                    this.setModalVisible(!this.state.modalVisible)
                  }}>
                  <View style={styles.tableViewContent}>
                    <Text style={styles.tableViewContentText}>1.00</Text>
                  </View>
                </TouchableOpacity>
                <View style={[styles.tableViewContent, styles.tableViewMD]}>
                  <Text style={styles.tableViewContentText}>8:00:00 น. (26 ธ.ค. 59)</Text>
                </View>
                <View style={[styles.tableViewContent, styles.tableViewMD]}>
                  <Text style={styles.tableViewContentText}>8:00:00 น. (26 ธ.ค. 59)</Text>
                </View>
                <TouchableOpacity onPress={() => {
                    this.setModalVisible(!this.state.modalVisible)
                  }}>
                  <View style={[styles.tableViewContent, styles.tableViewLG]}>
                    <Text style={styles.tableViewContentText}>1.04</Text>
                  </View>
                </TouchableOpacity>
                <View style={styles.tableViewContent}>
                  <Text style={styles.tableViewContentText}>-</Text>
                </View>
                <View style={styles.tableViewContent}>
                  <Text style={styles.tableViewContentText}></Text>
                </View>
              </View>)
            })
          } */}

        </View>
      </ScrollView>

      {/* modal edit data */}
      <Modal animationType={"fade"} transparent={false} visible={this.state.modalVisible} onRequestClose={() => {
          alert("Modal has been closed.")
        }}>
        <View style={styles.modalContainer}>
          <TouchableOpacity onPress={() => {
              this.setModalVisible(false)
            }} style={styles.modalCloseButton}>
            <Image source={require('../../assets/image/icon_close.png')}/>
          </TouchableOpacity>
          <View style={styles.modalDetail}>
            <Text style={styles.listTopicSubTextModal}>{I18n.t(this.state.text+'.drillsize') +I18n.t(this.state.text+'.m')}</Text>
            <TextInput keyboardType={"numeric"} underlineColorAndroid='transparent' style={[styles.listTextbox, styles.listTextboxModal]}/>
          </View>
        </View>
      </Modal>

    </View>)
  }

  _renderTable() {
    let index = 0
    // console.log(this.state.data['5_0'].data.Edit_Flag == 1);
    if (this.state.Edit_Flag == 0) {
      data = [
        {
          key: index++,
          section: true,
          label: I18n.t('mainpile.4_1.selector'),
          action: "head"
        },
        {
          key: index++,
          label: I18n.t('mainpile.4_1.see'),
          action: "edit"
        }
      ]
    }else {
      if(this.props.category == 4){
        if(this.state.status7 != 0){
          data = [
            {
              key: 0,
              section: true,
              label: I18n.t('mainpile.4_1.selector'),
              action: "head"
            },
            {
              key: 1,
              label: I18n.t('mainpile.4_1.edit'),
              action: "edit"
            }
          ]
        }else{
          data = [
            {
              key: 0,
              section: true,
              label: I18n.t('mainpile.4_1.selector'),
              action: "head"
            },
            {
              key: 1,
              label: I18n.t('mainpile.4_1.edit'),
              action: "edit"
            }, {
              key: 2,
              label: I18n.t('mainpile.4_1.delete'),
              action: 'delete'
            }
          ]
        }
      }else{
        if(this.props.shape == 1){
          if(this.state.status6 != 0){
            data = [
              {
                key: 0,
                section: true,
                label: I18n.t('mainpile.4_1.selector'),
                action: "head"
              },
              {
                key: 1,
                label: I18n.t('mainpile.4_1.edit'),
                action: "edit"
              }
            ]
          }else{
            data = [
              {
                key: 0,
                section: true,
                label: I18n.t('mainpile.4_1.selector'),
                action: "head"
              },
              {
                key: 1,
                label: I18n.t('mainpile.4_1.edit'),
                action: "edit"
              }, {
                key: 2,
                label: I18n.t('mainpile.4_1.delete'),
                action: 'delete'
              }
            ]
          }
        }else{
          if(this.state.status7 != 0){
            data = [
              {
                key: 0,
                section: true,
                label: I18n.t('mainpile.4_1.selector'),
                action: "head"
              },
              {
                key: 1,
                label: I18n.t('mainpile.4_1.edit'),
                action: "edit"
              }
            ]
          }else{
            data = [
              {
                key: 0,
                section: true,
                label: I18n.t('mainpile.4_1.selector'),
                action: "head"
              },
              {
                key: 1,
                label: I18n.t('mainpile.4_1.edit'),
                action: "edit"
              }, {
                key: 2,
                label: I18n.t('mainpile.4_1.delete'),
                action: 'delete'
              }
            ]
          }
        }
      }
      
    }
    var drillinglist = this.props.drillinglistChild
    // console.log('drillinglist1',drillinglist)
    return (<View style={styles.listTable}>


    <View style={styles.scrollData}>
        {/* start */}

              <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollLeft} onScroll={this.onScroll}>

                <View style={styles.tableRow}>

                  <View style={[styles.tableCol,{width:this.props.shape==1?'16%':'10%'}]}>
                  {
                    drillinglist.map((item, key) => {
                      var last = false
                      if(drillinglist.length-1 == key){
                        last = true
                      }
                    return (<View key={key} style={[styles.tableContent,(item.Local?{backgroundColor:'#F7F6BD',marginBottom:last?5:0}:{marginBottom:last?5:0})]}>
                       <View key={key} style={styles.tableContentCol}>
                            <ModalSelector
                              data={data}
                              cancelText="Cancel"
                              onChange={(option) => {
                                 if (option.action == "edit") {
                                   this.onEditData(key)
                                 } else if (option.action == "delete") {
                                   this.deleteRow(key)
                                 }
                               }}
                               >
                            <View style={styles.tableContentArea}>
                              {
                                item.IsBreak ? <Text style={styles.tableContentText}>{ (key + 1) + '\n' + '(หลุมพัง)' }</Text> :<Text style={styles.tableContentText}>{(key + 1)}</Text>
                              }
                              {/* <Icon name="check" reverse={true} color={!item.IsBreak ?  "grey" : "#6dcc64" } size={15} /> */}
                            </View>
                              </ModalSelector>
                              <View style={styles.tableContentBorder}></View>
                          </View>
                    </View>)
                    })
                  }
                  </View>

                  <View style={[styles.tableCol,{width:this.props.shape==1?'20%':'26%'}]}>
                  {
                        drillinglist.map((item, key) => {
                          var last = false
                          if(drillinglist.length-1 == key){
                            last = true
                          }
                          return (
                    <View key={key} style={[styles.tableContent,(item.Local?{backgroundColor:'#F7F6BD',marginBottom:last?5:0}:{marginBottom:last?5:0})]}>
                     <View key={key} style={styles.tableContentCol}>
                            <ModalSelector
                              data={data}
                              cancelText="Cancel"
                              onChange={(option) => {
                                 if (option.action == "edit") {
                                   this.onEditData(key)
                                 } else if (option.action == "delete") {
                                   this.deleteRow(key)
                                 }
                               }}
                               >
                            <View style={styles.tableContentArea}>
                              <Text style={styles.tableContentText}>{!item.DrillingToolType ? '' : item.DrillingToolType.Name }</Text>
                            </View>
                            </ModalSelector>
                            <View style={styles.tableContentBorder}></View>
                          </View>
                    </View>)
                     })
                    }
                  </View>


                  {/* <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} onScroll={this.onScroll}> */}

                    <View style={[styles.tableCol,{width:'34%'}]}>
                    {
                          drillinglist.map((item, key) => {
                            var last = false
                            if(drillinglist.length-1 == key){
                              last = true
                            }
                            return (
                      <View key={key} style={[styles.tableContent,(item.Local?{backgroundColor:'#F7F6BD',marginBottom:last?5:0}:{marginBottom:last?5:0})]}>
                       <View key={key} style={styles.tableContentCol}>
                              <ModalSelector
                                data={data}
                                cancelText="Cancel"
                                onChange={(option) => {
                                   if (option.action == "edit") {
                                     this.onEditData(key)
                                   } else if (option.action == "delete") {
                                     this.deleteRow(key)
                                   }
                                 }}
                                 >
                              <View style={styles.tableContentArea}>
                                <Text style={styles.tableContentText}>{item.Depth === null ? '' : parseFloat(item.Depth).toFixed(3)}</Text>
                              </View>
                              </ModalSelector>
                              <View style={styles.tableContentBorder}></View>
                            </View>
                          
                      </View>)
                      })
                    }
                    </View>

                    <View style={[styles.tableCol,{width:'30%'}]}>
                    {
                          drillinglist.map((item, key) => {
                            console.log('startdate05',item.StartDrilling)
                            console.log('startdate05 moment',moment(item.StartDrilling,'DD-MM-YYYY HH:mm::ss').format('DD/MM'))
                            let EndDrilling = ''
                            let Showtime = ''
                            let start = moment(item.StartDrilling,'DD/MM/YYYY HH:mm::ss').format('DD/MM')
                            let end = moment(item.EndDrilling,'DD/MM/YYYY HH:mm::ss').format('DD/MM')
                            let startHH = moment(item.StartDrilling,'DD/MM/YYYY HH:mm::ss').format('HH:mm')

                            if (item.EndDrilling != '' && start !== end ) {
                              EndDrilling = moment(item.EndDrilling,'DD/MM/YYYY HH:mm::ss').format('HH:mm')+' '+end
                              Showtime = startHH +' '+ start + '\n' + EndDrilling
                            }else if (item.EndDrilling != '') {
                              EndDrilling = moment(item.EndDrilling,'DD/MM/YYYY HH:mm::ss').format('HH:mm')
                              Showtime = startHH +' - '+ EndDrilling + '\n' + start
                            }else if(item.EndDrilling == '' && start != '' ) {
                              Showtime = startHH + '\n' + start
                            }
                            var last = false
                            if(drillinglist.length-1 == key){
                              last = true
                            }
                            return (
                      <View key={key} style={[styles.tableContent,(item.Local?{backgroundColor:'#F7F6BD',marginBottom:last?5:0}:{marginBottom:last?5:0})]}>
                       <View key={key} style={styles.tableContentCol}>
                              <ModalSelector
                                data={data}
                                cancelText="Cancel"
                                onChange={(option) => {
                                   if (option.action == "edit") {
                                     this.onEditData(key)
                                   } else if (option.action == "delete") {
                                     this.deleteRow(key)
                                   }
                                 }}
                                 >
                              <View style={styles.tableContentArea}>
                                <Text style={styles.tableContentText}>{
                                !item.StartDrilling ? ''
                                : Showtime
                             }</Text>
                            </View>
                           </ModalSelector>
                           <View style={styles.tableContentBorder}></View>
                         </View>
                         
                      </View>)
                       })
                      }
                    </View>

                </View>

                </ScrollView>

                {/* end */}
      </View>

      <Modal animationType={"fade"} transparent={false} visible={this.state.modalVisible}
        onRequestClose={() => {
          console.log(1);
        }}>
        <View style={styles.modalContainer}>
          <TouchableOpacity onPress={() => {
              this.onCloseModal()
            }} style={styles.modalCloseButton}>
            <Image source={require('../../assets/image/icon_close.png')}/>
          </TouchableOpacity>
          <View style={styles.modalDetail}>
            <Text style={styles.listTopicSubTextModal}>{this.state.titleModal}</Text>
            <TextInput
              value={this.state.depthRow.toString()}
              onChangeText={(value) => this.changeValue(value)}
              keyboardType={"numeric"}
              underlineColorAndroid='transparent'
              style={[styles.listTextbox, styles.listTextboxModal]}/>
          </View>
        </View>
      </Modal>

    </View>)
  }
  onCameraRoll(keyname,photos,type){
    var step = this.state.step
    if(this.props.category == 5){
      step = '5'
    }
    if (this.state.Edit_Flag == 1) {
      Actions.camearaRoll({jobid:this.props.jobid,process:this.state.process,step:step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:type,shape:this.props.shape,category:this.props.category,Edit_Flag:this.state.Edit_Flag})
    }else {
      Actions.camearaRoll({jobid:this.props.jobid,process:this.state.process,step:step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:'view',shape:this.props.shape,category:this.props.category,Edit_Flag:this.state.Edit_Flag})
    }
  }
  async selectedPlummet(){
    // console.warn('selectedPlummet',this.state.Edit_Flag)
    if (this.state.Edit_Flag == 1) {
      if(this.props.category == 5 && this.props.drillinglistChild.length > 0){

      }else{
        await this.setState({ checkplummet: !this.state.checkplummet })
        this.saveLocal('5_5',this.props.drillinglistChild)
      }
    }
     
  }
  async selectedWater(){
    // console.warn('selectedWater',this.state.Edit_Flag,this.props.parent)
    if (this.state.Edit_Flag == 1) {
      if(this.props.category == 5 && this.props.drillinglistChild.length > 0){

      }else{
        await this.setState({ checkwater: !this.state.checkwater })
        this.saveLocal('5_5',this.props.drillinglistChild)
      }
      
    }
  }
   render() {
    if (this.props.hidden) {
      return null
    }
    if (this.state.loading == true && this.props.pile5Type == 'content') {
      return (
        <View style={{ flex: 1,justifyContent: 'center' }}>
          <ActivityIndicator size="large" color="#007CC2" />
        </View>
      )
    }
    if(this.props.category == 5){
      if (this.props.pile5Type == 'head') {
        return(<View></View>)
      }else{
        return(
<View>
          <View>
            {this.props.category==5 &&<View style={[styles.listTopic,{marginTop: 10}]}>
            <Text style={styles.listTopicText}>{I18n.t(this.state.text+'.drill_pl')}</Text>
          </View>}
            {this.props.category==5 &&<View style={[styles.listTopic,{marginTop: 10}]}>
            <Text style={styles.listTopicText}>{I18n.t('mainpile.5_1.checkplummet2')}</Text>
          </View>}
          {this.props.category==5 &&<View style={[styles.listTopicSub2,{marginBottom:10}]}>
              <Text style={styles.listTopicSubText}>{I18n.t('mainpile.5_1.listchack')}</Text>
              <View style={styles.listCheckbox}>
                <View style={styles.listCheckboxBase}>
                  {this.state.checkplummet == true? (
                    <Icon
                      name="check"
                      reverse
                      color="#6dcc64"
                      size={15}
                      onPress={()=>this.state.status5==2?{}: this.selectedPlummet()}
                    />
                  ) : (
                    <Icon
                      name="check"
                      reverse
                      color="grey"
                      size={15}
                      onPress={()=>this.state.status5==2?{}: this.selectedPlummet()}
                    />
                  )}
                  <Text>{I18n.t('mainpile.5_1.checkplummet')}</Text>
                </View>
                <View style={styles.buttonImage}>
                  <TouchableOpacity
                    style={[styles.image,this.state.images_plummet.length>0?{backgroundColor:'#6dcc64'}:{}]}
                    onPress={() => this.onCameraRoll("images_plummet", this.state.images_plummet,this.state.Edit_Flag==0?'view':'edit')}
                  >
                    <Icon name="image" type="entypo" color="#FFF" />
                    <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.listCheckbox}>
                <View style={styles.listCheckboxBase}>
                  {this.state.checkwater == true? (
                    <Icon
                      name="check"
                      reverse
                      color="#6dcc64"
                      size={15}
                      onPress={()=> this.state.status5==2?{}: this.selectedWater()}
                    />
                  ) : (
                    <Icon
                      name="check"
                      reverse
                      color="grey"
                      size={15}
                      onPress={()=> this.state.status5==2?{}: this.selectedWater()}
                    />
                  )}
                  <Text>{I18n.t('mainpile.5_1.checkwater')}</Text>
                </View>
  
                <View style={styles.buttonImage}>
                  <TouchableOpacity
                    style={[styles.image,this.state.images_water.length>0?{backgroundColor:'#6dcc64'}:{}]}
                    onPress={() => this.onCameraRoll("images_water", this.state.images_water,this.state.Edit_Flag==0?'view':'edit')}
                  >
                    <Icon name="image" type="entypo" color="#FFF" />
                    <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                  </TouchableOpacity>
                </View>
  
              </View>
          </View>}
              <View style={styles.listTopic}>
                <Text style={styles.listTopicText}>{I18n.t(this.state.text+'.table')}</Text>
                {/* {this.renderAddData(flag_edit)} */}
                {this.state.Edit_Flag==1&&<TouchableOpacity activeOpacity={0.8} onPress={() => this.onAddData()}>
                  <View style={styles.listButton}>
                    <Image source={require('../../assets/image/icon_add.png')}/>
                    <Text style={styles.listButtonText}>{I18n.t(this.state.text+'.add')}</Text>
                  </View>
                </TouchableOpacity>}
              </View>
              <View style={styles.listTopicSub}>
                <Text style={styles.listTopicSubText}>{this.props.shape == 1?I18n.t(this.state.text+'.depth'):I18n.t(this.state.text+'.depth_dw')}</Text>
                <TextInput value={this.state.dataMasterPile5_2 == null ? '' : parseFloat(this.state.dataMasterPile5_2.PileInfo.DepthChild).toFixed(3)}
                  editable={false} keyboardType={"numeric"} underlineColorAndroid='transparent' style={[styles.listTextbox, styles.textboxDisable]}/>
                {/* <TextInput value={this.state.sumTotal.toString()}
                  editable={false} keyboardType={"numeric"} underlineColorAndroid='transparent' style={[styles.listTextbox, styles.textboxDisable]}/> */}
              </View>
            </View>
            <View style={{marginTop:10}}>
  
                <View style={styles.tableRow}>
                  <View style={[styles.tableHead,{width:this.props.shape==1?'16%':'10%'}]}>
                      <Text style={styles.tableHeadText}>#</Text>
                    </View>
                    <View style={[styles.tableHead,{width:this.props.shape==1?'20%':'26%'}]}>
                      <Text style={styles.tableHeadText}>{I18n.t(this.state.text+'.drill')}</Text>
                    </View>
                    {/* <ScrollView showsHorizontalScrollIndicator={false} scrollEnabled={false} horizontal={true} ref='scrollView'> */}
                    <View style={[styles.tableHead,{width:'34%'}]}>
                      <Text style={styles.tableHeadText}>{this.props.category == 1 ||this.props.category == 4? I18n.t(this.state.text+'.depthbp1') : I18n.t(this.state.text+'.depthdw1')}</Text>
                    </View>
                    <View style={[styles.tableHead,{width:'30%'}]}>
                      <Text style={styles.tableHeadText}>{I18n.t(this.state.text+'.timese')}</Text>
                    </View>
                    {/* <View style={[styles.tableHead, styles.tabelExtra]}>
                      <Text style={styles.tableHeadText}></Text>
                    </View> */}
                  {/* </ScrollView> */}
                </View>
            </View>
            {
            this.props.drillinglistChild.length > 0  ?
            <View style={{marginTop:this.props.pile5Type == 'head' ? 10: 0}}>
  
                <View>
                  {this._renderTable()}
                </View>
            </View>
            :
            <View>
                <View style={{marginVertical:10}}>
                  <View>
                    <Text style={styles.listTableDetailText}>{I18n.t(this.state.text+".no_data")}
                      {I18n.locale=='th'?"\n":''}
                      {I18n.locale=='th'?<Text style={styles.textHighligh}>{I18n.t(this.state.text+".add_data")}</Text>:''} 
                      {I18n.locale=='th'?I18n.t(this.state.text+".add_data_test"):''}
                    </Text>
                  </View>
                </View>
            </View>
          }
        </View>
        )
        
      }
    }else{
      if (this.props.pile5Type == 'head') {
        return (<View>
  
            <View>
            {this.props.category==5 &&<View style={[styles.listTopic,{marginTop: 10}]}>
            <Text style={styles.listTopicText}>{I18n.t(this.state.text+'.drill_pl')}</Text>
          </View>}
            {this.props.category==5 &&<View style={[styles.listTopic,{marginTop: 10}]}>
            <Text style={styles.listTopicText}>{I18n.t('mainpile.5_1.checkplummet2')}</Text>
          </View>}
          {this.props.category==5 &&<View style={[styles.listTopicSub2,{marginBottom:10}]}>
              <Text style={styles.listTopicSubText}>{I18n.t('mainpile.5_1.listchack')}</Text>
              <View style={styles.listCheckbox}>
                <View style={styles.listCheckboxBase}>
                  {this.state.checkplummet == true? (
                    <Icon
                      name="check"
                      reverse
                      color="#6dcc64"
                      size={15}
                      onPress={()=>this.state.status5==2?{}: this.selectedPlummet()}
                    />
                  ) : (
                    <Icon
                      name="check"
                      reverse
                      color="grey"
                      size={15}
                      onPress={()=>this.state.status5==2?{}: this.selectedPlummet()}
                    />
                  )}
                  <Text>{I18n.t('mainpile.5_1.checkplummet')}</Text>
                </View>
                <View style={styles.buttonImage}>
                  <TouchableOpacity
                    style={[styles.image,this.state.images_plummet.length>0?{backgroundColor:'#6dcc64'}:{}]}
                    onPress={() => this.onCameraRoll("images_plummet", this.state.images_plummet,this.state.Edit_Flag==0?'view':'edit')}
                  >
                    <Icon name="image" type="entypo" color="#FFF" />
                    <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.listCheckbox}>
                <View style={styles.listCheckboxBase}>
                  {this.state.checkwater == true? (
                    <Icon
                      name="check"
                      reverse
                      color="#6dcc64"
                      size={15}
                      onPress={()=> this.state.status5==2?{}: this.selectedWater()}
                    />
                  ) : (
                    <Icon
                      name="check"
                      reverse
                      color="grey"
                      size={15}
                      onPress={()=> this.state.status5==2?{}: this.selectedWater()}
                    />
                  )}
                  <Text>{I18n.t('mainpile.5_1.checkwater')}</Text>
                </View>
  
                <View style={styles.buttonImage}>
                  <TouchableOpacity
                    style={[styles.image,this.state.images_water.length>0?{backgroundColor:'#6dcc64'}:{}]}
                    onPress={() => this.onCameraRoll("images_water", this.state.images_water,this.state.Edit_Flag==0?'view':'edit')}
                  >
                    <Icon name="image" type="entypo" color="#FFF" />
                    <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                  </TouchableOpacity>
                </View>
  
              </View>
          </View>}
              <View style={styles.listTopic}>
                <Text style={styles.listTopicText}>{I18n.t(this.state.text+'.table')}</Text>
                {/* {this.renderAddData(flag_edit)} */}
                {this.state.Edit_Flag==1&&<TouchableOpacity activeOpacity={0.8} onPress={() => this.onAddData()}>
                  <View style={styles.listButton}>
                    <Image source={require('../../assets/image/icon_add.png')}/>
                    <Text style={styles.listButtonText}>{I18n.t(this.state.text+'.add')}</Text>
                  </View>
                </TouchableOpacity>}
              </View>
              <View style={styles.listTopicSub}>
                <Text style={styles.listTopicSubText}>{this.props.shape == 1?I18n.t(this.state.text+'.depth'):I18n.t(this.state.text+'.depth_dw')}</Text>
                <TextInput value={this.state.dataMasterPile5_2 == null ? '' : parseFloat(this.state.dataMasterPile5_2.PileInfo.DepthChild).toFixed(3)}
                  editable={false} keyboardType={"numeric"} underlineColorAndroid='transparent' style={[styles.listTextbox, styles.textboxDisable]}/>
                {/* <TextInput value={this.state.sumTotal.toString()}
                  editable={false} keyboardType={"numeric"} underlineColorAndroid='transparent' style={[styles.listTextbox, styles.textboxDisable]}/> */}
              </View>
            </View>
            <View style={{marginTop:this.props.pile5Type == 'head' ? 10: 0}}>
  
                <View style={styles.tableRow}>
                  <View style={[styles.tableHead,{width:this.props.shape==1?'16%':'10%'}]}>
                      <Text style={styles.tableHeadText}>#</Text>
                    </View>
                    <View style={[styles.tableHead,{width:this.props.shape==1?'20%':'26%'}]}>
                      <Text style={styles.tableHeadText}>{I18n.t(this.state.text+'.drill')}</Text>
                    </View>
                    {/* <ScrollView showsHorizontalScrollIndicator={false} scrollEnabled={false} horizontal={true} ref='scrollView'> */}
                    <View style={[styles.tableHead,{width:'34%'}]}>
                      <Text style={styles.tableHeadText}>{this.props.category == 1 ||this.props.category == 4 ? I18n.t(this.state.text+'.depthbp1') : I18n.t(this.state.text+'.depthdw1')}</Text>
                    </View>
                    <View style={[styles.tableHead,{width:'30%'}]}>
                      <Text style={styles.tableHeadText}>{I18n.t(this.state.text+'.timese')}</Text>
                    </View>
                    {/* <View style={[styles.tableHead, styles.tabelExtra]}>
                      <Text style={styles.tableHeadText}></Text>
                    </View> */}
                  {/* </ScrollView> */}
                </View>
            </View>
  
        </View>)
      }else{
        return (<View>
          {
            this.props.drillinglistChild.length > 0  ?
            <View style={{marginTop:this.props.pile5Type == 'head' ? 10: 0}}>
  
                <View>
                  {this._renderTable()}
                </View>
            </View>
            :
            <View>
                <View style={styles.listTableDetail}>
                  <View>
                    <Text style={styles.listTableDetailText}>{I18n.t(this.state.text+".no_data")}
                      {I18n.locale=='th'?"\n":''}
                      {I18n.locale=='th'?<Text style={styles.textHighligh}>{I18n.t(this.state.text+".add_data")}</Text>:''} 
                      {I18n.locale=='th'?I18n.t(this.state.text+".add_data_test"):''}
                    </Text>
                  </View>
                </View>
            </View>
          }
  
        </View>)
      }
    }
    

  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll || null,
    error: state.pile.error,
    drillinglist: state.drillinglist.drillinglist,
    drillinglistChild: state.drillinglist.drillinglistChild,
  }
}

export default connect(mapStateToProps, actions, null, {withRef: true})(Pile5_5);
