import React, { Component } from "react"
import {
  View,
  Text,
  TouchableOpacity,
  TextInput
} from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"
import I18n from "../../assets/languages/i18n"
import styles from "./styles/Pile7_2.style"
import { MENU_GREY_ITEM, SUB_COLOR } from "../Constants/Color"
import { Icon } from "react-native-elements"
import RNTooltips from "react-native-tooltips"

class Pile11_3 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      distance: "0",
      distanceCalled: false,
      pco: "0",
      pcoCalled: false,
      metal: "0",
      topcasing: 0,
      process: 11,
      step: 3,
      sub1: "",
      sub2: "",
      sub3: "",
      Edit_Flag: 1,
      ground: 0,
      cutoff: 0,
      visible: false,
      dismiss: false
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ visible: false })
    var pile11 = null
    var pile = null
    var pile33 = null
    if (
      nextProps.pileAll[this.props.pileid] &&
      nextProps.pileAll[this.props.pileid]["11_0"]
    ) {
      pile11 = nextProps.pileAll[this.props.pileid]["11_0"].data
      pile = nextProps.pileAll[this.props.pileid]["11_3"].data
    }
    if (
      this.props.pileAll[this.props.pileid] &&
      this.props.pileAll[this.props.pileid]["3_3"]
    ) {
      pile33 = this.props.pileAll[this.props.pileid]["3_3"].data
    }
    if (pile) {
      if (
        pile.ConcreteLevelBeforeWithdraw !== null &&
        pile.ConcreteLevelBeforeWithdraw !== ""
      ) {
        // console.warn("pile step11", pile)
        let transub1 = parseFloat(pile.ConcreteLevelBeforeWithdraw).toFixed(3)
        if (isNaN(transub1)) {
          // transub1 = 0
          // let transub1a2 = parseFloat(transub1).toFixed(3)
          this.setState({ sub1: null }, () => {
            console.log("pile step11 sub1", this.state.sub1)
            // this.saveLocal()
          })
        } else {
          this.setState({ sub1: transub1 }, () => {
            console.log("pile step11 sub12", this.state.sub1)
            // console.warn("SUB12", this.state.sub1)
            // this.saveLocal()
          })
        }
      } else {
        this.setState({ sub1: null }, () => {
          console.log("pile step11 sub12", this.state.sub1)
          // console.warn("SUB12", this.state.sub1)
          // this.saveLocal()
        })
      }

      if (
        pile.ConcreteLevelAfterWithdraw !== null &&
        pile.ConcreteLevelAfterWithdraw !== ""
      ) {
        let transub2 = parseFloat(pile.ConcreteLevelAfterWithdraw).toFixed(3)

        if (isNaN(transub2)) {
          // transub2 = 0
          // let transub2a2 = parseFloat(transub2).toFixed(3)
          this.setState({ sub2: null }, () => {
            // console.warn("SUB1", this.state.sub2)
            // this.saveLocal()
          })
        } else {
          this.setState({ sub2: transub2 }, () => {
            console.log("SUB2", this.state.sub2)
            // this.saveLocal()
          })
        }
      } else {
        this.setState({ sub2: null }, () => {
          console.log("pile step11 sub12", this.state.sub2)
          // console.warn("SUB12", this.state.sub1)
          // this.saveLocal()
        })
      }
      var ground = 0
      if (pile33 && pile33.ground) {
        ground = pile33.ground
        this.setState({ ground }, () => {
          let input = this.state.ground - this.state.sub1 - this.state.cutoff
          this.setState({ sub2: input })
        })
      }

      if (pile.ConcreteBleeding !== null && pile.ConcreteBleeding !== "") {
        console.log("pile step11ConcreteBleeding", pile.ConcreteBleeding)
        let transub3 = parseFloat(pile.ConcreteBleeding).toFixed(3)
        if (isNaN(transub3)) {
          transub3 = 0
          let transub3a2 = parseFloat(transub3).toFixed(3)
          this.setState({ sub3: transub3a2 }, () => {
            // console.warn("SUB3", this.state.sub3)
            // this.saveLocal()
          })
        } else {
          this.setState({ sub3: transub3 }, () => {
            // console.warn("SUB3", this.state.sub3)
            // this.saveLocal()
          })
        }
      } else {
        console.log("pile step11ConcreteBleedingelse", pile.ConcreteBleeding)
        this.setState({ sub3: null }, () => {
          // console.log("pile step11 sub12", this.state.sub3)
          // console.warn("SUB12", this.state.sub1)
          // this.saveLocal()
        })
      }
    }

    if (pile11) {
      this.setState({ Edit_Flag: pile11.Edit_Flag })
    }
  }

  //   var pile = null
  //   var pileMaster = null
  //   if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["11_3"]) {
  //     pile = nextProps.pileAll[this.props.pileid]["11_3"].data
  //     pileMaster = nextProps.pileAll[this.props.pileid]["11_3"].masterInfo
  //   }

  //   if (pile) {
  //     if (pile.ConcreteLevelBeforeWithdraw) {
  //       this.setState({ sub1: pile.ConcreteLevelBeforeWithdraw })
  //     }
  //     if (pile.ConcreteLevelAfterWithdraw) {
  //       this.setState({ sub2: pile.ConcreteLevelAfterWithdraw })
  //     }
  //     if (pile.ConcreteBleeding) {
  //       this.setState({ sub3: pile.ConcreteBleeding })
  //     }
  //   }

  componentDidMount() {
    var pile = null
    var pileMaster = null
    var pile33 = null
    // console.warn("componentDidMount")
    if (
      this.props.pileAll[this.props.pileid] &&
      this.props.pileAll[this.props.pileid]["11_3"]
    ) {
      pile = this.props.pileAll[this.props.pileid]["11_3"].data
      pileMaster = this.props.pileAll[this.props.pileid]["11_3"].masterInfo
    }

    if (
      this.props.pileAll[this.props.pileid] &&
      this.props.pileAll[this.props.pileid]["3_3"]
    ) {
      pile33 = this.props.pileAll[this.props.pileid]["3_3"].data
    }

    if (pile) {
      console.log("pile step11", pile)
      if (
        pile.ConcreteLevelBeforeWithdraw !== null &&
        pile.ConcreteLevelBeforeWithdraw !== ""
      ) {
        // console.warn("pile step11", pile)
        let transub1 = parseFloat(pile.ConcreteLevelBeforeWithdraw).toFixed(3)
        if (isNaN(transub1)) {
          // transub1 = 0
          // let transub1a2 = parseFloat(transub1).toFixed(3)
          this.setState({ sub1: null }, () => {
            // console.warn("SUB1", this.state.sub1)
            // this.saveLocal()
          })
        } else {
          this.setState({ sub1: transub1 }, () => {
            // console.warn("SUB12", this.state.sub1)
            // this.saveLocal()
          })
        }
      } else {
        // transub1 = 0
        // let transub1a2 = parseFloat(transub1).toFixed(3)

        this.setState({ sub1: null }, () => {
          // this.saveLocal()
        })
      }

      if (
        pile.ConcreteLevelAfterWithdraw !== null &&
        pile.ConcreteLevelAfterWithdraw !== ""
      ) {
        let transub2 = parseFloat(pile.ConcreteLevelAfterWithdraw).toFixed(3)

        if (isNaN(transub2)) {
          // transub2 = 0
          // let transub2a2 = parseFloat(transub2).toFixed(3)
          this.setState({ sub2: null }, () => {
            // console.warn("SUB1", this.state.sub1)
            // this.saveLocal()
          })
        } else {
          this.setState({ sub2: transub2 }, () => {
            console.log("SUB2", this.state.sub2)
            // this.saveLocal()
          })
        }
      } else {
        this.setState({ sub2: null }, () => {
          // this.saveLocal()
        })
      }
      if (
        pile.ConcreteBleeding !== null &&
        pile.ConcreteLevelBeforeWithdraw !== ""
      ) {
        // console.warn("pile step11", pile.ConcreteBleeding)
        let transub3 = parseFloat(pile.ConcreteBleeding).toFixed(3)
        if (isNaN(transub3)) {
          transub3 = 0
          let transub3a2 = parseFloat(transub3).toFixed(3)
          this.setState({ sub3: transub3a2 }, () => {
            // console.warn("SUB1", this.state.sub1)
            // this.saveLocal()
          })
        } else {
          this.setState({ sub3: transub3 }, () => {
            // console.warn("SUB12", this.state.sub1)
            // this.saveLocal()
          })
        }
      } else {
        this.setState({ sub3: null }, () => {
          // console.warn("SUB1", this.state.sub1)
          // this.saveLocal()
        })
      }
    }
    //   let transub3 = parseFloat(pile.ConcreteBleeding).toFixed(3)
    //   if (isNaN(transub3)) {
    //     transub3 = null
    //   }
    //   this.setState({ sub3: transub3 }, () => {
    //     console.log("SUB3", this.state.sub3)
    //     this.saveLocal()
    //   })
    // } else {
    //   this.setState({ sub3: parseFloat(0).toFixed(3) }, () => {
    //     console.log("SUB3", this.state.sub3)
    //     this.saveLocal()
    //   })
    // }

    if (pileMaster) {
      if (pileMaster.cutoff) {
        this.setState({ cutoff: pileMaster.cutoff })
      }
    }

    var ground = 0
    if (pile && pile.ground) {
      ground = pile.ground
    }
    if (pile33 && pile33.ground) {
      ground = pile33.ground
    }

    this.setState({ ground }, () => {
      let input = this.state.ground - this.state.sub1 - this.state.cutoff
      this.setState({ sub2: input })
    })

    // if(pile.startdate == '' || pile.startdate == null || pile.startdate == undefined){
    //   this.setState({
    //     datestart: moment().format("DD/MM/YYYY"),
    //     timestart: moment().format("HH:mm:ss"),
    //   })
    //   this.saveLocal()
    // }
    // if(pile.enddate == '' || pile.enddate == null || pile.enddate == undefined){
    //   this.setState({
    //     dateend: moment().format("DD/MM/YYYY"),
    //     timeend: moment().format("HH:mm:ss"),
    //   })
    //   this.saveLocal()
    // }
    // this.setState({
    //   datestart: moment().format("DD/MM/YYYY"),
    //   dateend: moment().format("DD/MM/YYYY"),
    //   timeend: moment().format("HH:mm:ss"),
    //   timestart: moment().format("HH:mm:ss"),
    // })
    // this.saveLocal()
  }

  onChangeValue = value => {
    // ground - value - cutoff
    // var input = this.state.ground - parseFloat(value) - this.state.cutoff
    // if (isNaN(input)) {
    //   input = 0
    // }
    // this.setState({ sub1: value, sub2: input.toFixed(3),visible:false }, () =>
    //   this.saveLocal()
    // )
  }

  onChangeFormatDecimal(data) {
    if (data.split(".")[1]) {
      if (data.split(".")[1].length > 3) {
        return parseFloat(data).toFixed(3)
      } else {
        return data
      }
    } else {
      return data
    }
  }
  async saveLocal(distance: String, pco: String, metal: String) {
    var value = {
      process: 3,
      step: 11,
      pile_id: this.props.pileid,
      data: {
        ConcreteLevelBeforeWithdraw: this.state.sub1,
        ConcreteLevelAfterWithdraw: this.state.sub2,
        ConcreteBleeding: this.state.sub3
      }
    }
    // console.warn(value)
    this.props.mainPileSetStorage(this.props.pileid, "11_3", value)
  }

  render() {
    if (this.props.hidden) {
      return null
    }

    return (
      <View>
        <RNTooltips
          text={I18n.t("tooltip.11_3")}
          textSize={16}
          visible={this.state.visible}
          reference={this.main}
          tintColor="#007CC2"
        />
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t("mainpile.11_3.topic")}</Text>
        </View>
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.11_3.sub1_edit") + " "}</Text>
          </View>
          <View style={styles.selectedView}>
            <View
              style={[
                styles.button,
                { height: 50, padding: 0 },
                this.state.Edit_Flag != "0" && { borderColor: SUB_COLOR }
              ]}
            >
              <TextInput
                placeholder={I18n.t("mainpile.11_3.sub1_edit")}
                editable={this.state.Edit_Flag == 1}
                keyboardType="numeric"
                value={
                  this.state.sub1 !== null ? this.state.sub1.toString() : ""
                }
                onChangeText={value => {
                  this.setState({ sub1: value }, () => {
                    // this.saveLocal()
                  })
                }}
                underlineColorAndroid="transparent"
                style={[
                  this.state.Edit_Flag == 1
                    ? styles.textInputStyle
                    : styles.textInputStyleGrey,
                  this.state.Edit_Flag != "0" && { color: SUB_COLOR }
                ]}
                onEndEditing={() => {
                  this.setState(
                    {
                      sub1: this.state.sub1
                        ? parseFloat(Math.abs(this.state.sub1)).toFixed(3)
                        : "",
                      visible: false
                    },
                    () => {
                      input =
                        this.state.ground - this.state.sub1 - this.state.cutoff
                      this.setState({ sub2: input.toFixed(3) }, () => {
                        this.saveLocal()
                      })
                    }
                    // this.saveLocal()
                  )
                }}
              />
            </View>
          </View>
        </View>
        <View>
          <View style={[styles.boxTopic, { flexDirection: "row" }]}>
            <Text>{I18n.t("mainpile.11_3.sub2_edit")}</Text>
            <Icon
              name="ios-information-circle"
              type="ionicon"
              color="#517fa4"
              onPress={() => {
                this.setState({
                  visible: true,
                  dismiss: false
                  // reference:this.main
                })
              }}
            />
          </View>
          <View style={styles.selectedView}>
            <View
              style={[
                styles.button,
                { height: 50, padding: 0, borderColor: MENU_GREY_ITEM }
              ]}
              ref={ref => {
                this.main = ref
              }}
            >
              <TextInput
                placeholder={I18n.t("mainpile.11_3.sub2_edit")}
                editable={false}
                keyboardType="numeric"
                value={
                  // let transub1 = parseFloat(pile.ConcreteLevelBeforeWithdraw).toFixed(3)
                  //   if (isNaN(transub1)) {}
                  isNaN(parseFloat(this.state.sub2).toFixed(3)) == true ? "": parseFloat(this.state.sub2).toFixed(3)
                  
                  // this.state.sub2 !== null ? this.state.sub2.toString() : ""
                }
                onChangeText={sub2 =>
                  this.setState(
                    { sub2, visible: false },
                    () => {}
                    // this.saveLocal()
                  )
                }
                underlineColorAndroid="transparent"
                style={[
                  styles.textInputStyleGrey
                 
                ]}
                onEndEditing={() =>
                  this.setState(
                    {
                      sub2: this.state.sub2
                        ? parseFloat(this.state.sub2).toFixed(3)
                        : "",
                      visible: false
                    },
                    () => {
                      this.saveLocal()
                    }
                    //  this.saveLocal()
                  )
                }
              />
            </View>
          </View>
        </View>
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.11_3.sub3_edit")}</Text>
          </View>
          <View style={styles.selectedView}>
            <View
              style={[
                styles.button,
                { height: 50, padding: 0 },
                this.state.Edit_Flag != "0" && { borderColor: SUB_COLOR }
              ]}
            >
              <TextInput
                placeholder={I18n.t("mainpile.11_3.sub3_edit")}
                editable={this.state.Edit_Flag == 1}
                keyboardType="numeric"
                value={
                  this.state.sub3 !== null ? this.state.sub3.toString() : ""
                }
                onChangeText={sub3 =>
                  this.setState(
                    { sub3, visible: false },
                    () => {}
                    // this.saveLocal()
                  )
                }
                underlineColorAndroid="transparent"
                style={[
                  this.state.Edit_Flag == 1
                    ? styles.textInputStyle
                    : styles.textInputStyleGrey,
                  this.state.Edit_Flag != "0" && { color: SUB_COLOR }
                ]}
                onEndEditing={() => {
                  if (this.state.sub3 !== null && this.state.sub3 !== "") {
                    console.log("this.state.sub3 ", this.state.sub3)
                    this.setState(
                      {
                        sub3: parseFloat(this.state.sub3).toFixed(3),
                        visible: false
                      },
                      () => {
                        this.saveLocal()
                      }
                    )
                  } else {
                    this.setState(
                      {
                        sub3: "",
                        visible: false
                      },
                      () => {
                        this.saveLocal()
                      }
                    )
                  }
                }}
              />
            </View>
          </View>
        </View>
        <View />
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(
  Pile11_3
)
