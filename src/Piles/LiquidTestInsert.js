import React, {Component} from "react"
import {
  Alert,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  BackHandler,
  Modal,
  ScrollView
} from "react-native"
import {connect} from "react-redux"
import {Container, Content} from "native-base"
import {Actions} from 'react-native-router-flux'
import TableView from "../Components/TableView"
import {MAIN_COLOR, SUB_COLOR, BASIC_COLOR, MENU_GREY_ITEM, DEFAULT_COLOR_1, DEFAULT_COLOR_4} from "../Constants/Color"
import {Icon} from "react-native-elements"
import I18n from '../../assets/languages/i18n'
import ModalSelector from "react-native-modal-selector"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import styles from './styles/LiquidTestInsert.style'
import * as actions from "../Actions"
import Loader from '../Components/Loader'
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from "moment"
class LiquidTestInsert extends Component {
  constructor(props) {
    super(props)
    this.state = {
        ProcessTesterListData: [{ key: 0, section: true, label: I18n.t("mainpile.liquidtestinsert.processtesterlistselect") }],
        SubProcessTesterListData: [{ key: 0, section: true, label: I18n.t("mainpile.liquidtestinsert.name_sludgeselect") }],
        TesterListData: [{ key: 0, section: true, label: I18n.t("mainpile.liquidtestinsert.testerlistselect") }],
        ImagePhFiles:[],
        Ph:'',
        ImageDensityFiles:[],
        Density:'',
        ImageViscosityFiles:[],
        Viscosity:'',
        ImageSandFiles:[],
        Sand:'',
        ProcessTesterList:'',
        ProcessTesterListID:'',
        ProcessTesterListNo:'',
        SubProcessTesterList:'',
        SubProcessTesterListID:'',
        SubProcessTesterListNo:'',
        SlurryList:'',
        SlurryListID:'',
        TesterList:'',
        TesterListID:'',
        data:null,
        Edit_Flag:1,
        SubDisable:true,
        loader:false,
        datevisibletime:false,
        datevisibledate:false,
        startdate:'',
        enddate:'',
        type:'',
        datestart:'',
        timestart:'',
        dateend:'',
        timeend:'',
        processtesterlistM:[],
        slurryListData: [{ key: 0, section: true, label: I18n.t("mainpile.liquidtestinsert.name_slurry") }],
        slurryDisable:true,

        Phtemp:'',
        Densitytemp:'',
        Viscositytemp:'',
        Sandtemp:'',

        PhMax:'',
        PhMin:'',
        DensityMax:'',
        DensityMin:'',
        ViscosityMax:'',
        ViscosityMin:'',
        PercentSandMax:'',
        PercentSandMin:'',

        Ph_flagend:true,
        Density_flagend:true,
        Viscosity_flagend:true,
        Sand_flagend:true,

        datamaster:null
    }
  }
  componentWillMount() {
   
  }
  async componentDidMount() {
    BackHandler.addEventListener("back", this._onBack)
    console.log(this.props.Edit_Flag)
    
    var pile = null
    var pileMaster = null
    let checkProcessTester = false
    if(this.props.pileAll[this.props.pileid] && this.props.pileAll[this.props.pileid]['4_1']) {
      pile = this.props.pileAll[this.props.pileid]['4_1'].data
      pileMaster = this.props.pileAll[this.props.pileid]['4_1'].masterInfo
      this.setState({data:pile, datamaster: pileMaster})
      console.warn('pileMaster.TesterList',pileMaster.TesterList)
      pileMaster.ProcesstesterList.map(data=>{
        this.state.processtesterlistM.push({id:data.Id, count:0,PositionFlag:data.PositionFlag,SlurryTypeFlag:data.SlurryTypeFlag})
      })
      var templist = null
      
      if(this.props.LiquidTestInsertType == 'edit'){
        templist = this.props.drillingFluidslistedit
      }else{
        templist = this.props.drillingFluidslist
      }
      // console.warn('this.props.drillingFluidslist',templist.length)
      if (templist.length > 0) {
        for (var i = 0; i < templist.length; i++) {
          for(var j = 0; j < this.state.processtesterlistM.length; j++){
            // console.warn('test',templist[i].TypeId,this.state.processtesterlistM[j].id)
            if(templist[i].TypeId == this.state.processtesterlistM[j].id){
              this.state.processtesterlistM[j].count++
            }
          }
        }
      }
    }
    if(this.props.edit){
      // console.warn('drillingFluidslist',this.props.drillingFluidslistedit.length)
    if (this.props.drillingFluidslist != undefined) {
        var pflag = false
        var sflag = false
        for(var k=0; k<this.state.processtesterlistM.length;k++){
          // console.warn('ttt',this.state.processtesterlistM[k].id , this.props.drillingFluidslist.TypeId)
          if(this.state.processtesterlistM[k].id == this.props.drillingFluidslist.TypeId){
            if(this.props.drillingFluidslistedit.length > 1){
              // console.warn('PositionFlag',this.state.processtesterlistM[k].PositionFlag)
              pflag = this.state.processtesterlistM[k].PositionFlag
              sflag = false
            }else{
              pflag = this.state.processtesterlistM[k].PositionFlag
              sflag = this.state.processtesterlistM[k].SlurryTypeFlag
            }
           
          }
        }
        
         await this.setState({
           ImagePhFiles:this.props.drillingFluidslist.ImagePh,
           Ph:isNaN(parseFloat(this.props.drillingFluidslist.Ph).toFixed(2)) ? '' : parseFloat(this.props.drillingFluidslist.Ph).toFixed(2) ,
           ImageDensityFiles:this.props.drillingFluidslist.ImageDensity,
           Density:isNaN(parseFloat(this.props.drillingFluidslist.Density).toFixed(2)) ? '' : parseFloat(this.props.drillingFluidslist.Density).toFixed(2) ,
           ImageViscosityFiles:this.props.drillingFluidslist.ImageViscosity,
           Viscosity:this.props.drillingFluidslist.Viscosity,
           ImageSandFiles:this.props.drillingFluidslist.ImageSand,
           Sand:isNaN(parseFloat(this.props.drillingFluidslist.Sand).toFixed(2)) ? '' : parseFloat(this.props.drillingFluidslist.Sand).toFixed(2) ,
           // ProcessTesterList:this.props.drillingFluidslist.ProcessTesterList,
           ProcessTesterListID:this.props.drillingFluidslist.TypeId,
           SubProcessTesterListID:this.props.drillingFluidslist.PositionId,
           // ProcessTesterListNo:this.props.drillingFluidslist.ProcessTesterListNo,
           // TesterList:this.props.drillingFluidslist.TesterList,
           SlurryListID:this.props.drillingFluidslist.SlurryTypeId,
           TesterListID:this.props.drillingFluidslist.TesterERPId,
           Edit_Flag:this.props.Edit_Flag,
           SubDisable: pflag ? false : true,
          // SubDisable: false ,
           slurryDisable: sflag ? false : true,
          // slurryDisable: true,
           datestart:moment(this.props.drillingFluidslist.startdate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY"),
           timestart:moment(this.props.drillingFluidslist.startdate,"DD/MM/YYYY HH:mm:ss").format("HH:mm"),
           dateend:moment(this.props.drillingFluidslist.enddate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY"),
           timeend:moment(this.props.drillingFluidslist.enddate,"DD/MM/YYYY HH:mm:ss").format("HH:mm"),
           startdate:moment(this.props.drillingFluidslist.startdate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY HH:mm"),
           enddate:moment(this.props.drillingFluidslist.enddate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY HH:mm"),

           Phtemp:isNaN(parseFloat(this.props.drillingFluidslist.Ph).toFixed(2)) ? '' : parseFloat(this.props.drillingFluidslist.Ph).toFixed(2),
           Densitytemp:isNaN(parseFloat(this.props.drillingFluidslist.Density).toFixed(2)) ? '' : parseFloat(this.props.drillingFluidslist.Density).toFixed(2),
           Viscositytemp:this.props.drillingFluidslist.Viscosity,
           Sandtemp:isNaN(parseFloat(this.props.drillingFluidslist.Sand).toFixed(2)) ? '' : parseFloat(this.props.drillingFluidslist.Sand).toFixed(2),
          })
      }
    }else{
      if(this.state.datestart == '' && this.state.timestart == ''){
        this.setState({
          datestart:moment().format("DD/MM/YYYY"),
          timestart:moment().format("HH:mm"),
          dateend:'',
          timeend:'',
          startdate:moment().format("DD/MM/YYYY HH:mm"),
          enddate:'',
        })
      }else{
        this.setState({
          datestart:moment(this.state.datestart,"DD/MM/YYYY").format("DD/MM/YYYY"),
          timestart:moment(this.state.timestart,"HH:mm").format("HH:mm"),
          dateend:'',
          timeend:'',
          startdate:moment(this.state.timestart).format("DD/MM/YYYY HH:mm"),
          enddate:'',
      })
    }
    }
      
    if (pile && Actions.currentScene == 'liquidTestInsert') {
     
      if (this.state.ProcessTesterListData != pileMaster.ProcesstesterList && pileMaster.ProcesstesterList != '' && this.state.ProcessTesterListData.length == 1) {
        let list = [{ key: 0, section: true, label: I18n.t("mainpile.liquidtestinsert.processtesterlistselect")  }]
        for (var i = 0; i < pileMaster.ProcesstesterList.length; i++) {
          if (pileMaster.ProcesstesterList[i].Id == this.state.ProcessTesterListID) {
            this.setState({
              ProcessTesterList: pileMaster.ProcesstesterList[i].Name,
              ProcessTesterListNo: pileMaster.ProcesstesterList[i].No,
              ProcessTesterListID: pileMaster.ProcesstesterList[i].Id,
            })
            if (pileMaster.ProcesstesterList[i].PositionFlag == true) {
              let sublist = [{ key: 0, section: true, label: I18n.t("mainpile.liquidtestinsert.name_sludgeselect")  }]
              for (var j = 0; j < pileMaster.ProcesstesterList[i].PositionList.length; j++) {
                sublist.push(
                  {
                    key: pileMaster.ProcesstesterList[i].PositionList[j].Id,
                    label: pileMaster.ProcesstesterList[i].PositionList[j].Name,
                    no: pileMaster.ProcesstesterList[i].PositionList[j].No,
                  }
                )
                if (pileMaster.ProcesstesterList[i].PositionList[j].Id == this.state.SubProcessTesterListID) {
                  this.setState({
                    SubProcessTesterList: pileMaster.ProcesstesterList[i].PositionList[j].Name,
                    SubProcessTesterListNo: pileMaster.ProcesstesterList[i].PositionList[j].No,
                    SubProcessTesterListID: pileMaster.ProcesstesterList[i].PositionList[j].Id,
                  })
                }
              }
              this.setState({SubProcessTesterListData:sublist})
            }
           
            if(pileMaster.ProcesstesterList[i].SlurryTypeFlag == true){
              let slurrylist = [{ key: 0, section: true, label: I18n.t("mainpile.liquidtestinsert.name_slurry")  }]
              for (var j = 0; j < pileMaster.ProcesstesterList[i].SlurryTypeList.length; j++) {
                slurrylist.push(
                  {
                    key: pileMaster.ProcesstesterList[i].SlurryTypeList[j].Id,
                    label: pileMaster.ProcesstesterList[i].SlurryTypeList[j].Name,
                  }
                )
                if (pileMaster.ProcesstesterList[i].SlurryTypeList[j].Id == this.state.SlurryListID) {
                  this.setState({
                    SlurryList: pileMaster.ProcesstesterList[i].SlurryTypeList[j].Name,
                    SlurryListID: pileMaster.ProcesstesterList[i].SlurryTypeList[j].Id,
                  })
                }
              }
              this.setState({slurryListData:slurrylist})
            }

          }
          list.push({
            key: pileMaster.ProcesstesterList[i].Id,
            label: pileMaster.ProcesstesterList[i].Name,
            no: pileMaster.ProcesstesterList[i].No,
            PositionFlag:pileMaster.ProcesstesterList[i].PositionFlag,
            PositionList:pileMaster.ProcesstesterList[i].PositionList,
            SlurryTypeFlag:pileMaster.ProcesstesterList[i].SlurryTypeFlag,
            SlurryTypeList:pileMaster.ProcesstesterList[i].SlurryTypeList
          })
          if(this.props.drillingFluidslist.length == 0){
            list.splice(2,pileMaster.ProcesstesterList.length - 1)
          }else{
            var indexindex = 0
            if(this.props.LiquidTestInsertType == 'edit'){
              // console.warn('editedit',this.state.processtesterlistM)
              for(var h=this.state.processtesterlistM.length-1;h>=0;h--){
                if(this.state.processtesterlistM[h].id == this.props.drillingFluidslist.TypeId){
                  indexindex = h
                  break
                }
              }
              // console.warn('index',indexindex)
              list.splice(indexindex+2,pileMaster.ProcesstesterList.length - 1)
            }else{
              var index = 0
              // console.warn('processtesterlistM',this.state.processtesterlistM)
              for(var h=this.state.processtesterlistM.length-1;h>=0;h--){
                if(this.state.processtesterlistM[h].count > 0){
                  index = h
                  break
                }
              }
              // console.warn('index',index,pileMaster.ProcesstesterList.length)
              list.splice(index+3,pileMaster.ProcesstesterList.length - 1)
            }
          }

        }
        await this.setState({ProcessTesterListData:list})
      }

      //Tester list
      if (this.state.TesterListData != pileMaster.TesterList && pileMaster.TesterList != '' && this.state.TesterListData.length == 1) {
        console.warn('pileMaster.TesterList',pileMaster.TesterList)
        let list = [{ key: 0, section: true, label: I18n.t("mainpile.liquidtestinsert.testerlistselect")  }]
        for (var i = 0; i < pileMaster.TesterList.length; i++) {
          if (pileMaster.TesterList[i].employee_id == this.state.TesterListID) {
            this.setState({
              TesterList: (pileMaster.TesterList[i].nickname && pileMaster.TesterList[i].nickname + "-") + pileMaster.TesterList[i].firstname + " " + pileMaster.TesterList[i].lastname,
              TesterListID: pileMaster.TesterList[i].employee_id,
            })
          }
           list.push({
            key: pileMaster.TesterList[i].employee_id,
            label: (pileMaster.TesterList[i].nickname && pileMaster.TesterList[i].nickname + "-") + pileMaster.TesterList[i].firstname + " " + pileMaster.TesterList[i].lastname,
          })
        }
          await this.setState({TesterListData:list})
      }
    }
    this.setMaxMin(this.state.datamaster.MaxMinList)
  }
  componentWillUnmount() {
    BackHandler.removeEventListener("back", this._onBack)
  }
  componentWillMount(){
    this.props.mainPileGetAllStorage()
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.action == "update"){
      this.setState(nextProps.value.data)
    }
  }
  setMaxMin(MaxMinList){
    
    var ProcessTesterListID = this.state.ProcessTesterListID == ''?null:this.state.ProcessTesterListID
    var SubProcessTesterListID = this.state.SubProcessTesterListID == ''?null:this.state.SubProcessTesterListID
    var SlurryListID = this.state.SlurryListID == ''?null:this.state.SlurryListID
    for(var i = 0 ; i < MaxMinList.length ; i++){
      // console.warn('setMaxMin',ProcessTesterListID,SubProcessTesterListID,SlurryListID)
      if(
        ProcessTesterListID == MaxMinList[i].ProcesstesterId &&
        SubProcessTesterListID == MaxMinList[i].PositionId &&
        SlurryListID == MaxMinList[i].SlurryTypeId
      ){
        // console.warn('setMaxMin if',MaxMinList[i])
        this.setState({
          PhMax:MaxMinList[i].PhMax,
          PhMin:MaxMinList[i].PhMin,
          DensityMax:MaxMinList[i].DensityMax,
          DensityMin:MaxMinList[i].DensityMin,
          ViscosityMax:MaxMinList[i].ViscosityMax,
          ViscosityMin:MaxMinList[i].ViscosityMin,
          PercentSandMax:MaxMinList[i].PercentSandMax,
          PercentSandMin:MaxMinList[i].PercentSandMin,
        })
      }
    }
  }
  getDate(){
    if(this.state.type == 'start'){
      if(this.state.startdate != ''){
        return new Date(moment(this.state.startdate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
      }else{
          return new Date()
      }
    }else{
      if(this.state.enddate != ''){
        return new Date(moment(this.state.enddate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
      }else{
          return new Date()
      }
    }
  }

  onCameraRoll(keyname,photos,type){
    if (this.state.Edit_Flag == 1 ) {
      Actions.camearaRoll({jobid:this.props.jobid,process:this.props.process,step:this.props.step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:type,shape:this.props.shape,category:this.props.category,Edit_Flag:this.state.Edit_Flag})
    }else {
      Actions.camearaRoll({jobid:this.props.jobid,process:this.props.process,step:this.props.step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:'view',shape:this.props.shape,category:this.props.category,Edit_Flag:this.state.Edit_Flag})
    }
  }
  selectedSlurry = async Slurry =>{
    // console.warn('Slurry',Slurry)
    await this.setState({ 
      SlurryList: Slurry.label,
      SlurryListID:Slurry.key
    },()=> this.setMaxMin(this.state.datamaster.MaxMinList))
  }
  selectedProcessTester = async ProcessTester => {
    let sublist = [{ key: 0, section: true, label: I18n.t("mainpile.liquidtestinsert.name_sludgeselect")  }]
    // let sub = []
    if (ProcessTester.PositionFlag == true) {
      for (var i = 0; i < ProcessTester.PositionList.length; i++) {
        sublist.push(
          {
            key: ProcessTester.PositionList[i].Id,
            label: ProcessTester.PositionList[i].Name,
            no: ProcessTester.PositionList[i].No,
          }
        )
      }
      await this.setState({ ProcessTesterList: ProcessTester.label,ProcessTesterListID:ProcessTester.key
        ,ProcessTesterListNo:ProcessTester.no,SubDisable:ProcessTester.PositionFlag == true ? false : true
        ,SubProcessTesterListData:sublist },()=> this.setMaxMin(this.state.datamaster.MaxMinList))
    }else {
      await this.setState({ ProcessTesterList: ProcessTester.label,ProcessTesterListID:ProcessTester.key
        ,ProcessTesterListNo:ProcessTester.no,SubDisable:ProcessTester.PositionFlag == true ? false : true
        ,SubProcessTesterList:'',SubProcessTesterListNo:'',SubProcessTesterListID:''},()=> this.setMaxMin(this.state.datamaster.MaxMinList))
    }
    var temptemp = null
    if(this.props.LiquidTestInsertType == 'edit'){
      temptemp = this.props.drillingFluidslistedit
    }else{
      temptemp = this.props.drillingFluidslist
    }
    if(temptemp.length > 0){
      let Slurrylist1 = []
      var checkp = true
      var slurrylistid = null
      var Slurrylistname = null
      // for(var p=0;p<this.props.drillingFluidslist.length;p++){
      //   if(this.props.drillingFluidslist[p].TypeId == ProcessTester.key){
      //     checkp = true
          slurrylistid = this.props.drillingFluidslist[0].SlurryTypeId
          Slurrylistname = this.props.drillingFluidslist[0].SlurryTypeName
      //   }
      // }
      
      if(checkp == true){
        Slurrylist1.push(
        {
          key: slurrylistid,
          label: Slurrylistname,
        }
        )
        await this.setState({ 
          slurryListData: Slurrylist1,
          SlurryList:Slurrylistname,
          SlurryListID: slurrylistid,
          // slurryDisable:ProcessTester.SlurryTypeFlag == true && checkp == false? false : true,
          slurryDisable:ProcessTester.SlurryTypeFlag == true && temptemp.length == 0?false : true
        },()=> this.setMaxMin(this.state.datamaster.MaxMinList))
      }else{
        let Slurrylist = [{ key: 0, section: true, label: I18n.t("mainpile.liquidtestinsert.name_slurry")  }]

        if(ProcessTester.SlurryTypeFlag == true){
          for (var i = 0; i < ProcessTester.SlurryTypeList.length; i++) {
            Slurrylist.push(
              {
                key: ProcessTester.SlurryTypeList[i].Id,
                label: ProcessTester.SlurryTypeList[i].Name,
              }
            )
          }
          await this.setState({ slurryListData: Slurrylist,SlurryList:'',SlurryListID:'', slurryDisable:ProcessTester.SlurryTypeFlag == true  ? false : true},()=> this.setMaxMin(this.state.datamaster.MaxMinList))
  
        }else{
          await this.setState({ SlurryList:'',SlurryListID:'',slurryDisable:ProcessTester.SlurryTypeFlag == true ? false : true},()=> this.setMaxMin(this.state.datamaster.MaxMinList))
        }
      }
    }else{
      let Slurrylist = [{ key: 0, section: true, label: I18n.t("mainpile.liquidtestinsert.name_slurry")  }]
      if(ProcessTester.SlurryTypeFlag == true){
        for (var i = 0; i < ProcessTester.SlurryTypeList.length; i++) {
          Slurrylist.push(
            {
              key: ProcessTester.SlurryTypeList[i].Id,
              label: ProcessTester.SlurryTypeList[i].Name,
            }
          )
        }
        await this.setState({ slurryListData: Slurrylist, slurryDisable:ProcessTester.SlurryTypeFlag == true ? false : true},()=> this.setMaxMin(this.state.datamaster.MaxMinList))

      }else{
        await this.setState({ slurryDisable:ProcessTester.SlurryTypeFlag == true ? false : true},()=> this.setMaxMin(this.state.datamaster.MaxMinList))
      }

    }



    
    // console.warn(this.state.SubDisable,this.state.slurryDisable)
  }

  selectedSubProcessTester = async SubProcessTester => {
    await this.setState({ SubProcessTesterList: SubProcessTester.label,SubProcessTesterListID:SubProcessTester.key,SubProcessTesterListNo:SubProcessTester.no },()=> this.setMaxMin(this.state.datamaster.MaxMinList))
  }

  selectedTesterList = async TesterList => {
    await this.setState({ TesterList: TesterList.label,TesterListID:TesterList.key })
  }

  closeButton(){
    Actions.pop()
  }
  async mixdate(){
    if( this.state.datestart != '' && this.state.timestart != ''){
     await this.setState({startdate:this.state.datestart+' '+this.state.timestart})
    }
    if(this.state.dateend != '' && this.state.timeend != ''){
      await this.setState({enddate:this.state.dateend+' '+this.state.timeend})
    }
  }
  async saveButton() {
   
    if (this.state.ProcessTesterList != '') {
        if(this.state.slurryDisable == false && this.state.SlurryList == ''){
          Alert.alert('', I18n.t('mainpile.liquidtestinsert.alert.error6'), [
            {
              text: 'OK'
            }
          ])
        }else{
          if(this.state.SubDisable == false && this.state.SubProcessTesterList == ''){
            Alert.alert('', I18n.t('mainpile.liquidtestinsert.alert.error1'), [
              {
                text: 'OK'
              }
            ])
          }else{
            if(this.state.TesterList == ''){
              Alert.alert('', I18n.t('mainpile.liquidtestinsert.alert.error2'), [
                {
                  text: 'OK'
                }
              ])
            }else{
              this.onSave()
              // console.warn('SAVE')
            }
            
          }
        }
        
      }else{
        Alert.alert('', I18n.t('mainpile.liquidtestinsert.alert.error3'), [
          {
            text: 'OK'
          }
        ])
      }
  }
  _onBack(){
    Actions.pop()
    return true
  }

  async onSave(){
    if(this.state.dateend == '' && this.state.timeend == ''){
      await this.setState({
        dateend:moment().format("DD/MM/YYYY"),
        timeend:moment().format("HH:mm")
      })
    }
    await this.mixdate()
    if (this.state.Edit_Flag == 1) {
        if(this.state.startdate != '' && this.state.enddate != ''){
          var a = moment(this.state.startdate, 'DD/MM/YYYY HH:mm')
          var b = moment(this.state.enddate, 'DD/MM/YYYY HH:mm')
          if(a <= b){
            await this.props.mainPileSetStorage(this.props.pileid, 'step_status', {Step4Status: 1})
            this.props.mainPileGetStorage(this.props.pileid, 'step_status')
            Actions.pop()
            this.saveStep04()
          }else{
            Alert.alert('', I18n.t('mainpile.liquidtestinsert.alert.error4'), [
              {
                text: 'OK'
              }])
          }
        }else{
          Alert.alert('', I18n.t('mainpile.liquidtestinsert.alert.error5'), [
            {
              text: 'OK'
            }])
        }
      }
  }

  selectPh(ph){
    if (this.state.Edit_Flag == 1) {
      this.setState({
        Ph:this.onChangeFormatDecimal(ph),
        Ph_flagend:true
      })
    }
  }
  selectDensity(density){
    if (this.state.Edit_Flag == 1) {
      this.setState({
        Density:this.onChangeFormatDecimal(density),
        Density_flagend:true
      })
    }
  }
  selectViscosity(vicostity){
    if (this.state.Edit_Flag == 1) {
      this.setState({
        Viscosity:this.onChangeFormatDecimal(vicostity),
        Viscosity_flagend:true
      })
    }
  }
  selectSand(sand){
    if (this.state.Edit_Flag == 1) {
      this.setState({
        Sand:this.onChangeFormatDecimal(sand),
        Sand_flagend:true
      })
    }
  }

  onChangeFormatDecimal(data){
    if (data.split(".")[1]) {
      if (data.split(".")[1].length > 2) {
        return parseFloat(data).toFixed(2)
      }else {
        return data
      }
    }else {
      return data
    }
  }

  saveStep04(){
    if(this.props.edit){
      var DrillingFluidIdd = this.props.drillingFluidslist.Id
    }else{
      var DrillingFluidIdd = null
    }
    // console.warn('saveStep04',this.state.SlurryList)
    var valueApi ={
      Language:I18n.locale,
      JobId: this.props.jobid,
      PileId: this.props.pileid,
      DrillingFluidId:DrillingFluidIdd,
      Page:0,
      No:this.props.index,
      ProcessTesterList:this.state.ProcessTesterList,
      ProcessTesterListID:this.state.ProcessTesterListID,
      TypeName: this.state.ProcessTesterList,
      SubProcessTesterList:this.state.SubProcessTesterList,
      SubProcessTesterListID:this.state.SubProcessTesterListID,
      PositionName:this.state.SubProcessTesterList,
      TesterList:this.state.TesterList,
      TesterListID:this.state.TesterListID,
      SlurryList:this.state.SlurryList,
      SlurryID:this.state.SlurryListID,
      SlurryTypeName:this.state.SlurryList,
      ImagePhFiles:this.state.ImagePhFiles,
      Ph:this.state.Ph,
      ImageDensityFiles:this.state.ImageDensityFiles,
      Density:this.state.Density,
      ImageViscosityFiles:this.state.ImageViscosityFiles,
      Viscosity:this.state.Viscosity,
      ImageSandFiles:this.state.ImageSandFiles,
      Sand:this.state.Sand,
      latitude:this.props.lat,
      longitude:this.props.log,
      TesterERPId: this.state.TesterListID,
      TesterERPName: this.state.TesterList,
      startdate:moment(this.state.startdate,'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm:ss'),
      enddate:moment(this.state.enddate,'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm:ss')
    }
    // console.warn('pile4_1',valueApi)
    this.props.pileSaveStep04(valueApi)
  }
  onSelectDate(date){
    let date2 = moment(date).format("DD/MM/YYYY")
    if(this.state.type=='start'){
      this.setState({ datestart: date2,datevisibledate: false })
      this.mixdate()
    }else if(this.state.type=='end'){
      this.setState({ dateend: date2,datevisibledate: false })
      this.mixdate()
    }
  }
  onSelectTime(date){
    let date2 = moment(date).format("HH:mm")
    if(this.state.type=='start'){
      this.setState({ timestart: date2,datevisibletime: false })
      this.mixdate()
    }else if(this.state.type=='end'){
      this.setState({ timeend: date2,datevisibletime: false })
      this.mixdate()
    }
  }
  submitph(next){
    // console.warn('submitph ',next)
    if(this.state.PhMin !== '' && this.state.PhMax !== ''){
      if( isNaN(parseFloat(this.state.Ph))){
        this.setState({Ph:''})
      }else{
        if(
          parseFloat(this.state.Ph) >= parseFloat(this.state.PhMin) && 
          parseFloat(this.state.Ph) <= parseFloat(this.state.PhMax)
        ){
          this.setState({Ph:this.state.Ph  ? parseFloat(this.state.Ph).toFixed(2) : '',Ph_flagend:false})
          if(next == true){
            this.DensityTextInput.focus()
          }
        }else{
          Alert.alert('', I18n.t('mainpile.liquidtestinsert.alert.error7'), [
            {
              text: I18n.t('mainpile.liquidtestinsert.editedit'),
              onPress: async () => {
                // this.setState({Ph:this.state.Ph  ? parseFloat(this.state.Ph).toFixed(2) : ''})
                this.setState({Ph:isNaN(parseFloat(this.state.Phtemp)) ? '' : parseFloat(this.state.Phtemp).toFixed(2)})
              }
            },
            {
              text: I18n.t('mainpile.liquidtestinsert.notedit'),
              onPress: async () => {
                // this.setState({Ph:isNaN(parseFloat(this.state.Phtemp)) ? '' : parseFloat(this.state.Phtemp).toFixed(2)})
                this.setState({Ph:this.state.Ph  ? parseFloat(this.state.Ph).toFixed(2) : '',Ph_flagend:false})
                if(next == true){
                  this.DensityTextInput.focus()
                }
              }
            }],
            { cancelable: false }
          )
        }
      }
    }else{
      this.setState({Ph:this.state.Ph  ? parseFloat(this.state.Ph).toFixed(2) : '',Ph_flagend:false})
      if(next == true){
        this.DensityTextInput.focus()
      }
    }
  }

  submitDensity(next){
    if(this.state.DensityMin !== '' && this.state.DensityMax !== ''){
      if( isNaN(parseFloat(this.state.Density))){
        this.setState({Density:''})
      }else{
        if(
          parseFloat(this.state.Density) >= parseFloat(this.state.DensityMin) && 
          parseFloat(this.state.Density) <= parseFloat(this.state.DensityMax)
        ){
          this.setState({Density:this.state.Density  ? parseFloat(this.state.Density).toFixed(2) : '',Density_flagend:false})
          if(next == true){
            this.ViscosityTextInput.focus()
          }
        }else{
          Alert.alert('', I18n.t('mainpile.liquidtestinsert.alert.error7'), [
            {
              text: I18n.t('mainpile.liquidtestinsert.editedit'),
              onPress: async () => {
                // this.setState({Density:this.state.Density  ? parseFloat(this.state.Density).toFixed(2) : ''})
                this.setState({Density:isNaN(parseFloat(this.state.Densitytemp)) ? '' : parseFloat(this.state.Densitytemp).toFixed(2)})
              }
            },
            {
              text: I18n.t('mainpile.liquidtestinsert.notedit'),
              onPress: async () => {
                // this.setState({Density:isNaN(parseFloat(this.state.Densitytemp)) ? '' : parseFloat(this.state.Densitytemp).toFixed(2)})
                this.setState({Density:this.state.Density  ? parseFloat(this.state.Density).toFixed(2) : '',Density_flagend:false})
                if(next == true){
                  this.ViscosityTextInput.focus()
                }
              }
            }],
            { cancelable: false }
          )
        }
      }
    }else{
      this.setState({Density:this.state.Density  ? parseFloat(this.state.Density).toFixed(2) : '',Density_flagend:false})
      if(next == true){
        this.ViscosityTextInput.focus()
      }
    }
  }

  submitViscosity(next){
    if(this.state.ViscosityMin !== '' && this.state.ViscosityMax !== ''){
      if( isNaN(parseFloat(this.state.Viscosity))){
        this.setState({Viscosity:''})
      }else{
        if(
          parseFloat(this.state.Viscosity) >= parseFloat(this.state.ViscosityMin) && 
          parseFloat(this.state.Viscosity) <= parseFloat(this.state.ViscosityMax)
        ){
          this.setState({Viscosity:this.state.Viscosity  ? parseFloat(this.state.Viscosity).toFixed(2) : '',Viscosity_flagend:false})
          if(next == true){
            this.SandTextInput.focus()
          }
        }else{
          Alert.alert('', I18n.t('mainpile.liquidtestinsert.alert.error7'), [
            {
              text: I18n.t('mainpile.liquidtestinsert.editedit'),
              onPress: async () => {
                // this.setState({Viscosity:this.state.Viscosity  ? parseFloat(this.state.Viscosity).toFixed(2) : ''})
                this.setState({Viscosity:isNaN(parseFloat(this.state.Viscositytemp)) ? '' : parseFloat(this.state.Viscositytemp).toFixed(2)})
              }
            },
            {
              text: I18n.t('mainpile.liquidtestinsert.notedit'),
              onPress: async () => {
                // this.setState({Viscosity:isNaN(parseFloat(this.state.Viscositytemp)) ? '' : parseFloat(this.state.Viscositytemp).toFixed(2)})
                this.setState({Viscosity:this.state.Viscosity  ? parseFloat(this.state.Viscosity).toFixed(2) : '',Viscosity_flagend:false})
                if(next == true){
                  this.SandTextInput.focus()
                }
              }
            }],
            { cancelable: false }
          )
        }
      }
    }else{
      this.setState({Viscosity:this.state.Viscosity  ? parseFloat(this.state.Viscosity).toFixed(2) : '',Viscosity_flagend:false})
      if(next == true){
        this.SandTextInput.focus()
      }
    }
  }

  submitSand(){
    if(this.state.PercentSandMin !== '' && this.state.PercentSandMax !== ''){
      if( isNaN(parseFloat(this.state.Sand))){
        this.setState({Sand:''})
      }else{
        if(
          parseFloat(this.state.Sand) >= parseFloat(this.state.PercentSandMin) && 
          parseFloat(this.state.Sand) <= parseFloat(this.state.PercentSandMax)
        ){
          this.setState({Sand:this.state.Sand  ? parseFloat(this.state.Sand).toFixed(2) : ''})
        }else{
          Alert.alert('', I18n.t('mainpile.liquidtestinsert.alert.error7'), [
            {
              text: I18n.t('mainpile.liquidtestinsert.editedit'),
              onPress: async () => {
                // this.setState({Sand:this.state.Sand  ? parseFloat(this.state.Sand).toFixed(2) : ''})
                this.setState({Sand:isNaN(parseFloat(this.state.Sandtemp)) ? '' : parseFloat(this.state.Sandtemp).toFixed(2)})
              }
            },
            {
              text: I18n.t('mainpile.liquidtestinsert.notedit'),
              onPress: async () => {
                // this.setState({Sand:isNaN(parseFloat(this.state.Sandtemp)) ? '' : parseFloat(this.state.Sandtemp).toFixed(2)})
                this.setState({Sand:this.state.Sand  ? parseFloat(this.state.Sand).toFixed(2) : ''})
              }
            }],
            { cancelable: false }
          )
        }
      }
    }else{
      this.setState({Sand:this.state.Sand  ? parseFloat(this.state.Sand).toFixed(2) : ''})
    }
  }
  onChangedate = (event, selectedDate) => {
    if(event.type == 'set'){
      this.onSelectDate(selectedDate)
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };
  onChangetime = (event, selectedDate) => {
    if(event.type == 'set'){
      this.onSelectTime(selectedDate)
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };
  render() {
    // console.warn('Phmin',this.state.PhMin,'PhMax',this.state.PhMax,this.state.Edit_Flag)
    return (<View style={styles.listContainer}>
      
      <View style={[
        this.props.category == 3|| this.props.category == 5 ? [styles.listNavigation,{backgroundColor: DEFAULT_COLOR_4}]:this.props.shape == 1 ? styles.listNavigation : [styles.listNavigation, {backgroundColor:DEFAULT_COLOR_1}] 
      ]}>
        <Text style={styles.listNavigationText}>{I18n.t("mainpile.liquidtestinsert.title")}</Text>
      </View>
      <ScrollView>
      {/* <KeyboardAwareScrollView scrollEnabled={true} resetScrollToCoords={{ x: 0, y: 0 }} enableOnAndroid ={true} keyboardShouldPersistTaps="always" > */}
      <View style={styles.listRow}>
        <View style={styles.listTopicSub}>
          <Text style={styles.listTopicSubText}>{I18n.t("mainpile.liquidtestinsert.name_processtesterlist")} *</Text>
          <View style={styles.listSelectedView}>
            <ModalSelector
                data={this.state.ProcessTesterListData}
                onChange={this.selectedProcessTester}
                selectTextStyle={styles.textButton}
                disabled={this.state.Edit_Flag == 0 ? true : false}
                cancelText="Cancel">
                <TouchableOpacity style={this.state.Edit_Flag == 0 ? [styles.button ,{ borderColor: MENU_GREY_ITEM }]:styles.button}>
                  <View style={styles.buttonTextStyle}>
                  <Text style={this.state.Edit_Flag == 0 ? [styles.buttonText ,styles.buttonLongText, { color: MENU_GREY_ITEM }] : [styles.buttonText, styles.buttonLongText, {color:MAIN_COLOR}]}>{this.state.ProcessTesterList || I18n.t("mainpile.liquidtestinsert.processtesterlistselect")}</Text>
                    <View style={{justifyContent: 'flex-end', flexDirection: 'row'}}>
                      <Icon name='arrow-drop-down' type='MaterialIcons' color={this.state.Edit_Flag == 0 ? MENU_GREY_ITEM : '#007CC2' }></Icon>
                    </View>
                  </View>
                </TouchableOpacity>
            </ModalSelector>
          </View>
        </View>
        {
          <View style={styles.listTopicSub}>
          <Text style={styles.listTopicSubText}>{I18n.t("mainpile.liquidtestinsert.typeselect")} *</Text>
          <View style={styles.listSelectedView}>
            <ModalSelector
                data={this.state.slurryListData}
                onChange={this.selectedSlurry}
                selectTextStyle={styles.textButton}
                disabled={this.state.Edit_Flag == 0 ?true : this.state.slurryDisable == true ? true : false}
                cancelText="Cancel">
                <TouchableOpacity style={this.state.Edit_Flag == 0
                  ? [styles.button ,{ borderColor: MENU_GREY_ITEM }]:this.state.slurryDisable == true ? [styles.button ,{ borderColor: MENU_GREY_ITEM }] : styles.button }>
                  <View style={styles.buttonTextStyle}>
                  <Text style={this.state.Edit_Flag == 0
                    ? [styles.buttonText ,styles.buttonLongText, { color: MENU_GREY_ITEM }]
                    
                    : this.state.slurryDisable == true ? [styles.buttonText ,styles.buttonLongText, { color: MENU_GREY_ITEM }]
                    :[styles.buttonText, styles.buttonLongText, {color:MAIN_COLOR}]}>{this.state.SlurryList || I18n.t("mainpile.liquidtestinsert.name_slurry")}</Text>
                    <View style={{justifyContent: 'flex-end', flexDirection: 'row'}}>
                      <Icon name='arrow-drop-down' type='MaterialIcons' color={this.state.Edit_Flag == 0
                        ? MENU_GREY_ITEM 
                        : this.state.slurryDisable == true ? MENU_GREY_ITEM : '#007CC2' }></Icon>
                    </View>
                  </View>
                </TouchableOpacity>
            </ModalSelector>
          </View>
        </View>
        }
        {
          // this.props.shape == 1 ?
          <View style={styles.listTopicSub}>
            <Text style={styles.listTopicSubText}>{I18n.t("mainpile.liquidtestinsert.name_sludge")} *</Text>
            <View style={styles.listSelectedView}>
              <ModalSelector
                  data={this.state.SubProcessTesterListData}
                  onChange={this.selectedSubProcessTester}
                  selectTextStyle={styles.textButton}
                  disabled={this.state.Edit_Flag == 0 ? true:this.state.SubDisable == true ? true : false}
                  cancelText="Cancel">
                  <TouchableOpacity style={this.state.Edit_Flag == 0
                    ? [styles.button ,{ borderColor: MENU_GREY_ITEM }] 
                    : this.state.SubDisable == true ? [styles.button ,{ borderColor: MENU_GREY_ITEM }] : styles.button }>
                    <View style={styles.buttonTextStyle}>
                    <Text style={this.state.Edit_Flag == 0
                      ? [styles.buttonText ,styles.buttonLongText, { color: MENU_GREY_ITEM }]
                      
                      : this.state.SubDisable == true ? [styles.buttonText ,styles.buttonLongText, { color: MENU_GREY_ITEM }]
                      :[styles.buttonText, styles.buttonLongText, {color:MAIN_COLOR}]}>{this.state.SubProcessTesterList || I18n.t("mainpile.liquidtestinsert.name_sludgeselect")}</Text>
                      <View style={{justifyContent: 'flex-end', flexDirection: 'row'}}>
                        <Icon name='arrow-drop-down' type='MaterialIcons' color={this.state.Edit_Flag == 0
                          ? MENU_GREY_ITEM 
                          : this.state.SubDisable == true ? MENU_GREY_ITEM : '#007CC2' }></Icon>
                      </View>
                    </View>
                  </TouchableOpacity>
              </ModalSelector>
            </View>
          </View>
          // :
          // <View/>
        }

        <View style={styles.listTopicSub}>
          <Text style={styles.listTopicSubText}>{I18n.t("mainpile.liquidtestinsert.name_testerlist")} *</Text>
          <View style={styles.listSelectedView}>
            <ModalSelector
                data={this.state.TesterListData}
                onChange={this.selectedTesterList}
                selectTextStyle={styles.textButton}
                disabled={this.state.Edit_Flag == 0 ? true : false}
                cancelText="Cancel">
                <TouchableOpacity style={this.state.Edit_Flag == 0 ? [styles.button ,{ borderColor: MENU_GREY_ITEM }]:styles.button}>
                  <View style={styles.buttonTextStyle}>
                    <Text style={this.state.Edit_Flag == 0 ? [styles.buttonText ,styles.buttonLongText, { color: MENU_GREY_ITEM }] : [styles.buttonText, styles.buttonLongText, {color: MAIN_COLOR}]}>{this.state.TesterList || I18n.t("mainpile.liquidtestinsert.testerlistselect")}</Text>
                    <View style={{justifyContent: 'flex-end', flexDirection: 'row'}}>
                      <Icon name='arrow-drop-down' type='MaterialIcons' color={this.state.Edit_Flag == 0 ? MENU_GREY_ITEM : '#007CC2' }></Icon>
                    </View>
                  </View>
                </TouchableOpacity>
            </ModalSelector>
          </View>
        </View>

        <View style={styles.listTopicSubRow}>
          <Text style={styles.listLabel}>{I18n.t("mainpile.liquidtestinsert.unit_ph")}*</Text>
          <TextInput
            ref={(input) => { this.PHTextInput = input; }}
            value={isNaN(parseFloat(this.state.Ph)) ? '' : this.state.Ph.toString()}
            onChangeText={(Ph) => this.selectPh(Ph)}
            editable={this.state.Edit_Flag == 1 ? true: false}
            onEndEditing={()=> {
              if(this.state.Ph_flagend){
                this.submitph(false)
              }
              }
            }
            onSubmitEditing={() => { 
              this.submitph(true)
            }}
            blurOnSubmit={false}
            keyboardType="numeric"
            underlineColorAndroid='transparent'
            style={[styles.listTextbox, styles.listTextboxFlex]}/>
          <View style={styles.buttonImage}>
            <TouchableOpacity
              style={[styles.image,this.state.ImagePhFiles.length>0?{backgroundColor:'#6dcc64'}:{}]}
              onPress={() => this.onCameraRoll("ImagePhFiles", this.state.ImagePhFiles,'edit')}
            >
              <Icon name="image" type="entypo" size={14} color="#fff" />
              <Text style={styles.imageText}>{I18n.t("mainpile.liquidtestinsert.view")}</Text>
            </TouchableOpacity>
          </View>

        </View>
        <View style={styles.listTopicSubRow}>
          <Text style={styles.listLabel}>Density {I18n.t("mainpile.liquidtestinsert.unit_density")} *</Text>
          <TextInput
            ref={(input) => { this.DensityTextInput = input; }}
            value={isNaN(parseFloat(this.state.Density)) ? '' : this.state.Density.toString()}
            onChangeText={(Density) => this.selectDensity(Density)}
            editable={this.state.Edit_Flag == 1 ? true: false}
            onEndEditing={()=> {
              if(this.state.Density_flagend){
                this.submitDensity(false)
              }
              }
            }
            onSubmitEditing={() => { 
              this.submitDensity(true)
            }}
            blurOnSubmit={false}
            keyboardType="numeric"
            underlineColorAndroid='transparent' style={[styles.listTextbox, styles.listTextboxFlex]}/>
          <View style={styles.buttonImage}>
            <TouchableOpacity
              style={[styles.image,this.state.ImageDensityFiles.length>0?{backgroundColor:'#6dcc64'}:{}]}
              onPress={() => this.onCameraRoll("ImageDensityFiles", this.state.ImageDensityFiles,'edit')}
            >
              <Icon name="image" type="entypo" size={14} color="#fff" />
              <Text style={styles.imageText}>{I18n.t("mainpile.liquidtestinsert.view")}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.listTopicSubRow}>
          <Text style={styles.listLabel}>Viscosity {I18n.t("mainpile.liquidtestinsert.unit_viscosity")} *</Text>
          <TextInput
            ref={(input) => { this.ViscosityTextInput = input; }}
            value={isNaN(parseFloat(this.state.Viscosity)) ? '' : this.state.Viscosity.toString() }
            onChangeText={(Viscosity) => this.selectViscosity(Viscosity)}
            onEndEditing={()=> {
              if(this.state.Viscosity_flagend){
                this.submitViscosity(false)
              }
              }
            }
            onSubmitEditing={() => { 
              this.submitViscosity(true)
            }}
            blurOnSubmit={false}
            editable={this.state.Edit_Flag == 1 ? true: false}
            keyboardType="numeric"
            underlineColorAndroid='transparent' style={[styles.listTextbox, styles.listTextboxFlex]}/>
          <View style={styles.buttonImage}>
            <TouchableOpacity
              style={[styles.image,this.state.ImageViscosityFiles.length>0?{backgroundColor:'#6dcc64'}:{}]}
              onPress={() => this.onCameraRoll("ImageViscosityFiles", this.state.ImageViscosityFiles,'edit')}
            >
              <Icon name="image" type="entypo" size={14} color="#fff" />
              <Text style={styles.imageText}>{I18n.t("mainpile.liquidtestinsert.view")}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.listTopicSubRow}>
          <Text style={styles.listLabel}>% Sand *</Text>
          <TextInput
            ref={(input) => { this.SandTextInput = input; }}
            value={this.state.Sand.toString()}
            onChangeText={(Sand) => this.selectSand(Sand)}
            editable={this.state.Edit_Flag == 1 ? true: false}
            onEndEditing={()=> 
              this.submitSand()
              // this.setState({Sand:this.state.Sand  ? parseFloat(this.state.Sand).toFixed(2) : ''})
            }
            keyboardType="numeric"
            underlineColorAndroid='transparent' style={[styles.listTextbox, styles.listTextboxFlex]}/>
          <View style={styles.buttonImage}>
            <TouchableOpacity
              style={[styles.image,this.state.ImageSandFiles.length>0?{backgroundColor:'#6dcc64'}:{}]}
              onPress={() => this.onCameraRoll("ImageSandFiles", this.state.ImageSandFiles,'edit')}
            >
              <Icon name="image" type="entypo" size={14} color="#fff" />
              <Text style={styles.imageText}>{I18n.t("mainpile.liquidtestinsert.view")}</Text>
            </TouchableOpacity>
          </View>
        </View>
          {this.state.datevisibledate &&<DateTimePicker
            value={this.getDate()}
            isVisible={this.state.datevisibledate}
            is24Hour={true}
            display='spinner'
            mode='date'
            onChange={this.onChangedate}
          />}
          {this.state.datevisibletime && <DateTimePicker
            value={this.getDate()}
            isVisible={this.state.datevisibletime}
            is24Hour={true}
            display='spinner'
            mode='time'
            onChange={this.onChangetime}
          />}
        <View>
            <View style={[styles.topic,{marginTop:10}]}>
              <Text style={styles.topicText}>{I18n.t('startenddate.startenddate')}{I18n.t('mainpile.step.4_1')}</Text>
            </View>
            <View style={[styles.imageDetail,{marginLeft:20,marginRight:20,marginBottom:15}]}>
            <Text style={{width:'25%'}}>{I18n.t('startenddate.start')}</Text>
            <TouchableOpacity
              style={[styles.buttonBox, { width: "35%" }]}
              onPress={() => {
                this.setState({datevisibledate:true,type:'start'})
              }}
              disabled={this.state.Edit_Flag == 0}
            >
              <Text>{this.state.datestart}</Text>
            </TouchableOpacity>
            <Text style={{width:'10%',textAlign:'center'}}>{I18n.t('startenddate.time')}</Text>
            <TouchableOpacity
              style={[styles.buttonBox, { width: "30%" }]}
              onPress={() => {
                this.setState({datevisibletime:true,type:'start'})
              }}
              disabled={this.state.Edit_Flag == 0}
            >
              <Text>{this.state.timestart}</Text>
            </TouchableOpacity>
            {/*<TouchableOpacity
              style={styles.approveBox}
              onPress={() => this.onnowdate('start')}
              disabled={this.state.Edit_Flag == 0}
            >
              <Text style={{color:'#fff'}}>เริ่ม</Text>
            </TouchableOpacity>*/}
          </View>
          <View style={[styles.imageDetail,{marginLeft:20,marginRight:20,marginBottom:15}]}>
          <Text style={{width:'25%'}}>{I18n.t('startenddate.end')}</Text>
          <TouchableOpacity
              style={[styles.buttonBox, { width: "35%" }]}
              onPress={() => {
                this.setState({datevisibledate:true,type:'end'})
              }}
              disabled={this.state.Edit_Flag == 0}
            >
              <Text>{this.state.dateend}</Text>
            </TouchableOpacity>
            <Text style={{width:'10%',textAlign:'center'}}>{I18n.t('startenddate.time')}</Text>
            <TouchableOpacity
              style={[styles.buttonBox, { width: "30%" }]}
              onPress={() => {
                this.setState({datevisibletime:true,type:'end'})
              }}
              disabled={this.state.Edit_Flag == 0}
            >
              <Text>{this.state.timeend}</Text>
            </TouchableOpacity>
            {/*<TouchableOpacity
              style={styles.approveBox}
              onPress={() => this.onnowdate('end')}
              disabled={this.state.Edit_Flag == 0}
            >
              <Text style={{color:'#fff'}}>สิ้นสุด</Text>
            </TouchableOpacity>*/}
          </View>
          </View>
      </View>
    {/* </KeyboardAwareScrollView> */}
    </ScrollView>
    <View style={styles.buttonFixed}>
      <View style={[styles.second,
        this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
      ]}>
        <TouchableOpacity style={styles.firstButton} onPress={()=>this.closeButton()}>
          <Text style={styles.selectButton}>{I18n.t('mainpile.steeldetail.button.close')}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.secondButton,
          this.props.Edit_Flag!=1?{backgroundColor:'#BABABA'}:this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
        ]} onPress={()=>this.saveButton()} disabled={this.props.Edit_Flag == 0}>
          <Text style={styles.selectButton}>{I18n.t('mainpile.steeldetail.button.save')}</Text>
        </TouchableOpacity>
      </View>
    </View>

    </View>)
  }
}

const mapStateToProps = state => {
  return {
      pileAll: state.mainpile.pileAll || null , pile4Id:state.pile.pile4Id
  }
}
export default connect(mapStateToProps, actions)(LiquidTestInsert)
