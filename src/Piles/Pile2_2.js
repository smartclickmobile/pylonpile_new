import React, { Component } from "react"
import { View, Text, StyleSheet, TouchableOpacity, TextInput } from "react-native"
import { MAIN_COLOR, SUB_COLOR } from "../Constants/Color"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"
import I18n from "../../assets/languages/i18n"
import { Icon } from "react-native-elements"
import styles from './styles/Pile2_2.style'
import moment from "moment"
import DateTimePicker from '@react-native-community/datetimepicker';
class Pile2_2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      group: [
        { label: I18n.t("mainpile.2_2.plummet"), value: "0" },
        { label: I18n.t("mainpile.2_2.water"), value: "1" }
      ],
      process:2,
      step:2,
      selected: 0,
      images_plummet:[],
      images_water:[],
      data:null,
      checkplummet:false,
      checkwater:false,
      Edit_Flag:1,
      datevisibletime:false,
      datevisibledate:false,
      startdate:'',
      enddate:'',
      type:'',
      datestart:'',
      timestart:'',
      dateend:'',
      timeend:'',
      set:false,
      status2:0
    }
  }
  async componentWillReceiveProps(nextProps) {
    var pile = null
    var pile2_0 = null
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['2_2']) {
      pile = nextProps.pileAll[this.props.pileid]['2_2'].data
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['2_0']) {
      pile2_0 = nextProps.pileAll[this.props.pileid]['2_0'].data
      this.setState({status2: nextProps.pileAll[this.props.pileid].step_status.Step2Status})
    }
    if (nextProps.pileAll[this.props.pileid].step_status.Step2Status == 0) {
      this.setState({checkplummet:false,checkwater:false,images_plummet:[],images_water:[]})
    }
    
    if (pile) {
      if (pile.checkplummet != null && pile.checkplummet != undefined) {
        await this.setState({checkplummet: pile.checkplummet})
      }else if(pile.checkplummet == null) {
        this.setState({checkplummet:false})
      }
      if (pile.images_plummet != "" && pile.images_plummet != undefined ) {
        await this.setState({images_plummet: pile.images_plummet})
      }else if(pile.images_plummet == null) {
        this.setState({images_plummet:[]})
      }
      if (pile.checkwater != null && pile.checkwater != undefined) {
        await this.setState({checkwater: pile.checkwater})
      }else if(pile.checkwater == null) {
        this.setState({checkwater:false})
      }
      if (pile.images_water != "" && pile.images_water != undefined ) {
        await this.setState({images_water: pile.images_water})
      }else if(pile.images_water == null) {
        this.setState({images_water:[]})
      }
      if(!this.state.set){
        if(this.props.startdatedefault != '' && this.props.startdatedefault != null){
          if(this.props.enddate != '' && this.props.enddate != null){
            this.setState({
              datestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
              timestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("HH:mm"),
              dateend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
              timeend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("HH:mm"),
              startdate:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
              enddate:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm")
            })
          }else{
            this.setState({
              datestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
              timestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("HH:mm"),
              dateend:'',
              timeend:'',
              startdate:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
              enddate:''
            })
          }
        }else{
          if(this.props.startdate != '' && this.props.startdate != null){
            this.setState({
              datestart:moment(this.props.startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
              timestart:moment(this.props.startdate,"DD/MM/YYYY HH:mm").format("HH:mm"),
              startdate:moment(this.props.startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
            })
          }
          if(this.props.enddate != '' && this.props.enddate != null){
            this.setState({
              dateend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
              timeend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("HH:mm"),
              enddate:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm")
            })
          }else{
            this.setState({
              dateend:'',
              timeend:''
            })
          }
        }
        
      }
      if(nextProps.pileAll[this.props.pileid]['2_2'].startdate != null && nextProps.pileAll[this.props.pileid]['2_2'].startdate != ''){
        this.setState({
          datestart:moment(nextProps.pileAll[this.props.pileid]['2_2'].startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
          timestart:moment(nextProps.pileAll[this.props.pileid]['2_2'].startdate,"DD/MM/YYYY HH:mm").format("HH:mm"),
          startdate:moment(nextProps.pileAll[this.props.pileid]['2_2'].startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
        })
      }
      if(nextProps.pileAll[this.props.pileid]['2_2'].enddate != null && nextProps.pileAll[this.props.pileid]['2_2'].enddate != ''){
        this.setState({
          dateend:moment(nextProps.pileAll[this.props.pileid]['2_2'].enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
          timeend:moment(nextProps.pileAll[this.props.pileid]['2_2'].enddate,"DD/MM/YYYY HH:mm").format("HH:mm"),
          enddate:moment(nextProps.pileAll[this.props.pileid]['2_2'].enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
        })
      }
      

    }
    if (pile2_0) {
      // if (pile3_0.Edit_Flag != null && pile3_0.Edit_Flag != undefined && pile3_0.Edit_Flag != '') {
        this.setState({Edit_Flag:pile2_0.Edit_Flag})
      // }
    }
  }

  onCameraRoll(keyname,photos,type){
    if (this.state.Edit_Flag == 1) {
      Actions.camearaRoll({jobid:this.props.jobid,process:this.state.process,step:this.state.step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:type,shape:this.props.shape,category:this.props.category,Edit_Flag:this.state.Edit_Flag})
    }else {
      Actions.camearaRoll({jobid:this.props.jobid,process:this.state.process,step:this.state.step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:'view',shape:this.props.shape,category:this.props.category,Edit_Flag:this.state.Edit_Flag})
    }
  }

  getState(){
    var data = {
      checkplummet: this.state.checkplummet,
      checkwater: this.state.checkwater,
      images_plummet: this.state.images_plummet,
      images_water: this.state.images_water,
    }
    return data
  }
  async mixdate(){
    if( this.state.datestart != '' && this.state.timestart != ''){
     await this.setState({startdate:this.state.datestart+' '+this.state.timestart})
    }
    if(this.state.dateend != '' && this.state.timeend != ''){
      await this.setState({enddate:this.state.dateend+' '+this.state.timeend})
    }
  }
  async saveLocal(process_step){
    await this.mixdate()
    var value = {
      process:2,
      step:2,
      pile_id: this.props.pileid,
      data:{
        checkplummet: this.state.checkplummet,
        checkwater: this.state.checkwater,
        images_plummet: this.state.images_plummet,
        images_water: this.state.images_water,
      },
      startdate: this.state.startdate,
      enddate: this.state.enddate
    }
    await this.props.mainPileSetStorage(this.props.pileid,process_step,value)
  }
  async selectedPlummet(){
    if (this.state.Edit_Flag == 1) {
      await this.setState({ checkplummet: !this.state.checkplummet })
      this.saveLocal('2_2')
    }
  }
  async selectedWater(){
    if (this.state.Edit_Flag == 1) {
      await this.setState({ checkwater: !this.state.checkwater })
      this.saveLocal('2_2')
    }
  }
  onSelectDate(date){
    let date2 = moment(date).format("DD/MM/YYYY")
    if(this.state.type=='start'){
      this.setState({ datestart: date2 ,set:true,datevisibledate: false})
      this.saveLocal('2_2')
    }else if(this.state.type=='end'){
      this.setState({ dateend: date2 ,set:true,datevisibledate: false})
      this.saveLocal('2_2')
    }
  }
  onSelectTime(date){
    // console.warn(moment(this.state.startdate,"DD/MM/YYYY HH:mm"))
    let date2 = moment(date).format("HH:mm")
    if(this.state.type=='start'){
      this.setState({ timestart: date2 ,set:true,datevisibletime: false})
      this.saveLocal('2_2')
    }else if(this.state.type=='end'){
      this.setState({ timeend: date2 ,set:true,datevisibletime: false})
      this.saveLocal('2_2')
    }
  }
  getDate(){
    if(this.state.type == 'start'){
      if(this.state.startdate != ''){
        return new Date(moment(this.state.startdate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        if(this.props.startdate != '' && this.props.startdate != undefined && this.props.startdate != null){
          return new Date(moment(this.props.startdate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
        }else{
          return new Date()
        }
      }
    }else{
      if(this.state.enddate != ''){
        return new Date(moment(this.state.enddate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        if(this.props.enddate != '' && this.props.enddate != undefined && this.props.enddate != null){
          return new Date(moment(this.props.enddate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
        }else{
          return new Date()
        }
      }
    }
  }
  _renderCheckBox() {
    // console.warn('status2',this.state.status2)
    return (
      <View>
        <View>
          <View style={styles.box}>
            <View style={styles.topicBox}>
              <Text style={styles.textTopicBox}>{I18n.t("mainpile.2_2.plummet")}</Text>
            </View>
            <View style={styles.imageDetail}>
              <View style={{ width: "40%", flexDirection: "row", alignItems: "center", marginLeft: "10%" }}>
                {this.state.checkplummet == true ? (
                  <Icon
                    name="check"
                    reverse
                    color="#6dcc64"
                    size={15}
                    onPress={()=> this.selectedPlummet()}
                  />
                ) : (
                  <Icon
                    name="check"
                    reverse
                    color="grey"
                    size={15}
                    onPress={()=> this.selectedPlummet()}
                  />
                )}
                <Text>{I18n.t("mainpile.steeldetail.approve")}</Text>
              </View>
              <View style={{ width: "50%", alignItems: "center" }}>
                <TouchableOpacity
                  style={[styles.image,this.state.images_plummet.length>0?{backgroundColor:'#6dcc64'}:{}]}
                  onPress={() => this.onCameraRoll("images_plummet", this.state.images_plummet,'edit')}
                  
                >
                  <Icon name="image" type="entypo" color="#FFF" />
                  <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>

        <View>
          <View style={styles.box}>
            <View style={styles.topicBox}>
              <Text style={styles.textTopicBox}>{I18n.t("mainpile.2_2.water")}</Text>
            </View>
            <View style={styles.imageDetail}>
              <View style={{ width: "40%", flexDirection: "row", alignItems: "center", marginLeft: "10%" }}>
                {this.state.checkwater == true ? (
                  <Icon
                    name="check"
                    reverse
                    color="#6dcc64"
                    size={15}
                    onPress={()=> this.selectedWater()}
                  />
                ) : (
                  <Icon
                    name="check"
                    reverse
                    color="grey"
                    size={15}
                    onPress={()=> this.selectedWater()}
                  />
                )}
                <Text>{I18n.t("mainpile.steeldetail.approve")}</Text>
              </View>
              <View style={{ width: "50%", alignItems: "center" }}>
                <TouchableOpacity
                  style={[styles.image,this.state.images_water.length>0?{backgroundColor:'#6dcc64'}:{}]}
                  onPress={() => this.onCameraRoll("images_water", this.state.images_water,'edit')}
                >
                  <Icon name="image" type="entypo" color="#FFF" />
                  <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
        
      </View>
    )
  }

  async updateState(value) {
    // console.log(value);
    if (value.data.images_water != undefined) {
      // console.log('value.data.images_water',value.data.images_water)
      await this.setState({images_water:value.data.images_water})
      this.saveLocal('2_2')
    }
    if (value.data.images_plummet != undefined) {
      // console.log('value.data.images_plummet',value.data.images_plummet)
      await this.setState({images_plummet:value.data.images_plummet})
      this.saveLocal('2_2')
    }
  }
  onChangedate = (event, selectedDate) => {
    if(event.type == 'set'){
      this.onSelectDate(selectedDate)
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };
  onChangetime = (event, selectedDate) => {
    if(event.type == 'set'){
      this.onSelectTime(selectedDate)
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };

  render() {
    if (this.props.hidden) {
      return null
    }
    return (
      <View>
        {this.state.datevisibledate &&<DateTimePicker
          value={this.getDate()}
          isVisible={this.state.datevisibledate}
          is24Hour={true}
          display='spinner'
          mode='date'
          onChange={this.onChangedate}
        />}
        {this.state.datevisibletime && <DateTimePicker
          value={this.getDate()}
          isVisible={this.state.datevisibletime}
          is24Hour={true}
          display='spinner'
          mode='time'
          onChange={this.onChangetime}
        />}
        <View>
          <View style={styles.box}>
            <View style={styles.topicBox}>
              <Text style={styles.textTopicBox}>{I18n.t('startenddate.startenddate')}{I18n.t('mainpile.step.2')}</Text>
            </View>
            <View style={[styles.imageDetail,{marginLeft:20,marginRight:20,marginBottom:15}]}>
            <Text style={{width:'25%'}}>{I18n.t('startenddate.start')}</Text>
            <TouchableOpacity
              style={[styles.buttonBox, { width: "35%" }]}
              onPress={() => {
                this.setState({datevisibledate:true,type:'start'})
              }}
              disabled={this.state.Edit_Flag == 0}
            >
              <Text>{this.state.datestart}</Text>
            </TouchableOpacity>
            <Text style={{width:'10%',textAlign:'center'}}>{I18n.t('startenddate.time')}</Text>
            <TouchableOpacity
              style={[styles.buttonBox, { width: "30%" }]}
              onPress={() => {
                this.setState({datevisibletime:true,type:'start'})
              }}
              disabled={this.state.Edit_Flag == 0}
            >
              <Text>{this.state.timestart}</Text>
            </TouchableOpacity>
            {/*<TouchableOpacity
              style={styles.approveBox}
              onPress={() => this.onnowdate('start')}
              disabled={this.state.Edit_Flag == 0}
            >
              <Text style={{color:'#fff'}}>เริ่ม</Text>
            </TouchableOpacity>*/}
          </View>
          <View style={[styles.imageDetail,{marginLeft:20,marginRight:20,marginBottom:15}]}>
          <Text style={{width:'25%'}}>{I18n.t('startenddate.end')}</Text>
          <TouchableOpacity
              style={[styles.buttonBox, { width: "35%" }]}
              onPress={() => {
                this.setState({datevisibledate:true,type:'end'})
              }}
              disabled={this.state.Edit_Flag == 0}
            >
              <Text>{this.state.dateend}</Text>
            </TouchableOpacity>
            <Text style={{width:'10%',textAlign:'center'}}>{I18n.t('startenddate.time')}</Text>
            <TouchableOpacity
              style={[styles.buttonBox, { width: "30%" }]}
              onPress={() => {
                this.setState({datevisibletime:true,type:'end'})
              }}
              disabled={this.state.Edit_Flag == 0}
            >
              <Text>{this.state.timeend}</Text>
            </TouchableOpacity>
            {/*<TouchableOpacity
              style={styles.approveBox}
              onPress={() => this.onnowdate('end')}
              disabled={this.state.Edit_Flag == 0}
            >
              <Text style={{color:'#fff'}}>สิ้นสุด</Text>
            </TouchableOpacity>*/}
                </View>
          </View>
        </View>
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t("mainpile.2_2.check")}</Text>
        </View>
        {this._renderCheckBox()}
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll || null,
    masterInfo: state.pile.masterInfo,
    valueInfo: state.pile.valueInfo,
  }
}

export default connect(mapStateToProps, actions, null, { withRef: true })(Pile2_2)
