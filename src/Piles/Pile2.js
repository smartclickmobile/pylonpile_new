import React, { Component } from "react"
import { View, Text, TouchableOpacity, Alert,ActivityIndicator } from "react-native"
import { Content } from "native-base"
import StepIndicator from "react-native-step-indicator"
import { connect } from "react-redux"
import * as actions from "../Actions"
import { MAIN_COLOR, DEFAULT_COLOR_1, DEFAULT_COLOR_4 } from "../Constants/Color"
import I18n from "../../assets/languages/i18n"
import Pile2_1 from "./Pile2_1"
import Pile2_2 from "./Pile2_2"
import styles from "./styles/Pile2.style"
import moment from "moment"
class Pile2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 2,
      step: 0,
      ref: 2,
      error: null,
      data: [],
      location: {
        position: {
          lat: 1,
          log: 1
        }
      },
      Edit_Flag: 1,
      startdate:'',
      status5:0,
      checksavebutton:false,
      process2_success_1_random:null,
      saveload:false,
      process2_success_2_random:null,
      statuscolor:null,
      stepstatus:null
    }
  }
  componentDidMount(){
    // console.log(this.props.stack)
    if (this.props.stack) {
      // console.log(this.props.stack);
      this.setState({step:this.props.stack[this.props.stack.length - 1].step - 1})
    }
  }
  async componentWillReceiveProps(nextProps) {
    if(nextProps.step_status2_random != this.state.stepstatus && nextProps.processstatus == 2){
      this.setState({stepstatus:nextProps.step_status2_random},async()=>{
        // console.warn('yes',nextProps.step_status2.Step2Status)
        await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step2Status: nextProps.step_status2.Step2Status })
        this.props.mainPileGetStorage(this.props.pileid, "step_status")
      })
    }
    if (nextProps.pileAll[this.props.pileid] != undefined) {
      var edit = nextProps.pileAll[this.props.pileid]
      if (edit["2_0"] != undefined) {
        await this.setState({ Edit_Flag: edit["2_0"].data.Edit_Flag })
      }

      if(edit != undefined){
        this.setState({status5:edit.step_status.Step5Status})
      }
      
    }
    await this.setState({
      error: nextProps.error,
      data: nextProps.pileAll[nextProps.pileid],
      location: nextProps.pileAll.currentLocation
    })
    if(nextProps.process2_success_1 == true && nextProps.process2_success_1_random != this.state.process2_success_1_random){
      this.setState({process2_success_1_random:nextProps.process2_success_1_random},async()=>{
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process:2
        })
        if(this.props.startdate != '' && this.props.startdate != null){
          this.setState({ step: 1 ,startdate: this.props.startdate,saveload:false})
        }else{
          this.setState({ step: 1 ,startdate: moment().format("DD/MM/YYYY HH:mm"),saveload:false})
        }
        this.onSetStack(2,1)
      })
    }
    if(nextProps.process2_success_2 == true && nextProps.process2_success_2_random != this.state.process2_success_2_random){
      this.setState({process2_success_2_random:nextProps.process2_success_2_random},async()=>{
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process:2
        })
        this.props.onNextStep()
        this.onSetStack(3,0)
        this.setState({checksavebutton:false,saveload:false})
      })
    }
  }
  updateState(value) {
    if (value.step != null) {
      const ref = "pile" + value.process + "_" + value.step
      if (this.refs[ref].getWrappedInstance().updateState(value)) {
        this.refs[ref].getWrappedInstance().updateState(value)
      }
    } else {
      // console.log("step2", value)
    }
  }

  setStep(step) {
    return this.setState({ step: step })
  }

  async onSetStack(pro,step) {
    var data = {
      process: pro,
      step: step + 1
    }
    await this.props.setStack(data)
    await this.props.mainPileGetAllStorage()
  }

  onSetStackReducer(data) {
    this.props.setStack(data)
    this.props.mainPileGetAllStorage()
  }
  onClearStackReducer() {
    this.props.clearStack()
  }

  deleteButton = () => {
    this.props.setStack({
      process: 2,
      step: 1
    })
    const data = {
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      processid: this.state.process,
      lat: this.state.location == undefined ? 1 : this.state.location.position.lat,
      log: this.state.location == undefined ? 1 : this.state.location.position.log,
    }
    if(this.state.status5 == 0){
      Alert.alert("Warning", I18n.t("mainpile.button.confirmdelete"), [
        { text: "Cancel" },
        { text: "OK", onPress: () => this.props.pileDelete(data) }
      ])
    }else{
      Alert.alert("", I18n.t('alert.error_delete2'), [
        {
          text: "OK"
        }
      ])
    }
    
  }

  _renderStepFooter() {
    return (
      <View>
        <View
          style={{
            flexDirection: "column"
          }}
        >
          <View
            style={{
              borderWidth: 1
            }}
          >
            <StepIndicator
              ref={"Indicator"}
              stepCount={2}
              onPress={step => {
                if (this.refs["pile2_1"].getWrappedInstance().state.loading != true) {
                  this.onSetStack(2,step)
                  this.setState({ step: step })
                }
              }}
              currentPosition={this.state.step}
            />
          </View>
          {
            this.state.Edit_Flag == 1 ?
            <View
              style={[
                styles.second,
                this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
              ]}
            >
              <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}
              // disabled={this.state.status5 == 0 ? false:true}
              >
                <Text style={styles.selectButton}>{I18n.t("mainpile.button.delete")}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.secondButton,
                  this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                ]}
                onPress={this.nextButton}
                disabled={this.state.checksavebutton}
              >
                <Text style={styles.selectButton}>
                  {this.state.Edit_Flag == 1 ? I18n.t("mainpile.button.next") : I18n.t("mainpile.button.continue")}
                </Text>
              </TouchableOpacity>
            </View>
            :
            <View/>
          }
        </View>
      </View>
    )
  }

  nextButton = async () => {
    switch (await this.state.step) {
      case 0:
        this.onSave(this.state.step)
        break
      case 1:
        this.onSave(this.state.step)
        break
      default:
        break
    }
  }

  onGetChildData(step) {
    if (this.refs["pile2_" + step].getWrappedInstance().getState() != undefined) {
      const dataChild = this.refs["pile2_" + step].getWrappedInstance().getState()
      return dataChild
    }
  }

  async onSave(step) {
    
        // const data = this.onGetChildData(step+1)
        await this.props.mainPileGetAllStorage()
        if (step == 0) {
          var value = {
            process: 2,
            step: 1,
            pile_id: this.props.pileid
          }
          // await this.props.mainPileSetStorage(this.props.pileid,'2_1',value)
          // console.log(this.state.data)
          if (this.state.Edit_Flag == 1) {
            // console.warn('process2l',this.state.data["2_1"].data.MachineCraneId)
            this.props.mainpileAction_checkStepStatus(value).then(async data => {

              var valueApi = {
                Language: I18n.locale,
                JobId: this.props.jobid,
                PileId: this.props.pileid,
                Page: 1,
                Length: this.state.data["2_1"].data.Length == null ? 15 : this.state.data["2_1"].data.Length,
                MachineCraneId: this.state.data["2_1"].data.MachineCraneId,
                MachineCraneName: this.state.data["2_1"].data.MachineCraneName,
                MachineVibroId: this.state.data["2_1"].data.MachineVibroId,
                MachineVibroName: this.state.data["2_1"].data.MachineVibroName,
                DriverId: this.state.data["2_1"].data.DriverId,
                DriverName: this.state.data["2_1"].data.DriverName,
                latitude: this.state.location == undefined ? 1 : this.state.location.position.lat,
                longitude: this.state.location == undefined ? 1 : this.state.location.position.log
              }
             

              if (this.state.error == null) {
                if (data.check == false) {
                  Alert.alert("", data.errorText[0], [
                    {
                      text: "OK"
                    }
                  ])
                  this.setState({checksavebutton:false,saveload:false})
                } else {
                  this.setState({saveload:true,statuscolor:data.statuscolor})
                
                      this.props.pileSaveStep02(valueApi)
                
                }
              } else {
                Alert.alert(this.state.error)
                this.setState({checksavebutton:false,saveload:false})
              }
            })
          } else {
            if(this.props.startdate != '' && this.props.startdate != null){
              this.setState({ step: 1 ,startdate: this.props.startdate})
            }else{
              this.setState({ step: 1 ,startdate: moment().format("DD/MM/YYYY HH:mm")})
            }
            this.onSetStack(2,1)
          }
        } else {
          this.setState({checksavebutton:true})
          var value = {
            process: 2,
            step: 2,
            pile_id: this.props.pileid
          }
          // await this.props.mainPileSetStorage(this.props.pileid,'2_2',value)
          // console.log(this.state.data)

          if (this.state.Edit_Flag == 1) {
            
            this.props.mainpileAction_checkStepStatus(value).then(async data => {
              

              // console.log("color", data)
              var ImagePlummet = []
              var ImageWaterLevel = []
              if (
                this.state.data["2_2"].data.images_plummet != null &&
                this.state.data["2_2"].data.images_plummet.length > 0
              ) {
                for (var i = 0; i < this.state.data["2_2"].data.images_plummet.length; i++) {
                  ImagePlummet.push(this.state.data["2_2"].data.images_plummet[i])
                }
              }
              if (this.state.data["2_2"].data.images_water != null && this.state.data["2_2"].data.images_water.length > 0) {
                for (var i = 0; i < this.state.data["2_2"].data.images_water.length; i++) {
                  ImageWaterLevel.push(this.state.data["2_2"].data.images_water[i])
                }
              }
              if (this.state.error == null) {
                if (data.check == false) {
                  Alert.alert("", data.errorText[0], [
                    {
                      text: "OK"
                    }
                  ])
                  this.setState({checksavebutton:false,saveload:false})
                } else {
                  if(this.state.data["2_2"].enddate != ''){
                    var end = this.state.data["2_2"].enddate
                    var valueend = {
                      process:2,
                      step:2,
                      pile_id: this.props.pileid,
                      data:{
                        checkplummet: this.state.data["2_2"].data.checkplummet,
                        checkwater: this.state.data["2_2"].data.checkwater,
                        images_plummet: this.state.data["2_2"].data.images_plummet,
                        images_water: this.state.data["2_2"].data.images_water,
                      },
                      startdate: this.state.data["2_2"].startdate,
                      enddate: end
                    }
                    await this.props.mainPileSetStorage(this.props.pileid,'2_2',valueend)
                  }else{
                    var end = moment().format("DD/MM/YYYY HH:mm")
                    var valueend = {
                      process:2,
                      step:2,
                      pile_id: this.props.pileid,
                      data:{
                        checkplummet: this.state.data["2_2"].data.checkplummet,
                        checkwater: this.state.data["2_2"].data.checkwater,
                        images_plummet: this.state.data["2_2"].data.images_plummet,
                        images_water: this.state.data["2_2"].data.images_water,
                      },
                      startdate: this.state.data["2_2"].startdate,
                      enddate: end
                    }
                    await this.props.mainPileSetStorage(this.props.pileid,'2_2',valueend)
                  }
                  var start = moment(this.state.data["2_2"].startdate,"DD/MM/YYYY HH:mm")
                  var end1 = moment(end,"DD/MM/YYYY HH:mm")
                  if(this.state.data["2_2"].startdate != undefined){
                    if(start <= end1){
                      var valueApi = {
                        Language: I18n.locale,
                        JobId: this.props.jobid,
                        PileId: this.props.pileid,
                        Page: 2,
                        CheckPlummet: this.state.data["2_2"].data.checkplummet,
                        CheckWaterLevel: this.state.data["2_2"].data.checkwater,
                        ImagePlummet: ImagePlummet,
                        ImageWaterLevel: ImageWaterLevel,
                        latitude: this.state.location == undefined ? 1 : this.state.location.position.lat,
                        longitude: this.state.location == undefined ? 1 : this.state.location.position.log,
                        startdate:moment(this.state.data["2_2"].startdate,'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm:ss'),
                        enddate:moment(end,'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm:ss')
                      }
                      
                      this.setState({saveload:true,statuscolor:data.statuscolor})
                      
                          this.props.pileSaveStep02(valueApi)
                     
                      
                    }else{
                      Alert.alert("", I18n.t("mainpile.liquidtestinsert.alert.error4"), [
                        {
                          text: "OK"
                        }
                      ])
                      this.setState({checksavebutton:false})
                    }
                  }else{
                    if(this.props.startdate <= this.props.enddate){
                      var valueApi = {
                        Language: I18n.locale,
                        JobId: this.props.jobid,
                        PileId: this.props.pileid,
                        Page: 2,
                        CheckPlummet: this.state.data["2_2"].data.checkplummet,
                        CheckWaterLevel: this.state.data["2_2"].data.checkwater,
                        ImagePlummet: ImagePlummet,
                        ImageWaterLevel: ImageWaterLevel,
                        latitude: this.state.location == undefined ? 1 : this.state.location.position.lat,
                        longitude: this.state.location == undefined ? 1 : this.state.location.position.log,
                        startdate:moment(this.props.startdate,'DD/MM/YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss'),
                        enddate:moment(this.props.enddate,'DD/MM/YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss')
                      }
                      // console.log("pile2_2", valueApi)
                      // this.props.pileSaveStep02(valueApi)
          
                      if (this.state.error == null) {
                        if (data.check == false) {
                          Alert.alert("", I18n.t("mainpile.2_2.error_nextstep"), [
                            {
                              text: "OK"
                            }
                          ])
                          this.setState({checksavebutton:false})
                        } else {
                          this.setState({saveload:true,statuscolor:data.statuscolor})
                         
                              this.props.pileSaveStep02(valueApi)
                         
                        }
                      } else {
                        Alert.alert(this.state.error)
                        this.setState({checksavebutton:false,saveload:false})
                      }
                      // this.props.onNextStep()
                      // this.onSetStack(3,0)
                      // setTimeout(()=>{this.setState({checksavebutton:false})},2000)
                    }else{
                      Alert.alert("", I18n.t("mainpile.liquidtestinsert.alert.error4"), [
                        {
                          text: "OK"
                        }
                      ])
                      this.setState({checksavebutton:false})
                    }
                  }
                
                }
              } else {
                Alert.alert(this.state.error)
                this.setState({checksavebutton:false,saveload:false})
              }
              
              
            })
          } else {
            this.props.onNextStep()
            this.onSetStack(3,0)
            setTimeout(()=>{this.setState({checksavebutton:false})},2000)
          }
        }
      
    
  }

  initialStep() {
    this.setState({ step: 0 })
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    return (
      <View style={{ flex: 1 }}>
        {this.state.saveload?<Content><ActivityIndicator size="large" color="#007CC2" /></Content>:<View/>}
        {!this.state.saveload?<Content>
          <Pile2_1
            ref="pile2_1"
            hidden={!(this.state.step == 0)}
            jobid={this.props.jobid}
            pileid={this.props.pileid}
            pile={this.props.pile}
            onRefresh={this.props.onRefresh}
          />
          <Pile2_2
            ref="pile2_2"
            hidden={!(this.state.step == 1)}
            jobid={this.props.jobid}
            pileid={this.props.pileid}
            shape={this.props.shape}
            category={this.props.category}
            startdatedefault={this.state.startdate}
            startdate={this.props.startdate}
            enddate={this.props.enddate}
          />
        </Content>:<View/>}
        {this._renderStepFooter()}
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    error: state.pile.error,
    pileAll: state.mainpile.pileAll || null,
    stack: state.mainpile.stack_item,
    valueInfo: state.pile.valueInfo,
    process2_success_1:state.pile.process2_success_1,
    process2_success_1_random:state.pile.process2_success_1_random,
    process2_success_2:state.pile.process2_success_2,
    process2_success_2_random:state.pile.process2_success_2_random,

    step_status2_random:state.mainpile.step_status2_random,
    step_status2:state.mainpile.step_status2,
    processstatus:state.mainpile.process
  }
}

export default connect(mapStateToProps, actions, null, { withRef: true })(Pile2)
