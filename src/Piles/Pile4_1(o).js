import React, {Component} from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  Dimensions
} from "react-native"
import {Container, Content} from "native-base"
import {Icon} from "react-native-elements"
import Carousel from "react-native-snap-carousel"
import ModalSelector from "react-native-modal-selector"
import {
  MAIN_COLOR,
  SUB_COLOR,
  BASIC_COLOR,
  MENU_GREY_ITEM,
  BUTTON_COLOR,
  BLUE_COLOR
} from "../Constants/Color"
import {GroupEmployee} from "../Controller/API"
import I18n from "../../assets/languages/i18n"
import {Extra} from "../Controller/API"
import TableView from "../Components/TableView"
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux'
import * as actions from '../Actions'
import styles from './styles/Pile4_1.style'
const data = [
  {
    key: 0,
    section: true,
    label: 'ตัวเลือก',
    action: "head"
  },
  {
    key: 1,
    label: 'แก้ไข',
    action: "edit"
  }, {
    key: 2,
    label: 'ลบ',
    action: 'delete'
  }
]
const count = 0

class Pile4_1 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 4,
      step: 1,
      data: [],
      DrillingFluids: [],
      location: {
        position: {
          lat: 1,
          log: 1
        }
      },
      Name: 'Test',
      pause: null,
      loading: true,
      dataMaster: null,
      Edit_Flag: 1,
      data4_0:[],
    }
  }

  componentDidMount() {
    console.log(this.props.onRefresh)
    if (this.props.onRefresh == 'refresh' ) {
        count = 0
        Actions.refresh({action: 'start'})
     
    }
  }
  componentWillReceiveProps(nextProps) {
    count = 0
    var pile = null
    var pile4_0 = null
    var pileMaster = null
    console.log('count test',count)
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['4_1']) {
      pile = nextProps.pileAll[this.props.pileid]['4_1'].data
      pileMaster = nextProps.pileAll[this.props.pileid]['4_1'].masterInfo
      this.setState({dataMaster: pileMaster,data:nextProps.pileAll[this.props.pileid]})
    }
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['4_0']) {
      pile4_0 = nextProps.pileAll[this.props.pileid]['4_0'].data
      this.setState({data4_0:pile4_0})
    }
    if (nextProps.pileAll != undefined && nextProps.pileAll != '') {
      this.setState({location: nextProps.pileAll.currentLocation})
    }

    // if (pile != null) {
    //   if (pile.DrillingFluids != null && pile.DrillingFluids.length > 0) {
    //     console.log(pile.DrillingFluids);
    //   }
    // }

    if (pile4_0) {
      // if (pile3_0.Edit_Flag != null && pile3_0.Edit_Flag != undefined && pile3_0.Edit_Flag != '') {
      this.setState({Edit_Flag: pile4_0.Edit_Flag})
      // }
    }

    if (pileMaster) {
      var list = []
      let SubNameType = ''
      count++
      console.log('count',count);
      console.log('have pliemaster')
      if (this.state.DrillingFluids.length == 0 && this.state.loading == true) {
        console.log('list0 true')
        for (var i = 0; i < pileMaster.ProcesstesterList.length; i++) {
          for (var j = 0; j < pile.DrillingFluids.length; j++) {
            if (pileMaster.ProcesstesterList[i].Id == pile.DrillingFluids[j].TypeId) {
              if (pileMaster.ProcesstesterList[i].PositionList) {
                for (var k = 0; k < pileMaster.ProcesstesterList[i].PositionList.length; k++) {
                  if (pileMaster.ProcesstesterList[i].PositionList[k].Id === pile.DrillingFluids[j].PositionId) {
                    SubNameType = pileMaster.ProcesstesterList[i].PositionList[k].Name
                  }
                }
              }
              list.push({
                NameType: pileMaster.ProcesstesterList[i].Name,
                NoType: pileMaster.ProcesstesterList[i].No,
                SubNameType:SubNameType,
                TypeId: pile.DrillingFluids[j].TypeId,
                PositionId: pile.DrillingFluids[j].PositionId,
                TesterERPId: pile.DrillingFluids[j].TesterERPId,
                Ph: pile.DrillingFluids[j].Ph,
                ImagePh: pile.DrillingFluids[j].ImagePh,
                Density: pile.DrillingFluids[j].Density,
                ImageDensity: pile.DrillingFluids[j].ImageDensity,
                Viscosity: pile.DrillingFluids[j].Viscosity,
                ImageViscosity: pile.DrillingFluids[j].ImageViscosity,
                Sand: pile.DrillingFluids[j].Sand,
                ImageSand: pile.DrillingFluids[j].ImageSand,
                No: pile.DrillingFluids[j].No,
                Id: pile.DrillingFluids[j].Id
              })
            }

          }
        }
        this.setState({DrillingFluids: list, loading: false})
      } 
      else if(this.state.loading == false && count>1){
        console.log('loading false 2 3')
        this.setState({loading:true},()=>{
          for (var i = 0; i < pileMaster.ProcesstesterList.length; i++) {
            for (var j = 0; j < pile.DrillingFluids.length; j++) {
              if (pileMaster.ProcesstesterList[i].Id == pile.DrillingFluids[j].TypeId) {
                if (pileMaster.ProcesstesterList[i].PositionList) {
                  for (var k = 0; k < pileMaster.ProcesstesterList[i].PositionList.length; k++) {
                    if (pileMaster.ProcesstesterList[i].PositionList[k].Id === pile.DrillingFluids[j].PositionId) {
                      SubNameType = pileMaster.ProcesstesterList[i].PositionList[k].Name
                    }
                  }
                }
                list.push({
                  NameType: pileMaster.ProcesstesterList[i].Name,
                  NoType: pileMaster.ProcesstesterList[i].No,
                  SubNameType:SubNameType,
                  TypeId: pile.DrillingFluids[j].TypeId,
                  PositionId: pile.DrillingFluids[j].PositionId,
                  TesterERPId: pile.DrillingFluids[j].TesterERPId,
                  Ph: pile.DrillingFluids[j].Ph,
                  ImagePh: pile.DrillingFluids[j].ImagePh,
                  Density: pile.DrillingFluids[j].Density,
                  ImageDensity: pile.DrillingFluids[j].ImageDensity,
                  Viscosity: pile.DrillingFluids[j].Viscosity,
                  ImageViscosity: pile.DrillingFluids[j].ImageViscosity,
                  Sand: pile.DrillingFluids[j].Sand,
                  ImageSand: pile.DrillingFluids[j].ImageSand,
                  No: pile.DrillingFluids[j].No,
                  Id: pile.DrillingFluids[j].Id
                })
              }

            }
          }
          setTimeout(()=>{
            this.setState({DrillingFluids: list, loading: false})
          },2000)
        })
      }
      else {
        if (this.state.loading == true) {
          console.log('else true')
          setTimeout(() => {
            this.setState({loading: false})
          }, 100)
        }
      }
    }
  }

  updateState(value) {
    // console.log('value',value);
    console.log('updatestate')
    this.props.pile4ClearId()
    var listDrillingFluids = []
    var index = 0
    if (this.state.DrillingFluids.length > 0 && value.data.LiquidTestInsertType == 'edit') {
      console.log('update edit')
      listDrillingFluids = this.state.DrillingFluids
      index = value.data.index
      listDrillingFluids[index].NameType = value.data.ProcessTesterList
      listDrillingFluids[index].NoType = value.data.ProcessTesterListNo
      listDrillingFluids[index].TypeId = value.data.ProcessTesterListID
      listDrillingFluids[index].SubNameType = value.data.SubProcessTesterList
      listDrillingFluids[index].SubNoType = value.data.SubProcessTesterListNo
      listDrillingFluids[index].PositionId = value.data.SubProcessTesterListID
      listDrillingFluids[index].TesterERPId = value.data.TesterListID
      listDrillingFluids[index].Ph = value.data.Ph
      listDrillingFluids[index].ImagePh = value.data.ImagePhFiles
      listDrillingFluids[index].Density = value.data.Density
      listDrillingFluids[index].ImageDensity = value.data.ImageDensityFiles
      listDrillingFluids[index].Viscosity = value.data.Viscosity
      listDrillingFluids[index].ImageViscosity = value.data.ImageViscosityFiles
      listDrillingFluids[index].Sand = value.data.Sand
      listDrillingFluids[index].ImageSand = value.data.ImageSandFiles
      listDrillingFluids[index].No = value.data.No
      listDrillingFluids[index].SubNo = value.data.SubNo
      listDrillingFluids[index].Id = value.data.Id,
      listDrillingFluids[index].Temp = value.data.Temp

    } else {
      console.log('update')
      if (this.state.DrillingFluids.length > 0) {
        console.log('update >0')
        listDrillingFluids = this.state.DrillingFluids
      }
      listDrillingFluids.push({
        NameType: value.data.ProcessTesterList,
        NoType: value.data.ProcessTesterListNo,
        TypeId: value.data.ProcessTesterListID,
        SubNameType: value.data.SubProcessTesterList,
        SubNoType: value.data.SubProcessTesterListNo,
        PositionId: value.data.SubProcessTesterListID,
        TesterERPId: value.data.TesterListID,
        Ph: value.data.Ph,
        ImagePh: value.data.ImagePhFiles,
        Density: value.data.Density,
        ImageDensity: value.data.ImageDensityFiles,
        Viscosity: value.data.Viscosity,
        ImageViscosity: value.data.ImageViscosityFiles,
        Sand: value.data.Sand,
        ImageSand: value.data.ImageSandFiles,
        No: value.data.No,
        SubNo: value.data.SubNo,
        Id: value.data.Id,
        Temp: value.data.Temp
      })
    }
    // console.log(listDrillingFluids);
    this.setState({
      DrillingFluids: listDrillingFluids,
      loading: true
    }, () => {
      setTimeout(() => {
        Actions.refresh()
        this.setState({loading: true})
        this.saveLocal('4_1')
      }, 3000)
    })
    // console.log('test',this.state.DrillingFluids)
  }

  async saveLocal(process_step) {
    var value = {
      process: 4,
      step: 1,
      pile_id: this.props.pileid,
      data: {
        DrillingFluids: this.state.DrillingFluids
      }
    }
    // if (this.state.DrillingFluids.length == this.state.dataMaster.ProcesstesterList.length) {
    //   await this.props.mainPileSetStorage(this.props.pileid, 'step_status', {Step4Status: 2})
    // } else {
    //   await this.props.mainPileSetStorage(this.props.pileid, 'step_status', {Step4Status: 1})
    // }
    if (this.state.data4_0.Status != 2) {
      await this.props.mainPileSetStorage(this.props.pileid, 'step_status', {Step4Status: 1})
    }
    await this.props.mainPileSetStorage(this.props.pileid, process_step, value)
    this.props.mainPileGetStorage(this.props.pileid, 'step_status')
  }

  deleteRow(index) {
    Alert.alert("", "ยืนยันการลบข้อมูล", [
      {
        text: "Cancel"
      }, {
        text: "OK",
        onPress: () => {
          // console.log(index);
          this.onDeleteItem(this.state.DrillingFluids[index])
          this.state.DrillingFluids.splice(index, 1);
          this.setState({DrillingFluids: this.state.DrillingFluids,loading:true});
          this.saveLocal('4_1')
          // var position = index + 1
          // if (position == this.state.DrillingFluids.length) {
          //   this.onDeleteItem(this.state.DrillingFluids[index])
          //   this.state.DrillingFluids.splice(index, 1);
          //   this.setState({DrillingFluids: this.state.DrillingFluids});
          //   this.saveLocal('4_1')
          // } else {
          //   Alert.alert("เกิดข้อผิดพลาด", 'ไม่สามารถลบข้อมูลได้', [
          //     {
          //       text: "OK"
          //     }
          //   ])
          // }
        }
      }
    ], {cancelable: false})

  }

  onAddData() {
    Actions.liquidTestInsert({
      jobid: this.props.jobid,
      pileid: this.props.pileid,
      process: this.state.process,
      step: this.state.step,
      Edit_Flag: '1',
      lat: this.state.location == undefined
        ? ''
        : this.state.location.position.lat,
      log: this.state.location == undefined
        ? ''
        : this.state.location.position.log,
      LiquidTestInsertType: 'add',
      datasize: this.state.DrillingFluids.length,
      shape: this.props.shape
    })
  }

  onDeleteItem(data) {
    count = 0
    var value = {
      Language: I18n.locale,
      JobId: this.props.jobid,
      PileId: this.props.pileid,
      // No: data.No,
      // TypeId: data.TypeId,
      // PositionId: data.PositionId,
      DrillingFluidId:data.Id,
      lat: this.state.location == undefined
        ? ''
        : this.state.location.position.lat,
      log: this.state.location == undefined
        ? ''
        : this.state.location.position.log,
    }
    // console.log('data',data);
    this.props.pile4DeleteRow(value)
  }

  onEditData(index) {
    var list = this.state.DrillingFluids
    Actions.liquidTestInsert({
      jobid: this.props.jobid,
      pileid: this.props.pileid,
      process: this.state.process,
      step: this.state.step,
      Edit_Flag: this.state.Edit_Flag,
      lat: this.state.location == undefined
        ? ''
        : this.state.location.position.lat,
      log: this.state.location == undefined
        ? ''
        : this.state.location.position.log,
      DrillingFluids: this.state.DrillingFluids[index],
      LiquidTestInsertType: 'edit',
      index: index,
      shape: this.props.shape,
      edit: true
    })
  }

  onScroll = (data) => {
    let offset = 0
    var currentOffsety = data.nativeEvent.contentOffset.y
    var currentOffsetx = data.nativeEvent.contentOffset.x
    // var direction = currentOffset > this.offset ? 'down' : 'up'
    // offset = currentOffset
    this.refs.scrollView.scrollTo({x: currentOffsetx, y: 0, animated: true})
    // console.log(currentOffsety)
    // console.log(currentOffsetx)
    // console.log(this.refs);
  }

  _renderTable() {
    let index = 0
    if (this.state.data['4_0'].data.Edit_Flag == 0) {
      data = [
        {
          key: index++,
          section: true,
          label: 'ตัวเลือก',
          action: "head"
        },
        {
          key: index++,
          label: 'ดูรายละเอียด',
          action: "edit"
        }
      ]
    }else {
      data = [
        {
          key: 0,
          section: true,
          label: 'ตัวเลือก',
          action: "head"
        },
        {
          key: 1,
          label: 'แก้ไข',
          action: "edit"
        }, {
          key: 2,
          label: 'ลบ',
          action: 'delete'
        }
      ]
    }
    if (Dimensions.get('screen').width > 360) {
      return (
        <View style={styles.listTable}>

        <View style={styles.scrollData}>
          {/* start */}

          <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollLeft} onScroll={this.onScroll}>

            <View style={styles.tableRow}>

              <View style={[styles.tableCol,{width:'20%' }]}>
               {
                this.state.DrillingFluids.map((item, key) => {
                return (<View key={key} style={[styles.tableContent,(item.Temp ===1?{backgroundColor:'#F7F6BD'}:{})]}>
                 <View key={key} style={styles.tableContentCol}>
                        <ModalSelector
                          data={data}
                          cancelText="Cancel"
                          onChange={(option) => {
                             if (option.action == "edit") {
                               this.onEditData(key)
                             } else if (option.action == "delete") {
                               this.deleteRow(key)
                             }
                           }}
                           >
                          <View style={styles.tableContentArea}>
                            <Text style={styles.tableContentText}>{item.SubNameType !== '' ? (key+1) + '\n' + item.NameType + '\n' +'(' +item.SubNameType + ')' : (key+1) + '\n' + item.NameType }</Text>
                          </View>
                        </ModalSelector>
                        <View style={styles.tableContentBorder}></View>
                      </View>    
                </View>)
              })
            }
              </View>
              {/* <ScrollView horizontal={true} onScroll={this.onScroll} showsHorizontalScrollIndicator={false}> */}
              <View style={[styles.tableCol,{width:'20%' }]}>
              {
                this.state.DrillingFluids.map((item, key) => {
                  return (<View key={key} style={[styles.tableContent,(item.Temp ===1?{backgroundColor:'#F7F6BD'}:{})]}>
                 <View key={key} style={styles.tableContentCol}>
                        <ModalSelector
                          data={data}
                          cancelText="Cancel"
                          onChange={(option) => {
                             if (option.action == "edit") {
                               this.onEditData(key)
                             } else if (option.action == "delete") {
                               this.deleteRow(key)
                             }
                           }}
                           >
                          <View style={styles.tableContentArea}>
                            <Text style={styles.tableContentText}>{isNaN(parseFloat(item.Ph).toFixed(2)) ? '' : parseFloat(item.Ph).toFixed(2)}</Text>
                          </View>
                        </ModalSelector>
                        <View style={styles.tableContentBorder}></View>
                      </View>
                </View>)
                })
              }
              </View>

              {/* <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} onScroll={this.onScroll}> */}

                <View style={[styles.tableCol,{width:'20%' }]}>
                {
                  this.state.DrillingFluids.map((item, key) => {
                    return (<View key={key} style={[styles.tableContent,(item.Temp ===1?{backgroundColor:'#F7F6BD'}:{})]}>
                    <View key={key} style={styles.tableContentCol}>
                          <ModalSelector
                            data={data}
                            cancelText="Cancel"
                            onChange={(option) => {
                               if (option.action == "edit") {
                                 this.onEditData(key)
                               } else if (option.action == "delete") {
                                 this.deleteRow(key)
                               }
                             }}
                             >
                              <View style={styles.tableContentArea}>
                                <Text style={styles.tableContentText}>{isNaN(parseFloat(item.Density).toFixed(2)) ? '' : parseFloat(item.Density).toFixed(2)}</Text>
                              </View>
                          </ModalSelector>
                          <View style={styles.tableContentBorder}></View>
                        </View>
                      
                  </View>)
                  })
                }
                </View>

                <View style={[styles.tableCol,{width:'20%' }]}>
                {
                  this.state.DrillingFluids.map((item, key) => {
                    return (<View key={key} style={[styles.tableContent,(item.Temp ===1?{backgroundColor:'#F7F6BD'}:{})]}>
                   <View key={key} style={styles.tableContentCol}>
                          <ModalSelector
                            data={data}
                            cancelText="Cancel"
                            onChange={(option) => {
                               if (option.action == "edit") {
                                 this.onEditData(key)
                               } else if (option.action == "delete") {
                                 this.deleteRow(key)
                               }
                             }}
                             >
                          <View style={styles.tableContentArea}>
                            <Text style={styles.tableContentText}>{item.Viscosity}</Text>
                          </View>
                          </ModalSelector>
                          <View style={styles.tableContentBorder}></View>
                        </View> 
                  </View>)
                  })
                }
                </View>

                <View style={[styles.tableCol,{width:'20%' }]}>
                {
                  this.state.DrillingFluids.map((item, key) => {
                    return (<View key={key} style={[styles.tableContent,(item.Temp ===1?{backgroundColor:'#F7F6BD'}:{})]}>
                    <View key={key} style={styles.tableContentCol}>
                          <ModalSelector
                            data={data}
                            cancelText="Cancel"
                            onChange={(option) => {
                               if (option.action == "edit") {
                                 this.onEditData(key)
                               } else if (option.action == "delete") {
                                 this.deleteRow(key)
                               }
                             }}
                             >
                          <View style={styles.tableContentArea}>
                            <Text style={styles.tableContentText}>{isNaN(parseFloat(item.Sand).toFixed(2)) ? '' : parseFloat(item.Sand).toFixed(2)}</Text>
                          </View>
                          </ModalSelector>
                          <View style={styles.tableContentBorder}></View>
                        </View>  
                  </View>)
                  })
                }
                </View>

                {/* <View style={styles.tableCol}>
                  <View style={[styles.tableContent, styles.tabelExtra]}>
                    {
                      this.state.DrillingFluids.map((item, key) => {
                        return (<View key={key} style={styles.tableContentCol}>
                          <View style={styles.tableContentArea}>
                          <View style={styles.tableAction}>
                            <TouchableOpacity activeOpacity={0.8} onPress={() => this.onEditData(key)}>
                              <View style={styles.buttonEdit}>
                                <Image source={require('../../assets/image/icon_edit.png')}/>
                              </View>
                            </TouchableOpacity>
                            {
                              this.state.Edit_Flag == 1
                                ? <TouchableOpacity activeOpacity={0.8} onPress={() => this.deleteRow(key)}>
                                    <View style={styles.buttonDelete}>
                                      <Image source={require('../../assets/image/icon_bin.png')}/>
                                    </View>
                                  </TouchableOpacity>
                                : <TouchableOpacity disabled={true} activeOpacity={0.8} onPress={() => this.deleteRow(key)}>
                                    <View style={[
                                        styles.buttonDelete, {
                                          borderColor: MENU_GREY_ITEM
                                        }
                                      ]}>
                                      <Image source={require('../../assets/image/icon_bin_disable.png')}/>
                                    </View>
                                  </TouchableOpacity>
                            }
                          </View>
                        </View>
                        <View style={styles.tableContentBorder}></View>
                        </View>)
                      })
                    }
                  </View>
                </View> */}

              {/* </ScrollView> */}
                {/* </ScrollView> */}
            </View>
          </ScrollView>

          {/* end */}
        </View>
      </View>)
    }else {
      return (
        <View style={styles.listTable}>

        <View style={styles.scrollData}>
          {/* start */}

          <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollLeft} onScroll={this.onScroll}>

            <View style={styles.tableRow}>

              <View style={[styles.tableCol,{width: 80}]}>
              {
                this.state.DrillingFluids.map((item, key) => {
                  return (<View key={key} style={[styles.tableContent,(item.Temp ===1?{backgroundColor:'#F7F6BD'}:{})]}>
                 <View  style={styles.tableContentCol}>
                        <ModalSelector
                          data={data}
                          cancelText="Cancel"
                          onChange={(option) => {
                             if (option.action == "edit") {
                               this.onEditData(key)
                             } else if (option.action == "delete") {
                               this.deleteRow(key)
                             }
                           }}
                           >
                          <View style={styles.tableContentArea}>
                            <Text numberOfLines={1} style={styles.tableContentText}>{key+1}</Text>
                            <Text numberOfLines={1} style={styles.tableContentText}>{item.NameType}</Text>
                            <Text numberOfLines={1} style={styles.tableContentText}>{item.SubNameType !== '' ? '(' +item.SubNameType + ')' : item.SubNameType }</Text>
                          </View>
                        </ModalSelector>
                        <View style={styles.tableContentBorder}></View>
                      </View>
                   </View>)
                })
              }
              </View>
              <ScrollView horizontal={true} onScroll={this.onScroll} showsHorizontalScrollIndicator={false}>
              <View style={[styles.tableCol,{width: 60}]}>
              {
                this.state.DrillingFluids.map((item, key) => {
                  return (<View key={key} style={[styles.tableContent,(item.Temp ===1?{backgroundColor:'#F7F6BD'}:{})]}>
                 <View key={key} style={styles.tableContentCol}>
                        <ModalSelector
                          data={data}
                          cancelText="Cancel"
                          onChange={(option) => {
                             if (option.action == "edit") {
                               this.onEditData(key)
                             } else if (option.action == "delete") {
                               this.deleteRow(key)
                             }
                           }}
                           >
                          <View style={styles.tableContentArea}>
                            <Text  style={styles.tableContentText}>{isNaN(parseFloat(item.Ph).toFixed(2)) ? '' : parseFloat(item.Ph).toFixed(2)}</Text>
                          </View>
                        </ModalSelector>
                        <View style={styles.tableContentBorder}></View>
                      </View>
                </View>)
                })
              }
              </View>

              {/* <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} onScroll={this.onScroll}> */}

                <View style={[styles.tableCol,{width: 70}]}>
                {
                  this.state.DrillingFluids.map((item, key) => {
                    return (<View key={key} style={[styles.tableContent,(item.Temp ===1?{backgroundColor:'#F7F6BD'}:{})]}>
                    <View key={key} style={styles.tableContentCol}>
                          <ModalSelector
                            data={data}
                            cancelText="Cancel"
                            onChange={(option) => {
                               if (option.action == "edit") {
                                 this.onEditData(key)
                               } else if (option.action == "delete") {
                                 this.deleteRow(key)
                               }
                             }}
                             >
                              <View style={styles.tableContentArea}>
                                <Text style={styles.tableContentText}>{isNaN(parseFloat(item.Density).toFixed(2)) ? '' : parseFloat(item.Density).toFixed(2)}</Text>
                              </View>
                          </ModalSelector>
                          <View style={styles.tableContentBorder}></View>
                        </View>
                  </View>)
                  })
                }
                </View>

                <View style={[styles.tableCol,{width: 80}]}>
                {
                  this.state.DrillingFluids.map((item, key) => {
                    return (<View key={key} style={[styles.tableContent,(item.Temp ===1?{backgroundColor:'#F7F6BD'}:{})]}>
                    <View key={key} style={styles.tableContentCol}>
                          <ModalSelector
                            data={data}
                            cancelText="Cancel"
                            onChange={(option) => {
                               if (option.action == "edit") {
                                 this.onEditData(key)
                               } else if (option.action == "delete") {
                                 this.deleteRow(key)
                               }
                             }}
                             >
                          <View style={styles.tableContentArea}>
                            <Text style={styles.tableContentText}>{item.Viscosity}</Text>
                          </View>
                          </ModalSelector>
                          <View style={styles.tableContentBorder}></View>
                        </View>   
                  </View>)
                  })
                }
                </View>

                <View style={[styles.tableCol,{width:  70}]}>
                {
                  this.state.DrillingFluids.map((item, key) => {
                    return (<View key={key} style={[styles.tableContent,(item.Temp ===1?{backgroundColor:'#F7F6BD'}:{})]}>
                    <View key={key} style={styles.tableContentCol}>
                          <ModalSelector
                            data={data}
                            cancelText="Cancel"
                            onChange={(option) => {
                               if (option.action == "edit") {
                                 this.onEditData(key)
                               } else if (option.action == "delete") {
                                 this.deleteRow(key)
                               }
                             }}
                             >
                          <View style={styles.tableContentArea}>
                            <Text style={styles.tableContentText}>{isNaN(parseFloat(item.Sand).toFixed(2)) ? '' : parseFloat(item.Sand).toFixed(2)}</Text>
                          </View>
                          </ModalSelector>
                          <View style={styles.tableContentBorder}></View>
                        </View>
                  </View>)
                })
              }
                </View>

                {/* <View style={styles.tableCol}>
                  <View style={[styles.tableContent, styles.tabelExtra]}>
                    {
                      this.state.DrillingFluids.map((item, key) => {
                        return (<View key={key} style={styles.tableContentCol}>
                          <View style={styles.tableContentArea}>
                          <View style={styles.tableAction}>
                            <TouchableOpacity activeOpacity={0.8} onPress={() => this.onEditData(key)}>
                              <View style={styles.buttonEdit}>
                                <Image source={require('../../assets/image/icon_edit.png')}/>
                              </View>
                            </TouchableOpacity>
                            {
                              this.state.Edit_Flag == 1
                                ? <TouchableOpacity activeOpacity={0.8} onPress={() => this.deleteRow(key)}>
                                    <View style={styles.buttonDelete}>
                                      <Image source={require('../../assets/image/icon_bin.png')}/>
                                    </View>
                                  </TouchableOpacity>
                                : <TouchableOpacity disabled={true} activeOpacity={0.8} onPress={() => this.deleteRow(key)}>
                                    <View style={[
                                        styles.buttonDelete, {
                                          borderColor: MENU_GREY_ITEM
                                        }
                                      ]}>
                                      <Image source={require('../../assets/image/icon_bin_disable.png')}/>
                                    </View>
                                  </TouchableOpacity>
                            }
                          </View>
                        </View>
                        <View style={styles.tableContentBorder}></View>
                        </View>)
                      })
                    }
                  </View>
                </View> */}

              {/* </ScrollView> */}
                </ScrollView>
            </View>
          </ScrollView>

          {/* end */}
        </View>
      </View>)
    }
  }

  renderHead(){
    if (Dimensions.get('screen').width > 360) {
      return(
        <View style={styles.tableRow}>
          <View style={[styles.tableHead,{width:'20%'}]}>
            <Text style={styles.tableHeadText}>#{"\n"}</Text>
          </View>
          {/* <ScrollView contentContainerStyle={{width:'100%'}} ref={'scrollView'} horizontal={true} scrollEnabled={false} showsHorizontalScrollIndicator={false} > */}
          <View style={[styles.tableHead,{width:'20%'}]}>
            <Text style={styles.tableHeadText}>pH{"\n"}</Text>
          </View>
          {/* <ScrollView showsHorizontalScrollIndicator={false} scrollEnabled={false} horizontal={true} ref='scrollView'> */}
            <View style={[styles.tableHead,{width:'20%'}]}>
              <Text style={styles.tableHeadText}>Density{"\n"}{I18n.t("mainpile.4_1.unit_density")}</Text>
            </View>
            <View style={[styles.tableHead,{width:'20%'}]}>
              <Text style={styles.tableHeadText}>Viscosity{"\n"}{I18n.t("mainpile.4_1.unit_viscosity")}</Text>
            </View>
            <View style={[styles.tableHead,{width:'20%'}]}>
              <Text style={styles.tableHeadText}>% Sand</Text>
            </View>
            {/* <View style={[styles.tableHead, styles.tabelExtra]}>
              <Text style={styles.tableHeadText}></Text>
            </View> */}
          {/* </ScrollView> */}
          {/* </ScrollView> */}
        </View>
      )
    }else {
      return(
        <View style={styles.tableRow}>
          <View style={[styles.tableHead,{width: 80}]}>
            <Text style={styles.tableHeadText}>#{"\n"}</Text>
          </View>
          <ScrollView ref={'scrollView'} horizontal={true} scrollEnabled={false} showsHorizontalScrollIndicator={false} >
          <View style={[styles.tableHead,{width: 60}]}>
            <Text style={styles.tableHeadText}>pH{"\n"}</Text>
          </View>
          {/* <ScrollView showsHorizontalScrollIndicator={false} scrollEnabled={false} horizontal={true} ref='scrollView'> */}
            <View style={[styles.tableHead,{width: 70}]}>
              <Text style={styles.tableHeadText}>Density{"\n"}{I18n.t("mainpile.4_1.unit_density")}</Text>
            </View>
            <View style={[styles.tableHead,{width:  80}]}>
              <Text style={styles.tableHeadText}>Viscosity{"\n"}{I18n.t("mainpile.4_1.unit_viscosity")}</Text>
            </View>
            <View style={[styles.tableHead,{width:  70}]}>
              <Text style={styles.tableHeadText}>% Sand</Text>
            </View>
            {/* <View style={[styles.tableHead, styles.tabelExtra]}>
              <Text style={styles.tableHeadText}></Text>
            </View> */}
          {/* </ScrollView> */}
          </ScrollView>
        </View>
      )
    }
  }

  render() {
    // if (this.state.loading && this.props.pile4Type == 'content') {
    //   return (<View style={{
    //       flex: 1,
    //       justifyContent: 'center'
    //     }}>
    //     <ActivityIndicator size="large" color="#007CC2"/>
    //   </View>)
    // }
      return (
        <View style={{height:'70%'}}>
          <View>
            <View style={styles.listTopic}>
              <Text style={styles.listTopicText}>{I18n.t("mainpile.4_1.title")}</Text>
              {
                this.state.Edit_Flag == 1
                  ? <TouchableOpacity activeOpacity={0.8} onPress={() => this.onAddData()}>
                      <View style={styles.listButton}>
                        <Image source={require('../../assets/image/icon_add.png')}/>
                        <Text style={styles.listButtonText}>{I18n.t("mainpile.4_1.add_data")}</Text>
                      </View>
                    </TouchableOpacity>
                  : <View/>
              }
            </View>
            <View style={{marginTop:10}}>
                {this.renderHead()}
            </View>
            <ScrollView>
            {
              this.state.DrillingFluids.length > 0
                ?
                <View style={{marginTop:this.props.pile4Type === 'head' ? 10: 0}}>
                  {/* <ScrollView horizontal={true} onScroll={this.onScroll}> */}
                    {this._renderTable()}
                  {/* </ScrollView> */}

                  </View>
                : <View style={styles.listTableDetail}>
                  <Text style={styles.listTableDetailText}>{I18n.t("mainpile.4_1.no_data")}{"\n"}
                    <Text style={styles.textHighligh}>{I18n.t("mainpile.4_1.add_data")}</Text>
                    {I18n.t("mainpile.4_1.add_data_test")}</Text>
                  </View>
            }
            </ScrollView>
          </View>

      </View>
      )

  }
}

const mapStateToProps = state => {
  return {pileAll: state.mainpile.pileAll}
}

export default connect(mapStateToProps, actions, null, {withRef: true})(Pile4_1)
