import React, { Component } from "react"
import {
  View,
  Text,
  Button,
  ScrollView,
  TouchableOpacity,
  BackHandler
} from "react-native"
import { Container, Content } from "native-base"
import { MENU_GREY_ITEM, MAIN_COLOR } from "../Constants/Color"
import { Icon } from "react-native-elements"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"
import I18n from "../../assets/languages/i18n"
import styles from "./styles/SelfAprrove.style"

class SelfApprove extends Component {
  constructor(props) {
    super(props)
    this.state = {
      accept: false,
      acceptcheck: false
    }
    console.log("proppppp" + this.props.jobid)
  }

  componentDidMount() {
    BackHandler.addEventListener("back", () => this._onBack())
    if (this.props.check10 == true) {
      this.setState({ acceptcheck: true })
    }
  }
  componentWillUnmount() {
    BackHandler.removeEventListener("back", () => this._onBack())
  }

  onApprove() {
    this.props.clearError()
    var value = {
      Token: this.state.token,
      Language: I18n.locale,
      JobId: this.props.jobid,
      ApproveType: 2,
      PileId: this.props.pileid,
      LocationLat: this.props.lat,
      LocationLong: this.props.log
    }
    if (this.props.process == 3) {
      this.props.pileSaveApproveStep03(value)
    }
    Actions.pop({
      refresh: {
        action: "update",
        value: {
          process: this.props.process,
          step: this.props.step,
          isapprove: false,
          approveBy: 2
        }
      }
    })
    return true
  }

  _onBack() {
    Actions.pop()
    return true
  }

  onBack() {
    Actions.pop()
  }

  render() {
    let textSelfAprrove = null
    if (this.state.acceptcheck == true) {
      textSelfAprrove = I18n.t("mainpile.selfapprove.accepttext")
    } else {
      textSelfAprrove = I18n.t("mainpile.selfapprove.textself")
    }

    return (
      <View
        style={{
          flex: 1
        }}
      >
        <View style={styles.topic}>
          <Text style={styles.topicText}>
            {this.state.acceptcheck != true
              ? I18n.t("mainpile.selfapprove.selfapprove")
              : I18n.t("mainpile.selfapprove.selfaccept")}
          </Text>
        </View>
        <ScrollView style={styles.scroll}>
          <Text style={[styles.scrollText, styles.textBold]}>
            {this.state.acceptcheck != true
              ? I18n.t("mainpile.selfapprove.condition")
              : I18n.t("mainpile.selfapprove.acceptcondition")}
          </Text>
          <Text numberOfLine={5} style={styles.scrollText}>
            {"     "}
            {this.state.acceptcheck != true
              ? I18n.t("mainpile.selfapprove.textself")
              : I18n.t("mainpile.selfapprove.accepttext")}
          </Text>
        </ScrollView>
        <View style={styles.acceptStyle}>
          {this.state.accept ? (
            <Icon
              name="check"
              reverse
              color="#6dcc64"
              size={15}
              onPress={() => {
                this.setState({
                  accept: !this.state.accept
                })
              }}
            />
          ) : (
            <Icon
              name="check"
              reverse
              color="grey"
              size={15}
              onPress={() => {
                this.setState({
                  accept: !this.state.accept
                })
              }}
            />
          )}
          <Text style={[styles.textCheck,{fontSize:I18n.locale=='en'?12:16}]}>
            {this.state.acceptcheck != true
              ? I18n.t("mainpile.selfapprove.confirmcondition")
              : I18n.t("mainpile.selfapprove.comfrimacceptcondition")}
          </Text>
        </View>
        <TouchableOpacity
          onPress={this.onApprove.bind(this)}
          disabled={!this.state.accept}
          style={[
            styles.button,
            this.state.accept
              ? {}
              : {
                  backgroundColor: MENU_GREY_ITEM
                }
          ]}
        >
          <Text style={styles.textButton}>
            {this.state.acceptcheck != true
              ? I18n.t("mainpile.selfapprove.approve")
              : I18n.t("mainpile.selfapprove.accept")}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.button,
            {
              backgroundColor: "#e65004"
            }
          ]}
          onPress={this.onBack.bind(this)}
        >
          <Text style={styles.textButton}>
            {I18n.t("mainpile.selfapprove.cancel")}
          </Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {}
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(
  SelfApprove
)
