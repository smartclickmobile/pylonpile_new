import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator
} from "react-native"
import { Container, Content } from "native-base"
import { Icon } from "react-native-elements"
import Carousel from "react-native-snap-carousel"
import ModalSelector from "react-native-modal-selector"
import { MAIN_COLOR, SUB_COLOR, BASIC_COLOR, MENU_GREY_ITEM, BUTTON_COLOR, BLUE_COLOR } from "../Constants/Color"
import { GroupEmployee } from "../Controller/API"
import I18n from "../../assets/languages/i18n"
import { Extra } from "../Controller/API"
import TableView from "../Components/TableView"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"
import styles from "./styles/Pile8_2.style"

class Pile8_2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 8,
      step: 2,
      ref: 1,
      error: null,
      data: [],
      location: {
        position: {
          lat: 1,
          log: 1
        }
      },
      Edit_Flag: 1,
      dataMaster: {
        ProcesstesterList: []
      },
      tremielist: [],
      index: 0,
      totallength: 0,
      disabled: false,
      Parentcheck: false,
      ondelete:false
    }
  }

  componentDidMount() {
    this.props.getTremieList({
      pileid: this.props.pileid,
      jobid: this.props.jobid
    })

    if (this.props.onRefresh === "refresh") {
      Actions.refresh({
        action: "start"
      })
    }
    
  }
  

  componentWillReceiveProps(nextProps) {
    if (this.props.haslast != nextProps.haslast) {
      this.saveLocal(nextProps.haslast)
    }
    var pile8 = null
    console.log("pile8.Edit_Flag will receice")
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["8_0"]) {
      pile8 = nextProps.pileAll[this.props.pileid]["8_0"].data
    }
    if (
      nextProps.pileAll[this.props.pileid] &&
      nextProps.pileAll[this.props.pileid]['9_1'] &&
      (this.props.category == 5 || this.props.category == 3)
    ) {

      let pileMaster9 = nextProps.pileAll[this.props.pileid]['9_1'].masterInfo;
      if (pileMaster9) {
        if (pileMaster9.PileDetail) {

          this.setState({
            Parentcheck:
            pileMaster9.PileDetail.pile_id == pileMaster9.PileDetail.parent_id
                ? true
                : false,
          });
        }
      }
    }

    if (nextProps.pileAll != undefined && nextProps.pileAll != "") {
      this.setState({ location: nextProps.pileAll.currentLocation })
    }
    
    if (pile8) {
      this.setState({ Edit_Flag: pile8.Edit_Flag })
    }
  }

  onAddData() {
    Actions.tremieinsert({
      jobid: this.props.jobid,
      pileid: this.props.pileid,
      process: this.state.process,
      step: this.state.step,
      Edit_Flag: "1",
      lat: this.state.location == undefined ? "" : this.state.location.position.lat,
      log: this.state.location == undefined ? "" : this.state.location.position.log,
      index: this.props.tremielist.length + 1,
      shape: this.props.shape,
      tremieid: this.props.tremiesizeid,
      category: this.props.category
    })
  }

  onEditData(index, list) {
    Actions.tremieinsert({
      jobid: this.props.jobid,
      pileid: this.props.pileid,
      process: this.state.process,
      step: this.state.step,
      Edit_Flag: "1",
      lat: this.state.location == undefined ? "" : this.state.location.position.lat,
      log: this.state.location == undefined ? "" : this.state.location.position.log,
      TremieList: list,
      edit: true,
      index: index + 1,
      allowlast: this.props.tremielist.length == index + 1,
      shape: this.props.shape,
      category: this.props.category
    })
  }
  onViewData(index, list) {
    Actions.tremieinsert({
      jobid: this.props.jobid,
      pileid: this.props.pileid,
      process: this.state.process,
      step: this.state.step,
      Edit_Flag: "0",
      lat: this.state.location == undefined ? "" : this.state.location.position.lat,
      log: this.state.location == undefined ? "" : this.state.location.position.log,
      TremieList: list,
      edit: true,
      index: index + 1,
      allowlast: this.props.tremielist.length == index + 1,
      shape: this.props.shape,
      category: this.props.category
    })
  }

  deleteRow(index, id) {
      console.warn('deleteRow')
    // use zero because last unit is on the first row
    if (index != 0) {
      console.warn('index != 0')
      Alert.alert(I18n.t('mainpile.8_2.error'), I18n.t('mainpile.8_2.cannotdelete'), [{ text: "OK" }])
    } else {
      Alert.alert(
        "",
        I18n.t('mainpile.8_2.confirmmdelete'),
        [
          {
            text: "Cancel"
          },
          {
            text: "OK",
            onPress: () => {
              console.warn('in ok',index,id)
              if (index == 0) {
                this.callDelete(id)
              } else {
                Alert.alert(I18n.t('mainpile.8_2.error'), I18n.t('mainpile.8_2.cannotdelete'), [{ text: "OK" }])
              }
            }
          }
        ],
        { cancelable: false }
      )
    }
  }

  callDelete(index) {
    valueApi = {
      Language: I18n.currentLocale(),
      JobId: this.props.jobid,
      PileId: this.props.pileid,
      index: index,
      latitude:this.state.location!=undefined ? this.state.location.position.lat:1,
      longitude:this.state.location!=undefined ? this.state.location.position.log:1
    }
    this.props.pileSaveTremieDelete(valueApi)
  }

  
  updateState(value) {}

  saveLocal(last) {
    var value = {
      process: 8,
      step: 2,
      pile_id: this.props.pileid,
      data: {
        Last: last
      }
    }
    console.log("save local tremie 8-2", value)
    this.props.mainPileSetStorage(this.props.pileid, "8_2", value)
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, justifyContent: "center" }}>
          <ActivityIndicator size="large" color="#007CC2" />
        </View>
      )
    }
    return (
      <View style={styles.listContainer}>
        <View style={styles.listTopic}>
          <Text style={styles.listTopicText}>{I18n.t("mainpile.8_2.title")}</Text>
          {this.state.Edit_Flag == 0||(( this.props.category == 3 || this.props.category == 5) &&this.state.Parentcheck == false) ? <View/> :
          <TouchableOpacity activeOpacity={0.8} onPress={() => this.onAddData()} disabled={this.props.haslast || this.state.Edit_Flag == 0}>
            <View style={[styles.listButton, this.props.haslast || this.state.Edit_Flag == 0 && { backgroundColor: MENU_GREY_ITEM }]}>
              <Image source={require("../../assets/image/icon_add.png")} />
              <Text style={styles.listButtonText}>{I18n.t("mainpile.4_1.add_data")}</Text>
            </View>
          </TouchableOpacity>}
        </View>
        <View style={[styles.listTopic, { marginTop: 10 }]}>
          <Text style={[styles.listTopicText, { color: "black" }]}>{I18n.t("mainpile.8_2.length")}</Text>
          <View style={styles.listButtonLength}>
            <Text style={styles.listButtonTextLength}>{this.props.tremievolume.toFixed(3)}</Text>
          </View>
        </View>
        {this.props.tremielist.length > 0 ? (
          <View>{this.renderTable()}</View>
        ) : (
          <View style={styles.listTableDetail}>
            <Text style={styles.listTableDetailText}>
              {I18n.t("mainpile.8_2.no_data")}
              {I18n.locale=='th'?"\n":''}
              {I18n.locale=='th'?<Text style={styles.textHighligh}>{I18n.t("mainpile.8_2.add_data")}</Text>:''}
              {I18n.locale=='th'?I18n.t("mainpile.8_2.add_data_test"):''}

            </Text>
          </View>
        )}
      </View>
    )
  }

  renderTable() {
    const data = [
      {
        key: 0,
        section: true,
        label: I18n.t('mainpile.8_2.selector'),
        action: "head"
      },
      {
        key: 1,
        label: I18n.t('mainpile.8_2.edit'),
        action: "edit"
      },
      {
        key: 2,
        label: I18n.t('mainpile.8_2.delete'),
        action: "delete"
      }
    ]
    const modaldata2 = [
      {
        key: 0,
        section: true,
        label: I18n.t("mainpile.8_2.selector"),
        action: "head"
      },
      {
        key: 1,
        label: I18n.t("mainpile.9_1.see"),
        action: "view"
      }
    ]
    
    return (
      <View style={{ marginTop: 10, width: "100%", borderWidth: 1 }}>
        <View style={{ flexDirection: "row", height: 40, backgroundColor: "#daeef8" }}>
          <View style={{ width: "15%", justifyContent: "center", alignItems: "center", borderRightWidth: 1 }}>
            <Text>#</Text>
          </View>
          <View style={{ width: "25%", justifyContent: "center", alignItems: "center", borderRightWidth: 1 }}>
            <Text style={{ textAlign: "center" }}>{I18n.t('mainpile.8_2.size')}</Text>
          </View>
          <View style={{ width: "30%", justifyContent: "center", alignItems: "center", borderRightWidth: 1 }}>
            <Text style={{ textAlign: "center" }}>{I18n.t('mainpile.8_2.lengthM')}</Text>
          </View>
          <View style={{ width: "30%", justifyContent: "center", alignItems: "center", borderRightWidth: 1 }}>
            <Text>{I18n.t('mainpile.8_2.last')}</Text>
          </View>
        </View>
        <ScrollView>
          {this.props.loading && (
            <View>
              <ActivityIndicator size="large" />
              <Text>{this.props.loading}</Text>
            </View>
          )}
          {!this.props.loading &&
            this.props.tremielist
              .slice(0)
              .reverse()
              .map((item, index) => {
                return (
                  <ModalSelector
                    data={this.state.Edit_Flag == 0||(( this.props.category == 3 || this.props.category == 5) &&this.state.Parentcheck == false) ?  modaldata2:data}
                    cancelText="Cancel"
                    onModalClose={option => {
                      // let count 
                      if (option.action == "edit") {
                        console.warn('item traime',item)
                        this.onEditData(this.props.tremielist.length - index - 1, item)
                      } else if (option.action == "delete") {
                        this.deleteRow(index, this.props.tremielist.length)
                      }else if (option.action == "view") {
                        this.onViewData(this.props.tremielist.length - index - 1, item)
                      }
                    }}
                    key={index}
                    // disabled={this.state.Edit_Flag == 0}
                  >
                    <View style={{ flexDirection: "row", height: 50, borderTopWidth: 1 }} key={index}>
                      <View
                        style={{ width: "15%", justifyContent: "center", alignItems: "center", borderRightWidth: 1 }}
                      >
                        <Text>{this.props.tremielist.length - index}</Text>
                        {index==0&&(this.props.tremielist.length - 1)!==index&&<Text style={{textAlign:'center',color:"#6dcc64"}}>{I18n.t("mainpile.7_3.firstitem")}</Text>}
                        {(this.props.tremielist.length - 1)==index&&<Text style={{textAlign:'center',color:"#6dcc64"}}>{I18n.t("mainpile.7_3.lastitem")}</Text>}
                      </View>
                      <View
                        style={{ width: "25%", justifyContent: "center", alignItems: "center", borderRightWidth: 1 }}
                      >
                        <Text>{item.TremieSize.Name}</Text>
                      </View>
                      <View
                        style={{ width: "30%", justifyContent: "center", alignItems: "center", borderRightWidth: 1 }}
                      >
                        <Text>{item.Length}</Text>
                      </View>
                      <View
                        style={{ width: "30%", justifyContent: "center", alignItems: "center", borderRightWidth: 1 }}
                      >
                        {item.IsLast && <Icon name="check" reverse color="#6dcc64" size={15} />}
                      </View>
                    </View>
                  </ModalSelector>
                )
              })}
        </ScrollView>
      </View>
    )
  }

  _renderTable() {
    return (
      <View style={styles.lisTable}>
        <ScrollView style={{ flex: 1 }} showsHorizontalScrollIndicator={false}>
          <View style={styles.tableCol}>
            <View style={styles.tableHeadGroup}>
              <View style={styles.tableHead}>
                <Text>{I18n.t('mainpile.8_2.order')}</Text>
              </View>
              <View style={styles.tableHead}>
                <Text>{I18n.t('mainpile.8_2.size1')}</Text>
              </View>
              <View style={styles.tableHead}>
                <Text>{I18n.t('mainpile.8_2.length1')}</Text>
              </View>
              <View style={styles.tableHead}>
                <Text>{I18n.t('mainpile.8_2.last')}</Text>
              </View>
              <View style={[styles.tableHead, styles.tableHeadTool]}>
                <Text />
              </View>
            </View>

            {this.state.tremielist.map((item, index) => {
              return (
                <View key={index} style={styles.tableContentGroup}>
                  <View style={[styles.tableContent, styles.tableContentCenter]}>
                    <Text>{index + 1}</Text>
                  </View>
                  <View style={[styles.tableContent, styles.tableContentCenter]}>
                    <Text>{item.size}</Text>
                  </View>
                  <View style={[styles.tableContent, styles.tableContentCenter]}>
                    <Text>{item.length}</Text>
                  </View>
                  <View style={styles.tableContent}>
                    <Text>{item.last}</Text>
                  </View>
                  <View style={[styles.tableContent, styles.tableContentTool]}>
                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.onEditData(index)}>
                      <View style={styles.buttonEdit}>
                        <Image source={require("../../assets/image/icon_edit.png")} />
                        <Text style={styles.buttonEditText}>{I18n.t('mainpile.8_2.edit')}</Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.deleteRow(index)}>
                      <View style={styles.buttonDelete}>
                        <Image source={require("../../assets/image/icon_bin.png")} />
                        <Text style={styles.buttonDeleteText}>{I18n.t("mainpile.4_1.delete")}</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              )
            })}
          </View>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll,
    tremielist: state.tremie.tremielist,
    tremievolume: state.tremie.tremievolume,
    loading: state.tremie.loading,
    tremiesizeid: state.tremie.tremiesizeid,
    haslast: state.tremie.haslast
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(Pile8_2)
