import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native"
import { Container, Content } from "native-base"
import { Icon } from "react-native-elements"
import Carousel from "react-native-snap-carousel"
import ModalSelector from "react-native-modal-selector"
import { MAIN_COLOR, SUB_COLOR, BASIC_COLOR, MENU_GREY_ITEM } from "../Constants/Color"
import { GroupEmployee } from "../Controller/API"
import I18n from "../../assets/languages/i18n"
import { Extra } from "../Controller/API"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"
import styles from './styles/Pile6_1.style'
import moment from "moment"
import DateTimePicker from '@react-native-community/datetimepicker';
const sliderWidth = Dimensions.get("window").width
const itemWidth = 320

class Pile6_1 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 6,
      step: 1,
      Edit_Flag:1,
      driver:'',
      machine:'',
      machineDataSelect:'',
      BucketSize:'',
      BucketSizeDataSelect:'',
      MachineCraneId:'',
      DriverId:'',
      BucketSizeId:'',
      machineData:[{ key: 0, section: true, label: I18n.t("mainpile.2_1.machineselect") }],
      driverData:[{ key: 0, section: true, label: I18n.t("mainpile.2_1.driverselect")  }],
      BucketSizeData:[{ key: 0, section: true, label: 'เลือกขนาดบัคเก็ตเก็บตะกอน'  }],
      loading:true,
      dataMaster:null,
      count:0,
      CrossFlag:false,
      dateBentronite:'',
      timestart:'',
      timeend:'',
      datevisibletime:false,
      datevisibledate:false,
      type:'',
      ChangeDrillingFluidStartDate:'',
      ChangeDrillingFluidEndDate:'',
      ChangeDrillingFluidBeforeDepth:'',
      ChangeDrillingFluidAfterDepth:'',
      status8:0,
      status10:0,
      before:'',
      after:'',
      status6:0,
      CrossFlagold:false,
      pile6_2:null,
      DepthLast:'',
      Depth:''
    }
  }

  componentDidUpdate(prevProps, prevState){
    if (this.state.BucketSizeData != prevState.BucketSizeData) {
      this.setState({count:this.state.count + 1})
    }
    if (this.state.machineData != prevState.machineData) {
      this.setState({count:this.state.count + 1})
    }
    if (this.state.driverData != prevState.driverData) {
      this.setState({count:this.state.count + 1})
      if (this.state.count == 2) {
        this.setState({loading:false})
        // console.log(this.state.DriverId);
        // this.saveLocal('6_1')
      }else {
        if (this.state.count && this.props.hidden == false) {
          setTimeout(() => {this.setState({loading: false})}, 100)
        }
      }
    }
  }
  componentDidMount(){
    if (this.props.onRefresh == 'refresh' && this.state.loading == true) {
      Actions.refresh({
          action: 'start',
      })
    }
  }

  async componentWillReceiveProps(nextProps) {
    // console.warn('AfterDepth6_1 ',nextProps.pileAll[this.props.pileid]["6_1"].data.ChangeDrillingFluidAfterDepth)
    var pile = null
    var pile6_0 = null
    var pileMaster = null
    var pile5_3 = null
    // if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["5_3"]) {
    //   pile5_3 = nextProps.pileAll[this.props.pileid]['5_3'].data
    //   if (pile5_3.DrillingList[pile5_3.DrillingList.length - 1] != undefined) {
    //     await this.setState({DepthLast:pile5_3.DrillingList[pile5_3.DrillingList.length - 1].Depth})
    //   }
    // }
    // if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['3_3']) {
    //   pile3_3 = nextProps.pileAll[this.props.pileid]['3_3'].data
    //   if (pile3_3) {
    //     if (pile3_3.top) {
    //       let cal = pile3_3.top - pileMaster5_2.PileInfo.PileTip
    //       await this.setState({Depth: cal.toString()})
    //     }
    //   }
    // }
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["6_1"]) {
       pile = nextProps.pileAll[this.props.pileid]["6_1"].data
       pileMaster = nextProps.pileAll[this.props.pileid]['6_1'].masterInfo
       this.setState({dataMaster:pileMaster})
      //  console.warn('pile',nextProps.pileAll[this.props.pileid]["6_1"].data)
    }
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["6_2"]) {
      
      this.setState({pile6_2:nextProps.pileAll[this.props.pileid]["6_2"]})
   }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['6_0']) {
      pile6_0 = nextProps.pileAll[this.props.pileid]['6_0'].data
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['8_0']){
      this.setState({status8:nextProps.pileAll[this.props.pileid].step_status.Step8Status,status10:nextProps.pileAll[this.props.pileid].step_status.Step10Status,status6:nextProps.pileAll[this.props.pileid].step_status.Step6Status})
    }
    
    if (pile != null) {
      // console.warn('test',pile.ChangeDrillingFluidBeforeDepth,this.props.pileinfo.ChangeDrillingFluidBeforeDepth,nextProps.pileAll[this.props.pileid]["6_2"].data.BeforeDepth)
      if(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidStartDate != '' && nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidStartDate != null && nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidStartDate != undefined){
        this.setState({
          ChangeDrillingFluidStartDate:moment(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidStartDate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY HH:mm"),
          dateBentronite: moment(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidStartDate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY"),
          timestart:moment(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidStartDate,"DD/MM/YYYY HH:mm:ss").format("HH:mm")
        })
      }else{
        if(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidStartDate == null){
          this.setState({
            ChangeDrillingFluidStartDate:'',
            dateBentronite: '',
            timestart:''
          })
        }
      }
      if(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidEndDate != '' && nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidEndDate != null && nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidEndDate != undefined){
        this.setState({
          ChangeDrillingFluidEndDate:moment(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidEndDate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY HH:mm"),
          dateBentronite: moment(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidEndDate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY"),
          timeend:moment(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidEndDate,"DD/MM/YYYY HH:mm:ss").format("HH:mm")
        })
      }else{
        if(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidEndDate == null){
          this.setState({
            ChangeDrillingFluidEndDate:'',
            dateBentronite: '',
            timeend:''
          })
        }
      }
     
      if(pile.ChangeDrillingFluidBeforeDepth != null && pile.ChangeDrillingFluidBeforeDepth != '' && pile.ChangeDrillingFluidBeforeDepth != undefined){
        this.setState({ChangeDrillingFluidBeforeDepth:parseFloat(pile.ChangeDrillingFluidBeforeDepth).toFixed(3)})
      }else{
        if(pile.ChangeDrillingFluidBeforeDepth == null){
          this.setState({ChangeDrillingFluidBeforeDepth:''})
        }
      }
      if(pile.ChangeDrillingFluidAfterDepth != null && pile.ChangeDrillingFluidAfterDepth != '' && pile.ChangeDrillingFluidAfterDepth != undefined){
        this.setState({ChangeDrillingFluidAfterDepth:parseFloat(pile.ChangeDrillingFluidAfterDepth).toFixed(3)})
      }else{
        if(pile.ChangeDrillingFluidAfterDepth == null){
          this.setState({ChangeDrillingFluidAfterDepth:''})
        }
      }

      if(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidStartDate != '' && nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidStartDate != null && nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidStartDate != undefined){
        this.setState({
          ChangeDrillingFluidStartDate:moment(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidStartDate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY HH:mm"),
          dateBentronite: moment(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidStartDate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY"),
          timestart:moment(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidStartDate,"DD/MM/YYYY HH:mm:ss").format("HH:mm")
        })
      }
      if(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidEndDate != '' && nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidEndDate != null && nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidEndDate != undefined){
        this.setState({
          ChangeDrillingFluidEndDate:moment(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidEndDate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY HH:mm"),
          dateBentronite: moment(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidEndDate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY"),
          timeend:moment(nextProps.pileAll[this.props.pileid]["6_1"].ChangeDrillingFluidEndDate,"DD/MM/YYYY HH:mm:ss").format("HH:mm")
        })
      }
      if ( pileMaster.BucketSize != null && this.state.BucketSizeData != pileMaster.BucketSize  && pileMaster.BucketSize != ''   && this.state.BucketSize =='' ) {
        let list = [{ key: 0, section: true, label: I18n.t('mainpile.6_1.bucketsizeselect') }]
        for (var i = 0; i <  pileMaster.BucketSize.length; i++) {
            if (pileMaster.BucketSize[i].IsDefault == true) {
              this.setState({
                BucketSize: pileMaster.BucketSize[i].Name,
                BucketSizeId: pileMaster.BucketSize[i].Id,
                BucketSizeDataSelect: pileMaster.BucketSize[i],
              })
            }
            if (pile.BucketSize != null) {
              if (pile.BucketSize.Id == pileMaster.BucketSize[i].Id) {
                this.setState({
                  BucketSize: pileMaster.BucketSize[i].Name,
                  BucketSizeId: pileMaster.BucketSize[i].Id,
                  BucketSizeDataSelect: pileMaster.BucketSize[i],
                })
              }
            }
          await list.push({
            key: pileMaster.BucketSize[i].Id,
            label: pileMaster.BucketSize[i].Name,
            IsDefault: pileMaster.BucketSize[i].IsDefault,
            index:i,
          })
        }
        await this.setState({BucketSizeData:list})
      }
      // console.warn('machine',this.state.machineDataSelect,this.state.machine)
      if ( pileMaster.MachineList != null && this.state.machineData != pileMaster.MachineList && pileMaster.MachineList != ''   && this.state.machine =='' ) {
        // console.warn('ff')
        let list = [{ key: 0, section: true, label: I18n.t("mainpile.2_1.machineselect") }]
        for (var i = 0; i < pileMaster.MachineList.length; i++) {
          // console.warn('gg',pile.Machine)
          if (pile.Machine != null) {
            if (pile.Machine.itemid == pileMaster.MachineList[i].itemid) {
              this.setState({
                machine: pileMaster.MachineList[i].machine_no,
                MachineCraneId: pileMaster.MachineList[i].itemid,
                machineDataSelect: pileMaster.MachineList[i],
              })
            }
          }else{
            // console.warn('ff2')
            this.setState({
              machine: '',
              MachineCraneId: '',
              machineDataSelect: '',
            })
          }
          await list.push({
            key: pileMaster.MachineList[i].itemid,
            label: pileMaster.MachineList[i].machine_no,
            typeid: pileMaster.MachineList[i].itemtype_id,
            index:i,
          })
        }
        await this.setState({machineData:list})
      }
      if (pileMaster.DriverList != null && this.state.driverData != pileMaster.DriverList && pileMaster.DriverList != ''  && this.state.driver =='' ) {
        let list = [{ key: 0, section: true, label: I18n.t("mainpile.2_1.driverselect")  }]
        for (var i = 0; i < pileMaster.DriverList.length; i++) {
          if (pile.DriverId != null) {
            if (pile.DriverId == pileMaster.DriverList[i].employee_id) {
               this.setState({
                driver: (pileMaster.DriverList[i].nickname && pileMaster.DriverList[i].nickname + "-") + pileMaster.DriverList[i].firstname + " " + pileMaster.DriverList[i].lastname,
                DriverId: pileMaster.DriverList[i].employee_id
              })
            }
          }
           await list.push({
            key: pileMaster.DriverList[i].employee_id,
            label: (pileMaster.DriverList[i].nickname && pileMaster.DriverList[i].nickname + "-") + pileMaster.DriverList[i].firstname + " " + pileMaster.DriverList[i].lastname
          })
        }
        await this.setState({driverData:list})
      }

    }
    // if (nextProps.count == 2) {
    //   this.saveLocal('2_1')
    //   this.props.pile2Count()
    //   await this.setState({loading:false})
    // }
    // console.warn('pile',pile)
    if (pile) {
      if (pile.Machine  == null) {
        this.setState({machine:'',machineDataSelect:''})
      }
      // else{
      //   this.setState({machine:pile.Machine.itemid})
      // }
      if (pile.DriverId == null) {
        this.setState({driver:null,DriverId:null})
      }
      // else{
      //   this.setState({driver:pile.DriverId})
      // }
      if (pile.BucketSize  == null) {
        this.setState({BucketSize:null,BucketSizeDataSelect:null})
      }
      // else{
      //   this.setState({BucketSize:pile.BucketSize.Id})
      // }
      // console.warn('CrossFlag',pile.CrossFlag)
      if (pile.CrossFlag == null) {
        this.setState({
          CrossFlag:false,
         
        })
      }
      else{
          this.setState({CrossFlag:pile.CrossFlag,CrossFlagold:pile.CrossFlag})
      }
    }

    if (pile6_0) {
      // if (pile3_0.Edit_Flag != null && pile3_0.Edit_Flag != undefined && pile3_0.Edit_Flag != '') {
        this.setState({Edit_Flag:pile6_0.Edit_Flag})
      // }
    }
    if(this.state.status6 == 2){
      if(this.state.CrossFlagold==false && this.state.CrossFlag == true){
        if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["6_2"]) {
          this.setState({
            // ChangeDrillingFluidBeforeDepth:nextProps.pileAll[this.props.pileid]["6_2"].data.BeforeDepth,
            // ChangeDrillingFluidAfterDepth:nextProps.pileAll[this.props.pileid]["6_2"].data.AfterDepth,
            ChangeDrillingFluidBeforeDepth:'',
            ChangeDrillingFluidAfterDepth:'',
            ChangeDrillingFluidStartDate:'',
            ChangeDrillingFluidEndDate:'',
            dateBentronite:'',
            timestart:'',
            timeend:'',
            driver:'',
            machine:'',
            machineDataSelect:'',
            BucketSize:'',
            BucketSizeDataSelect:'',
            MachineCraneId:'',
            DriverId:'',
            BucketSizeId:'',
          })
       }
      }
    }
  }
  async mixdate(){
    if( this.state.dateBentronite != '' && this.state.timestart != ''){
     await this.setState({ChangeDrillingFluidStartDate:this.state.dateBentronite+' '+this.state.timestart})
    }
    if(this.state.dateBentronite != '' && this.state.timeend != ''){
      await this.setState({ChangeDrillingFluidEndDate:this.state.dateBentronite+' '+this.state.timeend})
    }
  }
  async saveLocal(process_step,data){
    await this.mixdate()
    // console.warn('saveLocal1')
    var temp = false
    if(this.state.CrossFlag != this.CrossFlagold){
      temp = true
    }
    var value = {
      process:6,
      step:1,
      pile_id: this.props.pileid,
      data:{
          BucketSize:this.state.BucketSizeDataSelect,
          BucketSizeName: this.state.BucketSize,
          Machine:this.state.machineDataSelect,
          MachineName:this.state.machine,
          DriverId:this.state.DriverId,
          DriverName:this.state.driver,
          CrossFlag:this.state.CrossFlag,
          ChangeDrillingFluidBeforeDepth:this.state.ChangeDrillingFluidBeforeDepth,
          ChangeDrillingFluidAfterDepth:this.state.ChangeDrillingFluidAfterDepth,
          haveedit:temp
      },
      ChangeDrillingFluidStartDate:this.state.ChangeDrillingFluidStartDate,
      ChangeDrillingFluidEndDate:this.state.ChangeDrillingFluidEndDate
    }
    // console.warn('saveLocal',this.state.machineDataSelect,this.state.machine)
    await this.props.mainPileSetStorage(this.props.pileid,process_step,value)
  }

  selectedMachine =  async machine => {
    await this.setState({ machine: machine.label,MachineCraneId:machine.key,machineDataSelect:this.state.dataMaster.MachineList[machine.index]})
     this.saveLocal('6_1')
  }

  selectedDriver = async driver => {
    await this.setState({ driver: driver.label,DriverId: driver.key })
    this.saveLocal('6_1')
  }

  selectedBucketSize = async BucketSize => {
    await this.setState({ BucketSize: BucketSize.label,BucketSizeId: BucketSize.key,BucketSizeDataSelect:this.state.dataMaster.BucketSize[BucketSize.index]})
    this.saveLocal('6_1')
  }
  selecteBucketChanged(){
    
    if (this.state.Edit_Flag == 1) {
      Alert.alert(
        '',
        this.state.CrossFlag == false ? I18n.t('mainpile.6_1.qnotcrossflag') : I18n.t('mainpile.6_1.qyescrossflag'),
        [
          {text: 'Cancel'},
          {text: 'OK', onPress: async () => {
            await this.setState({ CrossFlag: !this.state.CrossFlag },()=>{
              // console.warn('CrossFlag',this.state.CrossFlag)
              if(this.state.CrossFlag){
                this.setState({ 
                  machine: '',MachineCraneId:'',machineDataSelect:'',
                  driver: '',DriverId: '',
                  BucketSize: '',BucketSizeId: '',BucketSizeDataSelect:''
                })
              }
              this.saveLocal('6_1')
            })
           
          }},
        ],
        { cancelable: false }
      )
    }
  }
  onSelectDate(date){
    let date2 = moment(date).format("DD/MM/YYYY")
    this.setState({ dateBentronite: date2,datevisibledate: false},()=>{
      this.saveLocal('6_1')
    })
  }
  onSelectTime(date){
    
    let date2 = moment(date).format("HH:mm")
    if(this.state.type=='start'){
      this.setState({ timestart: date2,datevisibletime: false},()=>{
        this.saveLocal('6_1')

      })
    }else if(this.state.type=='end'){
      this.setState({ timeend: date2,datevisibletime: false},()=>{
        this.saveLocal('6_1')

      })
    }
  }
  getDate(){
    if(this.state.type == 'start'){
      if(this.state.ChangeDrillingFluidStartDate != ''){
        return new Date(moment(this.state.ChangeDrillingFluidStartDate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        return new Date()
      }
    }else if(this.state.type == 'end'){
      if(this.state.ChangeDrillingFluidEndDate != ''){
        return new Date(moment(this.state.ChangeDrillingFluidEndDate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        return new Date()
      }
    }else{
      if(this.state.ChangeDrillingFluidStartDate != ''){
        return new Date(moment(this.state.ChangeDrillingFluidStartDate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        return new Date()
      }
    }
  }
  checkalert(){
    if(this.state.Edit_Flag == 1){
      Alert.alert('', I18n.t('mainpile.6_0.error'), [
        {
          text: 'OK'
        }
      ])
    }
   
  }
  onChangedate = (event, selectedDate) => {
    if(event.type == 'set'){
      this.onSelectDate(selectedDate)
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };
  onChangetime = (event, selectedDate) => {
    if(event.type == 'set'){
      this.onSelectTime(selectedDate)
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };
  render() {
    var temp=0
    if(this.state.CrossFlag && this.state.status10==0){
      temp=1
    }else if(this.state.CrossFlag && this.state.status10!=0){
      temp=2
    }
    if (this.props.hidden) {
      return null
    }
    if (this.state.loading) {
      return (
        <View style={{ flex: 1,justifyContent: 'center' }}>
          <ActivityIndicator size="large" color="#007CC2" />
        </View>
      )
    }
    return (
      
      <View style={styles.listContainer}>
        <View style={styles.listTopic}>
          <Text style={styles.listTopicText}>{I18n.t('mainpile.6_1.crosstitle')}</Text>
        </View>
        <View
          style={styles.icon}
        >
          {this.state.CrossFlag == true? (
            <Icon
              name="check"
              reverse
              color="#6dcc64"
              size={15}
              onPress={() => this.state.status8 != 0&&this.state.status10!=0 ?
                this.checkalert()
                : 
                this.selecteBucketChanged()}
            />
          ) : (
            <Icon
              name="check"
              reverse
              color="grey"
              size={15}
              onPress={() => this.state.status8 != 0&&this.state.status10!=0?
                this.checkalert()
                : this.selecteBucketChanged()}
            />
          )}
          <Text>{I18n.t('mainpile.6_1.Ncrossflag')}</Text>
        </View>
        {/*เก็บตะกอน*/}
        {!this.state.CrossFlag?<View style={styles.listTopicSub}>
            <Text style={styles.listTopicSubText}>{I18n.t('mainpile.6_1.bucketsize')}</Text>
            <View style={styles.listSelectedView}>
              <ModalSelector
                  data={this.state.BucketSizeData}
                  onChange={this.selectedBucketSize}
                  selectTextStyle={styles.textButton}
                  disabled={this.state.Edit_Flag == 0 || this.state.CrossFlag }
                  cancelText="Cancel">
                  <TouchableOpacity style={this.state.Edit_Flag == 0 || this.state.CrossFlag ?   [styles.button ,{ borderColor: MENU_GREY_ITEM }]:styles.button}>
                    <View style={styles.buttonTextStyle}>
                      <Text style={this.state.Edit_Flag == 0 || this.state.CrossFlag ? [styles.buttonText ,{ color: MENU_GREY_ITEM }]:styles.buttonText}>{this.state.BucketSize || I18n.t('mainpile.6_1.bucketsizeselect')}</Text>
                      <View style={styles.buttonSelectorIcon}>
                        <Icon name='arrow-drop-down' type='MaterialIcons' color={this.state.Edit_Flag == 0 || this.state.CrossFlag ? MENU_GREY_ITEM : '#007CC2' }></Icon>
                      </View>
                    </View>
                  </TouchableOpacity>
              </ModalSelector>
            </View>
        </View>:<View/>}
        {!this.state.CrossFlag?<View style={styles.listTopicSub}>
            <Text style={styles.listTopicSubText}>{I18n.t("mainpile.6_1.machine")}</Text>
            <View style={styles.listSelectedView}>
              <ModalSelector
                  data={this.state.machineData}
                  onChange={this.selectedMachine}
                  selectTextStyle={styles.textButton}
                  disabled={this.state.Edit_Flag == 0 || this.state.CrossFlag }
                  cancelText="Cancel">
                  <TouchableOpacity style={this.state.Edit_Flag == 0 || this.state.CrossFlag ? [styles.button ,{ borderColor: MENU_GREY_ITEM }]:styles.button}>
                    <View style={styles.buttonTextStyle}>
                      <Text style={this.state.Edit_Flag == 0 || this.state.CrossFlag ? [styles.buttonText ,{ color: MENU_GREY_ITEM }]:styles.buttonText}>{this.state.machine || I18n.t("mainpile.6_1.machineselect")}</Text>
                      <View style={styles.buttonSelectorIcon}>
                        <Icon name='arrow-drop-down' type='MaterialIcons' color={this.state.Edit_Flag == 0 || this.state.CrossFlag ? MENU_GREY_ITEM : '#007CC2' }></Icon>
                      </View>
                    </View>
                  </TouchableOpacity>
              </ModalSelector>
            </View>
        </View>:<View/>}
        {!this.state.CrossFlag?<View style={styles.listTopicSub}>
            <Text style={styles.listTopicSubText}>{I18n.t("mainpile.6_1.driver")}</Text>
            <View style={styles.listSelectedView}>
              <ModalSelector
                  data={this.state.driverData}
                  onChange={this.selectedDriver}
                  selectTextStyle={styles.textButton}
                  disabled={this.state.Edit_Flag == 0 || this.state.CrossFlag }
                  cancelText="Cancel">
                  <TouchableOpacity style={this.state.Edit_Flag == 0 || this.state.CrossFlag ? [styles.button ,{ borderColor: MENU_GREY_ITEM }]:styles.button}>
                    <View style={styles.buttonTextStyle}>
                      <Text style={this.state.Edit_Flag == 0 || this.state.CrossFlag ? [styles.buttonText ,{ color: MENU_GREY_ITEM }]:styles.buttonText}>{this.state.driver || I18n.t("mainpile.6_1.driverselect")}</Text>
                      <View style={styles.buttonSelectorIcon}>
                        <Icon name='arrow-drop-down' type='MaterialIcons' color={this.state.Edit_Flag == 0 || this.state.CrossFlag ? MENU_GREY_ITEM : '#007CC2' }></Icon>
                      </View>
                    </View>
                  </TouchableOpacity>
              </ModalSelector>
            </View>
        </View>:<View/>}
        {/*ไม่เก็บตะกอน*/}
        {this.state.datevisibledate &&<DateTimePicker
          value={this.getDate()}
          isVisible={this.state.datevisibledate}
          is24Hour={true}
          display='spinner'
          mode='date'
          onChange={this.onChangedate}
          />}
        {this.state.datevisibletime && <DateTimePicker
          value={this.getDate()}
          isVisible={this.state.datevisibletime}
          is24Hour={true}
          display='spinner'
          mode='time'
          onChange={this.onChangetime}
        />}
          {this.state.CrossFlag?<View style={{borderColor:'#cccccc',borderWidth:1,padding:10,marginBottom:15,paddingTop:0,marginTop:10}}>
        <View style={styles.listTopicSub}>
        <Text style={[styles.listTopicText,{marginBottom:10}]}>{I18n.t('mainpile.6_1.datetimeCross')}</Text>
        <View style={styles.listSelectedView}>
        <View style={{flexDirection:'row'}}> 
        <Text style={{flex:1,marginTop:7}}>{I18n.t('mainpile.6_1.dateCroos')}</Text>
            
              <TouchableOpacity style={[styles.button,{flex:1.5,height:40}]} disabled={this.state.Edit_Flag == 0 } onPress={()=> this.setState({datevisibledate:true})}>
                <View style={[styles.buttonTextStyle]}>
                  <Text style={styles.buttonText }>{this.state.dateBentronite}</Text>
                </View>
              </TouchableOpacity>
            </View>
            </View>
        </View>
        <View style={[styles.listTopicSub]}>
            <View style={[styles.listSelectedView]}>
            <View style={{flexDirection:'row',marginBottom:15}}>
              <Text style={{flex:1,marginTop:7}}>{I18n.t('mainpile.6_1.starttime')}</Text>
              <TouchableOpacity style={[styles.button,{flex:1.5,height:40}]} disabled={this.state.Edit_Flag == 0 } onPress={()=> this.setState({datevisibletime:true,type:'start'})}>
                <View style={[styles.buttonTextStyle]}>
                  <Text style={styles.buttonText }>{this.state.timestart}</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{flexDirection:'row'}}>
              <Text style={{flex:1,marginTop:7}}>{I18n.t('mainpile.6_1.endtime')}</Text>
              <TouchableOpacity style={[styles.button,{flex:1.5,height:40}]} disabled={this.state.Edit_Flag == 0 } onPress={()=> this.setState({datevisibletime:true,type:'end'})}>
                <View style={[styles.buttonTextStyle]}>
                  <Text style={styles.buttonText }>{this.state.timeend}</Text>
                </View>
              </TouchableOpacity>
              </View>

            </View>
        </View>
        </View>:<View/>}
        {temp==1?<View style={{borderColor:'#cccccc',borderWidth:1,padding:10,paddingTop:0}}>
        <View style={styles.listTopicSub}>
        <Text style={[styles.listTopicText,{marginBottom:10}]}>{this.props.category == 3||this.props.category == 5?I18n.t('mainpile.6_1.depthdw'):I18n.t('mainpile.6_1.depth')}</Text>
        <Text style={styles.listTopicSubText}>{I18n.t('mainpile.6_1.depthbefore')}</Text>
          <View style={[styles.button,{padding:0}]}>
                <TextInput
                  editable={this.state.Edit_Flag == 1 ?true:false}
                  keyboardType="numeric"
                  onChangeText={(top) => this.setState({ChangeDrillingFluidBeforeDepth:top})}
                  underlineColorAndroid="transparent"
                  value={this.state.ChangeDrillingFluidBeforeDepth}
                  style={[styles.buttonText,{textAlign:'center'}]}
                  placeholder={I18n.t('mainpile.6_1.depthbefore')}
                  onEndEditing={()=> {
                    this.setState({ChangeDrillingFluidBeforeDepth:this.state.ChangeDrillingFluidBeforeDepth ? parseFloat(this.state.ChangeDrillingFluidBeforeDepth).toFixed(3) : ''})
                    this.saveLocal('6_1')
                  }}
                />
                </View>
    </View>
        <View style={styles.listTopicSub}>
            <Text style={styles.listTopicSubText}>{I18n.t('mainpile.6_1.depthafter')}</Text>
                
                <View style={[styles.button,{padding:0}]}>
                <TextInput
                  editable={this.state.Edit_Flag == 1 && this.state.status10==0?true:false}
                  keyboardType="numeric"
                  onChangeText={(top) => this.setState({ChangeDrillingFluidAfterDepth:top})}
                  underlineColorAndroid="transparent"
                  value={this.state.ChangeDrillingFluidAfterDepth}
                  style={[styles.buttonText,{textAlign:'center'}]}
                  placeholder={I18n.t('mainpile.6_1.depthafter')}
                  onEndEditing={()=> {
                    this.setState({ChangeDrillingFluidAfterDepth:this.state.ChangeDrillingFluidAfterDepth ? parseFloat(this.state.ChangeDrillingFluidAfterDepth).toFixed(3) : ''})
                    this.saveLocal('6_1')
                  }}
                />
                </View>
            
        </View>
        </View>:<View/>}
        {temp==2?<View style={{borderColor:'#cccccc',borderWidth:1,padding:10,paddingTop:0}}>
        <View style={styles.listTopicSub}>
        <Text style={[styles.listTopicText,{marginBottom:10}]}>{this.props.category == 3||this.props.category == 5?I18n.t('mainpile.6_1.depthdw'):I18n.t('mainpile.6_1.depth')}</Text>
        <Text style={styles.listTopicSubText}>{I18n.t('mainpile.6_1.depthbefore')}</Text>
          <View style={[styles.button,{padding:0}]}>
                <TextInput
                  editable={this.state.Edit_Flag == 1 ?true:false}
                  keyboardType="numeric"
                  onChangeText={(top) => this.setState({ChangeDrillingFluidBeforeDepth:top})}
                  underlineColorAndroid="transparent"
                  value={this.state.ChangeDrillingFluidBeforeDepth}
                  style={[styles.buttonText,{textAlign:'center'}]}
                  placeholder={I18n.t('mainpile.6_1.depthbefore')}
                  onEndEditing={()=> {
                    this.setState({ChangeDrillingFluidBeforeDepth:this.state.ChangeDrillingFluidBeforeDepth ? parseFloat(this.state.ChangeDrillingFluidBeforeDepth).toFixed(3) : ''})
                    this.saveLocal('6_1')
                  }}
                />
                </View>
    </View>
        <View style={styles.listTopicSub}>
            <Text style={styles.listTopicSubText}>{I18n.t('mainpile.6_1.depthafter')}</Text>
                
                <TouchableOpacity disabled={this.state.Edit_Flag == 0} onPress={()=>{
                  // if(this.state.Edit_Flag == 1){
                    Alert.alert('', I18n.t('mainpile.6_0.error'), [
                      {
                        text: 'OK'
                      }
                    ])
                  // }
                }} style={[styles.button,{padding:0}]}>
                <TextInput
                  editable={this.state.Edit_Flag == 1 && this.state.status10==0?true:false}
                  keyboardType="numeric"
                  onChangeText={(top) => this.setState({ChangeDrillingFluidAfterDepth:top})}
                  underlineColorAndroid="transparent"
                  value={this.state.ChangeDrillingFluidAfterDepth}
                  style={[styles.buttonText,{textAlign:'center'}]}
                  placeholder={I18n.t('mainpile.6_1.depthafter')}
                  onEndEditing={()=> {
                    this.setState({ChangeDrillingFluidAfterDepth:this.state.ChangeDrillingFluidAfterDepth ? parseFloat(this.state.ChangeDrillingFluidAfterDepth).toFixed(3) : ''})
                    this.saveLocal('6_1')
                  }}
                />
                </TouchableOpacity>
            
        </View>
        </View>:<View/>}
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll || null ,count:state.pile.count,
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(Pile6_1)
