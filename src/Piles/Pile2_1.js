import React, { Component } from "react"
import { View, Text, StyleSheet, TouchableOpacity, TextInput, ActivityIndicator, } from "react-native"
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM } from "../Constants/Color"
import ModalSelector from "react-native-modal-selector"
import { Icon } from "react-native-elements"
import I18n from "../../assets/languages/i18n"
import { Extra } from "../Controller/API"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"
import styles from "./styles/Pile2_1.style"
import Spinner from "react-native-loading-spinner-overlay"

class Pile2_1 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      machine: "",
      MachineCraneId: "",
      vibro: "",
      MachineVibroId: "",
      driver: "",
      DriverId: "",
      machineData: [{ key: 0, section: true, label: I18n.t("mainpile.2_1.machineselect") }],
      vibroData: [{ key: 0, section: true, label: I18n.t("mainpile.2_1.vibroselect") }],
      driverData: [{ key: 0, section: true, label: I18n.t("mainpile.2_1.driverselect") }],
      vibroDisable: true,
      casinglength: "15",
      data: [],
      process: 2,
      step: 1,
      casing: parseFloat(this.props.pile.size).toFixed(2),
      loading: true,
      Edit_Flag: 1,
      count: 0,
      check:true
    }
    this.prevState = this.state
  }

  componentWillMount() {

  }
  async componentDidMount() {
    if(this.state.machine == '') {
      this.setState({ 
        vibroDisable: true ,
        // vibroData: [{ key: 0, section: true, label: I18n.t("mainpile.2_1.vibroselect") }],
        vibro: ""
      })
    }
    if(this.props.onRefresh == 'refresh' && this.state.loading == true) {
      Actions.refresh({
        action: 'start',
      })
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.state.machineData != prevState.machineData) {
      this.setState({ count: this.state.count + 1 })
    }
    if(this.state.vibroData != prevState.vibroData) {
      this.setState({ count: this.state.count + 1 })
    }
    if(this.state.driverData != prevState.driverData) {
      this.setState({ count: this.state.count + 1 })
    }
    if(this.state.count != prevState.count && this.state.count == 2) {
      this.setState({ loading: false })
    } else {
      if(this.state.loading == true && this.props.hidden == false) {
        setTimeout(() => { this.setState({ loading: false }) }, 1000)
      }
    }
  }

  async componentWillReceiveProps(nextProps) {
    var pile = null
    var pile2_0 = null
    var pileMaster = null
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["2_1"]) {
      pile = nextProps.pileAll[this.props.pileid]["2_1"].data
      pileMaster = nextProps.pileAll[this.props.pileid]['2_1'].masterInfo
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['2_0']) {
      pile2_0 = nextProps.pileAll[this.props.pileid]['2_0'].data
    }
    //       machineData.push({ key: index + 1, label: machine.machine_no, no: machine.machine_no, typeid: machine.itemtype_id })
    if(pile != null) {
      if(this.state.machineData != pileMaster.CraneList && pileMaster.CraneList != '' && this.state.machineData.length == 1 && this.state.machine == '') {
        let list = [{ key: 0, section: true, label: I18n.t("mainpile.2_1.machineselect") }]
        for(var i = 0; i < pileMaster.CraneList.length; i++) {
          if(pile) {
            if(pileMaster.CraneList[i].itemid == pile.MachineCraneId) {
              this.setState({
                machine: pileMaster.CraneList[i].machine_no,
                vibroDisable: pileMaster.CraneList[i].vibro_flag == true ? false : true,
                MachineCraneId: pile.MachineCraneId
              })
            }
          }
          await list.push({
            key: pileMaster.CraneList[i].itemid,
            label: pileMaster.CraneList[i].machine_no,
            typeid: pileMaster.CraneList[i].itemtype_id,
            vibro_flag: pileMaster.CraneList[i].vibro_flag
          })
        }
        await this.setState({ machineData: list })
        // console.log(this.state.machineData);
      }
      if(this.state.vibroData != pileMaster.VibroList && pileMaster.VibroList != '' && this.state.vibroData.length == 1 && this.state.vibro == '') {
        let list = [{ key: 0, section: true, label: I18n.t("mainpile.2_1.vibroselect") }]
        for(var i = 0; i < pileMaster.VibroList.length; i++) {
          if(pile) {
            if(pileMaster.VibroList[i].itemid == pile.MachineVibroId) {
              // console.warn('pileMaster',pileMaster.VibroList[i].machine_no,this.state.vibroDisable)
              this.setState({ vibro: pileMaster.VibroList[i].machine_no, MachineVibroId: pileMaster.VibroList[i].itemid })
            }
          }
          await list.push({
            key: pileMaster.VibroList[i].itemid,
            label: pileMaster.VibroList[i].machine_no
          })
        }
        await this.setState({ vibroData: list })
      }
      //       driverData.push({ key: index + 1, label: driver.firstname + " " + driver.lastname, id: driver.employee_id })
      if(this.state.driverData != pileMaster.DriverList && pileMaster.DriverList != '' && this.state.driverData.length == 1 && this.state.driver == '') {
        let list = [{ key: 0, section: true, label: I18n.t("mainpile.2_1.driverselect") }]
        for(var i = 0; i < pileMaster.DriverList.length; i++) {
          if(pile) {
            if(pileMaster.DriverList[i].employee_id == pile.DriverId) {
              this.setState({
                driver: (pileMaster.DriverList[i].nickname && pileMaster.DriverList[i].nickname + "-") + pileMaster.DriverList[i].firstname + " " + pileMaster.DriverList[i].lastname,
                DriverId: pile.DriverId
              })
            }
          }
          await list.push({
            key: pileMaster.DriverList[i].employee_id,
            label: (pileMaster.DriverList[i].nickname && pileMaster.DriverList[i].nickname + "-") + pileMaster.DriverList[i].firstname + " " + pileMaster.DriverList[i].lastname
          })
        }
        await this.setState({ driverData: list })
      }
    }
    if(nextProps.count == 2 && this.state.loading == true) {
      this.saveLocal('2_1')
      this.props.pile2Count()
      await this.setState({ loading: false })
    }
    if (nextProps.pileAll[this.props.pileid].step_status.Step2Status == 0) {
      this.setState({machine:'',vibro:'',driver:'',casinglength:'15'})
    }
    if(pile) {
      if(pile.machine != null && pile.machine != '' && pile.machine != undefined) {
        this.setState({ machine: pile.machine })
      }else if(pile.MachineCraneId == null) {
          this.setState({
            machine:'',
            vibroDisable:true,
            // vibroData: [{ key: 0, section: true, label: I18n.t("mainpile.2_1.vibroselect") }],
            vibro: ""
          })
      }
      if(pile.vibro != null && pile.vibro != '' && pile.vibro != undefined) {
        this.setState({ vibro: pile.vibro })
      }else if(pile.MachineVibroId == null) {
          this.setState({vibro:''})
      }
      if(pile.driver != null && pile.driver != '' && pile.driver != undefined) {
        this.setState({ driver: pile.driver })
      }else if(pile.DriverId == null) {
          this.setState({driver:''})
      }
      if(pile.casing != null && pile.casing != '' && pile.casing != undefined) {
        this.setState({ casing: pile.casing })
      }
      if(pile.Length != null && pile.Length != '' && pile.Length != undefined && pile.Length != '15') {
        this.setState({ casinglength: pile.Length.toString() })
      }else if(pile.machine == null) {
          this.setState({casinglength:'15'})
      }
      if(pile.MachineCraneId != null && pile.MachineCraneId != '' && pile.MachineCraneId != undefined) {
        this.setState({ MachineCraneId: pile.MachineCraneId })
      }else if(pile.MachineVibroId == null) {
          this.setState({MachineCraneId:null})
      }
      if(pile.MachineVibroId != null && pile.MachineVibroId != '' && pile.MachineVibroId != undefined) {
        this.setState({ MachineVibroId: pile.MachineVibroId })
      }else if(pile.MachineVibroId == null) {
          this.setState({MachineVibroId:null})
      }
      if(pile.DriverId != null && pile.DriverId != '' && pile.DriverId != undefined) {
        this.setState({ DriverId: pile.DriverId })
      }else if(pile.DriverId == null) {
          this.setState({DriverId:null})
      }
      // if (pile.vibro_flag == false) {
      //   this.setState({ vibroDisable: true })
      // }
    }
    if(this.state.vibroDisable) {
      this.setState({ 
        vibroDisable: this.state.vibroDisable ,
        // vibroData: [{ key: 0, section: true, label: I18n.t("mainpile.2_1.vibroselect") }],
        vibro: ""
      })
    }

    if(pile2_0) {
      // if (pile3_0.Edit_Flag != null && pile3_0.Edit_Flag != undefined && pile3_0.Edit_Flag != '') {
      this.setState({ Edit_Flag: pile2_0.Edit_Flag })
      // }
    }
    if(this.state.check){
      this.saveLocal('2_1')
      this.setState({check:false})
    }
  }


  async changeCasing(casing) {
    await this.setState({ casing: casing })
    this.saveLocal('2_1')
  }

  async changeLength(length) {
    await this.setState({ casinglength: String(length) })
    this.saveLocal('2_1')
  }


  selectedMachine = async machine => {
    if(machine.label == this.state.machine){

    }else{
      await this.setState({ machine: machine.label, MachineCraneId: machine.key, vibroDisable: machine.vibro_flag == true ? false : true },()=>{
        // console.warn(this.state.vibroDisable)
        // if(this.state.vibroDisable == true){
          this.setState({vibroData:[{ key: 0, section: true, label: I18n.t("mainpile.2_1.vibroselect") }],vibro:'',MachineVibroId:''},()=>{console.warn(this.state.vibro)})
        // }
      })
      // if (machine.vibro_flag == false) {
      //   await this.setState({vibro:'',MachineVibroId:'',vibroDisable:true})
      // }
    }
    
    this.saveLocal('2_1')
  }
  selectedVibro = async  vibro => {
    await this.setState({ vibro: vibro.label, MachineVibroId: vibro.key })
    this.saveLocal('2_1')
  }
  selectedDriver = async driver => {
    await this.setState({ driver: driver.label, DriverId: driver.key })
    this.saveLocal('2_1')
  }

  saveLocal(process_step) {
    // console.warn('Length',this.state.casinglength)
    var value = {
      process: 2,
      step: 1,
      pile_id: this.props.pileid,
      data: {
        casing: this.state.casing,
        Length: this.state.casinglength,
        machine: this.state.machine,
        vibro: this.state.vibro,
        driver: this.state.driver,
        MachineCraneId: this.state.MachineCraneId,
        MachineCraneName: this.state.machine,
        MachineVibroId: this.state.MachineVibroId,
        MachineVibroName: this.state.vibro,
        DriverId: this.state.DriverId,
        DriverName: this.state.driver,
        vibro_flag: !this.state.vibroDisable,
      }
    }
    // console.log(value)
    this.props.mainPileSetStorage(this.props.pileid, process_step, value)
  }

  getState() {
    var data = {
      machine: this.state.machine,
      vibro: this.state.vibro,
      driver: this.state.driver,
      casing: this.state.casing,
      Length: this.state.casinglength,
      MachineCraneId: this.state.MachineCraneId,
      MachineVibroId: this.state.MachineVibroId,
      DriverId: this.state.DriverId,
      vibroDisable: this.state.vibroDisable
    }
    return data
  }

  fixzero(value) {
    if(value.split(".")[1]) {
      if(value.split(".")[0].length == 0) {
        return "0" + value
      }
    }
    return value
  }

  render() {
    // console.warn('vibro',this.state.vibro)
    if(this.props.hidden) {
      return null
    }
    if(this.state.loading) {
      return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <ActivityIndicator size="large" color="#007CC2" />
        </View>
      )
    }
    return (
      <View>
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.2_1.casing")}</Text>
          </View>
          <View style={styles.selectedView}>
            <View style={[styles.button, { height: 50, padding: 0, borderColor: MENU_GREY_ITEM }]}>
              <TextInput
                keyboardType="phone-pad"
                onChangeText={casing => this.changeCasing(casing)}
                underlineColorAndroid="transparent"
                value={this.state.casing}
                style={styles.textInputStyleGrey}
                editable={false}
              />
            </View>
          </View>
        </View>
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.2_1.length")}</Text>
          </View>
          {
            this.state.Edit_Flag == 1 ?
              <View style={styles.selectedView}>
                <View style={[styles.button, { height: 50, padding: 0 }]}>
                  <TextInput
                    editable={true}
                    keyboardType="numeric"
                    value={this.state.casinglength}
                    onChangeText={casinglength => isNaN(this.state.casinglength) ? this.setState({ casinglength : '' }) :this.setState({ casinglength })}
                    underlineColorAndroid="transparent"
                    style={styles.textInputStyle}
                    onEndEditing = {() => this.saveLocal('2_1')}
                  />
                </View>
              </View>
              :
              <View style={styles.selectedView}>
                <View style={[styles.button, { height: 50, padding: 0, borderColor: MENU_GREY_ITEM }]}>
                  <TextInput
                    keyboardType="numeric"
                    value={this.state.casinglength}
                    onChangeText={casinglength => this.setState({ casinglength })}
                    underlineColorAndroid="transparent"
                    style={styles.textInputStyleGrey}
                    editable={false}
                    onEndEditing = {() => this.saveLocal('2_1')}
                  />
                </View>
              </View>
          }
        </View>
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.2_1.machine")}</Text>
          </View>
          {
            this.state.Edit_Flag == 1 ?
              <View style={styles.selectedView}>
                <ModalSelector
                  data={this.state.machineData}
                  onChange={this.selectedMachine}
                  selectTextStyle={styles.textButton}
                  cancelText="Cancel"
                >
                  <TouchableOpacity style={styles.button}>
                    <View style={styles.buttonTextStyle}>
                      <Text style={styles.buttonText}>{this.state.machine || I18n.t("mainpile.2_1.machineselect")}</Text>
                      <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                        <Icon name="arrow-drop-down" type="MaterialIcons" color="#007CC2" />
                      </View>
                    </View>
                  </TouchableOpacity>
                </ModalSelector>
              </View>
              :
              <View style={styles.selectedView}>
                <TouchableOpacity disabled={true} style={[styles.button, { borderColor: MENU_GREY_ITEM }]}>
                  <View style={styles.buttonTextStyle}>
                    <Text style={[styles.buttonText, { color: MENU_GREY_ITEM }]}>
                      {this.state.machine || I18n.t("mainpile.2_1.machineselect")}
                    </Text>
                    <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                      <Icon
                        name="arrow-drop-down"
                        type="MaterialIcons"
                        color={MENU_GREY_ITEM}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
          }
        </View>
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.2_1.vibro")}</Text>
          </View>
          {
            this.state.Edit_Flag == 1 ?
              <View style={styles.selectedView}>
                <ModalSelector
                  data={this.state.vibroData}
                  onChange={this.selectedVibro}
                  selectTextStyle={styles.textButton}
                  cancelText="Cancel"
                  disabled={this.state.vibroDisable}
                >
                  <TouchableOpacity style={[styles.button, this.state.vibroDisable ? { borderColor: MENU_GREY_ITEM } : {}]}>
                    <View style={styles.buttonTextStyle}>
                      <Text style={[styles.buttonText, this.state.vibroDisable ? { color: MENU_GREY_ITEM } : {}]}>
                        {this.state.vibro==''?I18n.t("mainpile.2_1.vibroselect"):this.state.vibro}
                      </Text>
                      <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                        <Icon
                          name="arrow-drop-down"
                          type="MaterialIcons"
                          color={this.state.vibroDisable ? MENU_GREY_ITEM : "#007CC2"}
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                </ModalSelector>
              </View>
              :
              <View style={styles.selectedView}>
                <TouchableOpacity disabled={true} style={[styles.button, { borderColor: MENU_GREY_ITEM }]}>
                  <View style={styles.buttonTextStyle}>
                    <Text style={[styles.buttonText, { color: MENU_GREY_ITEM }]}>
                      {this.state.vibro==''?I18n.t("mainpile.2_1.vibroselect"):this.state.vibro}
                    </Text>
                    <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                      <Icon
                        name="arrow-drop-down"
                        type="MaterialIcons"
                        color={MENU_GREY_ITEM}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
          }
        </View>
        <View style={{ marginBottom: 10 }}>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.2_1.driver")}</Text>
          </View>
          {
            this.state.Edit_Flag == 1 ?
              <View style={styles.selectedView}>
                <ModalSelector
                  data={this.state.driverData}
                  onChange={this.selectedDriver}
                  selectTextStyle={styles.textButton}
                  cancelText="Cancel"
                >
                  <TouchableOpacity style={styles.button}>
                    <View style={styles.buttonTextStyle}>
                      <Text style={styles.buttonText}>{this.state.driver || I18n.t("mainpile.2_1.driverselect")}</Text>
                      <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                        <Icon name="arrow-drop-down" type="MaterialIcons" color="#007CC2" />
                      </View>
                    </View>
                  </TouchableOpacity>
                </ModalSelector>
              </View>
              :
              <View style={styles.selectedView}>
                <TouchableOpacity disabled={true} style={[styles.button, { borderColor: MENU_GREY_ITEM }]}>
                  <View style={styles.buttonTextStyle}>
                    <Text style={[styles.buttonText, { color: MENU_GREY_ITEM }]}>
                      {this.state.driver || I18n.t("mainpile.2_1.driverselect")}
                    </Text>
                    <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                      <Icon
                        name="arrow-drop-down"
                        type="MaterialIcons"
                        color={MENU_GREY_ITEM}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
          }
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    count: state.pile.count,
    pileAll: state.mainpile.pileAll || null,
    // driver: state.pile.driver_items,
    // machine: state.pile.machine_items,
    // vibro: state.pile.vibro_items
  }
}

export default connect(mapStateToProps, actions, null, { withRef: true })(Pile2_1)
