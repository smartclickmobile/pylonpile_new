import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native"
import { Container, Content } from "native-base"
import { Icon } from "react-native-elements"
import Carousel from "react-native-snap-carousel"
import ModalSelector from "react-native-modal-selector"
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM } from "../Constants/Color"
import { GroupEmployee } from "../Controller/API"
import I18n from "../../assets/languages/i18n"
import { Extra } from "../Controller/API"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"
import styles from "./styles/Pile3_1.style"
import Spinner from "react-native-loading-spinner-overlay"

class Pile3_1 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      contractor: I18n.t('mainpile.3_1.contractorselect'),
      empid: 0,
      key: 0,
      employee: [],
      surveyData: [{ key: 0, section: true, label: I18n.t("mainpile.3_1.surveyorselect") }],
      survey: "",
      SurveyorId: "",
      benchmarkCamData: [{ key: 0, section: true, label: I18n.t("mainpile.3_1.stationselect") }],
      benchmarkCam: "",
      benchmarkCamLabel: "",
      LocationId: "",
      benchmarkFlagData: [{ key: 0, section: true, label: I18n.t("mainpile.3_1.bsselect") }],
      benchmarkFlag: "",
      benchmarkFlagLabel: "",
      BSFlagLocationId: "",
      resection: false,
      process: 3,
      step: 1,
      loading:true,
      Edit_Flag:1,
      count:0,
      ApprovedDate:''
    }
    this.prevState = this.state
  }

  componentDidUpdate(prevProps, prevState){
    if (this.state.surveyData != prevState.surveyData) {
      this.setState({count:this.state.count + 1})
    }
    if (this.state.benchmarkFlagData != prevState.benchmarkFlagData) {
      this.setState({count:this.state.count + 1})
    }
    if (this.state.count != prevState.count && this.state.count == 1) {
      this.setState({loading:false})
    }else {
      if (this.state.loading == true && this.props.hidden == false) {
        setTimeout(() => {this.setState({loading: false})}, 100)
      }
    }
  }
  async componentDidMount(){
    if (this.props.onRefresh === 'refresh' && this.state.loading === true) {
        Actions.refresh({
            action: 'start',
        })
    }
  }
  async componentWillReceiveProps(nextProps) {
    var pile = null
    var pile3_0 = null
    var pileMaster = null
    var pile3_4 = null
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["3_1"]) {
      pile = nextProps.pileAll[this.props.pileid]["3_1"].data
      pileMaster = nextProps.pileAll[this.props.pileid]['3_1'].masterInfo
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['3_0']) {
      pile3_0 = nextProps.pileAll[this.props.pileid]['3_0'].data
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["3_4"]){
      pile3_4 = nextProps.pileAll[this.props.pileid]["3_4"].data
    }

      //       surveyData.push({ key: index + 1, label: name.firstname + " " + name.lastname, id: name.employee_id })
    if (pile != null) {
      if (this.state.surveyData != pileMaster.SurveyList &&  pileMaster.SurveyList != '' && this.state.surveyData.length == 1  ) {
        let list = [{ key: 0, section: true, label: I18n.t("mainpile.3_1.surveyorselect")  }]
        for (var i = 0; i < pileMaster.SurveyList.length; i++) {
          if (pile) {
            if (pileMaster.SurveyList[i].employee_id == pile.SurveyorId) {
              this.setState({
                survey: (pileMaster.SurveyList[i].nickname && pileMaster.SurveyList[i].nickname + "-") + pileMaster.SurveyList[i].firstname + " " + pileMaster.SurveyList[i].lastname,
                SurveyorId: pile.SurveyorId,
              })
            }
          }
          await list.push({
            key: pileMaster.SurveyList[i].employee_id,
            label: (pileMaster.SurveyList[i].nickname && pileMaster.SurveyList[i].nickname + "-") + pileMaster.SurveyList[i].firstname + " " + pileMaster.SurveyList[i].lastname,
            id: pileMaster.SurveyList[i].employee_id
          })
        }
        await this.setState({surveyData:list})
      }
      //       benchmarkData.push({ key: index + 1, label: name.no, id: name.id, no: name.utm_n + ", " + name.utm_e })
      if (this.state.benchmarkCamData != pileMaster.BenchmarkList && pileMaster.BenchmarkList != '' && this.state.benchmarkCamData.length == 1 ) {
        let list = [{ key: 0, section: true, label: I18n.t("mainpile.3_1.stationselect")  }]
        for (var i = 0; i < pileMaster.BenchmarkList.length; i++) {
          if (pile) {
            if (pileMaster.BenchmarkList[i].id == pile.LocationId) {
              this.setState({
                benchmarkCam: pileMaster.BenchmarkList[i].no,
                LocationId: pile.LocationId,
                benchmarkCamLabel: parseFloat(pileMaster.BenchmarkList[i].utm_n).toFixed(3)
                + ", " + parseFloat(pileMaster.BenchmarkList[i].utm_e).toFixed(3)
              })
            }
          }
          await list.push({
            key: pileMaster.BenchmarkList[i].id,
            label: pileMaster.BenchmarkList[i].no,
            no: parseFloat(pileMaster.BenchmarkList[i].utm_n).toFixed(3)
            + ", " +parseFloat(pileMaster.BenchmarkList[i].utm_e).toFixed(3)
          })
        }
          await this.setState({benchmarkCamData:list})
      }
      if (this.state.benchmarkFlagData != pileMaster.BenchmarkList && pileMaster.BenchmarkList != '' && this.state.benchmarkFlagData.length == 1) {
        let list = [{ key: 0, section: true, label: I18n.t("mainpile.3_1.bsselect")  }]
        for (var i = 0; i < pileMaster.BenchmarkList.length; i++) {
          if (pile) {
            if (pileMaster.BenchmarkList[i].id == pile.BSFlagLocationId) {
              this.setState({
                benchmarkFlag: pileMaster.BenchmarkList[i].no,
                BSFlagLocationId: pile.BSFlagLocationId,
                benchmarkFlagLabel:parseFloat(pileMaster.BenchmarkList[i].utm_n).toFixed(3)
                + ", " + parseFloat(pileMaster.BenchmarkList[i].utm_e).toFixed(3)
              })
            }
          }
          await list.push({
            key: pileMaster.BenchmarkList[i].id,
            label: pileMaster.BenchmarkList[i].no,
            no: parseFloat(pileMaster.BenchmarkList[i].utm_n).toFixed(3)
            + ", " +parseFloat(pileMaster.BenchmarkList[i].utm_e).toFixed(3)
          })
        }
          await this.setState({benchmarkFlagData:list})
      }
    }

    if (pile) {
      if (pile.survey != null && pile.survey != undefined && pile.survey != '') {
        this.setState({ survey: pile.survey })
      }else if(pile.SurveyorId == null) {
        this.setState({survey:"",SurveyorId:null})
      }
      if (pile.benchmarkCam != null && pile.benchmarkCam != undefined && pile.benchmarkCam != '') {
        this.setState({ benchmarkCam: pile.benchmarkCam })
      }else if(pile.LocationId == null) {
        this.setState({benchmarkCam:""})
      }
      if (pile.benchmarkFlag != null && pile.benchmarkFlag != undefined && pile.benchmarkFlag != '') {
        this.setState({ benchmarkFlag: pile.benchmarkFlag })
      }else if(pile.BSFlagLocationId == null) {
        this.setState({benchmarkFlag:""})
      }
      if (pile.benchmarkCamLabel != null && pile.benchmarkCamLabel != undefined && pile.benchmarkCamLabel != '') {
        this.setState({ benchmarkCamLabel: pile.benchmarkCamLabel })
      }else if(pile.LocationId == null) {
        this.setState({benchmarkCamLabel:"",LocationId:null})
      }
      if (pile.benchmarkFlagLabel != null && pile.benchmarkFlagLabel != undefined && pile.benchmarkFlagLabel != '') {
        this.setState({ benchmarkFlagLabel: pile.benchmarkFlagLabel })
      }else if(pile.BSFlagLocationId == null) {
        this.setState({benchmarkFlagLabel:"",BSFlagLocationId:null})
      }
      if (pile.resection != null && pile.resection != undefined && pile.resection != '') {
        this.setState({ resection: pile.resection })
      }else if(pile.resection == null) {
        this.setState({resection:false})
      }
    }
    if(pile3_4){
      if(pile3_4.ApprovedDate != null && pile3_4.ApprovedDate != undefined && pile3_4.ApprovedDate != ''){
        this.setState({ApprovedDate:pile3_4.ApprovedDate})
      }
    }
    if (pile3_0) {
      // if (pile3_0.Edit_Flag != null && pile3_0.Edit_Flag != undefined && pile3_0.Edit_Flag != '') {
        this.setState({Edit_Flag:pile3_0.Edit_Flag})
      // }
    }
  }

  getState() {
    var data = {
      survey: this.state.survey,
      benchmarkCam: this.state.benchmarkCam,
      benchmarkFlag: this.state.benchmarkFlag,
      benchmarkCamLabel: this.state.benchmarkCamLabel,
      benchmarkFlagLabel: this.state.benchmarkFlagLabel,
      resection: this.state.resection,
      SurveyorId: this.state.SurveyorId,
      LocationId: this.state.LocationId,
      BSFlagLocationId: this.state.BSFlagLocationId
    }
    return data
  }

  isSelected() {
    return this.state.key != 0
  }

  async saveLocal(process_step){
    var value = {
      process:3,
      step:1,
      pile_id: this.props.pileid,
      data:{
        survey: this.state.survey,
        benchmarkCam: this.state.benchmarkCam,
        benchmarkFlag: this.state.benchmarkFlag,
        resection: this.state.resection,
        benchmarkCamLabel: this.state.benchmarkCamLabel,
        benchmarkFlagLabel: this.state.benchmarkFlagLabel,
        SurveyorId: this.state.SurveyorId,
        SurveyorName: this.state.survey,
        LocationId: this.state.LocationId,
        LocationName: this.state.benchmarkCam,
        BSFlagLocationId: this.state.BSFlagLocationId,
        BSFlagLocationName: this.state.benchmarkFlag
      }
    }
    await this.props.mainPileSetStorage(this.props.pileid,process_step,value)

  }
  selectedsurvey = async survey => {
    await this.setState({ survey: survey.label,SurveyorId:survey.id })
    this.saveLocal('3_1')
  }
  selectedBenchmarkCam = async benchmark => {
    await this.setState({ benchmarkCam: benchmark.label, benchmarkCamLabel: benchmark.no,LocationId:benchmark.key })
    this.saveLocal('3_1')
  }
  selectedBenchmarkFlag = async benchmark => {
    await this.setState({ benchmarkFlag: benchmark.label, benchmarkFlagLabel: benchmark.no,BSFlagLocationId:benchmark.key  })
    this.saveLocal('3_1')
  }
  async selectedResection(){
    if (this.state.Edit_Flag == 1) {
      await this.setState({ resection: !this.state.resection })
      this.saveLocal('3_1')
    }
  }

  _renderFirstTopic() {
    return (
      <View>
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t("mainpile.3_1.surveyor")}</Text>
        </View>
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.3_1.surveyorname")}</Text>
          </View>
          {
            // this.state.Edit_Flag == 1 && this.state.ApprovedDate == ''?
            this.state.Edit_Flag == 1 && this.props.waitApprove == false?
            <View style={styles.selectedView}>
              <ModalSelector
                data={this.state.surveyData}
                onChange={this.selectedsurvey}
                selectTextStyle={styles.textButton}
                cancelText="Cancel"
              >
                <TouchableOpacity style={styles.button}>
                  <View style={styles.buttonTextStyle}>
                    <Text style={styles.buttonText}>{this.state.survey || I18n.t("mainpile.3_1.surveyorselect")}</Text>
                    <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                      <Icon name="arrow-drop-down" type="MaterialIcons" color="#007CC2" />
                    </View>
                  </View>
                </TouchableOpacity>
              </ModalSelector>
            </View>
            :
            <View style={styles.selectedView}>
              <TouchableOpacity disabled={true} style={[styles.button, { borderColor: MENU_GREY_ITEM }]}>
                <View style={styles.buttonTextStyle}>
                  <Text style={[styles.buttonText ,{ color: MENU_GREY_ITEM }]}>
                    {this.state.survey || I18n.t("mainpile.3_1.surveyorselect")}
                  </Text>
                  <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                    <Icon
                      name="arrow-drop-down"
                      type="MaterialIcons"
                      color={MENU_GREY_ITEM}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          }
        </View>
      </View>
    )
  }

  _renderSecondTopic() {
    return (
      <View style={{ borderTopWidth: 0.5, marginTop: 15, marginBottom: 20 }}>
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t("mainpile.3_1.information")}</Text>
        </View>
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.3_1.station")}</Text>
          </View>
          {
            // this.state.Edit_Flag == 1 && this.state.ApprovedDate == ''?
            this.state.Edit_Flag == 1 && this.props.waitApprove == false?
            <View style={styles.selectedView}>
              <ModalSelector
                data={this.state.benchmarkCamData}
                onChange={this.selectedBenchmarkCam}
                selectTextStyle={styles.textButton}
                cancelText="Cancel"
              >
                <TouchableOpacity style={styles.button}>
                  <View style={styles.buttonTextStyle}>
                    <Text style={styles.buttonText}>
                      {this.state.benchmarkCam || I18n.t("mainpile.3_1.stationselect")}
                    </Text>
                    <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                      <Icon name="arrow-drop-down" type="MaterialIcons" color="#007CC2" />
                    </View>
                  </View>
                </TouchableOpacity>
              </ModalSelector>
            </View>
            :
            <View style={styles.selectedView}>
              <TouchableOpacity disabled={true} style={[styles.button, { borderColor: MENU_GREY_ITEM }]}>
                <View style={styles.buttonTextStyle}>
                  <Text style={[styles.buttonText ,{ color: MENU_GREY_ITEM }]}>
                    {this.state.benchmarkCam || I18n.t("mainpile.3_1.stationselect")}
                  </Text>
                  <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                    <Icon
                      name="arrow-drop-down"
                      type="MaterialIcons"
                      color={MENU_GREY_ITEM}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          }
          <View style={styles.description}>
            <Text>{this.state.benchmarkCamLabel}</Text>
          </View>
        </View>
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.3_1.bs")}</Text>
          </View>
          {
            // this.state.Edit_Flag == 1 && this.state.ApprovedDate == '' ?
            this.state.Edit_Flag == 1 && this.props.waitApprove == false?
            <View style={styles.selectedView}>
              <ModalSelector
                data={this.state.benchmarkFlagData}
                onChange={this.selectedBenchmarkFlag}
                selectTextStyle={styles.textButton}
                cancelText="Cancel"
              >
                <TouchableOpacity style={styles.button}>
                  <View style={styles.buttonTextStyle}>
                    <Text style={styles.buttonText}>{this.state.benchmarkFlag || I18n.t("mainpile.3_1.bsselect")}</Text>
                    <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                      <Icon name="arrow-drop-down" type="MaterialIcons" color="#007CC2" />
                    </View>
                  </View>
                </TouchableOpacity>
              </ModalSelector>
            </View>
            :
            <View style={styles.selectedView}>
              <TouchableOpacity disabled={true} style={[styles.button, { borderColor: MENU_GREY_ITEM }]}>
                <View style={styles.buttonTextStyle}>
                  <Text style={[styles.buttonText ,{ color: MENU_GREY_ITEM }]}>
                    {this.state.benchmarkFlag || I18n.t("mainpile.3_1.stationselect")}
                  </Text>
                  <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                    <Icon
                      name="arrow-drop-down"
                      type="MaterialIcons"
                      color={MENU_GREY_ITEM}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          }
          <View style={styles.description}>
            <Text>{this.state.benchmarkFlagLabel}</Text>
          </View>
        </View>
      </View>
    )
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    if (this.state.loading) {
      return (
        <View style={{ flex: 1,justifyContent: 'center' }}>
          <ActivityIndicator size="large" color="#007CC2" />
        </View>
      )
    }
    return (
      <View>
        {this._renderFirstTopic()}
        {this._renderSecondTopic()}
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            borderTopWidth: 1,
            borderColor: MENU_GREY_ITEM,
            paddingBottom: 10
          }}
        >
          {this.state.resection == true ? (
            <Icon
              name="check"
              reverse
              color="#6dcc64"
              size={15}
              // onPress={() => this.state.Edit_Flag == 1 && this.state.ApprovedDate == ''? this.selectedResection():console.log('not edit')}
              onPress={() => this.state.Edit_Flag == 1 && this.props.waitApprove == false? this.selectedResection():console.log('not edit')}
              />
          ) : (
            <Icon
              name="check"
              reverse
              color="grey"
              size={15}
              // onPress={() => this.state.Edit_Flag == 1 && this.state.ApprovedDate == ''? this.selectedResection():console.log('not edit')}
              onPress={() => this.state.Edit_Flag == 1 && this.props.waitApprove == false ? this.selectedResection():console.log('not edit')}
            />
          )}
          <Text>Resection</Text>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    // benchmark: state.pile.benchmark_items,
    // survey: state.pile.survey_items,
    pileAll: state.mainpile.pileAll || null
  }
}

export default connect(mapStateToProps, actions, null, { withRef: true })(Pile3_1)
