import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native"
import { Container, Content } from "native-base"
import { Icon } from "react-native-elements"
import Carousel from "react-native-snap-carousel"
import ModalSelector from "react-native-modal-selector"
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM } from "../Constants/Color"
import { GroupEmployee } from "../Controller/API"
import I18n from "../../assets/languages/i18n"
import { Extra } from "../Controller/API"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"
import styles from "./styles/Pile3_1.style"
import Spinner from "react-native-loading-spinner-overlay"

class Pile3_5 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      contractor: I18n.t('mainpile.3_1.contractorselect'),
      key: 0,
      surveyData: [{ key: 0, section: true, label: I18n.t("mainpile.3_1.surveyorselect") }],
      survey: "",
      SurveyorId: "",
      process: 3,
      step: 1,
      loading:true,
      Edit_Flag:1,
      count:0,
      top: "",
      ground: "",
      status3:0,
      status4:0,
      statusAll:0,
      status5:0,
      top1:false
    }
    this.prevState = this.state
  }

  componentDidUpdate(prevProps, prevState){
    if (this.state.surveyData != prevState.surveyData) {
      this.setState({count:this.state.count + 1})
    }
    
    if (this.state.count != prevState.count && this.state.count == 1) {
      this.setState({loading:false})
    }else {
      if (this.state.loading == true && this.props.hidden == false) {
        setTimeout(() => {this.setState({loading: false})}, 100)
      }
    }
  }
  async componentDidMount(){
    if (this.props.onRefresh === 'refresh' && this.state.loading === true) {
        Actions.refresh({
            action: 'start',
        })
    }
  }
  async componentWillReceiveProps(nextProps) {
    var pile = null
    var pile3_0 = null
    var pileMaster = null
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["3_5"]) {
      pile = nextProps.pileAll[this.props.pileid]["3_5"].data
      pileMaster = nextProps.pileAll[this.props.pileid]['3_5'].masterInfo
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['3_0']) {
      pile3_0 = nextProps.pileAll[this.props.pileid]['3_0'].data
    }
    if (nextProps.pileAll[nextProps.pileid] != undefined) {
      var edit = nextProps.pileAll[nextProps.pileid]
      if(edit != undefined){
        this.setState({
          status3:edit.step_status.Step3Status,
          status4:edit.step_status.Step4Status,
          statusAll:edit.step_status,
          status5:edit.step_status.Step5Status
        })
      }
    }
      //       surveyData.push({ key: index + 1, label: name.firstname + " " + name.lastname, id: name.employee_id })
    if (pile != null) {
      if (this.state.surveyData != pileMaster.SurveyList &&  pileMaster.SurveyList != '' && this.state.surveyData.length == 1  ) {
        let list = [{ key: 0, section: true, label: I18n.t("mainpile.3_1.surveyorselect")  }]
        for (var i = 0; i < pileMaster.SurveyList.length; i++) {
          if (pile) {
            if (pileMaster.SurveyList[i].employee_id == pile.SurveyorId) {
              this.setState({
                survey: (pileMaster.SurveyList[i].nickname && pileMaster.SurveyList[i].nickname + "-") + pileMaster.SurveyList[i].firstname + " " + pileMaster.SurveyList[i].lastname,
                SurveyorId: pile.SurveyorId,
              })
            }
          }
          await list.push({
            key: pileMaster.SurveyList[i].employee_id,
            label: (pileMaster.SurveyList[i].nickname && pileMaster.SurveyList[i].nickname + "-") + pileMaster.SurveyList[i].firstname + " " + pileMaster.SurveyList[i].lastname,
            id: pileMaster.SurveyList[i].employee_id
          })
        }
        await this.setState({surveyData:list})
      }
      
      
    }

    if (pile) {
      if (pile.survey != null && pile.survey != undefined && pile.survey != '') {
        this.setState({ survey: pile.survey })
      }else if(pile.SurveyorId == null) {
        this.setState({survey:"",SurveyorId:null})
      }
      if(pile.top != null && pile.top != undefined && this.state.top == ''){
        await this.setState({top:isNaN(parseFloat(pile.top.toString()).toFixed(3))?"":parseFloat(pile.top.toString()).toFixed(3)})
      }else if (pile.top == null) {
        this.setState({top:""})
      }
      if(pile.ground != null && pile.ground != undefined && this.state.ground == ''){
        await this.setState({ground:isNaN(parseFloat(pile.ground.toString()).toFixed(3))?"":parseFloat(pile.ground.toString()).toFixed(3)})
      }else if (pile.ground == null) {
        this.setState({ground:""})
      }
    }
    if (pile3_0) {
      // if (pile3_0.Edit_Flag != null && pile3_0.Edit_Flag != undefined && pile3_0.Edit_Flag != '') {
        this.setState({Edit_Flag:pile3_0.Edit_Flag})
      // }
    }
  }
  async selectedGround(ground){
    await this.setState({ ground: ground})
  }
  async selectedTop(top) {
    await this.setState({ top: top})
  }
  getState() {
    var data = {
      survey: this.state.survey,
      SurveyorId: this.state.SurveyorId,
    }
    return data
  }

  isSelected() {
    return this.state.key != 0
  }
  edittop(){
    // console.warn('edittop')
    var temp = ''
    if(I18n.locale=='th'){
      temp = '"'
    }
    if(this.state.status3 == 2 && this.state.status5 !=0){

      if(this.state.statusAll.Step10Status != 0){
        Alert.alert('', I18n.t('mainpile.3_3.warn1_1')+I18n.t('mainpile.step.10')+temp+I18n.t('mainpile.3_3.warn2dw'), [
          {text: 'OK'}
        ])
      }else if(this.state.statusAll.Step8Status != 0){
        Alert.alert('', I18n.t('mainpile.3_3.warn1_1')+I18n.t('mainpile.step.8')+temp+I18n.t('mainpile.3_3.warn2dw'), [
          {text: 'OK'}
        ])
      }else if(this.state.statusAll.Step7Status != 0){
        Alert.alert('', I18n.t('mainpile.3_3.warn1_1')+I18n.t('mainpile.step.7')+temp+I18n.t('mainpile.3_3.warn2dw'), [
          {text: 'OK'}
        ])
      }else if(this.state.statusAll.Step6Status != 0){
        Alert.alert('', I18n.t('mainpile.3_3.warn1_1')+I18n.t('mainpile.step.6')+temp+I18n.t('mainpile.3_3.warn2dw'), [
          {text: 'OK'}
        ])
      }else if(this.state.statusAll.Step5Status != 0){
        Alert.alert('', I18n.t('mainpile.3_3.warn4_1')+I18n.t('mainpile.step.5_dw')+'"'+I18n.t('mainpile.3_3.warn5dw'), [
          {text: 'OK'}
        ])
      }
      this.setState({top1:true})
      // Alert.alert('', I18n.t('mainpile.3_3.warn1')+index+I18n.t('mainpile.3_3.warn2'), [
      //   {text: 'OK'}
      //   ])
    }else{
      // console.warn('else')
      this.setState({top:this.state.top ? parseFloat(this.state.top).toFixed(3) : ''})
      this.saveLocal('3_5')
    }
    
     
  }
  onChangeFormatDecimal(data){
    if (data.split(".")[1]) {
      if (data.split(".")[1].length > 3) {
        return parseFloat(data).toFixed(3)
      }else {
        return data
      }
    }else {
      return data
    }
  }
  async saveLocal(process_step){
    // console.warn(isNaN(parseFloat(this.state.ground)))
    var value = {
      process:3,
      step:5,
      pile_id: this.props.pileid,
      data:{
        survey: this.state.survey,
        SurveyorId: this.state.SurveyorId,
        SurveyorName: this.state.survey,
        top:isNaN(parseFloat(this.state.top))?'':this.state.top,
        ground:isNaN(parseFloat(this.state.ground))?'':this.state.ground,
      }
    }
    await this.props.mainPileSetStorage(this.props.pileid,process_step,value)

  }
  selectedsurvey = async survey => {
    await this.setState({ survey: survey.label,SurveyorId:survey.id })
    this.saveLocal('3_5')
  }
  _renderSecondTopic(){
    return (
      <View>
        <View style={styles.topic}>
          <Text style={styles.topicText}>{this.props.shape == 1 ? I18n.t("mainpile.3_3.position_bp") : I18n.t("mainpile.3_3.position_dw") }</Text>
        </View>

        <View>
          <View style={styles.boxTopic}>
            <Text>{this.props.shape == 1 ? I18n.t("mainpile.3_3.top") : I18n.t("mainpile.3_3.top_dwall") }</Text>
          </View>
          {
            this.state.Edit_Flag == 1 ?
            <View style={styles.selectedView}>
            {this.state.status3 == 2 && this.state.status5 !=0?
              <TouchableOpacity onPress={()=>this.edittop()} style={styles.button1}>
                <TextInput
                  editable={false}
                  keyboardType="numeric"
                  onChangeText={(top) => this.state.top1?this.state.top:this.selectedTop(top)}
                  underlineColorAndroid="transparent"
                  value={this.state.top}
                  style={styles.buttonText1}
                  placeholder={this.props.shape == 1 ? I18n.t("mainpile.3_3.top") : I18n.t("mainpile.3_3.top_dwall") }
                  // onFocus={()=>this.edittop()}
                  // onEndEditing={()=> this.edittop()}
                />
              </TouchableOpacity>
              :
              <View style={styles.button1}>
                <TextInput
                  editable={true}
                  keyboardType="numeric"
                  onChangeText={(top) => this.state.top1?this.state.top:this.selectedTop(top)}
                  underlineColorAndroid="transparent"
                  value={this.state.top}
                  style={styles.buttonText1}
                  placeholder={this.props.shape == 1 ? I18n.t("mainpile.3_3.top") : I18n.t("mainpile.3_3.top_dwall") }
                  // onFocus={()=>this.edittop()}
                  onEndEditing={()=> { 
                    this.setState({top:isNaN(parseFloat(this.state.top))?'': parseFloat(this.state.top).toFixed(3)})
                    this.saveLocal('3_5')
                  }}
                />
              </View>
            }
            </View>
            :
            <View style={styles.selectedView}>
              <TouchableOpacity disabled={true} style={[styles.button, { borderColor: MENU_GREY_ITEM ,height:40}]}>
                <View style={styles.buttonTextStyle}>
                  <Text style={[styles.buttonText ,{ color: MENU_GREY_ITEM }]}>
                    {this.state.top!='' ? isNaN(this.state.top)  ? '' : parseFloat(this.state.top).toFixed(3) :I18n.t("mainpile.3_3.top_dwall")}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          }
        </View>

        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.3_3.ground")}</Text>
          </View>
            {
              this.state.Edit_Flag == 1 ?
              <View style={styles.selectedView}>
                <View style={styles.button1}>
                  <TextInput
                    editable={true}
                    keyboardType="numeric"
                    onChangeText={(ground) => this.selectedGround(ground)}
                    underlineColorAndroid="transparent"
                    value={this.state.ground}
                    style={styles.buttonText1}
                    placeholder={I18n.t("mainpile.3_3.ground")}
                    onEndEditing={()=> {
                      this.setState({ground:isNaN(parseFloat(this.state.ground))?'':  parseFloat(this.state.ground).toFixed(3) })
                      this.saveLocal('3_5')
                    }}
                  />
                </View>
              </View>
              :
              <View style={styles.selectedView}>
                <TouchableOpacity disabled={true} style={[styles.button, { borderColor: MENU_GREY_ITEM ,height:40}]}>
                  <View style={styles.buttonTextStyle}>
                    <Text style={[styles.buttonText ,{ color: MENU_GREY_ITEM }]}>
                      {this.state.ground!='' ? isNaN(this.state.ground)  ? '' : parseFloat(this.state.ground).toFixed(3) :I18n.t("mainpile.3_3.top_dwall")}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            }
          </View>
        </View>
    )
  }
  _renderFirstTopic() {
    return (
      <View>
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t("mainpile.3_1.surveyor")}</Text>
        </View>
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.3_1.surveyorname")}</Text>
          </View>
          {
            this.state.Edit_Flag == 1 ?
            <View style={styles.selectedView}>
              <ModalSelector
                data={this.state.surveyData}
                onChange={this.selectedsurvey}
                selectTextStyle={styles.textButton}
                cancelText="Cancel"
              >
                <TouchableOpacity style={styles.button}>
                  <View style={styles.buttonTextStyle}>
                    <Text style={styles.buttonText}>{this.state.survey || I18n.t("mainpile.3_1.surveyorselect")}</Text>
                    <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                      <Icon name="arrow-drop-down" type="MaterialIcons" color="#007CC2" />
                    </View>
                  </View>
                </TouchableOpacity>
              </ModalSelector>
            </View>
            :
            <View style={styles.selectedView}>
              <TouchableOpacity disabled={true} style={[styles.button, { borderColor: MENU_GREY_ITEM }]}>
                <View style={styles.buttonTextStyle}>
                  <Text style={[styles.buttonText ,{ color: MENU_GREY_ITEM }]}>
                    {this.state.survey || I18n.t("mainpile.3_1.surveyorselect")}
                  </Text>
                  <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                    <Icon
                      name="arrow-drop-down"
                      type="MaterialIcons"
                      color={MENU_GREY_ITEM}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          }
        </View>
      </View>
    )
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    if (this.state.loading) {
      return (
        <View style={{ flex: 1,justifyContent: 'center' }}>
          <ActivityIndicator size="large" color="#007CC2" />
        </View>
      )
    }
    return (
      <View>
        {this._renderFirstTopic()}
        {this._renderSecondTopic()}
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    // benchmark: state.pile.benchmark_items,
    // survey: state.pile.survey_items,
    pileAll: state.mainpile.pileAll || null
  }
}

export default connect(mapStateToProps, actions, null, { withRef: true })(Pile3_5)
