import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, Image, Picker, TextInput, Alert, TouchableOpacity,  ActivityIndicator, } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { Container, Content } from 'native-base'
import { Icon } from 'react-native-elements'
import { Actions } from "react-native-router-flux"
import Carousel from 'react-native-snap-carousel'
import ModalSelector from 'react-native-modal-selector'
import { MAIN_COLOR,MENU_GREY_ITEM } from '../Constants/Color'
import { Extra } from '../Controller/API'
import { connect } from "react-redux"
import * as actions from "../Actions"
import I18n from '../../assets/languages/i18n'
import styles from './styles/Pile1_1.style'
import Spinner from "react-native-loading-spinner-overlay"

class Pile1_1 extends Component {
  constructor(props) {
    super(props)
    props.pile = null
    this.state = {
      process: 1,
      step: 1,
      contractor: '',
      empid: 0,
      key: 0,
      employee: [],
      providers: [{ key: 0, section: true, label: I18n.t('mainpile.1_1.select') }],
      providerName: '',
      ProviderId:'',
      list:[],
      loading:true,
      Edit_Flag:1,
      count:0,
    }
  }

  componentDidUpdate(prevProps, prevState){
    if (this.state.providers != prevState.providers) {
      this.setState({count:this.state.count + 1})
    }
    if (this.state.count != prevState.count && this.state.count == 0) {
      // console.log(1)
      this.setState({loading:false})
    }else {
      if (this.state.loading == true && prevProps.hidden == false) {
        // console.log(this.state.loading)
        setTimeout(() => {this.setState({loading: false})}, 100)
      }
    }
  }
  async componentDidMount(){
    if (this.props.onRefresh == 'refresh' && this.state.loading == true) {
      Actions.refresh({
          action: 'start',
      })
    }
  }

  componentWillMount(){
    this.setState({contractor:'',empid:0,key:0})
    this.fetchProvider()

  }
   componentWillReceiveProps(nextProps) {
     // if (nextProps.onRefresh == 'refresh' && this.state.loading == false) {
     //   Actions.refresh({
     //       action: 'start',
     //   })
     //   this.props.mainPileGetAllStorage()
     // }
     var pile = null
     var pileMaster = null
     var pile1_0 = null
     if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['1_1']) {
       pile = nextProps.pileAll[this.props.pileid]['1_1'].data
       pileMaster = nextProps.pileAll[this.props.pileid]['1_1'].masterInfo
     }
     if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['1_0']) {
       pile1_0 = nextProps.pileAll[this.props.pileid]['1_0'].data
     }

     if (pileMaster && pileMaster.ProviderList && pileMaster.ProviderList != this.state.providers) {
      let providers = [{ key: 0, section: true, label: I18n.t('mainpile.1_1.select') }]
      pileMaster.ProviderList.map((provider, index) => {
        providers.push({ key: index + 1, label: provider.provider_name, id: provider.provider_id })
        if(pile){
          if (provider.provider_id == pile.ProviderId) {
            this.setState({contractor: provider.provider_name,empid: provider.provider_id,key:index + 1})
          }else if(pile.ProviderId==null){
            this.setState({contractor:'',empid:0,key:0})
          }
        }
        // if (nextProps.valueInfo != null && nextProps.valueInfo != '') {
        //   if (provider.provider_id == nextProps.valueInfo.ProviderId) {
        //     this.setState({contractor: provider.provider_name,empid: provider.provider_id,key:index + 1})
        //   }else if (nextProps.dataLocal != null && nextProps.dataLocal != '' && nextProps.dataLocal.data && provider.provider_id == nextProps.dataLocal.data.ProviderId) {
        //     this.setState({contractor: provider.provider_name,empid: provider.provider_id,key:index + 1})
        //   }else {
        //     this.setState({contractor: '',empid:0,key:0})
        //   }
        // }
      })
       this.setState({ providers: providers})
    }
    // if (nextProps.dataLocal != null && nextProps.dataLocal != '' && nextProps.dataLocal.data) {
    //   if(nextProps.dataLocal.data.providerName != null){
    //     this.setState({contractor: nextProps.dataLocal.data.providerName})
    //   }
    //   if(nextProps.dataLocal.data.ProviderId != null){
    //     this.setState({empid: nextProps.dataLocal.data.ProviderId});
    //   }
    // }
    if (pile1_0) {
      // if (pile3_0.Edit_Flag != null && pile3_0.Edit_Flag != undefined && pile3_0.Edit_Flag != '') {
        this.setState({Edit_Flag:pile1_0.Edit_Flag})
      // }
    }
  }

  fetchProvider(){
    this.props.providerItems({jobid: this.props.jobid})
  }

  getState(){
    var data = {
      providerName: this.state.contractor,
      ProviderId:this.state.empid,
    }
    return data
  }


  // async _loadEmployee() {
  //   var token = ""
  //   try {
  //     const value = await AsyncStorage.getItem("token")
  //     if (value !== null) {
  //       token = value
  //     }
  //   } catch (error) {
  //     console.log(error)
  //   }
  //   var param = {
  //     language: I18n.locale,
  //     token: token,
  //     jobid: this.props.jobid
  //   }
  //   Extra(param, 'provider', (json) => {
  //     console.log(json)
  //     emp = [{ key: 0, section: true, label: 'เลือกรายชื่อผู้รับเหมา' }]
  //     let employees = json.ProviderList
  //     employees.map((employee, index) => {
  //       emp.push({ key: index + 1, label: employee.provider_name, id: employee.provider_id })
  //     })
  //     this.setState({ employee: emp })
  //   }, (error) => {
  //     console.warn(error)
  //     console.log(error)
  //   })
  // }

  clearPicker() {
    this.setState({ id: 0, contractor: I18n.t('mainpile.1_1.select'), key: 0 })
  }

  isSelected() {
    return this.state.key != 0
  }

  async selectValue(contractor) {
    this.setState({contractor: contractor.label, empid: contractor.id, key: contractor.key})
    var value = {
      process:1,
      step:1,
      pile_id: this.props.pileid,
        data:{
        providerName:contractor.label,
        ProviderId:contractor.id,
      }
    }
    await this.props.mainPileSetStorage(this.props.pileid,'1_1',value)
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    if (this.state.loading) {
      return (
        <View style={{ flex: 1,justifyContent: 'center' }}>
          <ActivityIndicator size="large" color="#007CC2" />
        </View>
      )
    }
    return (
      <View>
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t('mainpile.1_1.name')}</Text>
        </View>
        {
          this.state.Edit_Flag == 1 ?
          <View style={{margin: 20}}>
            <ModalSelector
                data={this.state.providers}
                onChange={ (contractor) => this.selectValue(contractor) }
                selectTextStyle={styles.textButton}
                cancelText="Cancel">
                <TouchableOpacity style={styles.button}>
                  <View style={styles.buttonTextStyle}>
                    <Text style={styles.buttonText}>{this.state.contractor || I18n.t('mainpile.1_1.select')}</Text>
                    <View style={{justifyContent: 'flex-end', flexDirection: 'row'}}>
                      <Icon name='arrow-drop-down' type='MaterialIcons' color='#007CC2'></Icon>
                    </View>
                  </View>
                </TouchableOpacity>
            </ModalSelector>
          </View>
          :
          <View style={{margin: 20}}>
            <TouchableOpacity disabled={true} style={[styles.button, { borderColor: MENU_GREY_ITEM }]}>
              <View style={styles.buttonTextStyle}>
                <Text style={[styles.buttonText ,{ color: MENU_GREY_ITEM }]}>
                  {this.state.contractor || I18n.t("mainpile.1_1.select")}
                </Text>
                <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                  <Icon
                    name="arrow-drop-down"
                    type="MaterialIcons"
                    color={MENU_GREY_ITEM}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        }
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll || null,
    valueInfo: state.pile.valueInfo,
  }
}

export default connect(mapStateToProps, actions, null, { withRef: true })(Pile1_1)
