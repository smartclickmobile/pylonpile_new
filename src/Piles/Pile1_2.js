import React, { Component } from "react"
import {
  Alert,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Linking
} from "react-native"
import { connect } from "react-redux"
import { Container, Content } from "native-base"
import { Icon } from "react-native-elements"
import Carousel from "react-native-snap-carousel"
import ModalSelector from "react-native-modal-selector"
import * as actions from "../Actions"
import { MAIN_COLOR, MENU_GREY } from "../Constants/Color"
import { Extra } from "../Controller/API"
import I18n from '../../assets/languages/i18n'
import { Actions } from 'react-native-router-flux'
import styles from './styles/Pile1_2.style'
import moment from "moment"
import DateTimePicker from '@react-native-community/datetimepicker';
class Pile1_2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 1,
      step: 2,
      steelCageNo: '',
      steelCages: [],
      Edit_Flag:1,
      status:0,
      location:{
        position:{
          lat:1,
          log:1,
        }
      },
      pile: [],
      Step1Status:0,
      datevisibletime:false,
      datevisibledate:false,
      startdate:'',
      enddate:'',
      type:'',
      datestart:'',
      timestart:'',
      dateend:'',
      timeend:'',
      set:false,
      rbsheet:''
    }
    this.prevState = this.state
  }
  componentWillMount() {
    // this.fetchSteelCage()
  }

  async componentWillReceiveProps(nextProps) {
    var pile = null
    var pile1_0 = null
    var pileMaster = null
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['1_2']) {
      pile = nextProps.pileAll[this.props.pileid]['1_2'].data
      pileMaster = nextProps.pileAll[this.props.pileid]['1_2'].masterInfo
      this.setState({Step1Status:nextProps.pileAll[this.props.pileid].step_status.Step1Status})
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['1_0']) {
      pile1_0 = nextProps.pileAll[this.props.pileid]['1_0'].data
    }
    if(nextProps.pileAll != undefined && nextProps.pileAll != '') {
      this.setState({location:nextProps.pileAll.currentLocation})
    }
    if (nextProps.item != null) {
       this.setState({pile:nextProps.item})
    }
    if(pileMaster &&pileMaster.rbsheet){
      console.warn('pileMaster.rbsheet',pileMaster.rbsheet)
      await this.setState({rbsheet:pileMaster.rbsheet})

    }
    if(pileMaster && pileMaster.Steelcage && pileMaster.Steelcage.steelcage_no!="" && this.state.steelCages.length == 0){
      await this.setState({steelCageNo:pileMaster.Steelcage.steelcage_no, steelCages:pileMaster.Steelcage.SectionList})
      // this.saveLocal('1_2')
    }
    // console.warn('pile',pile.SectionDetail)
    // console.warn('pileMaster',pileMaster.Steelcage.SectionList)
    if (pile && nextProps.hidden == false) {
      if(pile.SectionDetail != null && pile.SectionDetail != '' ){
        if (this.state.steelCages.length != pileMaster.Steelcage.SectionList.length) {
          this.setState({steelCages: pileMaster.Steelcage.SectionList})
        }else {
          var list = pile.SectionDetail
          if (pile.SectionDetail != pileMaster.Steelcage.SectionList) {
            for (var i = 0; i < pileMaster.Steelcage.SectionList.length; i++) {
              for (var j = 0; j < pile.SectionDetail.length; j++) {
                if(pile.SectionDetail[j].checksteelcage == undefined){
                  if (pile.SectionDetail[j].sectionId == pileMaster.Steelcage.SectionList[i].section_id) {
                    pileMaster.Steelcage.SectionList[i].checkconcretespacer = pile.SectionDetail[j].CheckConcretespacer
                    pileMaster.Steelcage.SectionList[i].checksteelcage = pile.SectionDetail[j].Checksteelcage
                    pileMaster.Steelcage.SectionList[i].image_concretespacer = pile.SectionDetail[j].ImageConcretespacer
                    pileMaster.Steelcage.SectionList[i].image_steelcage = pile.SectionDetail[j].ImageSteelcage
                  }
                }else{
                  if (pile.SectionDetail[j].section_id == pileMaster.Steelcage.SectionList[i].section_id) {
                    pileMaster.Steelcage.SectionList[i].checkconcretespacer = pile.SectionDetail[j].checkconcretespacer
                    pileMaster.Steelcage.SectionList[i].checksteelcage = pile.SectionDetail[j].checksteelcage
                    pileMaster.Steelcage.SectionList[i].image_concretespacer = pile.SectionDetail[j].image_concretespacer
                    pileMaster.Steelcage.SectionList[i].image_steelcage = pile.SectionDetail[j].image_steelcage
                  }
                }
              }
            }
            this.setState({steelCages: pileMaster.Steelcage.SectionList})
          }
        }
      }
    }
    if (pile.SectionDetail.length == 0) {
      for (var i = 0; i < pileMaster.Steelcage.SectionList.length; i++) {
        for (var j = 0; j < pile.SectionDetail.length; j++) {
          if (pile.SectionDetail[j].section_id == pileMaster.Steelcage.SectionList[i].section_id) {
            pileMaster.Steelcage.SectionList[i].checkconcretespacer = false
            pileMaster.Steelcage.SectionList[i].checksteelcage = false
            pileMaster.Steelcage.SectionList[i].image_concretespacer = []
            pileMaster.Steelcage.SectionList[i].image_steelcage = []
          }
        }
      }
      this.setState({steelCages: pileMaster.Steelcage.SectionList})
      
    }
    if(!this.state.set){
      if(this.props.startdatedefault != '' && this.props.startdatedefault != null){
        if(this.props.enddate != '' && this.props.enddate != null){
          this.setState({
            datestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
            timestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("HH:mm"),
            dateend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
            timeend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("HH:mm"),
            startdate:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
            enddate:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm")
          })
        }else{
          this.setState({
            datestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
            timestart:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("HH:mm"),
            dateend:'',
            timeend:'',
            startdate:moment(this.props.startdatedefault,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
            enddate:''
          })
        }
      }else{
        if(this.props.startdate != '' && this.props.startdate != null){
          this.setState({
            datestart:moment(this.props.startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
            timestart:moment(this.props.startdate,"DD/MM/YYYY HH:mm").format("HH:mm"),
            startdate:moment(this.props.startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
          })
        }
        if(this.props.enddate != '' && this.props.enddate != null){
          this.setState({
            dateend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
            timeend:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("HH:mm"),
            enddate:moment(this.props.enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm")
          })
        }else{
          this.setState({
            dateend:'',
            timeend:''
          })
        }
      }
      
    }
    if(nextProps.pileAll[this.props.pileid]['1_2'].startdate != null && nextProps.pileAll[this.props.pileid]['1_2'].startdate != ''){
      this.setState({
        datestart:moment(nextProps.pileAll[this.props.pileid]['1_2'].startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
        timestart:moment(nextProps.pileAll[this.props.pileid]['1_2'].startdate,"DD/MM/YYYY HH:mm").format("HH:mm"),
        startdate:moment(nextProps.pileAll[this.props.pileid]['1_2'].startdate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
      })
    }
    if(nextProps.pileAll[this.props.pileid]['1_2'].enddate != null && nextProps.pileAll[this.props.pileid]['1_2'].enddate != ''){
      this.setState({
        dateend:moment(nextProps.pileAll[this.props.pileid]['1_2'].enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY"),
        timeend:moment(nextProps.pileAll[this.props.pileid]['1_2'].enddate,"DD/MM/YYYY HH:mm").format("HH:mm"),
        enddate:moment(nextProps.pileAll[this.props.pileid]['1_2'].enddate,"DD/MM/YYYY HH:mm").format("DD/MM/YYYY HH:mm"),
      })
    }
    
      
    

    if (pile1_0) {
      // if (pile3_0.Edit_Flag != null && pile3_0.Edit_Flag != undefined && pile3_0.Edit_Flag != '') {
        this.setState({Edit_Flag:pile1_0.Edit_Flag})
      // }
    }
  }

  fetchSteelCage(){
    this.props.steelCageItems({pileid: this.props.pileid})
  }

  getState(){
    var data = {
      SectionDetail:this.state.steelCages
    }
    return data
  }

  async updateState(value){
    if (this.state.Step1Status == 0) {
      await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step1Status:1})
      this.props.mainPileGetStorage(this.props.pileid, "step_status")
    }
    if(value.data.sectionid>0){
      steelCages = this.state.steelCages
      for(var i in steelCages){
        if(value.data.sectionid==steelCages[i].section_id){
          steelCages[i].checkconcretespacer = value.data.checkconcretespacer
          steelCages[i].checksteelcage = value.data.checksteelcage
          steelCages[i].image_concretespacer = value.data.image_concretespacer
          steelCages[i].image_steelcage = value.data.image_steelcage
          steelCages[i].startdate = value.data.startdate
          steelCages[i].enddate = value.data.enddate
        }
      }
      
      await this.setState({steelCages,status:1})
      this.saveLocal('1_2')
    }else{
      await this.setState(value.data)
      this.saveLocal('1_2')
    }
  }
  async mixdate(){
    if( this.state.datestart != '' && this.state.timestart != ''){
      await this.setState({startdate:this.state.datestart+' '+this.state.timestart})
     }
     if(this.state.dateend != '' && this.state.timeend != ''){
       await this.setState({enddate:this.state.dateend+' '+this.state.timeend})
     }
  }
  _renderTable() {
    console.log('this.state.steelCages',this.state.steelCages)
    return this.state.steelCages.map((sec, index) => {
      return (
        <Table
          pile={this.state.pile}
          name={sec.section_no}
          se={sec.se}
          checksteelcage={sec.checksteelcage}
          checkconcretespacer={sec.checkconcretespacer}
          image_concretespacer={sec.image_concretespacer}
          image_steelcage={sec.image_steelcage}
          key={index}
          PhotoPress_concretespacer = { () => this.PhotoPress_concretespacer({sectionid:sec.section_id,checksteelcage:sec.checksteelcage,checkconcretespacer:sec.checkconcretespacer,image_steelcage:sec.image_steelcage,image_concretespacer:sec.image_concretespacer}) }
          PhotoPress_steelcage = { () => this.PhotoPress_steelcage({
            sectionid:sec.section_id,
            checksteelcage:sec.checksteelcage,
            checkconcretespacer:sec.checkconcretespacer,
            image_steelcage:sec.image_steelcage,
            image_concretespacer:sec.image_concretespacer}) }
          iconPressed={ () => this.iconPressed({sectionid:sec.section_id,checksteelcage:sec.checksteelcage,checkconcretespacer:sec.checkconcretespacer,image_steelcage:sec.image_steelcage,image_concretespacer:sec.image_concretespacer,se:sec.se,index:index}) } />
      )
    })
  }

  onCameraRoll(keyname, photos, type,sectionid,checksteelcage,checkconcretespacer,image_steelcage,image_concretespacer) {
    if (this.state.Edit_Flag == 1) {
      Actions.camearaRoll({
        jobid: this.props.jobid,
        process: this.state.process,
        step: this.state.step,
        pileId: this.props.pileid,
        keyname: keyname,
        photos: photos,
        typeViewPhoto: 'view',
        sectionid,
        checksteelcage,
        checkconcretespacer,
        image_steelcage,
        image_concretespacer,
        shape:this.props.shape,
        category:this.props.category,
        Edit_Flag:0
      })
    } else {
      Actions.camearaRoll({
        jobid: this.props.jobid,
        process: this.state.process,
        step: this.state.step,
        pileId: this.props.pileid,
        keyname: keyname,
        photos: photos,
        typeViewPhoto: 'view',
        sectionid,
        checksteelcage,
        checkconcretespacer,
        image_steelcage,
        image_concretespacer,
        shape:this.props.shape,
        category:this.props.category,
        Edit_Flag:0
      })
    }
  }

  PhotoPress_concretespacer({sectionid,checksteelcage,checkconcretespacer,image_steelcage,image_concretespacer}){
    this.onCameraRoll('image_concretespacer',image_concretespacer,'edit',sectionid,checksteelcage,checkconcretespacer,image_steelcage,image_concretespacer)
  }
  PhotoPress_steelcage({sectionid,checksteelcage,checkconcretespacer,image_steelcage,image_concretespacer}){
    this.onCameraRoll(
      'image_steelcage',
      image_steelcage,
      'edit',
      sectionid,
      checksteelcage,
      checkconcretespacer,
      image_steelcage,
      image_concretespacer
    )
  }

  iconPressed({sectionid,checksteelcage,checkconcretespacer,image_steelcage,image_concretespacer,se,index}) {
    // console.log('statesteelcage',this.state.steelCages)
    Actions.steeldetail({
      process: this.state.process,
      step: this.state.step,
      title: I18n.t('mainpile.steeldetail.title'),
      pileid: this.props.pileid,
      sectionid: sectionid,
      checksteelcage: checksteelcage,
      checkconcretespacer: checkconcretespacer,
      image_steelcage: image_steelcage,
      image_concretespacer: image_concretespacer,
      jobid: this.props.jobid,
      Edit_Flag:this.state.Edit_Flag,
      lat:this.state.location == undefined ? 1 : this.state.location.position.lat ,
      log:this.state.location == undefined ? 1 : this.state.location.position.log ,
      shape:this.props.shape,
      concretedisabled: this.props.shape == 1 ? false : true,
      se:se,
      index:index,
      category: this.props.category,
      startdate: this.props.startdate,
      enddate: this.props.enddate,
      status:this.state.Step1Status,
      checkbox:this.state.Step1Status==2?0:this.state.Edit_Flag
    })

  }
  async saveLocal(process_step){
    await this.mixdate()
    var value = {
      process:1,
      step:2,
      pile_id: this.props.pileid,
      data:{
        SectionDetail:this.state.steelCages,
        startdate:this.state.steelCages.startdate,
        enddate: this.state.steelCages.enddate
      },
      startdate:this.state.startdate,
      enddate:this.state.enddate
    }
    // console.log(value)
    await this.props.mainPileSetStorage(this.props.pileid,process_step,value)
  }
  onSelectDate(date){
    let date2 = moment(date).format("DD/MM/YYYY")
    if(this.state.type=='start'){
      this.setState({ datestart: date2 ,set:true,datevisibledate: false})
      this.saveLocal('1_2')
    }else if(this.state.type=='end'){
      this.setState({ dateend: date2 ,set:true,datevisibledate: false})
      this.saveLocal('1_2')
    }
  }
  onSelectTime(date){
    let date2 = moment(date).format("HH:mm")
    if(this.state.type=='start'){
      this.setState({ timestart: date2 ,set:true,datevisibletime: false})
      this.saveLocal('1_2')
    }else if(this.state.type=='end'){
      this.setState({ timeend: date2 ,set:true,datevisibletime: false})
      this.saveLocal('1_2')
    }
  }
  getDate(){
    if(this.state.type == 'start'){
      if(this.state.startdate != ''){
        return new Date(moment(this.state.startdate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        if(this.props.startdate != '' && this.props.startdate != undefined && this.props.startdate != null){
          return new Date(moment(this.props.startdate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
        }else{
          return new Date()
        }
      }
    }else{
      if(this.state.enddate != ''){
        return new Date(moment(this.state.enddate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        if(this.props.enddate != '' && this.props.enddate != undefined && this.props.enddate != null){
          return new Date(moment(this.props.enddate,'DD/MM/YYYY HH:mm').format('MM/DD/YYYY HH:mm:ss'))
        }else{
          return new Date()
        }
      }
    }
  }
  onChangedate = (event, selectedDate) => {
    if(event.type == 'set'){
      this.onSelectDate(selectedDate)
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };
  onChangetime = (event, selectedDate) => {
    if(event.type == 'set'){
      this.onSelectTime(selectedDate)
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };
  render() {
    if (this.props.hidden) {
      return null
    }
    // console.log("Pile1_2 > render")
    return (
      <View>
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t('mainpile.1_2.information')}</Text>
        </View>
          <View style={styles.subTopic}>
            <View style={{ width: "40%", justifyContent: "center" }}>
              <Text style={{fontWeight: 'bold'}}>{I18n.t('mainpile.1_2.drawingno')}</Text>
            </View>
            <View style={{ width: "40%", backgroundColor: MENU_GREY, justifyContent: 'center' }}>
              <Text textBreakStrategy={'balanced'} numberOfLine={1} style={{marginLeft: 10}}>{this.state.steelCageNo || '-'}</Text>
            </View>
            <TouchableOpacity style={[
              { width: "15%", backgroundColor: "#6bacd5", justifyContent: 'center',marginLeft: 10 ,borderRadius:3},
              this.state.rbsheet==''&&{backgroundColor:'grey'}]} 
            onPress={() =>{
                        if(this.state.rbsheet=='')return
                        console.log("rbsheet: " + this.state.rbsheet)
                        Linking.openURL(this.state.rbsheet)
                        // Linking.canOpenURL(this.state.rbsheet).then(supported => {
                        //   if (supported) {
                        //     Linking.openURL(this.state.rbsheet)
                        //   } else {
                        //     console.log("Don't know how to open URI: " + this.state.rbsheet)
                        //   }
                        // })
                      }}>
              <Icon name="file-text" type='font-awesome' color='#fff' size={20}/>
            </TouchableOpacity>
          </View>
        <View>
          <ScrollView style={{ marginTop: 20 }}>
            {this._renderTable()}
          </ScrollView>
        </View>
        {this.state.datevisibledate &&<DateTimePicker
          value={this.getDate()}
          isVisible={this.state.datevisibledate}
          is24Hour={true}
          display='spinner'
          mode='date'
          onChange={this.onChangedate}
        />}
        {this.state.datevisibletime && <DateTimePicker
          value={this.getDate()}
          isVisible={this.state.datevisibletime}
          is24Hour={true}
          display='spinner'
          mode='time'
          onChange={this.onChangetime}
        />}
        {/*testtttttttt*/}
        <View style={[styles.box ,{marginBottom:5}]}>
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t('startenddate.startenddate')}{I18n.t('mainpile.step.1_1')}</Text>
        </View>
        <View>
        <View style={[styles.imageDetail,{marginLeft:20,marginRight:20,marginBottom:15}]}>
        <Text style={{width:'25%'}}>{I18n.t('startenddate.start')}</Text>
          <TouchableOpacity
            style={[styles.buttonBox, { width: "35%" }]}
            onPress={() => {
              this.setState({datevisibledate:true,type:'start'})
            }}
            disabled={this.state.Edit_Flag == 0}
          >
            <Text>{this.state.datestart}</Text>
          </TouchableOpacity>
          <Text style={{width:'10%',textAlign:'center'}}>{I18n.t('startenddate.time')}</Text>
          <TouchableOpacity
            style={[styles.buttonBox, { width: "30%" }]}
            onPress={() => {
              this.setState({datevisibletime:true,type:'start'})
            }}
            disabled={this.state.Edit_Flag == 0}
          >
            <Text>{this.state.timestart}</Text>
          </TouchableOpacity>
          {/*<TouchableOpacity
            style={styles.approveBox}
            onPress={() => this.onnowdate('start')}
            disabled={this.state.Edit_Flag == 0}
          >
            <Text style={{color:'#fff'}}>เริ่ม</Text>
          </TouchableOpacity>*/}
        </View>
        <View style={[styles.imageDetail,{marginLeft:20,marginRight:20,marginBottom:15}]}>
        <Text style={{width:'25%'}}>{I18n.t('startenddate.end')}</Text>
        <TouchableOpacity
            style={[styles.buttonBox, { width: "35%" }]}
            onPress={() => {
              this.setState({datevisibledate:true,type:'end'})
            }}
            disabled={this.state.Edit_Flag == 0}
          >
            <Text>{this.state.dateend}</Text>
          </TouchableOpacity>
          <Text style={{width:'10%',textAlign:'center'}}>{I18n.t('startenddate.time')}</Text>
          <TouchableOpacity
            style={[styles.buttonBox, { width: "30%" }]}
            onPress={() => {
              this.setState({datevisibletime:true,type:'end'})
            }}
            disabled={this.state.Edit_Flag == 0}
          >
            <Text>{this.state.timeend}</Text>
          </TouchableOpacity>
          {/*<TouchableOpacity
            style={styles.approveBox}
            onPress={() => this.onnowdate('end')}
            disabled={this.state.Edit_Flag == 0}
          >
            <Text style={{color:'#fff'}}>สิ้นสุด</Text>
          </TouchableOpacity>*/}
        </View>
      </View>
      </View>
      {/*testtttttttt*/}
      </View>
    )
  }
}

class Table extends Component {
  render() {
    return (
      <View style={{flexDirection: 'row', height: 70, borderTopWidth: 0.3, borderBottomWidth: 0.2}}>
        <View style={{width: '25%', borderRightWidth: 0.2, justifyContent: 'center'}}>
          {this.props.pile.shape != 2?<Text style={{textAlign:'center'}}>{this.props.name}</Text>
          :
          this.props.se == '' || this.props.se == null?<Text style={{textAlign:'center'}}>{this.props.name}</Text>
          :
          <Text style={{textAlign:'center'}}>{this.props.name} ({this.props.se})</Text>
          }
        </View>
        <View style={{width: '30%', borderRightWidth: 0.2, alignItems: 'center'}}>
          <Text style={{fontSize: 12, marginTop: 2}}>{I18n.t('mainpile.1_2.steelcage')}</Text>
          <View style={{flexDirection: 'row'}}>
            { this.props.checksteelcage ?
              <Icon name='check' reverse color='#6dcc64' size={15} />
              :
              <Icon name='check' reverse color='grey' size={15} />
            }
            <TouchableOpacity style={styles.button} onPress={() => this.props.PhotoPress_steelcage()}>
            { this.props.image_steelcage && this.props.image_steelcage.length>0 ?
              <Icon name='image' type='font-awesome' reverse color='#6dcc64' size={15} />
              :
              <Icon name='image' type='font-awesome' reverse color='grey' size={15} />
            }
            </TouchableOpacity>
          </View>
        </View>
        {
          this.props.pile.shape == 1 ?
          <View style={{width: '30%', borderRightWidth: 0.2, alignItems: 'center'}}>
            <Text style={{fontSize: 12, marginTop: 2}}>{I18n.t('mainpile.1_2.concrete')}</Text>
            <View style={{flexDirection: 'row'}}>
            { this.props.checkconcretespacer ?
              <Icon name='check' reverse color='#6dcc64' size={15} />
              :
              <Icon name='check' reverse color='grey' size={15} />
            }
            <TouchableOpacity style={styles.button} onPress={() => this.props.PhotoPress_concretespacer()}>
            { this.props.image_concretespacer && this.props.image_concretespacer.length>0 ?
              <Icon name='image' type='font-awesome' reverse color='#6dcc64' size={15} />
              :
              <Icon name='image' type='font-awesome' reverse color='grey' size={15} />
            }
            </TouchableOpacity>
            </View>
          </View>
          :
          <View
            style={[
              { width: "25%", borderRightWidth: 0.2, alignItems: "center", backgroundColor: "#CDD2D2" },
            ]}
          >
            <Text style={{ fontSize: 12, marginTop: 2 }}>{I18n.t("mainpile.7_3.concrete")}</Text>
            <View style={{ flexDirection: "row" }}>
              {this.props.checkconcretespacer ? (
                <Icon name="check" reverse color="#6dcc64" size={15} />
              ) : (
                <Icon name="check" reverse color="grey" size={15} />
              )}
              <TouchableOpacity
                style={styles.button}
                onPress={() => this.props.PhotoPress_concretespacer()}
                disabled={true}
              >
                {this.props.image_concretespacer && this.props.image_concretespacer.length > 0 ? (
                  <Icon name="image" type="font-awesome" reverse color="#6dcc64" size={15} />
                ) : (
                  <Icon name="image" type="font-awesome" reverse color="grey" size={15} />
                )}
              </TouchableOpacity>
            </View>
          </View>
        }
        <View style={{width: '15%', alignItems: 'center', justifyContent: 'center'}}>
          <Icon name="file-text" type='font-awesome' reverse color='#6bacd5' size={20} onPress={ () => this.props.iconPressed() } iconPressed={this.props.iconPressed} />
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    item: state.pile.item,
    pileAll: state.mainpile.pileAll || null,
  }
}

export default connect(mapStateToProps, actions, null, { withRef: true })(Pile1_2)
