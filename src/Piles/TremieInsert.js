import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  BackHandler
} from "react-native"
import { connect } from "react-redux"
import { Container, Content } from "native-base"
import { Actions } from "react-native-router-flux"
import TableView from "../Components/TableView"
import { MAIN_COLOR, SUB_COLOR, BASIC_COLOR, MENU_GREY_ITEM , DEFAULT_COLOR_1, DEFAULT_COLOR_4} from "../Constants/Color"
import { Icon } from "react-native-elements"
import I18n from "../../assets/languages/i18n"
import ModalSelector from "react-native-modal-selector"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import styles from "./styles/TremieInsert.style"
import * as actions from "../Actions"


class TremieInsert extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tremiedata: [{ key: 0, section: true, label: I18n.t("mainpile.tremieinsert.tremieselect") }],
      tremieimage: [],
      last: false,
      tremie: "",
      tremieid: "",
      index: 1,
      tremielength: "",
      locktremie: false,
      status10:0,
      checksavebutton: false,
      TremieList:null
    }
  }

  componentWillReceiveProps(nextProps) {
    var pile = null
    var pile4_0 = null
    var pileMaster = null
    
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["4_1"]) {
      pile = nextProps.pileAll[this.props.pileid]["4_1"].data
      pileMaster = nextProps.pileAll[this.props.pileid]["4_1"].masterInfo
      // this.setState({ dataMaster: pileMaster })
      // let editcheck10 = nextProps.pileAll[this.props.pileid]
      // console.warn(editcheck10)
      // console.warn("test")
      // if(editcheck10 != undefined){
      //   this.setState({status10:editcheck10.step_status.Step8Status})
      // }
    }
    

    if(nextProps.action == "update") {
      if(nextProps.value.data != undefined) {
        if(nextProps.value.data.tremieimage != undefined) {
          this.setState({ tremieimage: nextProps.value.data.tremieimage })
        }
      }
    }

    // if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["4_0"]) {
    //   pile4_0 = nextProps.pileAll[this.props.pileid]["4_0"].data
    // }
    // if (nextProps.pileAll != undefined && nextProps.pileAll != "") {
    //   this.setState({ location: nextProps.pileAll.currentLocation })
    // }
  }
  _backButton() {
    if(Actions.currentScene == 'tremieinsert') {
      Actions.pop()
      return true
    }
    return false
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("tiback", () => this._backButton())
  }
  componentDidMount() {
    BackHandler.addEventListener("tiback", () => this._backButton())

    var pile = null
    var pileMaster = null
    if(this.props.pileAll[this.props.pileid] && this.props.pileAll[this.props.pileid]["8_2"]) {
      pile = this.props.pileAll[this.props.pileid]["8_2"].data
      pileMaster = this.props.pileAll[this.props.pileid]["8_2"].masterInfo
      let editcheck10 = this.props.pileAll[this.props.pileid]
      if(editcheck10 != undefined){
        this.setState({status10:editcheck10.step_status.Step10Status},()=>{
          console.warn(this.state.status10)
        })

      }
    }

    if(pileMaster) {
      if(pileMaster.TremieSize) {
        let tremie = [{ key: 0, section: true, label: I18n.t("mainpile.tremieinsert.tremieselect") }]
        pileMaster.TremieSize.map((data, index) => {
          console.log(data)
          tremie.push({ key: index + 1, label: data.Name, id: data.Id })
          if(data.IsDefault && !this.props.tremieid) {
            this.setState({ tremie: data.Name, tremieid: data.Id })
          }
          if(this.props.tremieid && data.Id == this.props.tremieid) {
            console.log(data.Name, data.Id)
            this.setState({ tremie: data.Name, tremieid: data.Id, locktremie: true })
          }
        })
        this.setState({ tremiedata: tremie })
      }
    }

    if(this.props.TremieList != undefined) {
      var tremiesize = this.props.TremieList.TremieSize || []
      // console.warn("tremiesize",this.props.TremieList)
      this.setState({
        tremieimage: this.props.TremieList.Images,
        tremie: tremiesize.Name,
        tremieid: tremiesize.Id,
        tremielength: this.props.TremieList.Length,
        last: this.props.TremieList.IsLast,
        index: this.props.index,
        TremieList:this.props.TremieList
      })
    }
  }

  closeButton() {
    Actions.pop()
  }
  ConTwoDecDigit=(digit)=>{
    return digit.indexOf(".")>0?
            digit.split(".").length>=2?
             digit.split(".")[0]+"."+digit.split(".")[1].substring(-1,2)
            : digit
           : digit
  }

  onChangeTremieLength(length) {
    if(this.state.status10!=0){
      if(this.props.shape==1){
        Alert.alert('', I18n.t('mainpile.tremieinsert.warnshape1'), [
          {
            text: 'OK',
          }
          ])
      }else{
        Alert.alert('', I18n.t('mainpile.tremieinsert.warnshape2'), [
          {
            text: 'OK',
          }
          ])
      }
    }else{
      // let  tremie =  parseFloat(Math.abs(this.state.tremielength)).toFixed(2)
      this.setState({ tremielength: this.ConTwoDecDigit(length) })
    }
    
  }

  onCameraRoll(keyname, photos, type) {
    if(this.props.Edit_Flag == 1) {
      Actions.camearaRoll({
        jobid: this.props.jobid,
        process: this.props.process,
        step: this.props.step,
        pileId: this.props.pileid,
        keyname: keyname,
        photos: photos,
        typeViewPhoto: type,
        shape: this.props.shape,
        category: this.props.category,
        Edit_Flag:this.state.Edit_Flag
      })
    } else {
      Actions.camearaRoll({
        jobid: this.props.jobid,
        process: this.props.process,
        step: this.props.step,
        pileId: this.props.pileid,
        keyname: keyname,
        photos: photos,
        typeViewPhoto: "view",
        shape: this.props.shape,
        category: this.props.category,
        Edit_Flag:this.state.Edit_Flag
      })
    }
  }
  
  async saveButton() {
    // console.warn("this.state.tremielength",this.state.tremielength)

    if(this.props.edit && this.state.last && !this.props.allowlast && !this.props.TremieList.last) {
      this.setState({ checksavebutton: true },()=>{
        Alert.alert("", I18n.t('mainpile.tremieinsert.cannotselectlast'), [
          {
            text: "OK"
          }
        ])
       
      })
      return
    }
    if(this.state.tremielength==null || this.state.tremielength==""||this.state.tremielength<=0){
      
      this.setState({ checksavebutton: true },()=>{
        Alert.alert("", I18n.t('mainpile.tremieinsert.error1'), [
          {
            text: "OK"
          }
        ])
        
      })
      return
    }
    setTimeout(() => {
      this.setState({ checksavebutton: false })
    }, 2000)
          Actions.pop({
            refresh: {
              action: "update",
              value: {
                process: this.props.process,
                step: this.props.step,
                data: {
                  image: this.state.tremieimage,
                  tremiesize: this.state.tremie,
                  tremieid: this.state.tremieid,
                  length: this.state.tremielength,
                  last: this.state.last,
                  edit: this.props.edit,
                  index: this.props.index,
                  startdate: this.state.startdate,
                  enddate: this.state.enddate
                }
              }
            }
          })
          // Actions.pop()
          this.save()
  }

  save() {
    var valueApi = {
      Language: I18n.locale,
      JobId: this.props.jobid,
      PileId: this.props.pileid,
      Page: 2,
      No: this.props.index,
      latitude: this.props.lat,
      longitude: this.props.log,
      sizeid: this.state.tremieid,
      sizename: this.state.tremie,
      length: parseFloat(this.state.tremielength),
      islast: this.state.last,
      image: this.state.tremieimage,
    }
    if (this.props.edit==true&&this.state.TremieList!=null&&this.state.TremieList.Id!=null){
      // valueApi.
      // console.warn("SAVE TREMIE ", this.state.TremieList.Id)
      valueApi.TremieId = this.state.TremieList.Id
    }
    // console.log("SAVE TREMIE ", valueApi)
    this.props.pileSaveTremieInsert(valueApi)
    
  }
  
  render() {
    return (
      <View style={styles.listContainer}>
        <View style={
          this.props.category == 3|| this.props.category == 5 ? [styles.listNavigation,{backgroundColor: DEFAULT_COLOR_4}]:this.props.shape == 1 ? styles.listNavigation : [styles.listNavigation, {backgroundColor:DEFAULT_COLOR_1}] 
        }>
          <Text style={styles.listNavigationText}>{I18n.t('mainpile.tremieinsert.savetremie')}</Text>
        </View>
        <View style={styles.listNavigationStep}>
          <Text style={styles.listNavigationTextStep}>{I18n.t('mainpile.tremieinsert.tremieno')} {this.props.index.toString()}</Text>
        </View>
        <KeyboardAwareScrollView scrollEnabled={true} resetScrollToCoords={{ x: 0, y: 0 }} enableOnAndroid={true}>
          <View style={styles.listRow}>
            { /** <View style={{ marginTop: 10 }}>
              <Text style={styles.buttonText}>รวมความยาวท่อเทรมี่</Text>
            </View>
            */ }
            <View style={styles.listTopicSub}>
              <Text style={styles.listTopicSubText}>{I18n.t('mainpile.tremieinsert.sizetremie')}</Text>
              <View style={styles.listSelectedView}>
                <ModalSelector
                  data={this.state.tremiedata}
                  onChange={data => this.setState({ tremie: data.label, tremieid: data.id })}
                  selectTextStyle={styles.textButton}
                  cancelText="Cancel"
                  disabled={this.state.locktremie||this.props.Edit_Flag== 0}
                >
                  <TouchableOpacity style={[styles.button, this.state.locktremie||this.props.Edit_Flag== 0 && { borderColor: MENU_GREY_ITEM }]}>
                    <View style={styles.buttonTextStyle}>
                      <Text style={[styles.buttonText, this.state.locktremie ||this.props.Edit_Flag== 0&& { color: MENU_GREY_ITEM }]}>
                        {this.state.tremie || I18n.t("mainpile.tremieinsert.tremieselect")}
                      </Text>
                      <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                        <Icon name="arrow-drop-down" type="MaterialIcons" color={this.state.locktremie||this.props.Edit_Flag== 0 ? MENU_GREY_ITEM : "#007CC2"} />
                      </View>
                    </View>
                  </TouchableOpacity>
                </ModalSelector>
              </View>
            </View>

            <View style={styles.listTopicSub}>
              <Text style={styles.listTopicSubText}>{I18n.t("mainpile.tremieinsert.length")}</Text>
              <TextInput
                value={this.state.tremielength.toString()}
                editable={this.props.Edit_Flag== 0 ? false : true}
                keyboardType="numeric"
                underlineColorAndroid="transparent"
                style={[styles.listTextbox,this.state.Edit_Flag == 0 && { borderColor: MENU_GREY_ITEM ,color:MENU_GREY_ITEM}]}
                onChangeText={length => this.onChangeTremieLength(length)}
                onEndEditing={()=>{
                 let  tremie =  parseFloat(Math.abs(this.state.tremielength)).toFixed(2)
                //  console.warn(isNaN(tremie))
                  this.setState(
                    {
                      tremielength: this.state.tremielength 
                        ? (isNaN(tremie) == true ? "":parseFloat(Math.abs(this.state.tremielength)).toFixed(2))
                        : "",
                      visible: false
                    })}}
                    
                
                  
              />
            </View>

            <View style={styles.box}>
              <View style={{ marginTop: 20, width: "100%" }}>
                <View style={{ alignItems: "center" }}>
                  <TouchableOpacity
                    style={[styles.image,this.state.tremieimage.length>0?{backgroundColor:'#6dcc64'}:{}]}
                    onPress={() => this.onCameraRoll("tremieimage", this.state.tremieimage, "edit")}
                  >
                    <Icon name="image" type="entypo" color="#FFF" />
                    <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={styles.lastView}>
              {this.state.last == true ? (
                <Icon
                  name="check"
                  reverse
                  color="#6dcc64"
                  size={15}
                  disabled={this.props.Edit_Flag== 0}
                  onPress={() => {
                    if(this.state.status10!=0){
                        Alert.alert('', this.props.shape==1?I18n.t('mainpile.tremieinsert.lasttremiewarn'):I18n.t('mainpile.tremieinsert.lasttremiewarn_dw'), [
                          {
                            text: 'OK',
                          }
                          ])
                    }else{
                      this.setState({ last: !this.state.last })
                    }
                  }
                }
                />
              ) : (
                  <Icon
                    name="check"
                    reverse
                    color="grey"
                    size={15}
                    disabled={this.props.Edit_Flag== 0}
                    onPress={() => {
                      if(this.state.status10!=0){
                        Alert.alert('', this.props.shape==1?I18n.t('mainpile.tremieinsert.lasttremiewarn'):I18n.t('mainpile.tremieinsert.lasttremiewarn_dw'), [
                          {
                            text: 'OK',
                          }
                        ])
                        }else{
                          this.setState({ last: !this.state.last })
                        }
                      }
                    }
                  />
                )}
              <Text>{I18n.t("mainpile.tremieinsert.last")}</Text>
            </View>
            
            
          </View>
        </KeyboardAwareScrollView>

        {/* Bottom */}
        <View style={styles.buttonFixed}>
          <View style={styles.second}>
            <TouchableOpacity style={styles.firstButton} onPress={() => this.closeButton()}>
              <Text style={styles.selectButton}>{I18n.t("mainpile.steeldetail.button.close")}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.secondButton, this.props.Edit_Flag == 0 && { backgroundColor: MENU_GREY_ITEM }]} onPress={() => {this.setState({checksavebutton:true},()=>{this.saveButton()})}}  disabled={this.state.checksavebutton ||this.props.Edit_Flag== 0}>
              <Text style={styles.selectButton}>{I18n.t("mainpile.steeldetail.button.save")}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll || null
  }
}
export default connect(mapStateToProps, actions)(TremieInsert)
