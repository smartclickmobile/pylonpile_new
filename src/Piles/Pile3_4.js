import React, { Component } from "react"
import { View, Text, StyleSheet, TouchableOpacity, TextInput, Alert } from "react-native"
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM, MENU_GREY_BORDER, MENU_BACKGROUND } from "../Constants/Color"
import ModalSelector from "react-native-modal-selector"
import { Icon } from "react-native-elements"
import I18n from "../../assets/languages/i18n"
import { Extra } from "../Controller/API"
import TableView from "../Components/TableView"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"
import styles from "./styles/Pile3_4.style"
import moment from 'moment'
var index_page = 0
class Pile3_4 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 3,
      step: 4,
      length: "15",
      approve_user: [{ key: 0, section: true, label: '' }],
      select_user: "",
      approve: false,
      approveBy: "",
      cancelapprove:0,
      pile: [],
      top: "",
      ground: "",
      northPosition: "",
      eastPosition: "",
      northCal: "",
      eastCal: "",
      northCalBy: "",
      eastCalBy: "",
      constant: "",
      Image_ReadingCoordinate: [],
      Image_SurveyDiff:[],
      survey: "",
      benchmarkCamLabel: "",
      benchmarkCam: "",
      benchmarkFlag: "",
      benchmarkFlagLabel: "",
      resection: false,
      ApprovedUserId: "",
      canApprove: true,
      submit: false,
      Edit_Flag:1,
      approve_error:null,
      pile3_4:[],
      location: {
        position: {
          lat: 1,
          log: 1
        }
      },
      flagaccept:false,
      status5:0,
      status3:0,
      status1:0,
      status2:0,
      flagApprove: false,
      setflagApprove:false,
      pile3_2:[],

      approveagian:false,
      check:true,

      ApprovedDate:null
    }
  }
  async componentWillReceiveProps(nextProps) {
    var pile3_1 = null
    var pileMaster3_1 = null
    var pile3_2 = null
    var pileMaster3_2 = null
    var pile3_3 = null
    var pile3_4 = null
    var pileMaster3_4 = null
    var pile3_0 = null
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['3_1']) {
      pile3_1 = nextProps.pileAll[this.props.pileid]['3_1'].data
      pileMaster3_1 = nextProps.pileAll[this.props.pileid]['3_1'].masterInfo
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['3_2']) {
      pile3_2 = nextProps.pileAll[this.props.pileid]['3_2'].data
      pileMaster3_2 = nextProps.pileAll[this.props.pileid]['3_2'].masterInfo
      this.setState({pile3_2:nextProps.pileAll[this.props.pileid]['3_2']})
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['3_3']) {
      pile3_3 = nextProps.pileAll[this.props.pileid]['3_3'].data
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['3_4']) {
      pile3_4 = nextProps.pileAll[this.props.pileid]['3_4'].data
      pileMaster3_4 = nextProps.pileAll[this.props.pileid]['3_4'].masterInfo
      this.setState({pile3_4:pile3_4})
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['3_0']) {
      pile3_0 = nextProps.pileAll[this.props.pileid]['3_0'].data
      this.setState({
        status5:nextProps.pileAll[this.props.pileid].step_status.Step5Status,
        status3:nextProps.pileAll[this.props.pileid].step_status.Step3Status,
        status2:nextProps.pileAll[this.props.pileid].step_status.Step2Status,
        status1:nextProps.pileAll[this.props.pileid].step_status.Step1Status
      })
    }

    if (nextProps.pileAll != undefined && nextProps.pileAll != '') {
      this.setState({location: nextProps.pileAll.currentLocation})
    }
    if (pile3_0) {
      var Edit_Flag = pile3_0.Edit_Flag
      if (Edit_Flag != this.state.approve) {
        if (this.state.approve == false) {
          this.setState({approve:pile3_0.Edit_Flag == 1 ? false : true})
        }
      }
      this.setState({Edit_Flag:pile3_0.Edit_Flag})
    }

    if (nextProps.item != null) {
      await this.setState({pile:nextProps.item})
    }
    if (pileMaster3_4.ApprovedUser.length > 0 && this.state.approve_user.length == 1) {
      this.setState({approve_user:[{ key: 0, section: true, label:nextProps.pileAll[this.props.pileid]['3_2'].flagaccept == 1||(nextProps.pileAll[this.props.pileid].step_status.Step5Status != 0 && nextProps.pileAll[this.props.pileid].step_status.Step3Status != 2)?I18n.t('mainpile.3_4.namelist'):I18n.t('mainpile.SlumpApprove.approvelist')}]})
      for (var i = 0; i < pileMaster3_4.ApprovedUser.length; i++) {
        this.state.approve_user.push({
          key: pileMaster3_4.ApprovedUser[i].employee_id,
          label: pileMaster3_4.ApprovedUser[i].nickname + "-" + pileMaster3_4.ApprovedUser[i].firstname + " " + pileMaster3_4.ApprovedUser[i].lastname
        })
      }
    }
    if (pileMaster3_1.SurveyList.length > 0  && nextProps.hidden == false) {
      for (var i = 0; i < pileMaster3_1.SurveyList.length; i++) {
        if (pile3_1) {
          if (pileMaster3_1.SurveyList[i].employee_id == pile3_1.SurveyorId) {
              await this.setState({survey: pileMaster3_1.SurveyList[i].firstname +" "+pileMaster3_1.SurveyList[i].lastname,})
          }else if(pile3_1.SurveyorId == null) {
              await this.setState({survey: this.state.survey,})
          }
        }
      }
    }
    if (pileMaster3_1.BenchmarkList.length > 0 && nextProps.hidden == false) {
      for (var i = 0; i < pileMaster3_1.BenchmarkList.length; i++) {
        if (pile3_1) {
          if( pileMaster3_1.BenchmarkList[i].id == pile3_1.LocationId) {
              await this.setState({benchmarkCam:pileMaster3_1.BenchmarkList[i].no,
                benchmarkCamLabel: parseFloat(pileMaster3_1.BenchmarkList[i].utm_n).toFixed(4) + ', '+ parseFloat(pileMaster3_1.BenchmarkList[i].utm_e).toFixed(4),})
          }else if(pile3_1.LocationId == null) {
              await this.setState({benchmarkCamLabel: this.state.benchmarkCamLabel,benchmarkCam:this.state.benchmarkCam})
          }
        }
      }
    }
    if (pileMaster3_1.BenchmarkList.length > 0 && nextProps.hidden == false) {
      for (var i = 0; i < pileMaster3_1.BenchmarkList.length; i++) {
        if (pile3_1) {
          if(pileMaster3_1.BenchmarkList[i].id == pile3_1.BSFlagLocationId) {
              await this.setState({benchmarkFlag:pileMaster3_1.BenchmarkList[i].no,
                benchmarkFlagLabel: parseFloat(pileMaster3_1.BenchmarkList[i].utm_n).toFixed(4) + ', '+ parseFloat(pileMaster3_1.BenchmarkList[i].utm_e).toFixed(4),})
          }else if(pile3_1.BSFlagLocationId == null) {
              await this.setState({benchmarkFlagLabel: this.state.benchmarkFlagLabel,benchmarkFlag:this.state.benchmarkFlag})
          }
        }
      }
    }


    if (pile3_1) {
      if( pile3_1.survey ){
        if (pile3_1.survey != null) {
          await this.setState({survey: pile3_1.survey})
        }
      }
      if(pile3_1.resection){
        await this.setState({resection: pile3_1.resection == true ? 'Yes' : 'No' })
      }else {
        await this.setState({resection: pile3_1.resection == true ? 'Yes' : 'No' })
      }

      if(pile3_1.benchmarkCamLabel){
        if (this.state.benchmarkCamLabel == '') {
          await this.setState({benchmarkCamLabel: pile3_1.benchmarkCamLabel ,benchmarkCam:pileMaster3_1.benchmarkCam,})
        }
      }
      if(pile3_1.benchmarkFlagLabel){
        if (this.state.benchmarkFlagLabel == '') {
          await this.setState({benchmarkFlagLabel: pile3_1.benchmarkFlagLabel,benchmarkFlag:pileMaster3_1.benchmarkFlag})
        }
      }
    }
    if (pile3_2) {
      
      if(pile3_2.eastPosition != this.state.eastPosition && pile3_2.eastPosition !=null){
        await this.setState({eastPosition: pile3_2.eastPosition})
      }else{
        if (pileMaster3_2.CasingPosition != null && this.state.eastPosition !== pileMaster3_2.CasingPosition.easting && pileMaster3_2.CasingPosition.easting != undefined) {
          await this.setState({
            eastPosition: pileMaster3_2.CasingPosition.easting,
          })
        }
      }
      if(pile3_2.flagaccept != undefined && this.state.setflagApprove == false){
        // console.warn('rrr')
        await this.setState({flagaccept: pile3_2.flagaccept, flagApprove:pile3_2.flagApprove})
      }
      if(pile3_2.eastCal != this.state.eastCal && pile3_2.eastCal != '' && isNaN(this.state.eastCal) == true){
        if (this.state.eastCal == '') {
          await this.setState({eastCal: pile3_2.eastCal == "" ? "" : parseFloat(pile3_2.eastCal).toFixed(4)})
        }
      }
      if(pile3_2.eastCalBy != this.state.eastCalBy){
        if (pile3_2.eastCalBy != null) {
          await this.setState({eastCalBy: pile3_2.eastCalBy})
          await this.onCal( pile3_2.eastCalBy.toString(),'east')
          index_page=0
        }
      }
      
      if(pile3_2.northPosition != this.state.northPosition && pile3_2.northPosition !=null){
        if (this.state.northPosition == '') {
          await this.setState({northPosition: pile3_2.northPosition})
        }
      }else {
        if (pileMaster3_2.CasingPosition != null && this.state.northPosition !== pileMaster3_2.CasingPosition.northing) {
          await this.setState({
            northPosition: pileMaster3_2.CasingPosition.northing
          })
        }
      }
      // console.warn('pile3_2.northCal',pile3_2.northCal,'pile3_2.northCalBy',pile3_2.northCalBy)
      if(pile3_2.northCal != this.state.northCal  && pile3_2.northCal != '' && isNaN(this.state.northCal) == true){
        if (this.state.northCal == '') {
          await this.setState({northCal: pile3_2.northCal == "" ? "" : parseFloat(pile3_2.northCal).toFixed(4)})
        }
      }
      if(pile3_2.northCalBy != this.state.northCalBy){
        if (pile3_2.northCalBy != null) {
          await this.setState({northCalBy: pile3_2.northCalBy})
          await this.onCal( pile3_2.northCalBy.toString(),'north')
        }
      }
      if(pile3_2.Image_ReadingCoordinate != null && pile3_2.Image_ReadingCoordinate != '' && pile3_2.Image_ReadingCoordinate != this.state.Image_ReadingCoordinate){
        if (pile3_2.Image_ReadingCoordinate != this.state.Image_ReadingCoordinate) {
            await this.setState({Image_ReadingCoordinate: pile3_2.Image_ReadingCoordinate})
        }
      }
      if(pile3_2.Image_SurveyDiff != null && pile3_2.Image_SurveyDiff != '' && pile3_2.Image_SurveyDiff != this.state.Image_SurveyDiff){
        if (pile3_2.Image_SurveyDiff != this.state.Image_SurveyDiff) {
            await this.setState({Image_SurveyDiff: pile3_2.Image_SurveyDiff})
        }
      }

      if (nextProps.hidden == false) {
        if(this.state.eastCalBy != null && pile3_2.eastCalBy != null){
            await this.onCal( pile3_2.eastCalBy.toString(),'east')
        }
        if(this.state.northCalBy != null && pile3_2.northCalBy != null) {
           await this.onCal( pile3_2.northCalBy.toString(),'north')
        }
      }

      if(pile3_2.constant != this.state.constant){
        await this.setState({constant: pile3_2.constant})
        if (Math.abs(pile3_2.eastCal).toFixed(4) > parseFloat(pile3_2.constant)
        || Math.abs(pile3_2.northCal).toFixed(4) > parseFloat(pile3_2.constant)) {
          await this.setState({canApprove:true})
        }else if(Math.abs(pile3_2.eastCal).toFixed(4) <= parseFloat(pile3_2.constant)
        && Math.abs(pile3_2.northCal).toFixed(4) <= parseFloat(pile3_2.constant)) {
          if (this.state.Edit_Flag == 1) {
            await this.setState({canApprove:false,approveBy:'',approve:'',ApprovedUserId:''},()=>{
              // console.warn('t4')
              this.saveLocal('3_4')
            })
          }
        }

      }else{
        if(pileMaster3_2.Constants != null){
          await this.setState({constant: pileMaster3_2.Constants.Constant})
          if (Math.abs(pile3_2.eastCal).toFixed(4) > parseFloat(pile3_2.constant)
          || Math.abs(pile3_2.northCal).toFixed(4) > parseFloat(pile3_2.constant)) {
            await this.setState({canApprove:true})
          }else if(Math.abs(pile3_2.eastCal).toFixed(4) <= parseFloat(pile3_2.constant)
          && Math.abs(pile3_2.northCal).toFixed(4) <= parseFloat(pile3_2.constant)) {
            if (this.state.Edit_Flag == 1) {
              await this.setState({canApprove:false})
            }
          }
        }
      }
    }
    if (pile3_3) {
      if(pile3_3.top != null && pile3_3.top != undefined && pile3_3.top != this.state.top){
        await this.setState({top: pile3_3.top})
      }
      if(pile3_3.ground != null && pile3_3.ground != undefined && pile3_3.ground != this.state.ground){
        await this.setState({ground: pile3_3.ground})
      }
    }
    if (pile3_4) {
      
      if(pile3_4.approve != null && pile3_4.approve != undefined &&  pile3_4.approve != '' && this.state.approve != pile3_4.approve){
        if (this.state.cancelapprove == 0) {
          await this.setState({approve: pile3_4.approve},()=>{
           
          })
        }
      }
     
      if (pile3_4.approveBy != null && pile3_4.approveBy != undefined && pile3_4.approveBy !=  this.state.approveBy && this.state.approveBy == '') {
        // console.warn('www',this.props.approveagian)
        if (pile3_4.approveBy == null) {
          if (this.state.cancelapprove == 0) {
            await this.setState({ approve: false },()=>{
             
            })
          }
        } else {
          if (this.state.cancelapprove == 0) {
            if(pile3_4.approve==false && pile3_4.approveBy === 0){
              await this.setState({ approve: false },()=>{
                
              })
            }else{
              await this.setState({ approve: true },()=>{
                
              })
            }
            
          }
        }
        await this.setState({approveBy: pile3_4.approveBy})
      }
      if(pile3_4.ApprovedUserId != null && pile3_4.ApprovedUserId != undefined && this.state.ApprovedUserId == ''){
        await this.setState({ApprovedUserId: pile3_4.ApprovedUserId})
      }
    }

    if(nextProps.cancel_approve == 'success' && this.state.submit == true){
      Alert.alert(
        '',
        I18n.t('mainpile.3_4.cancelAppovesuccess'),
        [
          {text: 'OK', onPress: async () => {
            // await this.setState({ approveBy:"", ApprovedUserId: "", approve: false,submit:false,cancelapprove:1})
            var value3_0 = {
              process:3,
              step:0,
              pile_id: this.props.pileid,
              data:{
                id:pile3_0.Id,
                Edit_Flag:1,
                Status:pile3_0.Status,
                StartDate:pile3_0.StartDate,
                EndDate:pile3_0.EndDate,
                LocationLat:pile3_0.LocationLat,
                LocationLong:pile3_0.LocationLong,
              }
            }
            
            await this.props.mainPileSetStorage(this.props.pileid,'3_0',value3_0)
            this.props.mainPileGetStorage(this.props.pileid,'3_0')
            
            // console.warn('ca')
            this.setState({approveagian:true,flagApprove:false,approveBy:"", ApprovedUserId: "", approve: false,submit:false,cancelapprove:1})
            await this.saveLocal('3_4')
          }}
        ],
        { cancelable: false }
      )
    }else if (nextProps.cancel_approve != null && this.state.submit == true ) {
      Alert.alert(
        'ERROR',
        nextProps.cancel_approve,
        [
          {text: 'OK', onPress: () => {
            this.props.clearError()
          }}
        ],
        { cancelable: false }
      )
    }

    if (nextProps.approveState == "success" && this.state.submit == true) {
      var title = ''
      // console.warn('success',this.state.flagaccept)
      if (this.state.approveBy == 2 || this.state.approveBy == 3 ) {
        title = this.state.flagaccept == 1 || (this.state.status5!=0 && this.state.status3 != 2) ? I18n.t("mainpile.3_4.accept_success"): I18n.t("mainpile.3_4.approve_success")
      } else {
        title = this.state.flagaccept == 1 || (this.state.status5!=0 && this.state.status3 != 2) ? I18n.t("mainpile.3_4.accept_request") : I18n.t("mainpile.3_4.approve_request")
      }
      Alert.alert(
        '',
        title,
        [
          {text: 'OK', onPress: async () => {
            this.props.clearError()
            this.setState({ approve: true,submit: false })
            
              this.setState({flagApprove: true,setflagApprove:true,approveagian:false},async()=>{
                var value = {
                  process:3,
                  step:2,
                  pile_id: this.props.pileid,
                  data:{
                    eastPosition: this.state.pile3_2.data.eastPosition,
                    eastCal: this.state.pile3_2.data.eastCal,
                    eastCalBy: this.state.pile3_2.data.eastCalBy,
                    northPosition: this.state.pile3_2.data.northPosition,
                    northCal: this.state.pile3_2.data.northCal,
                    northCalBy: this.state.pile3_2.data.northCalBy,
                    Image_ReadingCoordinate:this.state.pile3_2.data.Image_ReadingCoordinate,
                    Image_SurveyDiff:this.state.pile3_2.data.Image_SurveyDiff,
                    constant:this.state.pile3_2.data.constant,
                    flagaccept:this.state.pile3_2.data.flagaccept,
                    flagApprove:true
                  },
                  haveedit:false
                }
                // console.warn('eee2',value)
                await this.props.mainPileSetStorage(this.props.pileid,'3_2',value)
                if(this.state.approveBy == 2 || this.state.approveBy == 3 || this.state.flagaccept == 1 || (this.state.status5!=0 )){
                  // console.warn('eee2',this.state.ApprovedUserId,this.state.status5,this.state.flagaccept)
                  await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step3Status:2})
                  
                  this.props.clearStatus()
                  this.props.pileClear()
                  
                  if(this.props.category == 4){
                    if(this.state.status2 == 2){
                      this.onSetStack(5,0)
                      this.props.onNextStep()
                    }else{
                      Alert.alert("",I18n.t('mainpile.lockprocess.process5_d'))
                    }
                  }else{
                    this.onSetStack(4,0)
                    this.props.onNextStep()
                  }
                  if(this.state.approveBy != 2){
                    this.props.mainRefresh(1)

                  }
                }
                if (this.state.approveBy == 2 || this.state.approveBy == 3 ) {
                  await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step3Status:2})
                  
                }else {
                  if(this.state.status5 != 0){
                    await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step3Status:2})
                  }else{
                    if (this.state.approveBy == 1 && this.state.flagApprove == true) {
                      // await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step3Status:2})
                    }else {
                      await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step3Status:1})
                    }
                  }
                  
                }
                this.props.mainPileGetStorage(this.props.pileid,'step_status')
              })
              
              var value3_0 = {
                process:3,
                step:0,
                pile_id: this.props.pileid,
                data:{
                  id:pile3_0.Id,
                  Edit_Flag:1,
                  Status:pile3_0.Status,
                  StartDate:pile3_0.StartDate,
                  EndDate:pile3_0.EndDate,
                  LocationLat:pile3_0.LocationLat,
                  LocationLong:pile3_0.LocationLong,
                }
              }
              await this.saveLocal('3_4')
              await this.props.mainPileSetStorage(this.props.pileid,'3_0',value3_0)
              this.props.mainPileGetStorage(this.props.pileid,'3_0')
              // console.warn('eee3',this.state.ApprovedUserId,this.state.status5,this.state.flagaccept)
             
          }}
        ],
        { cancelable: false }
      )
    } else if (nextProps.error_approve != null && this.state.approve_error == null ) {
      console.log("error", nextProps.error_approve + " " + nextProps.approveState+ " "+this.state.approveBy)
      // if (nextProps.error != null) {
      this.setState({ approveBy: "", ApprovedUserId: "", approve: false,submit:false,approve_error:nextProps.error_approve })
      Alert.alert(
        'ERROR',
        nextProps.error_approve,
        [
          {text: 'OK', onPress: () => {
            this.props.clearError()
            this.setState({approve_error:null})
          }}
        ],
        { cancelable: false }
      )
    } else {
      console.log("error", nextProps.error_approve + " " + nextProps.approveState+ " "+this.state.approveBy)
    }
    // if(pile3_4.check){
      // console.warn('check',this.props.waitApprove)
      if(this.state.status3 == 2 ){
        // console.warn('3 เท่ากับ 2')
        this.setState({approveagian:false})
      }else{
        if(this.props.waitApprove){
          this.setState({approveagian:false})
        }else{
          this.setState({approveagian:this.props.approveagian})
        }
       
      }
    // }
      
  }
  async onCal(text, type) {
    // console.warn('onCal1',parseFloat(text),'northPosition',parseFloat(this.state.northPosition))
    var text1 = parseFloat(text).toFixed(4)
    // if (parseFloat(text) >= 0) {
      if (type == "north") {
        let cal = parseFloat(text) - parseFloat(this.state.northPosition)
            await this.setState({ northCal: parseFloat(cal).toFixed(4),northCalBy:text1.toString() })
      } else {
        let cal = parseFloat(text) - parseFloat(this.state.eastPosition)
            await this.setState({ eastCal: parseFloat(cal).toFixed(4),eastCalBy:text1.toString() })
      }
    // }else {
    //   console.warn('else')
    //   await this.setState({ northCal: '',northCalBy:'' })
    //   await this.setState({ eastCal: '',eastCalBy:'' })
    // }
  }

  getState() {
    var data = {
      approve: this.state.approve,
      approveBy: this.state.approveBy,
      ApprovedUserId: this.state.ApprovedUserId
    }
    return data
  }

  onFetchApproveUser(type) {
    index_page=0
    this.refs.Modal_ApprovUser.open()
  }

  onApproveOnline() {
    if (this.state.approve == false || this.state.approveagian) {
      this.onFetchApproveUser(1)
    }
  }
  async onSetStack(pro,step) {
    var data = {
      process: pro,
      step: step + 1
    }
    await this.props.setStack(data)
    await this.props.mainPileGetAllStorage()
  }
  onApproveSelf() {
    if (this.state.approve == false || this.state.approveagian) {
      Actions.selfapprove({
        process: this.state.process,
        step: this.state.step,
        data: false,
        jobid: this.props.jobid,
        pileid: this.props.pileid,
        category:this.props.shape,
        lat: this.state.location == undefined
        ? 1
        : this.state.location.position.lat,
        log: this.state.location == undefined
        ? 1
        : this.state.location.position.log,
        check10: this.state.flagaccept == 1|| (this.state.status5!=0 && this.state.status3 != 2)?true:false
      })
    }
  }

  onApproveQrCode() {
    if (this.state.approve == false || this.state.approveagian) {
      Actions.scanQrCode({
        process: this.state.process,
        step: this.state.step,
        data: false,
        jobid: this.props.jobid,
        pileid: this.props.pileid,
        lat: this.state.location == undefined
        ? 1
        : this.state.location.position.lat,
        log: this.state.location == undefined
        ? 1
        : this.state.location.position.log,
      })
    }
  }
  onCancelApprove(){
    Alert.alert("", I18n.t('mainpile.3_4.needcancel'), [
      {
        text: "Cancel",
        // onPress: () => this.setState({ select_user: "", approveBy: "", ApprovedUserId: "" })
      },
      {
        text: "OK",
        onPress: () => {

          var value = {
            Language: I18n.locale,
            JobId: this.props.jobid,
            PileId: this.props.pileid
          }
          this.props.pileCancelApproveStep03(value)
          // this.setState({approve:false,approveagian:true})
          this.setState({submit:true})
        }
      }
    ],
    { cancelable: false })
  }

  onCalWithConstant(type) {
    
    if (type == "Online") {
      this.props.clearError()
      this.onApproveOnline()
    } else if (type == "Self") {
      this.props.clearError()
      this.onApproveSelf()
    } else if (type == "QrCode") {
      this.props.clearError()
      this.onApproveQrCode()
    }else {
      this.props.clearError()
      this.onCancelApprove()
    }
  }

  onCameraRoll(keyname,photos,type){
    this.props.mainPileGetStorage(this.props.pileid,6)
    Actions.camearaRoll({process:this.state.process,step:this.state.step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:type,shape:this.props.shape,category:this.props.category,Edit_Flag:0})
  }


  async updateState(value){
    if (this.state.submit == false) {
        await this.setState({approve:value.isapprove,approveBy:value.approveBy,ApprovedUserId:value.ApprovedUserId,submit:true})
    }
  }

  async saveLocal(process_step){
    // console.warn('this.state.approve = ',this.state.approve)
    var value = {
      process:3,
      step:4,
      pile_id: this.props.pileid,
      data:{
        approve:this.state.approve,
        approveBy:this.state.approveBy,
        ApprovedUserId:this.state.ApprovedUserId,
        ApprovedDate:this.state.flagaccept == 1 || (this.state.status5 !=0 )?this.state.ApprovedDate:null,
        check:this.state.check
      }
    }
    // console.warn(value)
    await this.props.mainPileSetStorage(this.props.pileid,process_step,value)
  }

  _renderApproveUser() {
    return (
      <ModalSelector
        data={this.state.approve_user}
        onChange={benchmark => console.log(1)}
        selectTextStyle={styles.textButton}
        cancelText="Cancel"
      />
    )
  }

  _renderTopic() {
    return (
      <View style={styles.topic}>
        <Text style={styles.topicText}>{this.props.shape == 1 ? I18n.t('mainpile.3_4.checkcasing') : I18n.t('mainpile.3_4.checktopdwall')}</Text>
      </View>
    )
  }

  _renderBasicInformation() {
    return (
      <View style={{ marginTop: 15 }}>
        <View style={styles.topicBox}>
          <Text style={styles.subTopic}>{I18n.t('notificationdetail.information')}</Text>
        </View>
        <View style={{ marginTop: 5 }}>
          <BoxView left={I18n.t('notificationdetail.projectName')} right={this.state.pile.job_fullname} />
          <BoxView left={I18n.t('notificationdetail.pileNo')} right={this.state.pile.pile_no} />
          <BoxView left={I18n.t('mainpile.3_4.diameter')} right={this.props.shape == 1 ? this.state.pile.size : this.state.pile.size} />
          <BoxView left={I18n.t('mainpile.3_4.cutoff')} right={parseFloat(this.state.pile.cutoff).toFixed(4)} />
    <BoxView left={I18n.t('mainpile.3_4.piletip')} right={parseFloat(this.state.pile.piletip).toFixed(4)} last={!this.state.pile.tgw}/>
          {/* {this.props.shape == 2 && <BoxView left="Top Guild Wall (ม.)" right={this.state.pile.tgw} last /> } */}
        </View>
      </View>
    )
  }

  _renderMeasurePosition() {
    return (
      <View style={{ marginTop: 15 }}>
        <View style={styles.topicBox}>
          <Text style={styles.subTopic}>{I18n.t('notificationdetail.informations')}</Text>
        </View>
        <View style={{ marginTop: 5 }}>
          <BoxView left={I18n.t('notificationdetail.surveyor')} right={this.state.survey} />
          <BoxView left={I18n.t('notificationdetail.station')} right={this.state.benchmarkCam + ' ('+this.state.benchmarkCamLabel + ')'} />
          <BoxView left={I18n.t('notificationdetail.stationES')} right={this.state.benchmarkFlag+ ' ('+this.state.benchmarkFlagLabel+ ')'} />
          <BoxView left="Resection" right={this.state.resection} last />
        </View>
      </View>
    )
  }

  _renderCasingPosition() {
    console.log('top process3',this.state.top)
    return (
      <View style={{ marginTop: 15 }}>
        <View style={styles.topicBox}>
          <Text style={styles.subTopic}>{this.props.shape == 1 ? I18n.t('mainpile.3_4.casinglevel') :I18n.t('mainpile.3_4.topwall') }</Text>
        </View>
        <View style={{ marginTop: 5 }}>
          <BoxView left={this.props.shape == 1 ? I18n.t("mainpile.3_3.top") : I18n.t("mainpile.3_3.top_dwall")} right={this.state.top== '' ?'0.000':parseFloat(this.state.top).toFixed(4)} />
          <BoxView left={I18n.t('mainpile.3_4.ge')} right={this.state.ground == ''?'0.000':parseFloat(this.state.ground).toFixed(4)} last />
        </View>
      </View>
    )
  }

  _renderTable() {
    return (
      <View style={{ marginTop: 15 }}>
        <TableView value={[I18n.t('notificationdetail.coordinate'), I18n.t('notificationdetail.plan'), I18n.t('notificationdetail.read'), I18n.t('notificationdetail.deviation')]} head shape={this.props.shape}/>
        { Math.abs(parseFloat(this.state.northCal)) > parseFloat(this.state.constant) ?
          <TableView value={["N ("+I18n.t('mainpile.3_4.m')+')', this.state.northPosition, parseFloat(this.state.northCalBy).toFixed(4), parseFloat(this.state.northCal).toFixed(4)]} red /> :
          <TableView value={["N ("+I18n.t('mainpile.3_4.m')+')', this.state.northPosition, parseFloat(this.state.northCalBy).toFixed(4), parseFloat(this.state.northCal).toFixed(4)]} />
        }
        { Math.abs(parseFloat(this.state.eastCal)) > parseFloat(this.state.constant) ?
          <TableView value={["E ("+I18n.t('mainpile.3_4.m')+')', this.state.eastPosition, parseFloat(this.state.eastCalBy).toFixed(4), parseFloat(this.state.eastCal).toFixed(4)]} red last /> :
          <TableView value={["E ("+I18n.t('mainpile.3_4.m')+')', this.state.eastPosition, parseFloat(this.state.eastCalBy).toFixed(4), parseFloat(this.state.eastCal).toFixed(4)]} last />
        }
         { /* <TableView value={["N (ม.)", this.state.northPosition, this.state.northCalBy, this.state.northCal]} />
      <TableView value={["E (ม.)", this.state.eastPosition, this.state.eastCalBy, this.state.eastCal]} last /> */ }
        {/* <Text>ค่าพิกัดที่อ่านได้</Text>
        <Text>ระยะคลาดเคลื่อน</Text> */}
      </View>
    )
  }
  _renderSendDataaccept() {
    // console.warn('acceepttion',this.state.flagApprove,this.state.approveagian)
    return (
      <View
        style={{
          paddingBottom: 15
        }}
      >
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t('mainpile.3_4.acceptby')}</Text>
        </View>
        {(this.state.flagApprove == false && this.state.approveagian )  ? (
          <View>
            <TouchableOpacity style={styles.button} onPress={() => this.onCalWithConstant("Online")}>
              <Text style={styles.textButton}>{I18n.t('mainpile.3_4.acceptonline')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={() => this.onCalWithConstant("Self")}>
              <Text style={styles.textButton}>{I18n.t('mainpile.3_4.acceptself')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={() => this.onCalWithConstant("QrCode")}>
              <Text style={styles.textButton}>{I18n.t('mainpile.3_4.acceptqr')}</Text>
            </TouchableOpacity>
           {/*<View style={styles.button_gray}>
              <Text style={styles.textButton}>{I18n.t('mainpile.3_4.cancelAppove')}</Text>
        </View>*/}
          </View>
        ) : (
          <View>
            {
              this.state.approveBy == 1 ?
              <View style={styles.button_green}>
                <Text style={styles.textButton}>{I18n.t('mainpile.3_4.acceptonline')}</Text>
              </View>
              :
              <View style={styles.button_gray}>
                <Text style={styles.textButton}>{I18n.t('mainpile.3_4.acceptonline')}</Text>
              </View>
            }
            {
              this.state.approveBy == 2 ?
              <View style={styles.button_green}>
                <Text style={styles.textButton}>{I18n.t('mainpile.3_4.acceptself')}</Text>
              </View>
              :
              <View style={styles.button_gray}>
                <Text style={styles.textButton}>{I18n.t('mainpile.3_4.acceptself')}</Text>
              </View>
            }
            {
              this.state.approveBy == 3 ?
              <View style={styles.button_green}>
                <Text style={styles.textButton}>{I18n.t('mainpile.3_4.acceptqr')}</Text>
              </View>
              :
              <View style={styles.button_gray}>
                <Text style={styles.textButton}>{I18n.t('mainpile.3_4.acceptqr')}</Text>
              </View>
            }
          </View>
        )}

        <ModalSelector
          data={this.state.approve_user}
          onChange={data => {
            Alert.alert("", this.state.flagaccept == 1|| (this.state.status5!=0 && this.state.status3 != 2)?I18n.t('mainpile.3_4.acceptbyheader') + data.label:I18n.t('mainpile.3_4.by') + data.label, [
              {
                text: "Cancel",
                onPress: () => this.setState({ select_user: "", approveBy: "", ApprovedUserId: "" })
              },
              {
                text: "OK",
                onPress: async () => {
                  var value = {
                    Language: I18n.locale,
                    JobId: this.props.jobid,
                    ApproveType: 1,
                    ApprovedUserId: data.key,
                    PileId: this.props.pileid,
                    LocationLat: this.state.location == undefined ? 1 : this.state.location.position.lat,
                    LocationLong: this.state.location == undefined ? 1 : this.state.location.position.log,
                  }
                  this.props.pileSaveApproveStep03(value)
                  this.setState({
                    select_user: data.label,
                    approveBy: 1,
                    ApprovedUserId: data.key,
                    submit: true
                  })
                  // console.warn('online',value)
                  // console.warn('set approvedate')
                  if(this.state.flagaccept == 1 || (this.state.status5 !=0 )){
                    this.setState({
                      ApprovedDate:moment().format("DD/MM/YYYY HH:mm")
                    },()=>{
                      // console.warn('set approvedate')
                    })
                    // await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step3Status:2})
                    // this.props.mainPileGetStorage(this.props.pileid,'step_status')
                  }
                }
              }
            ],
            { cancelable: false })
          }}
          selectTextStyle={styles.textButton}
          style={{ borderWidth: 0, height: 0, width: 0 }}
          cancelText="Cancel"
          ref="Modal_ApprovUser"
        />
      </View>
    )
  }

  _renderSendData() {
    if(this.state.status5 != 0){
      // console.warn('_renderSendData1',this.state.approveBy == 1 , this.state.pile3_4.ApprovedDate != null , this.state.pile3_4.ApprovedDate != '', this.state.Edit_Flag)
      return (
        <View
          style={{
            paddingBottom: 15
          }}
        >
          <View style={styles.topic}>
            <Text style={styles.topicText}>{I18n.t('mainpile.3_4.approveby')}</Text>
          </View>
          {(this.state.approve == false && this.state.approveagian) && this.state.Edit_Flag == 1 && this.state.flagaccept!=3? (
            <View>
              <TouchableOpacity style={styles.button} onPress={() => this.onCalWithConstant("Online")}>
                <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveonline')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={() => this.onCalWithConstant("Self")}>
                <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveself')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={() => this.onCalWithConstant("QrCode")}>
                <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveqr')}</Text>
              </TouchableOpacity>
              <View style={styles.button_gray}>
                <Text style={styles.textButton}>{I18n.t('mainpile.3_4.cancelAppove')}</Text>
              </View>
            </View>
          ) : (
            <View>
              {
                this.state.approveBy == 1 ?
                <View style={(this.state.pile3_4.ApprovedDate == null || this.state.pile3_4.ApprovedDate== '') &&  this.state.flagaccept!=3 ? styles.button:styles.button_green}>
                  <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveonline')}</Text>
                </View>
                :
                <View style={styles.button_gray}>
                  <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveonline')}</Text>
                </View>
              }
              {
                this.state.approveBy == 2 ?
                <View style={styles.button_green}>
                  <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveself')}</Text>
                </View>
                :
                <View style={styles.button_gray}>
                  <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveself')}</Text>
                </View>
              }
              {
                this.state.approveBy == 3 ?
                <View style={styles.button_green}>
                  <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveqr')}</Text>
                </View>
                :
                <View style={styles.button_gray}>
                  <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveqr')}</Text>
                </View>
              }
              {
                this.state.approveBy == 1 ?
                <TouchableOpacity disabled={(this.state.approveBy == 1 && this.state.pile3_4.ApprovedDate != null && this.state.pile3_4.ApprovedDate != '') || this.state.Edit_Flag==0? true : false }
                  style={this.state.approveBy == 1 && this.state.pile3_4.ApprovedDate != null && this.state.pile3_4.ApprovedDate != ''?
                    styles.button_gray :  [styles.button,{backgroundColor:'#ff7a73'}]} onPress={() => this.onCalWithConstant("Cancel")}>
                  <Text style={styles.textButton}>{I18n.t('mainpile.3_4.cancelAppove')}</Text>
                </TouchableOpacity>
                :
                <View style={styles.button_gray}>
                  <Text style={styles.textButton}>{I18n.t('mainpile.3_4.cancelAppove')}</Text>
                </View>
              }
            </View>
          )}
  
          <ModalSelector
            data={this.state.approve_user}
            onChange={data => {
              Alert.alert("", this.state.flagaccept == 1|| (this.state.status5!=0 && this.state.status3 != 2)?I18n.t('mainpile.3_4.acceptbyheader') + data.label:I18n.t('mainpile.3_4.by') + data.label, [
                {
                  text: "Cancel",
                  onPress: () => this.setState({ select_user: "", approveBy: "", ApprovedUserId: "" })
                },
                {
                  text: "OK",
                  onPress: async() => {
                    var value = {
                      Language: I18n.locale,
                      JobId: this.props.jobid,
                      ApproveType: 1,
                      ApprovedUserId: data.key,
                      PileId: this.props.pileid,
                      LocationLat: this.state.location == undefined ? 1 : this.state.location.position.lat,
                      LocationLong: this.state.location == undefined ? 1 : this.state.location.position.log,
                    }
                    this.props.pileSaveApproveStep03(value)
                    this.setState({
                      select_user: data.label,
                      approveBy: 1,
                      ApprovedUserId: data.key,
                      submit: true
                    })
                    // console.warn('online',value)
                    if(this.state.flagaccept == 1 || (this.state.status5 !=0 )){
                      this.setState({
                        ApprovedDate:moment().format("DD/MM/YYYY HH:mm")
                      },()=>{
                        // console.warn('set approvedate')
                      })
                      await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step3Status:2})
                      this.props.mainPileGetStorage(this.props.pileid,'step_status')
                    }
                  }
                }
              ],
              { cancelable: false })
            }}
            selectTextStyle={styles.textButton}
            style={{ borderWidth: 0, height: 0, width: 0 }}
            cancelText="Cancel"
            ref="Modal_ApprovUser"
          />
        </View>
      )
    }else{
      // console.warn('_renderSendData2',this.state.approve == false , this.state.approveagian, this.state.Edit_Flag == 1 , this.state.flagaccept!=3 , this.state.flagApprove == false)
      return (
        <View
          style={{
            paddingBottom: 15
          }}
        >
          <View style={styles.topic}>
            <Text style={styles.topicText}>{I18n.t('mainpile.3_4.approveby')}</Text>
          </View>
          {(this.state.approve == false || this.state.approveagian) && this.state.Edit_Flag == 1 && this.state.flagaccept!=3 && this.state.flagApprove == false? (
            <View>
              <TouchableOpacity style={styles.button} onPress={() => this.onCalWithConstant("Online")}>
                <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveonline')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={() => this.onCalWithConstant("Self")}>
                <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveself')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={() => this.onCalWithConstant("QrCode")}>
                <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveqr')}</Text>
              </TouchableOpacity>
              <View style={styles.button_gray}>
                <Text style={styles.textButton}>{I18n.t('mainpile.3_4.cancelAppove')}</Text>
              </View>
            </View>
          ) : (
            <View>
              {
                this.state.approveBy == 1 ?
                <View style={(this.state.pile3_4.ApprovedDate == null || this.state.pile3_4.ApprovedDate== '') &&  this.state.flagaccept!=3 ? styles.button:styles.button_green}>
                  <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveonline')}</Text>
                </View>
                :
                <View style={styles.button_gray}>
                  <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveonline')}</Text>
                </View>
              }
              {
                this.state.approveBy == 2 ?
                <View style={styles.button_green}>
                  <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveself')}</Text>
                </View>
                :
                <View style={styles.button_gray}>
                  <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveself')}</Text>
                </View>
              }
              {
                this.state.approveBy == 3 ?
                <View style={styles.button_green}>
                  <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveqr')}</Text>
                </View>
                :
                <View style={styles.button_gray}>
                  <Text style={styles.textButton}>{I18n.t('mainpile.3_4.approveqr')}</Text>
                </View>
              }
              {
                this.state.approveBy == 1 ?
                <TouchableOpacity disabled={(this.state.approveBy == 1 && this.state.pile3_4.ApprovedDate != null && this.state.pile3_4.ApprovedDate != '')|| this.state.Edit_Flag==0? true : false }
                  style={this.state.approveBy == 1 && this.state.pile3_4.ApprovedDate != null && this.state.pile3_4.ApprovedDate != ''?
                    styles.button_gray :  [styles.button,{backgroundColor:'#ff7a73'}]} onPress={() => this.onCalWithConstant("Cancel")}>
                  <Text style={styles.textButton}>{I18n.t('mainpile.3_4.cancelAppove')}</Text>
                </TouchableOpacity>
                :
                <View style={styles.button_gray}>
                  <Text style={styles.textButton}>{I18n.t('mainpile.3_4.cancelAppove')}</Text>
                </View>
              }
            </View>
          )}
  
          <ModalSelector
            data={this.state.approve_user}
            onChange={data => {
              // console.warn('ModalSelector 5',index_page,data)
             
              if(index_page%2 == 0){
                Alert.alert("", this.state.flagaccept == 1|| (this.state.status5!=0 && this.state.status3 != 2)?I18n.t('mainpile.3_4.acceptbyheader') + data.label:I18n.t('mainpile.3_4.by') + data.label, [
                  {
                    text: "Cancel",
                    onPress: () => {
                      this.setState({ select_user: "", approveBy: "", ApprovedUserId: "" })
                    }
                  },
                  {
                    text: "OK",
                    onPress: async() => {
                      var value = {
                        Language: I18n.locale,
                        JobId: this.props.jobid,
                        ApproveType: 1,
                        ApprovedUserId: data.key,
                        PileId: this.props.pileid,
                        LocationLat: this.state.location == undefined ? 1 : this.state.location.position.lat,
                        LocationLong: this.state.location == undefined ? 1 : this.state.location.position.log,
                      }
                      // console.warn('online',value)
                      this.props.pileSaveApproveStep03(value)
                      this.setState({
                        select_user: data.label,
                        approveBy: 1,
                        ApprovedUserId: data.key,
                        submit: true
                      })
                      if(this.state.flagaccept == 1 || (this.state.status5 !=0 )){
                        this.setState({
                          ApprovedDate:moment().format("DD/MM/YYYY HH:mm")
                        },()=>{
                          // console.warn('set approvedate')
                        })
                        await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step3Status:2})
                        this.props.mainPileGetStorage(this.props.pileid,'step_status')
                      }
                    }
                  }
                ],
                { cancelable: false })
              }
              index_page++
              
            }}
            selectTextStyle={styles.textButton}
            style={{ borderWidth: 0, height: 0, width: 0 }}
            cancelText="Cancel"
            ref="Modal_ApprovUser"
          />
        </View>
      )
    }
    
  }

_renderShowImage() {
  return (<View>
    <View>
      <View style={{ marginTop: 10,marginLeft: 10 }}>
        <Text>{I18n.t("mainpile.3_2.read")}</Text>
      </View>
      <View style={{ alignItems: "center", marginTop: 10 }}>
        <TouchableOpacity style={[styles.image,this.state.Image_ReadingCoordinate.length>0?{backgroundColor:'#6dcc64'}:{}]} onPress={() => this.onCameraRoll('Image_ReadingCoordinate',this.state.Image_ReadingCoordinate,'view')}>
          <Icon name="image" type="entypo" color="#FFF" />
          <Text style={styles.imageText}>{I18n.t("mainpile.3_2.view")}</Text>
        </TouchableOpacity>
      </View>
      <View style={{ marginTop: 10,marginLeft: 10 }}>
        <Text>{I18n.t("mainpile.3_2.deviation")}</Text>
      </View>
      <View style={{ alignItems: "center", marginTop: 10 }}>
        <TouchableOpacity style={[styles.image,this.state.Image_SurveyDiff.length>0?{backgroundColor:'#6dcc64'}:{}]} onPress={() => this.onCameraRoll('Image_SurveyDiff',this.state.Image_SurveyDiff,'view')}>
          <Icon name="image" type="entypo" color="#FFF" />
          <Text style={styles.imageText}>{I18n.t("mainpile.3_2.view")}</Text>
        </TouchableOpacity>
      </View>
    </View>
  </View>)
}

  render() {
    // console.warn(this.state.flagaccept,this.state.canApprove,this.props.approveagian)
    if (this.props.hidden) {
      return null
    }
    return (
      <View>
        {this._renderTopic()}
        {this._renderBasicInformation()}
        {this._renderMeasurePosition()}
        {this._renderCasingPosition()}
        {this._renderTable()}
        {this._renderShowImage()}
        {this.state.canApprove == true ? 
          this.state.flagaccept == 1||(this.state.status5 != 0 && this.state.status3 != 2) ? 
          this._renderSendDataaccept():this._renderSendData() 
          : <View />}
      </View>
    )
  }
}

class BoxView extends Component {
  render() {
    return (
      <View style={[styles.box, this.props.last ? { borderBottomWidth: 0.5 } : {}]}>
        <View style={styles.leftBox}>
          <Text style={styles.textBox}>{this.props.left}</Text>
        </View>
        <View style={styles.rightBox}>
          <Text numberOfLines={2} style={[styles.textBox]}>{this.props.right}</Text>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    item: state.pile.item,
    approveState: state.pile.approve,
    error_approve: state.pile.error_approve,
    cancel_approve: state.pile.cancel_approve,
    pileAll: state.mainpile.pileAll || null,
    stack: state.mainpile.stack_item,
  }
}


export default connect(mapStateToProps, actions, null, { withRef: true })(Pile3_4)
