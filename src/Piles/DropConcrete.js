import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  DatePickerAndroid,
  TimePickerAndroid,
  BackHandler
} from "react-native"
import { connect } from "react-redux"
import { Container, Content } from "native-base"
import { Actions } from "react-native-router-flux"
import TableView from "../Components/TableView"
import {
  MAIN_COLOR,
  SUB_COLOR,
  BASIC_COLOR,
  MENU_GREY_ITEM,
  MENU_GREEN,
  DEFAULT_COLOR_4,
  DEFAULT_COLOR_1
} from "../Constants/Color"
import { Icon } from "react-native-elements"
import I18n from "../../assets/languages/i18n"
import ModalSelector from "react-native-modal-selector"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import styles from "./styles/TremieInsert.style"
import * as actions from "../Actions"
import moment from "moment"
import DateTimePicker from '@react-native-community/datetimepicker';
import RNTooltips from "react-native-tooltips"

class DropConcrete extends Component {
  constructor(props) {
    super(props)
    this.state = {
      Edit_Flag: 1,
      depth: "",
      truckdata: [{ key: 0, section: true, label: "เลือกหมายเลขรถ" }],
      truck: I18n.t("mainpile.DropConcrete.selectCarno"),
      lastconcretetruckButton: false,
      // lastconcretetruckcheck4: false,
      lastconcretetruckButtonEnable: true,
      cuttremieButton: false,
      dropconcreteimage: [],
      startdrop: "",
      finishdrop: "",
      casingdepth: "",
      concretevolume: 0,
      concretetheoreticalvolume: 0,
      concretecumulative: 0,
      cutoff: 0,
      topcasing: 0,
      dia:0,
      overcast:0,
      size: 1,
      stopendPrimary:null,
      stopendClosing:null,
      size_bp: null,
      tremiepipe: [],
      truckarrival: null,
      truckid: "",
      mainpiledetail:null,
      concretemustpour:0,
      LastDrillingDepth:null,

      // LAST TRUCK
      showlastview: false,
      showcheck4: true,
      checkfinalpvc: false,
      checkfinalover: false,
      finalconcreteimage: [],
      finalconcreteoverimage: [],
      final_concretedepth: 0,
      final_concretevolume: "",
      final_pvcheight: 0,
      final_overheight:'',
      final_overwidth:'',
      final_overLength:'',
      final_concreteleftovervolume:'',
      final_concretevolumecal: '',
      final_concreteusedata: [{ key: 0, section: true, label: I18n.t("mainpile.DropConcrete.final_concrete_over_use") }],
      final_concreteuse:I18n.t("mainpile.DropConcrete.final_concrete_over_use"),
      final_concreteuseid:'',
      final_plummetheight: 0,
      final_remainusepilename:'',
      // LastTruckPlummetConcreteHeight

      // TREMIE CUT
      tremiepipeinstall: 0,
      tremiepipelength: 0,
      cuttremiepipe: 0,
      tremiepipecutlength: 0,
      tremiepipecutlength2: 0,
      remainingtremiepiplength: 0,
      tremiepipeembeddedlength: 0,
      beforetremiepipeembeddedlength: 0,
      concreteleft: 0,
      mustcuttremie: 0,
      mustcutarr: [],
      checkstopend:null,

      tremietemp: [],
      theorydepth: "0",

      err: false,
      errmsg: I18n.t("mainpile.DropConcrete.alert.error1"),

      locktremieEdit: false,
      trigger: false,
      casingdepthtemp: "",

      //date picker
      datevisibledate_startdrop:false,
      datevisibletime_startdrop:false,
      date_startdrop:'',

      datevisibledate_finishdrop:false,
      datevisibletime_finishdrop:false,
      date_finishdrop:'',

      //tip
      visible: false,
      dismiss: false,
      ref: undefined,
      visible2: false,
      dismiss2: false,
      ref2: undefined,

      // pre - drop con
      predropconcrete: "",
      postdropconcrete: "",
      newesttruck: false,

      //Predepth
      Predepth: "",
      Precutsink: 0,
      PreTruck: "",
      PredepthArr: null,
      Parentcheck: null,

      //stop in between
      checkstopbetween:false,
      showconcretevolumestop:false,
      concretevolumestop:0,
      pretruckConcreteVolstop:0,
      pretruckcheckstopbetween:false,

      
    }
  }

  updateState(value) {
    // console.warn(value)
  }

  componentWillReceiveProps(nextProps) {
    // console.warn(nextProps.concretevolume)
    // console.warn(nextProps.concretetruck)

    if (
      nextProps.concretetruck != null &&
      this.props.concretetruck != nextProps.concretetruck
    ) {
      let truckdata = [
        {
          key: 0,
          section: true,
          label: I18n.t("mainpile.DropConcrete.selectCarno")
        }
      ]
        nextProps.concretetruck.map((data, index) => {
          // console.warn(data)
          truckdata.push({
            key: index + 1,
            label: data.TruckNo,
            id: data.Id,
            volume: data.TruckConcreteVolume,
            truckarrival: data.TruckArrivalTime
          })
        })
      if (this.props.edit == true) {
        truckdata.push({
          key: truckdata.length + 1,
          label: this.props.data.truckno,
          id: this.props.data.truckid,
          volume: this.props.data.concretevolume,
          truckarrival: this.props.data.TruckArrivalTime
        })
      }
      this.setState({ truckdata })
    }
    // if(nextProps.predropconcrete!=""){
    //   console.log("nextProps.predropconcrete",nextProps.predropconcrete)
    //   this.setState({ predropconcrete: nextProps.predropconcrete})
    // }

    // var pile = null
    // var pile4_0 = null
    // var pileMaster = null
    // if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["4_1"]) {
    //   pile = nextProps.pileAll[this.props.pileid]["4_1"].data
    //   pileMaster = nextProps.pileAll[this.props.pileid]["4_1"].masterInfo
    // }
    // console.log(nextProps.value)
    if (nextProps.action == "update") {
      if (nextProps.value.approvetype != undefined) {
        this.setState({
          approvetype: nextProps.value.approvetype,
          approveid: nextProps.value.approveid,
          approved: true
        })
      }
      if (nextProps.value.data != undefined) {
        if (nextProps.value.data.dropconcreteimage != undefined) {
          this.setState({
            dropconcreteimage: nextProps.value.data.dropconcreteimage
          })
        }
        if (nextProps.value.data.finalconcreteimage != undefined) {
          this.setState({
            finalconcreteimage: nextProps.value.data.finalconcreteimage
          })
        }
        if (nextProps.value.data.finalconcreteoverimage != undefined) {
          this.setState({
            finalconcreteoverimage: nextProps.value.data.finalconcreteoverimage
          })
        }
      }
    }
  }

  fetch() {
    this.props.getConcreteList10({
      language: I18n.currentLocale(),
      pileid: this.props.pileid,
      jobid: this.props.jobid
    })
    // Get tremie pip
    this.props.getTremieList({
      language: I18n.currentLocale(),
      pileid: this.props.pileid,
      jobid: this.props.jobid
    })
    this.calculateTremieArr()
  }

  async calculateTremie() {
    const { tremiepipe, tremielist, tremievolume } = this.props

    const DEPTH =
      (this.props.category == 3 || this.props.category == 5) &&
      this.state.Parentcheck == false
        ? Math.abs(this.props.depth_child)
        : Math.abs(this.props.depth)
    const DEPTH_child =
      this.props.category == 5 ? Math.abs(this.props.depth_child) : ""

    let checkd_wall =
      this.props.category == 5
        ? this.state.Predepth != "" && this.state.Predepth != null
          ? this.state.Predepth < DEPTH
            ? true
            : false
          : false
        : null //ถ้า true เปลียนสูตรเป็นเหลี่มม

    const SIZE =
      this.props.category == 5
        ? checkd_wall == true
          ? this.state.size
          : this.state.size_bp
        : this.state.size

    const CIRCLE_AREA = (Math.PI * Math.pow(SIZE, 2)) / 4
    const RECTANGLE_AREA = SIZE

    var concretecumulative =
      this.state.concretevolume + this.props.concretecumulative


    console.warn(
      "calculateTremie",
      "checkd_wall : " + checkd_wall,
      "size : " + this.state.size,
      "size_bp :" + this.state.size_bp,
      "DEPTH_child : " + this.props.depth_child,
      "DEPTH : " + DEPTH
    )

    var concretecumulative =
      this.state.concretevolume + this.props.concretecumulative

    const AREA =
      this.props.category == 5
        ? checkd_wall == true
          ? RECTANGLE_AREA
          : CIRCLE_AREA
        : this.props.shape == 1
        ? CIRCLE_AREA
        : RECTANGLE_AREA // 1 use circle other use rectangle

    const CONCRETE_VOLUME_HEIGHT = 1 / AREA // คอนกรีต 1m^3 เทจะสูง

    var concrete_height = concretecumulative * CONCRETE_VOLUME_HEIGHT

    var concrete_height_per_bus =
      this.state.concretevolume * CONCRETE_VOLUME_HEIGHT //ความสูงคอนกรีตต่อคัน

    let theoretical_concrete_overflow = ""
    let Predepth =
      this.props.category == 5
        ? checkd_wall == true
          ? await this.calPredepthDwall()
          : await this.calPredepth()
        : ""
    // var theoretical_concrete =
    //   this.props.category == 5
    //     ? checkd_wall == true
    //       ? Predepth
    //       : Predepth - concrete_height_per_bus
    //     : DEPTH - concrete_height // ความลึกคอนกรีตจาก TCL (ตามทฤษฎี) (m.)

    var theoretical_concrete =
      this.props.category == 5 ? Predepth : DEPTH - concrete_height // ความลึกคอนกรีตจาก TCL (ตามทฤษฎี) (m.)

    // var theoretical_concrete =
    //   this.props.category == 5
    //     ? checkd_wall == true
    //       ? Predepth
    //       : DEPTH_child - concrete_height
    //     : DEPTH - concrete_height
    // console.warn(
    //   Predepth - concrete_height_per_bus,
    //   Predepth,
    //   concrete_height_per_bus,
    //   concrete_height
    // )
    console.warn(
      "theoretical_concrete_cal",
      theoretical_concrete,
      DEPTH - concrete_height
    )
    if (
      this.props.category == 5 &&
      Predepth <= DEPTH && //(old)Predepth - concrete_height_per_bus <= DEPTH
      checkd_wall == false
    ) {
      let data = {
        concrete_height_per_bus: concrete_height_per_bus,
        theoretical_concrete: Predepth,
        DEPTH: DEPTH
      }
      theoretical_concrete_overflow = this.calOverflow(data)
      if (theoretical_concrete_overflow != null) {
        theoretical_concrete = theoretical_concrete_overflow
      }
    }else if (
      this.props.category != 5 &&
      this.state.Predepth != null &&
      this.state.Predepth != ""
    ) {
      if (this.props.shape == 1) {
        let oldPredepth = await this.calPredepthArr(1)
        console.log(
          "oldPredepth",
          oldPredepth,
          "Predepth",
          this.state.Predepth,
          "concrete_height_per_bus",
          concrete_height_per_bus,
          "theoretical_concrete",
          this.state.Predepth - concrete_height_per_bus - oldPredepth
        )
        theoretical_concrete =
          this.state.Predepth - concrete_height_per_bus - oldPredepth
      } else {

        let oldPredepth = await this.calPredepthArr(2)
        console.log(
          "oldPredepth",
          oldPredepth,
          "Predepth",
          this.state.Predepth,
          "concrete_height_per_bus",
          concrete_height_per_bus,
          "theoretical_concrete",
          this.state.Predepth - concrete_height_per_bus - oldPredepth
        )
        theoretical_concrete =
          this.state.Predepth - concrete_height_per_bus - oldPredepth
      }
    }


    // if( this.props.category!=1&& this.props.category!=4){
    //   const stopend_length = this.state.mainpiledetail.stopend_length
    //   // let old_concrete_high = await this.calPredepthArr(2)
    //   // let Predepthcal = this.state.Predepth - old_concrete_high
    //   let old_theoretical_concrete = theoretical_concrete
    //   let Predepthcal = await this.calArrPredepthSTE()
      
    //   // console.warn('this.state.Predepth', this.state.Predepth,'old_concrete_high',old_concrete_high,'Predepth',Predepth)
    //   let casingdepth = this.state.casingdepth !== "" && this.state.casingdepth !== null
    //                       ? this.state.casingdepth
    //                       : theoretical_concrete
    //   let Stopend = await this.calStopend(casingdepth,stopend_length) 
    //   console.warn('casingdepth,stopend_length,Predepthcal',casingdepth,stopend_length,Predepthcal,Stopend)
    //   if(casingdepth<stopend_length&&Predepthcal<stopend_length){
    //     if(Stopend!=null){
    //       let theo_con_STE = Predepthcal-(this.state.concretevolume*(1/Stopend))
    //       // console.log('theo_con_STE Predepthcal<',theo_con_STE,'theoretical_concrete',theoretical_concrete,"this.state.Predepth",this.state.Predepth,"old_concrete_high",old_concrete_high,this.props.depth-old_concrete_high)
    //       theoretical_concrete =  theo_con_STE
    //       console.warn('Predepthcal<stopend_length',theoretical_concrete,Predepthcal,this.state.concretevolume,1/Stopend)
    //       casingdepth = this.state.casingdepth !== "" && this.state.casingdepth !== null
    //                       ? this.state.casingdepth
    //                       : theoretical_concrete
    //       this.calmustpourDwall(casingdepth,checkd_wall,3,Stopend)

    //     }
       
    //   }else if(casingdepth<stopend_length&&Predepthcal>=stopend_length){
    //     if(Stopend!=null){
    //       let theo_con_STE = Predepthcal - (Predepthcal-stopend_length)-((this.state.concretevolume-((Predepthcal-stopend_length)*RECTANGLE_AREA))/(1/Stopend))
         
    //       theoretical_concrete =  theo_con_STE
    //       // console.warn('Predepthcal>=stopend_length',theoretical_concrete)
    //       console.warn('Predepthcal>=stopend_length',theoretical_concrete,Predepthcal,this.state.concretevolume,1/Stopend)
    //       casingdepth = this.state.casingdepth !== "" && this.state.casingdepth !== null
    //                       ? this.state.casingdepth
    //                       : theoretical_concrete
    //       this.calmustpourDwall(casingdepth,checkd_wall,2,Stopend)
    //     }
       
    //   }else{
    //     if(Stopend!=null){
    //       console.warn('Stopend!=null',theoretical_concrete)
    //       this.calmustpourDwall(casingdepth,checkd_wall,1,Stopend)
    //     }
    //   }
      

    // }else{
    //   this.calmustpour(casingdepth)
    // }

    /** Calculate which concrete to use
     * Theoretical (ตามทฤษฎี) -> theoretical_concrete
     * Physical (กรอกเอง) -> this.state.casingdepth
     */

    var concretecal =
      this.state.casingdepth === "" || this.state.casingdepth === null
        ? theoretical_concrete
        : this.state.casingdepth
    // console.log("concrete_real_value_cal",  this.props.lastdepth, total_concrete_from_lastdepth , CONCRETE_VOLUME_HEIGHT)

    var concrete_real_value =
      tremievolume - this.props.tremiecutcumulativelength - concretecal //คอนกรีตอมหลังเท (ตามจริง) (m.)
    // console.log("ความสูงคอนกรีตสะสม", concrete_height.toFixed(2))
    // console.log(
    //   "ความลึกคอนกรีตจาก TCL (ตามทฤษฎี)",
    //   theoretical_concrete.toFixed(2)
    // )
    // console.log(
    //   "คอนกรีตอมหลังเท (ตามจริง) (m.)",
    //   concrete_real_value.toFixed(2)
    // )

    var tremiepossiblecutcount = Math.floor(concrete_real_value / 12)
    var tremiecut = 0
    var tremie_alreadycut = this.props.tremiecutcumulative
    var tremiecutlength = 0
    var temp_concrete = concrete_real_value
    var temp = []
    for (var i = 0; i < 20; i++) {
      var arrvalue = tremiepipe.length - 1 - tremiecut - tremie_alreadycut
      var lasttremielength = tremiepipe[arrvalue]
      var check12 = tremiecut == 0 ? temp_concrete >= 12 : true // Check if first cut must more than 12
      if (check12 && temp_concrete - lasttremielength >= 3) {
        temp_concrete = temp_concrete - lasttremielength
        tremiecut += 1
        tremiecutlength += tremielist[arrvalue].Length
        temp.push(tremielist[arrvalue].Length.toFixed(2))
        // console.log("CUTLength", tremielist[arrvalue].Length)
        // console.log("CUT", tremiecut)
        // console.log("อมหลังเท", temp_concrete)
      } else {
        break
      }
    }
    let remaining = (
      tremievolume -
      this.props.tremiecutcumulativelength -
      tremiecutlength
    ).toFixed(2)
    this.setState(
      {
        // cuttremieButton: !(tremiecut == 0),
        tremiepipeinstall: tremiepipe.length - this.props.tremiecutcumulative,
        tremiepipelength: (
          tremievolume - this.props.tremiecutcumulativelength
        ).toFixed(2),
        // cuttremiepipe: tremiecut,
        mustcuttremie: tremiecut,
        tremiepipecutlength2: tremiecutlength.toFixed(2),
        remainingtremiepiplength:
          remaining == 0 ? Math.abs(remaining).toFixed(2) : remaining,
        tremiepipeembeddedlength: (
          concrete_real_value - tremiecutlength
        ).toFixed(2),
        beforetremiepipeembeddedlength: concrete_real_value.toFixed(2),
        // tremietemp: temp,
        mustcutarr: temp,
        theorydepth: theoretical_concrete.toFixed(2),
        concreteleft: concrete_real_value.toFixed(2)
      },
      () => {
        console.log(
          "calculateTremie",
          this.state.tremiepipeembeddedlength,
          concrete_real_value,
          tremiecutlength
        )
      }
    )
    /**
    รวมความยาวท่อเทรมี่ที่ลง (ม.)
    จำนวนท่อเทรมี่ที่ตัด (ท่อน)
    ความยาวท่อที่ตัด (ม.)
    ความยาวท่อที่เหลือ (ม.)
    ความยาวท่อที่จมหลังตัด (ม.)
    */
    // Alert.alert('CUT', 'Possible Cut ' + tremiepossiblecutcount  +"\n" + "Real Cut " + tremiecut + " With total of " + tremiecutlength + " m. \n" + temp)
  }

  lastTruck() {
    var showlastview = true
    if(this.state.checkstopbetween == true){
      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.warn3"), [
        {
          text: "OK"
        }
      ])
      return
    }
    if (this.props.edit == true && this.state.newesttruck == false) {
      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.warn2"), [
        {
          text: "OK"
        }
      ])
      return
    } else {
      if (this.state.finishdrop) {
        this.setState({
          lastconcretetruckButton: !this.state.lastconcretetruckButton,
          showlastview
        })
      }
    }
  }

  calculateLeftTremieValue() {
    return this.props.tremiepipe.length - this.props.tremiecutcumulative
  }

  calculateLeftTremie() {
    var tremieleft =
      this.props.tremiepipe.length - this.props.tremiecutcumulative
    // console.warn("TREMIE LEFT = ", tremieleft)
    this.dryCalculate(tremieleft)
    this.setState({ locktremieEdit: true })
  }

  async dryCalculate(tremiecutted: int = 0) {
    const { tremiepipe, tremielist, tremievolume } = this.props

    /// Validate tremiecut not exceed tremiepip available
    if (tremiepipe.length < tremiecutted) {
      this.setState({
        err: true,
        tremiepipeinstall: 0,
        tremiepipelength: 0,
        // cuttremiepipe: tremiecut,
        tremiepipecutlength: 0,
        remainingtremiepiplength: 0,
        tremiepipeembeddedlength: 0,
        beforetremiepipeembeddedlength: 0,
        theorydepth: 0,
        concreteleft: 0
      })
      return
    }
    const DEPTH =
      (this.props.category == 3 || this.props.category == 5) &&
      this.state.Parentcheck == false
        ? Math.abs(this.props.depth_child)
        : Math.abs(this.props.depth)
    const DEPTH_child =
      this.props.category == 5 ? Math.abs(this.props.depth_child) : ""

    let checkd_wall =
      this.props.category == 5
        ? this.state.Predepth != "" && this.state.Predepth != null
          ? this.state.Predepth < DEPTH
            ? true
            : false
          : false
        : null //ถ้า == true เปลียนสูตรเป็นเหลี่มม

    console.warn(
      "DEPTH: " + DEPTH,
      "checkd_wall: " + checkd_wall,
      "size : " + this.state.size,
      "size_bp: " + this.state.size_bp
    )
    const SIZE =
      this.props.category == 5
        ? checkd_wall == true
          ? this.state.size
          : this.state.size_bp
        : this.state.size

    const CIRCLE_AREA = (Math.PI * Math.pow(SIZE, 2)) / 4
    const RECTANGLE_AREA = SIZE

    var concretecumulative =
      this.state.concretevolume + this.props.concretecumulative

    const AREA =
      this.props.category == 5
        ? checkd_wall == true
          ? RECTANGLE_AREA
          : CIRCLE_AREA
        : this.props.shape == 1
        ? CIRCLE_AREA
        : RECTANGLE_AREA // 1 use circle other use rectangle

    const CONCRETE_VOLUME_HEIGHT = 1 / AREA // คอนกรีต 1m^3 เทจะสูง
    // console.warn("CONCRETE_VOLUME_HEIGHT",CONCRETE_VOLUME_HEIGHT)
    var concrete_height = concretecumulative * CONCRETE_VOLUME_HEIGHT
    
    // console.log( this.state.concretevolume,this.props.concretecumulative, CONCRETE_VOLUME_HEIGHT)
    console.log(
      "DEPTH_child : " + DEPTH_child,
      "DEPTH :" + DEPTH,
      "concrete_height : " + concrete_height,
      "concretecumulative : " + concretecumulative,
      "CONCRETE_VOLUME_HEIGHT: " + CONCRETE_VOLUME_HEIGHT,
      "AREA : " + AREA,
      "SIZE : " + SIZE
    )
    var concrete_height_per_bus =
      this.state.concretevolume * CONCRETE_VOLUME_HEIGHT //ความสูงคอนกรีตต่อคัน
    let theoretical_concrete_overflow = ""
    let Predepth =
      this.props.category == 5
        ? checkd_wall == true
          ? await this.calPredepthDwall()
          : await this.calPredepth()
        : ""
    // var theoretical_concrete =
    //   this.props.category == 5
    //     ? checkd_wall == true
    //     ? Predepth
    //     : DEPTH_child - concrete_height
    //     : DEPTH - concrete_height // ความลึกคอนกรีตจาก TCL (ตามทฤษฎี) (m.)
    var theoretical_concrete =
      this.props.category == 5 ? Predepth : DEPTH - concrete_height // ความลึกคอนกรีตจาก TCL (ตามทฤษฎี) (m.)


    if (this.props.category == 5 && Predepth <= DEPTH && checkd_wall == false) {
      let data = {
        concrete_height_per_bus: concrete_height_per_bus,
        // theoretical_concrete:
        //   this.state.Parentcheck == false ? DEPTH - concrete_height : Predepth,
        DEPTH: DEPTH
      }

      data.theoretical_concrete = Predepth

      theoretical_concrete_overflow = this.calOverflow(data)
      if (theoretical_concrete_overflow != null) {
        console.warn(
          "theoretical_concrete_overflow",
          theoretical_concrete_overflow
        )
        theoretical_concrete = theoretical_concrete_overflow
      }
    } else if (
      this.props.category != 5 &&
      this.state.Predepth != null &&
      this.state.Predepth != ""
    ) {
      if (this.props.shape == 1) {
        let oldPredepth = await this.calPredepthArr(1)
        console.log(
          "oldPredepth",
          oldPredepth,
          "Predepth",
          this.state.Predepth,
          "concrete_height_per_bus",
          concrete_height_per_bus,
          "theoretical_concrete",
          this.state.Predepth - concrete_height_per_bus - oldPredepth
        )
        theoretical_concrete =
          this.state.Predepth - concrete_height_per_bus - oldPredepth
      } else {

        let oldPredepth = await this.calPredepthArr(2)
        console.log(
          "oldPredepth",
          oldPredepth,
          "Predepth",
          this.state.Predepth,
          "concrete_height_per_bus",
          concrete_height_per_bus,
          "theoretical_concrete",
          this.state.Predepth - concrete_height_per_bus - oldPredepth
        )
        theoretical_concrete =
          this.state.Predepth - concrete_height_per_bus - oldPredepth
      }
    }
    


    /** Calculate which concrete to use
     * Theoretical (ตามทฤษฎี) -> theoretical_concrete
     * Physical (กรอกเอง) -> this.state.casingdepth
     */
    // console.warn("theoretical_concrete", theoretical_concrete)
    console.warn(
      "theoretical_concrete_dry",
      theoretical_concrete,
      DEPTH - concrete_height,
      ' this.props.category', this.props.category
    )
    
    if(this.state.pretruckcheckstopbetween==true){
      console.warn('test')
    }
   
    if( this.props.category!=1&& this.props.category!=4){
      const stopend_length = this.state.mainpiledetail.stopend_length

      let old_theoretical_concrete = theoretical_concrete
      let Predepthcal = await this.calArrPredepthSTE()
    
      // console.warn('this.state.Predepth', this.state.Predepth,'old_concrete_high',old_concrete_high,'Predepth',Predepth)
      let casingdepth = this.state.casingdepth !== "" && this.state.casingdepth !== null
                          ? this.state.casingdepth
                          : theoretical_concrete
      let Stopend = await this.calStopend(casingdepth,stopend_length) 
      // console.warn('casingdepth,stopend_length,Predepthcal',casingdepth,stopend_length,Predepthcal,Stopend)
      if(this.state.checkstopbetween==true){

        await this.calstopbetween(Predepthcal,casingdepth,Stopend,stopend_length)
      }
      if(casingdepth<stopend_length&&Predepthcal<stopend_length){
        if(Stopend!=null){
          let theo_con_STE = Predepthcal-(this.state.concretevolume*(1/Stopend))

          // console.log('theo_con_STE Predepthcal<',theo_con_STE,'theoretical_concrete',theoretical_concrete,"this.state.Predepth",this.state.Predepth,"old_concrete_high",old_concrete_high,this.props.depth-old_concrete_high)
          theoretical_concrete =  theo_con_STE
          console.warn('Predepthcal<stopend_length',theoretical_concrete,Predepthcal,this.state.concretevolume,1/Stopend)
          casingdepth = this.state.casingdepth !== "" && this.state.casingdepth !== null
                          ? this.state.casingdepth
                          : theoretical_concrete
          await this.calmustpourDwall(casingdepth,checkd_wall,3,Stopend)
          
        }
       
      }else if(casingdepth<stopend_length&&Predepthcal>=stopend_length){
        if(Stopend!=null){
          let theo_con_STE = Predepthcal - (Predepthcal-stopend_length)-((this.state.concretevolume-((Predepthcal-stopend_length)*this.state.size))/Stopend)
         
          theoretical_concrete =  theo_con_STE
          // console.warn('Predepthcal>=stopend_length',theoretical_concrete)
          console.warn('Predepthcal>=stopend_length',theoretical_concrete,Predepthcal,this.state.concretevolume,Stopend,this.state.size)
          casingdepth = this.state.casingdepth !== "" && this.state.casingdepth !== null
                          ? this.state.casingdepth
                          : theoretical_concrete
          this.calmustpourDwall(casingdepth,checkd_wall,3,Stopend)
        }
       
      }else{
        if(Stopend!=null){
          console.warn('casingdepth<this.state.depth',casingdepth<this.state.depth,casingdepth,)
          if(casingdepth<this.props.depth){

            this.calmustpourDwall(casingdepth,checkd_wall,2,Stopend)

          }else{
            this.calmustpourDwall(casingdepth,checkd_wall,1,Stopend)
          }
         
          
        }
       
      }
      

    }else{
      let casingdepth = this.state.casingdepth !== "" && this.state.casingdepth !== null
                          ? this.state.casingdepth
                          : theoretical_concrete
      let Predepthcal = theoretical_concrete+ concrete_height_per_bus
      console.warn('this.state.pretruckcheckstopbetween==true',this.state.pretruckcheckstopbetween )
      if(this.state.pretruckcheckstopbetween==true){
        console.warn('Predepthcal,casingdepth',theoretical_concrete)
        // Predepthcal = theoretical_concrete+CONCRETE_VOLUME_HEIGHT
      }
      if(this.state.checkstopbetween==true){
       
        
        await this.calstopbetween(Predepthcal,casingdepth)
      }
      this.calmustpour(casingdepth)
    }
    var concretecal =
      this.state.casingdepth !== "" && this.state.casingdepth !== null
        ? this.state.casingdepth
        : theoretical_concrete
       
    // (this.state.casingdepth == "" || this.state.casingdepth == null) &&
    // (this.state.cuttremiepipe == 0 ||
    //   this.state.cuttremiepipe == "" ||
    //   this.state.cuttremiepipe == null)
    //   ? theoretical_concrete
    //   : this.state.casingdepth
    var concrete_real_value =
      tremievolume - this.props.tremiecutcumulativelength - concretecal //คอนกรีตอมหลังเท (ตามจริง) (m.)
    // console.log("concrete_real_value", tremievolume , this.props.tremiecutcumulativelength , concretecal)
    var tremiepossiblecutcount = Math.floor(concrete_real_value / 12)
    var tremiecut = 0
    var tremie_alreadycut = this.props.tremiecutcumulative
    var tremiecutlength = 0
    var temp_concrete = concrete_real_value
    var temp = []
    for (var i = 0; i < tremiecutted; i++) {
      var arrvalue = tremiepipe.length - 1 - tremiecut - tremie_alreadycut
      var lasttremielength = tremiepipe[arrvalue]
      var check12 = tremiecut == 0 ? temp_concrete >= 12 : true // Check if first cut must more than 12
      // var tremielistlength = tremielist[arrvalue].Length != undefined ? tremielist[arrvalue].Length:0
      // if (check12) {
      if (tremielist[arrvalue] != null) {
        temp_concrete = temp_concrete - lasttremielength
        tremiecut += 1
        tremiecutlength += tremielist[arrvalue].Length
        // temp.push(tremielist[arrvalue].Length.toFixed(2))
        console.log("ความยาวท่อเทมมี่", tremielist[arrvalue])
        console.log("CUT", tremiecut)
        // console.log("อมหลังเท", temp_concrete)
        // // }
        // temp_concrete = temp_concrete - lasttremielength
        // tremiecut += 1
        // tremiecutlength += tremielist[arrvalue].Length

        // Not check
        temp.push(tremielist[arrvalue].Length.toFixed(2))
      } else {
        this.setState({
          err: true,
          tremiepipeinstall: 0,
          tremiepipelength: 0,
          // cuttremiepipe: tremiecut,
          tremiepipecutlength: 0,
          remainingtremiepiplength: 0,
          tremiepipeembeddedlength: 0,
          // beforetremiepipeembeddedlength: 0,
          theorydepth: 0,
          concreteleft: 0
        })
        return
      }
    }
    let remaining = (
      tremievolume -
      this.props.tremiecutcumulativelength -
      tremiecutlength
    ).toFixed(2)
    this.setState(
      {
        // cuttremieButton: !(tremiecut == 0),
        tremiepipeinstall: tremiepipe.length - this.props.tremiecutcumulative,
        tremiepipelength: (
          tremievolume - this.props.tremiecutcumulativelength
        ).toFixed(2),
        // cuttremiepipe: tremiecut,
        tremiepipecutlength: tremiecutlength.toFixed(2),
        remainingtremiepiplength:
          remaining == 0 ? Math.abs(remaining).toFixed(2) : remaining,
        tremiepipeembeddedlength: (
          concrete_real_value - tremiecutlength
        ).toFixed(2),
        beforetremiepipeembeddedlength: concrete_real_value.toFixed(2),
        tremietemp: temp,
        theorydepth: theoretical_concrete.toFixed(2),
        concreteleft: concrete_real_value.toFixed(2),
        err: false
      },
      () => {
        console.log(
          "dryCalculate",
          this.state.tremiepipeembeddedlength,
          concrete_real_value,
          tremiecutlength
        )
      }
    )
  }
  calOverflow = data => {
    let Predepth = this.calPredepthover()
    // let Predepth =data.Predepth
    const CIRCLE_AREA = 1 / ((Math.PI * Math.pow(this.state.size_bp, 2)) / 4)
    const RECTANGLE_AREA = 1 / this.state.size
    // console.warn(
    //   "size_bp:" + this.state.size_bp,
    //   Math.PI * Math.pow(this.state.size_bp, 2)
    // )
    let D_theoretical_concrete = data.DEPTH - data.theoretical_concrete

    let overflow =
      data.DEPTH - (D_theoretical_concrete / CIRCLE_AREA) * RECTANGLE_AREA
    console.warn(
      "Predepth: " + Predepth,
      "DEPTH: " + data.DEPTH,
      "theoretical_concrete:" + data.theoretical_concrete,
      "CIRCLE_AREA: " + CIRCLE_AREA,
      "RECTANGLE_AREA: " + RECTANGLE_AREA,
      "concrete_height_per_bus: " + data.concrete_height_per_bus,
      "D_theoretical_concrete: " + D_theoretical_concrete
    )
    return overflow
  }

  calPredepth = async () => {
    var concretecumulative =
      this.state.concretevolume + this.props.concretecumulative

    const CONCRETE_VOLUME_HEIGHT =
      1 / ((Math.PI * Math.pow(this.state.size_bp, 2)) / 4) // คอนกรีต 1m^3 เทจะสูง
    let bp_depth = Math.abs(this.props.depth_child)
    var concrete_height = concretecumulative * CONCRETE_VOLUME_HEIGHT

    var concrete_height_per_bus =
      this.state.concretevolume * CONCRETE_VOLUME_HEIGHT
    let concrete_height_per_bus_perTruck =
      this.state.PreTruck.ConcreteTruckRegister != null
        ? this.state.PreTruck.ConcreteTruckRegister.TruckConcreteVolume *
          CONCRETE_VOLUME_HEIGHT
        : null
    let theoretical_No_depth = bp_depth - concrete_height
    let oldPredepth = await this.calPredepthArr(1)
    console.log("oldPredepth", oldPredepth)
    let theoretical_depth =
      this.state.Predepth != null && this.state.Predepth != ""
        ? this.state.Predepth - concrete_height_per_bus - oldPredepth
        : null

    if (
      this.props.lastdepth != null &&
      this.props.lastdepthconcrete != null &&
      this.props.category != 5 &&
      this.state.Predepth != ""
    ) {
      var concrete_from_lastdepth =
        this.props.concretecumulative - this.props.lastdepthconcrete
      if (this.props.edit == true) {
        if (this.props.lastdepth != this.state.Predepth) {
          concrete_from_lastdepth =
            this.props.concretecumulative -
            (this.props.lastdepthconcrete - this.state.concretevolume)
        }
      }
      var total_concrete_from_lastdepth =
        this.state.concretevolume + concrete_from_lastdepth
      theo_height =
        this.state.Predepth -
        total_concrete_from_lastdepth * CONCRETE_VOLUME_HEIGHT

      theoretical_depth = theo_height
    }

    let Predepth =
      this.state.Predepth != null && this.state.Predepth != ""
        ? theoretical_depth
        : theoretical_No_depth
    console.warn(
      "calPredepthB",
      "Predepth",
      Predepth,
      "bp_depth - concrete_height",
      bp_depth - concrete_height,
      "bp_depth",
      bp_depth,
      "concrete_height",
      concrete_height
    )
    return Predepth
  }

  calPredepthover = () => {
    var concretecumulative = this.props.concretecumulative

    const CONCRETE_VOLUME_HEIGHT =
      1 / ((Math.PI * Math.pow(this.state.size_bp, 2)) / 4) // คอนกรีต 1m^3 เทจะสูง
    var concrete_height = concretecumulative * CONCRETE_VOLUME_HEIGHT
    let depth = Math.abs(this.props.depth_child)
    let Predepth =
      this.state.Predepth != "" && this.state.Predepth != null
        ? this.state.Predepth
        : depth - concrete_height
    console.warn("calPredepthO", this.state.Predepth, depth - concrete_height)
    return Predepth
  }
  calArrPredepthSTE = async()=>{

    const CIRCLE_AREA =  ((Math.PI * Math.pow(this.state.size_bp, 2)) / 4)
    const RECTANGLE_AREA =  this.state.size

    const depth_bp =  Math.abs(this.props.depth_child)
    const depth_dwall=  Math.abs(this.props.depth)
  
    let truckarrbeteewn = await this.state.PredepthArr
    // if(this.props.edit&&truckarrbeteewn.length>1){
    //   console.log('truckarrbeteewn[0] before',truckarrbeteewn[0],)
    //   truckarrbeteewn.shift()
    //   console.log('truckarrbeteewn[0] after',truckarrbeteewn[0])
    // }
    const stopend_length = this.state.mainpiledetail.stopend_length
    let last_userdepth = this.state.Predepth!=undefined&&this.state.Predepth!=null&&this.state.Predepth!='' ? this.state.Predepth:Math.abs(this.props.depth)
    let Predepth = last_userdepth
    
    // let lastconcretecumulative = this.props.concretecumulative
    // let calPreconcretecumulative = truckarrbeteewn.map((item)=>{
    //   lastconcretecumulative = lastconcretecumulative-item.ConcreteTruckRegister.TruckConcreteVolume 
    // })
    // await Promise.all(calPreconcretecumulative)
    
    console.warn("last_userdepth",last_userdepth,'this.state.Predepth',this.state.Predepth,'this.props.depth',this.props.depth,'this.state.Predepth!=undefined&&this.state.Predepth!=null',this.state.Predepth!=undefined&&this.state.Predepth!=null)
      if(truckarrbeteewn.length>0){
       
         for(let i = truckarrbeteewn.length-1;i>=0;i--){
          console.warn("Predepth",Predepth)
          let data = truckarrbeteewn[i].ConcreteTruckRegister
          let concrete_height =data.TruckConcreteVolume* (1/RECTANGLE_AREA)
          console.warn('truckarrbeteewn.length',truckarrbeteewn.length,'TruckNo',data.TruckNo,'i',i,'TruckConcreteVolume',data.TruckConcreteVolume)
          if(this.props.category!=2&&this.props.category!=3){

          
          if(Predepth>=depth_dwall){
            concrete_height =data.TruckConcreteVolume* (1/CIRCLE_AREA)
           if( Predepth-concrete_height>=depth_dwall){
            
            Predepth = Predepth - concrete_height
            // console.log('Predepth -h>=depth_dwall',Predepth)
           }else if(Predepth-concrete_height<depth_dwall){
            // this.state.concretevolume * CONCRETE_VOLUME_HEIGHT lastconcretecumulative
            let D_theoretical_concrete = depth_dwall -concrete_height
            Predepth = Predepth- (Predepth-depth_dwall)  - (((depth_dwall-(Predepth-(concrete_height)))/(1/CIRCLE_AREA))*(1/RECTANGLE_AREA))
            // (depth_dwall-(Predepth-(data.TruckConcreteVolume*(1/CIRCLE_AREA)))/1/CIRCLE_AREA)*1/RECTANGLE_AREA
            // Predepth = depth_dwall - (D_theoretical_concrete / (1/CIRCLE_AREA)) * (1/RECTANGLE_AREA)
           
            // console.log('OVerflow',Predepth,'depth_dwall',depth_dwall,'D_theoretical_concrete',D_theoretical_concrete,'1/CIRCLE_AREA',1/CIRCLE_AREA,'1/RECTANGLE_AREA',1/RECTANGLE_AREA,concrete_height)
           }
          }else{
            if(Predepth- concrete_height<stopend_length&&Predepth>=stopend_length){
  
              let Stopend = await this.calStopend(Predepth,stopend_length) 
              Predepth = Predepth - (Predepth-stopend_length)-((data.TruckConcreteVolume-((Predepth-stopend_length)*RECTANGLE_AREA))/Stopend)
              console.log('Predepth>=stopend_length')
            }else if(Predepth- concrete_height<stopend_length&&Predepth<stopend_length){
              let Stopend = await this.calStopend(Predepth,stopend_length) 
              Predepth = Predepth - (this.state.concretevolume*(1/Stopend))
              console.log('Predepth<stopend_length',Predepth)
            }else{
             
              Predepth = Predepth - concrete_height
              console.log('last_userdepth < stopend_length',Predepth)
             
            }
          }
          }else{
            if(Predepth- concrete_height<stopend_length&&Predepth>=stopend_length){
  
              let Stopend = await this.calStopend(Predepth,stopend_length) 
              Predepth = Predepth - (Predepth-stopend_length)-((data.TruckConcreteVolume-((Predepth-stopend_length)*RECTANGLE_AREA))/Stopend)
              console.log('Predepth>=stopend_length')
            }else if(Predepth- concrete_height<stopend_length&&Predepth<stopend_length){
              let Stopend = await this.calStopend(Predepth,stopend_length) 
              console.log('Predepth<stopend_length before',Predepth,Stopend)
              Predepth = Predepth - (this.state.concretevolume*(1/Stopend))
              console.log('Predepth<stopend_length after',Predepth)
            }else{
             
              Predepth = Predepth - concrete_height
              console.log('last_userdepth < stopend_length',Predepth)
             
            }
          }
         
  
        }
  
        
      } 
      console.log('calArrPredepthSTE after Predepth',Predepth)
      return Predepth
   
   
      
  }

  calstopbetween=(Predepthcal,currentdepth,Stopend,stopend_length)=>{
    let newconvol =0
    let CIRCLE_AREA = ((Math.PI * Math.pow(this.state.size_bp, 2)) / 4)
    const RECTANGLE_AREA =  this.state.size
    if(this.props.category==1||this.props.category==4){
      CIRCLE_AREA = ((Math.PI * Math.pow(this.state.size, 2)) / 4)
      newconvol = (Predepthcal-currentdepth)*CIRCLE_AREA
      console.warn('Predepthcal',Predepthcal,"currentdepth",currentdepth,'CIRCLE_AREA',CIRCLE_AREA)
      this.setState({concretevolumestop:parseFloat(newconvol).toFixed(2)})
    }else{
      // await this.calstopbetween(Predepthcal,casingdepth,Stopend,stopend_length)
      if(this.props.category==2||this.props.category==3){
        if(Stopend!=null){
          if(currentdepth<stopend_length){
          
            newconvol = (Predepthcal-currentdepth)*Stopend
            console.warn('currentdepth<stopend_length',Predepthcal,currentdepth,Stopend,newconvol)
            this.setState({concretevolumestop:parseFloat(newconvol).toFixed(2)})
          }else{
            newconvol = (Predepthcal-currentdepth)*RECTANGLE_AREA
            console.warn('currentdepth<stopend_length else','Predepthcal',Predepthcal,'currentdepth',currentdepth,'RECTANGLE_AREA',RECTANGLE_AREA,newconvol)
            this.setState({concretevolumestop:parseFloat(newconvol).toFixed(2)})
          }
        }else{
          newconvol = (Predepthcal-currentdepth)*RECTANGLE_AREA
          console.warn('Stopend!=null',Predepthcal,currentdepth,RECTANGLE_AREA,newconvol)
          this.setState({concretevolumestop:parseFloat(newconvol).toFixed(2)})
        }
      }else{
      console.log('Stopbetween',currentdepth,stopend_length)
        if(currentdepth>this.state.depth){
          newconvol = (Predepthcal-currentdepth)*CIRCLE_AREA
          
          this.setState({concretevolumestop:parseFloat(newconvol).toFixed(2)})
        }else{
          
          if(Stopend!=null){
            if(currentdepth<stopend_length){
              
              newconvol = (Predepthcal-currentdepth)*Stopend
              
              this.setState({concretevolumestop:parseFloat(newconvol).toFixed(2)})
            }else{
              newconvol = (Predepthcal-currentdepth)*RECTANGLE_AREA
              this.setState({concretevolumestop:parseFloat(newconvol).toFixed(2)})
            } 
          }else{
            newconvol = (Predepthcal-currentdepth)*RECTANGLE_AREA
            this.setState({concretevolumestop:parseFloat(newconvol).toFixed(2)})
          }
        }
      }
    }


  }
  calPredepthArr = async id => {
    // console.warn("test")
    // console.warn(
    //   "calPredepthArr",
    //   this.props.PredepthArr != null && this.props.PredepthArr.length > 0
    // )
    let PredepthArr = this.props.PredepthArr
    let concrete_height = 0
    if (PredepthArr != null && PredepthArr.length > 0) {
      let lastconcretecumulative = this.props.concretecumulative

      let PredepthArrMap = await PredepthArr.map((item, index) => {
        console.warn("calPredepthArr for", item)
        lastconcretecumulative =
          lastconcretecumulative -
          item.ConcreteTruckRegister.TruckConcreteVolume
        var concretecumulative =
          item.ConcreteTruckRegister.TruckConcreteVolume +
          lastconcretecumulative

        let CONCRETE_VOLUME_HEIGHT = null
        if (this.props.category == 5) {
          if (id == 1) {
            CONCRETE_VOLUME_HEIGHT =
              1 / ((Math.PI * Math.pow(this.state.size_bp, 2)) / 4) // คอนกรีต 1m^3 เทจะสูง
          } else if (id == 2) {
            CONCRETE_VOLUME_HEIGHT = 1 / this.state.size
          }
        } else {
          const SIZE = this.state.size

          const CIRCLE_AREA = (Math.PI * Math.pow(SIZE, 2)) / 4
          const RECTANGLE_AREA = SIZE
          if (this.props.shape == 1) {
            CONCRETE_VOLUME_HEIGHT = 1 / CIRCLE_AREA
          } else {
            CONCRETE_VOLUME_HEIGHT = 1 / RECTANGLE_AREA
          }
        }

        // let bp_depth = Math.abs(this.props.depth_child)
        console.warn("CONCRETE_VOLUME_HEIGHT", CONCRETE_VOLUME_HEIGHT)
        console.warn("concretecumulative", concretecumulative)
        console.warn(" lastconcretecumulative", lastconcretecumulative)
        concrete_height =
          concrete_height +
          item.ConcreteTruckRegister.TruckConcreteVolume *
            CONCRETE_VOLUME_HEIGHT
      })
      let Newconcrete_height = await Promise.all(PredepthArrMap).then(() => {
        return concrete_height
      })
      console.warn("calPredepthArr  cal", concrete_height)
      // return concrete_height
      if (
        concrete_height == null ||
        concrete_height == "" ||
        concrete_height == undefined
      ) {
        return 0
      } else {
        return concrete_height
      }

      // try {
      //   for (i = 0; i  >this.props.PredepthArr.length; i++) {
      //     console.warn("calPredepthArr for")
      //     console.warn(i, this.props.PredepthArr[i])
      //   }
      // } catch (e) {
      //   console.warn("calPredepthArr for error", e)
      // }
    } else {
      return 0
    }
  }
  calPredepthDwall = async () => {
    var concretecumulative =
      this.state.concretevolume + this.props.concretecumulative

    const CONCRETE_VOLUME_HEIGHT = 1 / this.state.size // คอนกรีต 1m^3 เทจะสูง

    // let bp_depth =Math.abs(this.props.depth_child)
    let D_wall_depth = Math.abs(this.props.depth)
    var concrete_height = concretecumulative * CONCRETE_VOLUME_HEIGHT

    var concrete_height_per_bus =
      this.state.concretevolume * CONCRETE_VOLUME_HEIGHT
    let concrete_height_per_bus_perTruck =
      this.state.PreTruck.ConcreteTruckRegister != null
        ? this.state.PreTruck.ConcreteTruckRegister.TruckConcreteVolume *
          CONCRETE_VOLUME_HEIGHT
        : null
    // this.calPredepthArr(2)
    let oldPredepth = await this.calPredepthArr(2)
    console.log("oldPredepth", oldPredepth)
    let theoretical_No_depth = D_wall_depth - concrete_height
    let theoretical_depth =
      this.state.Predepth != null && this.state.Predepth != ""
        ? this.state.Predepth - concrete_height_per_bus - oldPredepth
        : null

    if (
      this.props.lastdepth != null &&
      this.props.lastdepthconcrete != null &&
      this.props.category != 5 &&
      this.state.Predepth != ""
    ) {
      var concrete_from_lastdepth =
        this.props.concretecumulative - this.props.lastdepthconcrete
      if (this.props.edit == true) {
        if (this.props.lastdepth != this.state.Predepth) {
          concrete_from_lastdepth =
            this.props.concretecumulative -
            (this.props.lastdepthconcrete - this.state.concretevolume)
        }
      }
      var total_concrete_from_lastdepth =
        this.state.concretevolume + concrete_from_lastdepth
      theo_height =
        this.state.Predepth -
        total_concrete_from_lastdepth * CONCRETE_VOLUME_HEIGHT

      theoretical_depth = theo_height
    }

    let Predepth =
      this.state.Predepth != null && this.state.Predepth != ""
        ? theoretical_depth
        : theoretical_No_depth
    console.warn(
      "calPredepthB",
      Predepth,
      D_wall_depth - concrete_height,
      D_wall_depth,
      concrete_height,
      theoretical_depth
    )
    return Predepth
  }

  calculateTremieArr(tremielist = this.props.tremielist) {
    var temp = []
    tremielist.map((data, index) => {
      temp.push(data.Length)
    })
    console.log("temp cal", temp)
    this.setState({ tremiepipe: temp })
  }

  calfinalconcreteleftover (){
    let concreteleft_height = this.state.final_overheight !==""&&this.state.final_overheight !==null? parseFloat(this.state.final_overheight):0
    let concreteleft_width = this.state.final_overwidth !==""&&this.state.final_overwidth !==null? parseFloat(this.state.final_overwidth):0
    let concreteleft_length = this.state.final_overLength !==""&&this.state.final_overLength !==null? parseFloat(this.state.final_overLength):0
    let a =(parseFloat(concreteleft_width)+ parseFloat(concreteleft_length))/2
    let resuth_concretleft =(( Math.PI * concreteleft_height )* (((3/4)*(Math.pow(a, 2)))+Math.pow(concreteleft_height, 2)))/6
    let fixed_resuth_concretleft = parseFloat(Math.abs(resuth_concretleft < 0 ? 0 :resuth_concretleft)).toFixed(2)
    let final_concrete_over_volume = isNaN(fixed_resuth_concretleft) == true ? 0 : fixed_resuth_concretleft
    let res_final_calrealuse = this.state.concretevolume-final_concrete_over_volume
    
    let Fixed_calrealuseres = parseFloat(
      Math.abs(res_final_calrealuse < 0 ? 0 :res_final_calrealuse)
    ).toFixed(2)
    let final_calrealuseres = isNaN(Fixed_calrealuseres) == true ? 0 : Fixed_calrealuseres
    this.setState({final_concreteleftovervolume:final_concrete_over_volume,final_concretevolumecal:final_calrealuseres})
  }

  calStopend = (currentLength,stopend_length) =>{
    
    
    // let Length = this.state.size
    // const stopend_length = this.state.mainpiledetail.stopend_length
  
    const STEArea =  this.state.mainpiledetail.STEArea
    const STELeft =  this.state.mainpiledetail.STELeft
    const STERigth =  this.state.mainpiledetail.STERigth
    const STETop =  this.state.mainpiledetail.STETop
    const STEWidth =  this.state.mainpiledetail.STEWidth

    const thickness =  this.state.mainpiledetail.dimension1
    const Length = this.state.mainpiledetail.dimension2
    const seqtype = this.state.mainpiledetail.seqtype
   
    let Stopend = null
    if(seqtype=="P"){
      Stopend = (thickness*(Length-(2*STEWidth)))+(4*(1/2)*STETop*(STERigth+((thickness-STELeft)/2)))
    }else if(seqtype=="C"){
      console.warn(thickness,Length,'STEArea',STEArea,'Stopend',Stopend)
      Stopend = (thickness*Length)+(2*STEArea)
      // console.warn('thickness*Length',thickness*Length,'2*STEArea',2*STEArea,'STEArea',STEArea,thickness,Length)
    }else{
      return null
    }
    // console.warn(' this.state.mainpiledetail.', this.state.mainpiledetail,'Stopend',Stopend)
    return Stopend
    
    
    // this.setState({stopendPrimary,stopendClosing})



  }

  calmustpour=(concretecal)=>{

    const Diameter = this.state.size
    const CIRCLE_AREA = (Math.PI * Math.pow(Diameter, 2)) / 4
    const topcasing =this.state.topcasing
    const cutoff = parseFloat(this.state.mainpiledetail.cutoff)

    const overcast = parseFloat(this.state.overcast)
    const mustpour = CIRCLE_AREA * (concretecal-(topcasing-(cutoff+overcast)))

    console.log('mustpour:'+mustpour+'\n CIRCLE_AREA: '+CIRCLE_AREA+' \n'+'topcasing: '+topcasing+'\n'+'cutoff: '+cutoff+'\n'+'overcast: '+overcast+'\n')
    // this.setState({concretemustpour:mustpour})
    this.setState({concretemustpour:  parseFloat(
      mustpour < 0 ? 0 : mustpour
    ).toFixed(2)})
    return
  }
  calmustpourDwall=(casingdepth,checkd_wall,passstop,Stopend)=>{


    const AREA =this.state.size
    const Diameter = this.state.size_bp
    const CIRCLE_AREA = (Math.PI * Math.pow(Diameter, 2)) / 4
    const topcasing =this.state.topcasing
    const cutoff = parseFloat(this.state.mainpiledetail.cutoff)
    const DEPTH =
    this.props.depth
    const DEPTH_child =
      this.props.category == 5 ? Math.abs(this.props.depth_child) : ""
    const stopend_length = this.state.mainpiledetail.stopend_length
    // const oldPredepth =this.calPredepthArr(1)
    const overcast = parseFloat(this.state.overcast)
    const thickness =  this.state.mainpiledetail.dimension1
    const Length = this.state.mainpiledetail.dimension2
    // const mustpour = CIRCLE_AREA * (concretecal-(topcasing-(cutoff+overcast)))
    let mustpour = 0
    // console.warn('passstop',passstop,'casingdepth',casingdepth,'AREA',AREA,'Stopend',Stopend,'topcasing',topcasing,'cutoff',cutoff,'overcast',overcast)
      if(this.props.category==2||this.props.category==3){
        
        if(passstop==1){
          mustpour = AREA *  (casingdepth-(topcasing-(cutoff+overcast)))
        }else if(passstop==2){
          mustpour =( (Stopend) *  (stopend_length-(topcasing-(cutoff+overcast))))+((thickness*Length)*(casingdepth-stopend_length))
          console.warn('mustpour',mustpour,'Stopend',Stopend,'casingdepth',casingdepth,'topcasing',topcasing,'cutoff',cutoff,'overcast',overcast,'thickness*Length',thickness*Length,'stopend_length',stopend_length)
        }else if(passstop==3){
          mustpour = (Stopend) *  (casingdepth-(topcasing-(cutoff+overcast)))
        }
         
      }else if(this.props.category==5){
        console.warn(this.props.category,passstop,checkd_wall)
        if(passstop==1){
          let TOP_vol =  CIRCLE_AREA*(casingdepth-DEPTH)
          let DWALL_vol =  AREA*(DEPTH-stopend_length)
          let STE_vol =  Stopend*(stopend_length-(topcasing-(cutoff+overcast)))
          // console.warn((cutoff+overcast)+',',topcasing-(cutoff+overcast)+',',stopend_length-(topcasing-(cutoff+overcast))+',')
          // console.warn(cutoff,overcast,cutoff+overcast,overcast.IsSt)
          // console.warn(parseFloat(overcast),parseFloat(cutoff),parseFloat(cutoff)+parseFloat(overcast))
          console.warn(CIRCLE_AREA,casingdepth,DEPTH )
          mustpour = TOP_vol+DWALL_vol+STE_vol
          console.log('passstop==1','mustpour:'+mustpour+'\nTOP_vol:'+TOP_vol+'\nDWALL_vol:'+DWALL_vol+'\nSTIP_vol:'+STE_vol)
        }else if(passstop==2){
          let DWALL_vol =  AREA*(casingdepth-stopend_length)
          let STE_vol =  Stopend*(stopend_length-(topcasing-(cutoff+overcast)))
          mustpour = DWALL_vol+STE_vol
         
          console.log('passstop==2','mustpour:'+mustpour+'\nDWALL_vol:'+DWALL_vol+'\nSTE_vol:'+STE_vol)
          console.log('passstop==2 DWALL_vol',AREA,casingdepth,stopend_length )
          console.log('passstop==2 STIP_vol',Stopend,stopend_length,topcasing,cutoff,overcast )
        }else if(passstop==3){
       
          let STE_vol =  Stopend*(casingdepth-(topcasing-(cutoff+overcast)))
          mustpour = STE_vol
          console.log('passstop==3','mustpour:'+mustpour+'\nSTE_vol:'+STE_vol,'Stopend',Stopend,'casingdepth',casingdepth,'topcasing',topcasing,'cutoff',cutoff,'overcast',overcast)
        }
            
      }
      console.log('calmustpourDwall','DEPTH',DEPTH,'stopend_length',stopend_length,'AREA',AREA,'mustpour',mustpour)

    // console.log('mustpour:'+mustpour+'\n CIRCLE_AREA: '+CIRCLE_AREA+' \n'+'topcasing: '+topcasing+'\n'+'cutoff: '+cutoff+'\n'+'overcast: '+overcast+'\n')

    this.setState({concretemustpour:  parseFloat(
      mustpour < 0 ? 0 : mustpour
    ).toFixed(2)})
    return
  }


  disabledLastTruck() {
    this.setState({ lastconcretetruckButtonEnable: false })
  }

  _backButton() {
    if (Actions.currentScene == "dropconcrete") {
      Actions.pop()
      return true
    }
    return false
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("dcback", () => this._backButton())
  }
  componentDidMount() {
    BackHandler.addEventListener("dcback", () => this._backButton())
    this.fetch()
    var pile33 = null
    var pile = null
    var pileMaster = null
    var pileMaster91 = null
    var pile10_1 = null
    
    var piledata
    if (
      this.props.pileAll[this.props.pileid] &&
      this.props.pileAll[this.props.pileid]["10_3"]
    ) {
      pile = this.props.pileAll[this.props.pileid]["10_3"].data
      pile10_1 = this.props.pileAll[this.props.pileid]["10_1"].data
      pileMaster = this.props.pileAll[this.props.pileid]["10_3"].masterInfo
      pileMaster91 = this.props.pileAll[this.props.pileid]["9_1"].masterInfo
    }

   
    
    if (
      this.props.pileAll[this.props.pileid] &&
      this.props.pileAll[this.props.pileid]["3_3"]
    ) {
      pile33 = this.props.pileAll[this.props.pileid]["3_3"].data
      // pileData.payload["3_3"].data.top
    }
    if(pile &&pile.LastDrillingDepth){
      console.warn('LastDrillingDepth',pile.LastDrillingDepth)
      this.setState({LastDrillingDepth:pile10_1.LastDrillingDepth})
    }
    if(pile10_1 &&pile10_1.Overcast){
      
      this.setState({overcast:parseFloat(pile10_1.Overcast).toFixed(2)})
    }
    if (pileMaster) {

      if (pileMaster.TheoreticalVolume) {
        this.setState({
          concretetheoreticalvolume: pileMaster.TheoreticalVolume.Qty
        })
      }

      if (pileMaster.PileInfo) {
        var topcasing = 0

        if (pile && pile.TopCasing) {
          topcasing = pile.TopCasing
        }
        if (pile33 && pile33.top) {
          topcasing = pile33.top
        }

        // if ( pileMaster.PileInfo.Dia) {
        //   console.warn(pileMaster.PileInfo)
        //   dia = pileMaster.PileInfo.Dia
        // }
        this.setState(
          {
            topcasing: topcasing,
            cutoff:pileMaster.PileInfo.CutOff,
            size: pileMaster.size,
            size_bp: pileMaster.size_bp,

          },
          () => {
            var showlastview = true
            if (this.state.topcasing - this.state.cutoff < 4) {
              // this.disabledLastTruck()
              // this.setState({ depth: 'low' })
              showlastview = false
            }
            this.setState({ showlastview })
          }
        )
      }

      if (pileMaster.ConcreteInfo) {
        this.setState({
          qty: pileMaster.ConcreteInfo.Qty,
          slumpmax: pileMaster.ConcreteInfo.SlumpMax,
          slumpmin: pileMaster.ConcreteInfo.SlumpMin,
          strength: pileMaster.ConcreteInfo.Strength,
          Unit: pileMaster.ConcreteInfo.Unit
        })
      }

      if (pileMaster.PileDetail) {
        this.setState({
          piledetail: pileMaster.PileDetail
        })
      }
      if (pileMaster.RemainConcrete	) {
        let remaindata = [{
          key: 0,
          section: true,
          label: I18n.t("mainpile.DropConcrete.final_concrete_over_use")
        }]
       
        pileMaster.RemainConcrete.map((data, index) => {
            // console.warn(data)
            remaindata.push({
              key: index + 1,
              label: data.Name,
              id: data.Id,
       
            })
          })
       

        this.setState({
          final_concreteusedata: remaindata
        })
      }
      if (pileMaster91) {
        if (pileMaster91.PileDetail) {
          this.setState({
            Parentcheck:
              pileMaster91.PileDetail.pile_id ==
              pileMaster91.PileDetail.parent_id
                ? true
                : false
          })
        }
      }
      if (pileMaster.ApprovedUser) {
        this.setState({ approveuser: pileMaster.ApprovedUser })
      }

      if (this.props.PreTruckEnd) {
        this.setState({ predropconcrete: this.props.PreTruckEnd })
      }
      if (this.props.Predepth) {

        this.setState({ Predepth: this.props.Predepth })
      }
      if (this.props.PreTruck) {

        this.setState({ PreTruck: this.props.PreTruck },()=>{
            // if(this.state.PreTruck.StopPouringConcrete==true){
            //   // console.log('pretruckcheckstopbetween',this.state.PreTruck.pretruckcheckstopbetween,this.state.PreTruck.ConcreteCalculateVolumn)
            //   this.setState({pretruckcheckstopbetween:true,pretruckConcreteVolstop:this.state.PreTruck.ConcreteCalculateVolumn})
            // }
        })
      }
      if(this.props.pretruckcheckstopbetween){
        this.setState({pretruckcheckstopbetween:true})
      }
      if(this.props.StopPourconVol){

        this.setState({pretruckConcreteVolstop:this.props.StopPourconVol})
      }

      if (this.props.PredepthArr) {
        this.setState({ PredepthArr: this.props.PredepthArr })
      }
      if (this.props.Precutsink) {

        this.setState({
          Precutsink: this.props.Precutsink != "" ? this.props.Precutsink : 0
        })
      }

      if(this.props.item){
        this.setState({mainpiledetail:this.props.item},()=>{
          // if( this.props.category!=1&&this.props.category!=4){

          // this.calStopend()
          // }else{
          //   // this.calmustpour()
          // }
        })
      }
      // if(this.props.item&&this.props.category!=1&&this.props.category!=4){
      //   console.warn('item piledetail',this.props.item)
      //   this.calStopend()
      // }else{

      // }

      if (this.props.edit && this.props.data) {
        console.warn("edit",this.props.data ,'RemainConcretePileNo',this.props.data.RemainConcretePileNo)

        this.setState(
          {
            Edit_Flag: this.props.Edit_Flag,
            concretevolume: this.props.data.concretevolume,
            startdrop: this.props.data.startconcrete,
            finishdrop: this.props.data.endconcrete,
            truck: this.props.data.truckno,
            truckid: this.props.data.truckid,
            casingdepth:
              this.props.data.depth === null ? "" : this.props.data.depth,
            cuttremiepipe: this.props.data.tremiecutcount,
            cuttremieButton:
              this.props.data.tremiecutcount !== null &&
              this.props.data.tremiecutcount !== "" &&
              this.props.data.tremiecutcount !== 0
                ? true
                : false,
            dropconcreteimage: this.props.data.imageconcrete,
            finalconcreteimage: this.props.data.imagelasttruck,
            finalconcreteoverimage:this.props.data.ImageConcreteRecordDump,
            final_pvcheight: this.props.data.LastTruckPVCConcreteHeight || 0,
            final_plummetheight:
              this.props.data.LastTruckPlummetConcreteHeight || 0,
            final_concretevolume:
              this.props.data.LastTruckConcreteVolume !== "" &&
              this.props.data.LastTruckConcreteVolume !== null
                ? this.props.data.LastTruckConcreteVolume.toString()
                : "",
            checkfinalpvc: this.props.data.LastTruckCheckPVCandPlummet,

            truckarrival: this.props.data.TruckArrivalTime,
            predropconcrete: this.props.data.PreTruckEnd,
            postdropconcrete: this.props.data.PostTruckStart,
            newesttruck: this.props.newesttruck,
            checkstopbetween: this.props.data.StopPouringConcrete ||false,
            pretruckcheckstopbetween: this.props.pretruckcheckstopbetween||false,

            concretevolumestop: this.props.data.ConcreteCalculateVolumn||0,
            // beforetremiepipeembeddedlength:this.props.data.
          },
          async () => {
            // console.warn(this.props.newesttruck, this.state.newesttruck)
            // console.log(
            //   "edit 10 drop",
            //   this.props.data.IsLastTruck,
            //   this.state.showlastview,
            //   this.state.lastconcretetruckButton
            // )
            this.setState({
              lastconcretetruckButton: this.props.data.IsLastTruck,
              showlastview: this.props.data.IsLastTruck ,
              final_overwidth:this.props.data.WidthConcrete,
              final_overLength:this.props.data.LengthConcrete,
              final_overheight:this.props.data.HeightConcrete,
              imagelasttruckover:this.props.data.ImageConcreteRecordDump,

            },()=>{
              if(this.props.data.IsLastTruck==true){
                
                if(this.props.data.WidthConcrete!=null&&this.props.data.WidthConcrete!=undefined&&this.props.data.WidthConcrete!=''&&
                this.props.data.LengthConcrete!=null&&this.props.data.LengthConcrete!=undefined&&this.props.data.LengthConcrete!=''&&
                this.props.data.HeightConcrete!=null&&this.props.data.HeightConcrete!=undefined&&this.props.data.HeightConcrete!=''){
                  this.setState({
                    checkfinalover: true},()=>{
                      this.calfinalconcreteleftover()
                    })
                }
                if(this.props.data.RemainConcrete!=null&&this.props.data.RemainConcrete!=undefined&&this.props.data.RemainConcrete!=''){
                  this.setState({
                    final_concreteuse:this.props.data.RemainConcrete.Name,
                    final_concreteuseid:this.props.data.RemainConcrete.Id,
                    final_remainusepilename:this.props.data.RemainConcretePileNo
                    })
                }
              }
            })
            await this.calculateTremie()
            // console.log("calculateTremie",this.state.tremiepipeembeddedlength)
            await this.dryCalculate(this.props.data.tremiecutcount)
            // console.log("tremiecutcount",this.props.data.tremiecutcount,this.state.cuttremiepipe,this.state.beforetremiepipeembeddedlength)
          }
        )
      }
    }

    if (this.props.TremieList != undefined) {
      this.setState({
        tremieimage: this.props.TremieList.image,
        tremie: this.props.TremieList.size,
        tremieid: this.props.TremieList.tremieid,
        tremielength: this.props.TremieList.length,
        last: this.props.TremieList.last,
        index: this.props.index
      })
    }
  }

  // onApprove() {
  //   Actions.slumpapprove({
  //     process: this.props.process,
  //     step: this.props.step,
  //     jobid: this.props.jobid,
  //     pileid: this.props.pileid,
  //     approveuser: this.state.approveuser,
  //     piledetail: this.state.piledetail,
  //     truckno: this.state.truckno,
  //     concretebrand: this.state.concretebrand,
  //     mix: this.state.mix,
  //     strength: this.state.strength,
  //     unit: this.state.unit,
  //     truckconcretevolume: this.state.truckconcretevolume,
  //     slump: this.state.slump,
  //     truckarrival: this.state.truckarrival,
  //     truckdepart: this.state.truckdepart,
  //     slumpimage: this.state.slumpimage,
  //     category: this.props.category
  //   })
  // }

  selectTruck = data => {
    console.log(
      "selectTruck",
     data
    )
    if (
      this.props.edit == true &&
      this.state.newesttruck == false &&
      data.volume != this.state.concretevolume
    ) {
      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.warn1"), [
        {
          text: "OK"
        }
      ])
      return
    } else {
      this.setState(
        {
          truck: data.label,
          truckid: data.id,
          concretevolume: data.volume,
          truckarrival: data.truckarrival,
          
          concretevolumestop:this.state.pretruckcheckstopbetween ==true ?(parseFloat(
            data.volume-this.state.pretruckConcreteVolstop
          ).toFixed(2)):0
        },
        // () => this.calculateTremie()
        () => {
          this.calculateTremie()
          this.dryCalculate(this.state.cuttremiepipe)
        }
      )
    }
  }

  selectreminuse = data => {
    this.setState(
      {
        final_concreteuse: data.label,
        final_concreteuseid: data.id,
        
      },()=>{
        if(data.id!==7){
          this.setState({final_remainusepilename:''})
        }
      }
    )
  }

  closeButton() {
    Actions.pop()
  }

  onCameraRoll(keyname, photos, type) {
    if (this.state.Edit_Flag == 1) {
      Actions.camearaRoll({
        jobid: this.props.jobid,
        process: this.props.process,
        step: this.props.step,
        pileId: this.props.pileid,
        keyname: keyname,
        photos: photos,
        typeViewPhoto: type,
        shape: this.props.shape,
        category: this.props.category,
        Edit_Flag: this.state.Edit_Flag
      })
    } else {
      Actions.camearaRoll({
        jobid: this.props.jobid,
        process: this.props.process,
        step: this.props.step,
        pileId: this.props.pileid,
        keyname: keyname,
        photos: photos,
        typeViewPhoto: "view",
        shape: this.props.shape,
        category: this.props.category,
        Edit_Flag: this.state.Edit_Flag
      })
    }
  }

  async saveButton() {
    // console.warn(this.state.final_concretevolume)
    let startdrop = moment(this.state.startdrop, "DD/MM/YYYY HH:mm")
    let finishdrop = moment(this.state.finishdrop, "DD/MM/YYYY HH:mm")
    let truckarrival = moment(this.state.truckarrival, "DD/MM/YYYY HH:mm")
    let predropconcrete = ""
    let postdropconcrete = ""
    let test = startdrop <= truckarrival
    // console.warn(test,startdrop,truckarrival,predropconcrete)

    if (this.state.predropconcrete != "") {
      predropconcrete = moment(this.state.predropconcrete, "DD/MM/YYYY HH:mm")
    }
    if (this.state.postdropconcrete != "") {
      postdropconcrete = moment(this.state.postdropconcrete, "DD/MM/YYYY HH:mm")
    }
    // Validate
    if (this.state.truckid == "") {
      console.log("error 1")
      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.error9"), [
        {
          text: "OK"
        }
      ])
      return
    }
    if (this.state.startdrop == "") {
      console.log("error 1")
      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.error8"), [
        {
          text: "OK"
        }
      ])
      return
    }

    if (this.state.finishdrop && finishdrop <= startdrop) {
      // console.warn("error tex")
      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.error3"), [
        {
          text: "OK"
        }
      ])
      return
    }
    if (startdrop <= truckarrival || finishdrop <= truckarrival) {
      console.log("error4", startdrop <= truckarrival)
      Alert.alert(
        "",
        I18n.t("mainpile.DropConcrete.alert.error4") +
          " " +
          moment(truckarrival).format("DD/MM/YYYY HH:mm"),
        [
          {
            text: "OK"
          }
        ]
      )
      return
    }
    if (predropconcrete != "" && startdrop <= predropconcrete) {
      console.log("error5", startdrop <= predropconcrete)
      Alert.alert(
        "",
        I18n.t("mainpile.DropConcrete.alert.error5") +
          " " +
          moment(predropconcrete).format("DD/MM/YYYY HH:mm"),
        [
          {
            text: "OK"
          }
        ]
      )
      return
    }
    if (postdropconcrete != "" && startdrop >= postdropconcrete) {
      console.log("error5", startdrop <= postdropconcrete)
      Alert.alert(
        "",
        I18n.t("mainpile.DropConcrete.alert.error11") +
          " " +
          moment(postdropconcrete).format("DD/MM/YYYY HH:mm"),
        [
          {
            text: "OK"
          }
        ]
      )
      return
    }
    if (
      predropconcrete != "" &&
      this.state.finishdrop != "" &&
      finishdrop <= predropconcrete
    ) {
      console.log("error5", startdrop <= predropconcrete)
      Alert.alert(
        "",
        I18n.t("mainpile.DropConcrete.alert.error12") +
          " " +
          moment(predropconcrete).format("DD/MM/YYYY HH:mm"),
        [
          {
            text: "OK"
          }
        ]
      )
      return
    }
    if (
      postdropconcrete != "" &&
      this.state.finishdrop != "" &&
      finishdrop >= postdropconcrete
    ) {
      Alert.alert(
        "",
        I18n.t("mainpile.DropConcrete.alert.error13") +
          " " +
          moment(postdropconcrete).format("DD/MM/YYYY HH:mm"),
        [
          {
            text: "OK"
          }
        ]
      )
      return
    }

    // if(this.state.cuttremieButton==true&&Number.isInteger(this.state.cuttremiepipe)==false){
    //   Alert.alert("", I18n.t("alert.errorcasingdepth3"), [
    //     {
    //       text: "OK"
    //     }
    //   ])

    //   return
    // }
    if(this.state.checkstopbetween==true){
      if(this.state.casingdepth === null || this.state.casingdepth === ""){
        if (this.props.category == 1||this.props.category == 4) {

          Alert.alert("", I18n.t("alert.errorcasingdepth1"), [
            {
              text: "OK"
            }
          ])
          return
        } else {

          Alert.alert("", I18n.t("alert.errorcasingdepth2"), [
            {
              text: "OK"
            }
          ])
          return
        }
      }
      if(this.state.cuttremiepipe == 0 ||
        this.state.cuttremiepipe == "" ||
        this.state.cuttremiepipe == null){
          Alert.alert("", I18n.t("mainpile.DropConcrete.alert.error20"), [
            {
              text: "OK"
            }
          ])
          return
        }
    }

    if(this.state.pretruckcheckstopbetween==true){
      if(this.state.casingdepth === null || this.state.casingdepth === ""){
        if (this.props.category == 1||this.props.category == 4) {

          Alert.alert("", I18n.t("alert.errorcasingdepth1"), [
            {
              text: "OK"
            }
          ])
          return
        } else {

          Alert.alert("", I18n.t("alert.errorcasingdepth2"), [
            {
              text: "OK"
            }
          ])
          return
        }
      }
    }

    if (
      this.state.cuttremiepipe != 0 ||
      this.state.cuttremiepipe != "" ||
      this.state.cuttremiepipe != null
    ) {
      if (
        (this.state.casingdepth === null || this.state.casingdepth === "") &&
        this.state.cuttremieButton
      ) {
        if (this.props.shape == 1) {
          console.log("error 2")
          Alert.alert("", I18n.t("alert.errorcasingdepth1"), [
            {
              text: "OK"
            }
          ])
          return
        } else {
          console.log("error 2")
          Alert.alert("", I18n.t("alert.errorcasingdepth2"), [
            {
              text: "OK"
            }
          ])
          return
        }
      }
    }
    if (
      (this.state.cuttremiepipe == 0 ||
        this.state.cuttremiepipe == "" ||
        this.state.cuttremiepipe == null) &&
      this.state.tremiepipecutlength != 0 &&
      this.state.tremiepipecutlength != null &&
      this.state.tremiepipecutlength != ""
    ) {
      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.error14"), [
        {
          text: "OK"
        }
      ])
      return
    }
    if (
      this.state.cuttremiepipe != 0 &&
      this.state.cuttremiepipe != "" &&
      this.state.cuttremiepipe != null &&
      (this.state.tremiepipecutlength == 0 ||
        this.state.tremiepipecutlength == null ||
        this.state.tremiepipecutlength == "")
    ) {
      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.error14"), [
        {
          text: "OK"
        }
      ])
      return
    }
    if (
      this.state.cuttremiepipe > this.state.tremiepipeinstall &&
      this.state.cuttremieButton
    ) {
      // console.warn("error 2",this.state.cuttremiepipe,this.state.tremiepipeinstall)
      Alert.alert("", I18n.t("alert.errorovertremie"), [
        {
          text: "OK"
        }
      ])
      return
    }

    if (
      this.state.showlastview &&
      this.state.lastconcretetruckButton &&
      this.state.checkfinalpvc == false
    ) {
      console.log("error 4")
      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.error10"), [
        {
          text: "OK"
        }
      ])
      return
    }
    if (
      this.state.showlastview &&
      this.state.lastconcretetruckButton &&
      (this.state.finalconcreteimage == "" ||
        this.state.finalconcreteimage == null ||
        this.state.finalconcreteimage.length == 0)
    ) {
      console.log("error 4")
      Alert.alert("", I18n.t("alert.errorPicfinalcheck"), [
        {
          text: "OK"
        }
      ])
      return
    }
  
    if (
      this.state.showlastview &&
      this.state.lastconcretetruckButton &&
      this.state.checkfinalover ==true &&
      (this.state.finalconcreteoverimage == "" ||
        this.state.finalconcreteoverimage == null ||
        this.state.finalconcreteimage.length == 0)
    ) {

      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.error15"), [
        {
          text: "OK"
        }
      ])
      return
    }
    if (
      this.state.showlastview &&
      this.state.lastconcretetruckButton &&
      this.state.checkfinalover ==true &&
      (this.state.final_overwidth == "" ||
        this.state.final_overwidth == null )
    ) {

      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.error16"), [
        {
          text: "OK"
        }
      ])
      return
    }

    if (
      this.state.showlastview &&
      this.state.lastconcretetruckButton &&
      this.state.checkfinalover ==true &&
      (this.state.final_overLength == "" ||
        this.state.final_overLength == null )
    ) {

      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.error17"), [
        {
          text: "OK"
        }
      ])
      return
    }

    if (
      this.state.showlastview &&
      this.state.lastconcretetruckButton &&
      this.state.checkfinalover ==true &&
      (this.state.final_overheight == "" ||
        this.state.final_overheight == null )
    ) {

      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.error18"), [
        {
          text: "OK"
        }
      ])
      return
    }
    if (
      this.state.showlastview &&
      this.state.lastconcretetruckButton &&
      this.state.checkfinalover ==true &&
      (this.state.final_concreteuseid == "" ||
        this.state.final_concreteuseid == null )
    ) {


      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.error19"), [
        {
          text: "OK"
        }
      ])
      return
    }
    

    if (
      this.state.showlastview &&
      this.state.lastconcretetruckButton &&
      this.state.checkfinalover ==true &&
      (this.state.final_concreteuseid != "" &&this.state.final_concreteuseid==7&&(
        this.state.final_remainusepilename=="" ||
        this.state.final_remainusepilename==null 
        ) )
    ) {
      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.error21"), [
        {
          text: "OK"
        }
      ])
      return
    }

    if (
      this.state.showlastview &&
      this.state.lastconcretetruckButton &&
      this.state.final_concretevolume > this.state.concretevolume
    ) {
      console.log("error 4")
      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.error7"), [
        {
          text: "OK"
        }
      ])
      return
    }
    if (
      this.state.showlastview &&
      this.state.lastconcretetruckButton &&
      (this.state.final_concretevolume === "" ||
        this.state.final_concretevolume === null)
    ) {
      console.log("error 3")
      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.error6"), [
        {
          text: "OK"
        }
      ])
      return
    }
    if (
      this.state.lastconcretetruckButton &&
      this.calculateLeftTremieValue() > this.state.cuttremiepipe
    ) {
      let button = [
        { text: I18n.t("mainpile.DropConcrete.button.cancel") },
        {
          text: I18n.t("mainpile.DropConcrete.button.cut"),
          onPress: () => {
            this.setState(
              {
                cuttremieButton: true,
                cuttremiepipe: this.calculateLeftTremieValue(),
                trigger: true
              },
              () => {
                setTimeout(() => {
                  this.dryCalculate(this.state.cuttremiepipe)
                  this.refs.scroll.scrollToEnd()
                }, 500)
              }
            )
          }
        }
      ]
      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.cut"), button)
      return
    }

    var tremietemp = await this.state.mustcutarr.map((data, index) => {
      // console.warn("tremietemp",data)
      index + 1 == this.state.mustcutarr.length ? data : data + " + "
    })

    let tremietemp2 = null
    await this.state.mustcutarr.map((data, index) => {
      console.warn("tremietemp", data)
      // index + 1 == this.state.mustcutarr.length ? data : data + " + "
      if (tremietemp2 != null) {
        if (index + 1 == this.state.mustcutarr.length) {
          tremietemp2 = tremietemp2 + data
          // tremietemp2.push({key:data})
        } else {
          let dataplus = data + " + "
          tremietemp2 = tremietemp2 + data + " + "
        }
      } else {
        if (index + 1 == this.state.mustcutarr.length) {
          tremietemp2 = data
          // tremietemp2.push({key:data})
        } else {
          // let dataplus = data + " + "
          tremietemp2 = data + " + "
        }
      }
    })
    console.warn(tremietemp2)

    var text2 = `${I18n.t("mainpile.DropConcrete.cuttrem")} ${
      this.state.mustcuttremie
    } ${I18n.t("mainpile.DropConcrete.cuttrem_plus")} (${tremietemp} = ${
      this.state.tremiepipecutlength2
    } ${I18n.t("mainpile.3_4.m")})`
    var text =
      `${I18n.t("mainpile.DropConcrete.cuttrem")} ${
        this.state.mustcuttremie
      } ${I18n.t("mainpile.DropConcrete.cuttrem_plus")} (` +
      tremietemp2 +
      ` = ${this.state.tremiepipecutlength2} ${I18n.t("mainpile.3_4.m")})`

    console.warn("tremietemp", tremietemp, this.state.mustcuttremie)
    var button = [
      { text: I18n.t("mainpile.DropConcrete.button.cancel") },
      {
        text: I18n.t("mainpile.DropConcrete.button.nocut"),
        onPress: () => {
          // this.savenotcut(0).then(()=>{this.save(()=>{Actions.pop()})})
          this.setState({ cuttremiepipe: 0 }, async () => {
            await this.calculateTremie()
            await this.dryCalculate(0)
            this.save()
            Actions.pop()
          })
        },
        style: "cancel"
      },
      {
        text: I18n.t("mainpile.DropConcrete.button.cut"),
        onPress: () => {
          this.setState({ cuttremieButton: true, trigger: true }, () =>
            setTimeout(() => {
              this.refs.scroll.scrollToEnd()
            }, 500)
          )
        }
      }
    ]
    if (this.state.checkfinalconcrete) {
      button.splice(1, 1)
    }

    if (
      (postdropconcrete == "" || postdropconcrete == null) &&
      this.state.finishdrop != "" &&
      this.state.mustcuttremie > 0 &&
      !this.state.cuttremieButton
    ) {
      Alert.alert(I18n.t("mainpile.DropConcrete.cuttrem2"), text, button)
    } else {
      this.save()
      Actions.pop()
    }
    // Alert.alert("ตัดท่อเทรมี่", text, button)

    // this.save()
    //     Actions.pop()
  }

  saveButton2() {
    // Validate
    if (
      this.state.startdrop == "" ||
      this.state.finishdrop == "" ||
      this.state.casingdepth == "" ||
      this.state.truckno == ""
    ) {
      Alert.alert("", I18n.t("mainpile.4_1.error_nextstep"), [
        {
          text: "OK"
        }
      ])
      return
    }
    Actions.pop()
    this.save()
  }

  save(cutdefault = false) {
    console.warn('save checkstopbetween',this.state.checkstopbetween  ,'pretruckcheckstopbetween' ,this.state.pretruckcheckstopbetween  , 'concretevolumestop',this.state.concretevolumestop )
    var data = this.props.data || []
    let No = this.props.dropNo? this.props.dropNo:this.props.index

    if(No.length>1){
      No = No.split(' ')
      No = No[0]
    }

    console.log('SAVE concrete dropNo',No)
    var valueApi = {
      Language: I18n.currentLocale(),
      latitude: this.props.lat,
      longitude: this.props.log,
      JobId: this.props.jobid,
      PileId: this.props.pileid,
      ConcreteTruckRegisterId: this.state.truckid,
      // No:this.props.dropNo? this.props.dropNo:this.props.index,
      No:No,
      StartConcreting: moment(this.state.startdrop, "DD/MM/YYYY HH:mm").format(
        "YYYY-MM-DD HH:mm"
      ),
      EndConcreting: moment(this.state.finishdrop, "DD/MM/YYYY HH:mm").format(
        "YYYY-MM-DD HH:mm"
      ),
      Depth: this.state.casingdepth !== null ? this.state.casingdepth : "", //this.state.theorydepth,
      imageconcrete: this.state.dropconcreteimage,
      IsLastTruck: this.state.lastconcretetruckButton,
      LastTruckCheckPVCandPlummet: this.state.checkfinalpvc,
      LastTruckPVCConcreteHeight: this.state.final_pvcheight,
      LastTruckPlummetConcreteHeight: this.state.final_plummetheight,
      LastTruckConcreteVolume: this.state.final_concretevolume,
      imagelasttruck: this.state.finalconcreteimage,
      TremyCutCount: cutdefault ? 0 : this.state.cuttremiepipe,
      ConcreteRecordId: data.id || "",
      TremyCutLength: this.state.tremiepipecutlength,
      TremyCutSinkAfter:
        this.state.tremiepipecutlength > 0
          ? this.state.tremiepipeembeddedlength
          : null,
      // TremyCutSinkBefore: this.state.beforetremiepipeembeddedlength,
      TremyCutSinkBefore: 
        this.state.beforetremiepipeembeddedlength > 0
          ? this.state.beforetremiepipeembeddedlength
          : null,
      TremyCutLeft: this.state.remainingtremiepiplength,
      LastTruckCheckOver:this.state.checkfinalover,
      imagelasttruckover:this.state.finalconcreteoverimage,
      LastTruckCheckOverWidth:this.state.final_overwidth,
      LastTruckCheckOverLength:this.state.final_overLength,
      LastTruckCheckOverHeight:this.state.final_overheight,
      RemainConcreteVolumn:this.state.final_concreteleftovervolume,
      RemainConcreteId:this.state.final_concreteuseid,
      RemainConcreteName:this.state.final_concreteuseid!=undefined &&this.state.final_concreteuseid !=null &&this.state.final_concreteuseid!=''? this.state.final_concreteuse:'',
      RemainConcretePileNo:this.state.final_remainusepilename !==null ? this.state.final_remainusepilename:'',
      ResidualConcreteVolumn:this.state.concretemustpour,
      StopPouringConcrete:this.state.checkstopbetween,
      ConcreteCalculateVolumn:this.state.checkstopbetween ==true ||this.state.pretruckcheckstopbetween==true ? this.state.concretevolumestop :''
    }
    console.log("SAVE concrete register", valueApi)
    this.props.pileSaveStep10_3(valueApi)
  }

  // savenotcut(data = 0) {
  //   data => {
  //     this.setState({ cuttremiepipe: "" }, data => {
  //       this.setState({ cuttremiepipe: data }, data => {
  //         this.dryCalculate(data)
  //       })
  //     })
  //   }
  // }
  onStartSelectDate(date) {
    let date2 = moment(date).format("DD/MM/YYYY HH:mm")
    this.setState({ startdrop: date2 })
  }
  onFinishSelectDate(date) {
    let date2 = moment(date).format("DD/MM/YYYY HH:mm")
    this.setState({ finishdrop: date2 })
  }
  onArriveSelect(time) {
    var now = moment().format("DD/MM/YYYY HH:mm")
    if (time == "START") this.setState({ startdrop: now })
    else if (time == "FINISH") this.setState({ finishdrop: now })
  }
  async onArriveSelectDate2(time) {
    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        date: new Date(moment().format("YYYY"), month, moment().format("D"))
      })
      if (action !== DatePickerAndroid.dismissedAction) {
        var date = new Date(year, month, day)
        date = moment(date).format("DD/MM/YYYY")
        try {
          const { action, hour, minute } = await TimePickerAndroid.open({
            is24Hour: true
          })
          if (action !== TimePickerAndroid.dismissedAction) {
            var second = moment().format("ss")
            var datepick = moment(
              date + " " + hour + ":" + minute + ":" + second,
              "DD/MM/YYYY HH:mm:ss"
            ).format("DD/MM/YYYY HH:mm")
            if (time == "START") this.setState({ startdrop: datepick })
            else if (time == "FINISH") this.setState({ finishdrop: datepick })
          }
        } catch ({ code, message }) {
          // console.warn("Cannot open time picker", message)
        }
      }
    } catch ({ code, message }) {
      // console.warn("Cannot open date picker", message)
    }
  }

  async onDepartSelect(data) {
    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        date: new Date(moment().format("YYYY"), month, moment().format("D"))
      })
      if (action !== DatePickerAndroid.dismissedAction) {
        var date = new Date(year, month, day)
        date = moment(date).format("DD/MM/YYYY")
        try {
          const { action, hour, minute } = await TimePickerAndroid.open({
            is24Hour: true
          })
          if (action !== TimePickerAndroid.dismissedAction) {
            var second = moment().format("ss")
            var datepick = moment(
              date + " " + hour + ":" + minute + ":" + second,
              "DD/MM/YYYY HH:mm:ss"
            ).format("DD/MM/YYYY HH:mm")
            if (data == "START") this.setState({ startdrop: datepick })
            else if (data == "FINISH") this.setState({ finishdrop: datepick })
          }
        } catch ({ code, message }) {
          // console.warn("Cannot open time picker", message)
        }
      }
    } catch ({ code, message }) {
      // console.warn("Cannot open date picker", message)
    }
  }

  onPressStopBe=()=>{

    if(this.state.lastconcretetruckButton == true){
      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.warn3"), [
        {
          text: "OK"
        }
      ])
      return
    }
    if (this.props.edit == true && this.state.newesttruck == false) {
      Alert.alert("", I18n.t("mainpile.DropConcrete.alert.warn2"), [
        {
          text: "OK"
        }
      ])
      return
    } else {
      this.setState({checkstopbetween:!this.state.checkstopbetween},()=>{
        if(this.state.checkstopbetween==true){
         
          this.setState({ cuttremieButton:true},async()=>{

            await this.calculateTremie()

            await this.dryCalculate(this.state.tremiecutcount)
          })
        }else{
          this.setState({concretevolumestop:this.state.pretruckcheckstopbetween ==true ?(parseFloat(
            this.state.concretevolume-this.state.pretruckConcreteVolstop
          ).toFixed(2)):0})
        }
      })
    }
    
   
  }

  renderLastTruck() {
    var text = ""
    if (this.props.category == 1||this.props.category ==4) {
      text = I18n.t("mainpile.DropConcrete.bp")
    } else {
      text = I18n.t("mainpile.DropConcrete.dw")
    }
    return (
      <View style={{ marginTop: 10, padding: 5 }}>
        <View style={styles.dropDownTopic}>
          <Text style={{ fontSize: 20 }}>
            {I18n.t("mainpile.DropConcrete.lartconconcrete1")}
          </Text>
        </View>

        <View style={{ borderWidth: 1 }}>
          {this.state.showcheck4 == true ? (
            <View>
              <Text
                style={[
                  styles.topicText,
                  { fontSize: I18n.locale == "en" ? 17 : 18 }
                ]}
              >
                {I18n.t("mainpile.DropConcrete.checklevelconcrete")}
              </Text>
            </View>
          ) : null}
          {this.state.showcheck4 == true ? (
            <View style={styles.lastView}>
              {this.state.checkfinalpvc == true ? (
                <Icon
                  disabled={this.state.Edit_Flag == 0}
                  name="check"
                  reverse
                  color="#6dcc64"
                  size={15}
                  onPress={() =>
                    this.setState({ checkfinalpvc: !this.state.checkfinalpvc })
                  }
                />
              ) : (
                <Icon
                  disabled={this.state.Edit_Flag == 0}
                  name="check"
                  reverse
                  color="grey"
                  size={15}
                  onPress={() =>
                    this.setState({ checkfinalpvc: !this.state.checkfinalpvc })
                  }
                />
              )}

              <View style={{ width: "60%" }}>
                <Text numberOfLines={0} style={{ textAlign: "center" }}>
                  {I18n.t("mainpile.DropConcrete.checklevelconcrete2")}
                </Text>
              </View>
            </View>
          ) : null}
          {this.state.showcheck4 == true ? (
            <View style={{ height: 50, alignItems: "center", marginTop: 10 }}>
              <TouchableOpacity
                style={[
                  styles.image,
                  this.state.finalconcreteimage.length > 0
                    ? { backgroundColor: "#6dcc64" }
                    : {}
                ]}
                onPress={() =>
                  this.onCameraRoll(
                    "finalconcreteimage",
                    this.state.finalconcreteimage,
                    "edit"
                  )
                }
              >
                <Icon name="image" type="entypo" color="#fff" />
                <Text style={styles.imageText}>
                  {I18n.t("mainpile.liquidtestinsert.view")}
                </Text>
              </TouchableOpacity>
            </View>
          ) : null}
          {this.state.showcheck4 == true ? (
            <View style={styles.listTopicSub}>
              <Text
                style={[
                  styles.topicText,
                  { fontSize: I18n.locale == "en" ? 17 : 18 }
                ]}
              >
                {I18n.locale == "th"
                  ? I18n.t("mainpile.DropConcrete.depth") +
                    I18n.t("mainpile.DropConcrete.from") +
                    text +
                    " " +
                    I18n.t("mainpile.DropConcrete.m")
                  : I18n.t("mainpile.DropConcrete.casingdepth") +
                    " " +
                    I18n.t("mainpile.DropConcrete.depth") +
                    " " +
                    text +
                    " " +
                    I18n.t("mainpile.DropConcrete.m")}
              </Text>
            </View>
          ) : null}
          {this.state.showcheck4 == true ? (
            <View style={{ marginTop: 10, padding: 10 }}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <View style={{ width: "80%" }}>
                  <Text>{I18n.t("mainpile.DropConcrete.plummetheigh")}</Text>
                </View>
                <View style={{ width: "20%" }}>
                  <TextInput
                    ref={(input) => { this.finalplummetheight = input; }}
                    keyboardType="numeric"
                    editable={this.state.Edit_Flag != 0}
                    value={this.state.final_plummetheight.toString()}
                    underlineColorAndroid="transparent"
                    style={styles.listTextbox}
                    onChangeText={data =>
                      this.setState({
                        final_plummetheight: data,
                        visible: false,
                        visible2: false
                      })
                      // 
                    }
                    onEndEditing={data => {
                      if (
                        this.state.final_plummetheight == null ||
                        this.state.final_plummetheight == ""
                      ) {
                        this.setState({ final_plummetheight: "" })
                      } else {
                        let newdata = Math.abs(this.state.final_plummetheight)
                        if (isNaN(newdata) == true) {
                          newdata = ""
                          this.setState({ final_plummetheight: newdata })
                        } else {
                          this.setState({ final_plummetheight: parseFloat(newdata).toFixed(2) })
                        }
                      }
                    }}
                    onSubmitEditing={()=>{
                      this.finalpvcheight.focus()
                    }}
                  />
                </View>
              </View>
            </View>
          ) : null}
          {this.state.showcheck4 == true ? (
            <View style={{ padding: 10 }}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <View style={{ width: "80%" }}>
                  <Text>{I18n.t("mainpile.DropConcrete.pvc")}</Text>
                </View>
                <View style={{ width: "20%" }}>
                  <TextInput
                    ref={(input) => { this.finalpvcheight = input; }}
                    keyboardType="numeric"
                    editable={this.state.Edit_Flag != 0}
                    value={this.state.final_pvcheight.toString()}
                    underlineColorAndroid="transparent"
                    style={styles.listTextbox}
                    onChangeText={data =>
                      this.setState({
                        final_pvcheight: data,
                        visible: false,
                        visible2: false
                      })
                    }
                    onEndEditing={data => {
                      if (
                        this.state.final_pvcheight == null ||
                        this.state.final_pvcheight == ""
                      ) {
                        this.setState({ final_pvcheight: "" })
                      } else {
                        let newdata = Math.abs(this.state.final_pvcheight)
                        if (isNaN(newdata) == true) {
                          newdata = ""
                          this.setState({ final_pvcheight: newdata })
                        } else {
                          this.setState({ final_pvcheight: parseFloat(newdata).toFixed(2) })
                        }
                      }
                    }}
                    onSubmitEditing={()=>{
                      if(this.state.checkfinalover==true){
                        this.finaloverwidth.focus()
                      }else{
                        this.finalconcretevolume.focus()
                      }
                    }}
                  />
                </View>
              </View>
            </View>
          ) : null}

          <View style={styles.listTopicSub}>
            <Text
              style={[
                styles.topicText,
                { fontSize: I18n.locale == "en" ? 17 : 18 }
              ]}
            >
              {I18n.t("mainpile.DropConcrete.final_concrete")}
            </Text>
          </View>
          <View style={styles.lastView}>
              {this.state.checkfinalover == true ? (
                <Icon
                  disabled={this.state.Edit_Flag == 0}
                  name="check"
                  reverse
                  color="#6dcc64"
                  size={15}
                  onPress={() =>
                    this.setState({ checkfinalover: !this.state.checkfinalover })
                  }
                />
              ) : (
                <Icon
                  disabled={this.state.Edit_Flag == 0}
                  name="check"
                  reverse
                  color="grey"
                  size={15}
                  onPress={() =>
                    this.setState({ checkfinalover: !this.state.checkfinalover })
                  }
                />
              )}

              <View style={{ width: "60%" }}>
                <Text numberOfLines={0} style={{ textAlign: "center" }}>
                  {I18n.t("mainpile.DropConcrete.final_concrete_over")}
                </Text>
              </View>
            </View>
            
        {this.state.checkfinalover==true&&
          <View>  
            <View style={{ height: 50, alignItems: "center", marginTop: 10 }}>
              <TouchableOpacity
                style={[
                  styles.image,
                  this.state.finalconcreteoverimage.length > 0
                    ? { backgroundColor: "#6dcc64" }
                    : {}
                ]}
                disabled={this.state.checkfinalover==false}
                onPress={() =>
                  this.onCameraRoll(
                    "finalconcreteoverimage",
                    this.state.finalconcreteoverimage,
                    "edit"
                  )
                }
              >
                <Icon name="image" type="entypo" color="#fff" />
                <Text style={styles.imageText}>
                  {I18n.t("mainpile.liquidtestinsert.view")}
                </Text>
              </TouchableOpacity>
            </View>

            <View style={{ padding: 10 }}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <View style={{ width: "40%" }}>
                <Image source={require('../../assets/image/spacer-pour.png')} style={{width: Dimensions.get('window').width/100*35, height: Dimensions.get('window').height/100*20, resizeMode: 'contain'}}/>
                </View>
                <View style={{ width: "60%" }}>
                <View style={{ flexDirection: "row", alignItems: "center" ,padding: 5}}>
                  <View style={{ width: "70%" }}>
                    <Text>{I18n.t("mainpile.DropConcrete.final_concrete_over_width")}</Text>
                  </View>
                <View style={{ width: "30%" }}>
                  <TextInput
                    ref={(input) => { this.finaloverwidth = input; }}
                    keyboardType="numeric"
                    editable={this.state.Edit_Flag != 0&&this.state.checkfinalover!=false}
                    value={this.state.final_overwidth!=null&&this.state.final_overwidth!=undefined? this.state.final_overwidth.toString():this.state.final_overwidth}
                    underlineColorAndroid="transparent"
                    style={styles.listTextbox}
                    onChangeText={data =>
                      this.setState({
                        final_overwidth: data,
                      
                      })
                    }
                    onEndEditing={data => {
                      if (
                        this.state.final_overwidth == null ||
                        this.state.final_overwidth == ""
                      ) {
                        this.setState({ final_overwidth: 0 },()=>{
                          this.calfinalconcreteleftover()
                        })
                      } else {
                        let newdata = Math.abs(this.state.final_overwidth)
                        if (isNaN(newdata) == true) {
                          newdata = ""
                          this.setState({ final_overwidth: newdata },()=>{ this.calfinalconcreteleftover()})
                        } else {
                          this.setState({ final_overwidth: parseFloat(newdata).toFixed(2)},()=>{ this.calfinalconcreteleftover()})
                        }
                      
                      }
                    }}
                    onSubmitEditing={()=>{

                        this.finaloverLength.focus()

                    }}
                  />
                 </View>
                 </View>

                 <View style={{ flexDirection: "row", alignItems: "center",padding: 5 }}>
                  <View style={{ width: "70%" }}>
                    <Text>{I18n.t("mainpile.DropConcrete.final_concrete_over_length")}</Text>
                  </View>
                <View style={{ width: "30%" }}>
                  <TextInput
                    ref={(input) => { this.finaloverLength = input; }}
                    keyboardType="numeric"
                    editable={this.state.Edit_Flag != 0&&this.state.checkfinalover!=false}
                    value={this.state.final_overLength != null ?this.state.final_overLength.toString():''}
                    underlineColorAndroid="transparent"
                    style={styles.listTextbox}
                    onChangeText={data =>
                      this.setState({
                        final_overLength: data,
                      
                      })
                    }
                    onEndEditing={data => {
                      if (
                        this.state.final_overLength == null ||
                        this.state.final_overLength == ""
                      ) {
                        this.setState({ final_overLength: 0 })
                      } else {
                        let newdata = Math.abs(this.state.final_overLength)
                        if (isNaN(newdata) == true) {
                          newdata = ""
                          this.setState({ final_overLength: newdata },()=>{
                            this.calfinalconcreteleftover()
                          })
                        } else {
                          this.setState({ final_overLength: parseFloat(newdata).toFixed(2) },()=>{
                            this.calfinalconcreteleftover()
                          })
                        }
                      
                      }
                    }}
                    onSubmitEditing={()=>{
                      this.finaloverheight.focus()
                    }}
                  />
                 </View>
                 </View>
                 <View style={{ flexDirection: "row", alignItems: "center",padding: 5 }}>
                  <View style={{ width: "70%" }}>
                    <Text>{I18n.t("mainpile.DropConcrete.final_concrete_over_height")}</Text>
                  </View>
                <View style={{ width: "30%" }}>
                  <TextInput
                    ref={(input) => { this.finaloverheight = input; }}
                    keyboardType="numeric"
                    editable={this.state.Edit_Flag != 0&&this.state.checkfinalover!=false}
                    value={this.state.final_overheight != null ?this.state.final_overheight.toString():''}
                    underlineColorAndroid="transparent"
                    style={styles.listTextbox}
                    onChangeText={data =>
                      this.setState({
                        final_overheight: data,
                      
                      })
                    }
                    onEndEditing={data => {
                      if (
                        this.state.final_overheight == null ||
                        this.state.final_overheight == ""
                      ) {
                        this.setState({ final_overheight: 0 })
                      } else {
                        let newdata = Math.abs(this.state.final_overheight)
                        if (isNaN(newdata) == true) {
                          newdata = ""
                          this.setState({ final_overheight: newdata },()=>{
                            this.calfinalconcreteleftover()
                          })
                        } else {
                          this.setState({ final_overheight: parseFloat(newdata).toFixed(2) },()=>{
                            this.calfinalconcreteleftover()
                          })
                        }
                      
                      }
                    }}
                    onSubmitEditing={()=>{
                      this.finalconcretevolume.focus()
                    }}
                  />
                 </View>
                 </View>
                </View>
            </View>
            </View>
            <View style={{ padding: 10, marginTop: 10 }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View style={{ width: "80%" }}>
                <Text>
                  {I18n.t("mainpile.DropConcrete.final_concrete_over_volume")}
                </Text>
              </View>
              <View style={{ width: "20%" }}>
                <TextInput
                  keyboardType="numeric"
                  editable={false}
                  value={this.state.final_concreteleftovervolume}
                  underlineColorAndroid="transparent"
                  style={[styles.listTextbox,{ backgroundColor: "#ebf1f7" }]}
                  
                />
              </View>
            </View>
            <View style={{ flexDirection: "row", alignItems: "center",marginTop: 10 }}>
              <View style={{ width: "100%" }}>
                <Text>
                  {I18n.t("mainpile.DropConcrete.final_concrete_over_volume_detail1")+' '+this.state.final_concreteleftovervolume+' '+I18n.t("mainpile.DropConcrete.final_concrete_over_volume_detail2")}
                </Text>
              </View>
              
            </View>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
            <View style={{ width: "100%" }}>
            <ModalSelector
                  data={this.state.final_concreteusedata}
                  onChange={this.selectreminuse}
                  selectTextStyle={styles.textButton}
                  cancelText="Cancel"
                  disabled={this.state.Edit_Flag == 0||this.state.checkfinalover==false}
                >
                  <TouchableOpacity
                    style={[
                      styles.button,
                      this.state.Edit_Flag == 0
                        ? { borderColor: MENU_GREY_ITEM }
                        : {}
                    ]}
                  >
                    <View style={styles.buttonTextStyle}>
                      <Text
                        style={[
                          styles.buttonText,
                          this.state.Edit_Flag == 0
                            ? { color: MENU_GREY_ITEM }
                            : {},{fontSize:16}
                        ]}
                      >
                        {this.state.final_concreteuse}
                      </Text>
                      <View
                        style={{
                          justifyContent: "flex-end",
                          flexDirection: "row"
                        }}
                      >
                        <Icon
                          name="arrow-drop-down"
                          type="MaterialIcons"
                          color={
                            this.state.Edit_Flag == 0
                              ? MENU_GREY_ITEM
                              : "#007CC2"
                          }
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                </ModalSelector>
            </View>
            </View>
            {this.state.final_concreteuseid==7&&
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View style={{ width: "100%" ,marginTop:10}}>
              <TextInput
                  // keyboardType="numeric"
                  value={this.state.final_remainusepilename}
                  underlineColorAndroid="transparent"
                  style={[styles.listTextbox]}
                  editable={this.state.Edit_Flag != 0}
                  placeholder={I18n.t("mainpile.DropConcrete.concreteRemainPileNo")}
                  onChangeText={data =>
                    this.setState({
                      final_remainusepilename: data
                    })
                  }
                  onSubmitEditing={()=>{
                    this.finalconcretevolume.focus()
                  }}
                />
              </View>
            </View>}
            
          </View>
          <View style={{ padding: 10, marginTop: 10 }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
            <View style={{ width: "80%" }}>
                <Text>
                  {I18n.t("mainpile.DropConcrete.final_concretevolumecal")}
                </Text>
              </View>
              <View style={{ width: "20%" }}>
              <TextInput
                  keyboardType="numeric"
                  editable={false}
                  value={this.state.final_concretevolumecal}
                  underlineColorAndroid="transparent"
                  style={[styles.listTextbox,{ backgroundColor: "#ebf1f7" }]}
                />
              </View>
            </View>
          </View>
          
         
          </View>
        }
          <View style={{ padding: 10, marginTop: 10 }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View style={{ width: "80%" }}>
                <Text>
                  {I18n.t("mainpile.DropConcrete.final_concretevolume")}
                </Text>
              </View>
              <View style={{ width: "20%" }}>
                <TextInput
                  ref={(input) => { this.finalconcretevolume = input; }}
                  keyboardType="numeric"
                  editable={this.state.Edit_Flag != 0}
                  value={this.state.final_concretevolume}
                  underlineColorAndroid="transparent"
                  style={styles.listTextbox}
                  onChangeText={data =>
                    this.setState({
                      final_concretevolume: data,
                      visible: false,
                      visible2: false
                    })
                  }
                  onEndEditing={() => {
                    let final_concretevolume = parseFloat(
                      Math.abs(this.state.final_concretevolume)
                    ).toFixed(2)
                    // console.warn(isNaN(final_concretevolume),final_concretevolume)
                    this.setState({
                      final_concretevolume: this.state.final_concretevolume
                        ? isNaN(final_concretevolume) == true
                          ? ""
                          : parseFloat(
                              Math.abs(this.state.final_concretevolume)
                            ).toFixed(2)
                        : ""
                    })
                  }}
                  onSubmitEditing={()=>{
                      if(this.state.checkfinalover==true){
                        // this.finaloverwidth.focus()
                      }else{
                        if(this.state.cuttremieButton){
                          this.cuttremiepipe.focus()
                        }
                        
                      }
                    }}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }

  renderCutTremie() {
    let cut = null
    if (this.state.cuttremiepipe != null) {
      cut = this.state.cuttremiepipe
    } else {
      cut = 0.0
    }
    return (
      <View style={{ marginTop: 10, padding: 5 }}>
        <View style={styles.dropDownTopic}>
          <Text style={{ fontSize: 20 }}>
            {I18n.t("mainpile.DropConcrete.cuttrem2")}
          </Text>
        </View>
        <View
          style={{ borderWidth: 1 }}
          ref={ref => {
            this.body = ref
          }}
        >
          <View style={{ padding: 10 }}>
            {!this.state.err ? (
              <View>
                <Text style={{ fontSize: 18 }}>
                  {I18n.t("mainpile.DropConcrete.cuttrem")}{" "}
                  {this.state.cuttremiepipe}{" "}
                  {I18n.t("mainpile.DropConcrete.cuttrem_plus")} (
                  {this.state.tremietemp.map((data, index) => {
                    if (index + 1 == this.state.tremietemp.length) {
                      return data
                    }
                    return data + " + "
                  })}{" "}
                  = {this.state.tremiepipecutlength.toString()}{" "}
                  {I18n.t("mainpile.DropConcrete.m1") + ")"}
                </Text>
                <Text>
                  {I18n.t("mainpile.DropConcrete.depth")}
                  {I18n.t("mainpile.DropConcrete.from")}
                  { this.props.category == 1||this.props.category ==4
                    ? I18n.t("mainpile.DropConcrete.bp")
                    : I18n.t("mainpile.DropConcrete.dw")}{" "}
                  = {this.state.theorydepth}{" "}
                  {I18n.t("mainpile.DropConcrete.m1")}
                </Text>
              </View>
            ) : (
              <Text style={{ fontSize: 18 }}>{this.state.errmsg}</Text>
            )}
            <View style={styles.listTopicSubRow}>
              <Text>
                {I18n.t("mainpile.DropConcrete.concretepourable")+this.state.concretemustpour+I18n.t("mainpile.DropConcrete.concretepourableM") }
              </Text>
            </View>
          </View>
          
          <View>
            <Text style={{ fontSize: 20, fontWeight: "bold" }}>
              {I18n.t("mainpile.DropConcrete.beforecut")}
            </Text>
          </View>
          <View style={{ padding: 10 }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View style={{ width: "80%" }}>
                <Text style={{ fontSize: 17 }}>
                  {I18n.t("mainpile.DropConcrete.tremiepipeinstall")}
                </Text>
              </View>
              <View style={{ width: "20%" }}>
                <TextInput
                  value={this.state.tremiepipeinstall.toString()}
                  editable={false}
                  keyboardType="numeric"
                  underlineColorAndroid="transparent"
                  style={[styles.listTextbox, { backgroundColor: "#ebf1f7" }]}
                />
              </View>
            </View>
          </View>

          <View style={{ padding: 10 }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View style={{ width: "80%" }}>
                <Text style={{ fontSize: 17 }}>
                  {I18n.t("mainpile.DropConcrete.tremiepipelength")}
                </Text>
              </View>
              <View style={{ width: "20%" }}>
                <TextInput
                  value={this.state.tremiepipelength.toString()}
                  editable={false}
                  keyboardType="numeric"
                  underlineColorAndroid="transparent"
                  style={[styles.listTextbox, { backgroundColor: "#ebf1f7" }]}
                />
              </View>
            </View>
          </View>

          <View style={{ padding: 10 }}>
            <RNTooltips
              text={
                this.props.shape == 1
                  ? I18n.t("tooltip.DC1")
                  : I18n.t("tooltip.DC1_dw")
              }
              textSize={16}
              visible={this.state.visible}
              reference={this.body}
              tintColor="#007CC2"
            />

            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View style={{ width: "80%", flexDirection: "row" }}>
                <Text style={{ fontSize: 17 }}>
                  {I18n.t(
                    "mainpile.DropConcrete.beforetremiepipeembeddedlength"
                  )}
                </Text>
                <Icon
                  name="ios-information-circle"
                  type="ionicon"
                  color="#517fa4"
                  onPress={() => {
                    this.setState(
                      {
                        visible: true,
                        dismiss: false,
                        ref: this.main,
                        visible2: false
                      },
                      () => {
                        // console.warn("reff")
                      }
                    )
                  }}
                />
              </View>

              <View style={{ width: "20%" }}>
                <TextInput
                  // value={this.state.beforetremiepipeembeddedlength.toString()}
                  value={this.state.beforetremiepipeembeddedlength>=0 ?this.state.beforetremiepipeembeddedlength.toString(): '0'}
                  editable={false}
                  keyboardType="numeric"
                  underlineColorAndroid="transparent"
                  style={[styles.listTextbox, { backgroundColor: "#ebf1f7" }]}
                />
              </View>
            </View>
          </View>
        </View>
        <View
          style={{ borderWidth: 1, borderTopWidth: 0 }}
          ref={ref => {
            this.body2 = ref
          }}
        >
          <View>
            <Text style={{ fontSize: 20, fontWeight: "bold" }}>
              {I18n.t("mainpile.DropConcrete.aftercut")}
            </Text>
          </View>
          <View style={{ padding: 10 }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View style={{ width: "80%" }}>
                <Text style={{ fontSize: 17 }}>
                  {I18n.t("mainpile.DropConcrete.cuttremiepipe")}
                </Text>
              </View>
              <View style={{ width: "20%" }}>
                <TextInput
                  ref={(input) => { this.cuttremiepipe = input; }}
                  keyboardType="numeric"
                  editable={this.state.Edit_Flag != 0}
                  value={this.state.cuttremiepipe.toString()}
                  underlineColorAndroid="transparent"
                  style={styles.listTextbox}
                  onChangeText={data => {
                    if (
                      this.props.edit == true &&
                      this.state.newesttruck == false
                    ) {
                      Alert.alert(
                        "",
                        I18n.t("mainpile.DropConcrete.alert.warn2"),
                        [
                          {
                            text: "OK"
                          }
                        ]
                      )
                      return
                    } else {
                      this.setState({
                        cuttremiepipe: data,
                        visible: false,
                        visible2: false
                      })
                    }
                  }}
                  onEndEditing={() => {
                    this.setState(
                      {
                        cuttremiepipe: this.state.cuttremiepipe
                          ? parseInt(Math.abs(this.state.cuttremiepipe))
                          : "",
                        visible: false
                      },
                      () => {
                        this.calculateTremie()
                        this.dryCalculate(this.state.cuttremiepipe)
                      }
                      // this.saveLocal()
                    )
                  }}
                />
              </View>
            </View>
          </View>

          <View style={{ padding: 10 }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View style={{ width: "80%" }}>
                <Text style={{ fontSize: 17 }}>
                  {I18n.t("mainpile.DropConcrete.tremiepipecutlength")}
                </Text>
              </View>
              <View style={{ width: "20%" }}>
                <TextInput
                  value={this.state.tremiepipecutlength.toString()}
                  editable={false}
                  keyboardType="numeric"
                  underlineColorAndroid="transparent"
                  style={[styles.listTextbox, { backgroundColor: "#ebf1f7" }]}
                />
              </View>
            </View>
          </View>

          <View style={{ padding: 10 }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View style={{ width: "80%" }}>
                <Text style={{ fontSize: 17 }}>
                  {I18n.t("mainpile.DropConcrete.remainingtremiepiplength")}
                </Text>
              </View>
              <View style={{ width: "20%" }}>
                <TextInput
                  value={this.state.remainingtremiepiplength.toString()}
                  editable={false}
                  keyboardType="numeric"
                  underlineColorAndroid="transparent"
                  style={[styles.listTextbox, { backgroundColor: "#ebf1f7" }]}
                />
              </View>
            </View>
          </View>
          {/* Fuck he want it and now he doesn't want it dafuq ??
          <View style={{ padding: 10 }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View style={{ width: "80%" }}>
                <Text style={{ fontSize: 17 }}>คอนกรีตอมหลังเท (ม.)</Text>
              </View>
              <View style={{ width: "20%" }}>
                <TextInput
                  value={this.state.concreteleft.toString()}
                  editable={false}
                  keyboardType="numeric"
                  underlineColorAndroid="transparent"
                  style={[styles.listTextbox, { backgroundColor: "#ebf1f7" }]}
                />
              </View>
            </View>
          </View>
          */}
          <RNTooltips
            text={I18n.t("tooltip.DC2")}
            textSize={16}
            visible={this.state.visible2}
            reference={this.body2}
            tintColor="#007CC2"
          />

          <View style={{ padding: 10 }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <View style={{ width: "80%", flexDirection: "row" }}>
                <Text style={{ fontSize: 17 }}>
                  {I18n.t("mainpile.DropConcrete.tremiepipeembeddedlength")}
                </Text>
                <Icon
                  name="ios-information-circle"
                  type="ionicon"
                  color="#517fa4"
                  onPress={() => {
                    this.setState(
                      {
                        visible2: true,
                        dismiss2: false,
                        ref2: this.main,
                        visible: false
                      },
                      () => {
                        // console.warn("reff")
                      }
                    )
                  }}
                />
              </View>
              <View style={{ width: "20%" }}>
                <TextInput
                  value={this.state.tremiepipeembeddedlength>=0 ?this.state.tremiepipeembeddedlength.toString(): '0'}
                  // value={this.state.tremiepipeembeddedlength.toString()}
                  editable={false}
                  keyboardType="numeric"
                  underlineColorAndroid="transparent"
                  style={[styles.listTextbox, { backgroundColor: "#ebf1f7" }]}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }

  onChangedate_startdrop = (event, selectedDate) => {
    if(event.type == 'set'){
      var date = moment(selectedDate).format('DD-MM-YYYY')
      this.setState({date_startdrop:date,datevisibledate_startdrop:false,datevisibletime_startdrop:true})
    }else{
      this.setState({datevisibledate_startdrop: false,datevisibletime_startdrop: false})
    }
  };
  onChangetime_startdrop = (event, selectedDate) => {
    var time = moment(selectedDate).format('HH:mm')
    var datetime_str = this.state.date_startdrop + time
    var datetime = moment(datetime_str, 'DD-MM-YYYY HH:mm')
    if(event.type == 'set'){
      this.setState({datevisibletime_startdrop:false})
      this.onStartSelectDate(datetime)
    }else{
      this.setState({datevisibledate_startdrop: false,datevisibletime_startdrop: false})
    }
  };

  onChangedate_finishdrop = (event, selectedDate) => {
    if(event.type == 'set'){
      var date = moment(selectedDate).format('DD-MM-YYYY')
      this.setState({date_finishdrop:date,datevisibledate_finishdrop:false,datevisibletime_finishdrop:true})
    }else{
      this.setState({datevisibledate_finishdrop: false,datevisibletime_finishdrop: false})
    }
  };
  onChangetime_finishdrop = (event, selectedDate) => {
    var time = moment(selectedDate).format('HH:mm')
    var datetime_str = this.state.date_finishdrop + time
    var datetime = moment(datetime_str, 'DD-MM-YYYY HH:mm')
    if(event.type == 'set'){
      this.setState({datevisibletime_finishdrop:false})
      this.onFinishSelectDate(datetime)
    }else{
      this.setState({datevisibledate_finishdrop: false,datevisibletime_finishdrop: false})
    }
  };

  render() {
    var text = ""
    if (this.props.category == 1||this.props.category ==4) {
      text = I18n.t("mainpile.DropConcrete.bp")
    } else {
      text = I18n.t("mainpile.DropConcrete.dw")
    }
    let currenttime = new Date()
    let startdrop = new Date(
      moment(this.state.startdrop, "DD/MM/YYYY HH:mm").format(
        "MM/DD/YYYY HH:mm"
      )
    )
    let finishdrop = new Date(
      moment(this.state.finishdrop, "DD/MM/YYYY HH:mm").format(
        "MM/DD/YYYY HH:mm"
      )
    )
    return (
      <View style={styles.listContainer}>
        <View
          style={
            this.props.category == 3 || this.props.category == 5
              ? [styles.listNavigation, { backgroundColor: DEFAULT_COLOR_4 }]
              : this.props.shape == 1
              ? styles.listNavigation
              : [styles.listNavigation, { backgroundColor: DEFAULT_COLOR_1 }]
          }
        >
          <Text style={styles.listNavigationText}>
            {I18n.t("mainpile.DropConcrete.DropConcrete")}
          </Text>
        </View>
        <View style={styles.listNavigationStep}>
          <Text style={styles.listNavigationTextStep}>
            {I18n.t("mainpile.DropConcrete.car") + this.props.index}
          </Text>
        </View>

        <KeyboardAwareScrollView
          ref="scroll"
          scrollEnabled={true}
          resetScrollToCoords={{ x: 0, y: 0 }}
          enableOnAndroid={true}
          enableResetScrollToCoords={false}
        >
          <View style={styles.listRow}>
            <View style={styles.listTopicSub}>
              {__DEV__ && (
                <View>
                  <Text>EDIT_FLAG {this.state.Edit_Flag}</Text>
                  <Text>
                    {I18n.t("mainpile.DropConcrete.planconcrete")}{" "}
                    {this.state.concretetheoreticalvolume}
                  </Text>
                  <Text>
                    {I18n.t("mainpile.DropConcrete.concretecol")}{" "}
                    {this.state.concretecumulative}
                  </Text>
                  <Text>CUTOFF {this.state.cutoff}</Text>
                  <Text>TOPCASING {this.state.topcasing}</Text>
                  <Text>DEPTH {this.state.depth}</Text>
                  <Text>TREMIE VOLUME {this.props.tremievolume}</Text>
                  <Text>
                    TREMIEPIPE {JSON.stringify(this.props.tremiepipe)}
                  </Text>
                  <Text>
                    tremie {I18n.t("mainpile.DropConcrete.used")}{" "}
                    {I18n.t("mainpile.DropConcrete.m")}
                    {this.props.tremiecutcumulativelength}
                  </Text>
                  <Text>
                    Tremie {I18n.t("mainpile.DropConcrete.used")}{" "}
                    {I18n.t("mainpile.DropConcrete.ton")}{" "}
                    {this.props.tremiecutcumulative}
                  </Text>
                </View>
              )}
            </View>

            <View style={styles.listTopicSub}>
              <Text style={styles.listTopicSubText}>
                {I18n.t("mainpile.concreteregister.Nocar")}
              </Text>
              <View style={styles.listSelectedView}>
                <ModalSelector
                  data={this.state.truckdata}
                  onChange={this.selectTruck}
                  selectTextStyle={styles.textButton}
                  cancelText="Cancel"
                  disabled={this.state.Edit_Flag == 0}
                >
                  <TouchableOpacity
                    style={[
                      styles.button,
                      this.state.Edit_Flag == 0
                        ? { borderColor: MENU_GREY_ITEM }
                        : {}
                    ]}
                  >
                    <View style={styles.buttonTextStyle}>
                      <Text
                        style={[
                          styles.buttonText,
                          this.state.Edit_Flag == 0
                            ? { color: MENU_GREY_ITEM }
                            : {}
                        ]}
                      >
                        {this.state.truck}
                      </Text>
                      <View
                        style={{
                          justifyContent: "flex-end",
                          flexDirection: "row"
                        }}
                      >
                        <Icon
                          name="arrow-drop-down"
                          type="MaterialIcons"
                          color={
                            this.state.Edit_Flag == 0
                              ? MENU_GREY_ITEM
                              : "#007CC2"
                          }
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                </ModalSelector>
              </View>
            </View>

            <View style={styles.listTopicSub}>
              <Text style={styles.listTopicSubText}>
                {I18n.t("mainpile.DropConcrete.concretevolume")}
              </Text>
              <TextInput
                value={this.state.concretevolume.toString()}
                editable={false}
                keyboardType="numeric"
                underlineColorAndroid="transparent"
                style={[styles.listTextbox, { backgroundColor: "#ebf1f7" }]}
              />
            </View>

            <View style={styles.listTopicSub}>
              <Text style={styles.listTopicSubText}>
                {I18n.t("mainpile.DropConcrete.starttime")}
              </Text>

              <View style={{ flexDirection: "row", justifyContent: "center" }}>
                {this.state.datevisibledate_startdrop &&<DateTimePicker
                  value={this.state.startdrop!="" ? startdrop  : currenttime}
                  isVisible={this.state.datevisibledate_startdrop}
                  is24Hour={true}
                  display='spinner'
                  mode='date'
                  onChange={this.onChangedate_startdrop}
                />}
                {this.state.datevisibletime_startdrop && <DateTimePicker
                  value={this.state.startdrop!="" ? startdrop  : currenttime}
                  isVisible={this.state.datevisibletime_startdrop}
                  is24Hour={true}
                  display='spinner'
                  mode='time'
                  onChange={this.onChangetime_startdrop}
                />}
                <TouchableOpacity
                  style={[styles.listicondatebox, { width: "68%" }]}
                  disabled={this.state.Edit_Flag == 0}
                  onPress={() => this.setState({ datevisibledate_startdrop: true })}
                >
                  <TextInput
                    value={
                      this.state.startdrop != ""
                        ? moment(
                            this.state.startdrop,
                            "DD/MM/YYYY HH:mm"
                          ).format("DD/MM/YYYY HH:mm")
                        : this.state.startdrop
                    }
                    editable={false}
                    keyboardType="numeric"
                    underlineColorAndroid="transparent"
                    style={{
                      flex: 1,
                      color: BASIC_COLOR,
                      textAlign: "center"
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.approveBox}
                  onPress={() => this.onArriveSelect("START")}
                  disabled={this.state.Edit_Flag == 0}
                >
                  <Text>{I18n.t("mainpile.DropConcrete.start")}</Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.listTopicSub}>
              <Text style={styles.listTopicSubText}>
                {I18n.t("mainpile.DropConcrete.endtime")}
              </Text>
              <View style={{ flexDirection: "row", justifyContent: "center" }}>
                {this.state.datevisibledate_finishdrop &&<DateTimePicker
                  value={this.state.finishdrop!="" ? finishdrop  : currenttime}
                  isVisible={this.state.datevisibledate_finishdrop}
                  is24Hour={true}
                  display='spinner'
                  mode='date'
                  onChange={this.onChangedate_finishdrop}
                />}
                {this.state.datevisibletime_finishdrop && <DateTimePicker
                  value={this.state.finishdrop!="" ? finishdrop  : currenttime}
                  isVisible={this.state.datevisibletime_finishdrop}
                  is24Hour={true}
                  display='spinner'
                  mode='time'
                  onChange={this.onChangetime_finishdrop}
                />}
                <TouchableOpacity
                  style={[styles.listicondatebox, { width: "68%" }]}
                  disabled={this.state.Edit_Flag == 0}
                  onPress={() => this.setState({ datevisibledate_finishdrop: true })}
                >
                  <TextInput
                    value={
                      this.state.finishdrop != ""
                        ? moment(
                            this.state.finishdrop,
                            "DD/MM/YYYY HH:mm"
                          ).format("DD/MM/YYYY HH:mm")
                        : this.state.finishdrop
                    }
                    editable={false}
                    keyboardType="numeric"
                    underlineColorAndroid="transparent"
                    style={{ flex: 1, color: BASIC_COLOR, textAlign: "center" }}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.approveBox}
                  onPress={() => this.onArriveSelect("FINISH")}
                  disabled={this.state.Edit_Flag == 0}
                >
                  <Text>{I18n.t("mainpile.DropConcrete.end")}</Text>
                </TouchableOpacity>
              </View>
            </View>


            <View style={styles.lastView}>
              {this.state.checkstopbetween == true ? (
                <Icon
                  disabled={this.state.Edit_Flag == 0}
                  name="check"
                  reverse
                  color="#6dcc64"
                  size={15}
                  onPress={() =>
                    // this.setState({ checkfinalpvc: !this.state.checkfinalpvc })
                    this.onPressStopBe()
                  }
                />
              ) : (
                <Icon
                  disabled={(this.state.Edit_Flag == 0 ||this.state.lastconcretetruckButton == true )&& true}
                  name="check"
                  reverse
                  color="grey"
                  size={15}
                  onPress={() =>
                    // this.setState({ checkfinalpvc: !this.state.checkfinalpvc })
                    this.onPressStopBe()
                  }
                />
              )}

              <View style={{ width: "60%" }}>
                <Text numberOfLines={0} style={{ textAlign: "center" }}>
                  {I18n.t("mainpile.DropConcrete.stoppourbetween")}
                </Text>
              </View>
            </View>
            <View style={styles.listTopicSub}>
              <Text style={styles.listTopicSubText}>
                {I18n.locale == "th"
                  ? I18n.t("mainpile.DropConcrete.depth") +
                    I18n.t("mainpile.DropConcrete.from") +
                    text +
                    " " +
                    "(" +
                    I18n.t("mainpile.DropConcrete.casingdepth") +
                    ")" +
                    " " +
                    I18n.t("mainpile.DropConcrete.m")
                  : I18n.t("mainpile.DropConcrete.casingdepth") +
                    " " +
                    I18n.t("mainpile.DropConcrete.depth") +
                    " " +
                    text +
                    " " +
                    I18n.t("mainpile.DropConcrete.m")}
              </Text>
              <TextInput
                keyboardType="numeric"
                editable={this.state.Edit_Flag != 0}
                value={this.state.casingdepth.toString()}
                underlineColorAndroid="transparent"
                style={styles.listTextbox}
                onChangeText={data => {
                  if (
                    this.props.edit == true &&
                    this.state.newesttruck == false
                  ) {
                    Alert.alert(
                      "",
                      I18n.t("mainpile.DropConcrete.alert.warn2"),
                      [
                        {
                          text: "OK"
                        }
                      ]
                    )
                  } else {
                    this.setState(
                      { casingdepth: data, visible: false, visible2: false },
                      () => {
                        console.log("casingdepthinput", this.state.casingdepth)
                      }
                    )
                  }
                }}
                onEndEditing={async () => {
                  if (
                    this.state.casingdepth == null ||
                    this.state.casingdepth == ""
                  ) {
                    this.setState({ casingdepth: "" })
                  } else {
                    let newdata = Math.abs(this.state.casingdepth)
                    if (isNaN(newdata) == true) {
                      newdata = ""
                      this.setState({ casingdepth: newdata }, async () => {
                        console.log(
                          "casingdepthinputend",
                          this.state.casingdepth
                        )
                        await this.calculateTremie()
                        if (
                          this.state.cuttremiepipe != "" &&
                          this.state.cuttremiepipe != null &&
                          this.state.cuttremiepipe != 0
                        ) {
                          await this.dryCalculate(this.state.cuttremiepipe)
                        } else {
                          await this.dryCalculate()
                        }
                      })
                    } else {
                      this.setState({ casingdepth: newdata }, async () => {
                        console.log(
                          "casingdepthinputend",
                          this.state.casingdepth
                        )
                        await this.calculateTremie()
                        if (
                          this.state.cuttremiepipe != "" &&
                          this.state.cuttremiepipe != null &&
                          this.state.cuttremiepipe != 0
                        ) {
                          await this.dryCalculate(this.state.cuttremiepipe)
                        } else {
                          await this.dryCalculate()
                        }
                      })
                    }
                  }
                }}
                onSubmitEditing={()=>{
                  if(this.state.showlastview &&
                    this.state.lastconcretetruckButton &&
                    this.state.finishdrop != null &&
                    this.state.finishdrop != "" ){
                      if(this.state.showcheck4 == true){
                        this.finalplummetheight.focus()
                      }else{
                        if(this.state.checkfinalover==true){
                          this.finaloverwidth.focus()
                        }else{
                          this.finalconcretevolume.focus()
                        }
                      }
                      return
                  }
                  if(this.state.cuttremieButton){
                          this.cuttremiepipe.focus()
                  }
                }}
              />
            </View>
          


           <View style={styles.listTopicSub}>
              <Text style={styles.listTopicSubText}>
                {I18n.t("mainpile.DropConcrete.concretepourcal")}
              </Text>
              <TextInput
                value={this.state.checkstopbetween||this.state.pretruckcheckstopbetween? this.state.concretevolumestop.toString():this.state.concretevolume.toString()}
                editable={false}
                keyboardType="numeric"
                underlineColorAndroid="transparent"
                style={[styles.listTextbox, { backgroundColor: "#ebf1f7" }]}
              />
            </View>
            
            <View style={styles.listTopicSubRow}>
              <Text style={styles.listLabel}>
                {I18n.t("mainpile.DropConcrete.imgDropcon")}
              </Text>
              <View style={styles.buttonImage}>
                <TouchableOpacity
                  style={[
                    styles.image,
                    this.state.dropconcreteimage.length > 0
                      ? { backgroundColor: "#6dcc64" }
                      : {}
                  ]}
                  onPress={() =>
                    this.onCameraRoll(
                      "dropconcreteimage",
                      this.state.dropconcreteimage,
                      "edit"
                    )
                  }
                >
                  <Icon name="image" type="entypo" color="#fff" />
                  <Text style={styles.imageText}>
                    {I18n.t("mainpile.liquidtestinsert.view")}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
           
            <View style={styles.listTopicSubRow}>
              <View style={{ width: "50%", alignItems: "center" }}>
                <TouchableOpacity
                  disabled={this.state.Edit_Flag == 0}
                  style={[
                    styles.buttonStyle,
                    this.state.lastconcretetruckButton == true
                      ? { backgroundColor: MENU_GREEN }
                      : {}
                    // !this.state.lastconcretetruckButtonEnable
                    //   ? { backgroundColor: "#BABABA" }
                    //   : {}
                  ]}
                  onPress={() => this.lastTruck()}
                >
                  <Text>{I18n.t("mainpile.DropConcrete.lartconconcrete")}</Text>
                </TouchableOpacity>
              </View>
              <View style={{ width: "50%", alignItems: "center" }}>
                <TouchableOpacity
                  disabled={this.state.Edit_Flag == 0}
                  style={[
                    styles.buttonStyle,
                    this.state.cuttremieButton
                      ? { backgroundColor: MENU_GREEN }
                      : {}
                  ]}
                  onPress={() => {
                    // if(this.state.Edit_Flag == 1){
                    if (
                      this.props.edit == true &&
                      this.state.newesttruck == false
                    ) {
                      Alert.alert(
                        "",
                        I18n.t("mainpile.DropConcrete.alert.warn2"),
                        [
                          {
                            text: "OK"
                          }
                        ]
                      )
                      return
                    } else {
                      this.setState(
                        {
                          cuttremieButton: !this.state.cuttremieButton
                        },
                        () => {
                          if (this.state.cuttremieButton == false) {
                            this.setState({ cuttremiepipe: 0 }, () => {
                              // this.calculateTremie(data)
                              this.calculateTremie()
                              this.dryCalculate()
                            })
                          }
                        }
                      )
                      // }
                    }
                  }}
                >
                  <Text>{I18n.t("mainpile.DropConcrete.cuttrem2")}</Text>
                </TouchableOpacity>
              </View>
            </View>
            {this.state.showlastview &&
              this.state.lastconcretetruckButton &&
              this.state.finishdrop != null &&
              this.state.finishdrop != "" &&
              this.renderLastTruck()}
            {this.state.cuttremieButton && this.renderCutTremie()}
          </View>
        </KeyboardAwareScrollView>
        {/* Bottom */}
        <View style={styles.buttonFixed}>
          <View style={styles.second}>
            <TouchableOpacity
              style={styles.firstButton}
              onPress={() => this.closeButton()}
            >
              <Text style={styles.selectButton}>
                {I18n.t("mainpile.steeldetail.button.close")}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.secondButton,
                this.props.category == 3 || this.props.category == 5
                  ? { backgroundColor: DEFAULT_COLOR_4 }
                  : this.props.shape == 1
                  ? { backgroundColor: MAIN_COLOR }
                  : { backgroundColor: DEFAULT_COLOR_1 },
                (this.state.slumpinvalid && !this.state.approved) ||
                this.state.Edit_Flag == 0
                  ? { backgroundColor: "#BABABA" }
                  : {}
              ]}
              onPress={() => this.saveButton()}
              disabled={
                (this.state.slumpinvalid && !this.state.approved) ||
                this.state.Edit_Flag == 0
              }
            >
              <Text style={styles.selectButton}>
                {I18n.t("mainpile.steeldetail.button.save")}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll || null,
    item: state.pile.item,
    concretevolume: state.concrete.concretevolume10,
    concretetruck: state.concrete.concretelist10,
    tremielist: state.tremie.tremielist,
    tremievolume: state.tremie.tremievolume,
    tremiepipe: state.tremie.tremiepipe,
    depth: state.tremie.depth,
    depth_child: state.tremie.depth_child
  }
}
export default connect(mapStateToProps, actions)(DropConcrete)
