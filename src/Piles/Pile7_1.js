import React, { Component } from "react"
import { View, Text, TouchableOpacity, TextInput } from "react-native"
import ModalSelector from "react-native-modal-selector"
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM } from "../Constants/Color"
import { Icon } from "react-native-elements"
import * as actions from "../Actions"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import I18n from "../../assets/languages/i18n"
import styles from "./styles/Pile7_1.style"

class Pile7_1 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      machine: "",
      machineid: "",
      driver: "",
      driverid: "",
      machineData: [{ key: 0, section: true, label: I18n.t("mainpile.7_1.machineselect") }],
      driverData: [{ key: 0, section: true, label: I18n.t("mainpile.7_1.driverselect") }],
      process: 7,
      step: 1,
      Edit_Flag: 1
    }
  }

  componentDidMount() {
    if (this.props.onRefresh === "refresh") {
      Actions.refresh({
        action: "start"
      })
    }
    // var pile = null
    // var pileMaster = null
    // var pile7 = null
    // if (this.props.pileAll[this.props.pileid] && this.props.pileAll[this.props.pileid]["7_1"]) {
    //   pile = this.props.pileAll[this.props.pileid]["7_1"].data
    //   pileMaster = this.props.pileAll[this.props.pileid]["7_1"].masterInfo
    // }
    // if (this.props.pileAll[this.props.pileid] && this.props.pileAll[this.props.pileid]["7_0"]) {
    //   pile7 = this.props.pileAll[this.props.pileid]["7_0"].data
    // }

    // if (this.props.pileAll != undefined && this.props.pileAll != "") {
    //   this.setState({ location: this.props.pileAll.currentLocation })
    // }

    // if (pileMaster) {
    //   if (pileMaster.MachineList) {
    //     let machine = [{ key: 0, section: true, label: I18n.t("mainpile.7_1.machineselect") }]
    //     pileMaster.MachineList.map((data, index) => {
    //       machine.push({ key: index + 1, label: data.machine_no, id: data.itemid })
    //       if (pile && pile.Machine) {
    //         if (pile.Machine.machine_no == data.machine_no && pile.Machine.itemid == data.itemid) {
    //           this.setState({ machine: data.machine_no, machineid: data.itemid })
    //         }
    //       }
    //       if (pile && pile.MachineName && this.state.machine == "") {
    //         if (pile.MachineName == data.machine_no) {
    //           // console.warn(' NAME ', pile.MachineName)
    //           this.setState({ machine: data.machine_no, machineid: data.itemid })
    //         }
    //       }
    //     })
    //     this.setState({ machineData: machine })
    //   }
    //   if (pileMaster.DriverList) {
    //     let driver = [{ key: 0, section: true, label: I18n.t("mainpile.7_1.driverselect") }]
    //     pileMaster.DriverList.map((data, index) => {
    //       let name = (data.nickname && data.nickname + "-") + data.firstname + " " + data.lastname
    //       driver.push({ key: index + 1, label: name, id: data.employee_id })
    //       if (pile && pile.DriverId) {
    //         if (pile.DriverId == data.employee_id) {
    //           this.setState({ driver: name, driverid: data.employee_id })
    //         }
    //       }
    //     })
    //     this.setState({ driverData: driver })
    //   }
    // }

    // if (pile7) {
    //   this.setState({ Edit_Flag: pile7.Edit_Flag })
    // }
  }

  componentWillReceiveProps(nextProps) {
    var pile = null
    var pileMaster = null
    var pile7 = null
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["7_1"]) {
      pile = nextProps.pileAll[this.props.pileid]["7_1"].data
      pileMaster = nextProps.pileAll[this.props.pileid]["7_1"].masterInfo
    }
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["7_0"]) {
      pile7 = nextProps.pileAll[this.props.pileid]["7_0"].data
    }

    if (nextProps.pileAll != undefined && nextProps.pileAll != "") {
      this.setState({ location: nextProps.pileAll.currentLocation })
    }

    if (pileMaster) {
      if (pileMaster.MachineList) {
        let machine = [{ key: 0, section: true, label: I18n.t("mainpile.7_1.machineselect") }]
        pileMaster.MachineList.map((data, index) => {
          machine.push({ key: index + 1, label: data.machine_no, id: data.itemid })
          if (pile && pile.Machine) {
            if (pile.Machine.machine_no == data.machine_no && pile.Machine.itemid == data.itemid) {
              this.setState({ machine: data.machine_no, machineid: data.itemid })
            }
          }
          if (pile && pile.MachineName && this.state.machine == "") {
            if (pile.MachineName == data.machine_no) {
              // console.warn(' NAME ', pile.MachineName)
              this.setState({ machine: data.machine_no, machineid: data.itemid })
            }
          }
          /**
           * If value don't have anything set value to nothing
           */
          // Value from API
          if (pile.hasOwnProperty('Machine') && pile.Machine == null) {
            this.setState({ machine: "", machineid: "" })
          }
          // Value from local
          if (pile.hasOwnProperty('MachineName') && pile.MachineName == null) {
            this.setState({ machine: "", machineid: "" })
          }
        })
        this.setState({ machineData: machine })
      }
      if (pileMaster.DriverList) {
        let driver = [{ key: 0, section: true, label: I18n.t("mainpile.7_1.driverselect") }]
        pileMaster.DriverList.map((data, index) => {
          let name = (data.nickname && data.nickname + "-") + data.firstname + " " + data.lastname
          driver.push({ key: index + 1, label: name, id: data.employee_id })
          if (pile && pile.DriverId) {
            if (pile.DriverId == data.employee_id) {
              this.setState({ driver: name, driverid: data.employee_id })
            }
          }
          if (pile.DriverId == null) {
            this.setState({ driver: "", driverid: "" })
          }
        })
        this.setState({ driverData: driver })
      }
    }

    if (pile7) {
      this.setState({ Edit_Flag: pile7.Edit_Flag })
    }
  }

  selectMachine = data => {
    this.setState({ machine: data.label, machineid: data.id }, () => this.saveLocal())
  }

  selectDriver = data => {
    this.setState({ driver: data.label, driverid: data.id }, () => this.saveLocal())
  }

  saveLocal() {
    var value = {
      process: 1,
      step: 7,
      pile_id: this.props.pileid,
      data: {
        DriverName: this.state.driver,
        DriverId: this.state.driverid,
        MachineName: this.state.machine,
        MachineId: this.state.machineid
      }
    }
    this.props.mainPileSetStorage(this.props.pileid, "7_1", value)
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    return (
      <View>
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t("mainpile.7_1.topic")}</Text>
        </View>
        <View style={{ marginTop: 20 }}>
          <View style={styles.boxTopic}>
            <Text style={{ marginLeft: 10 }}>{I18n.t("mainpile.7_1.machine")}</Text>
          </View>
          {this.state.Edit_Flag == 1 ? (
            <View style={styles.selectedView}>
              <ModalSelector
                data={this.state.machineData}
                onChange={this.selectMachine}
                selectTextStyle={styles.textButton}
                cancelText="Cancel"
              >
                <TouchableOpacity style={styles.button}>
                  <View style={styles.buttonTextStyle}>
                    <Text style={styles.buttonText}>{this.state.machine || I18n.t("mainpile.7_1.machineselect")}</Text>
                    <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                      <Icon name="arrow-drop-down" type="MaterialIcons" color="#007CC2" />
                    </View>
                  </View>
                </TouchableOpacity>
              </ModalSelector>
            </View>
          ) : (
            <View style={styles.selectedView}>
              <TouchableOpacity disabled style={[styles.button, { borderColor: MENU_GREY_ITEM }]}>
                <View style={styles.buttonTextStyle}>
                  <Text style={[styles.buttonText, { color: MENU_GREY_ITEM }]}>
                    {this.state.machine || I18n.t("mainpile.7_1.machineselect")}
                  </Text>
                  <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                    <Icon name="arrow-drop-down" type="MaterialIcons" color={MENU_GREY_ITEM} />
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          )}
        </View>
        <View style={{ marginTop: 10 }}>
          <View style={styles.boxTopic}>
            <Text style={{ marginLeft: 10 }}>{I18n.t("mainpile.7_1.driver")}</Text>
          </View>
          {this.state.Edit_Flag == 1 ? (
            <View style={styles.selectedView}>
              <ModalSelector
                data={this.state.driverData}
                onChange={this.selectDriver}
                selectTextStyle={styles.textButton}
                cancelText="Cancel"
              >
                <TouchableOpacity style={styles.button}>
                  <View style={styles.buttonTextStyle}>
                    <Text style={styles.buttonText}>{this.state.driver || I18n.t("mainpile.7_1.driverselect")}</Text>
                    <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                      <Icon name="arrow-drop-down" type="MaterialIcons" color="#007CC2" />
                    </View>
                  </View>
                </TouchableOpacity>
              </ModalSelector>
            </View>
          ) : (
            <View style={styles.selectedView}>
              <TouchableOpacity disabled style={[styles.button, { borderColor: MENU_GREY_ITEM }]}>
                <View style={styles.buttonTextStyle}>
                  <Text style={[styles.buttonText, { color: MENU_GREY_ITEM }]}>
                    {this.state.driver || I18n.t("mainpile.7_1.driverselect")}
                  </Text>
                  <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                    <Icon name="arrow-drop-down" type="MaterialIcons" color={MENU_GREY_ITEM} />
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(Pile7_1)
