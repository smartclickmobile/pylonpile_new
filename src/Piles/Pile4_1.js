import React, {Component} from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  Dimensions
} from "react-native"
import {Container, Content} from "native-base"
import {Icon} from "react-native-elements"
import Carousel from "react-native-snap-carousel"
import ModalSelector from "react-native-modal-selector"
import {
  MAIN_COLOR,
  SUB_COLOR,
  BASIC_COLOR,
  MENU_GREY_ITEM,
  BUTTON_COLOR,
  BLUE_COLOR
} from "../Constants/Color"
import {GroupEmployee} from "../Controller/API"
import I18n from "../../assets/languages/i18n"
import {Extra} from "../Controller/API"
import TableView from "../Components/TableView"
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux'
import * as actions from '../Actions'
import styles from './styles/Pile4_1.style'
var data = [
  {
    key: 0,
    section: true,
    label: I18n.t('mainpile.4_1.selector'),
    action: "head"
  },
  {
    key: 1,
    label: I18n.t('mainpile.4_1.edit'),
    action: "edit"
  }, {
    key: 2,
    label: I18n.t('mainpile.4_1.delete'),
    action: 'delete'
  }
]

var updateValue = []
var count = 0

const drillingfluidslistt = []
class Pile4_1 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 4,
      step: 1,
      data: [],
      DrillingFluids: [],
      location: {
        position: {
          lat: 1,
          log: 1
        }
      },
      Name: 'Test',
      pause: null,
      loading: false,
      dataMaster: null,
      Edit_Flag: 1,
      data4_0:[],
      updateData:false,
      status4:0,
      status5:0
    }
  }
  
  componentDidMount() {
    console.log(this.props.onRefresh)
    this.props.getDrillingFluids({
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      Language: I18n.locale
    })
    if (this.props.onRefresh == 'refresh' ) {
        Actions.refresh({action: 'start'})
    }
  }
  componentWillReceiveProps(nextProps) {
    var pile = null
    var pile4_0 = null
    var pileMaster = null
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['4_1']) {
      pile = nextProps.pileAll[this.props.pileid]['4_1'].data
      pileMaster = nextProps.pileAll[this.props.pileid]['4_1'].masterInfo
      this.setState({dataMaster: pileMaster,data:nextProps.pileAll[this.props.pileid]})
    }
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['4_0']) {
      pile4_0 = nextProps.pileAll[this.props.pileid]['4_0'].data
      this.setState({data4_0:pile4_0 ,status4:nextProps.pileAll[this.props.pileid].step_status.Step4Status,status5:nextProps.pileAll[this.props.pileid].step_status.Step5Status})
    }
    if (nextProps.pileAll != undefined && nextProps.pileAll != '') {
      this.setState({location: nextProps.pileAll.currentLocation})
    }
    if (pile4_0) {
      this.setState({Edit_Flag: pile4_0.Edit_Flag})
    }

    if (pileMaster) {
    }

  }

  updateState(value) {

  } 
  
  onEditData(index) {
    Actions.liquidTestInsert({
      jobid: this.props.jobid,
      pileid: this.props.pileid,
      process: this.state.process,
      step: this.state.step,
      Edit_Flag: this.state.Edit_Flag,
      lat: this.state.location == undefined
        ? 1
        : this.state.location.position.lat,
      log: this.state.location == undefined
        ? 1
        : this.state.location.position.log,
      drillingFluidslist: this.props.drillingfluidslist[index],
      drillingFluidslistedit: this.props.drillingfluidslist,
      LiquidTestInsertType: 'edit',
      index: index+1,
      shape: this.props.shape,
      edit: true,
      status: this.state.data4_0.Status,
      category: this.props.category
    })
  }

  deleteRow(index) {
    if(this.state.status5 != 0){
      if(index == 0 && this.props.drillingfluidslist.length == 1){
        Alert.alert('', I18n.t('mainpile.4_1.candelete'), [
          {
            text: 'OK'
          }
        ])
      }else{
        var count = 0
        for(var i = 0 ; i < this.props.drillingfluidslist.length ; i++){
          if(this.props.drillingfluidslist[index].TypeId == this.props.drillingfluidslist[i].TypeId){
            count++
          }
        }
        if(count == 1){
          if(this.props.drillingfluidslist.length-1 == index){
            Alert.alert("", I18n.t('mainpile.4_1.confirmdelect'), [
              {
                text: "Cancel"
              }, {
                text: "OK",
                onPress: () => {
                  this.onDeleteItem(this.props.drillingfluidslist[index])
                  drillingfluidslistt.splice(index, 1)
                  this.saveLocal('4_1')
                }
              }
            ], {cancelable: false})
          }else{
            Alert.alert('', I18n.t('mainpile.4_1.cannotdelete'), [
              {
                text: 'OK'
              }
            ])
          }

        }else{
          Alert.alert("", I18n.t('mainpile.4_1.confirmdelect'), [
            {
              text: "Cancel"
            }, {
              text: "OK",
              onPress: () => {
                this.onDeleteItem(this.props.drillingfluidslist[index])
                drillingfluidslistt.splice(index, 1)
                this.saveLocal('4_1')
              }
            }
          ], {cancelable: false})
        }
      }
    }else{
      var count = 0
      for(var i = 0 ; i < this.props.drillingfluidslist.length ; i++){
        if(this.props.drillingfluidslist[index].TypeId == this.props.drillingfluidslist[i].TypeId){
          count++
        }
      }
      if(count == 1){
        if(this.props.drillingfluidslist.length-1 == index){
          Alert.alert("", I18n.t('mainpile.4_1.confirmdelect'), [
            {
              text: "Cancel"
            }, {
              text: "OK",
              onPress: () => {
                this.onDeleteItem(this.props.drillingfluidslist[index])
                drillingfluidslistt.splice(index, 1)
                this.saveLocal('4_1')
              }
            }
          ], {cancelable: false})
        }else{
          Alert.alert('', I18n.t('mainpile.4_1.cannotdelete'), [
            {
              text: 'OK'
            }
          ])
        }
      }else{
        Alert.alert("", I18n.t('mainpile.4_1.confirmdelect'), [
          {
            text: "Cancel"
          }, {
            text: "OK",
            onPress: () => {
              this.onDeleteItem(this.props.drillingfluidslist[index])
              drillingfluidslistt.splice(index, 1)
              this.saveLocal('4_1')
            }
          }
        ], {cancelable: false})
      }
      
    }
   
  }

  async saveLocal(process_step) {
    var value = {
      process: 4,
      step: 1,
      pile_id: this.props.pileid,
      data: {
        DrillingFluids: this.props.drillingfluidslist
      }
    }
    if (this.state.data4_0.Status != 2) {
      await this.props.mainPileSetStorage(this.props.pileid, 'step_status', {Step4Status: 1})
    }
    await this.props.mainPileSetStorage(this.props.pileid, process_step, value)
    this.props.mainPileGetStorage(this.props.pileid, 'step_status')
  }

  onDeleteItem(data) {
    count = 0
    var value = {
      Language: I18n.locale,
      JobId: this.props.jobid,
      PileId: this.props.pileid,
      DrillingFluidId:data.Id,
      lat: this.state.location == undefined
        ? 1
        : this.state.location.position.lat,
      log: this.state.location == undefined
        ? 1
        : this.state.location.position.log,
    }
    this.props.pile4DeleteRow(value)
  }

  onAddData() {
    Actions.liquidTestInsert({
      jobid: this.props.jobid,
      pileid: this.props.pileid,
      process: this.state.process,
      step: this.state.step,
      Edit_Flag: '1',
      lat: this.state.location == undefined
        ? 1
        : this.state.location.position.lat,
      log: this.state.location == undefined
        ? 1
        : this.state.location.position.log,
      LiquidTestInsertType: 'add',
      datasize: this.props.drillingfluidslist.length,
      shape: this.props.shape,
      drillingFluidslist:this.props.drillingfluidslist,
      status: this.state.data4_0.Status,
      category: this.props.category,
      index: this.props.drillingfluidslist.length+1
    })
  }

  onScroll = (data) => {
    let offset = 0
    var currentOffsety = data.nativeEvent.contentOffset.y
    var currentOffsetx = data.nativeEvent.contentOffset.x
    // var direction = currentOffset > this.offset ? 'down' : 'up'
    // offset = currentOffset
    this.refs.scrollView.scrollTo({x: currentOffsetx, y: 0, animated: true})
    // console.log(currentOffsety)
    // console.log(currentOffsetx)
    // console.log(this.refs);
  }

  _renderTable() {
    var drillingfluidslist
    drillingfluidslist = this.props.drillingfluidslist
    
    console.log('updateValue',drillingfluidslist)
    let index = 0
    if (this.state.Edit_Flag == 0) {
      data = [
        {
          key: index++,
          section: true,
          label: I18n.t('mainpile.4_1.selector'),
          action: "head"
        },
        {
          key: index++,
          label: I18n.t('mainpile.4_1.see'),
          action: "edit"
        }
      ]
    }else {
      if(drillingfluidslist.length==1&&this.state.status5!=0){
        data = [
          {
            key: 0,
            section: true,
            label: I18n.t('mainpile.4_1.selector'),
            action: "head"
          },
          {
            key: 1,
            label: I18n.t('mainpile.4_1.edit'),
            action: "edit"
          }
        ]
      }else{
        data = [
          {
            key: 0,
            section: true,
            label: I18n.t('mainpile.4_1.selector'),
            action: "head"
          },
          {
            key: 1,
            label: I18n.t('mainpile.4_1.edit'),
            action: "edit"
          }, {
            key: 2,
            label: I18n.t('mainpile.4_1.delete'),
            action: 'delete'
          }
        ]
      }
      
    }
    if (Dimensions.get('screen').width > 360) {
      return (
        <View style={styles.listTable}>
        <View style={styles.scrollData}>
          {/* start */}

          <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollLeft} onScroll={this.onScroll}>

            <View style={styles.tableRow}>

              <View style={[styles.tableCol,{width:'20%' }]}>
               {
                drillingfluidslist.map((item, key) => {
                return (<View key={key} style={[styles.tableContent,(item.Local?{backgroundColor:'#F7F6BD'}:{})]}>
                 <View key={key} style={styles.tableContentCol}>
                        <ModalSelector
                        data={data}
                          cancelText="Cancel"
                          onChange={(option) => {
                             if (option.action == "edit") {
                               this.onEditData(key)
                             } else if (option.action == "delete") {
                               this.deleteRow(key)
                             }
                           }}
                           >
                          <View style={styles.tableContentArea}>
                            <Text numberOfLines={1} style={styles.tableContentText}>{key+1}</Text>
                            <Text numberOfLines={1} style={styles.tableContentText}>{item.TypeName}</Text>
                            <Text numberOfLines={1} style={styles.tableContentText}>{item.PositionName !== null ? '(' +item.PositionName + ')' : item.PositionName }</Text>
                            <Text numberOfLines={1} style={styles.tableContentText}>{item.SlurryTypeName !== null && item.SlurryTypeName !== undefined ? '(' +item.SlurryTypeName + ')' : '' }</Text>

                          </View>
                        </ModalSelector>
                        <View style={styles.tableContentBorder}></View>
                      </View>    
                </View>)
              })
            }
              </View>
              {/* <ScrollView horizontal={true} onScroll={this.onScroll} showsHorizontalScrollIndicator={false}> */}
              <View style={[styles.tableCol,{width:'20%' }]}>
              {
                drillingfluidslist.map((item, key) => {
                  return (<View key={key} style={[styles.tableContent,(item.Local?{backgroundColor:'#F7F6BD'}:{})]}>
                 <View key={key} style={styles.tableContentCol}>
                        <ModalSelector
                          data={data}
                          cancelText="Cancel"
                          onChange={(option) => {
                             if (option.action == "edit") {
                               this.onEditData(key)
                             } else if (option.action == "delete") {
                               this.deleteRow(key)
                             }
                           }}
                           >
                          <View style={styles.tableContentArea}>
                            <Text style={styles.tableContentText}>{isNaN(parseFloat(item.Ph).toFixed(2)) ? '' : parseFloat(item.Ph).toFixed(2)}</Text>
                          </View>
                        </ModalSelector>
                        <View style={styles.tableContentBorder}></View>
                      </View>
                </View>)
                })
              }
              </View>

              {/* <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} onScroll={this.onScroll}> */}

                <View style={[styles.tableCol,{width:'20%' }]}>
                {
                  drillingfluidslist.map((item, key) => {
                    return (<View key={key} style={[styles.tableContent,(item.Local?{backgroundColor:'#F7F6BD'}:{})]}>
                    <View key={key} style={styles.tableContentCol}>
                          <ModalSelector
                            data={data}
                            cancelText="Cancel"
                            onChange={(option) => {
                               if (option.action == "edit") {
                                 this.onEditData(key)
                               } else if (option.action == "delete") {
                                 this.deleteRow(key)
                               }
                             }}
                             >
                              <View style={styles.tableContentArea}>
                                <Text style={styles.tableContentText}>{isNaN(parseFloat(item.Density).toFixed(2)) ? '' : parseFloat(item.Density).toFixed(2)}</Text>
                              </View>
                          </ModalSelector>
                          <View style={styles.tableContentBorder}></View>
                        </View>
                      
                  </View>)
                  })
                }
                </View>

                <View style={[styles.tableCol,{width:'20%' }]}>
                {
                  drillingfluidslist.map((item, key) => {
                    return (<View key={key} style={[styles.tableContent,(item.Local?{backgroundColor:'#F7F6BD'}:{})]}>
                   <View key={key} style={styles.tableContentCol}>
                          <ModalSelector
                            data={data}
                            cancelText="Cancel"
                            onChange={(option) => {
                               if (option.action == "edit") {
                                 this.onEditData(key)
                               } else if (option.action == "delete") {
                                 this.deleteRow(key)
                               }
                             }}
                             >
                          <View style={styles.tableContentArea}>
                            <Text style={styles.tableContentText}>{item.Viscosity}</Text>
                          </View>
                          </ModalSelector>
                          <View style={styles.tableContentBorder}></View>
                        </View> 
                  </View>)
                  })
                }
                </View>

                <View style={[styles.tableCol,{width:'20%' }]}>
                {
                  drillingfluidslist.map((item, key) => {
                    return (<View key={key} style={[styles.tableContent,(item.Local?{backgroundColor:'#F7F6BD'}:{})]}>
                    <View key={key} style={styles.tableContentCol}>
                          <ModalSelector
                            data={data}
                            cancelText="Cancel"
                            onChange={(option) => {
                               if (option.action == "edit") {
                                 this.onEditData(key)
                               } else if (option.action == "delete") {
                                 this.deleteRow(key)
                               }
                             }}
                             >
                          <View style={styles.tableContentArea}>
                            <Text style={styles.tableContentText}>{isNaN(parseFloat(item.Sand).toFixed(2)) ? '' : parseFloat(item.Sand).toFixed(2)}</Text>
                          </View>
                          </ModalSelector>
                          <View style={styles.tableContentBorder}></View>
                        </View>  
                  </View>)
                  })
                }
                </View>

                {/* <View style={styles.tableCol}>
                  <View style={[styles.tableContent, styles.tabelExtra]}>
                    {
                      this.state.DrillingFluids.map((item, key) => {
                        return (<View key={key} style={styles.tableContentCol}>
                          <View style={styles.tableContentArea}>
                          <View style={styles.tableAction}>
                            <TouchableOpacity activeOpacity={0.8} onPress={() => this.onEditData(key)}>
                              <View style={styles.buttonEdit}>
                                <Image source={require('../../assets/image/icon_edit.png')}/>
                              </View>
                            </TouchableOpacity>
                            {
                              this.state.Edit_Flag == 1
                                ? <TouchableOpacity activeOpacity={0.8} onPress={() => this.deleteRow(key)}>
                                    <View style={styles.buttonDelete}>
                                      <Image source={require('../../assets/image/icon_bin.png')}/>
                                    </View>
                                  </TouchableOpacity>
                                : <TouchableOpacity disabled={true} activeOpacity={0.8} onPress={() => this.deleteRow(key)}>
                                    <View style={[
                                        styles.buttonDelete, {
                                          borderColor: MENU_GREY_ITEM
                                        }
                                      ]}>
                                      <Image source={require('../../assets/image/icon_bin_disable.png')}/>
                                    </View>
                                  </TouchableOpacity>
                            }
                          </View>
                        </View>
                        <View style={styles.tableContentBorder}></View>
                        </View>)
                      })
                    }
                  </View>
                </View> */}

              {/* </ScrollView> */}
                {/* </ScrollView> */}
            </View>
          </ScrollView>

          {/* end */}
        </View>
      </View>)
    }else {
      return (
        <View style={styles.listTable}>

        <View style={styles.scrollData}>
          {/* start */}

          <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollLeft} onScroll={this.onScroll}>

            <View style={styles.tableRow}>

              <View style={[styles.tableCol,{width: 80}]}>
              {
                drillingfluidslist.map((item, key) => {
                  // console.warn('item',item)
                  return (<View key={key} style={[styles.tableContent,(item.Local?{backgroundColor:'#F7F6BD'}:{})]}>
                 <View  style={styles.tableContentCol}>
                        <ModalSelector
                          data={data}
                          cancelText="Cancel"
                          onChange={(option) => {
                             if (option.action == "edit") {
                               this.onEditData(key)
                             } else if (option.action == "delete") {
                               this.deleteRow(key)
                             }
                           }}
                           >
                          <View style={styles.tableContentArea}>
                            <Text numberOfLines={1} style={styles.tableContentText}>{key+1}</Text>
                            <Text numberOfLines={1} style={styles.tableContentText}>{item.TypeName}</Text>
                            <Text numberOfLines={1} style={styles.tableContentText}>{item.PositionName !== null ? '(' +item.PositionName + ')' : item.PositionName }</Text>
                            <Text numberOfLines={1} style={styles.tableContentText}>{item.SlurryTypeName !== null && item.SlurryTypeName !== undefined ? '(' +item.SlurryTypeName + ')' : '' }</Text>
                          </View>
                        </ModalSelector>
                        <View style={styles.tableContentBorder}></View>
                      </View>
                   </View>)
                })
              }
              </View>
              <ScrollView horizontal={true} onScroll={this.onScroll} showsHorizontalScrollIndicator={false}>
              <View style={[styles.tableCol,{width: 60}]}>
              {
                drillingfluidslist.map((item, key) => {
                  return (<View key={key} style={[styles.tableContent,(item.Local?{backgroundColor:'#F7F6BD'}:{})]}>
                 <View key={key} style={styles.tableContentCol}>
                        <ModalSelector
                          data={data}
                          cancelText="Cancel"
                          onChange={(option) => {
                             if (option.action == "edit") {
                               this.onEditData(key)
                             } else if (option.action == "delete") {
                               this.deleteRow(key)
                             }
                           }}
                           >
                          <View style={styles.tableContentArea}>
                            <Text  style={styles.tableContentText}>{isNaN(parseFloat(item.Ph).toFixed(2)) ? '' : parseFloat(item.Ph).toFixed(2)}</Text>
                          </View>
                        </ModalSelector>
                        <View style={styles.tableContentBorder}></View>
                      </View>
                </View>)
                })
              }
              </View>

              {/* <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} onScroll={this.onScroll}> */}

                <View style={[styles.tableCol,{width: 70}]}>
                {
                  drillingfluidslist.map((item, key) => {
                    return (<View key={key} style={[styles.tableContent,(item.Local?{backgroundColor:'#F7F6BD'}:{})]}>
                    <View key={key} style={styles.tableContentCol}>
                          <ModalSelector
                            data={data}
                            cancelText="Cancel"
                            onChange={(option) => {
                               if (option.action == "edit") {
                                 this.onEditData(key)
                               } else if (option.action == "delete") {
                                 this.deleteRow(key)
                               }
                             }}
                             >
                              <View style={styles.tableContentArea}>
                                <Text style={styles.tableContentText}>{isNaN(parseFloat(item.Density).toFixed(2)) ? '' : parseFloat(item.Density).toFixed(2)}</Text>
                              </View>
                          </ModalSelector>
                          <View style={styles.tableContentBorder}></View>
                        </View>
                  </View>)
                  })
                }
                </View>

                <View style={[styles.tableCol,{width: 80}]}>
                {
                  drillingfluidslist.map((item, key) => {
                    return (<View key={key} style={[styles.tableContent,(item.Local?{backgroundColor:'#F7F6BD'}:{})]}>
                    <View key={key} style={styles.tableContentCol}>
                          <ModalSelector
                            data={data}
                            cancelText="Cancel"
                            onChange={(option) => {
                               if (option.action == "edit") {
                                 this.onEditData(key)
                               } else if (option.action == "delete") {
                                 this.deleteRow(key)
                               }
                             }}
                             >
                          <View style={styles.tableContentArea}>
                            <Text style={styles.tableContentText}>{item.Viscosity}</Text>
                          </View>
                          </ModalSelector>
                          <View style={styles.tableContentBorder}></View>
                        </View>   
                  </View>)
                  })
                }
                </View>

                <View style={[styles.tableCol,{width:  70}]}>
                {
                  drillingfluidslist.map((item, key) => {
                    return (<View key={key} style={[styles.tableContent,(item.Local?{backgroundColor:'#F7F6BD'}:{})]}>
                    <View key={key} style={styles.tableContentCol}>
                          <ModalSelector
                            data={data}
                            cancelText="Cancel"
                            onChange={(option) => {
                               if (option.action == "edit") {
                                 this.onEditData(key)
                               } else if (option.action == "delete") {
                                 this.deleteRow(key)
                               }
                             }}
                             >
                          <View style={styles.tableContentArea}>
                            <Text style={styles.tableContentText}>{isNaN(parseFloat(item.Sand).toFixed(2)) ? '' : parseFloat(item.Sand).toFixed(2)}</Text>
                          </View>
                          </ModalSelector>
                          <View style={styles.tableContentBorder}></View>
                        </View>
                  </View>)
                })
              }
                </View>

                {/* <View style={styles.tableCol}>
                  <View style={[styles.tableContent, styles.tabelExtra]}>
                    {
                      this.state.DrillingFluids.map((item, key) => {
                        return (<View key={key} style={styles.tableContentCol}>
                          <View style={styles.tableContentArea}>
                          <View style={styles.tableAction}>
                            <TouchableOpacity activeOpacity={0.8} onPress={() => this.onEditData(key)}>
                              <View style={styles.buttonEdit}>
                                <Image source={require('../../assets/image/icon_edit.png')}/>
                              </View>
                            </TouchableOpacity>
                            {
                              this.state.Edit_Flag == 1
                                ? <TouchableOpacity activeOpacity={0.8} onPress={() => this.deleteRow(key)}>
                                    <View style={styles.buttonDelete}>
                                      <Image source={require('../../assets/image/icon_bin.png')}/>
                                    </View>
                                  </TouchableOpacity>
                                : <TouchableOpacity disabled={true} activeOpacity={0.8} onPress={() => this.deleteRow(key)}>
                                    <View style={[
                                        styles.buttonDelete, {
                                          borderColor: MENU_GREY_ITEM
                                        }
                                      ]}>
                                      <Image source={require('../../assets/image/icon_bin_disable.png')}/>
                                    </View>
                                  </TouchableOpacity>
                            }
                          </View>
                        </View>
                        <View style={styles.tableContentBorder}></View>
                        </View>)
                      })
                    }
                  </View>
                </View> */}

              {/* </ScrollView> */}
                </ScrollView>
            </View>
          </ScrollView>

          {/* end */}
        </View>
      </View>)
    }
  }

  renderHead(){
    if (Dimensions.get('screen').width > 360) {
      return(
        <View style={styles.tableRow}>
          <View style={[styles.tableHead,{width:'20%'}]}>
            <Text style={[styles.tableHeadText,{fontSize:12}]}>#{"\n"}</Text>
          </View>
          {/* <ScrollView contentContainerStyle={{width:'100%'}} ref={'scrollView'} horizontal={true} scrollEnabled={false} showsHorizontalScrollIndicator={false} > */}
          <View style={[styles.tableHead,{width:'20%'}]}>
            <Text style={[styles.tableHeadText,{fontSize:12}]}>pH{"\n"}</Text>
          </View>
          {/* <ScrollView showsHorizontalScrollIndicator={false} scrollEnabled={false} horizontal={true} ref='scrollView'> */}
            <View style={[styles.tableHead,{width:'20%'}]}>
              <Text style={[styles.tableHeadText,{fontSize:12}]}>Density{"\n"}{I18n.t("mainpile.4_1.unit_density")}</Text>
            </View>
            <View style={[styles.tableHead,{width:'20%'}]}>
              <Text style={[styles.tableHeadText,{fontSize:12}]}>Viscosity{"\n"}{I18n.t("mainpile.4_1.unit_viscosity")}</Text>
            </View>
            <View style={[styles.tableHead,{width:'20%'}]}>
              <Text style={[styles.tableHeadText,{fontSize:12}]}>% Sand</Text>
            </View>
            {/* <View style={[styles.tableHead, styles.tabelExtra]}>
              <Text style={styles.tableHeadText}></Text>
            </View> */}
          {/* </ScrollView> */}
          {/* </ScrollView> */}
        </View>
      )
    }else {
      return(
        <View style={styles.tableRow}>
          <View style={[styles.tableHead,{width: 80}]}>
            <Text style={styles.tableHeadText}>#{"\n"}</Text>
          </View>
          <ScrollView ref={'scrollView'} horizontal={true} scrollEnabled={false} showsHorizontalScrollIndicator={false} >
          <View style={[styles.tableHead,{width: 60}]}>
            <Text style={styles.tableHeadText}>pH{"\n"}</Text>
          </View>
          {/* <ScrollView showsHorizontalScrollIndicator={false} scrollEnabled={false} horizontal={true} ref='scrollView'> */}
            <View style={[styles.tableHead,{width: 70}]}>
              <Text style={styles.tableHeadText}>Density{"\n"}{I18n.t("mainpile.4_1.unit_density")}</Text>
            </View>
            <View style={[styles.tableHead,{width:  80}]}>
              <Text style={styles.tableHeadText}>Viscosity{"\n"}{I18n.t("mainpile.4_1.unit_viscosity")}</Text>
            </View>
            <View style={[styles.tableHead,{width:  70}]}>
              <Text style={styles.tableHeadText}>% Sand</Text>
            </View>
            {/* <View style={[styles.tableHead, styles.tabelExtra]}>
              <Text style={styles.tableHeadText}></Text>
            </View> */}
          {/* </ScrollView> */}
          </ScrollView>
        </View>
      )
    }
  }

  render() {
      return (
        <View style={{height:'70%'}}>
          <View>
            <View style={styles.listTopic}>
              <Text style={styles.listTopicText}>{I18n.t("mainpile.4_1.title")}</Text>
              {
                this.state.Edit_Flag == 1
                  ? <TouchableOpacity activeOpacity={0.8} onPress={() => this.onAddData()}>
                      <View style={styles.listButton}>
                        <Image source={require('../../assets/image/icon_add.png')}/>
                        <Text style={styles.listButtonText}>{I18n.t("mainpile.4_1.add_data")}</Text>
                      </View>
                    </TouchableOpacity>
                  : <View/>
              }
            </View>
            <View style={{marginTop:10}}>
                {this.renderHead()}
            </View>
            <ScrollView>
            {
              this.props.drillingfluidslist.length > 0
                ?
                <View style={{marginTop:this.props.pile4Type === 'head' ? 10: 0}}>
                  {/* <ScrollView horizontal={true} onScroll={this.onScroll}> */}
                    {this._renderTable()}
                  {/* </ScrollView> */}

                  </View>
                : <View style={styles.listTableDetail}>
                  <Text style={styles.listTableDetailText}>{I18n.t("mainpile.4_1.no_data")}
                    {I18n.locale=='th'?"\n":''}
                    {I18n.locale=='th'?<Text style={styles.textHighligh}>{I18n.t("mainpile.4_1.add_data")}</Text>:''}
                    {I18n.locale=='th'?I18n.t("mainpile.4_1.add_data_test"):''}
                  </Text>
                  </View>
            }
            </ScrollView>
          </View>

      </View>
      )

  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll,
    drillingfluidslist: state.drillingFluids.drillingfluidslist,
    error: state.pile.error
  }
}

export default connect(mapStateToProps, actions, null, {withRef: true})(Pile4_1)
