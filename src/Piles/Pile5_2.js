import React, { Component } from "react"
import { View, Text, StyleSheet, TouchableOpacity, TextInput, Image, Dimensions } from "react-native"
import { MAIN_COLOR, SUB_COLOR, BASIC_COLOR, MENU_GREY_ITEM, BUTTON_COLOR, BLUE_COLOR } from "../Constants/Color"
import ModalSelector from "react-native-modal-selector"
import { Icon } from "react-native-elements"
import I18n from "../../assets/languages/i18n"
import { Extra } from "../Controller/API"
import styles from './styles/Pile5_2.style'
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"

class Pile5_2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data:{
        CutOff:0,
        Depth:0,
        GroundLevel:0,
        PileTip:0,
        TopCasingLevel:0,
      },
      process: 5,
      step: 2,
      pile:[],
      casing_length:15,
    }
  }

  componentDidMount (){
    console.log('test did mount',this.props.pileAll[this.props.pileid])
    let pileMaster,pile5_0,pile3_3,pile3_5 = null
    if(this.props.pileAll[this.props.pileid]){
      if(this.props.pileAll[this.props.pileid]['2_1'].masterInfo){
        this.setState({casing_length:this.props.pileAll[this.props.pileid]['2_1'].data.Length})
      }
      if(this.props.pileAll[this.props.pileid]['3_3'].masterInfo){
        pile3_3 = this.props.pileAll[this.props.pileid]['3_3'].data
      }
      if(this.props.pileAll[this.props.pileid]['3_5'].masterInfo){
        pile3_5 = this.props.pileAll[this.props.pileid]['3_5'].data
      }
      if(this.props.pileAll[this.props.pileid]['5_0'].masterInfo){
        pile5_0 = this.props.pileAll[this.props.pileid]['5_0'].data
      }
      if(this.props.pileAll[this.props.pileid]['5_2'].masterInfo){
        pileMaster = this.props.pileAll[this.props.pileid]['5_2'].masterInfo
        if(pileMaster){
          this.insertData(pileMaster,pile3_3,pile3_5)
        }
      }
    }
    
  }

  async componentWillReceiveProps(nextProps){
    var pile5_0 = null
    var pileMaster = null
    var pile3_3 = null
    var pile3_5 = null
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['5_2']) {
      pileMaster = nextProps.pileAll[this.props.pileid]['5_2'].masterInfo
      // this.setState({data:nextProps.pileAll[this.props.pileid]['5_2'].masterInfo})
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['5_0']) {
      pile5_0 = nextProps.pileAll[this.props.pileid]['5_0'].data
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['3_3']) {
      // if(this.props.shape == 2){
        // pile3_5 = nextProps.pileAll[this.props.pileid]['3_5'].data
        pile3_3 = nextProps.pileAll[this.props.pileid]['3_3'].data
        this.setState({casing_length:nextProps.pileAll[this.props.pileid]['2_1'].data.Length})
      // }else{
      //   pile3_3 = nextProps.pileAll[this.props.pileid]['3_3'].data
      //   this.setState({casing_length:nextProps.pileAll[this.props.pileid]['2_1'].data.Length})
      // }
      
    }
    console.log('pile3_5 test',nextProps.pileAll[this.props.pileid]['3_5'].data)
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['3_5']) {
        pile3_5 = nextProps.pileAll[this.props.pileid]['3_5'].data
    }
    if (nextProps.item && nextProps.error == null) {
      this.setState({ pile: nextProps.item })
    }

    if (pileMaster) {
      this.insertData(pileMaster,pile3_3,pile3_5)
    }

  }

  insertData = (pileMaster,pile3_3,pile3_5) =>{
    if (this.state.data != pileMaster.PileInfo) {
      this.setState({data:pileMaster.PileInfo},() =>{
        if(this.props.category == 5){
          if(this.props.shape == 2){
            if (pile3_5) {
              var data = pileMaster.PileInfo
              if (pile3_5.ground) {
                data.GroundLevel = pile3_5.ground
              }
              if (pile3_5.top) {
                data.TopCasingLevel = pile3_5.top
                data.Depth =  pile3_5.top - data.PileTip
                data.BGWLevel = pile3_5.top - 1.5
                data.DepthChild =  pile3_5.top - data.PileTipChild
              }
              
              console.log('5 pileMaster.PileInfo pile3_5 mae',pile3_5.top,data.PileTipChild,data);
              this.setState({data:data})
            }
            console.log('5 pileMaster.PileInfo not pile3_5');

          }else{
            if (pile3_3) {
              var data = pileMaster.PileInfo
              if (pile3_3.ground) {
                data.GroundLevel = pile3_3.ground
              }
              if (pile3_3.top) {
                data.TopCasingLevel = pile3_3.top
                data.DepthChild =  pile3_3.top - data.PileTipChild
                data.Depth =  pile3_3.top - data.PileTip
              }
              console.log('5 pileMaster.PileInfo pile3_3 look',pile3_3.top,data.PileTipChild,data);
              this.setState({data:data})
            }
            console.log('5 pileMaster.PileInfo not pile3_3');
          }
        }else{
          if(this.props.shape == 2){
            if (pile3_5) {
              var data = pileMaster.PileInfo
              if (pile3_5.ground) {
                data.GroundLevel = pile3_5.ground
              }
              if (pile3_5.top) {
                data.TopCasingLevel = pile3_5.top
                data.Depth =  pile3_5.top - data.PileTip
                data.BGWLevel = pile3_5.top - 1.5
              }
              
              console.log('pileMaster.PileInfo pile3_5',pile3_5.top,data.PileTip,pile3_5);
              this.setState({data:data})
            }
          }else{
            if (pile3_3) {
              var data = pileMaster.PileInfo
              if (pile3_3.ground) {
                data.GroundLevel = pile3_3.ground
              }
              if (pile3_3.top) {
                data.TopCasingLevel = pile3_3.top
                data.Depth =  pile3_3.top - data.PileTip
              }
              console.log('pileMaster.PileInfo pile3_3',pile3_3.top,data.PileTip);
              this.setState({data:data})
            }
          }
        }   
      })
    }
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    // console.warn(this.props.shape, this.props.category, this.props.parent)
    return (
      <View style={styles.listContainer}>
        <View style={styles.listTopic}>
          <Text style={styles.listTopicText}>{I18n.t('mainpile.5_2.info')}</Text>
        </View>
        <View style={styles.listInfo}>
          <View style={{alignItems: 'center', alignSelf: 'center'}}>
          {
            (this.props.shape == 1 && this.props.category != 5)&&
            <View style={{flex: 1, flexDirection: 'row', paddingTop: 30, paddingBottom: 20}}>
              <Image source={require('../../assets/image/pileB.jpg')} style={{width: Dimensions.get('window').width-150, height: Dimensions.get('window').height-360, resizeMode: 'contain'}}/>
              <View>
                <View style={{flexDirection: 'column', justifyContent: 'center', marginTop: -30}}>
                  <Text>{I18n.t("mainpile.5_2.boried_pile.top_elevation")}</Text>
                  <TextInput
                    value={this.state.data.TopCasingLevel == null ? '0.000' : parseFloat(this.state.data.TopCasingLevel.toString()).toFixed(3) >= 0 ? '+'+parseFloat(this.state.data.TopCasingLevel.toString()).toFixed(3):parseFloat(this.state.data.TopCasingLevel.toString()).toFixed(3)}
                    editable = {false}
                    keyboardType={"numeric"}
                    underlineColorAndroid='transparent'
                    style={[styles.listTextbox, styles.textboxDisable]}
                  />
                </View>
            <View style={{flexDirection: 'column'}}>
                <Text>{I18n.t("mainpile.5_2.boried_pile.ground_elevation")}</Text>
                <TextInput
                  value={this.state.data.GroundLevel == null ? '0.000' : parseFloat(this.state.data.GroundLevel.toString()).toFixed(3) >= 0 ? '+'+parseFloat(this.state.data.GroundLevel.toString()).toFixed(3):parseFloat(this.state.data.GroundLevel.toString()).toFixed(3)}
                  editable = {false}
                  keyboardType={"numeric"}
                  underlineColorAndroid='transparent'
                  style={[styles.listTextbox, styles.textboxDisable]}
                />
              </View>
            <View style={{flexDirection: 'column', justifyContent: 'center'}}>
              <Text>{I18n.t("mainpile.5_2.boried_pile.cutoff_elevation")}</Text>
                <TextInput
                  value={this.state.data.CutOff == null ? '0.000' : parseFloat(this.state.data.CutOff.toString()).toFixed(3) >= 0 ? '+'+parseFloat(this.state.data.CutOff.toString()).toFixed(3):parseFloat(this.state.data.CutOff.toString()).toFixed(3)}
                  editable = {false}
                  keyboardType={"numeric"}
                  underlineColorAndroid='transparent'
                  style={[styles.listTextbox, styles.textboxDisable]}
                />
              </View>
          <View style={{flexDirection: 'column', justifyContent: 'center'}}>
            <Text>{I18n.t("mainpile.5_2.boried_pile.casing_length")}</Text>
              <TextInput
                value={this.state.casing_length == null ? '0.000' : parseFloat(this.state.casing_length.toString()).toFixed(3) >= 0 ? '+'+parseFloat(this.state.casing_length.toString()).toFixed(3):parseFloat(this.state.casing_length.toString()).toFixed(3)}
                editable = {false}
                keyboardType={"numeric"}
                underlineColorAndroid='transparent'
                style={[styles.listTextbox, styles.textboxDisable]}/>
              </View>
            <View style={{flexDirection: 'column', justifyContent: 'center'}}>
              <Text>{I18n.t("mainpile.5_2.boried_pile.pile_tip")}</Text>
                <TextInput
                  value={this.state.data.PileTip == null ? '0.000' : parseFloat(this.state.data.PileTip.toString()).toFixed(3) >= 0 ? '+'+parseFloat(this.state.data.PileTip.toString()).toFixed(3):parseFloat(this.state.data.PileTip.toString()).toFixed(3)}
                  editable = {false}
                  keyboardType={"numeric"}
                  underlineColorAndroid='transparent'
                  style={[styles.listTextbox, styles.textboxDisable]}/>
                </View>
            <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                <Text>{I18n.t("mainpile.5_2.boried_pile.required_depth")}</Text>
                <TextInput
                  value={this.state.data.Depth == null ? '0.000' : parseFloat(this.state.data.Depth.toString()).toFixed(3)}
                  editable = {false}
                  keyboardType={"numeric"}
                  underlineColorAndroid='transparent'
                  style={[styles.listTextbox, styles.textboxDisable]}
                />
              </View>
              </View>
            </View>}

            {(this.props.shape == 2 && this.props.category != 5)&&<View style={{flex: 1, flexDirection: 'row'}}>
            <Image source={require('../../assets/image/dw_edit_1.png')} style={{width: Dimensions.get('window').width-200, height: Dimensions.get('window').height-350, resizeMode: 'contain'}}/>
            <View>
            <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                  <Text>{I18n.t("mainpile.5_2.dwall_pile.ground_elevation")}</Text>
                  <TextInput
                    value={this.state.data.GroundLevel == null ? '0.000' : parseFloat(this.state.data.GroundLevel.toString()).toFixed(3)>=0?'+'+parseFloat(this.state.data.GroundLevel.toString()).toFixed(3):parseFloat(this.state.data.GroundLevel.toString()).toFixed(3)}
                    editable = {false}
                    keyboardType={"numeric"}
                    underlineColorAndroid='transparent'
                    style={[styles.listTextbox, styles.textboxDisable]}
                  />
                </View>
            <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                  <Text>{I18n.t("mainpile.5_2.dwall_pile.top_elevation")}</Text>
                  <TextInput
                    value={this.state.data.TopCasingLevel == null ? '0.000' : parseFloat(this.state.data.TopCasingLevel.toString()).toFixed(3)>=0?'+'+parseFloat(this.state.data.TopCasingLevel.toString()).toFixed(3):parseFloat(this.state.data.TopCasingLevel.toString()).toFixed(3)}
                    editable = {false}
                    keyboardType={"numeric"}
                    underlineColorAndroid='transparent'
                    style={[styles.listTextbox, styles.textboxDisable]}
                  />
                </View>
              <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                <Text>{I18n.t("mainpile.5_2.dwall_pile.bottomguide_elevation")}</Text>
                  <TextInput
                    value={this.state.data.BGWLevel == null ? '0.000' : parseFloat(this.state.data.BGWLevel.toString()).toFixed(3) >=0?'+'+parseFloat(this.state.data.BGWLevel.toString()).toFixed(3):parseFloat(this.state.data.BGWLevel.toString()).toFixed(3)}
                    editable = {false}
                    keyboardType={"numeric"}
                    underlineColorAndroid='transparent'
                    style={[styles.listTextbox, styles.textboxDisable]}
                  />
                </View>
          <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                  <Text>{I18n.t("mainpile.5_2.dwall_pile.cutoff_elevation")}</Text>
                  <TextInput
                    value={this.state.data.CutOff == null ? '0.000' : parseFloat(this.state.data.CutOff.toString()).toFixed(3)>=0?'+'+parseFloat(this.state.data.CutOff.toString()).toFixed(3):parseFloat(this.state.data.CutOff.toString()).toFixed(3)}
                    editable = {false}
                    keyboardType={"numeric"}
                    underlineColorAndroid='transparent'
                    style={[styles.listTextbox, styles.textboxDisable]}
                  />
                </View>
              <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                <Text>{I18n.t("mainpile.5_2.dwall_pile.design_excavation")}</Text>
                    <TextInput
                      value={this.state.data.PileTip == null ? '0.000' : parseFloat(this.state.data.PileTip.toString()).toFixed(3)>=0?'+'+parseFloat(this.state.data.PileTip.toString()).toFixed(3):parseFloat(this.state.data.PileTip.toString()).toFixed(3)}
                      editable = {false}
                      keyboardType={"numeric"}
                      underlineColorAndroid='transparent'
                      style={[styles.listTextbox, styles.textboxDisable]}/>
                </View>
              {/* <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                <Text>{I18n.t("mainpile.5_2.dwall_pile.actual_excavation")}</Text>
                    <TextInput
                      value={this.state.data.RealDepth == null ? '0.000' : parseFloat(this.state.data.RealDepth.toString()).toFixed(3)}
                      editable = {false}
                      keyboardType={"numeric"}
                      underlineColorAndroid='transparent'
                      style={[styles.listTextbox, styles.textboxDisable]}/>
                </View> */}
              <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                <Text>{I18n.t("mainpile.5_2.dwall_pile.design_piletip")}</Text>
                    <TextInput
                      value={this.state.data.Depth == null ? '0.000' : parseFloat(this.state.data.Depth.toString()).toFixed(3)}
                      editable = {false}
                      keyboardType={"numeric"}
                      underlineColorAndroid='transparent'
                      style={[styles.listTextbox, styles.textboxDisable]}/>
                </View>
            {/* <View style={{flexDirection: 'column', justifyContent: 'center', flexWrap: 'wrap'}}>
                <Text>{I18n.t("mainpile.5_2.dwall_pile.autual_piletip")}</Text>
                    <TextInput
                      value={this.state.data.RealDepthLevel == null ? '0.000' : parseFloat(this.state.data.RealDepthLevel.toString()).toFixed(3)}
                      editable = {false}
                      keyboardType={"numeric"}
                      underlineColorAndroid='transparent'
                      style={[styles.listTextbox, styles.textboxDisable]}/>
                </View> */}
                </View>
              </View>
          }
          {this.props.category == 5&&<View style={{flex: 1, flexDirection: 'row'}}>
            <Image source={require('../../assets/image/pile-leg.png')} style={{width: Dimensions.get('window').width-200, height: Dimensions.get('window').height-350, resizeMode: 'contain'}}/>
            <View>
            <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                  <Text>{I18n.t("mainpile.5_2.pile_leg.ground_elevation")}</Text>
                  <TextInput
                    value={this.state.data.GroundLevel == null ? '0.000' : parseFloat(this.state.data.GroundLevel.toString()).toFixed(3)>=0?'+'+parseFloat(this.state.data.GroundLevel.toString()).toFixed(3):parseFloat(this.state.data.GroundLevel.toString()).toFixed(3)}
                    editable = {false}
                    keyboardType={"numeric"}
                    underlineColorAndroid='transparent'
                    style={[styles.listTextbox, styles.textboxDisable]}
                  />
                </View>
            <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                  <Text>{I18n.t("mainpile.5_2.pile_leg.top_elevation")}</Text>
                  <TextInput
                    value={this.state.data.TopCasingLevel == null ? '0.000' : parseFloat(this.state.data.TopCasingLevel.toString()).toFixed(3)>=0?'+'+parseFloat(this.state.data.TopCasingLevel.toString()).toFixed(3):parseFloat(this.state.data.TopCasingLevel.toString()).toFixed(3)}
                    editable = {false}
                    keyboardType={"numeric"}
                    underlineColorAndroid='transparent'
                    style={[styles.listTextbox, styles.textboxDisable]}
                  />
                </View>
              <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                <Text>{I18n.t("mainpile.5_2.pile_leg.bottomguide_elevation")}</Text>
                  <TextInput
                    value={this.state.data.BGWLevel == null ? '0.000' : parseFloat(this.state.data.BGWLevel.toString()).toFixed(3) >=0?'+'+parseFloat(this.state.data.BGWLevel.toString()).toFixed(3):parseFloat(this.state.data.BGWLevel.toString()).toFixed(3)}
                    editable = {false}
                    keyboardType={"numeric"}
                    underlineColorAndroid='transparent'
                    style={[styles.listTextbox, styles.textboxDisable]}
                  />
                </View>
          <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                  <Text>{I18n.t("mainpile.5_2.pile_leg.cutoff_elevation")}</Text>
                  <TextInput
                    value={this.state.data.CutOff == null ? '0.000' : parseFloat(this.state.data.CutOff.toString()).toFixed(3)>=0?'+'+parseFloat(this.state.data.CutOff.toString()).toFixed(3):parseFloat(this.state.data.CutOff.toString()).toFixed(3)}
                    editable = {false}
                    keyboardType={"numeric"}
                    underlineColorAndroid='transparent'
                    style={[styles.listTextbox, styles.textboxDisable]}
                  />
                </View>
              <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                <Text>{I18n.t("mainpile.5_2.pile_leg.design_excavation")}</Text>
                    <TextInput
                      value={this.state.data.PileTip == null ? '0.000' : parseFloat(this.state.data.PileTip.toString()).toFixed(3)>=0?'+'+parseFloat(this.state.data.PileTip.toString()).toFixed(3):parseFloat(this.state.data.PileTip.toString()).toFixed(3)}
                      editable = {false}
                      keyboardType={"numeric"}
                      underlineColorAndroid='transparent'
                      style={[styles.listTextbox, styles.textboxDisable]}/>
                </View>
              <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                <Text>{I18n.t("mainpile.5_2.pile_leg.design_piletip")}</Text>
                    <TextInput
                      value={this.state.data.Depth == null ? '0.000' : parseFloat(this.state.data.Depth.toString()).toFixed(3)}
                      editable = {false}
                      keyboardType={"numeric"}
                      underlineColorAndroid='transparent'
                      style={[styles.listTextbox, styles.textboxDisable]}/>
                </View>
                <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                <Text>{I18n.t("mainpile.5_2.pile_leg.design_excavation_pl")}</Text>
                    <TextInput
                      value={this.state.data.PileTipChild == null ? '0.000' : parseFloat(this.state.data.PileTipChild.toString()).toFixed(3)>=0?'+'+parseFloat(this.state.data.PileTipChild.toString()).toFixed(3):parseFloat(this.state.data.PileTipChild.toString()).toFixed(3)}
                      editable = {false}
                      keyboardType={"numeric"}
                      underlineColorAndroid='transparent'
                      style={[styles.listTextbox, styles.textboxDisable]}/>
                </View>
              <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                <Text>{I18n.t("mainpile.5_2.pile_leg.design_piletip_pl")}</Text>
                    <TextInput
                      value={this.state.data.DepthChild == null ? '0.000' : parseFloat(this.state.data.DepthChild.toString()).toFixed(3)}
                      editable = {false}
                      keyboardType={"numeric"}
                      underlineColorAndroid='transparent'
                      style={[styles.listTextbox, styles.textboxDisable]}/>
                </View>
            
                </View>
              </View>}
        </View>
      </View>
      </View>
    )
  }
}
const mapStateToProps = state => {
  return {
    item: state.pile.item,
    pileAll: state.mainpile.pileAll || null}
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(Pile5_2)
