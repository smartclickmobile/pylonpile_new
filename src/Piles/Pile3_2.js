import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  Dimensions,
  TouchableOpacity,
  ScrollView
} from "react-native"
import { Container, Content } from "native-base"
import { Icon } from "react-native-elements"
import Carousel from "react-native-snap-carousel"
import ModalSelector from "react-native-modal-selector"
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM,CATEGORY_1 } from "../Constants/Color"
import { GroupEmployee } from "../Controller/API"
import I18n from "../../assets/languages/i18n"
import { Extra } from "../Controller/API"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"
import RNTooltips from "react-native-tooltips"
const sliderWidth = Dimensions.get("window").width
const itemWidth = 320

class Pile3_2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      northPosition: '',
      eastPosition: '',
      northCal:'',
      eastCal:'',
      northCalBy:'',
      eastCalBy:'',
      northCalold:'',
      eastCalold:'',
      northCalByold:'',
      eastCalByold:'',
      constant:'',
      Image_ReadingCoordinate:[],
      Image_SurveyDiff:[],
      process:3,
      step:2,
      northAlert: false,
      southAlert: false,
      northAlertold: false,
      southAlertold: false,
      stack:[],
      Edit_Flag:1,
      Step3Status:0,
      visible: false,
      visible2: false,
      ApprovedDate:'',
      Step5Status:0,
      haveedit:false,

      Image_ReadingCoordinate_old:null,
      Image_SurveyDiff_old:null,
      checksaveimageold:false
    }
  }

  componentWillMount() {

  }

  async componentWillReceiveProps(nextProps) {
    this.setState({visible: false,visible2: false})
    var pile = null
    var pile3_0 = null
    var pileMaster = null
    var pile3_4 = null
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['3_2']) {
      pile = nextProps.pileAll[this.props.pileid]['3_2'].data
      pileMaster = nextProps.pileAll[this.props.pileid]['3_2'].masterInfo
      
      this.setState({Step3Status:nextProps.pileAll[this.props.pileid].step_status.Step3Status,Step5Status:nextProps.pileAll[this.props.pileid].step_status.Step5Status})
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['3_0']) {
      pile3_0 = nextProps.pileAll[this.props.pileid]['3_0'].data
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["3_4"]){
      pile3_4 = nextProps.pileAll[this.props.pileid]["3_4"].data
    }
    if(pileMaster.CasingPosition != null){
      var northing = pileMaster.CasingPosition.northing
      var easting = pileMaster.CasingPosition.easting
      await this.setState({
        northPosition: northing.toString(),
        eastPosition: easting.toString(),
      })
    }
    if(pileMaster.Constants != null){
      await this.setState({constant: pileMaster.Constants.Constant})
    }
    if (pile) {
      
      if(pile.constant != null && pile.constant != undefined && pile.constant != this.state.constant){
        await this.setState({constant: pile.constant})
      }
      if(pile.eastPosition != null && pile.eastPosition != undefined &&  pile.eastPosition != this.state.eastPosition){
        await this.setState({eastPosition: pile.eastPosition})
      }
      if(pile.eastCal != null && pile.eastCal != undefined && pile.eastCal != this.state.eastCal ){
        await this.setState({eastCal: pile.eastCal})
      }else if (pile.eastCal == "") {
        this.setState({eastCal:"",eastCalBy:""})
      }
      if(pile.eastCalBy != null && pile.eastCalBy != undefined && pile.eastCalBy !== this.state.eastCalBy){
        await this.setState({eastCalBy:pile.eastCalBy.toString()},()=> this.onCal(this.state.eastCalBy,'east'))
      }
      if(pile.northPosition != null && pile.northPosition != undefined && pile.northPosition !== this.state.northPosition){
        await this.setState({northPosition: pile.northPosition})
      }
      if(pile.northCal != null && pile.northCal != undefined && pile.northCal != ''){
        await this.setState({northCal: pile.northCal})
      }else if (pile.northCal == "") {
        this.setState({northCal:"",northCalBy:""})
      }
      if(pile.northCalBy != null && pile.northCalBy != undefined && pile.northCalBy !== this.state.northCalBy){
        // console.warn('northCalBy',pile.northCalBy.toString())
        await this.setState({northCalBy: pile.northCalBy.toString()},()=> this.onCal(this.state.northCalBy,'north'))
      }

      

      //save image old and n e old
      if(this.state.checksaveimageold==false){
        if(pile.Image_ReadingCoordinate != null && pile.Image_ReadingCoordinate != undefined && pile.Image_ReadingCoordinate != ''){
          await this.setState({Image_ReadingCoordinate_old:pile.Image_ReadingCoordinate})
        }
        if(pile.Image_SurveyDiff != null && pile.Image_SurveyDiff != undefined && pile.Image_SurveyDiff != '',this.state.checksaveimageold==false){
          await this.setState({Image_SurveyDiff_old:pile.Image_SurveyDiff})
        }

        if(pile.eastCal != null && pile.eastCal != undefined ){
          await this.setState({eastCalold:pile.eastCal})
        }
        if(pile.eastCalBy != null && pile.eastCalBy != undefined ){
          await this.setState({eastCalByold: pile.eastCalBy.toString()})
        }
        if(pile.northCal != null && pile.northCal != undefined && pile.northCal != ''){
          await this.setState({northCalold:pile.northCal})
        }
        if(pile.northCalBy != null && pile.northCalBy != undefined ){
          await this.setState({northCalByold: pile.northCalBy.toString()})
        }

        this.setState({checksaveimageold:true})
      }

      if(pile.Image_ReadingCoordinate != null && pile.Image_ReadingCoordinate != undefined && pile.Image_ReadingCoordinate != ''){
        await this.setState({Image_ReadingCoordinate: pile.Image_ReadingCoordinate})
      }else if (pile.Image_ReadingCoordinate == null) {
        this.setState({Image_ReadingCoordinate:[]})
      }
      if(pile.Image_SurveyDiff != null && pile.Image_SurveyDiff != undefined && pile.Image_SurveyDiff != ''){
        await this.setState({Image_SurveyDiff: pile.Image_SurveyDiff})
      }else if (pile.Image_SurveyDiff == null) {
        this.setState({Image_SurveyDiff:[],southAlert:false,northAlert:false})
      }

      
      


    }
    // console.log(pile.eastCalBy == null ? 1 : 0);/
    // if (nextProps.hidden == false) {
    //   // console.warn('hidden',this.state.northCalBy)
    //   if(this.state.eastCalBy == '' && pile.eastCalBy != null){
    //       await this.onCal( pile.eastCalBy.toString(),'east')
    //   }
    //   if(this.state.northCalBy == '' &&  pile.northCalBy != null) {
    //     // console.warn('northCalBy',pile.northCalBy)
    //      await this.onCal( pile.northCalBy.toString(),'north')
    //   }
    // }
    if(pile3_4){
      if(pile3_4.approve != null && pile3_4.approve != undefined && pile3_4.approve != ''){
        this.setState({ApprovedDate:pile3_4.approve})
      }else{
        this.setState({ApprovedDate:''})
      }
    }
    if (pile3_0) {
      // if (pile3_0.Edit_Flag != null && pile3_0.Edit_Flag != undefined && pile3_0.Edit_Flag != '') {
        this.setState({Edit_Flag:pile3_0.Edit_Flag})
      // }
    }
  }

  getState(){
    var data = {
      eastPosition: this.state.eastPosition,
      eastCal: this.state.eastCal,
      eastCalBy: this.state.eastCalBy,
      northPosition: this.state.northPosition,
      northCal: this.state.northCal,
      northCalBy: this.state.northCalBy,
      Image_ReadingCoordinate:this.state.Image_ReadingCoordinate,
      constant:this.state.constant,
    }
    return data
  }

  async updateState(value){
    if (value.data.Image_ReadingCoordinate != undefined) {
      await this.setState({Image_ReadingCoordinate:value.data.Image_ReadingCoordinate})
      this.saveLocal('3_2')
    }
    if (value.data.Image_SurveyDiff != undefined) {
      await this.setState({Image_SurveyDiff:value.data.Image_SurveyDiff})
      this.saveLocal('3_2')
    }
  }

  onCameraRoll(keyname,photos,type){
    if (this.state.Edit_Flag == 1 && this.props.waitApprove == false ) {
      Actions.camearaRoll({
        jobid:this.props.jobid,
        process:this.state.process,
        step:this.state.step,
        pileId:this.props.pileid,
        keyname:keyname,
        photos:photos,
        typeViewPhoto:type,
        shape:this.props.shape,
        category:this.props.category,
        // Edit_Flag:this.state.ApprovedDate==''?1:0,
        Edit_Flag:this.state.Edit_Flag,
        status:this.state.Step3Status
      })
        
    }else {
      Actions.camearaRoll({
        jobid:this.props.jobid,
        process:this.state.process,
        step:this.state.step,
        pileId:this.props.pileid,
        keyname:keyname,
        photos:photos,
        typeViewPhoto:'view',
        shape:this.props.shape,
        category:this.props.category,
        // Edit_Flag:this.state.ApprovedDate==''?1:0
        Edit_Flag:0,
        status:this.state.Step3Status
      })
    }
  }

  async onCal(text, type) {
   
    if (this.state.northPosition && this.state.eastPosition ) {
      if (type == "north") {
        let cal = parseFloat(text) - parseFloat(this.state.northPosition)
        // console.warn('onCal',cal,text,this.state.northPosition,this.state.constant)
         if (Math.abs(cal.toFixed(4)) > this.state.constant) {
           await this.setState({ northAlert: true ,visible: false,visible2: false})
         }
         else {
          await this.setState({ northAlert: false ,visible: false,visible2: false})
         }
         await this.setState({ northCal: (parseFloat(cal).toFixed(4)).toString(),northCalBy:this.onChangeFormatDecimal(text.toString()),visible: false,visible2: false})
      } else {
        let cal = parseFloat(text) - parseFloat(this.state.eastPosition)
        if (Math.abs(cal.toFixed(4)) > this.state.constant) {
          await this.setState({ southAlert: true ,visible: false,visible2: false })
        }
        else {
          await this.setState({ southAlert: false ,visible: false,visible2: false })
        }
        await this.setState({ eastCal: (parseFloat(cal).toFixed(4)).toString(),eastCalBy:this.onChangeFormatDecimal(text.toString()),visible: false,visible2: false})
      }
      // await this.saveLocal('3_2')
    } else if (!this.props.hidden) {
      // console.warn('hidden')
      if (type == "north") {
         await this.setState({ northAlert: false,northCal: "", northCalBy:'' ,visible: false,visible2: false},()=>{
           this.saveLocal('3_2')
         })
        return
      } else {
         await this.setState({ southAlert: false, eastCal: "", eastCalBy:'',visible: false,visible2: false},()=>{
           this.saveLocal('3_2')
         })
        return
      }
    }
  }
  onChangeFormatDecimal(data){
    if (data.split(".")[1]) {
      if (data.split(".")[1].length > 3) {
        return parseFloat(data).toFixed(4)
      }else {
        return data
      }
    }else {
      return data
    }
  }

  async saveLocal(process_step){
    var flagaccept= 0
   if(this.state.Step3Status == 2 && this.state.Step5Status != 0 ){
    if(parseFloat(this.state.northCal) > parseFloat(this.state.constant) || parseFloat(this.state.eastCal) > parseFloat(this.state.constant)){
      if((parseFloat(this.state.northCalold) > parseFloat(this.state.constant) || parseFloat(this.state.eastCalold) > parseFloat(this.state.constant)) && this.state.Step5Status != 0){
        flagaccept=3
      }else{
        flagaccept=1
      }
      
    }else{
      flagaccept=2
    }
   }
    var value = {
      process:3,
      step:2,
      pile_id: this.props.pileid,
      data:{
        eastPosition: this.state.eastPosition,
        eastCal: this.state.eastCal,
        eastCalBy: this.state.eastCalBy,
        northPosition: this.state.northPosition,
        northCal: this.state.northCal,
        northCalBy: this.state.northCalBy,
        Image_ReadingCoordinate:this.state.Image_ReadingCoordinate,
        Image_SurveyDiff:this.state.Image_SurveyDiff,
        constant:this.state.constant,
        flagaccept:flagaccept,
        flagApprove:false
      },
      haveedit:this.state.haveedit
    }
    await this.props.mainPileSetStorage(this.props.pileid,process_step,value)
  }

  _renderHeader() {
    return (
      <View>
        <View style={styles.header}>
          <Icon name="exclamation-triangle" type="font-awesome" color="#FFF" />
          <Text style={{ marginLeft: 10 }}>{this.props.shape == 1 ? I18n.t("mainpile.3_2.warning_bp", { number: this.state.constant }) : I18n.t("mainpile.3_2.warning_dw", { number: this.state.constant })}</Text>
        </View>
      </View>
    )
  }
  alert1(){
    if(this.state.Step3Status == 2 ){
      if(this.state.northCalBy!=this.state.northCalByold || this.state.eastCalBy!=this.state.eastCalByold){
        Alert.alert('', I18n.t('mainpile.3_2.alert1'), [
          {
            text: 'OK'
          }
        ])
      }
      
    }
  }
  alert2(){
    // if(this.state.Step3Status == 2 ){
      if(this.state.northCalBy!=this.state.northCalByold || this.state.eastCalBy!=this.state.eastCalByold){
        Alert.alert('', I18n.t('mainpile.3_2.alert1'), [
          {
            text: 'OK'
          }
        ])
      }
      
    // }
  }

  onendeditN(){
    // console.warn('onendeditN',this.state.northCalBy,this.state.northCalByold,this.state.ApprovedDate)
    if(parseFloat(this.state.northCalBy).toFixed(4)!= parseFloat(this.state.northCalByold).toFixed(4)){
    if ((parseFloat(this.state.northCalold) > parseFloat(this.state.constant) || parseFloat(this.state.eastCalold) > parseFloat(this.state.constant)) && this.state.ApprovedDate != ''){
      Alert.alert('',I18n.t('mainpile.3_2.error'),[
        {
          text: 'Cancle',
          onPress: async () => {
            // console.warn(this.state.northCalByold)
            this.setState({haveedit:false})
            await this.onCal(this.state.northCalByold, "north")
            this.saveLocal('3_2')
          }
          // onPress: () => this.saveLocal('3_2')
        },
        {
          text: 'OK',
          onPress: () =>{
            this.alert1()
            var temp = this.state.northCalBy
            var temp1 = parseFloat(temp).toFixed(4)
            this.setState({northCalBy:this.state.northCalBy ? isNaN(this.state.northCalBy) ?'':  temp1: '',haveedit:true},()=>{
              this.saveLocal('3_2')
            })
          }
        }
    ],
    {cancelable:false}
  )
    }else{
      // console.warn('image',JSON.stringify(this.state.Image_ReadingCoordinate_old)=='null', this.state.Image_ReadingCoordinate)
      var Image_ReadingCoordinate_old_string = JSON.stringify(this.state.Image_ReadingCoordinate_old)
      var Image_SurveyDiff_old_string = JSON.stringify(this.state.Image_SurveyDiff_old)

      var Image_ReadingCoordinate_string = JSON.stringify(this.state.Image_ReadingCoordinate)
      var Image_SurveyDiff_string = JSON.stringify(this.state.Image_SurveyDiff)

      if(Image_ReadingCoordinate_old_string == 'null' || Image_SurveyDiff_old_string == 'null'){

      }else{
        if(Image_ReadingCoordinate_string == Image_ReadingCoordinate_old_string || Image_SurveyDiff_old_string == Image_SurveyDiff_string){
          this.alert2()
        }
      }
      // this.alert1()
      var temp = this.state.northCalBy
      var temp1 = parseFloat(temp).toFixed(4)
      this.setState({northCalBy:this.state.northCalBy ? isNaN(this.state.northCalBy) ?'': temp1 : '',haveedit:true},()=>{
      this.saveLocal('3_2')
    })
    }
    
  }else{
    this.setState({haveedit:false},()=>{this.saveLocal('3_2')})
  }
}
  onendeditE(){
    // console.warn('ApprovedDate',this.state.ApprovedDate)
    if(parseFloat(this.state.eastCalBy).toFixed(4)!=parseFloat(this.state.eastCalByold).toFixed(4)){
    if ( (parseFloat(this.state.northCalold) > parseFloat(this.state.constant) || parseFloat(this.state.eastCalold) > parseFloat(this.state.constant)) && this.state.ApprovedDate != ''){
      Alert.alert('',I18n.t('mainpile.3_2.error'),[
        {
          text: 'Cancle',
          onPress: async () => {
            // console.warn(this.state.eastCalByold)
            this.setState({haveedit:false})
            await this.onCal(this.state.eastCalByold, "east")
            this.saveLocal('3_2')
          }
          // onPress: () => this.saveLocal('3_2')
        },
        {
          text: 'OK',
          onPress: () =>{
            this.alert1()
            var temp = this.state.eastCalBy
            var temp1 = parseFloat(temp).toFixed(4)
            this.setState({eastCalBy:this.state.eastCalBy ? isNaN(this.state.eastCalBy) ?'': temp1 : '',haveedit:true},()=>{
              this.saveLocal('3_2')
            })
          }
        }
    ],
    {cancelable:false}
  )
    }else{
      var Image_ReadingCoordinate_old_string = JSON.stringify(this.state.Image_ReadingCoordinate_old)
      var Image_SurveyDiff_old_string = JSON.stringify(this.state.Image_SurveyDiff_old)

      var Image_ReadingCoordinate_string = JSON.stringify(this.state.Image_ReadingCoordinate)
      var Image_SurveyDiff_string = JSON.stringify(this.state.Image_SurveyDiff)

      if(Image_ReadingCoordinate_old_string == 'null' || Image_SurveyDiff_old_string == 'null'){

      }else{
        if(Image_ReadingCoordinate_string == Image_ReadingCoordinate_old_string || Image_SurveyDiff_old_string == Image_SurveyDiff_string){
          this.alert2()
        }
      }
      // this.alert1()
      var temp = this.state.eastCalBy
      var temp1 = parseFloat(temp).toFixed(4)
      this.setState({eastCalBy:this.state.eastCalBy ? isNaN(this.state.eastCalBy) ?'': temp1 : '',haveedit:true},()=>{
      this.saveLocal('3_2')
    })
    }
    
  }else{
    this.setState({haveedit:false},()=>{this.saveLocal('3_2')})
  }
}
  _renderNorth() {
    return (
      <View style={styles.boxTopic}>
        <RNTooltips
          text={I18n.t('tooltip.3_21')}
          textSize={16}
          visible={this.state.visible}
          reference={this.main}
          tintColor="#007CC2"
          // onHide={this.setState({visible: false})}
          autoHide={true}
        />
        <View style={[styles.topic,{flexDirection:'row'}]}>
          <Text>{I18n.t('notificationdetail.coordinate')} N  </Text>
          <Icon
              name="ios-information-circle"
              type="ionicon"
              color="#517fa4"
              onPress={() => {
                this.setState({visible: true,visible2:false})
              }}
            />
        </View>
        <View style={styles.boxValue}>
          <View style={styles.valueLeft}>
            <Text>{I18n.t("mainpile.3_2.plan")}</Text>
          </View>
          <View style={[styles.valueRight, { backgroundColor: CATEGORY_1 }]}>
            <Text style={[styles.valueRightText,{color:'#fff',textAlign:'center'}]}>{isNaN(this.state.northPosition) ? '' : this.state.northPosition }</Text>
          </View>
        </View>
        <View style={styles.boxValue}>
          <View style={styles.valueLeft}>
            <Text>{I18n.t("mainpile.3_2.read")}</Text>
          </View>
          <View style={styles.valueRight}>
            <TextInput
              value={this.state.northCalBy}
              keyboardType="numeric"
              onChangeText={(text) => {
                var temp = parseFloat(text).toFixed(4)
               
                this.onCal(text, "north")
                
              }}
              // onFocus={()=> this.alert1()}
              style={[styles.valueRightText,{textAlign:'center',color:'#fff'}]}
              // editable={this.state.Edit_Flag == 1 && this.state.ApprovedDate == ''?  this.state.northPosition ? true : false : false}
              editable={this.state.Edit_Flag == 1 && this.props.waitApprove == false?  this.state.northPosition ? true : false : false}
              onEndEditing={()=> this.onendeditN()}
            />
          </View>
        </View>
        <View style={styles.boxValue}>
          <View style={styles.valueLeft}>
            <Text>{I18n.t("mainpile.3_2.deviation")}</Text>
          </View>
          <View ref={ref => {this.main = ref}}
          style={[styles.valueRight, this.state.northAlert ? { backgroundColor: '#ff7a73'} : { backgroundColor: MENU_GREY_ITEM }]}>
            <Text style={[styles.valueRightText,{textAlign:'center'}]}>{isNaN(this.state.northCal) ? '' : this.state.northCal }</Text>
          </View>
        </View>
      </View>
    )
  }

  _renderSouth() {
    return (
      <View style={styles.boxTopic}>
      <RNTooltips
          text={I18n.t('tooltip.3_21')}
          textSize={16}
          visible={this.state.visible2}
          reference={this.main2}
          tintColor="#007CC2"
        />
        <View style={[styles.topic,{flexDirection:'row'}]}>
          <Text>{I18n.t('notificationdetail.coordinate')} E  </Text>
          <Icon
              name="ios-information-circle"
              type="ionicon"
              color="#517fa4"
              onPress={() => {this.setState({visible2: true,visible:false})}}/>
        </View>
        <View style={styles.boxValue}>
          <View style={styles.valueLeft}>
            <Text>{I18n.t("mainpile.3_2.plan")}</Text>
          </View>
          <View style={[styles.valueRight, { backgroundColor: CATEGORY_1 }]}>
            <Text style={[styles.valueRightText,{textAlign:'center',color:'#fff'}]}>{isNaN(this.state.eastPosition) ? '' : this.state.eastPosition }</Text>
          </View>
        </View>
        <View style={styles.boxValue}>
          <View style={styles.valueLeft}>
            <Text>{I18n.t("mainpile.3_2.read")}</Text>
          </View>
          <View style={styles.valueRight}>
            <TextInput
              value={this.state.eastCalBy}
              keyboardType={"numeric"}
              onChangeText={(text) => {
                this.onCal(text, "east")
                this.setState({eastCalBy:isNaN(parseFloat(text))?'':parseFloat(text).toFixed(4)})
              }}
              // onFocus={()=> this.alert1()}
              style={[styles.valueRightText,{textAlign:'center',color:'#fff'}]}
              // editable={this.state.Edit_Flag == 1 && this.state.ApprovedDate == '' ? this.state.eastPosition ? true : false : false}
              editable={this.state.Edit_Flag == 1 && this.props.waitApprove == false? this.state.eastPosition ? true : false : false}
              onEndEditing={()=> this.onendeditE()}
            />
          </View>
        </View>
        <View style={styles.boxValue}>
          <View style={styles.valueLeft}>
            <Text>{I18n.t("mainpile.3_2.deviation")}</Text>
          </View>
          <View ref={ref => {
            this.main2 = ref
          }}
          style={[styles.valueRight, this.state.southAlert ? { backgroundColor : '#ff7a73' } : { backgroundColor: MENU_GREY_ITEM }]}>
            <Text style={[styles.valueRightText,{textAlign:'center'}]}>{isNaN(this.state.eastCal) ? '' : this.state.eastCal }</Text>
          </View>
        </View>
      </View>
    )
  }

  _renderShowImage() {
    
    return (
      <View>
        <View style={{ marginTop: 10,marginLeft: 10 }}>
          <Text>{I18n.t("mainpile.3_2.read")}</Text>
        </View>
        <View style={{ alignItems: "center", marginTop: 10 }}>
          <TouchableOpacity style={[styles.image,this.state.Image_ReadingCoordinate.length>0?{backgroundColor:'#6dcc64'}:{}]} onPress={() => this.onCameraRoll('Image_ReadingCoordinate',this.state.Image_ReadingCoordinate,'edit')}>
            <Icon name="image" type="entypo" color="#FFF" />
            <Text style={styles.imageText}>{I18n.t("mainpile.3_2.view")}</Text>
          </TouchableOpacity>
        </View>
        <View style={{ marginTop: 10,marginLeft: 10 }}>
          <Text>{I18n.t("mainpile.3_2.deviation")}</Text>
        </View>
        <View style={{ alignItems: "center", marginTop: 10 ,marginBottom:15}}>
          <TouchableOpacity style={[styles.image,this.state.Image_SurveyDiff.length>0?{backgroundColor:'#6dcc64'}:{}]} onPress={() => this.onCameraRoll('Image_SurveyDiff',this.state.Image_SurveyDiff,'edit')}>
            <Icon name="image" type="entypo" color="#FFF" />
            <Text style={styles.imageText}>{I18n.t("mainpile.3_2.view")}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    return (
      <View>
            {this._renderHeader()}
            {this._renderNorth()}
            {this._renderSouth()}
            {this._renderShowImage()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  boxTopic: {
    borderBottomWidth: 0.5,
    paddingBottom: 10
  },
  topic: {
    marginTop: 10,
    marginLeft: 10
  },
  boxValue: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    flexDirection: "row",
    height: 40,
    justifyContent: "center"
    // alignItems: 'center'
  },
  valueLeft: {
    width: "40%",
    height: 40,
    justifyContent: "center"
  },
  valueRight: {
    width: "60%",
    height: 40,
    justifyContent: "center",
    backgroundColor: SUB_COLOR
  },
  valueRightText: {
    marginLeft: 5,
    textAlign: 'right'
  },
  image: {
    backgroundColor: SUB_COLOR,
    height: 40,
    width: "50%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10
  },
  imageText: {
    marginLeft: 10,
    color: "white"
  },
  header: {
    margin: 10,
    backgroundColor: "#ff7a73",
    height: 40,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  }
})

const mapStateToProps = state => {
  // console.log(state);
  return {
    // casing: state.pile.casing_items,
    // constant: state.pile.constant_items,
    pileAll: state.mainpile.pileAll || null,
  }
}

export default connect(mapStateToProps, actions, null, { withRef: true })(Pile3_2)
