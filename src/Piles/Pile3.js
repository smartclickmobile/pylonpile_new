import React, { Component } from "react"
import { View, Text, StyleSheet, TouchableOpacity,Alert,ActivityIndicator } from "react-native"
import StepIndicator from "react-native-step-indicator"
import { Container, Content } from "native-base"
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux'
import * as actions from '../Actions'
import I18n from '../../assets/languages/i18n'
import {MainPileFunc} from '../Components/function'
import { MAIN_COLOR,DEFAULT_COLOR_1, DEFAULT_COLOR_4 } from "../Constants/Color"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import Pile3_1 from "./Pile3_1"
import Pile3_2 from "./Pile3_2"
import Pile3_3 from "./Pile3_3"
import Pile3_4 from "./Pile3_4"
import Pile3_5 from "./Pile3_5"
import styles from './styles/Pile3.style'
import moment from "moment"


class Pile3 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 3,
      step: 0,
      ref:4,
      error:null,
      isapprove:false,
      data:[],
      location:{
        position:{
          lat:1,
          log:1,
        }
      },
      Edit_Flag:1,
      startdate: '',
      status5:0,
      status3:0,
      status11:0,
      status1:0,
      status2:0,
      approveagian:false,
      waitApprove:false,
      checksavebutton:false,
      saveload:false,
      statuscolor:null,
      process3_success_1_random:null,
      process3_success_2_random:null,
      process3_success_3_random:null,
      process3_success_4_random:null,
      random:null,
      stepstatus:''
    }
  }
  componentDidMount(){
    if (this.props.stack) {
      if(this.props.shape==1){
        this.setState({step:this.props.stack[this.props.stack.length - 1].step - 1})
      }else{
        this.setState({step:4})
      }
      
    }
    

  }
   async componentWillReceiveProps(nextProps){
    //  console.warn('check check',nextProps.step_status2_random , this.state.stepstatus , nextProps.processstatus )
    if(nextProps.step_status2_random != this.state.stepstatus && nextProps.processstatus == 3){
      this.setState({stepstatus:nextProps.step_status2_random},async()=>{
        // console.warn('yes3',this.props.category)
        
        await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step3Status: nextProps.step_status2.Step3Status })
        this.props.mainPileGetStorage(this.props.pileid, "step_status")
        if(this.state.step == 3){
          if(this.props.category == 4){
            if(nextProps.step_status2.Step3Status != 2){
              Alert.alert("",I18n.t('mainpile.3_4.errorsave'))
            }else{
              if(this.state.status2 == 2){
                this.onSetStack(5,0)
                this.props.clearStatus()
                this.props.pileClear()
                this.props.ungetStepStatus2()
                this.props.mainRefresh(this.state.Edit_Flag)
                this.props.onNextStep()
              }else{
                Alert.alert("",I18n.t('mainpile.lockprocess.process5_d'))
              }
            }
          }else{
            this.onSetStack(4,0)
            this.props.clearStatus()
            this.props.pileClear()
            this.props.ungetStepStatus2()
            this.props.mainRefresh(this.state.Edit_Flag)
            this.props.onNextStep()
          }

        }else if(this.state.step == 4){
          this.onSetStack(4,0)
          this.props.clearStatus()
          this.props.pileClear()
          this.props.ungetStepStatus2()
          this.props.mainRefresh(this.state.Edit_Flag)
          this.props.onNextStep()
        }
      })
    }

    if (nextProps.pileAll[nextProps.pileid] != undefined) {
      var edit = nextProps.pileAll[nextProps.pileid]
      if (edit['3_0'] != undefined) {
          await this.setState({Edit_Flag:edit['3_0'].data.Edit_Flag})
      }

      if(edit != undefined){
        this.setState({
          status5:edit.step_status.Step5Status,
          status3:edit.step_status.Step3Status,
          status11:edit.step_status.Step11Status,
          status1:edit.step_status.Step1Status,
          status2:edit.step_status.Step2Status,
        })
        
      }
      if(this.props.fromnoti){
        // console.warn('status test',edit.step_status.Step3Status)
      }
      await this.setState({error:nextProps.error,data:nextProps.pileAll[nextProps.pileid],location:nextProps.pileAll.currentLocation},()=>{
        // console.warn('waitApprove',this.state.data['3_4'].data.ApprovedDate,this.state.data['3_4'].data.ApprovedUserId)
        if((this.state.data['3_4'].data.ApprovedDate == '' || this.state.data['3_4'].data.ApprovedDate == null) && 
        this.state.data['3_4'].data.ApprovedUserId != null && 
        this.state.data['3_4'].data.ApprovedUserId != ''){
          this.setState({waitApprove:true})
        }else{
          this.setState({waitApprove:false})
        }
      })
    }
    if (nextProps.pileAll[nextProps.pileid] != undefined && this.state.step == 3) {
      if (nextProps.pileAll[nextProps.pileid]['3_4'].data.approve == null) {
         await this.setState({isapprove:false})
      }else {
        console.log(nextProps.pileAll[this.props.pileid]['3_4'].data.approve)
         await this.setState({isapprove:nextProps.pileAll[this.props.pileid]['3_4'].data.approve})
      }
    }else {
        await this.setState({isapprove:false})
    }

    if(nextProps.process3_success_1 == true && nextProps.process3_success_1_random != this.state.process3_success_1_random && this.props.fromnoti != true){
      this.setState({process3_success_1_random:nextProps.process3_success_1_random},async()=>{
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process:3
        })
        this.onSetStack(3,1)
        if(this.props.startdate != '' && this.props.startdate != null){
          this.setState({ step: 1 ,startdate: this.props.startdate,saveload:false})
        }else{
          this.setState({ step: 1 ,startdate: moment().format("DD/MM/YYYY HH:mm"),saveload:false})
        }
      })
    }
    if(nextProps.process3_success_2 == true && nextProps.process3_success_2_random != this.state.process3_success_2_random && this.props.fromnoti != true){
      this.setState({process3_success_2_random:nextProps.process3_success_2_random},async()=>{
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process:3
        })
        this.onSetStack(3,2)
        this.setState({step: 2,saveload:false})
      })
    }

    if(nextProps.process3_success_3 ==  true && nextProps.process3_success_3_random != this.state.process3_success_3_random && this.props.fromnoti != true){
      this.setState({process3_success_3_random:nextProps.process3_success_3_random},()=>{
        this.setState({step: 3,saveload:false})
        this.onSetStack(3,3)
      })
    }

    if(nextProps.process3_success_4 == true && nextProps.process3_success_4_random != this.state.process3_success_4_random && this.props.fromnoti != true){
      this.setState({process3_success_4_random:nextProps.process3_success_4_random},async()=>{
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process:3
        })
        
        this.setState({checksavebutton:false,saveload:false,statuscolor:null})
      })
    }
   
     if(nextProps.error != null){
      
      this.setState({random:Math.floor((Math.random() * 1000) + 1)},()=>{
        // console.warn('testetst',this.state.saveload)
        
      })
     }
  }
  
  updateState(value){
    if (value.step != null) {
      const ref = 'pile'+ value.process +'_'+ value.step
      if (this.refs[ref].getWrappedInstance().updateState(value) != undefined) {
        this.refs[ref].getWrappedInstance().updateState(value)
      }
    }else{
      console.log(value)
    }
  }

  setStep(step){
    if(this.props.shape == 1){
      return this.setState({step:step})
    }else{
      return this.setState({step:4})
    }
  }

  onGetChildData(step){
    if (this.refs['pile3_'+step].getWrappedInstance().getState() != undefined) {
      const dataChild = this.refs['pile3_'+step].getWrappedInstance().getState()
      return dataChild
    }
  }

  async onSave(step){
    await this.props.mainPileGetAllStorage()
    if (step == 0) {
      var value = {
        process:3,
        step:1,
        pile_id: this.props.pileid,
      }
      // await this.props.mainPileSetStorage(this.props.pileid,'3_1',value)
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value)
          .then(async (data) =>
          {
            var valueApi ={
              Language:I18n.locale,
              JobId: this.props.jobid,
              PileId: this.props.pileid,
              Page:1,
              SurveyorId: this.state.data['3_1'].data.SurveyorId,
              SurveyorName: this.state.data['3_1'].data.SurveyorName,
              LocationId: this.state.data['3_1'].data.LocationId,
              LocationName: this.state.data['3_1'].data.LocationName,
              BSFlagLocationId: this.state.data['3_1'].data.BSFlagLocationId,
              BSFlagLocationName: this.state.data['3_1'].data.BSFlagLocationName,
              IsResection: this.state.data['3_1'].data.resection,
              latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
              longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
            }
            // console.warn('pile3_1',valueApi)
           

            if ( this.state.error == null) {
              if (data.check == false) {
                Alert.alert('', data.errorText[0], [
                  {
                    text: 'OK'
                  }
                ])
                this.setState({checksavebutton:false,saveload:false})
              }else {
                this.setState({saveload:true,statuscolor:data.statuscolor})
                this.props.pileSaveStep03(valueApi)
                // this.onSetStack(3,1)
                // if(this.props.startdate != '' && this.props.startdate != null){
                //   this.setState({ step: 1 ,startdate: this.props.startdate})
                // }else{
                //   this.setState({ step: 1 ,startdate: moment().format("DD/MM/YYYY HH:mm")})
                // }
              }
            } else {
              Alert.alert(this.state.error)
              this.setState({checksavebutton:false,saveload:false})
            }
          })
      }else {
        this.onSetStack(3,1)
        if(this.props.startdate != '' && this.props.startdate != null){
          this.setState({ step: 1 ,startdate: this.props.startdate})
        }else{
          this.setState({ step: 1 ,startdate: moment().format("DD/MM/YYYY HH:mm")})
        }
      }


    }
    else if (step == 1) {
      var value = {
        process:3,
        step:2,
        pile_id: this.props.pileid,
      }
     
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value)
          .then(async (data) =>
          {
          
            var ImageFile = []
            if (this.state.data['3_2'].data.Image_ReadingCoordinate != null && this.state.data['3_2'].data.Image_ReadingCoordinate.length > 0) {
              for (var i = 0; i < this.state.data['3_2'].data.Image_ReadingCoordinate.length; i++) {
                ImageFile.push(this.state.data['3_2'].data.Image_ReadingCoordinate[i])
              }
            }
            var Image_SurveyDiff = []
            if (this.state.data['3_2'].data.Image_SurveyDiff != null && this.state.data['3_2'].data.Image_SurveyDiff.length > 0) {
              for (var i = 0; i < this.state.data['3_2'].data.Image_SurveyDiff.length; i++) {
                Image_SurveyDiff.push(this.state.data['3_2'].data.Image_SurveyDiff[i])
              }
            }
            // if(this.state.data['3_2'].haveedit == true){
              var valueApi ={
                Language:I18n.locale,
                JobId: this.props.jobid,
                PileId: this.props.pileid,
                Page:2,
                ReadingNCoordinate:this.state.data['3_2'].data.northCalBy,
                ReadingECoordinate:this.state.data['3_2'].data.eastCalBy,
                ImageFile:ImageFile,
                Image_SurveyDiff:Image_SurveyDiff,
                latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
                longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
              }
              console.log('pile3_2',this.state.data['3_2'])
              // this.props.pileSaveStep03(valueApi)
            // }
            
            if ( this.state.error == null) {
              if (data.check == false) {
                Alert.alert('', data.errorText[0], [
                  {
                    text: 'OK'
                  }
                ])
                this.setState({checksavebutton:false,saveload:false})
              }else {
                
                var cal1 = parseFloat(this.state.data['3_2'].data.northCalBy) - parseFloat(this.state.data['3_2'].data.northPosition)
                var cal2 = parseFloat(this.state.data['3_2'].data.eastCalBy) - parseFloat(this.state.data['3_2'].data.eastPosition)
                if(Math.abs(cal1).toFixed(4) > this.state.data['3_2'].data.constant || Math.abs(cal2).toFixed(4) > this.state.data['3_2'].data.constant){
                  if(this.state.data['3_2'].haveedit && this.state.data['3_2'].data.flagaccept != 3){
                    // console.warn('eeee',this.state.data['3_2'].haveedit,this.state.data['3_4'].data)
                      await this.setState({statuscolor:1})
                    
                  }else{
                    await this.setState({statuscolor:2})
                  }
                }else{
                  this.setState({approveagian:false,statuscolor:data.statuscolor}
                    // ,()=> console.warn('approveagian not')
                )
                }
                // console.warn('valueApi',valueApi)
                this.setState({saveload:true})
                this.props.pileSaveStep03(valueApi)
                // this.onSetStack(3,2)
                // this.setState({step: 2})
              }
            } else {
              Alert.alert(this.state.error)
              this.setState({checksavebutton:false,saveload:false})
            }
          })
      }else {
        var cal1 = parseFloat(this.state.data['3_2'].data.northCalBy) - parseFloat(this.state.data['3_2'].data.northPosition)
        var cal2 = parseFloat(this.state.data['3_2'].data.eastCalBy) - parseFloat(this.state.data['3_2'].data.eastPosition)
        if(Math.abs(cal1).toFixed(4) > this.state.data['3_2'].data.constant || Math.abs(cal2).toFixed(4) > this.state.data['3_2'].data.constant){
         
          if(this.state.data['3_2'].haveedit && this.state.data['3_2'].data.flagaccept != 3){
           
            var value = {
              process:3,
              step:4,
              pile_id: this.props.pileid,
              data:{
                approve:null,
                approveBy:null,
                ApprovedUserId:null,
                ApprovedDate:null
              }
            }
            
            await this.props.mainPileSetStorage(this.props.pileid,'3_4',value)
          }else{
            
          }
        }else{
          
        }
        this.onSetStack(3,2)
        this.setState({step: 2})
      }


    }else if (step == 2) {
      var value = {
      process:3,
      step:3,
      pile_id: this.props.pileid,
      }
      
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value)
          .then(async (data) =>
          {
           
            if(data.check == true){
            if(this.state.data["3_3"].enddate != ''){
              var end = this.state.data["3_3"].enddate
              var valueend = {
                process:3,
                step:3,
                pile_id: this.props.pileid,
                data:{
                  top:this.state.data["3_3"].data.top,
                  ground:this.state.data["3_3"].data.ground,
                  },
                startdate: this.state.data["3_3"].startdate,
                enddate: end
                }
                await this.props.mainPileSetStorage(this.props.pileid,'3_3',valueend)
            }else{
              var end = moment().format("DD/MM/YYYY HH:mm")
              var valueend = {
                process:3,
                step:3,
                pile_id: this.props.pileid,
                data:{
                  top:this.state.data["3_3"].data.top,
                  ground:this.state.data["3_3"].data.ground,
                  },
                startdate: this.state.data["3_3"].startdate,
                enddate: end
                }
                await this.props.mainPileSetStorage(this.props.pileid,'3_3',valueend)
            }
            var start = moment(this.state.data["3_3"].startdate,"DD/MM/YYYY HH:mm")
            var end1 = moment(end,"DD/MM/YYYY HH:mm")
            if(this.state.data["3_3"].startdate != undefined){
              if(start <= end1){
                var cal1 = parseFloat(this.state.data['3_2'].data.northCalBy) - parseFloat(this.state.data['3_2'].masterInfo.CasingPosition.northing)
                var cal2 = parseFloat(this.state.data['3_2'].data.eastCalBy) - parseFloat(this.state.data['3_2'].masterInfo.CasingPosition.easting)
                // console.warn('cal',this.state.status3)
                if(Math.abs(cal1).toFixed(4) > this.state.data['3_2'].data.constant || Math.abs(cal2).toFixed(4) > this.state.data['3_2'].data.constant){
                  if(this.state.status3 != 2){
                    // console.warn('save33 3_4',this.state.data['3_4'].data.ApprovedDate === '' , 
                    // this.state.data['3_4'].data.ApprovedUserId !== null , 
                    // this.state.data['3_4'].data.ApprovedUserId !== '' )

                    if(this.state.data['3_4'].data.ApprovedDate == '' && 
                    this.state.data['3_4'].data.ApprovedUserId != null && 
                    this.state.data['3_4'].data.ApprovedUserId != '' ){
                      this.setState({approveagian:false}
                        // ,()=> console.warn('approveagian not1')
                      )
                    }else{
                      this.setState({approveagian:true}
                        // ,()=> console.warn('approveagian')
                      )
                    }
                   
                    
                  }
                  
                }else{
                  this.setState({approveagian:false}
                    // ,()=> console.warn('approveagian not1')
                  )
                }
                // console.warn('save 3',this.state.approveagian,data.statuscolor)
              if(this.state.data['3_2'].haveedit == true){
                if(this.state.approveagian && this.state.data['3_2'].data.flagaccept != 3){ 
                 
                 
                }else{
                  
                }
              }
                var valueApi ={
                  Language:I18n.locale,
                  JobId: this.props.jobid,
                  PileId: this.props.pileid,
                  Page:3,
                  TopCasingLevel:this.state.data['3_3'].data.top,
                  GroundLevel:this.state.data['3_3'].data.ground,
                  latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
                  longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
                  startdate:moment(this.state.data['3_3'].startdate,'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm:ss'),
                  enddate:moment(end,'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm:ss')
                }
                // console.warn('pile3_3',this.state.status3)
               
                if(this.state.status3 == 2){
                 
                    // console.warn('save33 3_4',this.state.data['3_4'].data)
                    // this.props.pileSaveStep03(valueApi)
                    if ( this.state.error == null) {
                      if (data.check == false) {
                        Alert.alert('',data.errorText[0], [
                          {
                            text: 'OK'
                          }
                        ])
                        this.setState({checksavebutton:false,saveload:false})
                      }else {
                        if (data.size >= 9) {
                          this.props.pileSaveStep03(valueApi)
                          this.setState({saveload:true})
                            
                           
                            if(this.state.approveagian && this.state.data['3_2'].data.flagaccept != 3){
                             
                              
                             
                            }
                            
                        }else {
                          Alert.alert('', data.errorText[0], [
                            {
                              text: 'OK'
                            }
                          ])
                          this.setState({checksavebutton:false,saveload:false})
                        }
      
                      }
                    } else {
                      Alert.alert(this.state.error)
                      this.setState({checksavebutton:false,saveload:false})
                    }
                  // }
                }else{
                 
                  if ( this.state.error == null) {
                    if (data.check == false) {
                      Alert.alert('test',data.errorText[0], [
                        {
                          text: 'OK'
                        }
                      ])
                      this.setState({checksavebutton:false,saveload:false})
                    }else {
                     
                      if (data.size >= 9) {
                        this.props.pileSaveStep03(valueApi)
                        this.setState({saveload:true})
                         
                         
                          if(this.state.approveagian && this.state.data['3_2'].data.flagaccept != 3){
                            await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step3Status:1})
                            this.props.mainPileGetStorage(this.props.pileid,'step_status')
                          }
                      }else {
                        if(data.errorText[0] == '' || data.errorText[0] == undefined){
                          Alert.alert('', I18n.t('mainpile.3_4.errorsave'), [
                            {
                              text: 'OK'
                            }
                          ])
                          this.setState({checksavebutton:false})
                        }else{
                          Alert.alert('', data.errorText[0], [
                            {
                              text: 'OK'
                            }
                          ])
                          this.setState({checksavebutton:false})
                        }
                        
                      }
    
                    }
                  } else {
                    Alert.alert(this.state.error)
                    this.setState({checksavebutton:false,saveload:false})
                  }
                }
  
                
              }else{
                Alert.alert('', I18n.t("mainpile.liquidtestinsert.alert.error4"), [
                  {
                    text: 'OK'
                  }
                ])
                this.setState({checksavebutton:false,saveload:false})
              }
            }else{
              if(this.props.startdate <= this.props.enddate){
                var valueApi ={
                  Language:I18n.locale,
                  JobId: this.props.jobid,
                  PileId: this.props.pileid,
                  Page:3,
                  TopCasingLevel:this.state.data['3_3'].data.top,
                  GroundLevel:this.state.data['3_3'].data.ground,
                  latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
                  longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
                  startdate:moment(this.props.startdate,'DD/MM/YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss'),
                  enddate:moment(this.props.enddate,'DD/MM/YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss')
                }
                console.log('pile3_3',valueApi)
                // this.props.pileSaveStep03(valueApi)
                
                if ( this.state.error == null) {
                  if (data.check == false) {
                    Alert.alert('', I18n.t('mainpile.3_1.error_nextstep'), [
                      {
                        text: 'OK'
                      }
                    ])
                    this.setState({checksavebutton:false,saveload:false})
                  }else {
                    if (data.size == 9 || data.size == 10) {
                      this.props.pileSaveStep03(valueApi)
                      // this.setState({step: 3})
                      // this.onSetStack(3,3)
                     this.setState({saveload:true})
                      if(this.state.approveagian && this.state.data['3_2'].data.flagaccept != 3){
                        
                      }
                    }else {
                      Alert.alert('', I18n.t('mainpile.3_1.error_nextstep'), [
                        {
                          text: 'OK'
                        }
                      ])
                      this.setState({checksavebutton:false})
                    }
  
                  }
                } else {
                  Alert.alert(this.state.error)
                  this.setState({checksavebutton:false,saveload:false})
                }
              }else{
                Alert.alert('', I18n.t('mainpile.3_1.error_step'), [
                  {
                    text: 'OK'
                  }
                ])
                this.setState({checksavebutton:false})
              }
            }
          }else{
            Alert.alert('',data.errorText[0], [
              {
                text: 'OK'
              }
            ])
            this.setState({checksavebutton:false})
          }
            
          })
      }else {
        this.onSetStack(3,3)
        this.setState({step: 3})
      
        if(this.state.approveagian && this.state.data['3_2'].data.flagaccept != 3){
          
        }
      }


    }else if (step == 3){
      this.setState({checksavebutton:true})
      var value = {
        process:3,
        step:4,
        pile_id: this.props.pileid,
      }
     
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value)
          .then(async (data) =>
          {
            if(this.state.data['3_2'].data.flagaccept != 3){
             
            }
            if ( this.state.error == null) {
              if (data.check == false ||
                this.state.data['3_2'].data.Image_ReadingCoordinate == null ||
                this.state.data['3_2'].data.Image_SurveyDiff == null ||
                this.state.data['3_2'].data.Image_ReadingCoordinate.length == 0 ||
                this.state.data['3_2'].data.Image_SurveyDiff.length == 0 ) {
                  if(this.state.data['3_2'].data.flagaccept == 3){
                    
                    setTimeout(()=>{this.setState({checksavebutton:false,saveload:false})},2000)
                    this.props.getStepStatus2({
                      jobid: this.props.jobid,
                      pileid: this.props.pileid,
                      process:3
                    })
                   
                  }else{
                    Alert.alert('', data.errorText[0], [
                      {
                        text: 'OK',  onPress: async () => {
                         
                        }
                      }
                    ])
                    this.setState({checksavebutton:false,saveload:false})
                  }
                
              }else {
                
                this.props.getStepStatus2({
                  jobid: this.props.jobid,
                  pileid: this.props.pileid,
                  process:3
                })
                setTimeout(()=>{this.setState({checksavebutton:false})},2000)
                
              }
            } else {
              Alert.alert(this.state.error)
              this.setState({checksavebutton:false,saveload:false})
            }
          })
      }else {
       
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process:3
        })
        setTimeout(()=>{this.setState({checksavebutton:false})},2000)
        
      }
    }else if(step == 4){
      this.setState({checksavebutton:true})
      var value = {
        process:3,
        step:5,
        pile_id: this.props.pileid,
      }
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value)
          .then(async (data) =>{
            
            if(data.check){
              var valueApi ={
                Language:I18n.locale,
                JobId: this.props.jobid,
                PileId: this.props.pileid,
                Page:5,
                TopCasingLevel:this.state.data['3_5'].data.top,
                GroundLevel:this.state.data['3_5'].data.ground,
                SurveyorId: this.state.data['3_5'].data.SurveyorId,
                SurveyorName: this.state.data['3_5'].data.SurveyorName,
                latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
                longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
                startdate:moment(this.props.startdate,'DD/MM/YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss'),
                enddate:moment(this.props.enddate,'DD/MM/YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss')
              }
              this.props.pileSaveStep03(valueApi)
              this.setState({saveload:true,statuscolor:data.statuscolor})
             
            }else{
              Alert.alert('',data.errorText[0], [
                {
                  text: 'OK'
                }
              ])
              this.setState({checksavebutton:false})
            }
          })
      }
    }

  }

  async onSetStack(pro,step) {
    var data = {
      process: pro,
      step: step + 1
    }
    await this.props.setStack(data)
    await this.props.mainPileGetAllStorage()
  }
  onBlock(step){
   
    if((this.state.data['3_4'].data.ApprovedDate == ''||this.state.data['3_4'].data.ApprovedDate ==null) && 
    this.state.data['3_4'].data.ApprovedUserId != null && 
    this.state.data['3_4'].data.ApprovedUserId != ''){
      this.setState({step: step})
      this.onSetStack(3,step)
    }else{
      // console.warn('status3',this.state.status3)
      if(this.state.status3 == 2){
        this.setState({step: step})
        this.onSetStack(3,step)
      }else{
        // console.warn('step',step)
        if (step == 3) {
         
            var value = {
              process: 3,
              step: 3,
              pile_id: this.props.pileid
            }
            this.props.mainpileAction_checkStepStatus(value).then(async (data) => {
              if (data.size == 9 || data.size == 10) {
                this.setState({step: step})
                this.onSetStack(3,step)
              }
            })
          // }
            
        }else {
          this.setState({step: step})
          this.onSetStack(3,step)
        }
      }
    }
    
    

  }

  _renderStepFooter() {
    // console.warn('_renderStepFooter',this.state.data['3_4'])
    if(this.props.shape == 1){
      if(this.state.data['3_4']){
        // console.warn('cccc',this.state.data['3_4'].data.ApprovedDate, this.state.data['3_4'].data.ApprovedUserId)
        if((this.state.data['3_4'].data.ApprovedDate == '' || this.state.data['3_4'].data.ApprovedDate == null) && 
        this.state.data['3_4'].data.ApprovedUserId != null && 
        this.state.data['3_4'].data.ApprovedUserId != ''){
          // console.warn('hide')
          return (
            <View>
              <View style={{ flexDirection: "column" }}>
                <View style={{ borderWidth: 1 }}>
                  <StepIndicator stepCount={4} onPress={step => {
                    if (this.refs['pile3_1'].getWrappedInstance().state.loading != true) {
                      this.onBlock(step)
                    }
                  }} currentPosition={this.state.step} />
                </View>
                
  
              </View>
            </View>
          )
        }else{
          if (this.state.step == 3 && this.state.data['3_2'] ) {
            // console.warn('step == 3')
            var canApprove = false
            if (Math.abs(parseFloat(this.state.data['3_2'].data.eastCal)) > Math.abs(parseFloat(this.state.data['3_2'].data.constant))
              || Math.abs(parseFloat(this.state.data['3_2'].data.northCal)) > Math.abs(parseFloat(this.state.data['3_2'].data.constant))
              || this.state.data['3_2'].data.eastCal == null || this.state.data['3_2'].data.northCal == null) {
                canApprove = true
            }else {
              canApprove = false
            }
            // console.warn('test33',canApprove,this.state.isapprove)
            if (this.state.isapprove == false && canApprove == true && this.state.Edit_Flag == 0) {
              // console.warn('test1')
              return (
                <View>
                  <View style={{ flexDirection: "column" }}>
                    <View style={{ borderWidth: 1 }}>
                      <StepIndicator stepCount={4} onPress={step => {
                        if (this.refs['pile3_1'].getWrappedInstance().state.loading != true) {
                          this.onBlock(step)
                        }
                      }} currentPosition={this.state.step} />
                    </View>
                    {
                      this.state.Edit_Flag == 1 ?
                      <View style={[styles.second,
                        this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                      ]}>
                        <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}
                        disabled={this.state.status5 == 0 ? false:true}
                        >
                          <Text style={styles.selectButton}>{I18n.t('mainpile.button.delete')}</Text>
                        </TouchableOpacity>
                        <View style={styles.secondButtonGrey}>
                          <Text style={styles.selectButton}>
                            {
                             I18n.t('mainpile.button.continue')
                            }
                          </Text>
                        </View>
                      </View>
                      :
                      <View/>
                    }
      
                  </View>
                </View>
              )
            }else if (this.state.isapprove == true && canApprove == true && this.state.Edit_Flag == 0 && this.state.data['3_4'].data.approveBy != 1 ) {
              // console.warn('test2')
              return (
                <View>
                  <View style={{ flexDirection: "column" }}>
                    <View style={{ borderWidth: 1 }}>
                      <StepIndicator stepCount={4} onPress={step => {
                        if (this.refs['pile3_1'].getWrappedInstance().state.loading != true) {
                          this.onBlock(step)
                        }
                      } } currentPosition={this.state.step} />
                    </View>
                    {
                      this.state.Edit_Flag == 1 ?
                      <View style={[styles.second,
                        this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                      ]}>
                        <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}>
                          <Text style={styles.selectButton}>{I18n.t('mainpile.button.delete')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.secondButton,
                          this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                        ]} onPress={this.nextButton}
                        disabled={this.state.checksavebutton}
                        >
                          <Text style={styles.selectButton}>
                            {
                             I18n.t('mainpile.button.continue')
                            }
                          </Text>
                        </TouchableOpacity>
                      </View>
                      :
                      <View/>
                    }
      
                  </View>
                </View>
              )
            }else if(this.state.data['3_4'].data.ApprovedDate != null && this.state.data['3_4'].data.ApprovedDate != ''){
              // console.warn('test3')
              return (
                <View>
                  <View style={{ flexDirection: "column" }}>
                    <View style={{ borderWidth: 1 }}>
                      <StepIndicator stepCount={4} onPress={step => {
                        if (this.refs['pile3_1'].getWrappedInstance().state.loading != true) {
                          this.onBlock(step)
                        }
                      } } currentPosition={this.state.step} />
                    </View>
                    {
                      this.state.Edit_Flag == 1 ?
                      <View style={[styles.second,
                        this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                      ]}>
                        <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}>
                          <Text style={styles.selectButton}>{I18n.t('mainpile.button.delete')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.secondButton,
                          this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                        ]} onPress={this.nextButton}
                        disabled={this.state.checksavebutton}
                        >
                          <Text style={styles.selectButton}>
                            {
                             I18n.t('mainpile.button.continue')
                            }
                          </Text>
                        </TouchableOpacity>
                      </View>
                      :
                      <View/>
                    }
      
                  </View>
                </View>
              )
            }else if(this.state.isapprove == false && canApprove == false ) {
              // console.warn('test4')
              return <View>
                <View style={{ flexDirection: "column" }}>
                  <View style={{ borderWidth: 1 }}>
                    <StepIndicator stepCount={4} onPress={step => {
                      if (this.refs['pile3_1'].getWrappedInstance().state.loading != true) {
                        this.onBlock(step)
                      }
                    }} currentPosition={this.state.step} />
                  </View>
                  {
                    this.state.Edit_Flag == 1 ?
                    <View style={[styles.second,
                      this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                    ]}>
                    <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}>
                      <Text style={styles.selectButton}>{I18n.t('mainpile.button.delete')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.secondButton,
                      this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                    ]} onPress={this.nextButton}
                    disabled={this.state.checksavebutton}
                    >
                      <Text style={styles.selectButton}>
                        {
                          <Text style={styles.selectButton}>{this.state.Edit_Flag == 1 ? I18n.t('mainpile.button.next'):I18n.t('mainpile.button.continue')}</Text>
                        }
                      </Text>
                    </TouchableOpacity>
                  </View>
                    :
                    <View/>
                  }
                </View>
              </View>
            }else {
              // console.warn('test5')
              if(canApprove == false){
                return <View>
                <View style={{ flexDirection: "column" }}>
                  <View style={{ borderWidth: 1 }}>
                    <StepIndicator stepCount={4} onPress={step => {
                      if (this.refs['pile3_1'].getWrappedInstance().state.loading != true) {
                        this.onBlock(step)
                      }
                    }} currentPosition={this.state.step} />
                  </View>
                  {
                    this.state.Edit_Flag == 1 ?
                    <View style={[styles.second,
                      this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                    ]}>
                    <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}>
                      <Text style={styles.selectButton}>{I18n.t('mainpile.button.delete')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.secondButton,
                      this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                    ]} onPress={this.nextButton}
                    disabled={this.state.checksavebutton}
                    >
                      <Text style={styles.selectButton}>
                        {
                          <Text style={styles.selectButton}>{this.state.Edit_Flag == 1 ? I18n.t('mainpile.button.next'):I18n.t('mainpile.button.continue')}</Text>
                        }
                      </Text>
                    </TouchableOpacity>
                  </View>
                    :
                    <View/>
                  }
                </View>
              </View>
              }else{
                if(this.state.data['3_2'].data.flagaccept == 3){
                  return <View>
                  <View style={{ flexDirection: "column" }}>
                    <View style={{ borderWidth: 1 }}>
                      <StepIndicator stepCount={4} onPress={step => {
                        if (this.refs['pile3_1'].getWrappedInstance().state.loading != true) {
                          this.onBlock(step)
                        }
                      }} currentPosition={this.state.step} />
                    </View>
                    {
                      this.state.Edit_Flag == 1 ?
                      <View style={[styles.second,
                        this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                      ]}>
                      <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}>
                        <Text style={styles.selectButton}>{I18n.t('mainpile.button.delete')}</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={[styles.secondButton,
                        this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                      ]} onPress={this.nextButton}
                      disabled={this.state.checksavebutton}
                      >
                        <Text style={styles.selectButton}>
                          {
                            <Text style={styles.selectButton}>{this.state.Edit_Flag == 1 ? I18n.t('mainpile.button.next'):I18n.t('mainpile.button.continue')}</Text>
                          }
                        </Text>
                      </TouchableOpacity>
                    </View>
                      :
                      <View/>
                    }
                  </View>
                </View>
                }else{
                  if(this.state.status3 == 2){
                    return <View>
                    <View style={{ flexDirection: "column" }}>
                      <View style={{ borderWidth: 1 }}>
                        <StepIndicator stepCount={4} onPress={step => {
                          if (this.refs['pile3_1'].getWrappedInstance().state.loading != true) {
                            this.onBlock(step)
                          }
                        }} currentPosition={this.state.step} />
                      </View>
                      {
                        this.state.Edit_Flag == 1 ?
                        <View style={[styles.second,
                          this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                        ]}>
                        <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}>
                          <Text style={styles.selectButton}>{I18n.t('mainpile.button.delete')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.secondButton,
                          this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                        ]} onPress={this.nextButton}
                        disabled={this.state.checksavebutton}
                        >
                          <Text style={styles.selectButton}>
                            {
                              <Text style={styles.selectButton}>{this.state.Edit_Flag == 1 ? I18n.t('mainpile.button.next'):I18n.t('mainpile.button.continue')}</Text>
                            }
                          </Text>
                        </TouchableOpacity>
                      </View>
                        :
                        <View/>
                      }
                    </View>
                  </View>
                  }else{
                    return (
                      <View>
                        <View style={{ flexDirection: "column" }}>
                          <View style={{ borderWidth: 1 }}>
                            <StepIndicator stepCount={4} onPress={step => {
                              if (this.refs['pile3_1'].getWrappedInstance().state.loading != true) {
                                this.onBlock(step)
                              }
                            }} currentPosition={this.state.step} />
                          </View>
                          {
                            this.state.Edit_Flag == 1 ?
                            <View style={[styles.second,
                              this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                            ]}>
                              <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}>
                                <Text style={styles.selectButton}>{I18n.t('mainpile.button.delete')}</Text>
                              </TouchableOpacity>
                              <View style={styles.secondButtonGrey}>
                                <Text style={styles.selectButton}>
                                  {
                                   I18n.t('mainpile.button.continue')
                                  }
                                </Text>
                              </View>
                            </View>
                            :
                            <View/>
                          }
                        </View>
                      </View>)
                  }
                  
                }
                
              }
              
              }
          }else {
    
            // console.warn('step != 3',this.state.step)
            return (
              <View>
                <View style={{ flexDirection: "column" }}>
                  <View style={{ borderWidth: 1 }}>
                    <StepIndicator stepCount={4} onPress={step => {
                      if (this.refs['pile3_1'].getWrappedInstance().state.loading != true) {
                        this.onBlock(step)
                      }
                    }} currentPosition={this.state.step} />
                  </View>
                  {
                    this.state.Edit_Flag == 1 ?
                    <View style={[styles.second,
                      this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                    ]}>
                    <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}>
                      <Text style={styles.selectButton}>{I18n.t('mainpile.button.delete')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.secondButton,
                      this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
                    ]} onPress={this.nextButton}
                    disabled={this.state.checksavebutton}
                    >
                      <Text style={styles.selectButton}>
                        {
                          <Text style={styles.selectButton}>{this.state.Edit_Flag == 1 ? I18n.t('mainpile.button.next'):I18n.t('mainpile.button.continue')}</Text>
                        }
                      </Text>
                    </TouchableOpacity>
                  </View>
                    :
                    <View/>
                  }
                </View>
              </View>
            )
          }
        }
      }
      
      
    }else{
      return (
        <View>
          <View style={{ flexDirection: "column" }}>
            {
              this.state.Edit_Flag == 1 ?
              <View style={[styles.second,
                this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
              ]}>
              <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}>
                <Text style={styles.selectButton}>{I18n.t('mainpile.button.delete')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.secondButton,
                this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
              ]} onPress={this.nextButton}
              disabled={this.state.checksavebutton}
              >
                <Text style={styles.selectButton}>
                  {
                    <Text style={styles.selectButton}>{this.state.Edit_Flag == 1 ? I18n.t('mainpile.button.next'):I18n.t('mainpile.button.continue')}</Text>
                  }
                </Text>
              </TouchableOpacity>
            </View>
              :
              <View/>
            }
          </View>
        </View>
      )
    }
    
  }

  nextButton = async () => {
    switch (await this.state.step) {
      case 0:
        this.onSave(this.state.step)
        break
      case 1:
        this.onSave(this.state.step)
        break
      case 2:
        this.onSave(this.state.step)
        break
      case 3:
        this.onSave(this.state.step)
        break
      case 4:
        this.onSave(this.state.step)
      break
      default:
        break
    }
  }

  deleteButton = () => {
    this.props.setStack({
      process: 3,
      step: 1
    })
    const data = {
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      processid: this.state.process,
      lat: this.state.location == undefined ? 1 : this.state.location.position.lat,
      log: this.state.location == undefined ? 1 : this.state.location.position.log,
    }
    if(this.state.status5 == 0){
      Alert.alert("Warning", I18n.t("mainpile.button.confirmdelete"), [
        { text: "Cancel" },
        { text: "OK", onPress: () => this.props.pileDelete(data) }
      ])
    }else{
      Alert.alert("", I18n.t('alert.error_delete3'), [
        {
          text: "OK"
        }
      ])
    }
  }

  initialStep(){
    if(this.props.shape == 1){
      this.setState({step:0})
    }else{
      this.setState({step:4})
    }
    
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    //  console.warn('edit flag',this.state.saveload)
    return (
      <View style={{ flex: 1 }}>
        <KeyboardAwareScrollView scrollEnabled={true} resetScrollToCoords={{ x: 0, y: 0 }} enableOnAndroid ={true}>
        {
          this.state.saveload == true?<Content><ActivityIndicator size="large" color="#007CC2" /></Content>:<View/>
        }
          {!this.state.saveload?<Content>
            <Pile3_1 
              ref='pile3_1' 
              hidden={!(this.state.step == 0)} 
              jobid={this.props.jobid} 
              pileid={this.props.pileid} 
              onRefresh={this.props.onRefresh} 
              waitApprove={this.state.waitApprove}
              />
            <Pile3_2 
              ref='pile3_2' 
              hidden={!(this.state.step == 1)} 
              jobid={this.props.jobid} 
              pileid={this.props.pileid} 
              shape={this.props.shape} 
              category={this.props.category}
              waitApprove={this.state.waitApprove}
              />
            <Pile3_3 
              ref='pile3_3' 
              hidden={!(this.state.step == 2)} 
              jobid={this.props.jobid} 
              pileid={this.props.pileid} 
              shape={this.props.shape} 
              startdatedefault={this.state.startdate}
              startdate={this.props.startdate}
              enddate={this.props.enddate}
              waitApprove={this.state.waitApprove}
              />
            <Pile3_4 
             ref='pile3_4' 
             hidden={!(this.state.step == 3)} 
             jobid={this.props.jobid} 
             pileid={this.props.pileid} 
             shape={this.props.shape} 
             approveagian={this.state.approveagian}
             category={this.props.category}
             flagApprove={false}
             onNextStep={this.props.onNextStep}
             waitApprove={this.state.waitApprove}
             />
             <Pile3_5 
             ref='pile3_5' 
             hidden={!(this.state.step == 4)} 
             jobid={this.props.jobid} 
             pileid={this.props.pileid} 
             shape={this.props.shape} 
             category={this.props.category}/>
          </Content>:<View/>}
        </KeyboardAwareScrollView>
        {this._renderStepFooter()}
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    error: state.pile.error,
    pileAll: state.mainpile.pileAll || null,
    stack: state.mainpile.stack_item,
    process3_success_1:state.pile.process3_success_1,
    process3_success_1_random:state.pile.process3_success_1_random,
    process3_success_2:state.pile.process3_success_2,
    process3_success_2_random:state.pile.process3_success_2_random,
    process3_success_3:state.pile.process3_success_3,
    process3_success_3_random:state.pile.process3_success_3_random,
    process3_success_4:state.pile.process3_success_4,
    process3_success_4_random:state.pile.process3_success_4_random,

    step_status2_random:state.mainpile.step_status2_random,
    step_status2:state.mainpile.step_status2,
    processstatus:state.mainpile.process
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(Pile3)
