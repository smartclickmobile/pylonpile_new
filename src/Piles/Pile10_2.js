import React, { Component } from "react"
import {
  View,
  Text,
  TouchableOpacity,
  TextInput
} from "react-native"
import ModalSelector from "react-native-modal-selector"
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM } from "../Constants/Color"
import { Icon } from "react-native-elements"
import * as actions from "../Actions"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import I18n from "../../assets/languages/i18n"
import styles from "./styles/Pile10_2.style"
import { Input, Item } from "native-base"

class Pile10_2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      weather1: false,
      weather2: false,
      weather3: false,
      weather4: false,
      weather5: false,
      weather0: false,
      etc: "",
      process: 10,
      step: 1,
      weather: "",
      Edit_Flag: 1
    }
  }

  updateState(value) {
    console.log("Update state from 10_1")
  }

  componentDidMount() {
    var pile = null
    var pileMaster = null
    if (
      this.props.pileAll[this.props.pileid] &&
      this.props.pileAll[this.props.pileid]["10_2"]
    ) {
      console.log(
        "this.props.pileAll[this.props.pileid]",
        this.props.pileAll[this.props.pileid]["10_2"]
      )
      pile = this.props.pileAll[this.props.pileid]["10_2"].data
      pileMaster = this.props.pileAll[this.props.pileid]["10_2"].masterInfo
    }

    if (pile) {
      // if (pile.Weather == "ท้องฟ้าโปร่ง" || pile.Weather == "มีเมฆมาก" || pile.Weather == "ฝนตก"||pile.Weather == "1" || pile.Weather == "2" || pile.Weather == "3") this.setState({ weather: pile.Weather })
      // else {
      //   this.setState({ weather: 0, etc: pile.Weather })
      // }
      console.log("didmount step10_2", pile)
      if (
        pile.IsFine ||
        pile.Weather == "ท้องฟ้าโปร่ง" ||
        pile.Weather == "1"
      ) {
        this.setState({ weather1: true })
      }
      if (pile.IsCloudy || pile.Weather == "มีเมฆมาก" || pile.Weather == "2") {
        this.setState({ weather2: true })
      }
      if (pile.IsRainy || pile.Weather == "ฝนตก" || pile.Weather == "3") {
        this.setState({ weather3: true })
      }
      if (pile.IsHail) {
        this.setState({ weather4: true })
      }
      if (pile.IsWindy) {
        this.setState({ weather5: true })
      }
      if (
        pile.Weather != "ท้องฟ้าโปร่ง" &&
        pile.Weather != "มีเมฆมาก" &&
        pile.Weather != "ฝนตก" &&
        pile.Weather != "1" &&
        pile.Weather != "2" &&
        pile.Weather != "3" &&
        pile.Weather != "" &&
        pile.Weather != null &&
        pile.Weather != "null"
      ) {
        console.log("pile.Weather", pile.Weather)
        this.setState({ weather0: true, etc: pile.Weather })
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    // console.warn('NEXT PROPS 10-2')
    var pile = null
    var pile10 = null
    if (
      nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['10_2']
    ) {
      pile = nextProps.pileAll[this.props.pileid]["10_2"].data
      pile10 = nextProps.pileAll[this.props.pileid]["10_0"].data
    }
    if (pile) {
      // if (pile.Weather == "ท้องฟ้าโปร่ง" || pile.Weather == "มีเมฆมาก" || pile.Weather == "ฝนตก"||pile.Weather == "1" || pile.Weather == "2" || pile.Weather == "3") this.setState({ weather: pile.Weather })
      // else {
      //   this.setState({ weather: 0, etc: pile.Weather })
      // }
      console.log("WillReceiveProps step10_2", pile)
      if (pile.IsFine != null) {
        // console.log(,pile.IsFine)
        this.setState({ weather1: pile.IsFine })
      } else if (pile.Weather == "ท้องฟ้าโปร่ง" || pile.Weather == "1") {
        this.setState({ weather1: true })
      }
      if (pile.IsCloudy != null) {
        this.setState({ weather2: pile.IsCloudy })
      } else if (pile.Weather == "มีเมฆมาก" || pile.Weather == "2") {
        this.setState({ weather2: true })
      }
      if (pile.IsRainy != null) {
        this.setState({ weather3: pile.IsRainy })
      } else if (pile.Weather == "ฝนตก" || pile.Weather == "3") {
        this.setState({ weather3: true })
      }
      if (pile.IsHail != null) {
        this.setState({ weather4: pile.IsHail })
      }
      if (pile.IsWindy) {
        this.setState({ weather5: pile.IsWindy })
      }
      if (
        pile.Weather != "ท้องฟ้าโปร่ง" &&
        pile.Weather != "มีเมฆมาก" &&
        pile.Weather != "ฝนตก" &&
        pile.Weather != "1" &&
        pile.Weather != "2" &&
        pile.Weather != "3" &&
        pile.Weather != "" &&
        pile.Weather != null &&
        pile.Weather != "null"
      ) {
        console.log("pile.Weather", pile.Weather)
        this.setState({ weather0: true, etc: pile.Weather })
      }
    }
    if (pile10) {
      this.setState({ Edit_Flag: pile10.Edit_Flag })
    }
  }

  saveLocal() {
    var value = {
      process: 10,
      step: 2,
      pile_id: this.props.pileid,
      data: {
        Weather: this.state.weather0 == true ? this.state.etc : "",
        IsFine: this.state.weather1,
        IsCloudy: this.state.weather2,
        IsRainy: this.state.weather3,
        IsHail: this.state.weather4,
        IsWindy: this.state.weather5
      }
    }
    // console.warn("savelcal", value)
    this.props.mainPileSetStorage(this.props.pileid, "10_2", value)
  }

  selectWeather(data) {
    if (this.state.Edit_Flag == 1) {
      if (data == 1) {
        this.setState({ weather1: true }, () => this.saveLocal())
      } else if (data == 2) {
        this.setState({ weather2: true }, () => this.saveLocal())
      } else if (data == 3) {
        this.setState({ weather3: true }, () => this.saveLocal())
      } else if (data == 4) {
        this.setState({ weather4: true }, () => this.saveLocal())
      } else if (data == 5) {
        this.setState({ weather5: true }, () => this.saveLocal())
      } else if (data == 0) {
        this.setState({ weather0: true }, () => this.saveLocal())
      }
    }
  }

  deselect(data) {
    if (this.state.Edit_Flag == 1) {
      // this.setState({ weather: "" }, () => this.saveLocal())
      if (data == 1) {
        this.setState({ weather1: false }, () => this.saveLocal())
      } else if (data == 2) {
        this.setState({ weather2: false }, () => this.saveLocal())
      } else if (data == 3) {
        this.setState({ weather3: false }, () => this.saveLocal())
      } else if (data == 4) {
        this.setState({ weather4: false }, () => this.saveLocal())
      } else if (data == 5) {
        this.setState({ weather5: false }, () => this.saveLocal())
      } else if (data == 0) {
        this.setState({ weather0: false, etc: null }, () => this.saveLocal())
      }
    }
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    return (
      <View>
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t("mainpile.10_2.topic")}</Text>
        </View>
        <View>
          <View style={styles.checkbox}>
            {this.state.weather1 ? (
              <Icon
                name="check"
                reverse
                color="#6dcc64"
                size={15}
                onPress={() => this.deselect(1)}
              />
            ) : (
              <Icon
                name="check"
                reverse
                color="grey"
                size={15}
                onPress={() => this.selectWeather(1)}
              />
            )}
            <Text style={styles.checkboxText}>
              {I18n.t("mainpile.10_2.clear")}
            </Text>
          </View>

          <View style={styles.checkbox}>
            {this.state.weather2 ? (
              <Icon
                name="check"
                reverse
                color="#6dcc64"
                size={15}
                onPress={() => this.deselect(2)}
              />
            ) : (
              <Icon
                name="check"
                reverse
                color="grey"
                size={15}
                onPress={() => this.selectWeather(2)}
              />
            )}
            <Text style={styles.checkboxText}>
              {I18n.t("mainpile.10_2.cloudy")}
            </Text>
          </View>
          <View style={styles.checkbox}>
            {this.state.weather3 ? (
              <Icon
                name="check"
                reverse
                color="#6dcc64"
                size={15}
                onPress={() => this.deselect(3)}
              />
            ) : (
              <Icon
                name="check"
                reverse
                color="grey"
                size={15}
                onPress={() => this.selectWeather(3)}
              />
            )}
            <Text style={styles.checkboxText}>
              {I18n.t("mainpile.10_2.rain")}
            </Text>
          </View>
          {/*<View style={styles.checkbox}>
            {this.state.weather4  ? (
              <Icon name="check" reverse color="#6dcc64" size={15} onPress={() => this.deselect(4)} />
            ) : (
              <Icon name="check" reverse color="grey" size={15} onPress={() => this.selectWeather(4)} />
            )}
            <Text style={styles.checkboxText}>{I18n.t('mainpile.10_2.snow')}</Text>
            </View>*/}
          <View style={styles.checkbox}>
            {this.state.weather5 ? (
              <Icon
                name="check"
                reverse
                color="#6dcc64"
                size={15}
                onPress={() => this.deselect(5)}
              />
            ) : (
              <Icon
                name="check"
                reverse
                color="grey"
                size={15}
                onPress={() => this.selectWeather(5)}
              />
            )}
            <Text style={styles.checkboxText}>
              {I18n.t("mainpile.10_2.wind")}
            </Text>
          </View>
          {/*<View style={styles.checkbox}>
            {this.state.weather0  ? (
              <Icon name="check" reverse color="#6dcc64" size={15} onPress={() => this.deselect(0)} />
            ) : (
              <Icon name="check" reverse color="grey" size={15} onPress={() => this.selectWeather(0)} />
            )}
            <Text style={styles.checkboxText}>{I18n.t('mainpile.10_2.other')}</Text>
            <View style={styles.inputView}>
              <TextInput
                editable={this.state.weather0 && this.state.Edit_Flag == 1}
                value={this.state.etc}
                onChangeText={etc => {
                  // console.warn(etc)
                  this.setState({ etc }, () => {
                    // this.saveLocal()
                    console.log("this.state.etc",this.state.etc)
                  })
                  }
                }
                onEndEditing={()=>{
                 
                    this.saveLocal()
                  
                  
                }}
                
              />
            </View>
              </View>*/}
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(
  Pile10_2
)
