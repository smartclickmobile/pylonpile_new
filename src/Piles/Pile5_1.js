import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native"
import { Container, Content } from "native-base"
import { Icon } from "react-native-elements"
import Carousel from "react-native-snap-carousel"
import ModalSelector from "react-native-modal-selector"
import { MAIN_COLOR, SUB_COLOR, BASIC_COLOR, MENU_GREY_ITEM, BUTTON_COLOR, BLUE_COLOR } from "../Constants/Color"
import { GroupEmployee } from "../Controller/API"
import I18n from "../../assets/languages/i18n"
import { Extra } from "../Controller/API"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"
import styles from './styles/Pile5_1.style'
import RNTooltips from "react-native-tooltips"
class Pile5_1 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 5,
      step: 1,
      Edit_Flag:1,
      images_water:[],
      images_plummet:[],
      images_hanging:[],
      checkplummet:false,
      checkwater:false,
      loading:true,
      count:0,
      status5:0,
      distance:'0',
      pco:'0',
      metal:'0',
      visible:false,
      cutoff: this.props.pile.cutoff || 0,
      distanceCalled:false,
      pcoCalled:false,
      topcasing:'',
      check1:false,
      check2:false,

      hanginglength:''
    }
  }

  componentWillMount() {

  }
  componentDidUpdate(prevProps, prevState){
    if (this.state.checkplummet != prevState.checkplummet) {
      this.setState({count:this.state.count + 1})
    }
    if (this.state.checkwater != prevState.checkwater) {
      this.setState({count:this.state.count + 1})
    }
    if (this.state.count != prevState.count && this.state.count == 1) {
      this.setState({loading:false})
    }else {
      if (this.state.loading == true && this.props.hidden == false) {
        setTimeout(() => {this.setState({loading: false})}, 100)
      }
    }
    
  }
  componentDidMount(){
    if (this.props.onRefresh == 'refresh' && this.state.loading == true) {
      Actions.refresh({
          action: 'start',
      })
      
    }
  }

  async componentWillReceiveProps(nextProps) {
    var pile = null
    var pile5_0 = null
    var pileMaster = null
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['5_1']) {
      pile = nextProps.pileAll[this.props.pileid]['5_1'].data
      pileMaster = nextProps.pileAll[this.props.pileid]["5_1"].masterInfo
    }
    if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]['5_0']) {
      pile5_0 = nextProps.pileAll[this.props.pileid]['5_0'].data
      this.setState({status5:nextProps.pileAll[this.props.pileid].step_status.Step5Status})
    }
    // if(pileMaster){
    //   this.setState({distance: parseFloat(pileMaster.OverLifting).toFixed(3),pco: parseFloat(pileMaster.Pcoring).toFixed(3)})
    // }
    
    if (pile) {
      console.log('enter pile')
      if (pile.CheckPlummet != null && pile.CheckPlummet != undefined) {
        await this.setState({checkplummet: pile.CheckPlummet})
      }else if(pile.CheckPlummet == null) {
        this.setState({checkplummet: false})
      }
      if (pile.ImageCheckPlummet != "" && pile.ImageCheckPlummet != undefined && pile.ImageCheckPlummet.length >0 ) {
        await this.setState({images_plummet: pile.ImageCheckPlummet})
      }else if(pile.ImageCheckPlummet == null || pile.ImageCheckPlummet.length == 0) {
        this.setState({images_plummet: []})
      }
      // if (pile.ImageLengthSteelCarry != "" && pile.ImageLengthSteelCarry != undefined && pile.ImageLengthSteelCarry.length >0 ) {
      //   await this.setState({images_hanging: pile.ImageLengthSteelCarry})
      // }else if(pile.ImageLengthSteelCarry == null || pile.ImageLengthSteelCarry.length == 0) {
      //   this.setState({images_hanging: []})
      // }
      if (pile.CheckWaterLevel != null && pile.CheckWaterLevel != undefined) {
        await this.setState({checkwater: pile.CheckWaterLevel})
      }else if(pile.CheckWaterLevel == null) {
        this.setState({checkwater: false})
      }
      if (pile.ImageCheckWaterLevel != "" && pile.ImageCheckWaterLevel != undefined && pile.ImageCheckWaterLevel.length >0 ) {
        await this.setState({images_water: pile.ImageCheckWaterLevel})
      }else if(pile.ImageCheckWaterLevel == null || pile.ImageCheckWaterLevel.length == 0) {
        this.setState({images_water: []})
      }
      // if (pile.HangingBarsLength != "" && pile.HangingBarsLength != undefined) {
      //   await this.setState({hanginglength: parseFloat(pile.HangingBarsLength).toFixed(3)})
      // }else{
      //   this.setState({hanginglength: ''})
      // }
      if (pile.CheckWaterLevel == false && pile.CheckWaterLevel == false) {
        await this.setState({loading: false})
      }

      

    }
    if (pile5_0 && this.props.hidden == false) {
      // if (pile3_0.Edit_Flag != null && pile3_0.Edit_Flag != undefined && pile3_0.Edit_Flag != '') {
        this.setState({Edit_Flag:pile5_0.Edit_Flag})
      // }
    }
    var pile3_3 = null
    if(this.props.shape != 1){
      if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["3_5"]) {
      
        pile3_3 = nextProps.pileAll[this.props.pileid]["3_5"].data
      }
    }else{
      if(nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["3_3"]) {
      
        pile3_3 = nextProps.pileAll[this.props.pileid]["3_3"].data
      }
    }
    
    if(pile3_3) {
      if(pile3_3.top != null && pile3_3.top != undefined) {
        this.setState({ topcasing: parseFloat(pile3_3.top)})
      }
    }
    if(pileMaster) {
      if(this.props.category == 5&&this.props.parent == false){
        this.setState({cutoff:pileMaster.cutoff})
      }
      
    }
    if(pile){
      let check1 = pile.OverLifting!==''&&pile.OverLifting!==null
      let check2 = pile.Pcoring!==''&&pile.Pcoring!==null
      if(check1&&check2){
        this.setState({distance: parseFloat(pile.OverLifting).toFixed(3),pco: parseFloat(pile.Pcoring).toFixed(3)},()=>{
          this.onCalculate()
        })
      }else{
        console.log('check master',check1,pileMaster.OverLifting,parseFloat(pileMaster.OverLifting).toFixed(3))
        // if(check1) this.setState({distance: (parseFloat(pileMaster.OverLifting).toFixed(3)).toString()})
        // if(check2) this.setState({pco: (parseFloat(pileMaster.Pcoring).toFixed(3)).toString()})
        if(!this.state.distanceCalled&&!this.state.pcoCalled)
        this.setState({distance: (parseFloat(pileMaster.OverLifting).toFixed(3)).toString(),pco: (parseFloat(pileMaster.Pcoring).toFixed(3)).toString()},()=>{
          this.onCalculate()
        })
        // this.saveLocal('5_1')
      }
    }
    
    // if(pileMaster) {
    //   if(pileMaster.OverLifting && !this.state.distanceCalled) {
    //     if(pile && pile.OverLifting!==null) {
    //       this.setState({ distance: isNaN(parseFloat(pile.OverLifting))?'': (parseFloat(pile.OverLifting).toFixed(3)).toString() }, () => {
    //         if(this.state.distance != '' && this.state.pco != ''){
    //           this.onCalculate()
    //           if(this.state.check1 == false){
    //             // console.warn('OverLifting1',pile.OverLifting)
    //             this.saveLocal('5_1')
    //             this.setState({check1: true})
    //           }
    //         }
    //       })
    //     } else {
    //       this.setState({ distance: (parseFloat(pileMaster.OverLifting).toFixed(3)).toString() }, () => {
    //         this.onCalculate()
    //         if(this.state.check1 == false){
    //           // console.warn('OverLifting2',pile.OverLifting)
    //           this.saveLocal('5_1')
    //           this.setState({check1: true})
    //         }
    //       })
    //     }
    //   }
    //   if(pileMaster.Pcoring && !this.state.pcoCalled) {
    //     if(pile && pile.Pcoring!==null) {
    //       this.setState({ pco:isNaN(parseFloat(pile.Pcoring))?'': (parseFloat(pile.Pcoring).toFixed(3)).toString() }, () => {
    //         if(this.state.distance != '' && this.state.pco != ''){
    //           this.onCalculate()
    //           if(this.state.check2 == false){
    //             this.saveLocal('5_1')
    //             this.setState({check2: true})
    //           }
    //         }
    //       })
    //     } else {
    //       this.setState({ pco: (parseFloat(pileMaster.Pcoring).toFixed(3)).toString() }, () => {
    //         this.onCalculate()
    //         if(this.state.check2 == false){
    //           this.saveLocal('5_1')
    //           this.setState({check2: true})
    //         }
    //       })
    //     }
    //   }
    //   if(pile.OverLifting==null){
    //     this.setState({ distance: (parseFloat(pileMaster.OverLifting).toFixed(3)).toString()}, () => 
    //     {
    //       if(this.state.distance != ''){
    //         this.onCalculate()
    //       }
    //     }
    //   )
    //   }
    //   if(pile.Pcoring==null){
    //     this.setState({ pco: (parseFloat(pileMaster.Pcoring).toFixed(3)).toString()}, () => this.onCalculate())
    //   }
    // }

  }

  async selectedPlummet(){
    var flag_edit = true
    if(this.props.category == 5){
      if(this.props.parent == false){
        flag_edit = false
      }
    }else{
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
    }
    if (flag_edit) {
      await this.setState({ checkplummet: !this.state.checkplummet })
      this.saveLocal('5_1')
    }
  }
  async selectedWater(){
    var flag_edit = true
    if(this.props.category == 5){
      if(this.props.parent == false){
        flag_edit = false
      }
    }else{
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
    }
    if (flag_edit == 1) {
      await this.setState({ checkwater: !this.state.checkwater })
      this.saveLocal('5_1')
    }
  }

  onCameraRoll(keyname,photos,type){
    // console.warn('this.state.process =',this.state.process)
    if (this.state.Edit_Flag == 1) {
      Actions.camearaRoll({status:this.state.status5,jobid:this.props.jobid,process:this.state.process,step:this.state.step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:type,shape:this.props.shape,category:this.props.category,Edit_Flag:this.state.Edit_Flag})
    }else {
      Actions.camearaRoll({status:this.state.status5,jobid:this.props.jobid,process:this.state.process,step:this.state.step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:'view',shape:this.props.shape,category:this.props.category,Edit_Flag:this.state.Edit_Flag})
    }
  }


  async updateState(value) {
    if (value.data.images_water != undefined) {
       await this.setState({images_water:value.data.images_water})
       this.saveLocal('5_1')
    }
    if (value.data.images_plummet != undefined) {
       await this.setState({images_plummet:value.data.images_plummet})
       this.saveLocal('5_1')
    }

    if (value.data.images_hanging != undefined) {
      await this.setState({images_hanging:value.data.images_hanging})
      this.saveLocal('5_1')
   }
  }

  async saveLocal(process_step){
    // console.warn('saveLocal')
    var value = {
      process:5,
      step:1,
      pile_id: this.props.pileid,
      data:{
        CheckPlummet:this.state.checkplummet,
        CheckWaterLevel:this.state.checkwater,
        ImageCheckPlummet: this.state.images_plummet,
        ImageCheckWaterLevel: this.state.images_water,
        ImageLengthSteelCarry: this.state.images_hanging,
        OverLifting: this.state.distance,
        Pcoring: this.state.pco,
        Metal: this.state.metal,
        HangingBarsLength: this.state.hanginglength
      }
    }
    // console.warn('saveLocal',value)
    console.log(value)
    await this.props.mainPileSetStorage(this.props.pileid,process_step,value)
  }
  onChangeFormatDecimal(data) {
      if(data.split(".")[1]) {
        if(data.split(".")[1].length > 3) {
          return parseFloat(data).toFixed(3)
        } else {
          return data
        }
      } else {
        return data
      }

  }
  onCalculate(type, value) {
    console.warn("on cal",value,type)

    switch(type) {
      case "distance":

        if(value != '' &&  value != undefined&&value>=0){
          var temp = this.state.topcasing - this.state.cutoff - parseFloat(value) - parseFloat(this.state.pco)
          this.setState({ metal: isNaN(temp.toFixed(3))?'':temp.toFixed(3), distance: this.onChangeFormatDecimal(value),visible:false }, () => {
            // this.saveLocal('5_1')
          })
        }else{
          this.setState({ metal: '', distance:value!='.'? '':value,visible:false }, () => {
            // this.saveLocal('5_1')
          })
        }
        break
      case "pco":
        if(value != '' &&  value != undefined&value>=0){
          var temp = this.state.topcasing - this.state.cutoff - parseFloat(this.state.distance) - parseFloat(value)
          this.setState({ metal: isNaN(temp.toFixed(3))?'':temp.toFixed(3), pco: this.onChangeFormatDecimal(value),visible:false }, () => {
            // this.saveLocal('5_1')
          })
        }else{
          this.setState({ metal: '', pco: value!='.'? '':value,visible:false }, () => {
            // this.saveLocal('5_1')
          })
        }
        break
      default:
      
        let metal = (this.state.topcasing - this.state.cutoff - parseFloat(this.state.distance) - parseFloat(this.state.pco)).toFixed(3)
        // console.warn("metal",metal)
        
        this.setState(
          {
            metal:metal
          },
          () => {
            // console.log("default on cal")
            // this.saveLocal('5_1')
          }
        )
        break
    }
  }




  render() {
    if (this.props.hidden) {
      return null
    }
    if (this.state.loading) {
      return (
        <View style={{ flex: 1,justifyContent: 'center' }}>
          <ActivityIndicator size="large" color="#007CC2" />
        </View>
      )
    }
    var flag_edit = true
    if(this.props.category == 5){
      if(this.props.parent == false){
        flag_edit = false
      }
    }else{
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
    }
    return (
      <View style={styles.listContainer}>
        <RNTooltips
          text={this.props.category!=1&&this.props.category!=4?I18n.t('tooltip.7_2dw'):I18n.t('tooltip.7_2')}
          textSize={16}
          visible={this.state.visible}
          reference={this.main}
          tintColor="#007CC2"
          duration={6500}/>
        <View >
          <Text style={styles.topicText}>{I18n.t("mainpile.5_2.topic")}</Text>
        </View>
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.7_2.length")}</Text>
          </View>
          <View style={styles.selectedView}>
            <View style={[styles.button, { height: 50, padding: 0 }, flag_edit==false && { borderColor: MENU_GREY_ITEM }]}>
              <TextInput
                ref={(input) => { this.distanceInput = input; }}
                editable={flag_edit}
                keyboardType="numeric"
                value={this.state.distance.toString()}
                placeholder={I18n.t("mainpile.7_2.length")}
                onChangeText={distance => this.setState({distanceCalled:true},()=>{this.onCalculate("distance", distance)})}
                underlineColorAndroid="transparent"
                style={[flag_edit ? styles.textInputStyle : styles.textInputStyleGrey,{fontSize: this.state.distance==''? 18:20}]}
                onEndEditing={() => {
                  this.setState({distance: isNaN(parseFloat(this.state.distance))?'':parseFloat(this.state.distance).toFixed(3)},()=>{
                    this.saveLocal('5_1')
                    this.pcoInput.focus()
                  })
                  
                }
              }
              />
            </View>
          </View>
        </View>
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.7_2.height")}</Text>
          </View>
          <View style={styles.selectedView}>
            <View style={[styles.button, { height: 50, padding: 0 }, flag_edit == false && { borderColor: MENU_GREY_ITEM }]}>
              <TextInput
                ref={(input) => { this.pcoInput = input; }}
                editable={flag_edit}
                keyboardType="numeric"
                value={this.state.pco.toString()}
                placeholder={I18n.t("mainpile.7_2.height")}
                // onChangeText={pco => this.onCalculate("pco", pco)}
                onChangeText={pco => this.setState({pcoCalled:true},()=>{this.onCalculate("pco", pco)})}
                underlineColorAndroid="transparent"
                style={[flag_edit ? styles.textInputStyle : styles.textInputStyleGrey,{fontSize: this.state.pco==''? 18:20}]}
                onEndEditing={() => 
                  {
                    this.setState({pco: isNaN(parseFloat(this.state.pco))?'':parseFloat(this.state.pco).toFixed(3)},()=>{
                      this.saveLocal('5_1')

                      // this.hanginglengthInput.focus()
                    })
                    
                  }
                }
              />
            </View>
          </View>
        </View>
        <View>
          <View style={[styles.boxTopic,{flexDirection:"row"}]}>
            {/* <Text>{I18n.t("mainpile.7_2.maxlength")+" "}</Text> */}
            <Text>{I18n.t((this.props.category!=1&&this.props.category!=4)?"mainpile.7_2.maxlength_super":"mainpile.7_2.maxlength")+" "}</Text>
            <Icon
            name="ios-information-circle"
            type="ionicon"
            color="#517fa4"
            onPress={() => {
              this.setState(
                {
                  visible: true,
                  // dismiss: false,
                  reference:this.main
                },()=>{this.setState({visible: false})})
            }}
          />
          </View>
          <View style={styles.selectedView} >
            <View style={[styles.button, { height: 50, padding: 0, borderColor: MENU_GREY_ITEM }]} ref={ref => {
              this.main = ref
            }}>
              <TextInput
                keyboardType="numeric"
                value={this.state.metal}
                onChangeText={metal => this.setState({ metal })}
                underlineColorAndroid="transparent"
                style={styles.textInputStyleGrey}
                editable={false}
              />
            </View>
          </View>
          {/* <View style={[styles.boxTopic,{flexDirection:"row",justifyContent:'center',alignItems:'center'}]}>
            <Text>{I18n.t("mainpile.5_1.hangingbar")}</Text>
            <View style={styles.buttonImage}>
                <TouchableOpacity
                  style={[styles.image,this.state.images_hanging.length>0?{backgroundColor:'#6dcc64'}:{}]}
                  onPress={() => this.onCameraRoll("images_hanging", this.state.images_hanging,flag_edit?'edit':'view')}
                >
                  <Icon name="image" type="entypo" color="#FFF" />
                  <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                </TouchableOpacity>
            </View>
          </View>
          <View style={styles.selectedView} >
            <View style={[styles.button, { height: 50, padding: 0 }, flag_edit == false && { borderColor: MENU_GREY_ITEM }]}>
              <TextInput
                ref={(input) => { this.hanginglengthInput = input; }}
                editable={flag_edit}
                keyboardType="numeric"
                value={this.state.hanginglength.toString()}
                onChangeText={hanginglength => this.setState({ hanginglength })}
                underlineColorAndroid="transparent"
                style={flag_edit ? styles.textInputStyle : styles.textInputStyleGrey}
                placeholder={I18n.t("mainpile.5_1.hangingbarlength")}
                onEndEditing={()=> {
                  this.setState({hanginglength:isNaN(parseFloat(this.state.hanginglength))?'':  parseFloat(this.state.hanginglength).toFixed(3) })
                  this.saveLocal('5_1')
                }}
              />
            </View>
          </View> */}
        </View>

        {this.props.category!=5 &&<View style={[styles.listTopic,{marginTop: 10}]}>
          <Text style={styles.listTopicText}>{I18n.t('mainpile.5_1.checkplummet2')}</Text>
        </View>}
        {this.props.category!=5 &&<View style={styles.listTopicSub}>
            <Text style={styles.listTopicSubText}>{I18n.t('mainpile.5_1.listchack')}</Text>
            <View style={styles.listCheckbox}>
              <View style={styles.listCheckboxBase}>
                {this.state.checkplummet == true? (
                  <Icon
                    name="check"
                    reverse
                    color="#6dcc64"
                    size={15}
                    onPress={()=>this.state.status5==2?{}: this.selectedPlummet()}
                  />
                ) : (
                  <Icon
                    name="check"
                    reverse
                    color="grey"
                    size={15}
                    onPress={()=>this.state.status5==2?{}: this.selectedPlummet()}
                  />
                )}
                <Text>{I18n.t('mainpile.5_1.checkplummet')}</Text>
              </View>
              <View style={styles.buttonImage}>
                <TouchableOpacity
                  style={[styles.image,this.state.images_plummet.length>0?{backgroundColor:'#6dcc64'}:{}]}
                  onPress={() => this.onCameraRoll("images_plummet", this.state.images_plummet,flag_edit?'edit':'view')}
                >
                  <Icon name="image" type="entypo" color="#FFF" />
                  <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.listCheckbox}>
              <View style={styles.listCheckboxBase}>
                {this.state.checkwater == true? (
                  <Icon
                    name="check"
                    reverse
                    color="#6dcc64"
                    size={15}
                    onPress={()=> this.state.status5==2?{}: this.selectedWater()}
                  />
                ) : (
                  <Icon
                    name="check"
                    reverse
                    color="grey"
                    size={15}
                    onPress={()=> this.state.status5==2?{}: this.selectedWater()}
                  />
                )}
                <Text>{I18n.t('mainpile.5_1.checkwater')}</Text>
              </View>
              {/* <TouchableOpacity activeOpacity={0.8} onPress={() => Actions.camearaRoll()}>
                <View style={[styles.listButton, styles.listButtonView]}>
                  <Text style={styles.listButtonText}>ดูภาพ</Text>
                </View>
              </TouchableOpacity> */}

              <View style={styles.buttonImage}>
                <TouchableOpacity
                  style={[styles.image,this.state.images_water.length>0?{backgroundColor:'#6dcc64'}:{}]}
                  onPress={() => this.onCameraRoll("images_water", this.state.images_water,flag_edit?'edit':'view')}
                >
                  <Icon name="image" type="entypo" color="#FFF" />
                  <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                </TouchableOpacity>
              </View>

            </View>
        </View>}

      </View>
    )
  }
}

const mapStateToProps = state => {
  return { pileAll: state.mainpile.pileAll || null }
}

export default connect(mapStateToProps, actions, null, { withRef: true })(Pile5_1)
