import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TextInput,
  Alert,
  TouchableOpacity,
  ActivityIndicator
} from "react-native"
import { connect } from "react-redux"
import { Container, Content } from "native-base"
import { Icon } from "react-native-elements"
import Carousel from "react-native-snap-carousel"
import ModalSelector from "react-native-modal-selector"
import * as actions from "../Actions"
import { MainPileFunc } from "../Components/function"
import Pile1_1 from "./Pile1_1"
import Pile1_2 from "./Pile1_2"
import { MAIN_COLOR, DEFAULT_COLOR_1, DEFAULT_COLOR_4 } from "../Constants/Color"
import PileFooter from "../Components/PileFooter"
import StepIndicator from "react-native-step-indicator"
import I18n from "../../assets/languages/i18n"
import styles from "./styles/Pile1.style"
import moment from "moment"
class Pile1 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 1,
      step: 0,
      ref: 2,
      error: null,
      data: [],
      location: {
        position: {
          lat: 1,
          log: 1
        }
      },
      Edit_Flag: 1,
      startdate:'',
      status7:0,
      status5:0,
      checksavebutton:false,
      process1_success_1_random:null,
      saveload:false,
      process1_success_3_random:null,
      statuscolor:null,
      check:false
    }
  }
  componentDidMount(){
    // console.log(this.props.stack)
    if (this.props.stack) {
      this.setState({step:this.props.stack[this.props.stack.length - 1].step - 1})
    }
  }
  async componentWillReceiveProps(nextProps) {
    if(nextProps.step_status2_random != this.state.stepstatus && nextProps.processstatus == 1){
      this.setState({stepstatus:nextProps.step_status2_random},async()=>{
        // console.warn('yes 1',nextProps.step_status2.Step1Status)
        await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step1Status: nextProps.step_status2.Step1Status })
        this.props.mainPileGetStorage(this.props.pileid, "step_status")
      })
    }
    if (nextProps.pileAll[this.props.pileid] != undefined) {
      var edit = nextProps.pileAll[this.props.pileid]
      if (edit["1_0"] != undefined) {
        await this.setState({ Edit_Flag: edit["1_0"].data.Edit_Flag })
      }
      if(edit != undefined){
        this.setState({status7:edit.step_status.Step7Status,status5:edit.step_status.Step5Status})
      }
    }
    await this.setState({
      error: nextProps.error,
      data: nextProps.pileAll[nextProps.pileid],
      location: nextProps.pileAll.currentLocation
    })
    if(nextProps.process1_success_1 == true && nextProps.process1_success_1_random != this.state.process1_success_1_random){
      this.setState({process1_success_1_random:nextProps.process1_success_1_random},async()=>{
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process:1
        })
        if(this.props.startdate != '' & this.props.startdate != null){
          this.setState({ step: 1 ,startdate: moment(this.props.startdate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY HH:mm"),saveload:false})
        }else{
          this.setState({ step: 1 ,startdate: moment().format("DD/MM/YYYY HH:mm"),saveload:false})
        }
          this.onSetStack(1,1)
      })

    }
    if(nextProps.process1_success_3 == true && nextProps.process1_success_3_random != this.state.process1_success_3_random){
      this.setState({process1_success_3_random:nextProps.process1_success_3_random},async()=>{
        this.props.getStepStatus2({
          jobid: this.props.jobid,
          pileid: this.props.pileid,
          process:1
        })
        this.props.onNextStep()
        this.onSetStack(2,0)
        this.setState({checksavebutton:false,saveload:false})
      })

    }
   
   
  }

  updateState(value) {
    const ref = "pile" + value.process + "_" + value.step
    if (this.refs[ref].getWrappedInstance()) {
      this.refs[ref].getWrappedInstance().updateState(value)
    }
  }

  setStep(step) {
    return this.setState({ step: step })
  }

  async onSetStack(pro,step) {
    // console.warn('onSetStack',pro,step)
    var data = {
      process: pro,
      step: step + 1
    }
    await this.props.setStack(data)
    await this.props.mainPileGetAllStorage()
  }

  _renderStepFooter() {
    return (
      <View>
        <View style={{ flexDirection: "column" }}>
          <View style={{ borderWidth: 1 }}>
            <StepIndicator
              stepCount={2}
              onPress={step => {
                // if (this.refs["pile1_1"].getWrappedInstance().state.loading != true) {
                  if(!this.state.saveload){
                    this.onSetStack(1,step)
                    this.setState({ step: step })
                  }
                  
                // }
              }}
              currentPosition={this.state.step}
            />
          </View>
          {
            this.state.Edit_Flag == 1 ?
            <View
              style={[
                styles.second,
                this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? { backgroundColor: MAIN_COLOR }:{ backgroundColor: DEFAULT_COLOR_1 }
              ]}
            >
              <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton} 
              // disabled={this.state.status6 == 0 ? false:true}
              >
                <Text style={styles.selectButton}>{I18n.t("mainpile.button.delete")}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.secondButton,
                  this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? { backgroundColor: MAIN_COLOR }:{ backgroundColor: DEFAULT_COLOR_1 }
                ]}
                onPress={this.nextButton}
                disabled={this.state.checksavebutton}
              >
                <Text style={styles.selectButton}>
                  {this.state.Edit_Flag == 1 ? I18n.t("mainpile.button.next") : I18n.t("mainpile.button.continue")}
                </Text>
              </TouchableOpacity>
            </View>
            :
            <View/>
          }
        </View>
      </View>
    )
  }

  nextButton = () => {
    switch (this.state.step) {
      case 0:
        this.onSave(this.state.step)
        break
      case 1:
        this.onSave(this.state.step)
        break
      default:
        break
    }
  }

  initialStep() {
    // console.log("initialStep", this.props.pileid)
    this.setState({ step: 0 })
  }

  deleteButton = () => {
    this.props.setStack({
      process: 1,
      step: 1
    })
    const data = {
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      processid: this.state.process,
      lat: this.state.location == undefined ? 1 : this.state.location.position.lat,
      log: this.state.location == undefined ? 1 : this.state.location.position.log,
    }
  // console.warn('status7',this.state.status7)
      if(this.state.status7 == 0){
        Alert.alert("Warning", I18n.t("mainpile.button.confirmdelete"), [
          { text: "Cancel" },
          { text: "OK", onPress: () => this.props.pileDelete(data) }
        ])
      }else{
        Alert.alert("", I18n.t('alert.error_delete1'), [
          {
            text: "OK"
          }
        ])
      }
    
  }
  onGetChildData(step) {
    if (this.refs["pile1_" + step].getWrappedInstance().getState()) {
      const dataChild = this.refs["pile1_" + step].getWrappedInstance().getState()
      return dataChild
    }
  }

  async onSave(step) {
    await this.props.mainPileGetAllStorage()
    if (step == 0) {
      var value = {
        process: 1,
        step: 1,
        pile_id: this.props.pileid
      }
      // await this.props.mainPileSetStorage(this.props.pileid,'1_1',value)
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value).then(async data => {
          var valueApi = {
            Language: I18n.locale,
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            Page: 1,
            ProviderId: this.state.data["1_1"].data.ProviderId,
            ProviderName: this.state.data["1_1"].data.providerName,
            latitude: this.state.location == undefined ? 1 : this.state.location.position.lat,
            longitude: this.state.location == undefined ? 1 : this.state.location.position.log
          }
          // console.warn("pile1_1", valueApi)

         
          if (this.state.error == null) {
            if (data.check == false) {
              Alert.alert("", I18n.t("mainpile.1_1.alert.select"), [
                {
                  text: "OK"
                }
              ])
              this.setState({checksavebutton:false,saveload:false})
             
            } else {
              this.setState({saveload:true,statuscolor:data.statuscolor})
              this.props.pileSaveStep01(valueApi)
              // if(this.props.startdate != '' & this.props.startdate != null){
              //   this.setState({ step: 1 ,startdate: moment(this.props.startdate,"DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY HH:mm")})
              // }else{
              //   this.setState({ step: 1 ,startdate: moment().format("DD/MM/YYYY HH:mm")})
              // }
              // this.onSetStack(1,1)
            }
          } else {
            Alert.alert(this.state.error)
            this.setState({checksavebutton:false,saveload:false})
          }
        })
      } else {
        if(this.props.startdate != '' && this.props.startdate != null){
          this.setState({ step: 1 ,startdate: this.props.startdate})
        }else{
          this.setState({ step: 1 ,startdate: moment().format("DD/MM/YYYY HH:mm")})
        }
        this.onSetStack(1,1)
      }
    } else {
      this.setState({checksavebutton:true})
      var value = {
        process: 1,
        step: 2,
        pile_id: this.props.pileid,
        shape:this.props.shape
      }
      // await this.props.mainPileSetStorage(this.props.pileid,'1_2',value)
      if (this.state.Edit_Flag == 1) {
        this.props.mainpileAction_checkStepStatus(value).then(async data => {
         
            if (data.check != false) {
          var dataSectionDetail = []
          var dataImageSteelcage = []
          var dataImageConcretespacer = []
          // console.warn('SectionDetail',this.state.data["1_2"].data.SectionDetail)
          for (var i = 0; i < this.state.data["1_2"].data.SectionDetail.length; i++) {
            dataImageSteelcage = []
            if(this.state.data["1_2"].data.SectionDetail[i].checkconcretespacer == undefined){
              if (
                this.state.data["1_2"].data.SectionDetail[i].ImageSteelcage != null &&
                this.state.data["1_2"].data.SectionDetail[i].ImageSteelcage.length > 0
              ) {
                for (var j = 0; j < this.state.data["1_2"].data.SectionDetail[i].ImageSteelcage.length; j++) {
                  dataImageSteelcage.push(this.state.data["1_2"].data.SectionDetail[i].ImageSteelcage[j])
                }
              }
              dataImageConcretespacer = []
              if (
                this.state.data["1_2"].data.SectionDetail[i].ImageConcretespacer != null &&
                this.state.data["1_2"].data.SectionDetail[i].ImageConcretespacer.length > 0
              ) {
                for (var j = 0; j < this.state.data["1_2"].data.SectionDetail[i].ImageConcretespacer.length; j++) {
                  dataImageConcretespacer.push(this.state.data["1_2"].data.SectionDetail[i].ImageConcretespacer[j])
                }
              }
  
              dataSectionDetail.push({
                sectionId: this.state.data["1_2"].data.SectionDetail[i].sectionId,
                Checksteelcage: this.state.data["1_2"].data.SectionDetail[i].Checksteelcage,
                CheckConcretespacer: this.state.data["1_2"].data.SectionDetail[i].CheckConcretespacer,
                ImageSteelcage: dataImageSteelcage,
                ImageConcretespacer: dataImageConcretespacer
              })
            }else{
              if (
                this.state.data["1_2"].data.SectionDetail[i].image_steelcage != null &&
                this.state.data["1_2"].data.SectionDetail[i].image_steelcage.length > 0
              ) {
                for (var j = 0; j < this.state.data["1_2"].data.SectionDetail[i].image_steelcage.length; j++) {
                  dataImageSteelcage.push(this.state.data["1_2"].data.SectionDetail[i].image_steelcage[j])
                }
              }
              dataImageConcretespacer = []
              if (
                this.state.data["1_2"].data.SectionDetail[i].image_concretespacer != null &&
                this.state.data["1_2"].data.SectionDetail[i].image_concretespacer.length > 0
              ) {
                for (var j = 0; j < this.state.data["1_2"].data.SectionDetail[i].image_concretespacer.length; j++) {
                  dataImageConcretespacer.push(this.state.data["1_2"].data.SectionDetail[i].image_concretespacer[j])
                }
              }
  
              dataSectionDetail.push({
                sectionId: this.state.data["1_2"].data.SectionDetail[i].section_id,
                Checksteelcage: this.state.data["1_2"].data.SectionDetail[i].checksteelcage,
                CheckConcretespacer: this.state.data["1_2"].data.SectionDetail[i].checkconcretespacer,
                ImageSteelcage: dataImageSteelcage,
                ImageConcretespacer: dataImageConcretespacer
              })
            }
            
          }
          if(this.state.data["1_2"].enddate != ''){
            var end = this.state.data["1_2"].enddate
            var valueend  = {
              process:1,
              step:2,
              pile_id: this.props.pileid,
              data:{
                SectionDetail:dataSectionDetail,
              },
              startdate:this.state.data["1_2"].startdate,
              enddate:end
            }
            await this.props.mainPileSetStorage(this.props.pileid,'1_2',valueend)

          }else{
            var end = moment().format("DD/MM/YYYY HH:mm")
            var valueend  = {
              process:1,
              step:2,
              pile_id: this.props.pileid,
              data:{
                SectionDetail:this.state.data["1_2"].data.SectionDetail,
              },
              startdate:this.state.data["1_2"].startdate,
              enddate:end
            }
            await this.props.mainPileSetStorage(this.props.pileid,'1_2',valueend)
          }
          var start = moment(this.state.data["1_2"].startdate,"DD/MM/YYYY HH:mm")
          var end1 = moment(end,"DD/MM/YYYY HH:mm")
          if(this.state.data["1_2"].startdate != undefined){
            if(start <= end1){
              
              var valueApi = {
                Language: I18n.locale,
                JobId: this.props.jobid,
                PileId: this.props.pileid,
                Page: 3,
                SectionDetail: dataSectionDetail,
                latitude: this.state.location == undefined ? 1 : this.state.location.position.lat,
                longitude: this.state.location == undefined ? 1 : this.state.location.position.log,
                startdate: moment(this.state.data["1_2"].startdate,"DD/MM/YYYY HH:mm").format("YYYY-MM-DD HH:mm:ss"),
                enddate: moment(end,"DD/MM/YYYY HH:mm").format("YYYY-MM-DD HH:mm:ss")
              }
              if (this.state.error == null) {
                if (data.check == false) {
                  // console.warn("20",data.statuscolor)
                  Alert.alert("", data.error, [
                    {
                      text: "OK"
                    }
                  ])
                  this.setState({checksavebutton:false,saveload:false})
                 
                } else {
                  this.setState({saveload:true,statuscolor:data.statuscolor})
                  this.props.pileSaveStep01(valueApi)
                  // this.props.onNextStep()
                  // this.onSetStack(2,0)
                  // setTimeout(()=>{this.setState({checksavebutton:false})},2000)
                }
              } else {
                Alert.alert(this.state.error)
                this.setState({checksavebutton:false,saveload:false})
              }
              // if (dataSectionDetail.length > 0) {
                // this.props.pileSaveStep01(valueApi)
              // }
            }else{
              // console.warn("20")
              // var valueApi = {
              //   Language: I18n.locale,
              //   JobId: this.props.jobid,
              //   PileId: this.props.pileid,
              //   Page: 2,
              //   SectionDetail: dataSectionDetail,
              //   latitude: this.state.location == undefined ? 1 : this.state.location.position.lat,
              //   longitude: this.state.location == undefined ? 1 : this.state.location.position.log,
              // }
              Alert.alert("", I18n.t("mainpile.liquidtestinsert.alert.error4"), [
                {
                  text: "OK"
                }
              ])
              this.setState({checksavebutton:false})
            }
          }else{
            if(this.props.startdate <= this.props.enddate){
              var valueApi = {
                Language: I18n.locale,
                JobId: this.props.jobid,
                PileId: this.props.pileid,
                Page: 3,
                SectionDetail: dataSectionDetail,
                latitude: this.state.location == undefined ? 1 : this.state.location.position.lat,
                longitude: this.state.location == undefined ? 1 : this.state.location.position.log,
                startdate: moment(this.props.startdate,"DD/MM/YYYY HH:mm:ss").format("YYYY-MM-DD HH:mm:ss"),
                enddate: moment(this.props.enddate,"DD/MM/YYYY HH:mm:ss").format("YYYY-MM-DD HH:mm:ss")
              }
              if (this.state.error == null) {
                if (data.check == false) {
                  Alert.alert("", data.error, [
                    {
                      text: "OK"
                    }
                  ])
                  this.setState({checksavebutton:false,saveload:false})
                  
                } else {
                  this.props.onNextStep()
                  this.onSetStack(2,0)
                  setTimeout(()=>{this.setState({checksavebutton:false})},2000)
                }
              } else {
                Alert.alert(this.state.error)
                this.setState({checksavebutton:false,saveload:false})
              }
    
                this.props.pileSaveStep01(valueApi)
         
            }else{
             
              Alert.alert("", I18n.t("mainpile.4_1.error_step"), [
                {
                  text: "OK"
                }
              ])
              this.setState({checksavebutton:false})
            }
          }
        }else{
          Alert.alert("", data.error, [
            {
              text: "OK"
            }
          ])
          this.setState({checksavebutton:false})
          
        }   
        })
      
      } else {
        this.props.onNextStep()
        this.onSetStack(2,0)
        setTimeout(()=>{this.setState({checksavebutton:false})},2000)
      }
    }
  }
  async onNotPass(i) {
    await this.setState({ check: i })
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    // console.warn('render',this.state.saveload)
    return (
      <View style={{ flex: 1 }}>
      {this.state.saveload?<Content><ActivityIndicator size="large" color="#007CC2" /></Content>:<View/>}
      {!this.state.saveload?
        <Content>
          <Pile1_1
            ref="pile1_1"
            hidden={!(this.state.step == 0)}
            jobid={this.props.jobid}
            pileid={this.props.pileid}
            onRefresh={this.props.onRefresh}
            category={this.props.category}
          />
          <Pile1_2
            ref="pile1_2"
            hidden={!(this.state.step == 1)}
            jobid={this.props.jobid}
            pileid={this.props.pileid}
            shape={this.props.shape}
            category={this.props.category}
            startdate={this.props.startdate}
            startdatedefault={this.state.startdate}
            enddate={this.props.enddate}
          />
        </Content>
        :<View/>
      }
        {this._renderStepFooter()}
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    error: state.pile.error,
    pileAll: state.mainpile.pileAll || null,
    stack: state.mainpile.stack_item,
    process1_success_1:state.pile.process1_success_1,
    process1_success_1_random:state.pile.process1_success_1_random,
    process1_success_3:state.pile.process1_success_3,
    process1_success_3_random:state.pile.process1_success_3_random,

    step_status2_random:state.mainpile.step_status2_random,
    step_status2:state.mainpile.step_status2,
    processstatus:state.mainpile.process
  }
}

export default connect(mapStateToProps, actions, null, { withRef: true })(Pile1)
