import React, {Component} from "react"
import {
  Alert,
  Text,
  View,
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  BackHandler,
  DatePickerAndroid,
  TimePickerAndroid,
} from "react-native"
import {connect} from "react-redux"
import {Container, Content} from "native-base"
import {Actions} from 'react-native-router-flux'
import * as actions from "../Actions"
import TableView from "../Components/TableView"
import {
  MAIN_COLOR,
  SUB_COLOR,
  BASIC_COLOR,
  BLUE_COLOR,
  MENU_GREY_ITEM,
  BUTTON_COLOR,
  DEFAULT_COLOR_1,
} from "../Constants/Color"
import {Icon} from "react-native-elements"
import I18n from '../../assets/languages/i18n'
import ModalSelector from "react-native-modal-selector"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import styles from './styles/Pile5_4.style'
import moment from 'moment'
import DateTimePicker from '@react-native-community/datetimepicker';
class Pile5_4 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 5,
      step: 4,
      location:{
        position:{
          lat:1,
          log:1,
        }
      },
      Edit_Flag:1,
      loading:false,

      StartDrillingFluidDate:'',
      EndDrillingFluidDate:'',

      StartCheckstopend:'',
      EndCheckstopend:'',

      CheckStopEnd:false,
      CheckWaterStop:false,
      CheckInstallStopEnd:false,
      CheckDrillingFluid:false,
      ImageCheckStopEnd:[],
      ImageCheckWaterStop:[],
      ImageCheckInstallStopEnd:[],
      ImageCheckDrillingFluid:[],
      dataPile5_3:[],
      dataPile5_4:[],
      dataMaster:[],
      datevisible:false,
      Type:'',
      datevisibledate:false,
      datevisibletime:false,
      datevisibledate1:false,
      datevisibletime1:false,

      StartDrillingFluidDate_date:'',
      StartDrillingFluidDate_time:'',
      EndDrillingFluidDate_date:'',
      EndDrillingFluidDate_time:'',

      StartCheckstopend_date:'',
      StartCheckstopend_time:'',
      EndCheckstopend_date:'',
      EndCheckstopend_time:'',
      set:false,

      waterstopTypelist:[{ key: 0, section: true, label: I18n.t("mainpile.5_4.selectWaterstop") }],
      waterstopType:'',
      waterstopTypeID:'',

      waterstoplenght:''
    }
  }


  async componentWillReceiveProps(nextProps){
    var pile = null
    var pile5_3 = null
    var pile5_0 = null
    var pileMaster = null

    if(this.props.pileAll[this.props.pileid] && this.props.pileAll[this.props.pileid]['5_3']) {
      pile5_3 = nextProps.pileAll[this.props.pileid]['5_3'].data
      this.setState({dataPile5_3:pile5_3})
    }

    if(this.props.pileAll[this.props.pileid] && this.props.pileAll[this.props.pileid]['5_0']) {
      pile5_0 = nextProps.pileAll[this.props.pileid]['5_0'].data
    }
    if(this.props.pileAll[this.props.pileid] && this.props.pileAll[this.props.pileid]['5_4']) {
      pile = nextProps.pileAll[this.props.pileid]['5_4'].data
      pileMaster = nextProps.pileAll[this.props.pileid]["5_4"].masterInfo
    }
    if (pile5_0) {
      console.log('Edit_Flag step5',pile5_0.Edit_Flag)
      // if (pile3_0.Edit_Flag != null && pile3_0.Edit_Flag != undefined && pile3_0.Edit_Flag != '') {
        this.setState({Edit_Flag:pile5_0.Edit_Flag})
      // }
    }
    // console.log(nextProps.hidden);
    if (pile && nextProps.hidden == false) {
      if (pile.WaterStopLength != null && pile.WaterStopLength != undefined) {
        this.setState({waterstoplenght: isNaN(parseFloat(pile.WaterStopLength).toFixed(3))?'':parseFloat(pile.WaterStopLength).toFixed(3)})
     }else if(pile.WaterStopLength == null) {
       this.setState({waterstoplenght:''})
     }
      if (pile.CheckStopEnd != null && pile.CheckStopEnd != undefined) {
         this.setState({CheckStopEnd: pile.CheckStopEnd})
      }else if(pile.CheckStopEnd == null) {
        this.setState({CheckStopEnd:false})
      }
      if (pile.CheckWaterStop != null && pile.CheckWaterStop != undefined) {
          this.setState({CheckWaterStop: pile.CheckWaterStop})
      }else if(pile.CheckWaterStop == null) {
        this.setState({CheckWaterStop:false})
      }
      if (pile.CheckInstallStopEnd != null && pile.CheckInstallStopEnd != undefined) {
          this.setState({CheckInstallStopEnd: pile.CheckInstallStopEnd})
      }else if(pile.CheckInstallStopEnd == null) {
        this.setState({CheckInstallStopEnd:false})
      }
      if (pile.CheckDrillingFluid != null && pile.CheckDrillingFluid != undefined) {
          this.setState({CheckDrillingFluid: pile.CheckDrillingFluid})
      }else if(pile.CheckDrillingFluid == null) {
        this.setState({CheckDrillingFluid:false})
      }
      if (pile.ImageCheckStopEnd != null && pile.ImageCheckStopEnd != undefined) {
          this.setState({ImageCheckStopEnd: pile.ImageCheckStopEnd})
      }else if(pile.ImageCheckStopEnd == null) {
        this.setState({ImageCheckStopEnd:[]})
      }
      if (pile.ImageCheckWaterStop != null && pile.ImageCheckWaterStop != undefined) {
          this.setState({ImageCheckWaterStop: pile.ImageCheckWaterStop})
      }else if(pile.ImageCheckWaterStop == null) {
        this.setState({ImageCheckWaterStop:[]})
      }
      if (pile.ImageCheckInstallStopEnd != null && pile.ImageCheckInstallStopEnd != undefined) {
          this.setState({ImageCheckInstallStopEnd: pile.ImageCheckInstallStopEnd})
      }else if(pile.ImageCheckInstallStopEnd == null) {
        this.setState({ImageCheckInstallStopEnd:[]})
      }
      if (pile.ImageCheckDrillingFluid != null && pile.ImageCheckDrillingFluid != undefined) {
          this.setState({ImageCheckDrillingFluid: pile.ImageCheckDrillingFluid})
      }else if(pile.ImageCheckDrillingFluid == null) {
        this.setState({ImageCheckDrillingFluid:[]})
      }
      if (pile.StartDrillingFluidDate != null && pile.StartDrillingFluidDate != undefined && pile.StartDrillingFluidDate!= ''&& this.state.set == false) {
          this.setState({StartDrillingFluidDate: pile.StartDrillingFluidDate,
            StartDrillingFluidDate_date:moment(pile.StartDrillingFluidDate,'DD/MM/YYYY HH:mm:ss').format('DD/MM/YYYY'), 
            StartDrillingFluidDate_time:moment(pile.StartDrillingFluidDate,'DD/MM/YYYY HH:mm:ss').format('HH:mm')
          })
      }else if(pile.StartDrillingFluidDate == null) {
        this.setState({StartDrillingFluidDate:''})
      }
      if (pile.EndDrillingFluidDate != null && pile.EndDrillingFluidDate != undefined && pile.EndDrillingFluidDate != '' && this.state.set == false) {
          this.setState({EndDrillingFluidDate: pile.EndDrillingFluidDate,
            EndDrillingFluidDate_date:moment(pile.EndDrillingFluidDate,'DD/MM/YYYY HH:mm:ss').format('DD/MM/YYYY'), 
            EndDrillingFluidDate_time:moment(pile.EndDrillingFluidDate,'DD/MM/YYYY HH:mm:ss').format('HH:mm')
          })
      }else if(pile.EndDrillingFluidDate == null) {
        this.setState({EndDrillingFluidDate:''})
      }

      if (pile.StartStopEndDate != null && pile.StartStopEndDate != undefined && pile.StartStopEndDate!= ''&& this.state.set == false) {
        this.setState({StartCheckstopend: pile.StartStopEndDate,
          StartCheckstopend_date:moment(pile.StartStopEndDate,'DD/MM/YYYY HH:mm:ss').format('DD/MM/YYYY'), 
          StartCheckstopend_time:moment(pile.StartStopEndDate,'DD/MM/YYYY HH:mm:ss').format('HH:mm')
        })
    }else if(pile.StartStopEndDate == null) {
      this.setState({StartCheckstopend:''})
    }
    if (pile.EndStopEndDate != null && pile.EndStopEndDate != undefined && pile.EndStopEndDate != '' && this.state.set == false) {
        this.setState({EndCheckstopend: pile.EndStopEndDate,
          EndCheckstopend_date:moment(pile.EndStopEndDate,'DD/MM/YYYY HH:mm:ss').format('DD/MM/YYYY'), 
          EndCheckstopend_time:moment(pile.EndStopEndDate,'DD/MM/YYYY HH:mm:ss').format('HH:mm')
        })
    }else if(pile.EndStopEndDate == null) {
      this.setState({EndCheckstopend:''})
    }
    // console.warn('WaterStopList',pile)
    if (this.state.waterstopTypelist != pileMaster.WaterStopList &&  pileMaster.WaterStopList != '' && this.state.waterstopTypelist.length == 1  ) {
      let list = [{ key: 0, section: true, label: I18n.t("mainpile.5_4.selectWaterstop")  }]
      for (var i = 0; i < pileMaster.WaterStopList.length; i++) {
        if (pile) {
          if (pileMaster.WaterStopList[i].waterstopid == pile.waterstopid) {
            this.setState({
              waterstopType: pileMaster.WaterStopList[i].waterstopname ,
              waterstopTypeID: pile.waterstopid,
            })
          }
        }
        await list.push({
          key: pileMaster.WaterStopList[i].waterstopid,
          label: pileMaster.WaterStopList[i].waterstopname,
          id: pileMaster.WaterStopList[i].waterstopid
        })
      }
      await this.setState({waterstopTypelist:list})
    }



    }
  }
  async onCheckStopEnd(){
    var flag_edit = true
    if(this.props.category == 5){
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
      if(this.props.parent == false){
        flag_edit = false
      }
    }else{
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
    }
    if (flag_edit) {
      await this.setState({ CheckStopEnd: !this.state.CheckStopEnd })
      this.saveLocal('5_4')
    }
  }
  async onCheckWaterStop(){
    var flag_edit = true
    if(this.props.category == 5){
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
      if(this.props.parent == false){
        flag_edit = false
      }
    }else{
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
    }
    if (flag_edit) {
      await this.setState({ CheckWaterStop: !this.state.CheckWaterStop })
      this.saveLocal('5_4')
    }
  }
  async onCheckInstallStopEnd(){
    var flag_edit = true
    if(this.props.category == 5){
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
      if(this.props.parent == false){
        flag_edit = false
      }
    }else{
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
    }
    if (flag_edit) {
      await this.setState({ CheckInstallStopEnd: !this.state.CheckInstallStopEnd })
      this.saveLocal('5_4')
    }
  }
  async onCheckDrillingFluid(){
    var flag_edit = true
    if(this.props.category == 5){
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
      if(this.props.parent == false){
        flag_edit = false
      }
    }else{
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
    }
    if (flag_edit) {
      await this.setState({ CheckDrillingFluid: !this.state.CheckDrillingFluid })
      this.saveLocal('5_4')
    }
  }

  async updateState(value) {
    console.log(value)
    if (value.data.ImageCheckStopEnd != undefined) {
       await this.setState({ImageCheckStopEnd:value.data.ImageCheckStopEnd})
       this.saveLocal('5_4')
    }
    if (value.data.ImageCheckWaterStop != undefined) {
       await this.setState({ImageCheckWaterStop:value.data.ImageCheckWaterStop})
       this.saveLocal('5_4')
    }
    if (value.data.ImageCheckInstallStopEnd != undefined) {
       await this.setState({ImageCheckInstallStopEnd:value.data.ImageCheckInstallStopEnd})
       this.saveLocal('5_4')
    }
    if (value.data.ImageCheckDrillingFluid != undefined) {
       await this.setState({ImageCheckDrillingFluid:value.data.ImageCheckDrillingFluid})
       this.saveLocal('5_4')
    }
  }
  async mixdate(){
    if( this.state.StartDrillingFluidDate_date != '' && this.state.StartDrillingFluidDate_time != ''){
     await this.setState({StartDrillingFluidDate:this.state.StartDrillingFluidDate_date+' '+this.state.StartDrillingFluidDate_time})
    }
    if(this.state.EndDrillingFluidDate_date != '' && this.state.EndDrillingFluidDate_time != ''){
      await this.setState({EndDrillingFluidDate:this.state.EndDrillingFluidDate_date+' '+this.state.EndDrillingFluidDate_time})
    }

    if( this.state.StartCheckstopend_date != '' && this.state.StartCheckstopend_time != ''){
      await this.setState({StartCheckstopend:this.state.StartCheckstopend_date+' '+this.state.StartCheckstopend_time})
     }
     if(this.state.EndCheckstopend_date != '' && this.state.EndCheckstopend_time != ''){
       await this.setState({EndCheckstopend:this.state.EndCheckstopend_date+' '+this.state.EndCheckstopend_time})
     }
  }
  async saveLocal(process_step){
    await this.mixdate()
    var value = {
      process:5,
      step:4,
      pile_id: this.props.pileid,
      data:{
        StartDrillingFluidDate:this.state.StartDrillingFluidDate,
        EndDrillingFluidDate:this.state.EndDrillingFluidDate,
        CheckStopEnd:this.state.CheckStopEnd,
        CheckWaterStop:this.state.CheckWaterStop,
        CheckInstallStopEnd:this.state.CheckInstallStopEnd,
        CheckDrillingFluid:this.state.CheckDrillingFluid,
        ImageCheckStopEnd:this.state.ImageCheckStopEnd,
        ImageCheckWaterStop:this.state.ImageCheckWaterStop,
        ImageCheckInstallStopEnd:this.state.ImageCheckInstallStopEnd,
        ImageCheckDrillingFluid:this.state.ImageCheckDrillingFluid,
        StartStopEndDate:this.state.StartCheckstopend,
        EndStopEndDate:this.state.EndCheckstopend,
        waterstopid:this.state.waterstopTypeID,
        WaterStopTypeName: this.state.waterstopType,
        WaterStopLength:this.state.waterstoplenght
      }
    }
    // console.warn('saveLocal',value)
    await this.props.mainPileSetStorage(this.props.pileid,process_step,value)
  }

  onCameraRoll(keyname,photos,type){
    console.log('onCameraRoll step 5,',this.props.parent)
    var flag_edit = true
    if(this.props.category == 5){
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
      if(this.props.parent == false){
        flag_edit = false
      }
    }else{
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
    }
    if (flag_edit) {
      Actions.camearaRoll({jobid:this.props.jobid,process:this.state.process,step:this.state.step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:type,shape:this.props.shape,category:this.props.category,Edit_Flag:this.state.Edit_Flag})
    }else {
      Actions.camearaRoll({jobid:this.props.jobid,process:this.state.process,step:this.state.step,pileId:this.props.pileid,keyname:keyname,photos:photos,typeViewPhoto:'view',shape:this.props.shape,category:this.props.category,Edit_Flag:this.state.Edit_Flag})
    }
  }

  setTime(type){
    // if (this.state.drilltype == '') {
    //   Alert.alert('กรุณาตรวจสอบการ แซะ Stop End ของ Panel ก่อนหน้าแล้ว')
    // }else if(this.state.machine == ''){
    //   Alert.alert('กรุณาตรวจสอบการ ติดตั้ง Water Stop เข้าไปใน Stop End')
    // }else if (this.state.driver == '') {
    //   Alert.alert('กรุณาตรวจสอบการ ติดตั้ง Stop End เข้าไปในแผง D-Wall')
    // }else if (this.state.driver == '') {
    //   Alert.alert('กรุณาตรวจสอบการ เปลี่ยนน้ำยาหลังเจาะเสร็จแล้ว')
    // }else {
      var title = ''
      if (type == 'start') {
        title = I18n.t('mainpile.5_4.startliquidchange')
      }else {
        title = I18n.t('mainpile.5_4.endliquidchange')
      }
      Alert.alert('', title, [
        {
          text: 'Cancel'
        },
        {
          text: 'OK',
          onPress: ()=>{
            var now = moment().format("DD/MM/YYYY HH:mm:ss")
            if (type == 'start') {
              this.setState({StartDrillingFluidDate:now})
              this.saveLocal('5_4')
            }else {
              if (this.state.EndDrillingFluidDate == '') {
                this.setState({EndDrillingFluidDate:now})
                this.saveLocal('5_4')
              }
            }
          }
        }
      ])
    // }
  }

  // async setCalendar(type){
  //   var month =  moment().format('M')
  //   month = month != 1 ? month : parseInt(month) - 1
  //   try {
  //     const {action, year, month, day} = await DatePickerAndroid.open({
  //       // Use `new Date()` for current date.
  //       // May 25 2020. Month 0 is January.
  //       date: new Date(moment().format('YYYY'),month,moment().format('D'))
  //     })
  //     if (action !== DatePickerAndroid.dismissedAction) {
  //       // Selected year, month (0-11), day
  //       var date = new Date(year, month, day)
  //       date = moment(date).format("DD/MM/YYYY")

  //       try {
  //           const {action, hour, minute} = await TimePickerAndroid.open({
  //             is24Hour: true,
  //           })
  //           if (action !== TimePickerAndroid.dismissedAction) {
  //             // Selected hour (0-23), minute (0-59) datavalue

  //             if (type == 'start') {
  //               // this.setState({StartDrillingFluidDate:date +' '+moment(hour+':'+minute,'HH:mm').format("HH:mm:ss")}
  //               this.onCalDate(date +' '+moment(hour+':'+minute,'HH:mm').format("HH:mm:ss"),type)

  //             }else {
  //               // this.setState({EndDrillingFluidDate:date +' '+moment(hour+':'+minute,'HH:mm').format("HH:mm:ss")}
  //               this.onCalDate(date +' '+moment(hour+':'+minute,'HH:mm').format("HH:mm:ss"),type)
  //             }
  //           }
  //         } catch ({code, message}) {
  //           console.warn('Cannot open time picker', message)
  //         }
  //     }
  //   } catch ({code, message}) {
  //     console.warn('Cannot open date picker', message)
  //   }
  // }

  onCalDate(date,type){
    if (this.state.dataPile5_3.DrillingList.length  > 0 ) {
      var index = this.state.dataPile5_3.DrillingList.length - 1
      if (type == 'start') {
        
          var a = moment(date,'DD-MM-YYYY HH:mm:ss')
          var b = moment(this.state.dataPile5_3.DrillingList[index].StartDrilling,'DD-MM-YYYY HH:mm:ss')
          var check = false
          //after
          if (a.diff(b, 'years',true) >= 0 && a.diff(b, 'months',true) >=0 && b.diff(a, 'days',true) <= 0 && b.diff(a, 'hours',true) <= 0 && b.diff(a, 'minutes',true) <= 0) {
            if (this.state.EndDrillingFluidDate != '') {
              var c = moment(this.state.EndDrillingFluidDate,'DD-MM-YYYY HH:mm:ss')
              //before
              if (a.diff(c, 'years',true) <= 0 && a.diff(c, 'months',true) <=0 && c.diff(a, 'days',true) >= 0 && c.diff(a, 'hours',true) >= 0 && c.diff(a, 'minutes',true) >= 0) {
                check = true
                this.setState({StartDrillingFluidDate:date})
              }
            }else {
              var c = moment(this.state.dataPile5_3.DrillingList[index].EndDrilling,'DD-MM-YYYY HH:mm:ss')
              if (a.diff(c, 'years',true) >= 0 && a.diff(c, 'months',true) >=0 && c.diff(a, 'days',true) <= 0 && c.diff(a, 'hours',true) <= 0 && c.diff(a, 'minutes',true) <= 0) {
                check = true
                this.setState({StartDrillingFluidDate:date})
              }else {
                check = false
                this.setState({StartDrillingFluidDate:date})
              }
            }
          }

          if (check == false) {
            Alert.alert('', I18n.t('mainpile.5_1.error_step'), [
              {
                text: 'OK'
              }
            ])
          }
        }else {
          var a = moment(date,'DD-MM-YYYY HH:mm:ss')
          var b = moment(this.state.StartDrillingFluidDate,'DD-MM-YYYY HH:mm:ss')
          var check = false
          if (a.diff(b, 'years',true) >= 0 && a.diff(b, 'months',true) >=0 && b.diff(a, 'days',true) <= 0 && b.diff(a, 'hours',true) <= 0 && b.diff(a, 'minutes',true) <= 0) {
              check = true
              this.setState({EndDrillingFluidDate:date})
          }
          if (check == false) {
            Alert.alert('', I18n.t('mainpile.5_1.error_step'), [
              {
                text: 'OK'
              }
            ])
          }
        }
    }else {
        if (type == 'start') {
          this.setState({StartDrillingFluidDate:date})
        }else {
          this.setState({EndDrillingFluidDate:date})
        }

      }
      this.saveLocal('5_4')
  }



  renderlistenerButton(){
    if (this.state.StartDrillingFluidDate == '' && this.state.EndDrillingFluidDate == '') {
      return(
        <TouchableOpacity activeOpacity={0.8} onPress={() => this.setTime('start')} >
          <View style={styles.buttonStart}>
            <Image source={require('../../assets/image/icon_play.png')}/>
          </View>
        </TouchableOpacity>
      )
    }else if (this.state.StartDrillingFluidDate != '' && this.state.EndDrillingFluidDate == '' ) {
      return(
        <TouchableOpacity activeOpacity={0.8} onPress={() => this.setTime('end')} style={styles.listButtonCenter}>
          <View style={styles.buttonStop}>
            <Image source={require('../../assets/image/icon_stop.png')}/>
          </View>
        </TouchableOpacity>
      )
    }
  }
  setCalendar2(date,type){
    let date2 = moment(date).format("DD/MM/YYYY HH:mm:ss")
    if (type == 'start') {
      this.onCalDate(date2,type)
    }else {
      this.onCalDate(date2,type)
    }
  }
  getTime(){
    if(this.state.Type == 'start'){
      if(this.state.StartDrillingFluidDate != ''){
        return new Date(moment(this.state.StartDrillingFluidDate,'DD/MM/YYYY HH:mm:ss').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        return new Date()
      }
    }else{
      if(this.state.EndDrillingFluidDate != ''){
        return new Date(moment(this.state.EndDrillingFluidDate,'DD/MM/YYYY HH:mm:ss').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        return new Date()
      }
    }
  }

  getTime1(){
    if(this.state.Type == 'start'){
      if(this.state.StartCheckstopend != ''){
        return new Date(moment(this.state.StartCheckstopend,'DD/MM/YYYY HH:mm:ss').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        return new Date()
      }
    }else{
      if(this.state.EndCheckstopend != ''){
        return new Date(moment(this.state.EndCheckstopend,'DD/MM/YYYY HH:mm:ss').format('MM/DD/YYYY HH:mm:ss'))
      }else{
        return new Date()
      }
    }
  }
  renderTime(){
    var flag_edit = true
    if(this.props.category == 5){
      if(this.props.parent == false){
        flag_edit = false
      }
    }else{
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
    }
    return(
    <View style={[styles.listTime, styles.listTimeLeft,{borderBottomWidth:1,borderColor:'#ccc',paddingBottom:15}]}>
    <DateTimePicker
          date={this.getTime()}
              isVisible={this.state.datevisible}
              onConfirm={(date) => { 
                this.setState({ datevisible: false },
                  this.setCalendar2(date,this.state.Type)
                )
              }}
              onCancel={() => this.setState({ datevisible: false })}
              datePickerModeAndroid="spinner"
              timePickerModeAndroid="spinner"
              mode='datetime'
            />
      <Text style={{marginLeft:15}}>{I18n.t('mainpile.5_4.titletime')}</Text>
      {
        this.state.StartDrillingFluidDate == '' ? <Text></Text> :
        <View style={[styles.listTimeRow, styles.rowStart]}>
            <Text>{I18n.t('mainpile.5_4.timestart')}</Text>
            {/* <Text>{this.state.StartDrillingFluidDate != '' ?  this.state.StartDrillingFluidDate : ''}</Text> */}
            <View style={styles.listTimeCol}>
              <Text>{moment(this.state.StartDrillingFluidDate,'DD/MM/YYYY HH:mm::ss').format('DD/MM/YYYY')}</Text>
              <Text>{moment(this.state.StartDrillingFluidDate,'DD/MM/YYYY HH:mm::ss').format('HH:mm:ss')}</Text>
            </View>
          <TouchableOpacity activeOpacity={0.8} onPress={()=> this.setState({datevisible:true,Type:'start'})}>
            {
              flag_edit ?
              <View style={styles.buttonEdit}>
                <Image source={require('../../assets/image/icon_edit.png')}/>
                <Text style={styles.buttonEditText}>{I18n.t('mainpile.5_4.edit')}</Text>
              </View>
              :
              <View/>
            }
          </TouchableOpacity>
        </View>
      }

      {
        this.state.EndDrillingFluidDate == '' ? <Text></Text> :
        <View style={styles.listTimeRow}>
            <Text>{I18n.t('mainpile.5_4.timeend')}</Text>
            {/* <Text>{this.state.EndDrillingFluidDate != '' ? this.state.EndDrillingFluidDate : ''}</Text> */}
            <View style={styles.listTimeCol}>
              <Text>{moment(this.state.EndDrillingFluidDate,'DD/MM/YYYY HH:mm::ss').format('DD/MM/YYYY')}</Text>
              <Text>{moment(this.state.EndDrillingFluidDate,'DD/MM/YYYY HH:mm::ss').format('HH:mm:ss')}</Text>
            </View>
          <TouchableOpacity activeOpacity={0.8} onPress={()=>this.setState({datevisible:true,Type:'end'})}>
            {
              flag_edit ?
              <View style={styles.buttonEdit}>
                <Image source={require('../../assets/image/icon_edit.png')}/>
                <Text style={styles.buttonEditText}>{I18n.t('mainpile.5_4.edit')}</Text>
              </View>
              :
              <View/>
            }
          </TouchableOpacity>
        </View>
      }
      <View style={{flex:1 ,width: '100%',alignItems:'center'}}>
        {this.renderlistenerButton()}
      </View>
    </View>)
}
onSelectDate(date,type){
  let date2 = moment(date).format("DD/MM/YYYY")
  // console.warn('type',type)
  if(type == 'DrillingFluid'){
    if(this.state.Type=='start'){
      this.setState({ StartDrillingFluidDate_date: date2 ,set:true,datevisibledate: false,datevisibledate1: false})
      this.saveLocal('5_4')
    }else if(this.state.Type=='end'){
      this.setState({ EndDrillingFluidDate_date: date2 ,set:true,datevisibledate: false,datevisibledate1: false})
      this.saveLocal('5_4')
    }
  }else{
    // console.warn('checkstop')
    if(this.state.Type=='start'){
      this.setState({ StartCheckstopend_date: date2 ,set:true,datevisibledate: false,datevisibledate1: false})
      this.saveLocal('5_4')
    }else if(this.state.Type=='end'){
      this.setState({ EndCheckstopend_date: date2 ,set:true,datevisibledate: false,datevisibledate1: false})
      this.saveLocal('5_4')
    }
  }
  
}
onSelectTime(date,type){
  let date2 = moment(date).format("HH:mm")
  
  if(type == 'DrillingFluid'){
    if(this.state.Type=='start'){
      this.setState({ StartDrillingFluidDate_time: date2 ,set:true,datevisibletime: false,datevisibletime1: false})
      this.saveLocal('5_4')
    }else if(this.state.Type=='end'){
      this.setState({ EndDrillingFluidDate_time: date2 ,set:true,datevisibletime: false,datevisibletime1: false})
      this.saveLocal('5_4')
    }
  }else{
    if(this.state.Type=='start'){
      this.setState({ StartCheckstopend_time: date2 ,set:true,datevisibletime: false,datevisibletime1: false})
      this.saveLocal('5_4')
    }else if(this.state.Type=='end'){
      this.setState({ EndCheckstopend_time: date2 ,set:true,datevisibletime: false,datevisibletime1: false})
      this.saveLocal('5_4')
    }
  }
}

onChangedateDrillingFluid = (event, selectedDate) => {
  if(event.type == 'set'){
    this.onSelectDate(selectedDate,'DrillingFluid')
  }else{
    this.setState({datevisibledate: false,datevisibletime: false,datevisibledate1: false,datevisibletime1: false})
  }
};
onChangetimeDrillingFluid = (event, selectedDate) => {
  if(event.type == 'set'){
    this.onSelectTime(selectedDate,'DrillingFluid')
  }else{
    this.setState({datevisibledate: false,datevisibletime: false,datevisibledate1: false,datevisibletime1: false})
  }
};

onChangedateStopEnd = (event, selectedDate) => {
  if(event.type == 'set'){
    this.onSelectDate(selectedDate,'StopEnd')
  }else{
    this.setState({datevisibledate: false,datevisibletime: false,datevisibledate1: false,datevisibletime1: false})
  }
};
onChangetimeStopEnd = (event, selectedDate) => {
  if(event.type == 'set'){
    this.onSelectTime(selectedDate,'StopEnd')
  }else{
    this.setState({datevisibledate: false,datevisibletime: false,datevisibledate1: false,datevisibletime1: false})
  }
};
  renderTimeCheckDrillingFluid(){
    var flag_edit = true
    if(this.props.category == 5){
      if(this.props.parent == false){
        flag_edit = false
      }
    }else{
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
    }
      return(
      <View style={[styles.listTime, styles.listTimeLeft,{borderBottomWidth:1,borderColor:'#ccc',paddingBottom:15}]}>
        {this.state.datevisibledate &&<DateTimePicker
          value={this.getTime()}
          isVisible={this.state.datevisibledate}
          is24Hour={true}
          display='spinner'
          mode='date'
          onChange={this.onChangedateDrillingFluid}
        />}
        {this.state.datevisibletime && <DateTimePicker
          value={this.getTime()}
          isVisible={this.state.datevisibletime}
          is24Hour={true}
          display='spinner'
          mode='time'
          onChange={this.onChangetimeDrillingFluid}
        />}
        <Text style={{marginLeft:15}}>{I18n.t('mainpile.5_4.titletime')}</Text>
        <View style={[styles.imageDetail,{marginLeft:20,marginRight:20,marginBottom:15}]}>
            <Text style={{width:'25%'}}>{I18n.t('startenddate.start')}</Text>
            <TouchableOpacity
              style={[styles.buttonBox, { width: "35%" }]}
              onPress={() => {
                this.setState({datevisibledate:true,Type:'start'})
              }}
              disabled={flag_edit == false}
            >
              <Text>{this.state.StartDrillingFluidDate_date}</Text>
            </TouchableOpacity>
            <Text style={{width:'10%',textAlign:'center'}}>{I18n.t('startenddate.time')}</Text>
            <TouchableOpacity
              style={[styles.buttonBox, { width: "30%" }]}
              onPress={() => {
                this.setState({datevisibletime:true,Type:'start'})
              }}
              disabled={flag_edit == false}
            >
              <Text>{this.state.StartDrillingFluidDate_time}</Text>
            </TouchableOpacity>
          </View>
          <View style={[styles.imageDetail,{marginLeft:20,marginRight:20,marginBottom:15}]}>
          <Text style={{width:'25%'}}>{I18n.t('startenddate.end')}</Text>
          <TouchableOpacity
              style={[styles.buttonBox, { width: "35%" }]}
              onPress={() => {
                this.setState({datevisibledate:true,Type:'end'})
              }}
              disabled={flag_edit == false}
            >
              <Text>{this.state.EndDrillingFluidDate_date}</Text>
            </TouchableOpacity>
            <Text style={{width:'10%',textAlign:'center'}}>{I18n.t('startenddate.time')}</Text>
            <TouchableOpacity
              style={[styles.buttonBox, { width: "30%" }]}
              onPress={() => {
                this.setState({datevisibletime:true,Type:'end'})
              }}
              disabled={flag_edit == false}
            >
              <Text>{this.state.EndDrillingFluidDate_time}</Text>
            </TouchableOpacity>
                </View>
      </View>)
  }
  renderTimeStopEnd(){
    var flag_edit = true
    if(this.props.category == 5){
      if(this.props.parent == false){
        flag_edit = false
      }
    }else{
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
    }
    return(
    <View style={[styles.listTime, styles.listTimeLeft,{borderBottomWidth:1,borderColor:'#ccc',paddingBottom:15}]}>
      {this.state.datevisibledate1 &&<DateTimePicker
        value={this.getTime1()}
        isVisible={this.state.datevisibledate1}
        is24Hour={true}
        display='spinner'
        mode='date'
        onChange={this.onChangedateStopEnd}
      />}
      {this.state.datevisibletime1 && <DateTimePicker
        value={this.getTime1()}
        isVisible={this.state.datevisibletime1}
        is24Hour={true}
        display='spinner'
        mode='time'
        onChange={this.onChangetimeStopEnd}
      />}
      <Text style={{marginLeft:15}}>{I18n.t('mainpile.5_4.titletime1')}</Text>
      <View style={[styles.imageDetail,{marginLeft:20,marginRight:20,marginBottom:15}]}>
          <Text style={{width:'25%'}}>{I18n.t('startenddate.start')}</Text>
          <TouchableOpacity
            style={[styles.buttonBox, { width: "35%" }]}
            onPress={() => {
              this.setState({datevisibledate1:true,Type:'start'})
            }}
            disabled={flag_edit == false}
          >
            <Text>{this.state.StartCheckstopend_date}</Text>
          </TouchableOpacity>
          <Text style={{width:'10%',textAlign:'center'}}>{I18n.t('startenddate.time')}</Text>
          <TouchableOpacity
            style={[styles.buttonBox, { width: "30%" }]}
            onPress={() => {
              this.setState({datevisibletime1:true,Type:'start'})
            }}
            disabled={flag_edit == false}
          >
            <Text>{this.state.StartCheckstopend_time}</Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.imageDetail,{marginLeft:20,marginRight:20,marginBottom:15}]}>
        <Text style={{width:'25%'}}>{I18n.t('startenddate.end')}</Text>
        <TouchableOpacity
            style={[styles.buttonBox, { width: "35%" }]}
            onPress={() => {
              this.setState({datevisibledate1:true,Type:'end'})
            }}
            disabled={flag_edit == false}
          >
            <Text>{this.state.EndCheckstopend_date}</Text>
          </TouchableOpacity>
          <Text style={{width:'10%',textAlign:'center'}}>{I18n.t('startenddate.time')}</Text>
          <TouchableOpacity
            style={[styles.buttonBox, { width: "30%" }]}
            onPress={() => {
              this.setState({datevisibletime1:true,Type:'end'})
            }}
            disabled={flag_edit == false}
          >
            <Text>{this.state.EndCheckstopend_time}</Text>
          </TouchableOpacity>
              </View>
    </View>)
}
  selectedwaterstopType= async typeitem =>{
    // console.warn('typeitem',typeitem)
    this.setState({waterstopType: typeitem.label,waterstopTypeID:typeitem.id},()=>{this.saveLocal('5_4')})
    // this.saveLocal('5_4')
  }
  renderWaterStop(){
    var flag_edit = true
    if(this.props.category == 5){
      if(this.props.parent == false){
        flag_edit = false
      }
    }else{
      if(this.state.Edit_Flag != 1){
        flag_edit = false
      }
    }
    return(
      <View style={{margin:5}}>
      <View >
        {/*<View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t("mainpile.5_4.waterstop")}</Text>
    </View>*/}
        <View>
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.5_4.waterstop")}</Text>
          </View>
          {
            flag_edit ?
            <View style={styles.selectedView}>
              <ModalSelector
                data={this.state.waterstopTypelist}
                onChange={this.selectedwaterstopType}
                selectTextStyle={styles.textButton}
                cancelText="Cancel"
              >
                <TouchableOpacity style={styles.button}>
                  <View style={styles.buttonTextStyle}>
                    <Text style={styles.buttonText} numberOfLines={1}>{this.state.waterstopType || I18n.t("mainpile.5_4.selectWaterstop")}</Text>
                    <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                      <Icon name="arrow-drop-down" type="MaterialIcons" color="#007CC2" />
                    </View>
                  </View>
                </TouchableOpacity>
              </ModalSelector>
            </View>
            :
            <View style={styles.selectedView}>
              <TouchableOpacity disabled={true} style={[styles.button, { borderColor: MENU_GREY_ITEM }]}>
                <View style={styles.buttonTextStyle}>
                  <Text style={[styles.buttonText ,{ color: MENU_GREY_ITEM,marginHorizontal:5 }]} numberOfLines={1}>
                    {this.state.waterstopType || I18n.t("mainpile.5_4.selectWaterstop")}
                  </Text>
                  <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                    <Icon
                      name="arrow-drop-down"
                      type="MaterialIcons"
                      color={MENU_GREY_ITEM}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          }
        </View>
      </View>


      <View >
          <View style={styles.boxTopic}>
            <Text>{I18n.t("mainpile.5_4.waterstoplenght")}</Text>
          </View>
          <View style={styles.selectedView}>
            <View style={[styles.button, { height: 40, padding: 0 }, flag_edit == false && { borderColor: MENU_GREY_ITEM }]}>
              <TextInput
                editable={flag_edit}
                keyboardType="numeric"
                placeholder={I18n.t("mainpile.5_4.waterstoplenght")}
                value={this.state.waterstoplenght.toString()}
                onChangeText={value => this.setState({waterstoplenght:value})}
                underlineColorAndroid="transparent"
                style={flag_edit ? styles.textInputStyle : styles.textInputStyleGrey}
                onEndEditing={() => {
                  this.setState({waterstoplenght: isNaN(parseFloat(this.state.waterstoplenght))?'':parseFloat(Math.abs(this.state.waterstoplenght)).toFixed(3)},()=>{
                    this.saveLocal('5_4')
                  })
                  }
                }
              />
            </View>
          </View>
      </View>


      </View>


        
    )
  }
  render() {
    console.log('pile5 edit')
    if (this.props.hidden) {
      return null
    }
    return (<View style={styles.listContainer}>
      <KeyboardAwareScrollView scrollEnabled={true} resetScrollToCoords={{ x: 0, y: 0 }} enableOnAndroid ={true}>
        <View style={styles.listTopic}>
          <Text style={styles.listTopicText}>{I18n.t('mainpile.drillpileinsert_dw.drillpileinsert')}</Text>
        </View>
        <View style={styles.listTopicSub}>
            <Text style={styles.listTopicSubText}>{I18n.t('mainpile.5_1.listchack')}</Text>
            <View style={styles.listCheckbox}>
              <View style={styles.listCheckboxBase}>
                {this.state.CheckStopEnd == true ? (
                  <Icon
                    name="check"
                    reverse
                    color="#6dcc64"
                    size={15}
                    onPress={()=> this.onCheckStopEnd()}
                  />
                ) : (
                  <Icon
                    name="check"
                    reverse
                    color="grey"
                    size={15}
                    onPress={()=> this.onCheckStopEnd()}
                  />
                )}
                <Text style={styles.textFlex}>{I18n.t('mainpile.5_4.text1')}</Text>
              </View>
              <View style={styles.buttonImage}>
                <TouchableOpacity
                  style={[styles.image,this.state.ImageCheckStopEnd.length>0?{backgroundColor:'#6dcc64'}:{}]}
                  onPress={() => this.onCameraRoll("ImageCheckStopEnd", this.state.ImageCheckStopEnd,'edit')}
                >
                  <Icon name="image" type="entypo" size={14} color="#FFF" />
                  <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.listCheckbox}>
              <View style={styles.listCheckboxBase}>
                {this.state.CheckWaterStop == true ? (
                  <Icon
                    name="check"
                    reverse
                    color="#6dcc64"
                    size={15}
                    onPress={()=> this.onCheckWaterStop()}
                  />
                ) : (
                  <Icon
                    name="check"
                    reverse
                    color="grey"
                    size={15}
                    onPress={()=> this.onCheckWaterStop()}
                  />
                )}
              <Text style={styles.textFlex}>{I18n.t('mainpile.5_4.text2')}</Text>
              </View>
              <View style={styles.buttonImage}>
                <TouchableOpacity
                  style={[styles.image,this.state.ImageCheckWaterStop.length>0?{backgroundColor:'#6dcc64'}:{}]}
                  onPress={() => this.onCameraRoll("ImageCheckWaterStop", this.state.ImageCheckWaterStop,'edit')}
                >
                  <Icon name="image" type="entypo" size={14} color="#FFF" />
                  <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                </TouchableOpacity>
              </View>
            </View>
            {
              this.state.CheckWaterStop?
              this.renderWaterStop()
              :
              <View/>
            }
            <View style={styles.listCheckbox}>
              <View style={styles.listCheckboxBase}>
                {this.state.CheckInstallStopEnd == true ? (
                  <Icon
                    name="check"
                    reverse
                    color="#6dcc64"
                    size={15}
                    onPress={()=> this.onCheckInstallStopEnd()}
                  />
                ) : (
                  <Icon
                    name="check"
                    reverse
                    color="grey"
                    size={15}
                    onPress={()=> this.onCheckInstallStopEnd()}
                  />
                )}
                <Text style={styles.textFlex}>{I18n.t('mainpile.5_4.text3')}</Text>
              </View>
              <View style={styles.buttonImage}>
                <TouchableOpacity
                  style={[styles.image,this.state.ImageCheckInstallStopEnd.length>0?{backgroundColor:'#6dcc64'}:{}]}
                  onPress={() => this.onCameraRoll("ImageCheckInstallStopEnd", this.state.ImageCheckInstallStopEnd,'edit')}
                >
                  <Icon name="image" type="entypo" size={14} color="#FFF" />
                  <Text style={styles.imageText}>{I18n.t("mainpile.steeldetail.view")}</Text>
                </TouchableOpacity>
              </View>
            </View>
           {
            this.state.CheckInstallStopEnd?
              <View>
                {
                  this.renderTimeStopEnd()
                }
              </View>
              : <View/>
           }
            <View style={[styles.listCheckbox,this.state.CheckDrillingFluid == false?{borderBottomWidth: 1,borderColor: '#ccc',paddingBottom:15}:{}]}>
              <View style={styles.listCheckboxBase}>
                {this.state.CheckDrillingFluid == true ? (
                  <Icon
                    name="check"
                    reverse
                    color="#6dcc64"
                    size={15}
                    onPress={()=> this.onCheckDrillingFluid()}
                  />
                ) : (
                  <Icon
                    name="check"
                    reverse
                    color="grey"
                    size={15}
                    onPress={()=> this.onCheckDrillingFluid()}
                  />
                )}
                <Text style={styles.textFlex}>{I18n.t('mainpile.5_4.text4')}</Text>
              </View>
              <View style={styles.buttonImage}>
                <TouchableOpacity
                  style={[styles.image,this.state.ImageCheckDrillingFluid.length>0?{backgroundColor:'#6dcc64'}:{}]}
                  onPress={() => this.onCameraRoll("ImageCheckDrillingFluid", this.state.ImageCheckDrillingFluid,'edit')}
                >
                  <Icon name="image" type="entypo" size={14} color="#FFF" />
                  <Text style={[styles.imageText]}>{I18n.t("mainpile.steeldetail.view")}</Text>
                </TouchableOpacity>
              </View>
            </View>
            {
              // this.state.CheckDrillingFluid == true && this.state.CheckWaterStop == true && this.state.CheckInstallStopEnd == true && this.state.CheckStopEnd == true
              this.state.CheckDrillingFluid == true
              ?
              <View>
                {this.renderTimeCheckDrillingFluid()}
              </View>
              : <View/>
            }
            
        </View>
      {/* <View style={styles.listRow}>
        <View style={styles.listTopicSub}>
          <Text style={styles.listTopicText}>ตรวจเช็คการะเจาะเสาเข็ม</Text>
          <View style={styles.listSelectedView}>
          </View>
        </View>

        <View style={styles.listTopicSub}>
          <Text style={styles.listTopicSubText}>รายการตรวจเช็ค</Text>
          <View style={styles.listSelectedView}>
          </View>
        </View>

        <View style={styles.listTopicSubRow}>
          {this.state.CheckStopEnd == true ? (
            <Icon
              name="check"
              reverse
              color="#6dcc64"
              size={15}
              onPress={()=> this.onCheckStopEnd()}
            />
          ) : (
            <Icon
              name="check"
              reverse
              color="grey"
              size={15}
              onPress={()=> this.onCheckStopEnd()}
            />
          )}
          <Text style={styles.listLabel}>{I18n.t("mainpile.liquidtestinsert.unit_ph")} แซะ Stop End ของ Panel ก่อนหน้าแล้ว</Text>
          <View style={styles.buttonImage}>
            <TouchableOpacity
              style={styles.image}
              onPress={() => this.onCameraRoll("ImageCheckStopEnd", this.state.ImageCheckStopEnd,'edit')}
            >
              <Icon name="image" type="entypo" color="#fff" />
              <Text style={styles.imageText}>{I18n.t("mainpile.liquidtestinsert.view")}</Text>
            </TouchableOpacity>
          </View>

        </View>
        <View style={styles.listTopicSubRow}>
          {this.state.CheckWaterStop == true ? (
            <Icon
              name="check"
              reverse
              color="#6dcc64"
              size={15}
              onPress={()=> this.onCheckWaterStop()}
            />
          ) : (
            <Icon
              name="check"
              reverse
              color="grey"
              size={15}
              onPress={()=> this.onCheckWaterStop()}
            />
          )}
          <Text style={styles.listLabel}>ติดตั้ง Water Stop เข้าไปใน Stop End {I18n.t("mainpile.liquidtestinsert.unit_density")} *</Text>
          <View style={styles.buttonImage}>
            <TouchableOpacity
              style={styles.image}
              onPress={() => this.onCameraRoll("ImageCheckWaterStop", this.state.ImageCheckWaterStop,'edit')}
            >
              <Icon name="image" type="entypo" color="#fff" />
              <Text style={styles.imageText}>{I18n.t("mainpile.liquidtestinsert.view")}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.listTopicSubRow}>
          {this.state.CheckInstallStopEnd == true ? (
            <Icon
              name="check"
              reverse
              color="#6dcc64"
              size={15}
              onPress={()=> this.onCheckInstallStopEnd()}
            />
          ) : (
            <Icon
              name="check"
              reverse
              color="grey"
              size={15}
              onPress={()=> this.onCheckInstallStopEnd()}
            />
          )}
          <Text style={styles.listLabel}>ติดตั้ง Stop End เข้าไปในแผง D-Wall {I18n.t("mainpile.liquidtestinsert.unit_viscosity")} *</Text>
          <View style={styles.buttonImage}>
            <TouchableOpacity
              style={styles.image}
              onPress={() => this.onCameraRoll("ImageCheckInstallStopEnd", this.state.ImageCheckInstallStopEnd,'edit')}
            >
              <Icon name="image" type="entypo" color="#fff" />
              <Text style={styles.imageText}>{I18n.t("mainpile.liquidtestinsert.view")}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.listTopicSubRow}>
          {this.state.CheckDrillingFluid == true ? (
            <Icon
              name="check"
              reverse
              color="#6dcc64"
              size={15}
              onPress={()=> this.onCheckDrillingFluid()}
            />
          ) : (
            <Icon
              name="check"
              reverse
              color="grey"
              size={15}
              onPress={()=> this.onCheckDrillingFluid()}
            />
          )}
          <Text style={styles.listLabel}>เปลี่ยนน้ำยาหลังเจาะเสร็จแล้ว</Text>
          <View style={styles.buttonImage}>
            <TouchableOpacity
              style={styles.image}
              onPress={() => this.onCameraRoll("ImageCheckDrillingFluid", this.state.ImageCheckDrillingFluid,'edit')}
            >
              <Icon name="image" type="entypo" color="#fff" />
              <Text style={styles.imageText}>{I18n.t("mainpile.liquidtestinsert.view")}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View> */}
      {/* {this.renderTime()} */}
    </KeyboardAwareScrollView>

    </View>)
  }
}

const mapStateToProps = state => {
  return {
      pileAll: state.mainpile.pileAll || null,
  }
}

export default connect(mapStateToProps, actions, null, {withRef: true})(Pile5_4)
