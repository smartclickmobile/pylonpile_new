import React, { Component } from "react"
import {
  Text,
  View,
  Alert,
  Dimensions,
  BackHandler,
  TouchableOpacity,
  ActivityIndicator,
  PermissionsAndroid
} from "react-native"
import NetInfo from "@react-native-community/netinfo";
import { Container } from "native-base"
import {
  MENU_ORANGE,
  MENU_ORANGE_ITEM,
  MENU_ORANGE_BORDER,
  MENU_GREEN,
  MENU_GREEN_ITEM,
  MENU_GREEN_BORDER,
  DEFAULT_COLOR_2,
  DEFAULT_COLOR_3,
  DEFAULT_COLOR_5,
  DEFAULT_COLOR_6
} from "../Constants/Color"
import Carousel from "react-native-snap-carousel"
import { Actions } from "react-native-router-flux"
import Pile1 from "./Pile1"
import Pile2 from "./Pile2"
import Pile3 from "./Pile3"
import Pile4 from "./Pile4"
import Pile5 from "./Pile5"
import Pile6 from "./Pile6"
import Pile7 from "./Pile7"
import Pile8 from "./Pile8"
import Pile9 from "./Pile9"
import Pile10 from "./Pile10"
import Pile11 from "./Pile11"
import { connect } from "react-redux"
import * as actions from "../Actions"
import { MainPileFunc } from "../Components/function"
import PileDetailRightButton from "../Components/PileDetailRightButton"
import I18n from "../../assets/languages/i18n"
import styles from "./styles/MainPile.style"
import Svg,{
    Line,
} from 'react-native-svg'
import DeviceInfo from 'react-native-device-info'
import firebase from "react-native-firebase"
import FusedLocation from 'react-native-fused-location'
const sliderWidth = Dimensions.get("window").width
const itemWidth = sliderWidth * 0.7

class MainPile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [
        {
          title: I18n.t("mainpile.step.1"),
          step: "01"
        },
        {
          title: I18n.t("mainpile.step.2"),
          step: "02"
        },
        {
          title: I18n.t("mainpile.step.3"),
          step: "03"
        },
        {
          title: I18n.t("mainpile.step.4"),
          step: "04"
        },
        {
          title: this.props.category==1 || this.props.category == 4?I18n.t("mainpile.step.5"):I18n.t("mainpile.step.5_dw"),
          step: "05"
        },
        {
          title: I18n.t("mainpile.step.6"),
          step: "06"
        },
        {
          title: I18n.t("mainpile.step.7"),
          step: "07"
        },
        {
          title: I18n.t("mainpile.step.8"),
          step: "08"
        },
        {
          title: I18n.t("mainpile.step.9"),
          step: "09"
        },
        {
          title: I18n.t("mainpile.step.10"),
          step: "10"
        },
        {
          title: I18n.t("mainpile.step.11"),
          step: "11"
        }
      ],
      activeSlide: 0,
      index: 0,
      pile: [],
      firstItem: 0,
      stack: [],
      stepStatus: null,
      isStart: false,
      loading: true,
      error: null,
      pileValueInfo: [],
      randomlockstep7_8:'',
      checkto7_super_random:null,
      checkto6_super_random:null,
      checkto5_super_random:null,
      checkfromnoti:false,
      parentFalg:false,
      parentNo:null,
      Lockerror:false
    }
  }

  componentDidMount() {
    firebase.analytics().setCurrentScreen("MainPile")
    // console.warn("mount mainpile",this.props.fromnoti)
    BackHandler.addEventListener("mainback", () => this._handleBack())
    // this.setState({ loading: false })
    var data = {
      Language:I18n.locale,
      JobId:this.props.jobid,
      PileId:this.props.pileid,
    }
    // console.warn('data',data)
    this.props.checkmessagepile(data)
  }

  componentWillUnmount() {
    console.log("unmount mainpile")
    BackHandler.removeEventListener("mainback", () => this._handleBack())
    if (this.refs["pile2"] !== undefined) {
      if (this.props.stack.length == 0) {
        var data = {
          process: 1,
          step: 1
        }
        if (typeof this.refs["pile2"].getWrappedInstance().onSetStackReducer === "function") {
          this.refs["pile2"].getWrappedInstance().onSetStackReducer(data)
        }
      }
    }
    this.props.clearStatus()
    this.props.pileClear()
  }

  detailRightButton = () => <PileDetailRightButton data={this} index={this.state.index} category={this.props.category} pileAll1={this.props.pileAll} finish={this.props.finish} sp_parentid={this.props.sp_parentid} icon_image={this.props.icon_image} url_dash={this.props.url_dash}/>

  async componentWillMount() {
    // this.setState({isStart:false})
    this.props.pileValueInfoReset()
    this.onCheckInternet()
    Actions.refresh({ right: this.detailRightButton, action: "start" })

    this.fetchData()
    this.props.clearStack()
    this.saveLocation()

    
  }

  async componentWillReceiveProps(nextProps) {
    this.setState({ stack: nextProps.stack })
    if((nextProps.category == 3|| this.props.category == 5) && nextProps.lockstep7_8 && nextProps.randomlockstep7_8 != this.state.randomlockstep7_8){
      this.setState({randomlockstep7_8:nextProps.randomlockstep7_8})
      Alert.alert("", I18n.t("mainpile.button.goto") + " " +I18n.t('mainpile.step.8'), [
        { text: "Cancel", onPress: () => {}, style: "cancel" },
        {
          text: "Ok",
          onPress: async () => {
            this.alertNext(7)
            if( (this.props.category == 5 || this.props.category == 3) && (this.props.pileid != this.props.sp_parentid)){
              Alert.alert("",I18n.t('mainpile.lockprocess.error11'),[
                {
                  text:"Ok",
                  onPress: ()=> {
                    // this.alertNext(index)
                  }
                }
              ])
            }
            ///fjkhskfhkfuhskfjhdksfhskdhf
            // console.log(data)
          }
        }
      ])
    }
    // console.warn('jjjjjjjj' ,(this.props.category == 5 || this.props.category == 3) ,nextProps.checkto5_super == true , nextProps.checkto5_super_random != this.state.checkto5_super_random , nextProps.lockstepcheckto5_super == true)
    if((nextProps.category == 3 || this.props.category == 5) && nextProps.lockstepcheckto7_super == true && nextProps.checkto7_super_random != this.state.checkto7_super_random ){
      this.setState({checkto7_super_random:nextProps.checkto7_super_random})
      Alert.alert("", I18n.t("mainpile.button.goto") + " " +I18n.t('mainpile.step.7'), [
        { text: "Cancel", onPress: () => {}, style: "cancel" },
        {
          text: "Ok",
          onPress: async () => {
            this.alertNext(6)
            // console.log(data)
          }
        }
      ])
    }
    if((this.props.category == 5 || this.props.category == 3) &&nextProps.checkto5_super == true && nextProps.checkto5_super_random != this.state.checkto5_super_random && nextProps.lockstepcheckto5_super == true){
      this.setState({checkto5_super_random: nextProps.checkto5_super_random},()=>{
        // console.warn('ffff')
      
      // setTimeout(()=>{this.setState({checksavebutton:false})},2000)
      Alert.alert("", I18n.t("mainpile.button.goto") + " " +I18n.t('mainpile.step.5'), [
        { text: "Cancel", onPress: () => {}, style: "cancel" },
        {
          text: "Ok",
          onPress: async () => {
        // setTimeout(()=>{
          this.alertNext(4)
        // },5000)
            
          }
        }
      ])
      })
    }
    // console.warn(nextProps.category == 5, nextProps.checkto6_super , nextProps.checkto6_super_random , this.state.checkto6_super_random)
    if((this.props.category == 5) &&nextProps.checkto6_super == true && nextProps.checkto6_super_random != this.state.checkto6_super_random && nextProps.lockstepcheckto6_super == true){
      this.setState({checkto6_super_random: nextProps.checkto6_super_random},()=>{
        // console.warn('gggggg')
        Alert.alert("", I18n.t("mainpile.button.goto") + " " +I18n.t('mainpile.step.6'), [
          { text: "Cancel", onPress: () => {}, style: "cancel" },
          {
            text: "Ok",
            onPress: async () => {
          setTimeout(()=>{
            this.alertNext(5)
          },5000)
              
            }
          }
        ])
      })
    }
    if (nextProps.error != null) {
     
      // var olderrortext =  errortext
      var errortext = nextProps.error
      // console.log('test_error_alret',olderrortext,errortext)
      if(this.state.Lockerror==false){
        this.setState({Lockerror:true},()=>{
          Alert.alert("ERROR", errortext.replace(/\\n| \\n/g, "\n"), [
            {
              text: "OK",
              onPress: () => {
                this.props.getDrillingFluids({
                  pileid: this.props.pileid,
                  jobid: this.props.jobid,
                  Language: I18n.locale
                })
                this.setState({Lockerror:false})
                console.log("Error MainPile",errortext.replace(/\\n| \\n/g, "\n"))
                this.props.clearError()
              }
            }
          ],{cancelable:false})
        })
      }else{
        console.log('excess log error',nextProps.error)
      }
     
      
    } else if (nextProps.action == "update") {
      const value = nextProps.value
      if (typeof this.refs["pile" + value.process].getWrappedInstance === "function") {
        this.refs["pile" + value.process].getWrappedInstance().updateState(value)
      }
      Actions.refresh({
        action: null,
        value: null
      })
    } else if (nextProps.item && nextProps.error == null) {
      this.setState({ pile: nextProps.item })
    }

    if (nextProps.action == "refresh" && this.state.loading == false) {
      this.fetchData()
      this.setState({ loading: true })

    }
    var pile = nextProps.valueInfo ? await MainPileFunc.getStorage(nextProps.valueInfo.pile_id) : null
    if (nextProps.valueInfo != null && nextProps.masterInfo != null) {
      // step_status
      var value_step_status = {
        Step1Status: nextProps.valueInfo.Step1Status,
        Step2Status: nextProps.valueInfo.Step2Status,
        Step3Status: nextProps.valueInfo.Step3Status,
        Step4Status: nextProps.valueInfo.Step4Status,
        Step5Status: nextProps.valueInfo.Step5Status,
        Step6Status: nextProps.valueInfo.Step6Status,
        Step7Status: nextProps.valueInfo.Step7Status,
        Step8Status: nextProps.valueInfo.Step8Status,
        Step9Status: nextProps.valueInfo.Step9Status,
        Step10Status: nextProps.valueInfo.Step10Status,
        Step11Status: nextProps.valueInfo.Step11Status
      }
      // await this.props.mainPileSetStorageNoneRedux(nextProps.valueInfo.pile_id,'step_status',value_step_status)

      var data = {}

      var sectionList = []
      // console.warn('SectionList',nextProps.valueInfo.SectionList)
      if (nextProps.valueInfo.SectionList != null && nextProps.valueInfo.SectionList.length > 0) {
        for (var i = 0; i < nextProps.valueInfo.SectionList.length; i++) {
          if(nextProps.valueInfo.SectionList[i].checksteelcage == undefined){
            sectionList.push({
              section_id: nextProps.valueInfo.SectionList[i].sectionId,
              section_no: nextProps.valueInfo.SectionList[i].section_no,
              checksteelcage: nextProps.valueInfo.SectionList[i].Checksteelcage,
              checkconcretespacer: nextProps.valueInfo.SectionList[i].CheckConcretespacer,
              image_steelcage: nextProps.valueInfo.SectionList[i].ImageSteelcage,
              image_concretespacer: nextProps.valueInfo.SectionList[i].ImageConcretespacer
            })
          }else{
            sectionList.push({
              section_id: nextProps.valueInfo.SectionList[i].section_id,
              section_no: nextProps.valueInfo.SectionList[i].section_no,
              checksteelcage: nextProps.valueInfo.SectionList[i].checksteelcage,
              checkconcretespacer: nextProps.valueInfo.SectionList[i].checkconcretespacer,
              image_steelcage: nextProps.valueInfo.SectionList[i].image_steelcage,
              image_concretespacer: nextProps.valueInfo.SectionList[i].image_concretespacer
            })
          }
          
        }
      }
      var section73 = []

      // if (nextProps.masterInfo.Step7.Steelcage.SectionList != null) {
      //   section73 = nextProps.masterInfo.Step7.Steelcage.SectionList
      // }

      /** I am bad 7_3 rever to normal


      if (nextProps.valueInfo.SectionList != null && nextProps.valueInfo.SectionList.length > 0) {
        var section1_2 = nextProps.valueInfo.SectionList
        var section7_3 = nextProps.valueInfo.Step7.SectionList
        var shape = nextProps.masterInfo.PileDetail.shape
        if (shape == 1) {
          if (section7_3 && section7_3 != null ) {
            section7_3.map((data, index) => {
              if(typeof section73[index] === 'undefined') {
                section73.push({
                  // section_id: data.section_id,
                  // section_no: data.section_no,
                  checksteelcage: data.checksteelcage,
                  image_steelcage: data.image_steelcage,
                })
              }
              else {
                // section73[index].section_id = data.section_id
                // section73[index].section_no = data.section_no
                section73[index].checksteelcage = data.checksteelcage
                section73[index].image_steelcage = data.image_steelcage
              }
            })
          }
          if (section1_2 && section1_2 != null) {
            section1_2.map((data, index) => {
              if(typeof section73[index] === 'undefined') {
                section73.push({
                  checkconcretespacer: data.checkconcretespacer,
                  image_concretespacer: data.image_concretespacer
                })
              }
              else {
                section73[index].checkconcretespacer = data.checkconcretespacer
                section73[index].image_concretespacer = data.image_concretespacer
              }
            })
          }
        }
        // DWALL
        else if (shape == 2) {
          if (section7_3 && section7_3 != null ) {
            section7_3.map((data, index) => {
              section73[index].checkconcretespacer = data.checkconcretespacer
              section73[index].image_concretespacer = data.image_concretespacer
              section73[index].checksteelcage = data.checksteelcage
              section73[index].image_steelcage = data.image_steelcage
            })
          }
        }
      }

      */

      if (nextProps.valueInfo.SectionList != null && nextProps.valueInfo.SectionList.length > 0) {
        for (var i = 0; i < nextProps.valueInfo.Step7.SectionList.length; i++) {
          section73.push({
            section_id: nextProps.valueInfo.Step7.SectionList[i].section_id,
            section_no: nextProps.valueInfo.Step7.SectionList[i].section_no,
            checksteelcage: nextProps.valueInfo.Step7.SectionList[i].checksteelcage,
            checkconcretespacer: nextProps.valueInfo.Step7.SectionList[i].checkconcretespacer,
            image_steelcage: nextProps.valueInfo.Step7.SectionList[i].image_steelcage,
            image_concretespacer: nextProps.valueInfo.Step7.SectionList[i].image_concretespacer
          })
        }
      }
      console.log('nextProps.valueInfo',nextProps.valueInfo.Step7)
      data = {
        step_status: {
          Step1Status: nextProps.valueInfo.Step1Status,
          Step2Status: nextProps.valueInfo.Step2Status,
          Step3Status: nextProps.valueInfo.Step3Status,
          Step4Status: nextProps.valueInfo.Step4Status,
          Step5Status: nextProps.valueInfo.Step5Status,
          Step6Status: nextProps.valueInfo.Step6Status,
          Step7Status: nextProps.valueInfo.Step7Status,
          Step8Status: nextProps.valueInfo.Step8Status,
          Step9Status: nextProps.valueInfo.Step9Status,
          Step10Status: nextProps.valueInfo.Step10Status,
          Step11Status: nextProps.valueInfo.Step11Status
        },
        "1_0": {
          process: 1,
          step: 0,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Id: nextProps.valueInfo.Step1.Id,
            Edit_Flag:this.props.viewType == 0 ? 0 : nextProps.valueInfo.Step1.Edit_Flag,
            Status: nextProps.valueInfo.Step1.Status,
            StartDate: nextProps.valueInfo.Step1.StartDate,
            EndDate: nextProps.valueInfo.Step1.EndDate,
            LocationLat: nextProps.valueInfo.Step1.LocationLat,
            LocationLong: nextProps.valueInfo.Step1.LocationLong
          },
        },
        "1_1": {
          process: 1,
          step: 1,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            providerName: nextProps.valueInfo.ProviderName,
            ProviderId: nextProps.valueInfo.ProviderId
          },
          masterInfo: {
            ProviderList: nextProps.masterInfo.Step1.ProviderList
          }
        },
        "1_2": {
          process: 1,
          step: 2,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            SectionDetail: sectionList,
          },
          startdate:nextProps.valueInfo.Step1.StartDate,
          enddate:nextProps.valueInfo.Step1.EndDate,
          masterInfo: {
            Steelcage: nextProps.masterInfo.Step1.Steelcage,
            rbsheet:nextProps.masterInfo.Step1.rbsheet
          }
        },
        "2_0": {
          process: 2,
          step: 0,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Id: nextProps.valueInfo.Step2.Id,
            Edit_Flag:this.props.viewType == 0 ? 0 :  nextProps.valueInfo.Step2.Edit_Flag,
            Status: nextProps.valueInfo.Step2.Status,
            StartDate: nextProps.valueInfo.Step2.StartDate,
            EndDate: nextProps.valueInfo.Step2.EndDate,
            LocationLat: nextProps.valueInfo.Step2.LocationLat,
            LocationLong: nextProps.valueInfo.Step2.LocationLong
          }
        },
        "2_1": {
          process: 2,
          step: 1,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            machine: null,
            vibro: null,
            driver: null,
            Length: nextProps.valueInfo.Casing_Length,
            MachineCraneId: nextProps.valueInfo.Casing_MachineCraneId,
            MachineVibroId: nextProps.valueInfo.Casing_MachineVibroId,
            DriverId: nextProps.valueInfo.Casing_DriverId,
            vibro_flag: nextProps.valueInfo.Casing_MachineVibroId == null ? false : true
          },
          masterInfo: {
            CraneList: nextProps.masterInfo.Step2.CraneList,
            VibroList: nextProps.masterInfo.Step2.VibroList,
            DriverList: nextProps.masterInfo.Step2.DriverList
          }
        },
        "2_2": {
          process: 2,
          step: 2,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            checkplummet: nextProps.valueInfo.Casing_CheckPlummet,
            checkwater: nextProps.valueInfo.Casing_CheckWaterLevel,
            images_plummet: nextProps.valueInfo.Image_CasingPlummet,
            images_water: nextProps.valueInfo.Image_CasingWaterLevel,
          },
          startdate:nextProps.valueInfo.Step2.StartDate,
          enddate:nextProps.valueInfo.Step2.EndDate
        },
        "3_0": {
          process: 3,
          step: 0,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Id: nextProps.valueInfo.Step3.Id,
            Edit_Flag:this.props.viewType == 0 ? 0 :  nextProps.valueInfo.Step3.Edit_Flag,
            Status: nextProps.valueInfo.Step3.Status,
            StartDate: nextProps.valueInfo.Step3.StartDate,
            EndDate: nextProps.valueInfo.Step3.EndDate,
            LocationLat: nextProps.valueInfo.Step3.LocationLat,
            LocationLong: nextProps.valueInfo.Step3.LocationLong
          }
        },
        "3_1": {
          process: 3,
          step: 1,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            survey: null,
            benchmarkCam: null,
            benchmarkFlag: null,
            resection: nextProps.valueInfo.PileSurvey_IsResection,
            SurveyorId: nextProps.valueInfo.PileSurvey_SurveyorId,
            LocationId: nextProps.valueInfo.PileSurvey_LocationId,
            BSFlagLocationId: nextProps.valueInfo.PileSurvey_BSFlagLocationId,
            benchmarkCamLabel: null,
            benchmarkFlagLabel: null
          },
          masterInfo: {
            SurveyList: nextProps.masterInfo.Step3.SurveyList,
            BenchmarkList: nextProps.masterInfo.Step3.BenchmarkList
          }
        },
        "3_2": {
          process: 3,
          step: 2,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            eastPosition: null,
            eastCal: nextProps.valueInfo.PileSurvey_ReadingECoordinate
              ? parseFloat(
                  parseFloat(nextProps.valueInfo.PileSurvey_ReadingECoordinate) -
                    parseFloat(nextProps.masterInfo.Step3.CasingPosition.easting)
                ).toFixed(4)
              : "",
            eastCalBy: nextProps.valueInfo.PileSurvey_ReadingECoordinate
              ? parseFloat(nextProps.valueInfo.PileSurvey_ReadingECoordinate).toFixed(4)
              : nextProps.valueInfo.PileSurvey_ReadingECoordinate,
            northPosition: null,
            northCal: nextProps.valueInfo.PileSurvey_ReadingNCoordinate
              ? parseFloat(
                  parseFloat(nextProps.valueInfo.PileSurvey_ReadingNCoordinate) -
                    parseFloat(nextProps.masterInfo.Step3.CasingPosition.northing)
                ).toFixed(4)
              : "",
            northCalBy: nextProps.valueInfo.PileSurvey_ReadingNCoordinate
              ? parseFloat(nextProps.valueInfo.PileSurvey_ReadingNCoordinate).toFixed(4)
              : nextProps.valueInfo.PileSurvey_ReadingNCoordinate,
            Image_ReadingCoordinate: nextProps.valueInfo.Image_ReadingCoordinate,
            Image_SurveyDiff: nextProps.valueInfo.Image_SurveyDiff,
            constant: nextProps.masterInfo.Step3.Constants.Constant
          },
          masterInfo: {
            CasingPosition: nextProps.masterInfo.Step3.CasingPosition,
            Constants: nextProps.masterInfo.Step3.Constants
          }
        },
        "3_3": {
          process: 3,
          step: 3,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            top: nextProps.valueInfo.PileSurvey_TopCasingLevel
              ? parseFloat(nextProps.valueInfo.PileSurvey_TopCasingLevel).toFixed(3)
              : nextProps.valueInfo.PileSurvey_TopCasingLevel,
            ground: nextProps.valueInfo.PileSurvey_GroundLevel
              ? parseFloat(nextProps.valueInfo.PileSurvey_GroundLevel).toFixed(3)
              : nextProps.valueInfo.PileSurvey_GroundLevel
          },
          startdate:nextProps.valueInfo.Step3.StartDate,
          enddate:nextProps.valueInfo.Step3.EndDate
        },
        "3_4": {
          process: 3,
          step: 4,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            approve: nextProps.valueInfo.PileSurvey_ApprovedType == null || nextProps.valueInfo.PileSurvey_ApprovedType == '' ? false : true,
            approveBy: nextProps.valueInfo.PileSurvey_ApprovedType,
            ApprovedDate: nextProps.valueInfo.PileSurvey_ApprovedDate,
            ApprovedUserId: nextProps.valueInfo.PileSurvey_ApprovedUserId
          },
          masterInfo: {
            ApprovedUser: nextProps.masterInfo.Step3.ApprovedUser
          }
        },
        "3_5": {
          process: 3,
          step: 5,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            top: nextProps.valueInfo.PileSurvey_TopCasingLevel
              ? parseFloat(nextProps.valueInfo.PileSurvey_TopCasingLevel).toFixed(3)
              : nextProps.valueInfo.PileSurvey_TopCasingLevel,
            ground: nextProps.valueInfo.PileSurvey_GroundLevel
              ? parseFloat(nextProps.valueInfo.PileSurvey_GroundLevel).toFixed(3)
              : nextProps.valueInfo.PileSurvey_GroundLevel,
            survey: null,
            SurveyorId: nextProps.valueInfo.PileSurvey_SurveyorId,
          },
          masterInfo: {
            SurveyList: nextProps.masterInfo.Step3.SurveyList,
          }
        },
        "4_0": {
          process: 4,
          step: 0,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Id: nextProps.valueInfo.Step4.Id,
            Edit_Flag:this.props.viewType == 0 ? 0 :  nextProps.valueInfo.Step4.Edit_Flag,
            Status: nextProps.valueInfo.Step4.Status,
            StartDate: nextProps.valueInfo.Step4.StartDate,
            EndDate: nextProps.valueInfo.Step4.EndDate,
            LocationLat: nextProps.valueInfo.Step4.LocationLat,
            LocationLong: nextProps.valueInfo.Step4.LocationLong
          }
        },
        "4_1": {
          process: 4,
          step: 1,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            DrillingFluids:
              nextProps.valueInfo.Step4.DrillingFluids == null ? [] : nextProps.valueInfo.Step4.DrillingFluids
          },
          masterInfo: {
            ProcesstesterList: nextProps.masterInfo.Step4.ProcesstesterList,
            TesterList: nextProps.masterInfo.Step4.TesterList,
            MaxMinList: nextProps.masterInfo.Step4.MaxMinList
          }
        },
        "5_0": {
          process: 5,
          step: 0,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Id: nextProps.valueInfo.Step5.Id,
            Edit_Flag:this.props.viewType == 0 ? 0 :  nextProps.valueInfo.Step5.Edit_Flag,
            Status: nextProps.valueInfo.Step5.Status,
            StartDate: nextProps.valueInfo.Step5.StartDate,
            EndDate: nextProps.valueInfo.Step5.EndDate,
            LocationLat: nextProps.valueInfo.Step5.LocationLat,
            LocationLong: nextProps.valueInfo.Step5.LocationLong
          }
        },
        "5_1": {
          process: 5,
          step: 1,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            CheckPlummet: nextProps.valueInfo.Step5.CheckPlummet,
            CheckWaterLevel: nextProps.valueInfo.Step5.CheckWaterLevel,
            ImageCheckPlummet:
              nextProps.valueInfo.Step5.ImageCheckPlummet == null ? [] : nextProps.valueInfo.Step5.ImageCheckPlummet,
            ImageCheckWaterLevel:
              nextProps.valueInfo.Step5.ImageCheckWaterLevel == null
                ? []
                : nextProps.valueInfo.Step5.ImageCheckWaterLevel,
            ImageLengthSteelCarry:
              nextProps.valueInfo.Step5.ImageLengthSteelCarry == null
                ? []
                : nextProps.valueInfo.Step5.ImageLengthSteelCarry,
            OverLifting: nextProps.valueInfo.Step5.OverLifting,
            Pcoring: nextProps.valueInfo.Step5.Pcoring,
            HangingBarsLength: nextProps.valueInfo.Step5.HangingBarsLength
          },
          masterInfo: {
            PileInfo: nextProps.masterInfo.Step5.PileInfo,
            OverLifting: nextProps.masterInfo.Step5.OverLifting,
            Pcoring: nextProps.masterInfo.Step5.Pcoring,
            cutoff: nextProps.masterInfo.PileDetail.cutoff_parent
          }
        },
        "5_2": {
          process: 5,
          step: 2,
          pile_id: nextProps.valueInfo.pile_id,
          data: {},
          masterInfo: {
            PileInfo: nextProps.masterInfo.Step5.PileInfo
          }
        },
        "5_3": {
          process: 5,
          step: 3,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            DrillingList: nextProps.valueInfo.Step5.DrillingList == null ? [] : nextProps.valueInfo.Step5.DrillingList,
            CheckPlummet: nextProps.valueInfo.Step5.CheckPlummet,
            CheckWaterLevel: nextProps.valueInfo.Step5.CheckWaterLevel,
            ImageCheckPlummet:
              nextProps.valueInfo.Step5.ImageCheckPlummet == null ? [] : nextProps.valueInfo.Step5.ImageCheckPlummet,
            ImageCheckWaterLevel:
              nextProps.valueInfo.Step5.ImageCheckWaterLevel == null
                ? []
                : nextProps.valueInfo.Step5.ImageCheckWaterLevel
          
          },
          masterInfo: {
            DrillingToolTypeList: nextProps.masterInfo.Step5.DrillingToolTypeList,
            CraneList: nextProps.masterInfo.Step5.CraneList,
            DriverList: nextProps.masterInfo.Step5.DriverList,
            DrillingToolList: nextProps.masterInfo.Step5.DrillingToolList,
            PowerPackList: nextProps.masterInfo.Step5.PowerPackList,
            PileInfo: nextProps.masterInfo.Step5.PileInfo
          }
        },
        "5_4": {
          process: 5,
          step: 4,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            StartDrillingFluidDate: nextProps.valueInfo.Step5.StartDrillingFluidDate,
            EndDrillingFluidDate: nextProps.valueInfo.Step5.EndDrillingFluidDate,
            CheckStopEnd: nextProps.valueInfo.Step5.CheckStopEnd,
            CheckWaterStop: nextProps.valueInfo.Step5.CheckWaterStop,
            CheckInstallStopEnd: nextProps.valueInfo.Step5.CheckInstallStopEnd,
            CheckDrillingFluid: nextProps.valueInfo.Step5.CheckDrillingFluid,
            ImageCheckStopEnd: nextProps.valueInfo.Step5.ImageCheckStopEnd,
            ImageCheckWaterStop: nextProps.valueInfo.Step5.ImageCheckWaterStop,
            ImageCheckInstallStopEnd: nextProps.valueInfo.Step5.ImageCheckInstallStopEnd,
            ImageCheckDrillingFluid: nextProps.valueInfo.Step5.ImageCheckDrillingFluid,
            StartStopEndDate:nextProps.valueInfo.Step5.StartStopEndDate,
            EndStopEndDate:nextProps.valueInfo.Step5.EndStopEndDate,
            waterstopid:nextProps.valueInfo.Step5.WaterStopTypeId,
            WaterStopLength:nextProps.valueInfo.Step5.WaterStopLength
          },
          masterInfo: {
            WaterStopList: nextProps.masterInfo.Step5.WaterStopList,
          }
        },
        "5_5": {
          process: 5,
          step: 5,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            DrillingList: nextProps.valueInfo.Step5.DrillingChildList == null ? [] : nextProps.valueInfo.Step5.DrillingChildList,
            CheckPlummet: nextProps.valueInfo.Step5.CheckPlummetChild,
            CheckWaterLevel: nextProps.valueInfo.Step5.CheckWaterLevelChild,
            ImageCheckPlummet:
              nextProps.valueInfo.Step5.ImageCheckPlummetChild == null ? [] : nextProps.valueInfo.Step5.ImageCheckPlummetChild,
              ImageCheckWaterLevel:
              nextProps.valueInfo.Step5.ImageCheckWaterLevelChild == null
                ? []
                : nextProps.valueInfo.Step5.ImageCheckWaterLevelChild
          
          },
          masterInfo: {
            DrillingToolTypeList: nextProps.masterInfo.Step5.DrillingToolTypeChildList,
            CraneList: nextProps.masterInfo.Step5.CraneChildList,
            DriverList: nextProps.masterInfo.Step5.DriverList,
            DrillingToolList: nextProps.masterInfo.Step5.DrillingToolChildList,
            PowerPackList: nextProps.masterInfo.Step5.PowerPackChildList,
            PileInfo: nextProps.masterInfo.Step5.PileInfo
          }
        },
        "6_0": {
          process: 6,
          step: 0,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Id: nextProps.valueInfo.Step6.Id,
            Edit_Flag:this.props.viewType == 0 ? 0 :  nextProps.valueInfo.Step6.Edit_Flag,
            Status: nextProps.valueInfo.Step6.Status,
            StartDate: nextProps.valueInfo.Step6.StartDate,
            EndDate: nextProps.valueInfo.Step6.EndDate,
            LocationLat: nextProps.valueInfo.Step6.LocationLat,
            LocationLong: nextProps.valueInfo.Step6.LocationLong
          }
        },
        "6_1": {
          process: 6,
          step: 1,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            BucketSize: nextProps.valueInfo.Step6.BucketSize,
            DriverId: nextProps.valueInfo.Step6.DriverId,
            Machine: nextProps.valueInfo.Step6.Machine,
            CrossFlag: nextProps.valueInfo.Step6.CrossFlag,
            ChangeDrillingFluidBeforeDepth:nextProps.valueInfo.Step6.ChangeDrillingFluidBeforeDepth,
            ChangeDrillingFluidAfterDepth:nextProps.valueInfo.Step6.ChangeDrillingFluidAfterDepth
          },
          masterInfo: {
            BucketSize: nextProps.masterInfo.Step6.BucketSize,
            MachineList: nextProps.masterInfo.Step6.MachineList,
            DriverList: nextProps.masterInfo.Step6.DriverList
          },
          ChangeDrillingFluidStartDate:nextProps.valueInfo.Step6.ChangeDrillingFluidStartDate,
          ChangeDrillingFluidEndDate:nextProps.valueInfo.Step6.ChangeDrillingFluidEndDate
        },
        "6_2": {
          process: 6,
          step: 2,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            AfterDepth: nextProps.valueInfo.Step6.AfterDepth
              ? parseFloat(nextProps.valueInfo.Step6.AfterDepth).toFixed(3)
              : "",
            BeforeDepth: nextProps.valueInfo.Step6.BeforeDepth
              ? parseFloat(nextProps.valueInfo.Step6.BeforeDepth).toFixed(3)
              : "",
            BucketChanged: nextProps.valueInfo.Step6.BucketChanged,
            WaitingTimeMinute: nextProps.valueInfo.Step6.WaitingTimeMinute,
            Images: nextProps.valueInfo.Step6.Images
          },
          startdate:nextProps.valueInfo.Step6.StartDate,
          enddate:nextProps.valueInfo.Step6.EndDate
        },
        "7_0": {
          process: 7,
          step: 0,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Id: nextProps.valueInfo.Step7.Id,
            Edit_Flag:this.props.viewType == 0 ? 0 :  nextProps.valueInfo.Step7.Edit_Flag,
            Status: nextProps.valueInfo.Step7.Status,
            StartDate: nextProps.valueInfo.Step7.StartDate,
            EndDate: nextProps.valueInfo.Step7.EndDate,
            LocationLat: nextProps.valueInfo.Step7.LocationLat,
            LocationLong: nextProps.valueInfo.Step7.LocationLong
          }
        },
        "7_1": {
          process: 7,
          step: 1,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Machine: nextProps.valueInfo.Step7.Machine,
            MachineId: nextProps.valueInfo.Step7.Machine == null ? "" : nextProps.valueInfo.Step7.Machine.itemid,
            DriverId: nextProps.valueInfo.Step7.DriverId,
            DriverName: nextProps.valueInfo.Step7.DriverName
          },
          masterInfo: {
            MachineList: nextProps.masterInfo.Step7.MachineList,
            DriverList: nextProps.masterInfo.Step7.DriverList
          }
        },
        "7_2": {
          process: 7,
          step: 2,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            OverLifting: nextProps.valueInfo.Step7.OverLifting,
            Pcoring: nextProps.valueInfo.Step7.Pcoring,
            ImageLengthSteelCarry: nextProps.valueInfo.Step7.ImageLengthSteelCarry,
            HangingBarsLength: nextProps.valueInfo.Step7.HangingBarsLength
          },
          masterInfo: {
            OverLifting: nextProps.masterInfo.Step7.OverLifting,
            Pcoring: nextProps.masterInfo.Step7.Pcoring
          }
        },
        "7_3": {
          process: 7,
          step: 3,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            SectionDetail: section73,
            SectionList: nextProps.valueInfo.Step7.SectionList,
            concretedisabled: nextProps.masterInfo.PileDetail.shape == 1
            // MachineList: nextProps.valueInfo.Step7.MachineList,
            // DriverList: nextProps.valueInfo.Step7.DriverList
          },
          masterInfo: {
            Steelcage: nextProps.masterInfo.Step7.Steelcage,
            Shape: nextProps.masterInfo.PileDetail.shape,
            rbsheet:nextProps.masterInfo.Step7.rbsheet
          },
          startdate:nextProps.valueInfo.Step7.StartDate,
          enddate:nextProps.valueInfo.Step7.EndDate
        },
        "8_0": {
          process: 8,
          step: 0,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Id: nextProps.valueInfo.Step8.Id,
            Edit_Flag:this.props.viewType == 0 ? 0 :  nextProps.valueInfo.Step8.Edit_Flag,
            Status: nextProps.valueInfo.Step8.Status,
            StartDate: nextProps.valueInfo.Step8.StartDate,
            EndDate: nextProps.valueInfo.Step8.EndDate,
            LocationLat: nextProps.valueInfo.Step8.LocationLat,
            LocationLong: nextProps.valueInfo.Step8.LocationLong
          }
        },
        "8_1": {
          process: 8,
          step: 1,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Machine: nextProps.valueInfo.Step8.Machine,
            MachineId: nextProps.valueInfo.Step8.Machine == null ? "" : nextProps.valueInfo.Step8.Machine.itemid,
            DriverId: nextProps.valueInfo.Step8.DriverId,
            DriverName: nextProps.valueInfo.Step8.DriverName,
            TremieTypeId: nextProps.valueInfo.Step8.TremieTypeId,
            TremieTypeName: nextProps.valueInfo.Step8.TremieTypeName
          },
          masterInfo: {
            MachineList: nextProps.masterInfo.Step8.MachineList,
            DriverList: nextProps.masterInfo.Step8.DriverList,
            TremieTypeList:nextProps.masterInfo.Step8.TremieTypeList
          }
        },
        "8_2": {
          process: 8,
          step: 2,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            TremieList: nextProps.valueInfo.Step8.TremieList
          },
          masterInfo: {
            TremieSize: nextProps.masterInfo.Step8.TremieSize
          }
        },
        "8_3": {
          process: 8,
          step: 3,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            foamimage: nextProps.valueInfo.Step8.Images,
            checkfoam: nextProps.valueInfo.Step8.AddFoam
          },
          masterInfo: {},
          startdate:nextProps.valueInfo.Step8.StartDate,
          enddate:nextProps.valueInfo.Step8.EndDate
        },
        "9_0": {
          process: 9,
          step: 0,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Id: nextProps.valueInfo.Step9.Id,
            Edit_Flag:this.props.viewType == 0 ? 0 :  nextProps.valueInfo.Step9.Edit_Flag,
            Status: nextProps.valueInfo.Step9.Status,
            StartDate: nextProps.valueInfo.Step9.StartDate,
            EndDate: nextProps.valueInfo.Step9.EndDate,
            LocationLat: nextProps.valueInfo.Step9.LocationLat,
            LocationLong: nextProps.valueInfo.Step9.LocationLong
          }
        },
        "9_1": {
          process: 9,
          step: 1,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Id: nextProps.valueInfo.Step9.Id,
            Edit_Flag:this.props.viewType == 0 ? 0 :  nextProps.valueInfo.Step9.Edit_Flag,
            Status: nextProps.valueInfo.Step9.Status,
            StartDate: nextProps.valueInfo.Step9.StartDate,
            EndDate: nextProps.valueInfo.Step9.EndDate,
            LocationLat: nextProps.valueInfo.Step9.LocationLat,
            LocationLong: nextProps.valueInfo.Step9.LocationLong,
            ConcreteTruckRegisterList: nextProps.valueInfo.Step9.ConcreteTruckRegisterList
          },
          masterInfo: {
            ConcreteBrandList: nextProps.masterInfo.Step9.ConcreteBrandList,
            ConcreteInfo: nextProps.masterInfo.Step9.ConcreteInfo,
            ApprovedUser: nextProps.masterInfo.Step9.ApprovedUser,
            PileDetail: nextProps.masterInfo.PileDetail,
            
          }
        },
        "10_0": {
          process: 10,
          step: 0,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Id: nextProps.valueInfo.Step10.Id,
            Edit_Flag:this.props.viewType == 0 ? 0 :  nextProps.valueInfo.Step10.Edit_Flag,
            Status: nextProps.valueInfo.Step10.Status,
            StartDate: nextProps.valueInfo.Step10.StartDate,
            EndDate: nextProps.valueInfo.Step10.EndDate,
            LocationLat: nextProps.valueInfo.Step10.LocationLat,
            LocationLong: nextProps.valueInfo.Step10.LocationLong
          }
        },
        "10_1": {
          process: 10,
          step: 1,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            ForemanSet: nextProps.valueInfo.Step10.ForemanSet,
            ForemanId: nextProps.valueInfo.Step10.ForemanId,
            Overcast:nextProps.valueInfo.Step10.ConcreteOvercast
          },
          masterInfo: {
            Foreman: nextProps.masterInfo.Step10.Foreman,
            ForemanSet: nextProps.masterInfo.Step10.ForemanSet
          }
        },
        "10_2": {
          process: 10,
          step: 2,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Weather: nextProps.valueInfo.Step10.Weather,
            IsFine : nextProps.valueInfo.Step10.IsFine,
            IsCloudy : nextProps.valueInfo.Step10.IsCloudy,
            IsRainy : nextProps.valueInfo.Step10.IsRainy,
            IsHail : nextProps.valueInfo.Step10.IsHail,
            IsWindy : nextProps.valueInfo.Step10.IsWindy
          },
          masterInfo: {}
        },
        "10_3": {
          process: 10,
          step: 3,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Weather: nextProps.valueInfo.Step10.Weather,
            TopCasing: nextProps.valueInfo.PileSurvey_TopCasingLevel,
            LastDrillingDepth	: nextProps.valueInfo.Step10.LastDrillingDepth
          },
          masterInfo: {
            TheoreticalVolume: nextProps.masterInfo.Step10.TheoreticalVolume,
            PileInfo: nextProps.masterInfo.Step10.PileInfo,
            size: nextProps.masterInfo.PileDetail.sizecal,
            size_bp: nextProps.masterInfo.PileDetail.sizecal_bp,
            RemainConcrete:nextProps.masterInfo.Step10.RemainConcrete
          }
        },
        "11_0": {
          process: 11,
          step: 0,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Id: nextProps.valueInfo.Step11.Id,
            Edit_Flag:this.props.viewType == 0 ? 0 :  nextProps.valueInfo.Step11.Edit_Flag,
            Status: nextProps.valueInfo.Step11.Status,
            StartDate: nextProps.valueInfo.Step11.StartDate,
            EndDate: nextProps.valueInfo.Step11.EndDate,
            LocationLat: nextProps.valueInfo.Step11.LocationLat,
            LocationLong: nextProps.valueInfo.Step11.LocationLong
          }
        },
        "11_1": {
          process: 11,
          step: 1,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            Machine: nextProps.valueInfo.Step11.Machine,
            Vibro: nextProps.valueInfo.Step11.Vibro,
            DriverId: nextProps.valueInfo.Step11.DriverId
          },
          masterInfo: {
            CraneList: nextProps.masterInfo.Step11.CraneList,
            VibroList: nextProps.masterInfo.Step11.VibroList,
            DriverList: nextProps.masterInfo.Step11.DriverList
          }
        },
        "11_2": {
          process: 11,
          step: 2,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            CheckPlummet: nextProps.valueInfo.Step11.CheckPlummet,
            CheckWaterLevel: nextProps.valueInfo.Step11.CheckWaterLevel,
            ImagePlummet: nextProps.valueInfo.Step11.ImagePlummet,
            ImageWaterLevel: nextProps.valueInfo.Step11.ImageWaterLevel
          },
          startdate:nextProps.valueInfo.Step11.StartDate,
          enddate:nextProps.valueInfo.Step11.EndDate
        },
        "11_3": {
          process: 11,
          step: 3,
          pile_id: nextProps.valueInfo.pile_id,
          data: {
            ConcreteLevelBeforeWithdraw: nextProps.valueInfo.Step11.ConcreteLevelBeforeWithdraw,
            ConcreteLevelAfterWithdraw: nextProps.valueInfo.Step11.ConcreteLevelAfterWithdraw,
            ConcreteBleeding: nextProps.valueInfo.Step11.ConcreteBleeding,
            ground: nextProps.valueInfo.PileSurvey_GroundLevel
          },
          masterInfo: {
            cutoff: nextProps.masterInfo.PileDetail.cutoff
          }
        }
      }
      if(nextProps.masterInfo.PileDetail.pile_id != nextProps.masterInfo.PileDetail.parent_id){
        // console.warn('parent',nextProps.masterInfo.PileDetail)
        
        this.setState({parentFalg:true,parentNo:nextProps.masterInfo.PileDetail.parent_no})
      }
      console.log('nextProps.valueInfo',nextProps.valueInfo)
      await this.props.mainPileSetStorageNoneRedux(nextProps.valueInfo.pile_id, data)

      // console.log(this.state.stepStatus);
      this.setState({ stepStatus: value_step_status, isStart: false, pileValueInfo: nextProps.valueInfo}, () =>
        this.setCurrentActivity(this.state.stepStatus)
      )
      // this.setCurrentActivity(this.state.stepStatus)

      this.props.pileValueInfoReset()
      this.props.mainPileGetAllStorage()
    } else if (pile != null) {
      this.props.mainPileGetAllStorage()
    }
    if (pile != null && nextProps.step_status != null) {
      this.setState({ stepStatus: pile.step_status, isStart: false })
      this.setCurrentActivity(this.state.stepStatus)
    } else if (pile != null && nextProps.step_status == null) {
      if (nextProps.valueInfo != null && pile == null) {
        var value_step_status = {
          Step1Status: nextProps.valueInfo.Step1Status,
          Step2Status: nextProps.valueInfo.Step2Status,
          Step3Status: nextProps.valueInfo.Step3Status,
          Step4Status: nextProps.valueInfo.Step4Status,
          Step5Status: nextProps.valueInfo.Step5Status,
          Step6Status: nextProps.valueInfo.Step6Status,
          Step7Status: nextProps.valueInfo.Step7Status,
          Step8Status: nextProps.valueInfo.Step8Status,
          Step9Status: nextProps.valueInfo.Step9Status,
          Step10Status: nextProps.valueInfo.Step10Status,
          Step11Status: nextProps.valueInfo.Step11Status
        }
        this.setState({ stepStatus: value_step_status, isStart: false }, () =>
          this.setCurrentActivity(this.state.stepStatus)
        )
        // await this.setCurrentActivity(this.state.stepStatus)
      } else {
        this.setState({ stepStatus: pile.step_status, isStart: false }, () =>
          this.setCurrentActivity(this.state.stepStatus)
        )
        // await this.setCurrentActivity(this.state.stepStatus)
      }
    } else {
      if ((nextProps.valueInfo == null && pile == null && nextProps.step_status != null) || pile != undefined) {
        this.setState({ stepStatus: nextProps.step_status }, () => this.setCurrentActivity(this.state.stepStatus))
        //  await this.setCurrentActivity(this.state.stepStatus)
      } else {
        if (nextProps.valueInfo != null && nextProps.hidden == false) {
          var value_step_status = {
            Step1Status: nextProps.valueInfo.Step1Status,
            Step2Status: nextProps.valueInfo.Step2Status,
            Step3Status: nextProps.valueInfo.Step3Status,
            Step4Status: nextProps.valueInfo.Step4Status,
            Step5Status: nextProps.valueInfo.Step5Status,
            Step6Status: nextProps.valueInfo.Step6Status,
            Step7Status: nextProps.valueInfo.Step7Status,
            Step8Status: nextProps.valueInfo.Step8Status,
            Step9Status: nextProps.valueInfo.Step9Status,
            Step10Status: nextProps.valueInfo.Step10Status,
            Step11Status: nextProps.valueInfo.Step11Status
          }
          this.setState({ stepStatus: value_step_status, isStart: false }, () =>
            this.setCurrentActivity(this.state.stepStatus)
          )
          // this.setCurrentActivity(this.state.stepStatus)
        }
      }
    }
    // }
    if(this.props.fromnoti && this.state.checkfromnoti == false){
      this.props.clearStatus()
      this.props.pileClear()
      this.props.setStack({
        process: 3,
        step: 4
      })
      this.props.mainRefresh(0)
      this.setState({checkfromnoti:true})
    }
  }

  setCurrentActivity(stepStatus) {
    var page = 0
    if (
      stepStatus != null &&
      this.state.isStart == false &&
      this.state.loading == true &&
      this.props.action == "start"
    ) {
      /**
       * For dev to not fuckup release build
       */
      if (__DEV__) {
        page = 9
      }
      if(this.props.category == 2){
        if(stepStatus.Step1Status == 0 && stepStatus.Step2Status == 0 && stepStatus.Step3Status == 0
          && stepStatus.Step4Status == 0 && stepStatus.Step5Status == 0 && stepStatus.Step6Status == 0
          && stepStatus.Step7Status == 0 && stepStatus.Step8Status == 0 && stepStatus.Step9Status == 0
          && stepStatus.Step10Status == 0 && stepStatus.Step11Status == 0){
            if (this.props.position == "survey") {
              page = 2
            }else {
              page = 0
            }
        }else{
          if ((stepStatus.Step10Status == 1 || stepStatus.Step10Status == 0)&& stepStatus.Step9Status == 2) {
            page = 9
          }
          else if ((stepStatus.Step9Status == 1 || stepStatus.Step9Status == 0)&& stepStatus.Step8Status == 2) {
            page = 8  
          }
          else if ((stepStatus.Step8Status == 1 || stepStatus.Step8Status == 0)&& stepStatus.Step7Status == 2) {
            if(this.props.category == 3){
              page = 6
            }else{
              page = 7
            }
            
          }
          else if ((stepStatus.Step7Status == 1 || stepStatus.Step7Status == 0)&& stepStatus.Step5Status == 2 ) {
            if(this.props.category == 3){
              page = 4
            }else{
              if(stepStatus.Step1Status == 2){
                page = 6
              }else{
                page = 0
              }
              
            }
           
          }
          else if ((stepStatus.Step5Status == 1 || stepStatus.Step5Status == 0)&& stepStatus.Step4Status != 0) {
            if(stepStatus.Step4Status == 1){
              page = 3
            }else{
              page = 4
            } 
          }
          else if ((stepStatus.Step4Status == 1 || stepStatus.Step4Status == 0)&& stepStatus.Step3Status != 0) {
            if(stepStatus.Step3Status == 1){
              if(this.props.pileid != this.props.sp_parentid){
                page = 3
              }else{
                page = 2
              }
            }else{
              page = 3
            }
          }
          else if ((stepStatus.Step3Status == 1 || stepStatus.Step3Status == 0)&& stepStatus.Step1Status != 0) {
            if(stepStatus.Step1Status == 1){
              page = 0
            }else{
              if(this.props.pileid != this.props.sp_parentid){
                page = 3
              }else{
                page = 2
              }
            }
          }
          // else if ((stepStatus.Step2Status == 1 || stepStatus.Step2Status == 0) && stepStatus.Step1Status == 2) {
          //   page = 2
          // }
          else{
            page = 0
          }
        }
      }
      else if(this.props.category == 3){
        if(stepStatus.Step1Status == 0 && stepStatus.Step2Status == 0 && stepStatus.Step3Status == 0
          && stepStatus.Step4Status == 0 && stepStatus.Step5Status == 0 && stepStatus.Step6Status == 0
          && stepStatus.Step7Status == 0 && stepStatus.Step8Status == 0 && stepStatus.Step9Status == 0
          && stepStatus.Step10Status == 0 && stepStatus.Step11Status == 0){
            if (this.props.position == "survey") {
              page = 2
            }else {
              page = 0
            }
        }else{
          if ((stepStatus.Step10Status == 1 || stepStatus.Step10Status == 0)&& stepStatus.Step9Status == 2) {
            page = 9
          }
          else if ((stepStatus.Step9Status == 1 || stepStatus.Step9Status == 0)&& stepStatus.Step8Status == 2) {
            page = 8  
          }
          else if ((stepStatus.Step8Status == 1 || stepStatus.Step8Status == 0)&& stepStatus.Step7Status == 2) {
            if(this.props.category == 3){
              page = 6
            }else{
              page = 7
            }
            
          }
          else if ((stepStatus.Step7Status == 1 || stepStatus.Step7Status == 0)&& stepStatus.Step5Status == 2 ) {
            if(this.props.category == 3){
              page = 4
            }else{
              if(stepStatus.Step1Status == 2){
                page = 6
              }else{
                page = 0
              }
              
            }
           
          }
          else if ((stepStatus.Step5Status == 1 || stepStatus.Step5Status == 0)&& stepStatus.Step4Status != 0) {
            if(stepStatus.Step4Status == 1){
              page = 3
            }else{
              page = 4
            } 
          }
          else if ((stepStatus.Step4Status == 1 || stepStatus.Step4Status == 0)&& stepStatus.Step3Status != 0) {
            if(stepStatus.Step3Status == 1){
              if(this.props.pileid != this.props.sp_parentid){
                page = 3
              }else{
                page = 2
              }
            }else{
              page = 3
            }
          }
          else if ((stepStatus.Step3Status == 1 || stepStatus.Step3Status == 0)&& stepStatus.Step1Status != 0) {
            if(stepStatus.Step1Status == 1){
              page = 0
            }else{
              if(this.props.pileid != this.props.sp_parentid){
                page = 3
              }else{
                page = 2
              }
            }
          }
          // else if ((stepStatus.Step2Status == 1 || stepStatus.Step2Status == 0) && stepStatus.Step1Status == 2) {
          //   page = 2
          // }
          else{
            page = 0
          }
        }
      }else if(this.props.category == 1){
        if(stepStatus.Step1Status == 0 && stepStatus.Step2Status == 0 && stepStatus.Step3Status == 0
          && stepStatus.Step4Status == 0 && stepStatus.Step5Status == 0 && stepStatus.Step6Status == 0
          && stepStatus.Step7Status == 0 && stepStatus.Step8Status == 0 && stepStatus.Step9Status == 0
          && stepStatus.Step10Status == 0 && stepStatus.Step11Status == 0){
            if (this.props.position == "survey") {
              page = 2
            }else {
              page = 0
            }
        }else{
          if ((stepStatus.Step11Status == 1 || stepStatus.Step11Status == 0) && stepStatus.Step10Status == 2) {
            page = 10
          }
          else if ((stepStatus.Step10Status == 1 || stepStatus.Step10Status == 0)&& stepStatus.Step9Status == 2) {
            page = 9
          }
          else if ((stepStatus.Step9Status == 1 || stepStatus.Step9Status == 0)&& stepStatus.Step8Status == 2) {
            page = 8
          }
          else if ((stepStatus.Step8Status == 1 || stepStatus.Step8Status == 0)&& stepStatus.Step7Status == 2) {
            page = 7
          }
          else if ((stepStatus.Step7Status == 1 || stepStatus.Step7Status == 0)&& stepStatus.Step6Status == 2) {
            if(stepStatus.Step1Status == 2){
              page = 6
            }else{
              page = 0
            }
           
          }
          else if ((stepStatus.Step6Status == 1 || stepStatus.Step6Status == 0)&& stepStatus.Step5Status == 2) {
            if(stepStatus.Step1Status == 2){
              page = 5
            }else{
              page = 0
            }
            
          }
          else if ((stepStatus.Step5Status == 1 || stepStatus.Step5Status == 0)&& stepStatus.Step4Status != 0) {
            if(stepStatus.Step4Status == 1){
              page = 3
            }else{
              page = 4
            } 
          }
          else if ((stepStatus.Step4Status == 1 || stepStatus.Step4Status == 0)&& stepStatus.Step3Status != 0) {
            if(stepStatus.Step3Status == 1){
              page = 2
            }else{
              page = 3
            }   
          }
          else if ((stepStatus.Step3Status == 1 || stepStatus.Step3Status == 0)&& stepStatus.Step2Status != 0) {
            if(stepStatus.Step2Status == 1){
              page = 1
            }else{
              page = 2
            }
          }
          else if ((stepStatus.Step2Status == 1 || stepStatus.Step2Status == 0) && stepStatus.Step1Status != 0) {
            if(stepStatus.Step1Status == 1){
              page = 0
            }else{
              page = 1
            }
          }
          else{
            page = 0
          }
        }
      }else if(this.props.category == 4){
          if ((stepStatus.Step11Status == 1 || stepStatus.Step11Status == 0) && stepStatus.Step10Status == 2) {
            page = 10
          }
          else if ((stepStatus.Step10Status == 1 || stepStatus.Step10Status == 0)&& stepStatus.Step9Status == 2) {
            page = 9
          }
          else if ((stepStatus.Step9Status == 1 || stepStatus.Step9Status == 0)&& stepStatus.Step8Status == 2) {
            page = 8
          }
          else if ((stepStatus.Step8Status == 1 || stepStatus.Step8Status == 0)&& stepStatus.Step7Status == 2) {
            page = 7
          }
          else if ((stepStatus.Step7Status == 1 || stepStatus.Step7Status == 0)&& stepStatus.Step5Status == 2) {
            if(stepStatus.Step1Status == 2){
              page = 6
            }else{
              page = 0
            }
          
          }
          // else if ((stepStatus.Step6Status == 1 || stepStatus.Step6Status == 0)&& stepStatus.Step5Status == 2) {
          //   page = 5
          // }
          else if ((stepStatus.Step5Status == 1 || stepStatus.Step5Status == 0)&& stepStatus.Step3Status != 0) {
            if(stepStatus.Step3Status == 1){
              page = 2
            }else{
              page = 4
            } 
          }
          else if ((stepStatus.Step3Status == 1 || stepStatus.Step3Status == 0)&& stepStatus.Step2Status != 0) {
            if(stepStatus.Step2Status == 1){
              page = 1
            }else{
              page = 2
            }
          }
          else if ((stepStatus.Step2Status == 1 || stepStatus.Step2Status == 0) && stepStatus.Step1Status != 0) {
            if(stepStatus.Step1Status == 1){
              page = 0
            }else{
              page = 1
            }
          }
          else{
            page = 0
          }
        }else{
          if(this.state.pile.shape == 2){
            if(stepStatus.Step1Status == 0 && stepStatus.Step2Status == 0 && stepStatus.Step3Status == 0
              && stepStatus.Step4Status == 0 && stepStatus.Step5Status == 0 && stepStatus.Step6Status == 0
              && stepStatus.Step7Status == 0 && stepStatus.Step8Status == 0 && stepStatus.Step9Status == 0
              && stepStatus.Step10Status == 0 && stepStatus.Step11Status == 0){
                if (this.props.position == "survey") {
                  page = 2
                }else {
                  page = 0
                }
            }else{
              if ((stepStatus.Step10Status == 1 || stepStatus.Step10Status == 0)&& stepStatus.Step9Status == 2) {
                page = 9
              }
              else if ((stepStatus.Step9Status == 1 || stepStatus.Step9Status == 0)&& stepStatus.Step8Status == 2) {
                page = 8  
              }
              else if ((stepStatus.Step8Status == 1 || stepStatus.Step8Status == 0)&& stepStatus.Step7Status == 2) {
              
                  page = 6
                
                
              }
              else if ((stepStatus.Step7Status == 1 || stepStatus.Step7Status == 0)&& stepStatus.Step5Status == 2 ) {
                
                  if(stepStatus.Step1Status == 2){
                    page = 4
                  }else{
                    page = 0
                  }
                  
                
               
              }
              else if ((stepStatus.Step5Status == 1 || stepStatus.Step5Status == 0)&& stepStatus.Step4Status != 0) {
                if(stepStatus.Step4Status == 1){
                  page = 3
                }else{
                  page = 4
                } 
              }
              else if ((stepStatus.Step4Status == 1 || stepStatus.Step4Status == 0)&& stepStatus.Step3Status != 0) {
                if(stepStatus.Step3Status == 1){
                  if(this.props.pileid != this.props.sp_parentid){
                    page = 3
                  }else{
                    page = 2
                  }
                }else{
                  page = 3
                }
              }
              else if ((stepStatus.Step3Status == 1 || stepStatus.Step3Status == 0)&& stepStatus.Step1Status != 0) {
                if(stepStatus.Step1Status == 1){
                  page = 0
                }else{
                  if(this.props.pileid != this.props.sp_parentid){
                    page = 3
                  }else{
                    page = 2
                  }
                }
              }
              else{
                page = 0
              }
            }

          }else{
            if(stepStatus.Step1Status == 0 && stepStatus.Step2Status == 0 && stepStatus.Step3Status == 0
              && stepStatus.Step4Status == 0 && stepStatus.Step5Status == 0 && stepStatus.Step6Status == 0
              && stepStatus.Step7Status == 0 && stepStatus.Step8Status == 0 && stepStatus.Step9Status == 0
              && stepStatus.Step10Status == 0 && stepStatus.Step11Status == 0){
                if (this.props.position == "survey") {
                  page = 2
                }else {
                  page = 0
                }
            }else{
              if ((stepStatus.Step10Status == 1 || stepStatus.Step10Status == 0)&& stepStatus.Step9Status == 2) {
                page = 9
              }
              else if ((stepStatus.Step9Status == 1 || stepStatus.Step9Status == 0)&& stepStatus.Step8Status == 2) {
                page = 8
              }
              else if ((stepStatus.Step8Status == 1 || stepStatus.Step8Status == 0)&& stepStatus.Step7Status == 2) {
                page = 7
              }
              else if ((stepStatus.Step7Status == 1 || stepStatus.Step7Status == 0)&& stepStatus.Step6Status == 2) {
                if(stepStatus.Step1Status == 2){
                  page = 5
                }else{
                  page = 0
                }
               
              }
              else if ((stepStatus.Step6Status == 1 || stepStatus.Step6Status == 0)&& stepStatus.Step5Status == 2) {
                page = 4
              }
              else if ((stepStatus.Step5Status == 1 || stepStatus.Step5Status == 0)&& stepStatus.Step4Status != 0) {
                if(stepStatus.Step4Status == 1){
                  page = 3
                }else{
                  page = 4
                } 
              }
              else if ((stepStatus.Step4Status == 1 || stepStatus.Step4Status == 0)&& stepStatus.Step3Status != 0) {
                if(stepStatus.Step3Status == 1){
                  if(this.props.pileid != this.props.sp_parentid){
                    page = 3
                  }else{
                    page = 2
                  }
                }else{
                  page = 3
                }   
              }
              else if ((stepStatus.Step3Status == 1 || stepStatus.Step3Status == 0)&& stepStatus.Step1Status != 0) {
                if(stepStatus.Step1Status == 1){
                  page = 0
                }else{
                  if(this.props.pileid != this.props.sp_parentid){
                    page = 3
                  }else{
                    page = 2
                  }
                }
                  
              }
              else{
                page = 0
              }
            }
          }
        }
      
      if (this.props.toProcess) {
        page = this.props.toProcess
      }
      this.setState({ firstItem: page, isStart: true, index: page, loading: false }, () => {
        console.log("Finish loading")
      })
      var data = {
        process: page + 1,
        step: 1
      }
      if (this.props.stack == "") {
        this.props.setStack(data)
      }
    } else {
      if (this.props.action == "refresh" && this.state.loading == true && this.state.isStart == false) {
        // this.setState({loading:true},()=>{
        setTimeout(async () => {
        var pile_process = this.props.stack[this.props.stack.length - 1].process - 1
        this.setState({ firstItem: pile_process, index: pile_process, loading: false, isStart: true })
        }, 3000)
        // })
      }
    }
  }

  async saveLocation() {
    // navigator.geolocation.watchPosition(async position => {
    //   let lat = position.coords.latitude
    //   let log = position.coords.longitude
    //   let data = {
    //     position: {
    //       lat: lat,
    //       log: log
    //     }
    //   }
    //   await this.props.mainPileSetStorageNoneRedux("currentLocation", data)
    //   // console.warn(lat,log)
    // })

    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
          title: 'App needs to access your location',
          message: 'App needs access to your location ' +
          'so we can let our app be even more awesome.'
          }
      )
      if (granted) {
        FusedLocation.setLocationPriority(FusedLocation.Constants.HIGH_ACCURACY)
        const location = await FusedLocation.getFusedLocation()
        this.props.setlocation(location)
        if(location == undefined){
          let lat = this.props.location_lat
          let log = this.props.location_log
           data = {
            position:{
              lat:lat,
              log:log
            }
          }
        }else{
          let lat = location.latitude
          let log = location.longitude
          data = {
            position:{
              lat:lat,
              log:log
            }
          }
        }
        await this.props.mainPileSetStorageNoneRedux('currentLocation',data)
       
      }else{

      }
  }

  _handleBack() {
    /** Disabled back temporary */
    // console.warn(Actions.currentScene)
    if (Actions.currentScene == "_mainpile") {
      Alert.alert("Back", I18n.t('mainpile.alert.backtopile'), [
        {
          text: "Cancel",
          onPress: () => {},
          style: "cancel"
        },
        {
          text: "Ok",
          onPress: () => {
            this.props.ungetStepStatus2()
            this.props.pileClear()
            this.props.mainpileClear()
            Actions.projectlist({ result: ''})
          }
        }
      ])
      return true
    }
    else {
      Actions.pop()
      return true
    }

  }

  fetchData() {
    console.log("fetchData", this.props.jobid + " " + this.props.pileid)
    // Actions.refresh({
    //     action: 'start',
    // })
    // this.props.pileMasterInfo({ jobid: this.props.jobid, pileid: this.props.pileid })
    this.props.pileMasterInfo({ jobid: this.props.jobid, pileid: this.props.pileid, Language:I18n.locale })
    this.props.pileValueInfo({ jobid: this.props.jobid, pileid: this.props.pileid })
    this.props.pileItem({ jobid: this.props.jobid, pileid: this.props.pileid , Language:I18n.locale})
  }

  onCheckInternet() {
    // NetInfo.isConnected.fetch().then(isConnected => {
    //   console.log("First, is " + (isConnected ? "online" : "offline"))
    // })
  }

  _getColorItem(type) {
    // Temporary grey
    if (type == 0) {
      return {}
    } else if (type == 2) {
      return {
        borderColor: MENU_GREEN_BORDER,
        backgroundColor: MENU_GREEN_ITEM
      }
    } else {
      return {
        borderColor: MENU_ORANGE_BORDER,
        backgroundColor: MENU_ORANGE_ITEM
      }
    }
  }

  _getColor(type) {
    // Temporary grey
    if (type == 0) {
      return {}
    } else if (type == 2) {
      return {
        borderColor: MENU_GREEN_BORDER,
        backgroundColor: MENU_GREEN
      }
    } else {
      return {
        borderColor: MENU_ORANGE_BORDER,
        backgroundColor: MENU_ORANGE
      }
    }
  }

  async _alertNextStep(title, index) {
    // BY PASS STEP NOT CHECKING DEPENDENCY
    // var BYPASS = true
    // if (!__DEV__) {
      var BYPASS = false
    // }
    // console.warn('index',index)
   if(index == 2 && (this.props.category == 5 || this.props.category == 3) && (this.props.pileid != this.props.sp_parentid)){
    Alert.alert(
      "",
      I18n.t('mainpile.lockprocess.error9'),
      [{ text: "OK" ,onPress: () => this._carousel.snapToItem(this.state.index)}],
      { cancelable: false }
    )
   }else if(index == 1 && this.props.category == 5){
      Alert.alert(
        "",
        I18n.t('mainpile.lockprocess.error6'),
        [{ text: "OK" ,onPress: () => this._carousel.snapToItem(this.state.index)}],
        { cancelable: false }
      )
    }
    else if (index == 1 && this.state.pile.shape == 2) {
      Alert.alert(
        "",
        I18n.t('mainpile.lockprocess.error1'),
        [{ text: "OK" , onPress: () => this._carousel.snapToItem(this.state.index) }],
        { cancelable: false }
      )
    }  
    else if(index == 3 && this.props.category == 4){
      Alert.alert(
        "",
        I18n.t('mainpile.lockprocess.error4'),
        [{ text: "OK" ,onPress: () => this._carousel.snapToItem(this.state.index)}],
        { cancelable: false }
      )
    } else if(index == 5 && this.props.category == 4){
      Alert.alert(
        "",
        I18n.t('mainpile.lockprocess.error5'),
        [{ text: "OK" ,onPress: () => this._carousel.snapToItem(this.state.index)}],
        { cancelable: false }
      )
    }
    else if (index == 5 && this.props.category == 5 && this.state.pile.shape == 2) {
      Alert.alert(
        "",
        I18n.t('mainpile.lockprocess.error8'),
        [{ text: "OK" ,onPress: () => this._carousel.snapToItem(this.state.index)}],
        { cancelable: false }
      )
      console.log("block")
    }
    else if (index == 5 && this.state.pile.shape == 2) {
      Alert.alert(
        "",
        I18n.t('mainpile.lockprocess.error2'),
        [{ text: "OK" ,onPress: () => this._carousel.snapToItem(this.state.index)}],
        { cancelable: false }
      )
      console.log("block")
    }else if(index == 10 && this.props.category == 5){
      Alert.alert(
        "",
        I18n.t('mainpile.lockprocess.error7'),
        [{ text: "OK" ,onPress: () => this._carousel.snapToItem(this.state.index)}],
        { cancelable: false }
      )
    }
    else if (index == 10 && this.state.pile.shape == 2) {
      Alert.alert(
        "",
        I18n.t('mainpile.lockprocess.error3'),
        [{ text: "OK" ,onPress: () => this._carousel.snapToItem(this.state.index)}],
        { cancelable: false }
      )
      console.log("block")
    } else if(!BYPASS && (index == 4 || index == 5 || index == 6 || index == 7 || index == 9 || index == 10)){ // blockstep
      console.warn('lock',this.props.category,index)
      if(this.props.category == 3 && (index == 7 || index == 6)){
        if(index == 7){
          var valueApi = {
            Language: I18n.currentLocale(),
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            lockstep:true
          }
          this.props.pileSaveStep07super(valueApi)
        }else if(index == 6){
          var valueApi = {
            Language: I18n.currentLocale(),
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            lockstep:true
          }
          this.props.pileCheckStep07super(valueApi)
        }
        
      }else if((this.props.category == 5 || this.props.category == 3) && index == 4){
        // console.warn('chhhhh')
        var valuecheck = {
          Language:I18n.locale,
          JobId: this.props.jobid,
          PileId: this.props.pileid,
          lockstep:true
        }
        this.props.checkstepofsuperstructure5(valuecheck)
      }
      else if(this.props.category == 5 && index == 5){
        var valuecheck ={
          Language:I18n.locale,
          JobId: this.props.jobid,
          PileId: this.props.pileid,
          lockstep:true
        }
        this.props.pileCheckStep06super(valuecheck)
      }else if(this.props.category == 5 && index == 6){
        var valuecheck ={
          Language:I18n.locale,
          JobId: this.props.jobid,
          PileId: this.props.pileid,
          lockstep:true
        }
        this.props.pileCheckStep07super(valuecheck)
      }
      else if(this.props.category == 5 && index == 7){
        var valuecheck ={
          Language:I18n.locale,
          JobId: this.props.jobid,
          PileId: this.props.pileid,
          lockstep:true
        }
        this.props.pileSaveStep07super(valuecheck)
        
      }
      else{
        var value = {
          process: index + 1,
          step: 0,
          pile_id: this.props.pileid,
          shape: this.state.pile.shape,
          drillingFluids: this.props.drillingfluidslist,
          drillinglist: this.props.drillinglist,
          category:this.props.category,
          sp_parentid: this.props.sp_parentid
        }
        this.props.mainpileAction_checkStepStatus(value).then(data => {
          var temp_c = ''
          if(this.state.pile.shape == 1){
            if(this.props.category == 4){
              temp_c = "d"
            }else if(this.props.category == 5){
              temp_c = "i"
            }else{
              temp_c = "bp"
            }
            
          }else{
            if(this.props.category == 5){
              temp_c = "i"
            }else{
              temp_c = "dw"
            }
          }
          const text = "mainpile.lockprocess.process" + (index + 1) + "_" + (temp_c)
          if (data.check == true) {
            // console.warn('Alert up')
            Alert.alert("", I18n.t("mainpile.button.goto") + " " + title, [
              { text: "Cancel", onPress: () => {}, style: "cancel" },
              {
                text: "Ok",
                onPress: async () => {
                  // console.warn('Ok Press')
                  if((this.props.category == 3 ||this.props.category == 5) && this.state.parentFalg==true && index == 9){
                      Alert.alert("",I18n.t('alert.errorParentcheck10')+" "+this.state.parentNo,[
                        {
                          text:"Ok",
                          onPress: ()=> {
                            this.alertNext(index)
                          }
                        }
                      ])
                  }else{
                    // console.warn(index,this.props.category,this.props.pileid,this.props.sp_parentid)
                    if(index == 4 && this.props.category == 5 && (this.props.pileid != this.props.sp_parentid)){
                      Alert.alert("",I18n.t('mainpile.lockprocess.error10'),[
                        {
                          text:"Ok",
                          onPress: ()=> {
                            this.alertNext(index)
                          }
                        }
                      ])
                    }else if(index == 7 && (this.props.category == 5 || this.props.category == 3) && (this.props.pileid != this.props.sp_parentid)){
                      Alert.alert("",I18n.t('mainpile.lockprocess.error11'),[
                        {
                          text:"Ok",
                          onPress: ()=> {
                            this.alertNext(index)
                          }
                        }
                      ])
                    }
                    else{
                      this.alertNext(index)
                    }
                  }
                }
              }
            ])

          } else {
            // console.warn('cate',this.props.category)
            Alert.alert("", I18n.t(text), [
              {
                text: "OK", onPress: () => this._carousel.snapToItem(this.state.index)
              }
            ])
          }
        })
      } 
    }else {
      // console.warn('lock2',this.props.category,index)
      if(this.props.category == 3 && (index == 7 || index == 6)){
        if(index == 7){
          var valueApi = {
            Language: I18n.currentLocale(),
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            lockstep:true
          }
          this.props.pileSaveStep07super(valueApi)
        }else if(index == 6){
          var valueApi = {
            Language: I18n.currentLocale(),
            JobId: this.props.jobid,
            PileId: this.props.pileid,
            lockstep:true
          }
          this.props.pileCheckStep07super(valueApi)
        }
        
      }else{
        // console.warn('Alert down')
        Alert.alert("", I18n.t("mainpile.button.goto") + " " + title, [
          { text: "Cancel", onPress: () => {}, style: "cancel" },
          {
            text: "Ok",
            onPress: async () => {
              
              // console.log(data)
              // console.warn('Ok Press down')
              if((this.props.category == 3||this.props.category == 5) && this.state.parentFalg==true && index == 8){
                Alert.alert("",I18n.t('alert.errorParentcheck')+" "+this.state.parentNo,[
                  {
                    text:"Ok",
                    onPress: ()=> {
                      this.alertNext(index)
                    }
                  }
                ])
              }else{
                this.alertNext(index)
              }
            }
          }
        ])
      }
      
    }
  }
  alertNext(index){
    // console.warn('alertNext',index)
    this.setState({ index })
    this._carousel.snapToItem(index)

    var data = {
      process: index + 1,
      step: 1
    }
    if (typeof this.refs["pile" + data.process].getWrappedInstance().initialStep === "function") {
      this.refs["pile" + data.process].getWrappedInstance().initialStep()
    }
    if (this.refs["pile2"].getWrappedInstance().onSetStackReducer(data)) {
      this.refs["pile2"].getWrappedInstance().onSetStackReducer(data)
    }
    if(this.props.sp_parentid != this.props.pileid && (index + 1) == 5 && this.props.category == 5){
      this.props.mainRefresh(0)
    }
   
  }

  _renderMenu = ({ item, index }) => {
    // console.warn('_renderMenu',this.props.category,this.state.pile.shape)
    const step_status =
      this.state.stepStatus && this.state.stepStatus["Step" + (index + 1) + "Status"]
        ? this.state.stepStatus["Step" + (index + 1) + "Status"]
        : 0
        var no_process = 0
        
        if(this.props.category == 5 || this.props.category == 3){
          if(this.state.pile.shape == 2){
            if(index == 1 || index == 5 || index == 10){
              no_process = 1
            }else if(index == 2 && (this.props.pileid != this.props.sp_parentid)){
              no_process = 1
            }
          }else{
            if(index == 1  || index == 10){
              no_process = 1
            }else if(index == 2 && (this.props.pileid != this.props.sp_parentid)){
              no_process = 1
            }
          }
        }else{
          if(this.state.pile.shape == 2){
            if(index == 1 || index == 5 || index == 10){
              no_process = 1
            }
          }else{
            if(this.props.category == 4){
              if(index == 3 || index == 5 ){
                no_process = 1
              }
            }
          }
        }
        
    return (
      <View style={styles.slide}>
        <View style={[styles.leftMenu, this._getColorItem(step_status)]}>
          <Text style={styles.textStyle}>{item.step}</Text>
        </View>
        <TouchableOpacity
          style={[styles.centerMenu,no_process == 1 ? {backgroundColor:'#636262',borderColor:'#636262'} : this._getColor(step_status) ]}
          onPress={() => this._alertNextStep(item.title, index)}
          activeOpacity={1}
        >
          <View style={{position:"absolute"}}>
            <Text style={[styles.textStyle, { textAlign: "center" }]}>{item.title}</Text>
          </View>
            {
             no_process == 1  ?
                <View style={{position:"absolute"}}>
                  <Svg
                      height={DeviceInfo.isTablet() ? 60 : 30}
                      width={itemWidth * 0.6}
                  >
                      <Line
                          x1="0"
                          y1="0"
                          x2={itemWidth * 0.6}
                          y2={DeviceInfo.isTablet() ? 60 : 30}
                          stroke="red"
                          strokeWidth="1"
                      />
                  </Svg>
              </View>
              :
              <View/>
              
            }
            {
              no_process == 1  ?
                <View style={{position:"absolute"}}>
                  <Svg
                    height={DeviceInfo.isTablet() ? 60 : 30}
                    width={itemWidth * 0.6}
                  >
                      <Line
                          x1={itemWidth * 0.6}
                          y1="0"
                          x2="0"
                          y2={DeviceInfo.isTablet() ? 60 : 30}
                          stroke="red"
                          strokeWidth="1"
                      />
                  </Svg>
              </View>
              :
              <View/>
              
            }
        </TouchableOpacity>
        <View style={[styles.rightMenu, this._getColorItem(step_status)]}>
          <Text style={styles.textStyle}>{item.step}</Text>
        </View>
      </View>
    )
  }

  _renderTop = () => {
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, justifyContent: "center" }}>
          <ActivityIndicator size="large" color="#007CC2" />
        </View>
      )
    }
    return (
      <View>
        {this._renderTopMenu()}
        {this._renderSwiperMenu()}
      </View>
    )
  }

  changeMenu(index) {
    console.log("Changed", index)
  }

  _renderTopMenu = () => {
    const pile = this.state.pile
    const size = pile.size || ""
    const piletip = pile.piletip || 0
    const cutoff = pile.cutoff || 0
    return (
      <View>
        <View style={[styles.main,this.props.category == 3|| this.props.category == 5 ?{ backgroundColor: DEFAULT_COLOR_5 }:this.state.pile.shape==2?{ backgroundColor: DEFAULT_COLOR_2 }:{}]}>
          <Text style={styles.textMain}>{I18n.t('mainpile.head.type')} : {pile.type}</Text>
        </View>
        <View style={styles.topic}>
          <View style={[styles.topicView, this.props.category == 3|| this.props.category == 5 ?{ backgroundColor: DEFAULT_COLOR_6 }:this.state.pile.shape==2?{ backgroundColor: DEFAULT_COLOR_3 }:{}]}>
            <Text style={styles.textTopic}>{I18n.t('mainpile.head.diameter')}</Text>
            <Text style={styles.textTopic}>{size}</Text>
          </View>
          <View style={[styles.topicView, this.props.category == 3|| this.props.category == 5 ?{ backgroundColor: DEFAULT_COLOR_6 }:this.state.pile.shape==2?{ backgroundColor: DEFAULT_COLOR_3 }:{}]}>
            <Text style={styles.textTopic}>{I18n.t('mainpile.head.piletip')}</Text>
            <Text style={styles.textTopic}>{piletip.toFixed(3)}</Text>
          </View>
          <View style={[styles.topicView, this.props.category == 3|| this.props.category == 5 ?{ backgroundColor: DEFAULT_COLOR_6 }:this.state.pile.shape==2?{ backgroundColor: DEFAULT_COLOR_3 }:{}]}>
            <Text style={styles.textTopic}>{I18n.t('mainpile.head.cutoff')}</Text>
            <Text style={styles.textTopic}>{cutoff.toFixed(3)}</Text>
          </View>
        </View>
      </View>
    )
  }

  _renderSwiperMenu = () => {
    return (
      <View style={styles.menuBorder}>
        <Carousel
          ref={c => {
            this._carousel = c
          }}
          data={this.state.data}
          renderItem={this._renderMenu}
          sliderWidth={sliderWidth}
          itemWidth={itemWidth}
          sliderHeight={30}
          onSnaptoItem={item => console.log("item", item)}
          inactiveSlideOpacity={1}
          enableMomentum={true}
          firstItem={this.state.firstItem}
          initialNumToRender={11}
          // currentScrollPosition={this.state.firstItem}
        />
      </View>
    )
  }

  nextStep = () => {
    if((this.props.category == 5 || this.props.category == 3) && this.props.pileid != this.props.sp_parentid && this.state.index === 0){
      this._carousel.snapToItem(3)
      this.setState({ index: this.state.index + 3 })
    }
    else if ((this.state.pile.shape === 2) && (this.state.index === 4)) {
      this._carousel.snapToItem(6)
      this.setState({ index: this.state.index + 2 })
    } else if ((this.state.pile.shape === 2) && (this.state.index === 0)) {
      this._carousel.snapToItem(2)
      this.setState({ index: this.state.index + 2 })
    } else {
      if(this.props.category == 4 && this.state.index === 2){
        this._carousel.snapToItem(4)
        this.setState({ index: this.state.index + 2 })
      }else if(this.props.category == 4 && this.state.index === 4){
        this._carousel.snapToItem(6)
        this.setState({ index: this.state.index + 2 })
      }else if(this.props.category == 5 && this.state.index === 0){
        this._carousel.snapToItem(2)
        this.setState({ index: this.state.index + 2 })
      }
      else if(this.props.category == 5 && this.state.index === 4 && this.state.pile.shape === 2){
        this._carousel.snapToItem(6)
        this.setState({ index: this.state.index + 2 })
      }
      else{
        this._carousel.snapToItem(this.state.index + 1)
        this.setState({ index: this.state.index + 1 })
      }
    }
    var indeX = this.state.index == 0 ? 1 : this.state.index + 1

    if (typeof this.refs["pile" + indeX].getWrappedInstance().initialStep === "function") {
      this.refs["pile" + indeX].getWrappedInstance().initialStep()
    }
  }

  render() {
    const pile = this.state.pile
    // pile = {
    //   job_id: 43,
    //   pile_id: 19934,
    //   type: "#3",
    //   size: '33',
    //   piletip: 're',
    // }
    // console.warn('shape',this.state.pile.shape)
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, justifyContent: "center" }}>
          <ActivityIndicator size="large" color="#007CC2" />
        </View>
      )
    }
    // console.warn('main',this.state.pileValueInfo.Step8.StartDate)
    return (
      <Container style={{ backgroundColor: "white" }}>
        {this._renderTop()}
        <Pile1
          ref="pile1"
          hidden={!(this.state.index == 0)}
          jobid={this.props.jobid}
          pileid={this.props.pileid}
          onNextStep={this.nextStep}
          onRefresh={this.props.action}
          shape={this.state.pile.shape}
          category={this.props.category}
          startdate={this.state.pileValueInfo.Step1==null?'':this.state.pileValueInfo.Step1.StartDate}
          enddate={this.state.pileValueInfo.Step1==null?'':this.state.pileValueInfo.Step1.EndDate}
        />
        <Pile2
          ref="pile2"
          hidden={!(this.state.index == 1)}
          jobid={this.props.jobid}
          pileid={this.props.pileid}
          onNextStep={this.nextStep}
          pile={pile}
          onRefresh={this.props.action}
          shape={this.state.pile.shape}
          category={this.props.category}
          startdate={this.state.pileValueInfo.Step2==null?'':this.state.pileValueInfo.Step2.StartDate}
          enddate={this.state.pileValueInfo.Step2==null?'':this.state.pileValueInfo.Step2.EndDate}
        />
        <Pile3
          ref="pile3"
          hidden={!(this.state.index == 2)}
          jobid={this.props.jobid}
          pileid={this.props.pileid}
          onNextStep={this.nextStep}
          onRefresh={this.props.action}
          shape={this.state.pile.shape}
          category={this.props.category}
          startdate={this.state.pileValueInfo.Step3==null?'':this.state.pileValueInfo.Step3.StartDate}
          enddate={this.state.pileValueInfo.Step3==null?'':this.state.pileValueInfo.Step3.EndDate}
          fromnoti={this.props.fromnoti}
        />
        <Pile4
          ref="pile4"
          hidden={!(this.state.index == 3)}
          jobid={this.props.jobid}
          pileid={this.props.pileid}
          onNextStep={this.nextStep}
          onRefresh={this.props.action}
          shape={this.state.pile.shape}
          category={this.props.category}
        />
        <Pile5
          ref="pile5"
          hidden={!(this.state.index == 4)}
          jobid={this.props.jobid}
          pileid={this.props.pileid}
          onNextStep={this.nextStep}
          onRefresh={this.props.action}
          shape={this.state.pile.shape}
          category={this.props.category}
          pile={pile}
          parent={this.props.pileid == this.props.sp_parentid}
          sp_parentid={this.props.sp_parentid}
        />
        <Pile6
          ref="pile6"
          hidden={!(this.state.index == 5)}
          jobid={this.props.jobid}
          pileid={this.props.pileid}
          onNextStep={this.nextStep}
          onRefresh={this.props.action}
          shape={this.state.pile.shape}
          category={this.props.category}
          startdate={this.state.pileValueInfo.Step6==null?'':this.state.pileValueInfo.Step6.StartDate}
          enddate={this.state.pileValueInfo.Step6==null?'':this.state.pileValueInfo.Step6.EndDate}
          pileinfo={this.state.pileValueInfo.Step6}
        />
        <Pile7
          ref="pile7"
          hidden={!(this.state.index == 6)}
          jobid={this.props.jobid}
          pileid={this.props.pileid}
          onNextStep={this.nextStep}
          onRefresh={this.props.action}
          pile={pile}
          shape={this.state.pile.shape}
          category={this.props.category}
          parent={this.props.pileid == this.props.sp_parentid}
          startdate={this.state.pileValueInfo.Step7==null?'':this.state.pileValueInfo.Step7.StartDate}
          enddate={this.state.pileValueInfo.Step7==null?'':this.state.pileValueInfo.Step7.EndDate}
          sp_parentid={this.props.sp_parentid}
        />
        <Pile8
          ref="pile8"
          hidden={!(this.state.index == 7)}
          jobid={this.props.jobid}
          pileid={this.props.pileid}
          onNextStep={this.nextStep}
          onRefresh={this.props.action}
          shape={this.state.pile.shape}
          category={this.props.category}
          startdate={this.state.pileValueInfo.Step8==null?'':this.state.pileValueInfo.Step8.StartDate}
          enddate={this.state.pileValueInfo.Step8==null?'':this.state.pileValueInfo.Step8.EndDate}
        />
        <Pile9
          ref="pile9"
          hidden={!(this.state.index == 8)}
          jobid={this.props.jobid}
          pileid={this.props.pileid}
          onNextStep={this.nextStep}
          onRefresh={this.props.action}
          shape={this.state.pile.shape}
          category={this.props.category}
        />
        <Pile10
          ref="pile10"
          hidden={!(this.state.index == 9)}
          jobid={this.props.jobid}
          pileid={this.props.pileid}
          onNextStep={this.nextStep}
          onRefresh={this.props.action}
          shape={this.state.pile.shape}
          category={this.props.category}
        />
        <Pile11
          ref="pile11"
          hidden={!(this.state.index == 10)}
          jobid={this.props.jobid}
          pileid={this.props.pileid}
          onNextStep={this.nextStep}
          onRefresh={this.props.action}
          shape={this.state.pile.shape}
          category={this.props.category}
          startdate={this.state.pileValueInfo.Step11==null?'':this.state.pileValueInfo.Step11.StartDate}
          enddate={this.state.pileValueInfo.Step11==null?'':this.state.pileValueInfo.Step11.EndDate}
        />
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    error: state.pile.error,
    item: state.pile.item,
    valueInfo: state.pile.valueInfo,
    masterInfo: state.pile.masterInfo,
    step_status: state.mainpile.step_status,
    stack: state.mainpile.stack_item,
    position: state.pile.position,
    drillingfluidslist: state.drillingFluids.drillingfluidslist,
    drillinglist: state.drillinglist.drillinglist,
    drillinglistChild: state.drillinglist.drillinglistChild,
    pileAll: state.mainpile.pileAll || null,
    lockstep7_8: state.pile.lockstep7_8,
    randomlockstep7_8:state.pile.randomlockstep7_8,
    checkto7_super: state.pile.checkto7_super,
    checkto7_super_random: state.pile.checkto7_super_random,
    lockstepcheckto7_super: state.pile.lockstepcheckto7_super,

    location_lat: state.location.location_lat,
    location_log:state.location.location_log,

    checkto6_super: state.pile.checkto6_super,
    checkto6_super_random: state.pile.checkto6_super_random,
    lockstepcheckto6_super:state.pile.lockstepcheckto6_super,

    checkto5_super: state.pile.checkto5_super,
    checkto5_super_random: state.pile.checkto5_super_random,
    lockstepcheckto5_super:state.pile.lockstepcheckto5_super,
  }
}

export default connect(mapStateToProps, actions, null, { withRef: true })(MainPile)
