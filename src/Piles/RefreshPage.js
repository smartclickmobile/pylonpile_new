import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Linking, Image, Dimensions, BackHandler,ActivityIndicator } from 'react-native'
import { Container, Content, } from 'native-base'
import { Button, Icon } from 'react-native-elements'
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import * as actions from '../Actions';


class Refresh extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading:true,
      submit:false,
    }

  }

  componentDidMount() {
    BackHandler.addEventListener('backPress', () => this.onBackPress() )
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('backPress')
  }
  componentWillMount(){
    this.props.clearStatus()
    this.props.pileClear()
    this.props.pileMasterInfo({ jobid: this.props.jobid, pileid: this.props.pileid })
    this.props.pileValueInfo({ jobid: this.props.jobid, pileid: this.props.pileid })
    this.props.pileItem({ jobid: this.props.jobid, pileid: this.props.pileid , Language:I18n.locale})
  }

  onBackPress () {
    Actions.pop({
      refresh: {
        action: 'start',
      }
    })
    return true
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.item  != null && nextProps.valueInfo != null && nextProps.masterInfo != null && this.state.submit == false) {
      this.setState({submit:true},()=>{
        setTimeout(()=>{
          Actions.pop({
            refresh: {
              action: 'refresh',
            }
          })
        },2000)
      })
    }
  }

  render() {
    if (this.state.loading) {
      return (
        <View style={{ flex: 1,justifyContent: 'center' }}>
          <ActivityIndicator size="large" color="#007CC2" />
        </View>
      )
    }
  }
}

const mapStateToProps = state => {
  return {
    item: state.pile.item,
    valueInfo: state.pile.valueInfo,
    masterInfo: state.pile.masterInfo,
  }
}

export default connect(mapStateToProps,actions )(Refresh)
