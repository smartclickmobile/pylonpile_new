import React, { Component } from "react"
import { View, Text, StyleSheet, TouchableOpacity, Modal,Alert, ActivityIndicator } from "react-native"
import StepIndicator from "react-native-step-indicator"
import { Container, Content } from "native-base"
import { MAIN_COLOR,DEFAULT_COLOR_1, DEFAULT_COLOR_4 } from "../Constants/Color"
import Pile6_1 from "./Pile6_1"
import Pile6_2 from "./Pile6_2"
import I18n from '../../assets/languages/i18n'
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux'
import * as actions from '../Actions'
import styles from './styles/Pile6.style'
import moment from "moment"
import Loader from '../Components/Loader'
const count = 0
class Pile6 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 6,
      step: 0,
      ref:2,
      error:null,
      data:null,
      location:{
        position:{
          lat:1,
          log:1,
        }
      },
      Edit_Flag:1,
      startdate:'',
      pile6_1success:false,
      pile6_2success:false,
      save1:false,
      save2:false,
      Status6:0,
      set1:false,
      set2:false,
      Status7:0,
      Status8:0,
      Status10:0,
      Status1:0,
      last8:'',
      random1:'',
      random2:'',
      check:false,
      saveload:false,
      checksavebutton:false,
      stepstatus:null,
      hiddenstep2:false,

      checkto7_super_random:null,
      percentageCheckDepth:null
    }
  }
  componentDidMount(){
    // console.log(this.props.stack)
    let piledata33,pilemaster52,pilemaster53,piledata55

    if (this.props.stack) {
      this.setState({step:this.props.stack[this.props.stack.length - 1].step - 1})
    }
    this.props.getTremieList({
      pileid: this.props.pileid,
      jobid: this.props.jobid
    })
    if(this.props.pileAll[this.props.pileid] ) {
      if(this.props.pileAll[this.props.pileid]['3_3'])  piledata33 = this.props.pileAll[this.props.pileid]['3_3'].data
      if(this.props.pileAll[this.props.pileid]['5_2'])  pilemaster52 = this.props.pileAll[this.props.pileid]['5_2'].masterInfo
      if(this.props.pileAll[this.props.pileid]['5_3'])  pilemaster53 = this.props.pileAll[this.props.pileid]['5_3'].masterInfo
      if(this.props.pileAll[this.props.pileid]['5_5'])  piledata55 = this.props.pileAll[this.props.pileid]['5_5'].data
    }
    if(pilemaster52&&pilemaster53&&pilemaster53.PileInfo &&pilemaster53.PileInfo.PercentageCheckDepth){
     let dataMasterPile5_2 = pilemaster52
      if(piledata33) {
       let  pile3_3 = piledata33.data
        if (pile3_3) {
          var data = dataMasterPile5_2
          if (pile3_3.top) {
            dataMasterPile5_2.Depth =  pile3_3.top - data.PileTip
          }
          // this.setState({dataMasterPile5_2:data})
        }
      }
      let depthneed = null
      console.warn('pile3_3.top',dataMasterPile5_2.PileInfo)
      if(this.props.category == 5){
        if (piledata55.DrillingList[piledata55.DrillingList.length - 1] != undefined) {
          depthneed = piledata55.DrillingList[piledata55.DrillingList.length - 1].Depth
          // await this.setState({DepthLast:piledata55.DrillingList[piledata55.DrillingList.length - 1].Depth})
        }
      }else{
        depthneed = dataMasterPile5_2.PileInfo.Depth ? dataMasterPile5_2.PileInfo.Depth :null
      }
      if(depthneed){
        let goal = parseFloat(depthneed.toString()).toFixed(3)
        let value =(1+pilemaster53.PileInfo.PercentageCheckDepth)*goal
         console.log('pileMaster.PileInfo',value)
         this.setState({percentageCheckDepth: value})
      }

     
    }
    // if(){
    //   this.props.pileAll[this.props.pileid]['5_3'].masterInfo
    //   pileMaster = this.props.pileAll[this.props.pileid][pile_page].masterInfo
    // }
  }
  async componentWillReceiveProps(nextProps){
    if(nextProps.step_status2_random != this.state.stepstatus && nextProps.processstatus == 6){
      this.setState({stepstatus:nextProps.step_status2_random},async()=>{
        // console.warn('yes6',this.state.data['6_1'].data.CrossFlag)
        await this.props.mainPileSetStorage(this.props.pileid, "step_status", { Step6Status: nextProps.step_status2.Step6Status })
        this.props.mainPileGetStorage(this.props.pileid, "step_status")
        if(nextProps.step_status2.Step6Status == 2){
          if(this.state.data['6_1'].data.CrossFlag == true){
            if(this.state.Status1 == 2){
              if(this.props.category == 5){
                var valuecheck ={
                  Language:I18n.locale,
                  JobId: this.props.jobid,
                  PileId: this.props.pileid,
                  lockstep:false,
                  process:6
                }
                this.props.pileCheckStep07super(valuecheck)
              }else{
                setTimeout(()=>{this.setState({checksavebutton:false})},2000)
                this.onSetStack(7,0)
                this.props.onNextStep()
              }
              
            }else{
              setTimeout(()=>{this.setState({checksavebutton:false})},2000)
              Alert.alert('', I18n.t('mainpile.lockprocess.process7_dw1'), [
                {
                  text: 'OK'
                }
              ])
            }
          }else{
            if(this.state.step == 1){
              if(this.state.Status1 == 2){
                if(this.props.category == 5){
                  var valuecheck ={
                    Language:I18n.locale,
                    JobId: this.props.jobid,
                    PileId: this.props.pileid,
                    lockstep:false,
                    process:6
                  }
                  this.props.pileCheckStep07super(valuecheck)
                }else{
                  this.onSetStack(7,0)
                  this.props.onNextStep()
                }
              }else{
                setTimeout(()=>{this.setState({checksavebutton:false})},2000)
                Alert.alert('', I18n.t('mainpile.lockprocess.process7_dw1'), [
                  {
                    text: 'OK'
                  }
                ])
              }
            }else{
              setTimeout(()=>{this.setState({checksavebutton:false})},2000)
            }
          }
        }
        
      })
    }
    console.warn('checkto7_super process6',nextProps.checkto7_super_process)
    if(nextProps.checkto7_super == true && nextProps.checkto7_super_random != this.state.checkto7_super_random && this.props.category == 5 && nextProps.lockstepcheckto7_super == false && nextProps.checkto7_super_process == 6){
      this.setState({checkto7_super_random: nextProps.checkto7_super_random},()=>{
        console.warn('checkto7_super process6')
        setTimeout(()=>{this.setState({checksavebutton:false})},2000)
        this.onSetStack(7,0)
        this.props.onNextStep()
      })
    }
    if (nextProps.pileAll[this.props.pileid] != undefined) {
      var edit = nextProps.pileAll[this.props.pileid]
      if (edit['6_0'] != undefined) {
          await this.setState({Edit_Flag:edit['6_0'].data.Edit_Flag})
      }
      if (edit['8_2'] != undefined) {
        await this.setState({last8:edit['8_2'].data})
    }
      if(edit != undefined){
        await this.setState({Status6:edit.step_status.Step6Status,Status7:edit.step_status.Step7Status,Status8:edit.step_status.Step8Status,Status10:edit.step_status.Step10Status,Status1:edit.step_status.Step1Status})
      }
    }
  
    if(nextProps.pile6_1success = true && this.state.random1 != nextProps.random1){
     
      this.setState({pile6_1success:nextProps.pile6_1success,random1:nextProps.random1},async()=>{
        if(this.state.pile6_1success != undefined && this.state.pile6_1success != null){
          if(this.state.pile6_1success ==  true && this.state.save1 == false){
            if (this.state.data['6_1'].data.CrossFlag == false) {
              setTimeout(()=>{this.setState({checksavebutton:false})},2000)
              this.props.getStepStatus2({
                jobid: this.props.jobid,
                pileid: this.props.pileid,
                process:6
              })
              if(this.state.check == false){
                var value = {
                  process:6,
                  step:1,
                  pile_id: this.props.pileid,
                  data:{
                      BucketSize:this.state.data['6_1'].data.BucketSize,
                      Machine:this.state.data['6_1'].data.Machine,
                      DriverId:this.state.data['6_1'].data.DriverId,
                      CrossFlag:false,
                      ChangeDrillingFluidBeforeDepth:null,
                      ChangeDrillingFluidAfterDepth:null
                  },
                  ChangeDrillingFluidStartDate:null,
                  ChangeDrillingFluidEndDate:null
                }
                await this.props.mainPileSetStorage(this.props.pileid,'6_1',value)
                this.setState({saveload:false})
                this.onSetStack(6,0)
                
                if(this.props.startdate != '' && this.props.startdate != null){
                  this.setState({ step: 1 ,startdate: this.props.startdate,save1:true})
                }else{
                  this.setState({ step: 1 ,startdate: moment().format("DD/MM/YYYY HH:mm"),save1:true})
                }
              }
            }else{
              this.props.getStepStatus2({
                jobid: this.props.jobid,
                pileid: this.props.pileid,
                process:6
              })
             
                this.setState({saveload:false})    
                setTimeout(()=>{this.setState({checksavebutton:false})},2000)
              
              
              this.setState({save1:true})
              // console.warn('save1',value)
            }
          }
    
      }
      })
    }
    if(this.state.random2 != nextProps.random2 && nextProps.pile6_2success == true){
      this.setState({pile6_2success:nextProps.pile6_2success,random2:nextProps.random2},async()=>{
        if(this.state.pile6_2success != undefined && this.state.pile6_2success != null){
          if(this.state.pile6_2success ==  true && this.state.save2 == false ){
            // console.warn('pile6_2success')
            this.props.getStepStatus2({
              jobid: this.props.jobid,
              pileid: this.props.pileid,
              process:6
            })
            var value = {
              process:6,
              step:1,
              pile_id: this.props.pileid,
              data:{
                  BucketSize:this.state.data['6_1'].data.BucketSize,
                  Machine:this.state.data['6_1'].data.Machine,
                  DriverId:this.state.data['6_1'].data.DriverId,
                  CrossFlag:false,
                  ChangeDrillingFluidBeforeDepth:null,
                  ChangeDrillingFluidAfterDepth:null
              },
              ChangeDrillingFluidStartDate:null,
              ChangeDrillingFluidEndDate:null
            }
            await this.props.mainPileSetStorage(this.props.pileid,'6_1',value)
            this.setState({saveload:false})
            setTimeout(()=>{this.setState({checksavebutton:false})},2000)
            this.setState({save2:true})
          }else{
            setTimeout(()=>{this.setState({checksavebutton:false})},2000)
          }  
      }
      })
    }else{
    }
    
    await this.setState({
      error:nextProps.error,
      data:nextProps.pileAll[nextProps.pileid],
      location:nextProps.pileAll.currentLocation,
      pile6_1success:nextProps.pile6_1success,
      pile6_2success:nextProps.pile6_2success
    },()=>{
      //console.warn('set data',this.state.data['6_1'].data)
    })
  }

  updateState(value){
    if (value.step != null) {
      const ref = 'pile'+ value.process +'_'+ value.step
      if (this.refs[ref].getWrappedInstance().updateState(value) != undefined) {
        this.refs[ref].getWrappedInstance().updateState(value)
      }
    }else{
      console.log(value)
    }
  }

  async onSave(step){
    await this.props.mainPileGetAllStorage()
    
    if (step == 0) {
      var value = {
        process:6,
        step:1,
        pile_id: this.props.pileid,
        category:this.props.category
      }
      // await this.props.mainPileSetStorage(this.props.pileid,'3_1',value)
      if (this.state.Edit_Flag == 1) {
        this.setState({save1:false})
        if (this.state.data['6_1'].data.CrossFlag == false) {
          this.props.mainpileAction_checkStepStatus(value)
            .then(async (data) =>
            {
              console.log(data)
              
              // await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step6Status:data.statuscolor})
              // this.props.mainPileGetStorage(this.props.pileid,'step_status')

              var valueApi ={
                Language:I18n.locale,
                JobId: this.props.jobid,
                PileId: this.props.pileid,
                Page:1,
                BucketSizeId: this.state.data['6_1'].data.BucketSize == null ? '' : this.state.data['6_1'].data.BucketSize.Id,
                MachineId: this.state.data['6_1'].data.Machine == null || this.state.data['6_1'].data.Machine==''? '' : this.state.data['6_1'].data.Machine.itemid,
                DriverId: this.state.data['6_1'].data.DriverId,
                BucketSizeName: this.state.data['6_1'].data.BucketSize == null ? '' : this.state.data['6_1'].data.BucketSizeName,
                MachineName:this.state.data['6_1'].data.Machine == null || this.state.data['6_1'].data.Machine == '' ? '' :this.state.data['6_1'].data.MachineName,
                DriverName:this.state.data['6_1'].data.DriverName,

                latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
                longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
                CrossFlag:false ,
              }

              
             

              if ( this.state.error == null) {
                if (data.check == false) {
                  this.setState({check:true,saveload:false})
                  Alert.alert('', data.errorText[0], [
                    {
                      text: 'OK'
                    }
                  ])
                }else {
                  this.setState({saveload:true})
                   this.props.pileSaveStep06(valueApi)
                  this.setState({check:false})
                  // if(this.state.pile6_1success == true){
                  //   this.onSetStack(6,0)
                  //   if(this.props.startdate != '' && this.props.startdate != null){
                  //     this.setState({ step: 1 ,startdate: this.props.startdate})
                  //   }else{
                  //     this.setState({ step: 1 ,startdate: moment().format("DD/MM/YYYY HH:mm")})
                  //   }
                  // }
                }
              } else {
                Alert.alert(this.state.error)
                this.setState({checksavebutton:false,saveload:false})
              }
            })
        }else if(this.state.data['5_3'].data.DrillingList.length > 0 && this.state.data['6_1'].data.CrossFlag == true ) {
          if(this.state.data['6_1'].ChangeDrillingFluidStartDate != '' && 
          this.state.data['6_1'].ChangeDrillingFluidEndDate && 
          this.state.data['6_1'].data.ChangeDrillingFluidBeforeDepth != '' &&
          this.state.data['6_1'].data.ChangeDrillingFluidAfterDepth != ''
        ){
          this.setState({checksavebutton:true})
          if (this.state.data["6_1"].data.ChangeDrillingFluidAfterDepth !== ''&&parseFloat(this.state.data["6_1"].data.ChangeDrillingFluidAfterDepth).toFixed(3) > this.state.percentageCheckDepth) {
              let con = true

              let DepthAlert = await this.checkdepthalert("1").then((a,b)=>{
              
                  return a
              })
              console.log('DepthAlert',DepthAlert)
              if(DepthAlert=='YES'){
                return this.setState({checksavebutton:false})
              }
            }
            if(this.state.data['6_1'].ChangeDrillingFluidStartDate<=this.state.data['6_1'].ChangeDrillingFluidEndDate){
              if(parseFloat(this.state.data['6_1'].data.ChangeDrillingFluidAfterDepth) < parseFloat(this.state.data['6_1'].data.ChangeDrillingFluidBeforeDepth)){
                Alert.alert('', I18n.t('mainpile.6_1.errordepth1'), [
                  {
                    text: 'OK'
                  }
                ])
                this.setState({checksavebutton:false})
              }else{
                 var depth = ''
                 var temp = 1
                if(this.state.data['5_3'].data.DrillingList[this.state.data['5_3'].data.DrillingList.length - 1].Depth >= this.state.data['3_3'].data.top - this.state.data['5_2'].masterInfo.PileInfo.PileTip){
                  depth = this.state.data['5_3'].data.DrillingList[this.state.data['5_3'].data.DrillingList.length - 1].Depth
                  temp = 1
                }else{
                  depth = this.state.data['3_3'].data.top - this.state.data['5_2'].masterInfo.PileInfo.PileTip
                  temp = 2
                }
                // console.warn('dddd',parseFloat(this.state.data['6_1'].data.ChangeDrillingFluidAfterDepth).toFixed(3) , parseFloat(depth).toFixed(3))
                // if(parseFloat(this.state.data['6_1'].data.ChangeDrillingFluidBeforeDepth).toFixed(3) > parseFloat(this.state.data['5_3'].data.DrillingList[this.state.data['5_3'].data.DrillingList.length-1].Depth).toFixed(3)){
                //   Alert.alert('', I18n.t('mainpile.6_1.errordepth3'), [
                //     {
                //       text: 'OK'
                //     }
                //   ])
                // }else{
                  if(parseFloat(this.state.data['6_1'].data.ChangeDrillingFluidAfterDepth).toFixed(3) < parseFloat(depth).toFixed(3)){
                    if(temp == 1){
                      if(this.props.category == 5){
                        Alert.alert('', I18n.t('mainpile.6_1.errordepth2_dw'), [
                          {
                            text: 'OK'
                          }
                        ])
                      }else if(this.props.category == 1||this.props.category == 4){
                        Alert.alert('', I18n.t('mainpile.6_1.errordepth4'), [
                          {
                            text: 'OK'
                          }
                        ])
                      }else{
                        Alert.alert('', I18n.t('mainpile.6_1.errordepth4_dw'), [
                          {
                            text: 'OK'
                          }
                        ])
                      }
                      
                    }else{
                      if(this.props.category == 5){
                        Alert.alert('', I18n.t('mainpile.6_1.errordepth2_dw'), [
                          {
                            text: 'OK'
                          }
                        ])
                      }else if(this.props.category == 1||this.props.category == 4){
                        Alert.alert('', I18n.t('mainpile.6_1.errordepth4'), [
                          {
                            text: 'OK'
                          }
                        ])
                      }else{
                        Alert.alert('', I18n.t('mainpile.6_1.errordepth2'), [
                          {
                            text: 'OK'
                          }
                        ])
                      }
                      
                    }
                    
                    this.setState({checksavebutton:false})
                  }else{
                    if(this.state.Status8 ==0){
                      //   await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step6Status:2})
                      // await this.props.mainPileGetStorage(this.props.pileid,'step_status')
                  var valueApi ={
                    Language:I18n.locale,
                    JobId: this.props.jobid,
                    PileId: this.props.pileid,
                    Page:1,
                    latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
                    longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
                    CrossFlag:true ,
                    ChangeDrillingFluidStartDate:moment(this.state.data['6_1'].ChangeDrillingFluidStartDate,"DD/MM/YYYY HH:mm").format("YYYY-MM-DD HH:mm:ss"),
                    ChangeDrillingFluidEndDate:moment(this.state.data['6_1'].ChangeDrillingFluidEndDate,"DD/MM/YYYY HH:mm").format("YYYY-MM-DD HH:mm:ss"),
                    ChangeDrillingFluidBeforeDepth:this.state.data['6_1'].data.ChangeDrillingFluidBeforeDepth,
                    ChangeDrillingFluidAfterDepth:this.state.data['6_1'].data.ChangeDrillingFluidAfterDepth
                  }
                  console.log('pile6_1',valueApi)
                  if(this.state.Status1 == 2){
                    this.setState({saveload:true})
                  }
                  
                  this.props.pileSaveStep06(valueApi)
        
                  if ( this.state.error == null) {
                    // await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step6Status:2,Step5Status:2})
                    // this.props.mainPileGetStorage(this.props.pileid,'step_status')
        
                    var edit = this.props.pileAll[this.props.pileid]
                    var value5_0 = {
                      process:5,
                      step:0,
                      pile_id: this.props.pileid,
                      data:{
                        id:edit['5_0'].data.Id,
                        Edit_Flag:1,
                        Status:edit['5_0'].data.Status,
                        StartDate:edit['5_0'].data.StartDate,
                        EndDate:edit['5_0'].data.EndDate,
                        LocationLat:edit['5_0'].data.LocationLat,
                        LocationLong:edit['5_0'].data.LocationLong,
                      }
                    }
                    await this.props.mainPileSetStorage(this.props.pileid,'5_0',value5_0)
        
                    var edit = this.props.pileAll[this.props.pileid]
                    var value6_0 = {
                      process:6,
                      step:0,
                      pile_id: this.props.pileid,
                      data:{
                        id:edit['6_0'].data.Id,
                        Edit_Flag:1,
                        Status:edit['6_0'].data.Status,
                        StartDate:edit['6_0'].data.StartDate,
                        EndDate:edit['6_0'].data.EndDate,
                        LocationLat:edit['6_0'].data.LocationLat,
                        LocationLong:edit['6_0'].data.LocationLong,
                      }
                    }
                    await this.props.mainPileSetStorage(this.props.pileid,'6_0',value6_0)
        
                    var value0 = {
                      process: 7,
                      step: 0,
                      pile_id: this.props.pileid,
                      shape: this.props.shape
                    }
                    this.props.mainpileAction_checkStepStatus(value0).then(data => {
                      const text = "mainpile.lockprocess.process" + 7 + "_" + (this.props.shape == 1 ? "bp" : "dw")
                      if (data.check == true) {
                        // console.warn('check')
                        // if(this.state.Status6 == 2){
                        //   this.props.onNextStep()
                        //   this.onSetStack(7,0)
                        // }
                        
                      } else {
                        // Alert.alert("", I18n.t(text), [
                        //   {
                        //     text: "OK",
                        //   }
                        // ])
                      }
                    })
                  } else {
                    Alert.alert(this.state.error)
                    this.setState({checksavebutton:false,saveload:false})
                  }
                }else{
                  // console.warn('ccccccc',this.props.tremielist)
                  var length8=0
                  for(var i=0;i<this.props.tremielist.length;i++){
                    length8 += this.props.tremielist[i].Length
                  }
                  // console.warn('ccccccc',this.state.data['6_1'].data.ChangeDrillingFluidAfterDepth-0.3,length8)
                  if(this.state.data['6_1'].data.ChangeDrillingFluidAfterDepth >= length8){
                    var valueApi ={
                      Language:I18n.locale,
                      JobId: this.props.jobid,
                      PileId: this.props.pileid,
                      Page:1,
                      latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
                      longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
                      CrossFlag:true ,
                      ChangeDrillingFluidStartDate:moment(this.state.data['6_1'].ChangeDrillingFluidStartDate,"DD/MM/YYYY HH:mm").format("YYYY-MM-DD HH:mm:ss"),
                      ChangeDrillingFluidEndDate:moment(this.state.data['6_1'].ChangeDrillingFluidEndDate,"DD/MM/YYYY HH:mm").format("YYYY-MM-DD HH:mm:ss"),
                      ChangeDrillingFluidBeforeDepth:this.state.data['6_1'].data.ChangeDrillingFluidBeforeDepth,
                      ChangeDrillingFluidAfterDepth:this.state.data['6_1'].data.ChangeDrillingFluidAfterDepth
                    }
                    console.log('pile6_1',valueApi)
                    if(this.state.Status1 == 2){
                      this.setState({saveload:true})
                    }
                    this.props.pileSaveStep06(valueApi)
          
                    if ( this.state.error == null) {
                      // await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step6Status:2,Step5Status:2})
                      // this.props.mainPileGetStorage(this.props.pileid,'step_status')
          
                      var edit = this.props.pileAll[this.props.pileid]
                      var value5_0 = {
                        process:5,
                        step:0,
                        pile_id: this.props.pileid,
                        data:{
                          id:edit['5_0'].data.Id,
                          Edit_Flag:1,
                          Status:edit['5_0'].data.Status,
                          StartDate:edit['5_0'].data.StartDate,
                          EndDate:edit['5_0'].data.EndDate,
                          LocationLat:edit['5_0'].data.LocationLat,
                          LocationLong:edit['5_0'].data.LocationLong,
                        }
                      }
                      await this.props.mainPileSetStorage(this.props.pileid,'5_0',value5_0)
          
                      var edit = this.props.pileAll[this.props.pileid]
                      var value6_0 = {
                        process:6,
                        step:0,
                        pile_id: this.props.pileid,
                        data:{
                          id:edit['6_0'].data.Id,
                          Edit_Flag:1,
                          Status:edit['6_0'].data.Status,
                          StartDate:edit['6_0'].data.StartDate,
                          EndDate:edit['6_0'].data.EndDate,
                          LocationLat:edit['6_0'].data.LocationLat,
                          LocationLong:edit['6_0'].data.LocationLong,
                        }
                      }
                      await this.props.mainPileSetStorage(this.props.pileid,'6_0',value6_0)
          
                      var value0 = {
                        process: 7,
                        step: 0,
                        pile_id: this.props.pileid,
                        shape: this.props.shape
                      }
                      this.props.mainpileAction_checkStepStatus(value0).then(data => {
                        const text = "mainpile.lockprocess.process" + 7 + "_" + (this.props.shape == 1 ? "bp" : "dw")
                        if (data.check == true) {
                          // this.props.onNextStep()
                          // this.onSetStack(7,0)
                        } else {
                          // Alert.alert("", I18n.t(text), [
                          //   {
                          //     text: "OK",
                          //   }
                          // ])
                        }
                      })
                    } else {
                      Alert.alert(this.state.error)
                      this.setState({checksavebutton:false,saveload:false})
                    }
                  }else{
                    Alert.alert('', I18n.t('mainpile.6_0.error_depth'), [
                      {
                        text: 'OK'
                      }
                    ])
                    this.setState({checksavebutton:false})
                  }
                  
                }
                  }
                // }
                
              }
              
        }else{
          Alert.alert('', I18n.t('mainpile.6_1.error_step'), [
            {
              text: 'OK'
            }
          ])
          this.setState({checksavebutton:false})
        }
        }else{
          Alert.alert('', I18n.t('mainpile.6_1.error_nextstep'), [
            {
              text: 'OK'
            }
          ])
          this.setState({checksavebutton:false})
        }
        }else {
          Alert.alert('', I18n.t('mainpile.6_1.error_nextstep'), [
            {
              text: 'OK'
            }
          ])
          this.setState({checksavebutton:false})
        }
      }
    }  else if (step == 1) {
      this.setState({checksavebutton:true})
      var value = {
        process:6,
        step:2,
        pile_id: this.props.pileid,
        category:this.props.category
      }
      this.setState({save2:false})
      // console.warn('BucketChanged',this.state.data['6_2'].data.BucketChanged)
      if(!this.state.data['6_1'].data.CrossFlag){
        
        if(this.state.data['6_2'].data.BucketChanged == false){
          Alert.alert(I18n.t('mainpile.6_2.alert1'))
          this.setState({checksavebutton:false})
        }else{

          console.warn('6_2 checkdepthalert',parseFloat(this.state.data["6_2"].data.AfterDepth).toFixed(3) ,this.state.percentageCheckDepth)

           if (this.state.data["6_2"].data.AfterDepth !== ''&&parseFloat(this.state.data["6_2"].data.AfterDepth).toFixed(3) > this.state.percentageCheckDepth) {
            
              let con = true

              let DepthAlert = await this.checkdepthalert().then((a,b)=>{
              
                  return a
              })
            
              console.log('DepthAlert',DepthAlert)
              if(DepthAlert=='YES'){
                return this.setState({checksavebutton:false})
              }
            }
          
                  if (this.state.Edit_Flag == 1) {
                    this.props.mainpileAction_checkStepStatus(value)
                      .then(async (data) =>
                      {
                        if ( this.state.error == null) {
                          // console.warn('data,check',data.check)
                          if (data.check == false) {
                            if(this.state.Status6 == 2){
                              var valueend = {
                                process:6,
                                step:2,
                                pile_id: this.props.pileid,
                                data:{
                                  BucketChanged: this.state.data["6_2"].data.BucketChanged,
                                  WaitingTimeMinute: this.state.data["6_2"].data.WaitingTimeMinute_old,
                                  BeforeDepth: this.state.data["6_2"].data.BeforeDepth_old,
                                  AfterDepth: this.state.data["6_2"].data.AfterDepth_old,
                                  Images: this.state.data["6_2"].data.ImageFiles,
                                  
                                },
                                startdate: this.state.data["6_2"].startdate,
                                enddate: this.state.data["6_2"].enddate
                                }
                                this.props.mainPileSetStorage(this.props.pileid,'6_2',valueend)
                            }
                            Alert.alert('', data.errorText[0], [
                              {
                                text: 'OK'
                              }
                            ])
                            this.setState({checksavebutton:false,saveload:false})
                          }else {
                            var ImageFiles = []
                            if (this.state.data['6_2'].data.Images != null && this.state.data['6_2'].data.Images.length > 0) {
                              for (var i = 0; i < this.state.data['6_2'].data.Images.length; i++) {
                                ImageFiles.push(this.state.data['6_2'].data.Images[i])
                              }
                            }
                            
    
                            if(this.state.data["6_2"].enddate != ''){
                              var end = this.state.data["6_2"].enddate
                              if(this.state.Status6 != 2){
                                var valueend = {
                                  process:6,
                                  step:2,
                                  pile_id: this.props.pileid,
                                  data:{
                                    BucketChanged: this.state.data["6_2"].data.BucketChanged,
                                    WaitingTimeMinute: this.state.data["6_2"].data.WaitingTimeMinute,
                                    BeforeDepth: this.state.data["6_2"].data.BeforeDepth,
                                    AfterDepth: this.state.data["6_2"].data.AfterDepth,
                                    Images: this.state.data["6_2"].data.ImageFiles,
                                    
                                  },
                                  startdate: this.state.data["6_2"].startdate,
                                  enddate: end
                                  }
                                  await this.props.mainPileSetStorage(this.props.pileid,'6_2',valueend)
                              }
                             
                            }else{
                              var end = moment().format("DD/MM/YYYY HH:mm")
                              if(this.state.Status6 != 2){
                                var valueend = {
                                  process:6,
                                  step:2,
                                  pile_id: this.props.pileid,
                                  data:{
                                    BucketChanged: this.state.data["6_2"].data.BucketChanged,
                                    WaitingTimeMinute: this.state.data["6_2"].data.WaitingTimeMinute,
                                    BeforeDepth: this.state.data["6_2"].data.BeforeDepth,
                                    AfterDepth: this.state.data["6_2"].data.AfterDepth,
                                    Images: this.state.data["6_2"].data.ImageFiles,
                                    
                                  },
                                  startdate: this.state.data["6_2"].startdate,
                                  enddate: end
                                  }
                                  await this.props.mainPileSetStorage(this.props.pileid,'6_2',valueend)
                              }
                              
                            }
                            var start = moment(this.state.data["6_2"].startdate,"DD/MM/YYYY HH:mm")
                            var end1 = moment(end,"DD/MM/YYYY HH:mm")
                            if(this.state.data["6_2"].startdate != undefined){
                              if(start <= end1){
                                if(this.state.Status8 == 0){
                                var valueApi ={
                                  Language:I18n.locale,
                                  JobId: this.props.jobid,
                                  PileId: this.props.pileid,
                                  Page:2,
                                  BucketChanged: this.state.data['6_2'].data.BucketChanged,
                                  WaitingTimeMinute: this.state.data['6_2'].data.WaitingTimeMinute,
                                  BeforeDepth: this.state.data['6_2'].data.BeforeDepth,
                                  AfterDepth: this.state.data['6_2'].data.AfterDepth,
                                  ImageFiles: ImageFiles,
                                  latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
                                  longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
                                  CrossFlag:false ,
                                  startdate:moment(this.state.data['6_2'].startdate,'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm:ss'),
                                  enddate:moment(end,'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm:ss')
                                }
                                console.log('pile6_2',valueApi)
                                if(this.state.Status1 == 2){
                                  this.setState({saveload:true})
                                }
                                this.props.pileSaveStep06(valueApi)
                                
    
                            var edit = this.props.pileAll[this.props.pileid]
                            var value5_0 = {
                              process:5,
                              step:0,
                              pile_id: this.props.pileid,
                              data:{
                                id:edit['5_0'].data.Id,
                                Edit_Flag:1,
                                Status:edit['5_0'].data.Status,
                                StartDate:edit['5_0'].data.StartDate,
                                EndDate:edit['5_0'].data.EndDate,
                                LocationLat:edit['5_0'].data.LocationLat,
                                LocationLong:edit['5_0'].data.LocationLong,
                              }
                            }
                            await this.props.mainPileSetStorage(this.props.pileid,'5_0',value5_0)
    
                            var edit = this.props.pileAll[this.props.pileid]
                            var value6_0 = {
                              process:6,
                              step:0,
                              pile_id: this.props.pileid,
                              data:{
                                id:edit['6_0'].data.Id,
                                Edit_Flag:1,
                                Status:edit['6_0'].data.Status,
                                StartDate:edit['6_0'].data.StartDate,
                                EndDate:edit['6_0'].data.EndDate,
                                LocationLat:edit['6_0'].data.LocationLat,
                                LocationLong:edit['6_0'].data.LocationLong,
                              }
                            }
                            await this.props.mainPileSetStorage(this.props.pileid,'6_0',value6_0)
                            
                            var value0 = {
                              process: 7,
                              step: 0,
                              pile_id: this.props.pileid,
                              shape: this.props.shape
                            }
                            // this.props.mainpileAction_checkStepStatus(value0).then(data => {
                              // const text = "mainpile.lockprocess.process" + 7 + "_" + (this.props.shape == 1 ? "bp" : "dw")
                              if (data.check == true) {
                            //     await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step6Status:data.statuscolor})
                            // this.props.mainPileGetStorage(this.props.pileid,'step_status')
                                // Alert.alert(
                                //   '',
                                //   I18n.t('mainpile.6_2.alert2'),
                                //   [
                                //     {
                                //       text:'Ok',
                                //       onPress:  () => {
                                // if(this.state.Status6 == 2){
                                //   this.props.onNextStep()
                                //   this.onSetStack(7,0)
                                // }
                                
                                //       }
                                //     }
                                //   ])
                              } else {
                                Alert.alert("", data.errorText[0], [
                                  {
                                    text: "OK",
                                  }
                                ])
                                this.setState({checksavebutton:false})
                              }
                            // })
                                }else{
                                  // console.warn('dddd',this.props.tremielist)
                                  var length8=0
                                  for(var i=0;i<this.props.tremielist.length;i++){
                                    length8 += this.props.tremielist[i].Length
                                  }
                                  // console.warn('ccccccc',this.state.data['6_1'].data.ChangeDrillingFluidAfterDepth-0.3,length8)
                                  if(this.state.data['6_2'].data.AfterDepth >= length8){
                                    var valueApi ={
                                      Language:I18n.locale,
                                      JobId: this.props.jobid,
                                      PileId: this.props.pileid,
                                      Page:2,
                                      BucketChanged: this.state.data['6_2'].data.BucketChanged,
                                      WaitingTimeMinute: this.state.data['6_2'].data.WaitingTimeMinute,
                                      BeforeDepth: this.state.data['6_2'].data.BeforeDepth,
                                      AfterDepth: this.state.data['6_2'].data.AfterDepth,
                                      ImageFiles: ImageFiles,
                                      latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
                                      longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
                                      CrossFlag:false ,
                                      startdate:moment(this.state.data['6_2'].startdate,'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm:ss'),
                                      enddate:moment(end,'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm:ss')
                                    }
                                    console.log('pile6_2',valueApi)
                                    if(this.state.Status1 == 2){
                                      this.setState({saveload:true})
                                    }
                                    this.props.pileSaveStep06(valueApi)
                                    
        
                                var edit = this.props.pileAll[this.props.pileid]
                                var value5_0 = {
                                  process:5,
                                  step:0,
                                  pile_id: this.props.pileid,
                                  data:{
                                    id:edit['5_0'].data.Id,
                                    Edit_Flag:1,
                                    Status:edit['5_0'].data.Status,
                                    StartDate:edit['5_0'].data.StartDate,
                                    EndDate:edit['5_0'].data.EndDate,
                                    LocationLat:edit['5_0'].data.LocationLat,
                                    LocationLong:edit['5_0'].data.LocationLong,
                                  }
                                }
                                await this.props.mainPileSetStorage(this.props.pileid,'5_0',value5_0)
        
                                var edit = this.props.pileAll[this.props.pileid]
                                var value6_0 = {
                                  process:6,
                                  step:0,
                                  pile_id: this.props.pileid,
                                  data:{
                                    id:edit['6_0'].data.Id,
                                    Edit_Flag:1,
                                    Status:edit['6_0'].data.Status,
                                    StartDate:edit['6_0'].data.StartDate,
                                    EndDate:edit['6_0'].data.EndDate,
                                    LocationLat:edit['6_0'].data.LocationLat,
                                    LocationLong:edit['6_0'].data.LocationLong,
                                  }
                                }
                                await this.props.mainPileSetStorage(this.props.pileid,'6_0',value6_0)
                                
                                var value0 = {
                                  process: 7,
                                  step: 0,
                                  pile_id: this.props.pileid,
                                  shape: this.props.shape
                                }
                                // this.props.mainpileAction_checkStepStatus(value0).then(data => {
                                  // const text = "mainpile.lockprocess.process" + 7 + "_" + (this.props.shape == 1 ? "bp" : "dw")
                                  if (data.check == true) {
                                //     await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step6Status:data.statuscolor})
                                // this.props.mainPileGetStorage(this.props.pileid,'step_status')
                                    // Alert.alert(
                                    //   '',
                                    //   I18n.t('mainpile.6_2.alert2'),
                                    //   [
                                    //     {
                                    //       text:'Ok',
                                    //       onPress:  () => {
                                    // if(this.state.Status6 == 2){
                                    //   this.props.onNextStep()
                                    //   this.onSetStack(7,0)
                                    // }
                                    
                                    //       }
                                    //     }
                                    //   ])
                                  } else {
                                    Alert.alert("", data.errorText[0], [
                                      {
                                        text: "OK",
                                      }
                                    ])
                                    this.setState({checksavebutton:false})
                                  }
                                  }else{
                                    Alert.alert('', I18n.t('mainpile.6_0.error_depth'), [
                                      {
                                        text: 'OK'
                                      }
                                    ])
                                    this.setState({checksavebutton:false})
                                  }

                                }
                              }else{
                                Alert.alert('', I18n.t("mainpile.liquidtestinsert.alert.error4"), [
                                  {
                                    text: 'OK'
                                  }
                                ])
                                this.setState({checksavebutton:false})
                              }
                            }else{
                              if(this.props.startdate <= this.props.enddate){
                                var valueApi ={
                                  Language:I18n.locale,
                                  JobId: this.props.jobid,
                                  PileId: this.props.pileid,
                                  Page:2,
                                  BucketChanged: this.state.data['6_2'].data.BucketChanged,
                                  WaitingTimeMinute: this.state.data['6_2'].data.WaitingTimeMinute,
                                  BeforeDepth: this.state.data['6_2'].data.BeforeDepth,
                                  AfterDepth: this.state.data['6_2'].data.AfterDepth,
                                  ImageFiles: ImageFiles,
                                  latitude:this.state.location == undefined ? 1 : this.state.location.position.lat ,
                                  longitude:this.state.location == undefined ? 1 : this.state.location.position.log ,
                                  CrossFlag:false ,
                                  startdate:moment(this.props.startdate,'DD/MM/YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss'),
                                  enddate:moment(this.props.enddate,'DD/MM/YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss')
                                }
                                console.log('pile6_2',valueApi)
                                if(this.state.Status1 == 2){
                                  this.setState({saveload:true})
                                }
                                this.props.pileSaveStep06(valueApi)
                            //     await this.props.mainPileSetStorage(this.props.pileid,'step_status',{Step6Status:data.statuscolor})
                            // this.props.mainPileGetStorage(this.props.pileid,'step_status')
    
                            var edit = this.props.pileAll[this.props.pileid]
                            var value5_0 = {
                              process:5,
                              step:0,
                              pile_id: this.props.pileid,
                              data:{
                                id:edit['5_0'].data.Id,
                                Edit_Flag:1,
                                Status:edit['5_0'].data.Status,
                                StartDate:edit['5_0'].data.StartDate,
                                EndDate:edit['5_0'].data.EndDate,
                                LocationLat:edit['5_0'].data.LocationLat,
                                LocationLong:edit['5_0'].data.LocationLong,
                              }
                            }
                            await this.props.mainPileSetStorage(this.props.pileid,'5_0',value5_0)
    
                            var edit = this.props.pileAll[this.props.pileid]
                            var value6_0 = {
                              process:6,
                              step:0,
                              pile_id: this.props.pileid,
                              data:{
                                id:edit['6_0'].data.Id,
                                Edit_Flag:1,
                                Status:edit['6_0'].data.Status,
                                StartDate:edit['6_0'].data.StartDate,
                                EndDate:edit['6_0'].data.EndDate,
                                LocationLat:edit['6_0'].data.LocationLat,
                                LocationLong:edit['6_0'].data.LocationLong,
                              }
                            }
                            await this.props.mainPileSetStorage(this.props.pileid,'6_0',value6_0)
                            
                            var value0 = {
                              process: 7,
                              step: 0,
                              pile_id: this.props.pileid,
                              shape: this.props.shape
                            }
                            this.props.mainpileAction_checkStepStatus(value0).then(data => {
                              const text = "mainpile.lockprocess.process" + 7 + "_" + (this.props.shape == 1 ? "bp" : "dw")
                              if (data.check == true) {
                                // Alert.alert(
                                //   '',
                                //   I18n.t('mainpile.6_2.alert2'),
                                //   [
                                    
                                //     {
                                //       text:'Ok',
                                //       onPress:  () => {
                                  if(this.state.Status6 == 2){
                                    this.props.onNextStep()
                                    this.onSetStack(7,0)
                                  }
                                // this.props.onNextStep()
                                // this.onSetStack(7,0)
                                //       }
                                //     }])
                              } else {
                                // Alert.alert("", I18n.t(text), [
                                //   {
                                //     text: "OK",
                                //   }
                                // ])
                              }
                            })
      
                              }else{
                                Alert.alert('', I18n.t('mainpile.3_1.error_step'), [
                                  {
                                    text: 'OK'
                                  }
                                ])
                                this.setState({checksavebutton:false})
                              }
                            }

                            
                            
    
                          }
                        } else {
                          Alert.alert(this.state.error)
                          this.setState({checksavebutton:false,saveload:false})
                        }
                      })
                  }else {
                    
                    // Alert.alert(
                    //   '',
                    //   I18n.t('mainpile.6_2.alert2'),
                    //   [
                        
                    //     {
                    //       text:'Ok',
                    //       onPress:  () => {
                      if(this.state.Status6 == 2){
                        this.props.onNextStep()
                        this.onSetStack(7,0)
                      }
                    // this.props.onNextStep()
                    // this.onSetStack(7,0)
                    //       }
                    //     }])
                  }
                
        }
      }else{
        // Alert.alert(
        //   '',
        //   I18n.t('mainpile.6_2.alert2'),
        //   [
            
        //     {
        //       text:'Ok',
        //       onPress:  () => {
          if(this.state.Status6 == 2){
            this.props.onNextStep()
            this.onSetStack(7,0)
          }
        // this.props.onNextStep()
        // this.onSetStack(7,0)
        //       }
        //     }])
      }
        
    }
  }

  checkdepthalert=(type)=>{
    let text,warning =''

    // if(this.props.shape == 1){
    //   text='mainpile.drillpileinsert'
    // }else{
    //   text='mainpile.drillpileinsert_dw'
    // }
    if(type=='1'){
      warning =I18n.t( 'mainpile.6_1.errordepth5')
    }else{
      warning =I18n.t( 'mainpile.6_2.alert3')
    }
    return new Promise((resolve, reject) => {
      Alert.alert('',warning,[
        {
          text: I18n.t( 'mainpile.drillpileinsert.depth3_yes'),onPress:()=>{
            resolve('YES')
          }
        },
        {
          text: I18n.t('mainpile.drillpileinsert.depth3_no'),onPress:()=>{
            resolve('NO')
          }
        }
      ],{cancelable:false}) 
    })
   
  }

  async onSetStack(pro,step) {
    // console.warn('onSetStack')
    var data = {
      process: pro,
      step: step + 1
    }
    await this.props.setStack(data)
    await this.props.mainPileGetAllStorage()
  }
  setStep(step){
    return this.setState({step:step})
  }

  _renderStepFooter() {
    // console.warn(this.state.data['6_1'])
    // let checkcross = this.state.data&&this.state.data['6_1']&&this.state.data['6_1'].data&&this.state.data['6_1'].data.CrossFlag&&this.state.data['6_1'].data.CrossFlag == true
    // let checkcross = false
    // if( this.state.data&&this.state.data['6_1']&&this.state.data['6_1'].data&&this.state.data['6_1'].data.CrossFlag){
    //   console.log('6_1 crossf',this.state.data['6_1'].data.CrossFlag)
    //   checkcross = this.state.data['6_1'].data.CrossFlag 
    // }

    //  if(checkcross==true){
    //   return (
    //     <View>
    //       <View style={styles.buttonFooterGroup}>
    //         <View style={styles.buttonFooterStep}>
    //           <StepIndicator stepCount={1} onPress={step => {
    //             console.warn('step')
    //             if (this.refs['pile6_1'].getWrappedInstance().state.loading != true) {
    //               if(this.state.data&&this.state.data['6_1']&&this.state.data['6_1'].data&&this.state.data['6_1'].data.CrossFlag&&this.state.data['6_1'].data.CrossFlag == false){
    //                 this.onSetStack(6,step)
    //                 this.setState({step: step})
    //               }
    //             }
    //             }} currentPosition={this.state.step} />
    //         </View>
    //         {
    //           this.state.Edit_Flag == 1 ?
    //           <View style={[styles.second,
    //             this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
    //           ]}>
    //             <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}>
    //               <Text style={styles.selectButton}>{I18n.t('mainpile.button.delete')}</Text>
    //             </TouchableOpacity>
    //             <TouchableOpacity style={[styles.secondButton,
    //               this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
    //             ]} onPress={this.nextButton}
    //             disabled={this.state.checksavebutton}
    //             >
    //               <Text style={styles.selectButton}>{this.state.Edit_Flag == 1 ? I18n.t('mainpile.button.next'):I18n.t('mainpile.button.continue')}</Text>
    //             </TouchableOpacity>
    //           </View>
    //           :
    //           <View/>
    //         }
    //       </View>
    //     </View>
    //   )
    //  }else{
    //   return (
    //     <View>
    //       <View style={styles.buttonFooterGroup}>
    //         <View style={styles.buttonFooterStep}>
    //           <StepIndicator stepCount={2} onPress={step => {
    //             console.warn('step')
    //             if (this.refs['pile6_1'].getWrappedInstance().state.loading != true) {
    //               if(this.state.data&&this.state.data['6_1']&&this.state.data['6_1'].data&&this.state.data['6_1'].data.CrossFlag&&this.state.data['6_1'].data.CrossFlag == false){
    //                 this.onSetStack(6,step)
    //                 this.setState({step: step})
    //               }
    //             }
    //             }} currentPosition={this.state.step} />
    //         </View>
    //         {
    //           this.state.Edit_Flag == 1 ?
    //           <View style={[styles.second,
    //             this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
    //           ]}>
    //             <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}>
    //               <Text style={styles.selectButton}>{I18n.t('mainpile.button.delete')}</Text>
    //             </TouchableOpacity>
    //             <TouchableOpacity style={[styles.secondButton,
    //               this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
    //             ]} onPress={this.nextButton}
    //             disabled={this.state.checksavebutton}
    //             >
    //               <Text style={styles.selectButton}>{this.state.Edit_Flag == 1 ? I18n.t('mainpile.button.next'):I18n.t('mainpile.button.continue')}</Text>
    //             </TouchableOpacity>
    //           </View>
    //           :
    //           <View/>
    //         }
    //       </View>
    //     </View>
    //   )
    //  }
    let checkcross = false
    if(this.state.data&&this.state.data['6_1']&&this.state.data['6_1'].data &&this.state.data['6_1'].data.CrossFlag) {

    console.warn(this.state.data['6_1'].data.CrossFlag)
      checkcross = this.state.data['6_1'].data.CrossFlag
    }

    return (
      <View>
        <View style={styles.buttonFooterGroup}>
          {checkcross == false && <View style={styles.buttonFooterStep}>
            <StepIndicator stepCount={2} onPress={step => {
              if (this.refs['pile6_1'].getWrappedInstance().state.loading != true) {
                if(this.state.data['6_1'].data.CrossFlag == false){
                  this.onSetStack(6,step)
                  this.setState({step: step})
                }
              }
              }} currentPosition={this.state.step} />
          </View>}
          {
            this.state.Edit_Flag == 1 ?
            <View style={[styles.second,
              this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
            ]}>
              <TouchableOpacity style={styles.firstButton} onPress={this.deleteButton}>
                <Text style={styles.selectButton}>{I18n.t('mainpile.button.delete')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.secondButton,
                this.props.category == 3|| this.props.category == 5 ? {backgroundColor: DEFAULT_COLOR_4}:this.props.shape == 1 ? {backgroundColor: MAIN_COLOR}:{backgroundColor: DEFAULT_COLOR_1}
              ]} onPress={this.nextButton}
              disabled={this.state.checksavebutton}
              >
                <Text style={styles.selectButton}>{this.state.Edit_Flag == 1 ? I18n.t('mainpile.button.next'):I18n.t('mainpile.button.continue')}</Text>
              </TouchableOpacity>
            </View>
            :
            <View/>
          }
        </View>
      </View>
    )
  }

  nextButton = async () => {
    switch (await this.state.step) {
      case 0:
        this.onSave(this.state.step)
        break
      case 1:
        this.onSave(this.state.step)
        break
      default:
        break
    }
  }

  initialStep(){
    this.setState({step:0})
  }

   deleteButton = () => {
    this.props.setStack({
      process: 6,
      step: 1
    })
    const data = {
      pileid: this.props.pileid,
      jobid: this.props.jobid,
      processid: this.state.process,
      lat: this.state.location == undefined ? 1 : this.state.location.position.lat,
      log: this.state.location == undefined ? 1 : this.state.location.position.log,
    }
    if(this.state.Status7 == 0){
      Alert.alert("Warning", I18n.t("mainpile.button.confirmdelete"), [
        { text: "Cancel" },
        { text: "OK", onPress: () => this.props.pileDelete(data) }
      ])
    }else{
      Alert.alert("", I18n.t('alert.error_delete6'), [
        {
          text: "OK"
        }
      ])
    }
    
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    // console.warn('6_0')
    return (
      <View style={{ flex: 1 }}>
        {this.state.saveload?<Content><ActivityIndicator size="large" color="#007CC2" /></Content>:<View/>}
        {!this.state.saveload?<Content>
          <Pile6_1 
            ref='pile6_1' 
            hidden={!(this.state.step == 0)} 
            jobid={this.props.jobid} 
            pileid={this.props.pileid}
            category={this.props.category} 
            onRefresh={this.props.onRefresh} 
            pileinfo = {this.props.pileinfo}
            />

          <Pile6_2 
            ref='pile6_2' 
            hidden={!(this.state.step == 1)} 
            jobid={this.props.jobid} 
            pileid={this.props.pileid} 
            shape={this.props.shape} 
            category={this.props.category} 
            startdatedefault={this.state.startdate}
            startdate={this.props.startdate}
            enddate={this.props.enddate}/>
            
        </Content>:<View/>}
        {this._renderStepFooter()}
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    error: state.pile.error,
    pileAll: state.mainpile.pileAll || null,
    stack: state.mainpile.stack_item,
    drillinglist: state.drillinglist.drillinglist,
    drillinglistChild: state.drillinglist.drillinglistChild,
    pile6_1success:state.pile.pile6_1success,
    pile6_2success:state.pile.pile6_2success,
    tremielist: state.tremie.tremielist,
    random1:state.pile.random1,
    random2:state.pile.random2,

    step_status2_random:state.mainpile.step_status2_random,
    step_status2:state.mainpile.step_status2,
    processstatus:state.mainpile.process,

    checkto7_super:state.pile.checkto7_super,
    lockstepcheckto7_super:state.pile.lockstepcheckto7_super,
    checkto7_super_random:state.pile.checkto7_super_random,
    checkto7_super_process:state.pile.checkto7_super_process
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(Pile6)
