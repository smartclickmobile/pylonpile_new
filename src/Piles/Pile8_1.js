import React, { Component } from "react"
import { View, Text, TouchableOpacity, TextInput } from "react-native"
import ModalSelector from "react-native-modal-selector"
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM } from "../Constants/Color"
import { Icon } from "react-native-elements"
import * as actions from "../Actions"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import I18n from "../../assets/languages/i18n"
import styles from "./styles/Pile8_1.style"

class Pile8_1 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      machine: "",
      machineid: "",
      driver: "",
      driverid: "",
      tremietype: "",
      tremietypeid: "",
      machineData: [{ key: 0, section: true, label: I18n.t("mainpile.8_1.machineselect") }],
      driverData: [{ key: 0, section: true, label: I18n.t("mainpile.8_1.driverselect") }],
      tremietypeData:[{ key: 0, section: true, label: I18n.t("mainpile.8_1.tremietypeselect") }],
      process: 8,
      step: 1,
      Edit_Flag: 1,
      Parentcheck: false
    }
  }

  updateState(value) {
    console.log('Update state from 8_1')
  }

  componentDidMount() {
    if (this.props.onRefresh === "refresh") {
      Actions.refresh({
        action: "start"
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    var pile = null
    var pileMaster = null
    var pile8 = null
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["8_1"]) {
      pile = nextProps.pileAll[this.props.pileid]["8_1"].data
      pileMaster = nextProps.pileAll[this.props.pileid]["8_1"].masterInfo
      
    }
    if (
      nextProps.pileAll[this.props.pileid] &&
      nextProps.pileAll[this.props.pileid]['9_1'] &&
      (this.props.category == 5 || this.props.category == 3)
    ) {

      let pileMaster9 = nextProps.pileAll[this.props.pileid]['9_1'].masterInfo;
      if (pileMaster9) {
        if (pileMaster9.PileDetail) {

          this.setState({
            Parentcheck:
            pileMaster9.PileDetail.pile_id == pileMaster9.PileDetail.parent_id
                ? true
                : false,
          })
        }
      }
    }
    if (nextProps.pileAll[this.props.pileid] && nextProps.pileAll[this.props.pileid]["8_0"]) {
      pile8 = nextProps.pileAll[this.props.pileid]["8_0"].data
    }

    if (pileMaster) {
      if (pileMaster.MachineList) {
        let machine = [{ key: 0, section: true, label: I18n.t("mainpile.8_1.machineselect") }]
        pileMaster.MachineList.map((data, index) => {
          machine.push({ key: index + 1, label: data.machine_no, id: data.itemid })
          if (pile && pile.Machine) {
            if (pile.Machine.machine_no == data.machine_no && pile.Machine.itemid == data.itemid) {
              this.setState({ machine: data.machine_no, machineid: data.itemid })
            }
          }
          if (pile && pile.MachineName && this.state.machine == "") {
            if (pile.MachineName == data.machine_no) {
              // console.warn(' NAME ', pile.MachineName)
              this.setState({ machine: data.machine_no, machineid: data.itemid })
            }
          }
           /**
           * If value don't have anything set value to nothing
           */
          // Value from API
          if (pile.hasOwnProperty('Machine') && pile.Machine == null) {
            this.setState({ machine: "", machineid: "" })
          }
          // Value from local
          if (pile.hasOwnProperty('MachineName') && pile.MachineName == null) {
            this.setState({ machine: "", machineid: "" })
          }
        })
        this.setState({ machineData: machine })
      }
      if (pileMaster.DriverList) {
        let driver = [{ key: 0, section: true, label: I18n.t("mainpile.8_1.driverselect") }]
        pileMaster.DriverList.map((data, index) => {
          let name = (data.nickname && data.nickname + "-") + data.firstname + " " + data.lastname
          driver.push({ key: index + 1, label: name, id: data.employee_id })
          if (pile && pile.DriverId&&this.state.driver=="") {
           
            if (pile.DriverId == data.employee_id) {
              this.setState({ driver: name, driverid: data.employee_id })
            }
            
          }
          if (pile.hasOwnProperty('DriverId') && pile.DriverId == null) {
            this.setState({ driver: "", driverid: "" })
          }
        })
        this.setState({ driverData: driver })
      }
      if(pileMaster.TremieTypeList){
        let tremietype = [{ key: 0, section: true, label: I18n.t("mainpile.8_1.tremietypeselect") }]
        pileMaster.TremieTypeList.map((data, index)=>{
          tremietype.push({key: index + 1, label: data.Name, id: data.Id})
          if (pile&&pile.TremieTypeId&&this.state.tremietype=="") {
             if (pile.TremieTypeId == data.Id) {
              this.setState({ tremietype: data.Name, tremietypeid: data.Id })
            }
          }
          if (pile.hasOwnProperty('TremieTypeId') && pile.DriverId == null) {
            this.setState({ tremietype: "", tremietypeid: "" })
          }
          
        })
        this.setState({ tremietypeData: tremietype })
      }
    }

    if (pile8) {
      this.setState({ Edit_Flag: pile8.Edit_Flag })
    }
    

   
  }

  selectMachine = data => {
    this.setState({ machine: data.label, machineid: data.id }, () => this.saveLocal())
  }

  selectDriver = data => {
    this.setState({ driver: data.label, driverid: data.id }, () => this.saveLocal())
  }
  selectTremieType = data => {
    this.setState({ tremietype: data.label, tremietypeid: data.id }, () => this.saveLocal())
  }

  saveLocal() {
    var value = {
      process: 8,
      step: 1,
      pile_id: this.props.pileid,
      data: {
        DriverName: this.state.driver,
        DriverId: this.state.driverid,
        MachineName: this.state.machine,
        MachineId: this.state.machineid,
        TremieTypeName:this.state.tremietype,
        TremieTypeId:this.state.tremietypeid
      }
    }
    // console.warn("test",value)
    this.props.mainPileSetStorage(this.props.pileid, "8_1", value)
  }

  render() {
    if (this.props.hidden) {
      return null
    }
    let Edit_Flag = this.state.Edit_Flag
    let Flag=  false
   
    if(this.state.Edit_Flag == 0 ||(( this.props.category == 3 || this.props.category == 5) &&this.state.Parentcheck == false)){
      console.warn('if')
      Flag = true
    }else{
      console.warn('else')
    }
    return (
      <View>
        <View style={styles.topic}>
          <Text style={styles.topicText}>{I18n.t("mainpile.8_1.topic")}</Text>
        </View>
        <View style={{ marginTop: 20 }}>
          <View style={styles.boxTopic}>
            <Text style={{ marginLeft: 10 }}>{I18n.t("mainpile.8_1.machine")}</Text>
          </View>
          <View style={styles.selectedView}>
            <ModalSelector
              data={this.state.machineData}
              onChange={this.selectMachine}
              selectTextStyle={styles.textButton}
              cancelText="Cancel"
              disabled={Flag}
            >
              <TouchableOpacity style={[styles.button,Flag==true && { borderColor: MENU_GREY_ITEM }]} disabled={Flag}>
                <View style={styles.buttonTextStyle}>
                  <Text style={[styles.buttonText, Flag==true && { color: MENU_GREY_ITEM }]}>{this.state.machine || I18n.t("mainpile.8_1.machineselect")}</Text>
                  <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                    <Icon name="arrow-drop-down" type="MaterialIcons" color={Flag==true ? MENU_GREY_ITEM : "#007CC2" } />
                  </View>
                </View>
              </TouchableOpacity>
            </ModalSelector>
          </View>
        </View>
        <View style={{ marginTop: 10 }}>
          <View style={styles.boxTopic}>
            <Text style={{ marginLeft: 10 }}>{I18n.t("mainpile.8_1.driver")}</Text>
          </View>
          <View style={styles.selectedView}>
            <ModalSelector
              data={this.state.driverData}
              onChange={this.selectDriver}
              selectTextStyle={styles.textButton}
              cancelText="Cancel"
              disabled={Flag}
            >
              <TouchableOpacity style={[styles.button, Flag==true && { borderColor: MENU_GREY_ITEM }]} disabled={Flag}>
                <View style={styles.buttonTextStyle}>
                <Text style={[styles.buttonText, Flag==true  && { color: MENU_GREY_ITEM }]}>{this.state.driver || I18n.t("mainpile.8_1.driverselect")}</Text>
                  <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                    <Icon name="arrow-drop-down" type="MaterialIcons" color={ Flag==true  ? MENU_GREY_ITEM : "#007CC2" } />
                  </View>
                </View>
              </TouchableOpacity>
            </ModalSelector>
          </View>
        </View>
        {/* {this.props.shape!=1 && this.props.category != 3 && this.props.category != 5   &&( <View style={{ marginTop: 10 }}> */}
        {this.props.shape!=1 && this.props.category != 5   &&( <View style={{ marginTop: 10 }}>
        <View style={styles.boxTopic}>
          <Text style={{ marginLeft: 10 }}>{I18n.t("mainpile.8_1.tremietype")}</Text>
        </View>
        <View style={styles.selectedView}>
          <ModalSelector
            data={this.state.tremietypeData}
            onChange={this.selectTremieType}
            selectTextStyle={styles.textButton}
            cancelText="Cancel"
            disabled={Flag}
          >
            <TouchableOpacity style={[styles.button,  Flag==true && { borderColor: MENU_GREY_ITEM }]} disabled={ Flag}>
              <View style={styles.buttonTextStyle}>
              <Text style={[styles.buttonText, Flag==true&& { color: MENU_GREY_ITEM }]}>{this.state.tremietype || I18n.t("mainpile.8_1.tremietypeselect")}</Text>
                <View style={{ justifyContent: "flex-end", flexDirection: "row" }}>
                  <Icon name="arrow-drop-down" type="MaterialIcons" color={ Flag==true ? MENU_GREY_ITEM : "#007CC2" } />
                </View>
              </View>
            </TouchableOpacity>
          </ModalSelector>
        </View>
      </View>)}
        
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(Pile8_1)
