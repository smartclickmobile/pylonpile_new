import React, { Component } from "react"
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput
} from "react-native"
import { MAIN_COLOR, SUB_COLOR } from "../Constants/Color"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"
import I18n from "../../assets/languages/i18n"
import { Icon } from "react-native-elements"
import styles from "./styles/Pile2_2.style"
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from "moment"
class Pile8_3 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      process: 8,
      step: 3,
      checkfoam: false,
      foamimage: [],
      Edit_Flag: 1,
      datevisibletime: false,
      datevisibledate: false,
      startdate: "",
      enddate: "",
      type: "",
      datestart: "",
      timestart: "",
      dateend: "",
      timeend: "",
      set: false,
      set1: false,
      Parentcheck: false
    }
  }

  componentWillReceiveProps(nextProps) {
    // console.warn(this.props.enddatedefault,this.props.enddate,this.state.set,this.props.startdatedefault,this.props.startdate)
    var pile = null
    var pileMaster = null
    var pile8 = null

    if (
      nextProps.pileAll[this.props.pileid] &&
      nextProps.pileAll[this.props.pileid]["8_3"]
    ) {
      pile = nextProps.pileAll[this.props.pileid]["8_3"].data
    }

    if (
      nextProps.pileAll[this.props.pileid] &&
      nextProps.pileAll[this.props.pileid]["8_0"]
    ) {
      pile8 = nextProps.pileAll[this.props.pileid]["8_0"].data
    }

    if (pile8) {
      this.setState({ Edit_Flag: pile8.Edit_Flag })
    }

    if (pile) {
      if (pile.checkfoam != null && pile.checkfoam != undefined) {
        this.setState({ checkfoam: pile.checkfoam })
      }
      if (pile.foamimage != null && pile.foamimage != undefined) {
        this.setState({ foamimage: pile.foamimage })
      }
    }
    console.warn('startdate',this.props.startdatedefault,this.props.enddatedefault,this.props.startdate,this.props.enddate)
    if(!this.state.set1){
      if (!this.state.set) {
        if (
          this.props.startdatedefault != "" &&
          this.props.startdatedefault != null
        ) {
          if (this.props.enddate != "" && this.props.enddate != null) {
            this.setState({
              datestart: moment(
                this.props.startdatedefault,
                "DD/MM/YYYY HH:mm"
              ).format("DD/MM/YYYY"),
              timestart: moment(
                this.props.startdatedefault,
                "DD/MM/YYYY HH:mm"
              ).format("HH:mm"),
              dateend: moment(this.props.enddate, "DD/MM/YYYY HH:mm").format(
                "DD/MM/YYYY"
              ),
              timeend: moment(this.props.enddate, "DD/MM/YYYY HH:mm").format(
                "HH:mm"
              ),
              startdate: moment(
                this.props.startdatedefault,
                "DD/MM/YYYY HH:mm"
              ).format("DD/MM/YYYY HH:mm"),
              enddate: moment(this.props.enddate, "DD/MM/YYYY HH:mm").format(
                "DD/MM/YYYY HH:mm"
              )
            },()=>{

            })
          } else {
            if (
              this.props.enddatedefault != "" &&
              this.props.enddatedefault != null
            ) {
              this.setState({
                datestart: moment(
                  this.props.startdatedefault,
                  "DD/MM/YYYY HH:mm"
                ).format("DD/MM/YYYY"),
                timestart: moment(
                  this.props.startdatedefault,
                  "DD/MM/YYYY HH:mm"
                ).format("HH:mm"),
                dateend: moment(
                  this.props.enddatedefault,
                  "DD/MM/YYYY HH:mm"
                ).format("DD/MM/YYYY"),
                timeend: moment(
                  this.props.enddatedefault,
                  "DD/MM/YYYY HH:mm"
                ).format("HH:mm"),
                startdate: moment(
                  this.props.startdatedefault,
                  "DD/MM/YYYY HH:mm"
                ).format("DD/MM/YYYY HH:mm"),
                enddate: moment(
                  this.props.enddatedefault,
                  "DD/MM/YYYY HH:mm"
                ).format("DD/MM/YYYY HH:mm")
              })
            } else {
              this.setState({
                datestart: moment(
                  this.props.startdatedefault,
                  "DD/MM/YYYY HH:mm"
                ).format("DD/MM/YYYY"),
                timestart: moment(
                  this.props.startdatedefault,
                  "DD/MM/YYYY HH:mm"
                ).format("HH:mm"),
                dateend: "",
                timeend: "",
                startdate: moment(
                  this.props.startdatedefault,
                  "DD/MM/YYYY HH:mm"
                ).format("DD/MM/YYYY HH:mm"),
                enddate: ""
              })
            }
          }
        } else {
          if (this.props.startdate != "" && this.props.startdate != null) {
            this.setState({
              datestart: moment(this.props.startdate, "DD/MM/YYYY HH:mm").format(
                "DD/MM/YYYY"
              ),
              timestart: moment(this.props.startdate, "DD/MM/YYYY HH:mm").format(
                "HH:mm"
              ),
              startdate: moment(this.props.startdate, "DD/MM/YYYY HH:mm").format(
                "DD/MM/YYYY HH:mm"
              )
            })
            // console.warn('check kk')
            if (this.props.enddate != "" && this.props.enddate != null) {
              // console.warn('check oo',this.props.enddate)
              this.setState({
                dateend: moment(this.props.enddate, "DD/MM/YYYY HH:mm").format(
                  "DD/MM/YYYY"
                ),
                timeend: moment(this.props.enddate, "DD/MM/YYYY HH:mm").format(
                  "HH:mm"
                ),
                enddate: moment(this.props.enddate, "DD/MM/YYYY HH:mm").format(
                  "DD/MM/YYYY HH:mm"
                )
              })
            } else if (
              this.props.enddatedefault != "" &&
              this.props.enddatedefault != null
            ) {
              console.warn(
                "check ll",
                moment(this.props.enddatedefault, "DD/MM/YYYY HH:mm").format(
                  "DD/MM/YYYY"
                )
              )
              this.setState(
                {
                  dateend: moment(
                    this.props.enddatedefault,
                    "DD/MM/YYYY HH:mm"
                  ).format("DD/MM/YYYY"),
                  timeend: moment(
                    this.props.enddatedefault,
                    "DD/MM/YYYY HH:mm"
                  ).format("HH:mm"),
                  enddate: moment(
                    this.props.enddatedefault,
                    "DD/MM/YYYY HH:mm"
                  ).format("DD/MM/YYYY HH:mm")
                },
                () => {
                  console.warn(
                    "test",
                    this.state.dateend,
                    this.state.timeend,
                    this.state.enddate
                  )
                }
              )
            } else {
              // console.warn('check jj',moment().format("DD/MM/YYYY"))
              this.setState({
                dateend: moment().format("DD/MM/YYYY"),
                timeend: moment().format("HH:mm"),
                enddate: moment().format("DD/MM/YYYY HH:mm")
              })
            }
          } else {
            this.setState({
              datestart: "",
              timestart: ""
            })
          }
        }
      }
      if (
        nextProps.pileAll[this.props.pileid]&&
        nextProps.pileAll[this.props.pileid]["8_3"].startdate != null &&
        nextProps.pileAll[this.props.pileid]["8_3"].startdate != ""
      ) {
        this.setState({
          datestart: moment(
            nextProps.pileAll[this.props.pileid]["8_3"].startdate,
            "DD/MM/YYYY HH:mm"
          ).format("DD/MM/YYYY"),
          timestart: moment(
            nextProps.pileAll[this.props.pileid]["8_3"].startdate,
            "DD/MM/YYYY HH:mm"
          ).format("HH:mm"),
          startdate: moment(
            nextProps.pileAll[this.props.pileid]["8_3"].startdate,
            "DD/MM/YYYY HH:mm"
          ).format("DD/MM/YYYY HH:mm")
        })
      }
      if (
        nextProps.pileAll[this.props.pileid]&&
        nextProps.pileAll[this.props.pileid]["8_3"].enddate != null &&
        nextProps.pileAll[this.props.pileid]["8_3"].enddate != ""
      ) {
        this.setState({
          dateend: moment(
            nextProps.pileAll[this.props.pileid]["8_3"].enddate,
            "DD/MM/YYYY HH:mm"
          ).format("DD/MM/YYYY"),
          timeend: moment(
            nextProps.pileAll[this.props.pileid]["8_3"].enddate,
            "DD/MM/YYYY HH:mm"
          ).format("HH:mm"),
          enddate: moment(
            nextProps.pileAll[this.props.pileid]["8_3"].enddate,
            "DD/MM/YYYY HH:mm"
          ).format("DD/MM/YYYY HH:mm")
        })
      }
      if (
        nextProps.pileAll[this.props.pileid] &&
        nextProps.pileAll[this.props.pileid]['9_1'] &&
        (this.props.category == 5 || this.props.category == 3)
      ) {
  
        let pileMaster9 = nextProps.pileAll[this.props.pileid]['9_1'].masterInfo;
        console.warn('Parentcheck',pileMaster9)
        if (pileMaster9) {
          if (pileMaster9.PileDetail) {
            console.warn('PileDetail',pileMaster9.PileDetail)
            this.setState({
              Parentcheck:
              pileMaster9.PileDetail.pile_id == pileMaster9.PileDetail.parent_id
                  ? true
                  : false,
            },()=>{
              console.warn('Parentcheck',this.state.Parentcheck )
            });
          }
        }
      }
        if(this.props.category == 4 && this.state.startdate!= "" && this.state.enddate != ""){
          this.saveLocal('8_3')
          this.setState({set1:true})
        }
    }
  }

  onCameraRoll(keyname, photos, type) {
    
    // console.warn('Parentcheck',( this.props.category == 3||this.props.category == 5) &&this.state.Parentcheck == true)
    if (this.state.Edit_Flag == 1) {
      Actions.camearaRoll({
        jobid: this.props.jobid,
        process: this.state.process,
        step: this.state.step,
        pileId: this.props.pileid,
        keyname: keyname,
        photos: photos,
        typeViewPhoto: type,
        shape: this.props.shape,
        category: this.props.category,
        Edit_Flag: this.state.Edit_Flag
      })
    } else {
      Actions.camearaRoll({
        jobid: this.props.jobid,
        process: this.state.process,
        step: this.state.step,
        pileId: this.props.pileid,
        keyname: keyname,
        photos: photos,
        typeViewPhoto: "view",
        shape: this.props.shape,
        category: this.props.category,
        Edit_Flag: this.state.Edit_Flag
      })
    }
  }

  updateState(value) {
    console.log(value)
    if (value.data.foamimage != undefined) {
      console.log("value.data.foamimage", value.data.foamimage)
      this.setState({ foamimage: value.data.foamimage }, () =>
        this.saveLocal("8_3")
      )
    }
  }
  async mixdate() {
    if (this.state.datestart != "" && this.state.timestart != "") {
      await this.setState({
        startdate: this.state.datestart + " " + this.state.timestart
      })
    }
    if (this.state.dateend != "" && this.state.timeend != "") {
      await this.setState({
        enddate: this.state.dateend + " " + this.state.timeend
      })
    }
  }
  onSelectDate(date) {
    let date2 = moment(date).format("DD/MM/YYYY")
    if (this.state.type == "start") {
      this.setState({ datestart: date2, set: true ,datevisibledate: false})
      this.saveLocal("8_3")
    } else if (this.state.type == "end") {
      this.setState({ dateend: date2, set: true ,datevisibledate: false})
      this.saveLocal("8_3")
    }
  }
  onSelectTime(date) {
    let date2 = moment(date).format("HH:mm")
    if (this.state.type == "start") {
      this.setState({ timestart: date2, set: true,datevisibletime: false })
      this.saveLocal("8_3")
    } else if (this.state.type == "end") {
      this.setState({ timeend: date2, set: true,datevisibletime: false })
      this.saveLocal("8_3")
    }
  }
  async mixdate() {
    if (this.state.datestart != "" && this.state.timestart != "") {
      await this.setState({
        startdate: this.state.datestart + " " + this.state.timestart
      })
    }
    if (this.state.dateend != "" && this.state.timeend != "") {
      await this.setState({
        enddate: this.state.dateend + " " + this.state.timeend
      })
    }
  }
  async saveLocal(process_step) {
    await this.mixdate()
    console.warn('saveLocal 8',this.state.startdate,this.state.enddate)
    var value = {
      process: 8,
      step: 3,
      pile_id: this.props.pileid,
      data: {
        checkfoam: this.state.checkfoam,
        foamimage: this.state.foamimage
      },
      startdate: this.state.startdate,
      enddate: this.state.enddate
    }
    this.props.mainPileSetStorage(this.props.pileid, process_step, value)
  }
  selectFoam() {
    if (this.state.Edit_Flag == 1) {
      this.setState({ checkfoam: !this.state.checkfoam }, () => {
        this.saveLocal("8_3")
      })
    }
  }
  getDate() {
    if (this.state.type == "start") {
      if (this.state.startdate != "") {
        return new Date(
          moment(this.state.startdate, "DD/MM/YYYY HH:mm").format(
            "MM/DD/YYYY HH:mm:ss"
          )
        )
      } else {
        if (
          this.props.startdate != "" &&
          this.props.startdate != undefined &&
          this.props.startdate != null
        ) {
          return new Date(
            moment(this.props.startdate, "DD/MM/YYYY HH:mm").format(
              "MM/DD/YYYY HH:mm:ss"
            )
          )
        } else {
          return new Date()
        }
      }
    } else {
      if (this.state.enddate != "") {
        return new Date(
          moment(this.state.enddate, "DD/MM/YYYY HH:mm").format(
            "MM/DD/YYYY HH:mm:ss"
          )
        )
      } else {
        if (
          this.props.enddate != "" &&
          this.props.enddate != undefined &&
          this.props.enddate != null
        ) {
          return new Date(
            moment(this.props.enddate, "DD/MM/YYYY HH:mm").format(
              "MM/DD/YYYY HH:mm:ss"
            )
          )
        } else {
          return new Date()
        }
      }
    }
  }
  onChangedate = (event, selectedDate) => {
    if(event.type == 'set'){
      this.onSelectDate(selectedDate)
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };
  onChangetime = (event, selectedDate) => {
    if(event.type == 'set'){
      this.onSelectTime(selectedDate)
    }else{
      this.setState({datevisibledate: false,datevisibletime: false})
    }
  };
  render() {
    if (this.props.hidden) {
      return null
    }
    let Flag=  false
   
    if(this.state.Edit_Flag == 0 ||(( this.props.category == 3 || this.props.category == 5) &&this.state.Parentcheck == false)){
      console.warn('if')
      Flag = true
    }else{
      console.warn('else')
    }
    return (
      <View>
        <View style={styles.box}>
        {this.state.datevisibledate &&<DateTimePicker
          value={this.getDate()}
          isVisible={this.state.datevisibledate}
          is24Hour={true}
          display='spinner'
          mode='date'
          onChange={this.onChangedate}
          />}
        {this.state.datevisibletime && <DateTimePicker
          value={this.getDate()}
          isVisible={this.state.datevisibletime}
          is24Hour={true}
          display='spinner'
          mode='time'
          onChange={this.onChangetime}
        />}

          <View>
            <View style={[styles.listTopic, { marginTop: 10 }]}>
              <Text style={styles.textTopicBox}>
                {I18n.t("startenddate.startenddate")}
                {I18n.t("mainpile.8_3.stepname")}
              </Text>
            </View>
            <View
              style={[
                styles.imageDetail,
                { marginLeft: 20, marginRight: 20, marginBottom: 15 }
              ]}
            >
              <Text style={{ width: "25%" }}>
                {I18n.t("startenddate.start")}
              </Text>
              <TouchableOpacity
                style={[styles.buttonBox, { width: "35%" }]}
                onPress={() => {
                  this.setState({ datevisibledate: true, type: "start" })
                }}
                disabled={this.state.Edit_Flag == 0||(( this.props.category == 3 || this.props.category == 5) &&this.state.Parentcheck == false)}
              >
                <Text>{this.state.datestart}</Text>
              </TouchableOpacity>
              <Text style={{ width: "10%", textAlign: "center" }}>
                {I18n.t("startenddate.time")}
              </Text>
              <TouchableOpacity
                style={[styles.buttonBox, { width: "30%" }]}
                onPress={() => {
                  this.setState({ datevisibletime: true, type: "start" })
                }}
                disabled={this.state.Edit_Flag == 0||(( this.props.category == 3 || this.props.category == 5) &&this.state.Parentcheck == false)}
              >
                <Text>{this.state.timestart}</Text>
              </TouchableOpacity>
              {/*<TouchableOpacity
          style={styles.approveBox}
          onPress={() => this.onnowdate('start')}
          disabled={this.state.Edit_Flag == 0}
        >
          <Text style={{color:'#fff'}}>เริ่ม</Text>
        </TouchableOpacity>*/}
            </View>
            <View
              style={[
                styles.imageDetail,
                { marginLeft: 20, marginRight: 20, marginBottom: 15 }
              ]}
            >
              <Text style={{ width: "25%" }}>{I18n.t("startenddate.end")}</Text>
              <TouchableOpacity
                style={[styles.buttonBox, { width: "35%" }]}
                onPress={() => {
                  this.setState({ datevisibledate: true, type: "end" })
                }}
                disabled={this.state.Edit_Flag == 0||(( this.props.category == 3 || this.props.category == 5) &&this.state.Parentcheck == false)}
              >
                <Text>{this.state.dateend}</Text>
              </TouchableOpacity>
              <Text style={{ width: "10%", textAlign: "center" }}>
                {I18n.t("startenddate.time")}
              </Text>
              <TouchableOpacity
                style={[styles.buttonBox, { width: "30%" }]}
                onPress={() => {
                  this.setState({ datevisibletime: true, type: "end" })
                }}
                disabled={this.state.Edit_Flag == 0||(( this.props.category == 3 || this.props.category == 5) &&this.state.Parentcheck == false)}
              >
                <Text>{this.state.timeend}</Text>
              </TouchableOpacity>
              {/*<TouchableOpacity
          style={styles.approveBox}
          onPress={() => this.onnowdate('end')}
          disabled={this.state.Edit_Flag == 0}
        >
          <Text style={{color:'#fff'}}>สิ้นสุด</Text>
        </TouchableOpacity>*/}
            </View>
          </View>
        </View>

        {this.props.category != 4 &&<View
          style={[
            styles.box,
            this.props.category == 4&& { backgroundColor: "#bababa" }
          ]}
        >
          <View
            style={[
              styles.topicBox,
              this.props.category == 4 && { backgroundColor: "#bababa" }
            ]}
          >
            <Text style={styles.textTopicBox}>
              {I18n.t("mainpile.8_3.confirmfoam")}
            </Text>
          </View>
          <View style={styles.imageDetail}>
            <View
              style={[
                {
                  width: "40%",
                  flexDirection: "row",
                  alignItems: "center",
                  marginLeft: "10%"
                }
              ]}
            >
              {this.state.checkfoam ? (
                <Icon
                  name="check"
                  reverse
                  color="#6dcc64"
                  size={15}
                  onPress={() => this.selectFoam()}
                  disabled={
                    this.state.Edit_Flag == 0 || this.props.category == 4||(( this.props.category == 3 || this.props.category == 5) &&this.state.Parentcheck == false)
                  }
                />
              ) : (
                <Icon
                  name="check"
                  reverse
                  color="grey"
                  size={15}
                  onPress={() => this.selectFoam()}
                  disabled={
                    this.state.Edit_Flag == 0 || this.props.category == 4||(( this.props.category == 3 || this.props.category == 5) &&this.state.Parentcheck == false)
                  }
                />
              )}
              <Text>{I18n.t("mainpile.8_3.putfoam")}</Text>
            </View>
            <View style={{ width: "50%", alignItems: "center" }}>
              <TouchableOpacity
                style={[
                  styles.image,
                  this.state.foamimage.length > 0
                    ? { backgroundColor: "#6dcc64" }
                    : {}
                ]}
                onPress={() =>
                  this.onCameraRoll("foamimage", this.state.foamimage, "edit")
                }
                disabled={this.props.category == 4}
              >
                <Icon name="image" type="entypo" color="#FFF" />
                <Text style={styles.imageText}>
                  {I18n.t("mainpile.steeldetail.view")}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>}

      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    pileAll: state.mainpile.pileAll || null
  }
}

export default connect(mapStateToProps, actions,null,{ withRef: true })(
  Pile8_3
)
