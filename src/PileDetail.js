import React, { Component } from "react"
import { StyleSheet, View, Text, TouchableOpacity, Linking, Image, Dimensions, BackHandler } from "react-native"
import { Container, Content } from "native-base"
import { Button, Icon } from "react-native-elements"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import { pileItem } from "./Actions"
import DeviceInfo from 'react-native-device-info'
import I18n from '../assets/languages/i18n'
import { DEFAULT_COLOR_1, DEFAULT_COLOR_4, GRAY } from "./Constants/Color"

class PileDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      pile: this.props.item
    }
  }

  componentDidMount() {
    BackHandler.addEventListener("backPress", () => this.onBackPress())
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("backPress")
  }

  onBackPress() {
    console.log("piledetail", Actions.currentScene)
    if (Actions.currentScene == "piledetail") {
      Actions.pop()
      return true
    }
    return false
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.item != this.state.pile) {
      console.log("piledetail", nextProps.item )
      this.setState({ pile: nextProps.item })
      if(this.props.category==3){
        Actions.refresh({ navigationBarStyle: { backgroundColor: DEFAULT_COLOR_4 } })
      }else if (nextProps.item.shape == 2) {
        Actions.refresh({ navigationBarStyle: { backgroundColor: DEFAULT_COLOR_1 } })
      }
    }
  }

  render() {
    // const pile = this.props.data.PileDetail
    const pile = this.state.pile
    const size = pile.size || ""
    const piletip = pile.piletip_old || 0
    const cutoff = pile.cutoff_old || 0
    const ge = pile.ge || 0

    return (
      <Container style={{backgroundColor:GRAY}}>
        <Content>
          <View style={{ marginTop: 5 }}>
            <TopicText topic={I18n.t('projectlist.projectinfo')} text={pile.job_fullname} />
          </View>
          <View style={{ marginTop: 5 }}>
            <TopicText topic={I18n.t('piledetail.pileNo')} text={pile.pile_no} />
          </View>
          <View style={{ marginTop: 5 }}>
            <Topic topic={I18n.t('piledetail.pileinfo')} />
            <SubTopic left={I18n.t('piledetail.worktype')} right={pile.element_type} />
            <SubTopic left={I18n.t('piledetail.type')} right={pile.type} />
            <SubTopic left={I18n.t('piledetail.diameter')} right={size} />
            <SubTopic left={I18n.t('piledetail.piletip')} right={piletip.toFixed(3)} />
            <SubTopic left={I18n.t('piledetail.cutoff')} right={cutoff.toFixed(3)} />
            <SubTopic left={I18n.t('piledetail.ge')} right={ge.toFixed(3)} />
            {pile.seqname!==''&& <SubTopic left={I18n.t('piledetail.sequence')} right={pile.seqname} />}
            {/*{pile.shape == 2 && <SubTopic left="TGW (ม.)" right={pile.tgw || "-"} />}*/}
            {/* <SubTopic left='TGW(ม.)' right={pile.tgw}/> */}
          </View>
          <View style={{ marginTop: 5 }}>
            <Topic topic={I18n.t('piledetail.productioninfo')} />
            <SubTopic left={I18n.t('piledetail.startdate')} right={pile.start_date} />
            <SubTopic left={I18n.t('piledetail.enddate')} right={pile.end_date} />
            <SubTopic left={I18n.t('piledetail.period')} right={pile.total_date} />
          </View>
        </Content>
      </Container>
    )
  }
}

class People extends Component {
  render() {
    return (
      <View style={[styles.sub, { height: 60 }]}>
        <View style={styles.subtopic}>
          <Text style={styles.textSubTopic}>{this.props.left}</Text>
        </View>
        <View style={{ width: "45%" }}>
          <Text style={styles.textSubTopic}>{this.props.right}</Text>
          <Text style={styles.textSubTopic}>{this.props.mobile}</Text>
        </View>
        <View>
          <Icon
            name="phone"
            type="font-awesome"
            color="#57c3e8"
            onPress={() => Linking.openURL("tel:" + this.props.mobile)}
          />
        </View>
      </View>
    )
  }
}

class TopicText extends Component {
  render() {
    return (
      <View style={styles.topicText}>
        <Text style={styles.textTopic}>{this.props.topic}</Text>
        <Text style={[styles.textSubTopic, { marginTop: 10, marginLeft: 12 }]}>{this.props.text}</Text>
      </View>
    )
  }
}

class Topic extends Component {
  render() {
    return (
      <View style={styles.topic}>
        <Text style={styles.textTopic}>{this.props.topic}</Text>
      </View>
    )
  }
}

class SubTopic extends Component {
  render() {
    return (
      <View style={[styles.sub, this.props.main ? { backgroundColor: "#57c3e8" } : {}]}>
        <View style={[styles.subtopic,{width:"50%"}]}>
          <Text style={styles.textSubTopic}>{this.props.left}</Text>
        </View>
        <View style={{ width: "50%" }}>
          <Text style={styles.textSubTopic}>{this.props.right}</Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  topicText: {
    backgroundColor: "#FFF",
    height: DeviceInfo.isTablet() ? 100 : 80,
    justifyContent: "center"
  },
  topic: {
    backgroundColor: "#FFF",
    height: DeviceInfo.isTablet() ? 60 : 40,
    justifyContent: "center"
  },
  textTopic: {
    marginLeft: 12,
    color: "#007CC2",
    fontSize: DeviceInfo.isTablet() ? 30 : 20,
    fontFamily: "THSarabunNew Bold",
    fontWeight: "bold"
  },
  sub: {
    backgroundColor: "#FFF",
    marginTop: 2,
    flexDirection: "row",
    height: "auto",
    alignItems: "center",
    paddingTop: 2,
    paddingBottom: 2
  },
  subtopic: {
    width: "40%"
  },
  textSubTopic: {
    marginLeft: 12,
    fontFamily: "THSarabunNewBold",
    fontSize: DeviceInfo.isTablet() ? 20 : 15,
  }
})

const mapStateToProps = state => {
  return {
    error: state.pile.error,
    item: state.pile.item
  }
}

export default connect(mapStateToProps, { pileItem })(PileDetail)
