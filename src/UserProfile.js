import React, { Component } from "react"
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Dimensions,
  Modal,
  TextInput,
  Alert
} from "react-native"
import { connect } from "react-redux"
import { Container, Content } from "native-base"
import I18n from "../assets/languages/i18n"
import ModalSelector from "react-native-modal-selector"
import { Icon } from "react-native-elements"
import { SUB_COLOR, GRAY } from "./Constants/Color"
import QRCode from "react-native-qrcode-svg"
import * as actions from "./Actions"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import moment from "moment"
import { Actions } from "react-native-router-flux"
import AsyncStorage from '@react-native-community/async-storage';
class UserProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      language:
        I18n.locale == "th"
          ? I18n.t("loginpage.thai")
          : I18n.t("loginpage.english"),
      username: "",
      position: "",
      showqr: false,
      qrplustime: "",
      qr: "",
      picture: "",
      changepassword: false,
      oldpassword: "",
      newpassword: "",
      renewpassword: "",
      date:"",
      touchid:false,
      user:'',
      password:'',
      touch:''
    }
  }

  async componentDidMount() {
    try {
      const value = await AsyncStorage.multiGet([
        "fullname",
        "usergroupname",
        "qrcode",
        "profilepic",
        "nickname",
        "user",
        "password",
        "Touch",
        "position"
      ])
      // let date = moment().format("DDMMYYYYHHmmss")
      // console.log("QRcode data", value[2][1] + date)
      if (value !== null) {
        this.setState({
          username: value[0][1],
          position: value[8][1],
          qr: value[2][1],
          picture: value[3][1],
          nickname: value[4][1],
          user: value[5][1],
          password: value[6][1],
          touch: value[7][1]

        })
      }
    } catch (error) {
      console.log(error)
    }
    if(this.state.touch == 'YES'){
      this.setState({touchid:true})
    }else if(this.state.touch == 'NO'){
      this.setState({touchid:false})
    }
    var data ={
      Language:I18n.locale
    }
    this.props.getUserinfo(data)
    
  }
  changeLanguage(option) {
    switch (option.key) {
      case 1:
        I18n.locale = "th"
        this.setState({ language: I18n.t("loginpage.thai") })
        break
      case 2:
        I18n.locale = "en"
        this.setState({ language: I18n.t("loginpage.english") })
        break
      default:
        console.log("default")
    }
    var data ={
      Language:I18n.locale
    }
    this.props.getUserinfo(data)
    Actions.reset('drawer')
  }

  changePassword() {
    // Validate first !!
    if (this.state.newpassword != this.state.renewpassword) {
      Alert.alert("Error", I18n.t('userprofile.error_step'))
      return
    }
    const data = {
      oldpassword: this.state.oldpassword,
      newpassword: this.state.newpassword,
      language: I18n.currentLocale()
    }
    this.props.authChangePass(data)
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.error != null) {
      Alert.alert("ERROR", nextProps.error)
    }
    if (nextProps.changepass != null) {
      Alert.alert(I18n.t('userprofile.success_title'), I18n.t('userprofile.success_password'), [
        {
          text: "OK"
        }
      ])
      return {
        changepassword: false
      }
    }
  //  console.warn('position',nextProps.userinfo)
    if(nextProps.userinfo != null){
      return{
        nickname:nextProps.userinfo.nickname,
        username:nextProps.userinfo.firstname+' '+nextProps.userinfo.lastname,
        position:nextProps.userinfo.position
      }

    }
    return null
  }
  
  setTouchID(value){
    this.setState({touchid:value})
    if(value){
      AsyncStorage.setItem("Touch",'YES')
    }else{
      AsyncStorage.setItem("Touch",'NO')
    }
  }

  render() {
    // const date = moment().format("DDMMYYYYHHmmss")
    const language = [
      { key: 0, section: true, label: "Language" },
      { key: 1, label: I18n.t("loginpage.thai") },
      { key: 2, label: I18n.t("loginpage.english") }
    ]
    // const qr = await AsyncStorage.getItem("qrcode")
    const image = this.state.picture
      ? { uri: this.state.picture }
      : require("../assets/image/temp.jpg")
    const name = this.state.nickname
      ? this.state.nickname + "-" + this.state.username
      : this.state.username
    return (
      <Container style={{backgroundColor:GRAY}}>
        <Modal
          visible={this.state.showqr}
          onRequestClose={() => this.setState({ showqr: false })}
        >
          <TouchableOpacity onPress={() => this.setState({ showqr: false })}>
            <View style={styles.closeButton}>
              <Icon
                name="close"
                type="evilicons"
                color="black"
                style={{ marginRight: 10 }}
              />
            </View>
          </TouchableOpacity>
          <View
            style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
          >
            <QRCode value={this.state.qrplustime=="" ? this.state.qr:this.state.qrplustime} size={300} />
          </View>
        </Modal>
        <Modal
          visible={this.state.changepassword}
          onRequestClose={() => this.setState({ changepassword: false })}
          animationType="fade"
          presentationStyle="pageSheet"
          style={{ backgroundColor: SUB_COLOR }}
        >
          <KeyboardAwareScrollView keyboardDismissMode="interactive">
            <TouchableOpacity
              onPress={() => this.setState({ changepassword: false })}
            >
              <View style={styles.closeButton}>
                <Icon
                  name="close"
                  type="evilicons"
                  color="black"
                  style={{ marginRight: 10 }}
                />
              </View>
            </TouchableOpacity>
            <View style={styles.header}>
              <Text style={styles.headerText}>{I18n.t('userprofile.changePassword')}</Text>
            </View>
            <View style={{ flex: 1, alignItems: "center" }}>
              <View style={styles.modalBox}>
                <Text style={styles.modalText}>{I18n.t('userprofile.oldPassword')}</Text>
                <View style={styles.passwordBox}>
                  <TextInput
                    secureTextEntry
                    value={this.state.oldpassword}
                    onChangeText={oldpassword => this.setState({ oldpassword })}
                  />
                </View>
              </View>
              <View style={styles.modalBox}>
                <Text style={styles.modalText}>{I18n.t('userprofile.newPassword')}</Text>
                <View style={styles.passwordBox}>
                  <TextInput
                    secureTextEntry
                    value={this.state.newpassword}
                    onChangeText={newpassword => this.setState({ newpassword })}
                  />
                </View>
              </View>
              <View style={styles.modalBox}>
                <Text style={styles.modalText}>{I18n.t('userprofile.newPassword')}</Text>
                <View style={styles.passwordBox}>
                  <TextInput
                    secureTextEntry
                    value={this.state.renewpassword}
                    onChangeText={renewpassword =>
                      this.setState({ renewpassword })
                    }
                  />
                </View>
              </View>
              <View style={[styles.modalBox, { marginTop: 30 }]}>
                <TouchableOpacity
                  style={styles.submitButtom}
                  onPress={() => this.changePassword()}
                >
                  <Text style={{ color: "#FFF", fontSize: 15 }}>
                  {I18n.t('userprofile.changePassword')}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </Modal>
        <Content>
          <View style={styles.profileView}>
            <Image
              style={{ width: "100%", height: "100%" }}
              source={image}
              resizeMode="contain"
            />
          </View>
          <View style={styles.textView}>
            <Text style={styles.name}>{name}</Text>
            <Text style={styles.position}>{this.state.position}</Text>
          </View>
          <View
            style={{
              alignContent: "center",
              alignItems: "center",
              marginTop: 30
            }}
          >
            <TouchableOpacity
              style={styles.qrcode}
              onPress={() =>
                this.setState(
                  { showqr: true, date: moment().format("DDMMYYYYHHmmss") },
                  () => {
                    this.setState(
                      { qrplustime: this.state.qr +"-"+ this.state.date },
                      () => {
                        console.log(this.state.qrplustime)
                      }
                    )
                  }
                )
              }
            >
              <Icon name="qrcode" type="font-awesome" color="#FFF" size={70} />
              <Text style={{ color: "#FFF" }}>My QR code</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.button}>
            <ModalSelector
              data={language}
              initValue={this.state.language}
              onChange={option => this.changeLanguage(option)}
              cancelText="Cancel"
            >
              <TouchableOpacity style={styles.button2} underlayColor="white">
                <View style={styles.buttonTextStyle}>
                  <Text style={styles.textButton}>{this.state.language}</Text>
                  <View
                    style={{ justifyContent: "flex-end", flexDirection: "row" }}
                  >
                    <Icon
                      name="arrow-drop-down"
                      type="MaterialIcons"
                      color="#6bacd5"
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </ModalSelector>
          </View>
          <View style={styles.button}>
            <TouchableOpacity
              style={styles.button2}
              underlayColor="white"
              onPress={() => this.setState({ changepassword: true })}
            >
              <View style={styles.buttonTextStyle}>
                <Text style={styles.textButton}>{I18n.t('userprofile.changePassword')}</Text>
              </View>
            </TouchableOpacity>
          </View>
          {/*<View style={[styles.button,{flexDirection:'row',height:40}]}>
          <View style={{flex:4,justifyContent:'center'}}>
            <Text style={{color:'#000',fontSize:15}}>ตั้งค่า Touch ID</Text>
          </View>
            <TouchableOpacity style={this.state.touchid?styles.buttontouchy:styles.buttontouchn} onPress={()=> this.setTouchID(true)}>
            <Text style={this.state.touchid?styles.texttouchy:styles.texttouchn}>YES</Text>
            </TouchableOpacity>
            <TouchableOpacity style={!this.state.touchid?styles.buttontouchy:styles.buttontouchn} onPress={()=> this.setTouchID(false)}>
            <Text style={!this.state.touchid?styles.texttouchy:styles.texttouchn}>NO</Text>
            </TouchableOpacity>
            </View>*/}
        </Content>
      </Container>
    )
  }
}

const width = Dimensions.get("window").width

const styles = StyleSheet.create({
  // buttontouchy:{
  //   flex:1,backgroundColor:'#007CC2',justifyContent:'center',borderRadius:20
  // },
  // buttontouchn:{
  //   flex:1,justifyContent:'center',borderRadius:20
  // },
  // texttouchy:{
  //   textAlign:'center',color:'#fff',fontSize:15
  // },
  // texttouchn:{
  //   textAlign:'center',color:'#000',fontSize:15
  // },
  textView: {
    marginTop: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  name: {
    color: SUB_COLOR,
    fontSize: 20
  },
  position: {
    // color: 'black',
    fontSize: 15,
    marginTop: 5
  },
  profileView: {
    width: width,
    height: width / 2.5,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20
    // borderWidth: 1
  },
  qrcode: {
    backgroundColor: SUB_COLOR,
    height: 100,
    width: 100,
    justifyContent: "center",
    alignItems: "center"
  },
  button: {
    marginTop: 20,
    paddingRight: 40,
    paddingLeft: 40
  },
  textButton: {
    color: "#007CC2",
    fontSize: 20
  },
  button2: {
    padding: 5,
    backgroundColor: "#FFF",
    borderRadius: 5
  },
  buttonTextStyle: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  closeButton: {
    borderColor: "black",
    padding: 8,
    borderRadius: 3,
    margin: 10,
    alignSelf: "flex-end"
  },
  modalBox: {
    width: "100%",
    alignItems: "center",
    marginTop: 10
  },
  modalText: {
    fontSize: 25,
    color: "#000"
  },
  passwordBox: {
    height: 50,
    width: "80%",
    borderWidth: 1
  },
  submitButtom: {
    height: 50,
    width: "80%",
    borderRadius: 15,
    backgroundColor: SUB_COLOR,
    alignItems: "center",
    justifyContent: "center"
  },
  header: {
    marginTop: 10,
    marginBottom: 20,
    alignItems: "center"
  },
  headerText: {
    fontSize: 40,
    color: "#000"
  }
})

const mapStateToProps = state => {
  return {
    changepass: state.auth.changepass,
    error: state.auth.error,
    userinfo: state.auth.userinfo
  }
}

export default connect(mapStateToProps, actions)(UserProfile)
