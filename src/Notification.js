import React, { Component } from "react"
import { View, Text, StyleSheet, Image, FlatList, TouchableOpacity, Alert,RefreshControl } from "react-native"
import { connect } from "react-redux"
import { Container, Content } from "native-base"
import * as actions from "./Actions"
import { MAIN_COLOR, SUB_COLOR } from "./Constants/Color"
import { Actions } from "react-native-router-flux"
import I18n from '../assets/languages/i18n'
class Notification extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      updated: false,
      noti: [],
      notiEg: [
        {
          Id: 100,
          JobId: 100,
          JobName: "PYLON APPLICATiON PROJEct",
          PileId: 200,
          PileNo: 200,
          SenderId: 1150,
          SenderFirstName: "Arnold",
          SenderLastName: "Schwarzenegger",
          SenderNickName: "TERMINATOR",
          Step: 2,
          Flag: false
        },
        {
          Id: 21313,
          JobId: 123123,
          JobName: "WOOO HOO",
          PileId: 444,
          PileNo: 77823,
          SenderId: 1150,
          SenderFirstName: "Sony",
          SenderLastName: "Entertainment",
          SenderNickName: "So",
          Step: 3,
          Flag: true
        }
      ],
      random:''
    }
  }

  componentWillMount() {
    this.loadNoti()
    if(this.props.fromnoti) {
      this.props.clearNoti()
      Actions.notidetail({ alertid: this.props.alertid })
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.error == null) {
      this.setState({ noti: nextProps.notilist, loading: false })
    }
    if(nextProps.error != null) {
      Alert.alert("ERROR", nextProps.error, [{ text: "OK" }])
    }
    // if(nextProps.value && nextProps.value.force) {
    //   this.refresh()
    //   console.warn('loadNoti',nextProps.value,nextProps.value.force)
    // }
    if(nextProps.random){
      if(nextProps.random != this.state.random){
        this.refresh()
        this.setState({random:nextProps.random})
      }
      // console.warn('random',nextProps.random)
      // this.refresh()
    }
  }

  loadNoti() {
    this.props.notiList()
  }

  updateState(value) {
    // console.warn("INSIDE", value)
    if(value.force) {
      this.forceUpdate()
    }
  }

  renderNotiTable() {
    const list = this.state.noti
    return (
      <View style={{ margin: 10, borderWidth: 1 }}>
        <Text>{I18n.t('notification.approver')} : </Text>
        <Text>{I18n.t('notification.projectName')}</Text>
        <Text>{I18n.t('notification.pileNo')}</Text>
        <Text>{I18n.t('notification.process')}</Text>
      </View>
    )
  }

  selectNoti(id,status,processidvalue) {
    // this.setState({ updated: false })
    Actions.notidetail({ alertid: id ,status:status, processid:processidvalue})
  }

  refresh = () => {
    this.setState({ loading: true }, () => this.loadNoti())
  }

  renderNoti = (row, section, id) => {
    var accept = ""
    var color = {}
    var disable = false
    var check = false
    if(row.item.Flag == null) {
      accept = row.item.Status!=1&&row.item.Status!=5&&row.item.Status!=15?I18n.t('notification.pending_accept'):I18n.t('notification.pending')
      if(row.item.Status!=1&&row.item.Status!=5&&row.item.Status!=15){
        check = true
      }
    } else if(row.item.Flag) {
      accept = row.item.Status!=1&&row.item.Status!=5&&row.item.Status!=15?I18n.t('notification.accept'):I18n.t('notification.approve')
      if(row.item.Status!=1&&row.item.Status!=5&&row.item.Status!=15){
        check = true
      }
      color = { color: "green" }
      disable = true
    } else if(!row.item.Flag) {
      accept = I18n.t('notification.notapprove')
      color = { color: "red" }
      disable = true
    }
    return (
      <TouchableOpacity onPress={() => this.selectNoti(row.item.Id,row.item.Status,row.item.ProcessId)} style={[styles.rowView, row.item.Flag == null && { backgroundColor: '#E6E6E6'} ]} disabled={disable}>
        <Row
          left={row.item.Status==1||row.item.Status==5||row.item.Status==15?I18n.t('notification.approver'):I18n.t('notification.acceper')}
          right={
            (row.item.SenderNickName && row.item.SenderNickName + "-") +
            row.item.SenderFirstName +
            " " +
            row.item.SenderLastName
          }
        />
        <Row left={I18n.t('notification.projectName')} right={row.item.JobName} />
        <Row left={I18n.t('notification.pileNo')} right={row.item.PileNo} />
        <Row left={I18n.t('notification.process')} right={row.item.ProcessName} />
        {
          row.item.Status == 2 || row.item.Status == 5?<Text style={{textAlign:'center',fontWeight:'bold',color:'red',fontSize:16}}>{I18n.t('notification.editNE')}</Text>:<View/>
        }
        {
          row.item.Status == 3?<Text style={{textAlign:'center',fontWeight:'bold',color:'red',fontSize:16}}>{I18n.t('notification.AcceptSlumpNotDrop')}</Text>:<View/>
        }
        {
          row.item.Status == 4?<Text style={{textAlign:'center',fontWeight:'bold',color:'red',fontSize:16}}>{I18n.t('notification.AcceptSlumpDrop')}</Text>:<View/>
        }
        {
          row.item.Status == 6?<Text style={{textAlign:'center',fontWeight:'bold',color:'red',fontSize:16}}>{I18n.t('notification.process5')}</Text>:<View/>
        }
        {
          row.item.Status == 7?<Text style={{textAlign:'center',fontWeight:'bold',color:'red',fontSize:16}}>{I18n.t('notification.notistatus7')}</Text>:<View/>
        }
        {
          row.item.Status == 8?<Text style={{textAlign:'center',fontWeight:'bold',color:'red',fontSize:16}}>{I18n.t('notification.notistatus8')}</Text>:<View/>
        }
        {
          row.item.Status == 9?<Text style={{textAlign:'center',fontWeight:'bold',color:'red',fontSize:16}}>{I18n.t('notification.notistatus9')}</Text>:<View/>
        }
        {
          row.item.Status == 10?<Text style={{textAlign:'center',fontWeight:'bold',color:'red',fontSize:16}}>{I18n.t('notification.notistatus10')}</Text>:<View/>
        }
        {
          row.item.Status == 11?<Text style={{textAlign:'center',fontWeight:'bold',color:'red',fontSize:16}}>{I18n.t('notification.notistatus11')}</Text>:<View/>
        }
        {
          row.item.Status == 12?<Text style={{textAlign:'center',fontWeight:'bold',color:'red',fontSize:16}}>{I18n.t('notification.notistatus12')}</Text>:<View/>
        }
        {
          row.item.Status == 13?<Text style={{textAlign:'center',fontWeight:'bold',color:'red',fontSize:16}}>{I18n.t('notification.notistatus13')}</Text>:<View/>
        }
        {
          row.item.Status == 14?<Text style={{textAlign:'center',fontWeight:'bold',color:'red',fontSize:16}}>{I18n.t('notification.notistatus14')}</Text>:<View/>
        }
        {
          row.item.Status == 15?<Text style={{textAlign:'center',fontWeight:'bold',color:'red',fontSize:16}}>{I18n.t('notification.notistatus15')}</Text>:<View/>
        }
        <View style={styles.acceptView}>
          <View style={{ width:I18n.locale == 'en'?'41%': "65%"}}>
            <Text style={styles.date}>{row.item.CreatedDate}</Text>
          </View>
          <View style={{ width:I18n.locale == 'en'?'59%': "35%"}}>
            <Text style={[styles.accept, color,{fontSize:I18n.locale == 'en'?16:20,textAlign:'right',marginRight:10}]}>{accept}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    const alertid = 93
    return (
      <Container>
          <View>
            <View style={styles.topicView}>
              <Text style={styles.topic}>{I18n.t('notification.notification')}</Text>
            </View>
            <FlatList data={this.state.noti} renderItem={this.renderNoti} keyExtractor={(item, index) => index.toString()} 
            // refreshing={this.state.loading}
              // onRefresh={this.refresh} 
              refreshControl={
                <RefreshControl
                    refreshing={this.state.loading}
                    onRefresh={this.refresh}
                />}
                style={{height:'90%'}}
              />
          </View>
          {__DEV__ && (
            <View style={styles.debugview}>
              <TouchableOpacity onPress={() => Actions.notidetail({ alertid })} style={styles.debug}>
                <Text>DEBUG AlertId {alertid}</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() =>  Actions.mainpile({title: 'DEBUG', pileid: 20305, jobid: 38, toProcess: 3, fromnoti: true,viewType:0 , finish:false})} style={styles.debug}>
                <Text>DEBUG WARP TO pile-20366 job-48</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => Actions.scanQrCode()} style={styles.debug}>
                <Text>SCAN QRCODE</Text>
              </TouchableOpacity>
            </View>
          )}
        
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    notilist: state.noti.notilist,
    error: state.noti.error,
    random: state.noti.noticheck
  }
}

const styles = StyleSheet.create({
  rowView: {
    margin: 10,
    borderWidth: 1,
    borderColor: SUB_COLOR,
    borderRadius: 10,
    backgroundColor: "#FFF"
  },
  row: {
    height: "auto",
    minHeight: 30,
    flexDirection: "row",
    // borderTopWidth: 0.5,
    borderColor: SUB_COLOR
  },
  leftBox: {
    width: "40%",
    justifyContent: "center"
  },
  rightBox: {
    width: "60%",
    justifyContent: "center"
  },
  textBox: {
    marginLeft: 10,
    textAlignVertical: "center",
    color: "black",
    fontSize: 15
  },
  topicView: {
    marginTop: 10,
    // marginLeft: 10
    alignItems: "center"
  },
  topic: {
    fontSize: 30,
    color: MAIN_COLOR
  },
  acceptView: {
    // alignItems: "flex-end",
    marginTop: 5,
    marginBottom: 10,
    flexDirection: "row"
  },
  accept: {
    color: MAIN_COLOR,
    fontSize: 20,
    // marginLeft: 10,
    
  },
  date: {
    fontSize: 15,
    color: "black",
    marginLeft: 10
  },
  debug: {
    width: 300,
    height: 40,
    alignItems: 'center',
    borderWidth: 1,
    justifyContent: 'center',
    borderRadius: 10,
    backgroundColor: 'white'
  },
  debugview: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20
  }
})

export default connect(mapStateToProps, actions)(Notification)

class Row extends Component {
  render() {
    return (
      <View style={styles.row}>
        <View style={styles.leftBox}>
          <Text style={styles.textBox}>{this.props.left}</Text>
        </View>
        <View style={styles.rightBox}>
          <Text style={styles.textBox}>{this.props.right}</Text>
        </View>
      </View>
    )
  }
}
