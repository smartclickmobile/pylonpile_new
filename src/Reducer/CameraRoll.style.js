import {StyleSheet, Platform} from 'react-native';

export default StyleSheet.create({
  //---------- start navigation style ----------//
  container: {
    flex: 1
  },
  navigation: {
    paddingTop: Platform.OS === 'ios'
      ? 25
      : 10,
    backgroundColor: '#007cc2',
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1
  },
  buttonLeft: {
    position: 'absolute',
    top: 4,
    left: 30
  },
  buttonLeftResize: {
    width: 25,
    height: 25
  },
  buttonOption: {
    marginTop: 14,
    marginLeft: -5
  },
  buttonRight: {
    position: 'absolute',
    top: 0,
    right: 30
  },
  buttonText: {
    fontFamily: 'Prompt-Medium',
    fontSize: 16,
    color: '#c37f6f'
  },
  navigationTitle: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  navigationTitleRow: {
    flexDirection: 'row'
  },
  navigationTitleText: {
    fontFamily: 'Prompt-Bold',
    color: '#fff',
    fontSize: 20,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10
  },
  //---------- end navigation style ----------//

  //---------- start camera roll style ----------//
  cameraRollContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingTop: 100
  },
  cameraRollRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 100
  },
  cameraRollList: {
    width: 122,
    height: 224,
    backgroundColor: '#4d4d4d',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 2,
    borderRightWidth: 2,
    borderBottomWidth: 2,
    borderColor: '#000'
  },
  cameraRollImg: {
    width: 120,
    height: 220
  },
  cameraRollImgBorder: {
    borderTopWidth: 2,
    borderRightWidth: 2,
    borderBottomWidth: 2,
    borderColor: '#000'
  },
  cameraButtonGroup: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: '100%',
    paddingBottom: 50
  },
  cameraRollButton: {
    width: 28,
    height: 28,
    borderRadius: 8,
    borderWidth: 2,
    borderColor: '#fff'
  },
  previewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    backgroundColor: '#fff'
  },
  preview: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 80,
    paddingLeft: 10,
    paddingRight: 10
  },
  previewImg: {
    width: 350,
    maxWidth: '100%',
    height: 350
  },
  previewCropImg: {
    width: 350,
    height: 350,
    borderRadius: 350,
    position: 'absolute'
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40
  },
  //---------- end camera roll style ----------//
  cameraRollContainer: {
    width: 600,
    maxWidth: '100%'
  },
  cameraButtonPosition: {
    // flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center'
  }
});
