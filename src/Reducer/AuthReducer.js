import { 
  AUTH_LOGIN_SUCCESS, 
  AUTH_LOGIN_ERROR, 
  AUTH_LOGOUT_SUCCESS, 
  AUTH_LOGOUT_ERROR, 
  AUTH_LOGIN_DUP, 
  AUTH_RESET, 
  AUTH_CHANGEPASS_SUCCESS, 
  AUTH_CHANGEPASS_FAIL, 
  AUTH_LOADING,
  USER_INFO_SUCCESS,
  USER_INFO_ERROR
 } from "../Actions/types"

const INITIAL_STATE = {
  error: null,
  login: false,
  logout: null,
  data: null,
  duplicate: false,
  changepass: null,
  dashboard:null,
  userinfo:null,
  dashboard1:null
}

export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case AUTH_LOGIN_SUCCESS:
      return {
        ...state,
        login: true,
        data: action.payload,
        error: null,
        duplicate: false,
        logout: null,
        dashboard: action.dashboard
      }
    case AUTH_LOGIN_ERROR:
      return {
        ...state,
        error: action.payload,
        login: false,
        duplicate: false,
        logout: null
      }
    case AUTH_LOGIN_DUP:
      return {
        ...state,
        login:false,
        error: action.payload,
        duplicate: true,
        logout: null
      }
    case AUTH_LOGOUT_SUCCESS:
      return {
        ...state,
        logout: true,
        login: false,
        error: null,
        duplicate: false
      }
    case AUTH_LOGOUT_ERROR:
      return {
        ...state,
        error: action.payload,
        logout: false,
        duplicate: false
      }
    case AUTH_RESET:
      return {
        error: null,
        login: false,
        logout: null,
        data: null,
        duplicate: false
      }
    case AUTH_CHANGEPASS_SUCCESS:
      return {
        ...state,
        error: null,
        changepass: action.payload
      }
    case AUTH_CHANGEPASS_FAIL:
      return {
        ...state,
        error: action.payload,
        changepass: null
      }
    case AUTH_LOADING:
      return {
        ...state,
        error: null,
        changepass: null
      }
      case USER_INFO_SUCCESS:
      return {
        ...state,
        error: null,
        userinfo: action.payload,
        dashboard1: action.dashboard
      }
    case USER_INFO_ERROR:
      return {
        ...state,
        error: action.payload,
        userinfo: null
      }
    default:
      return state
  }
}
