import { JOBLIST_SUCCESS, JOBLIST_ERROR,JOBLIST_CLEAR } from "../Actions/types"

const INITIAL_STATE = {
  error: null,
  data: null,
  success: false,
  haveperm: [],
  noperm: []
}

export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case JOBLIST_SUCCESS:
      return {
        ...state,
        success: true,
        data: action.payload,
        error: null,
        haveperm: action.haveperm,
        noperm: action.noperm
      }
    case JOBLIST_ERROR:
      return {
        ...state,
        success: false,
        error: action.payload,
        data: null,
        haveperm: [],
        noperm: []
      }
    case JOBLIST_CLEAR:
      return INITIAL_STATE
    default:
      return state
  }
}
