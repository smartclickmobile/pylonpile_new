import { NOTE_SAVE, NOTE_ERROR, NOTE } from "../Actions/types"

const INITIAL_STATE = {
  note: "",
  error: null,
  notesave: 0
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case NOTE:
      return {
        ...state,
        note: action.payload,
        error: null,
        notesave: 0
      }
    case NOTE_SAVE:
      return {
        ...state,
        note: "",
        error: null,
        notesave: state.notesave + 1
      }
    default:
      return { ...state }
  }
}
