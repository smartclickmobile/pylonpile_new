import { combineReducers } from "redux"
import AuthReducer from "./AuthReducer"
import JobListReducer from "./JobListReducer"
import JobDetailReducer from "./JobDetailReducer"
import MainPileReducer from "./MainPileReducer"
import PileReducer from "./PileReducer"
import ProviderReducer from "./ProviderReducer"
import SteelCageReducer from "./SteelCageReducer"
import SteelSecDetailReducer from "./SteelSecDetailReducer"
import NotiReducer from "./NotiReducer"
import DuplicateReducer from "./DuplicateReducer"
import HistoryReducer from "./HistoryReducer"
import ConcreteReducer from "./ConcreteReducer"
import NoteReducer from "./NoteReducer"
import TremieReducer from "./TremieReducer"
import DrillingFluidsReducer from "./DrillingFluidsReducer"
import DrillingListReducer from './DrillingListReducer'
import LocationReducer from './LocationReducer'

export default combineReducers({
  auth: AuthReducer,
  joblist: JobListReducer,
  jobdetail: JobDetailReducer,
  mainpile: MainPileReducer,
  pile: PileReducer,
  provider: ProviderReducer,
  steelCage: SteelCageReducer,
  steelSecDetail: SteelSecDetailReducer,
  noti: NotiReducer,
  duplicate: DuplicateReducer,
  history: HistoryReducer,
  concrete: ConcreteReducer,
  note: NoteReducer,
  tremie: TremieReducer,
  drillingFluids: DrillingFluidsReducer,
  drillinglist: DrillingListReducer,
  location:LocationReducer
})
