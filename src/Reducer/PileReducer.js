import {
  PILE_ERROR,
  PILE_ITEM,
  PILE_ITEMS,
  PILE_VALUE_INFO,
  PILE_VALUE_INFO_RESET,
  PILE_MASTER_INFO,
  PILE_PILE01_SUCCESS,
  PILE_PILE01_SUCCESS_1,
  PILE_PILE01_SUCCESS_3,
  PILE_PILE02_SUCCESS,
  PILE_PILE02_SUCCESS_1,
  PILE_PILE03_SUCCESS,
  PILE_PILE03_SUCCESS_1,
  PILE_PILE03_SUCCESS_2,
  PILE_PILE03_SUCCESS_3,
  PILE_PILE03_SUCCESS_4,
  PILE_PILE04_SUCCESS,
  PILE_PILE05_SUCCESS,
  PILE_PILE05_SUCCESS_1,
  PILE_PILE05_SUCCESS_4,
  PILE_PILE05_CHECK_SUCCESS,
  PILE_PILE06_SUCCESS,
  PILE_PILE07_SUCCESS,
  PILE_PILE07_SUPER_SUCCESS,
  PILE_PILE07_SUCCESS_CASING,
  PILE_PILE07_CHECK_SUCCESS,
  PILE_PILE06_CHECK_SUCCESS,
  PILE_PILE08_SUCCESS,
  PILE_PILE09_SUCCESS,
  PILE_PILE11_SUCCESS,
  PILE_PILE10_SUCCESS,
  PILE_PILE05_ADDROW_SUCCESS,
  PILE_PILE04_DELETE_SUCCESS,
  PILE_PILE05_DELETE_SUCCESS,
  PILE_PILE03_APPROVE_SUCCESS,
  PILE_PILE03_APPROVE_FAIL,
  PILE_PILE03_CANCEL_APPROVE_SUCCESS,
  PILE_PILE03_CANCEL_APPROVE_FAIL,
  PILE_SLUMP_CANCEL_APPROVE_SUCCESS,
  PILE_SLUMP_CANCEL_APPROVE_FAIL,
  PILE_ERROR_CLEAR,
  PILE_COUNT,
  PILE_CLEAR,
  PILE_PILE09_ERROR,
  PILE_LOADING,
  PILE_PILE04_CLEAR_ID,
  PILE_ITEMS_CLEAR,
  Importtoerp,
  Unblockerp,
  loading
} from "../Actions/types"

const INITIAL_STATE = {
  status0: 0,
  status1: 0,
  status2: 0,
  totalpile:0,
  error: null,
  error_approve: null,
  item: null,
  items: [],
  valueInfo: null,
  masterInfo: null,
  approve: null,
  cancel_approve: null,
  count: 0,
  status: 0,
  pile9success: null,
  position:null,
  recent: null,
  pile4Id:0,
  pile6_1success:null,
  pile6_2success:null,
  jobid:null,
  searchpile:null,
  input:null,
  clearerp:null,
  random1:null,
  random2:null,
  loading1:false,
  pile7:false,
  random7_1:null,
  save7page:null,
  pile7_pass8:null,
  lockstep7_8:null,
  randomlockstep7_8:null,
  randomlockstep7_casing:null,
  Unblockerp_success: null,
  Unblockerp_random: null,
  data_Unblockerp:null,
  checkto7_super:null,
  checkto7_super_random:null,
  lockstepcheckto7_super:null,
  checkto7_super_process:null,
  checkto6_super:null,
  lockstepcheckto6_super:null,
  checkto6_super_random:null,

  checkto5_super:null,
  lockstepcheckto5_super:null,
  checkto5_super_random:null,


  process1_success_1:null,
  process1_success_1_random:null,
  process1_success_2:null,
  process1_success_2_random:null,
  process1_success_3:null,
  process1_success_3_random:null,
  process2_success_1:null,
  process2_success_1_random:null,
  process2_success_2:null,
  process2_success_2_random:null,
  process3_success_1:null,
  process3_success_1_random:null,
  process3_success_2:null,
  process3_success_2_random:null,
  process3_success_3:null,
  process3_success_3_random:null,
  process3_success_4:null,
  process3_success_4_random:null,
  process5_success_1:null,
  process5_success_1_random:null,
  process5_success_4:null,
  process5_success_4_random:null,
  pile8:false,
  random8:null,
  save8page:null,
  pile10:false,
  random10:null,
  save10page:null,
  confirm:null,
  pile11:false,
  random11:null,
  save11page:null,

  process5_deleterow:null
}

export default (state = INITIAL_STATE, action) => {
  console.log(action.type,action.payload)
  switch (action.type) {
    
    case PILE_LOADING:
      return {
        ...state,
        loading: action.loading
      }
      break
    case PILE_ERROR:
      return {
        ...state,
        error: action.payload,
        status: -1,
        loading: false,
        pile7:false,
        pile7_pass8:false,
        lockstep7_8:false,
        randomlockstep7_casing:null,
        pile8:false,
        pile10:false,
        pile11:false,
        checkto7_super_random:action.lockstep==false && action.process==6 ?Math.floor((Math.random() * 1000) + 1):null
      }
    case PILE_ITEM:
      return {
        ...state,
        item: action.payload,
        error: null
      }
    case PILE_ITEMS:
      return {
        ...state,
        items: action.payload,
        error: null,
        status0: action.status0,
        status1: action.status1,
        status2: action.status2,
        loading: false,
        position: action.position,
        recent: action.recent,
        jobid: action.jobid,
        totalpile : action.totalpile,
        searchpile:action.searchpile,
        input:action.input
      }
    case PILE_VALUE_INFO:
      return {
        ...state,
        valueInfo: action.payload,
        error: null,
        count: state.count + 1
      }
    case PILE_VALUE_INFO_RESET:
      return {
        ...state,
        valueInfo: null,
        error: null
      }
    case PILE_MASTER_INFO:
      return {
        ...state,
        masterInfo: action.payload,
        error: null,
        count: state.count + 1
      }
    case PILE_PILE01_SUCCESS:
      return {
        ...state,
        process1_success_2:true,
        process1_success_2_random:Math.floor((Math.random() * 1000) + 1),
        error: null
      }
    case PILE_PILE01_SUCCESS_1:
    // console.warn('PILE_PILE01_SUCCESS_1')
      return {
        ...state,
        process1_success_1:true,
        process1_success_1_random:Math.floor((Math.random() * 1000) + 1),
        error: null
      }
      case PILE_PILE01_SUCCESS_3:
    // console.warn('PILE_PILE01_SUCCESS_1')
      return {
        ...state,
        process1_success_3:true,
        process1_success_3_random:Math.floor((Math.random() * 1000) + 1),
        error: null
      }
    case PILE_PILE02_SUCCESS:
    console.warn('PILE_PILE02_SUCCESS')
      return {
        ...state,
        process2_success_2:true,
        process2_success_2_random:Math.floor((Math.random() * 1000) + 1),
        error: null
      }
    case PILE_PILE02_SUCCESS_1:
      return {
        ...state,
        process2_success_1:true,
        process2_success_1_random:Math.floor((Math.random() * 1000) + 1),
        error: null
      }
    case PILE_PILE03_SUCCESS:
      return {
        ...state,
        error: null
      }
    case PILE_PILE03_SUCCESS_1:
      return {
        ...state,
        process3_success_1:true,
        process3_success_1_random:Math.floor((Math.random() * 1000) + 1),
        error: null
      }
      case PILE_PILE03_SUCCESS_2:
      return {
        ...state,
        process3_success_2:true,
        process3_success_2_random:Math.floor((Math.random() * 1000) + 1),
        error: null
      }
      case PILE_PILE03_SUCCESS_3:
      return {
        ...state,
        process3_success_3:true,
        process3_success_3_random:Math.floor((Math.random() * 1000) + 1),
        error: null
      }
      case PILE_PILE03_SUCCESS_4:
      return {
        ...state,
        process3_success_4:true,
        process3_success_4_random:Math.floor((Math.random() * 1000) + 1),
        error: null
      }
    case PILE_PILE04_SUCCESS:
      return {
        ...state,
        error: null,
        pile4Id: action.id
      }
    case PILE_PILE05_SUCCESS:
      return {
        ...state,
        error: null
      }
      case PILE_PILE05_SUCCESS_1:
      return {
        ...state,
        process5_success_1:true,
        process5_success_1_random:Math.floor((Math.random() * 1000) + 1),
        error: null
      }
      case PILE_PILE05_SUCCESS_4:
      return {
        ...state,
        process5_success_4:true,
        process5_success_4_random:Math.floor((Math.random() * 1000) + 1),
        error: null
      }
      case PILE_PILE05_CHECK_SUCCESS: 
      console.warn('lockstep',action.lockstep)
      return {
        ...state,
        error: null,
       checkto5_super:true,
       lockstepcheckto5_super:action.lockstep?true:false,
       checkto5_super_random:Math.floor((Math.random() * 1000) + 1)
      }
    case PILE_PILE06_SUCCESS:
      return {
        ...state,
        error: null,
        pile6_1success:action.idupdate==1?true:false,
        pile6_2success:action.idupdate==2?true:false,
        random1:action.idupdate==1?Math.floor((Math.random() * 100) + 1):null,
        random2:action.idupdate==2?Math.floor((Math.random() * 100) + 1):null
      }
    case PILE_PILE07_SUCCESS:
      return {
        ...state,
        error: null,
        pile7:true,
        random7_1:Math.floor((Math.random() * 100) + 1),
        save7page:action.payload
      }
    case PILE_PILE07_SUPER_SUCCESS: 
    // console.warn('PILE_PILE07_SUPER_SUCCESS',action.lockstep)
      return {
        ...state,
        error: null,
        pile7_pass8:true,
        lockstep7_8:action.lockstep?true:false,
        randomlockstep7_8:Math.floor((Math.random() * 100) + 1)
      }
      case PILE_PILE07_SUCCESS_CASING:
      return {
        ...state,
        error: null,
        randomlockstep7_casing:Math.floor((Math.random() * 100) + 1)
      }
      case PILE_PILE07_CHECK_SUCCESS: 
      return {
        ...state,
        error: null,
       checkto7_super:true,
       lockstepcheckto7_super:action.lockstep?true:false,
       checkto7_super_random:Math.floor((Math.random() * 1000) + 1),
       checkto7_super_process:action.process
      }
      case PILE_PILE06_CHECK_SUCCESS: 
      return {
        ...state,
        error: null,
       checkto6_super:true,
       lockstepcheckto6_super:action.lockstep?true:false,
       checkto6_super_random:Math.floor((Math.random() * 1000) + 1)
      }
    case PILE_PILE08_SUCCESS:
      return {
        ...state,
        error: null,
        random8:Math.floor((Math.random() * 100) + 1),
        pile8:true,
        save8page:action.payload
      }
    case PILE_PILE09_SUCCESS:
      return {
        ...state,
        pile9success: true,
        error: null
      }
      
    case PILE_PILE10_SUCCESS:
      return {
        ...state,
        error: null,
        random10:Math.floor((Math.random() * 100) + 1),
        pile10:true,
        save10page:action.payload,
        confirm:action.confirm
      }
    case PILE_PILE11_SUCCESS:
      
      return {
        ...state,
        error: null,
        random11:Math.floor((Math.random() * 100) + 1),
        pile11:true,
        save11page:action.payload
      }
     
    case PILE_PILE05_ADDROW_SUCCESS:
      return {
        ...state,
        error: null,
        status: 1
      }
    case PILE_PILE04_DELETE_SUCCESS:
      return {
        ...state,
        error: null
      }
    case PILE_PILE05_DELETE_SUCCESS:
      return {
        ...state,
        error: null,
        process5_deleterow: Math.floor((Math.random() * 100) + 1)
      }
    case PILE_PILE03_APPROVE_SUCCESS:
      return {
        ...state,
        approve: "success",
        error: null
      }
    case PILE_PILE03_APPROVE_FAIL:
      return {
        ...state,
        approve: null,
        error_approve: action.payload
      }

    case PILE_PILE03_CANCEL_APPROVE_SUCCESS:
      return {
        ...state,
        cancel_approve: "success"
      }
    case PILE_PILE03_CANCEL_APPROVE_FAIL:
      return {
        ...state,
        cancel_approve: action.payload
      }
    case PILE_SLUMP_CANCEL_APPROVE_SUCCESS:
      return {
        ...state,
        cancel_approve: "success"
      }
    case PILE_SLUMP_CANCEL_APPROVE_FAIL:
      return {
        ...state,
        cancel_approve: action.payload
      }
    case PILE_ERROR_CLEAR:
      return {
        ...state,
        approve: null,
        error: null,
        error_approve: null,
        status: 0,
        cancel_approve: null,
        confirm:null
      }
    case PILE_COUNT:
      return {
        ...state,
        count: 0
      }
    case PILE_CLEAR:
      return INITIAL_STATE
    case PILE_PILE04_CLEAR_ID:
      return {
        ...state,
        pile4Id: 0
      }
    case PILE_ITEMS_CLEAR:
      return {
        ...state,
        items:null,
        // jobid:null
        searchpile:null,
        clearerp:action.clearerp
      }
    case loading:
      return {
        ...state,
        loading1:action.loading
      }
    case Unblockerp:
      return {
        ...state,
        Unblockerp_success:true,
        Unblockerp_random:Math.floor((Math.random() * 1000) + 1),
        data_Unblockerp:action.data_Unblockerp
      }
    default:
      return state
  }
}
