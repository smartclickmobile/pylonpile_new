import { DUPLICATE_LOGIN } from "../Actions/types"
import { Alert } from "react-native"
import { Actions} from 'react-native-router-flux'
import AsyncStorage from '@react-native-community/async-storage';
const INITIAL_STATE = {
  dup: null
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case DUPLICATE_LOGIN:
    AsyncStorage.removeItem("token")
    // console.warn('DUPLICATE LOGIN DETECTED !!')
    Alert.alert("ERROR", action.payload, [
      {
        text: "OK",
        onPress: () => {
          Actions.login({ type: "reset" })
        }
      }
    ])
      return {
        ...state,
        dup: action.payload
      }
    default:
      return {
        ...state
      }
  }
}

function showAlert(message) {
  Alert.alert(message, "", [
    {
      text: "OK",
      onPress: () => {
        Actions.login({ type: "reset" })
      }
    }
  ])
}
