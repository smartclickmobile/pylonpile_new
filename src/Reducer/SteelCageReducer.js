import {STEEL_CAGE_ERROR, STEEL_CAGE_ERROR_RESET, STEEL_CAGE_ITEMS} from "../Actions/types"

const INITIAL_STATE = {
  error: null,
  items: [],
  steelCageNo: ''
}

export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case STEEL_CAGE_ERROR:
      return {
        ...state,
        error: action.payload
      }
    case STEEL_CAGE_ERROR_RESET:
      return {
        ...state,
        error: null
      }
    case STEEL_CAGE_ITEMS:
      return {
        ...state,
        items: action.payload,
        steelCageNo: action.steelcage_no,
        error: null
      }
    default:
      return state
  }
}
