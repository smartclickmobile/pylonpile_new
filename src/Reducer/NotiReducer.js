import { NOTILIST_SUCCESS, NOTILIST_ERROR, NOTIDETAIL_SUCCESS, NOTIDETAIL_ERROR, NOTICONFIRM_SUCCESS, NOTICONFIRM_ERROR, NOTISET, NOTICLEAR } from "../Actions/types"

const INITIAL_STATE = {
  error: null,
  notilist: [],
  notidetail: [],
  count: 0,
  notidata: null,
  process: 0,
  noticheck:null
}

export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case NOTILIST_SUCCESS:
      return {
        ...state,
        notilist: action.payload,
        error: null,
        count: action.count
      }
      break
    case NOTILIST_ERROR:
      return {
        ...state,
        notilist: null,
        error: action.payload
      }
      break
    case NOTIDETAIL_SUCCESS:
      return {
        ...state,
        notidetail: action.payload,
        error: null,
        process: action.process
      }
      break
    case NOTIDETAIL_ERROR:
      return {
        ...state,
        notidetail: null,
        error: action.payload
      }
      break
    case NOTICONFIRM_SUCCESS:
      return {
        ...state,
        error: null,
        noticheck: Math.floor((Math.random() * 100) + 1)
      }
    case NOTICONFIRM_ERROR:
      return {
        ...state,
        error: action.payload
      }
    case NOTISET:
      return {
        ...state,
        notidata: action.payload
      }
    case NOTICLEAR:
      return {
        ...state,
        notidata: null
      }
    default:
      return state
  }
}