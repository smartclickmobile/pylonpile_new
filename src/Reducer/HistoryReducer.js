import { HISTORYLIST_SUCCESS, HISTORYLIST_ERROR, HISTORYLISTMORE_SUCCESS, HISTORYLISTMORE_END } from "../Actions/types"

const INITIAL_STATE = {
  error: null,
  historylist: [],
  end: false
}

export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case HISTORYLIST_SUCCESS: 
      return {
        ...state,
        error: null,
        historylist: action.payload,
        end: false
      }
    case HISTORYLISTMORE_SUCCESS:
      return {
        ...state,
        error: null,
        historylist: [...state.historylist, ...action.payload],
        end: false
      }
      case HISTORYLISTMORE_END:
      return {
        ...state,
        error: null,
        historylist: [...state.historylist, ...action.payload],
        end: true
      }
    case HISTORYLIST_ERROR:
      return {
        ...state,
        error: action.payload,
        historylist: null
      }
    default:
      return {
        ...state
      }
  }
}
