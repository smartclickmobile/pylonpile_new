import { DrillingFluidslist, DrillingFluidsError, DrillingFluidId_ADDTEMP,DrillingFluidId_EDITTEMP} from "../Actions/types"
const INITIAL_STATE = {
    drillingfluidslist:[]
}
export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
      case DrillingFluidslist:
        return {
          ...state,
          drillingfluidslist: action.payload,
        }
        case DrillingFluidsError:
        return {
          ...state,
          drillingfluidslist: [],
          error: action.payload
        }
        case DrillingFluidId_ADDTEMP:
        return{
          ...state,
          drillingfluidslist: [ ...state.drillingfluidslist, action.payload],
        }
        case DrillingFluidId_EDITTEMP:
        return{
          ...state,
          ...state.drillingfluidslist[action.index] = action.payload,
          drillingfluidslist: [ ...state.drillingfluidslist ],
        }
        default:
      return { ...state }
    }
}