import { JOBDETAIL_SUCCESS, JOBDETAIL_ERROR, JOBDETAIL_CLEAR } from "../Actions/types"

const INITIAL_STATE = {
  error: null,
  data: null,
  success: false
}

export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case JOBDETAIL_SUCCESS:
      return {
        ...state,
        success: true,
        data: action.payload,
        error: null
      }
    case JOBDETAIL_ERROR:
      return {
        ...state,
        success: false,
        error: action.payload,
        data: null
      }
    case JOBDETAIL_CLEAR:
      return INITIAL_STATE
    default:
      return state
  }
}