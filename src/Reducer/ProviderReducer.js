import {PROVIDER_ERROR, PROVIDER_ITEMS} from "../Actions/types"

const INITIAL_STATE = {
  error: null,
  items: []
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PROVIDER_ERROR:
      return {
        ...state,
        error: action.payload
      }
    case PROVIDER_ITEMS:
      return {
        ...state,
        items: action.payload,
        error: null
      }
    default:
      return state
  }
}
