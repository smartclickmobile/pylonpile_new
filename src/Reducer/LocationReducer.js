import { 
    LOCATION,
    LOCATION_ERROR
   } from "../Actions/types"
  
  const INITIAL_STATE = {
    error: null,
    location_lat:null,
    location_log:null
  }
  
  export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case LOCATION:
        return {
          ...state,
          location_lat: action.payload.latitude,
          location_log: action.payload.longitude,
          error: null,
        }
      case LOCATION_ERROR:
        return {
          ...state,
          location_lat: null,
          location_log: null,
          error: action.payload,
        }
      default:
        return state
    }
  }
  