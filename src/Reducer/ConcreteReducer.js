import { ConcreteList, ConcreteError, ConcreteList10, ConcreteError10, ConcreteRecord, ConcreteRecordError, CONCRETE_ADDTEMP, CONCRETE_EDITTEMP } from "../Actions/types"

const INITIAL_STATE = {
  concretelist: [],
  concretevolume: 0,
  status: 0, // Status step09
  status10: 0,
  error: null,
  concretelist10: [],
  concretevolume10: 0,
  error10: null,
  concreterecord: [],
  errorrecord: null,
  tremiecut: [],
  tremieleft: [],
  tremiedeep: [],
  concretecumulative: 0,
  tremiecutcumulative: 0,
  tremiecutcumulativelength: 0,
  break: false,
  concretecumulativeArr: [],
  tremiecutcumulativeArr: [],
  tremiecutcumulativelengthArr: [],
  index: [],
  concreteid: null,
  mixid: null,
  lasttruck: false,
  lastdepthArr: [null],
  lastdepthconcreteArr: [null]
}

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case ConcreteList:
      return {
        ...state,
        concretelist: action.payload,
        concretevolume: action.volume,
        status: action.status,
        error: null,
        index: action.index,
        concreteid: action.concreteid,
        mixid: action.mixid
      }
    case CONCRETE_ADDTEMP:
      return {
        ...state,
        concretelist: [ ...state.concretelist, action.payload],
        concreteid: action.concreteid,
        mixid: action.mixid,
        index: state.index.push(state.index.length)
      }
      break
    case CONCRETE_EDITTEMP:
    return {
      ...state,
      ...state.concretelist[action.index]=action.payload,
      concretelist: [ ...state.concretelist],
      concreteid: action.concreteid,
      mixid: action.mixid,
      index: state.index.push(state.index.length)
    }
    case ConcreteError:
      return {
        ...state,
        concretelist: [],
        concretevolume: 0,
        error: action.payload
      }
    case ConcreteList10:
      return {
        ...state,
        concretelist10: action.payload,
        concretevolume10: action.volume,
        error10: null
      }
    case ConcreteError10:
      return {
        ...state,
        concretelist10: [],
        concretevolume10: 0,
        error10: action.payload
      }
    case ConcreteRecord:
      return {
        ...state,
        concreterecord: action.payload,
        concreteerror: null,
        tremiecut: action.tremiecut,
        tremieleft: action.tremieleft,
        tremiedeep: action.tremiedeep,
        concretecumulative: action.concretecumulative,
        tremiecutcumulative: action.tremiecutcumulative,
        tremiecutcumulativelength: action.tremiecutcumulativelength,
        break: action.break,
        concretecumulativeArr: action.concretecumulativeArr,
        tremiecutcumulativeArr: action.tremiecutcumulativeArr,
        tremiecutcumulativelengthArr: action.tremiecutcumulativelengthArr,
        status10: action.status,
        lasttruck: action.lasttruck,
        lastdepthArr: action.lastdepthArr,
        lastdepthconcreteArr: action.lastdepthconcreteArr
      }
    case ConcreteRecordError:
      return {
        ...state,
        concreteerror: action.payload,
        concreterecord: []
      }
    default:
      return { ...state }
  }
}
