import {STEEL_SEC_DETAIL_ERROR, STEEL_SEC_DETAIL_ERROR_RESET, STEEL_SEC_DETAIL} from "../Actions/types"

const INITIAL_STATE = {
  error: null,
  sectionDetail: {},
}

export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case STEEL_SEC_DETAIL_ERROR:
      return {
        ...state,
        error: action.payload
      }
    case STEEL_SEC_DETAIL_ERROR_RESET:
      return {
        ...state,
        error: null
      }
    case STEEL_SEC_DETAIL:
      return {
        ...state,
        sectionDetail: action.payload,
        error: null
      }
    default:
      return state
  }
}
