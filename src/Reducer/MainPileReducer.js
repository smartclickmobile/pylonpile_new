import {
  MAIN_PILE_GET_STORAGE,
  MAIN_PILE_GET_STORAGE_ERROR,
  MAIN_PILE_GET_ALL_STORAGE,
  MAIN_PILE_GET_ALL_STORAGE_ERROR,
  MAIN_PILE_STACK,
  MAIN_PILE_CLEAR_STACK,
  MAIN_PILE_STEP_STATUS,
  MAIN_PILE_CLEAR_STATUS,
  STEP,
  STEP_STATUS,
  STEP_STATUS_UN,
  MAIN_PILE_CLEAR
} from "../Actions/types"

const INITIAL_STATE = {
  pile: null,
  pileAll: [],
  stack_item: [],
  step_status: null,
  pile_title:'',
  step:[],
  step_status2:null,
  step_status2_random:null,
  process:null
}

export default(state = INITIAL_STATE, action) => {
  // console.warn('action.type',action.type)
  switch (action.type) {
    case MAIN_PILE_GET_STORAGE:
      return {
        ...state,
        pile: action.payload
      }
    case MAIN_PILE_GET_STORAGE_ERROR:
      return {
        ...state,
      }
    case MAIN_PILE_GET_ALL_STORAGE:
      return {
        ...state,
        pileAll: action.payload
      }
    case MAIN_PILE_GET_ALL_STORAGE_ERROR:
      return {
        ...state,
      }
    case MAIN_PILE_STACK:
      if (action.payload.length > 0) {
        var data = state.stack_item
        data = action.payload
      } else {
        var data = state.stack_item
        data.push(action.payload)
        if (data.length > 1) {
          if (data[data.length - 2].process === action.payload.process && data[data.length - 2].step === action.payload.step ) {
            data.splice(data.length - 1,1)
          }
        }
      }

      return {
        ...state,
        stack_item: data
      }
    case MAIN_PILE_CLEAR_STACK:
      return {
        ...state,
        stack_item: []
      }
    case MAIN_PILE_STEP_STATUS:
      return {
        ...state,
        step_status: action.payload
      }
    case MAIN_PILE_CLEAR_STATUS:
      return {
        ...state,
        step_status: null
      }
      case STEP:
      return {
        ...state,
        step: action.payload
      }
      case STEP_STATUS:
      return {
        ...state,
        step_status2: action.payload,
        step_status2_random:Math.floor((Math.random() * 1000) + 1),
        process:action.process
      }
      case STEP_STATUS_UN:
      return {
        ...state,
        step_status2_random:null,
        step_status2:null,
        process:null
      }
    case MAIN_PILE_CLEAR:
    console.warn('MAIN_PILE_CLEAR',INITIAL_STATE)
      return INITIAL_STATE
    default:
      return {
        ...state
      }
      break
  }
}
