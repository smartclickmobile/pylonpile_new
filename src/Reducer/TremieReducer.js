import { TREMIELIST, TREMIEERROR, TREMIELOADING, TREMIESAVETEMP } from "../Actions/types"

const INITIAL_STATE = {
  tremielist: [],
  tremievolume: 0,
  tremiepipe: [],
  depth: 0,
  depth_child:0,
  loading: false,
  tremiesizeid: null,
  haslast: false
}

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case TREMIELIST:
      return {
        ...state,
        tremielist: action.payload,
        tremievolume: action.volume,
        tremiepipe: action.pipe,
        depth: action.depth,
        depth_child:action.depth_child,
        loading: action.loading,
        tremiesizeid: action.tremiesizeid,
        haslast: action.haslast
      }
    case TREMIESAVETEMP:
      return {
        ...state,
        tremielist: [ ...state.tremielist, action.payload ],
        tremievolume: state.tremievolume + action.volume,
        tremiesizeid: action.tremiesizeid
      }
    case TREMIEERROR:
      return {
        ...state,
        tremielist: [],
        tremievolume: 0,
        tremiepipe: [],
        depth: 0,
        depth_child:0,
        loading: false
      }
    case TREMIELOADING:
      return {
        ...state,
        loading: action.loading
      }
    default:
      return {
        ...state
      }
  }
}