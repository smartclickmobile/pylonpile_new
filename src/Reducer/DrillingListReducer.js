import { 
  Drilling, 
  DrillingError, 
  Drilling_ADDTEMP, 
  Drilling_EDITTEMP,
  DrillingChild, 
  DrillingChildError, 
  DrillingChild_ADDTEMP, 
  DrillingChild_EDITTEMP,
} from "../Actions/types"
const INITIAL_STATE = {
    drillinglist:[],
    drillinglistChild:[],
    drillinglist_random:null,
    drillinglistChild_random:null
}
export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
      case Drilling:
        return {
          ...state,
          drillinglist: action.payload,
          drillinglist_random:Math.floor((Math.random() * 100) + 1),
        }
      case DrillingError:
        return {
          ...state,
          drillinglist: [],
          error: action.payload
        }
      case Drilling_ADDTEMP:
        return{
          ...state,
          drillinglist: [ ...state.drillinglist, action.payload],
        }
      case Drilling_EDITTEMP:
        return{
          ...state,
          ...state.drillinglist[action.index]=action.payload,
          drillinglist: [ ...state.drillinglist ],
        }
      case DrillingChild:
        return {
          ...state,
          drillinglistChild: action.payload,
          drillinglistChild_random:Math.floor((Math.random() * 100) + 1),
        }
      case DrillingChildError:
        return {
          ...state,
          drillinglistChild: [],
          error: action.payload
        }
      case DrillingChild_ADDTEMP:
        return{
          ...state,
          drillinglistChild: [ ...state.drillinglistChild, action.payload],
        }
      case DrillingChild_EDITTEMP:
        return{
          ...state,
          ...state.drillinglistChild[action.index]=action.payload,
          drillinglistChild: [ ...state.drillinglistChild ],
        }
        default:
      return { ...state }
    }
}