import React, { Component } from "react"
import { WebView,Text } from 'react-native'
import { connect } from "react-redux"
import * as actions from "./Actions"
import { Actions } from "react-native-router-flux"
class Dashboard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data:[]
        }
    }
    detailRightButton = () => <Text>test</Text>
    componentDidMount(){
      // Actions.refresh({ right: this.detailRightButton, action: "start" })
    }
  render() {
      // console.warn(this.props.dashboard.Link)
    return (
      <WebView
        source={{uri: this.props.dashboard.Link}}
        // style={{marginTop: 20}}
      />
    )
  }
}
const mapStateToProps = state => {
    return {
        dashboard: state.auth.dashboard,
    }
  }
export default connect(mapStateToProps, actions)(Dashboard)