import React, { Component } from "react"
import { StyleSheet, Text, View, TouchableOpacity, Linking} from "react-native"
import { Content, Container,List,ListItem,Button,Right,Left,Body,Badge,Thumbnail } from "native-base"
import { Actions } from "react-native-router-flux"
import {  Icon } from "react-native-elements"
import I18n from "../../assets/languages/i18n"
import DeviceInfo from "react-native-device-info"
import { connect } from "react-redux"
import * as actions from "../Actions"


class SideBar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      noti: 0,
      Label:'Dashboard',
      dashboardlist:[],
      test:[{Label:'test',Link:'test'}]
    }
  }
  logout() {
    this.props.clearNoti()
    this.props.authLogout()
    this.props.ungetStepStatus2()
  }

  setWhiteColor(scene) {
    if (Actions.currentScene == scene) {
      return { color: "#FFF" }
    }
  }

  _toHome(current) {
    Actions.joblist()
  }

  toUserProfile() {
    Actions.userprofile()
  }
  toDashboard(Linkurl) {
    console.log('Test toDashboard')
    Linking.openURL(Linkurl)
    // Linking.canOpenURL(Linkurl).then(supported => {
    //   if (supported) {
    //     Linking.openURL(Linkurl)
    //   } else {
    //     console.log("Don't know how to open URI: " + Linkurl)
    //   }
    // })
  }
  toNoti() {
    Actions.notification({ fromnoti: false })
  }

  toHistory() {
    Actions.history()
  }

  toNote() {
    Actions.note()
  }

  componentDidMount() {
    this.props.notiList()
    if (this.props.noti != this.state.noti) {
      this.setState({ noti: this.props.noti})
    }
    var data ={
      Language:I18n.locale
    }
    this.props.getUserinfo(data)
    // this.setState({ Label:this.props.dashboard==undefined || this.props.dashboard== null?'Dashboard': this.props.dashboard.Label,dashboardlist:this.props.dashboard==undefined || this.props.dashboard== null?[]: this.props.dashboard})
  }

  componentWillReceiveProps(props) {
    // console.warn('dashboardlist1',props.dashboard1)
    if (props.noti != this.state.noti) {
      this.setState({ noti: props.noti })
    }
    if(props.dashboard1!=undefined && props.dashboard1!= null){
      this.setState({dashboardlist:props.dashboard1})
    }
  }

  _containerStyle(scene) {
    if (scene.includes(Actions.currentScene)) {
      return { backgroundColor: "#037aaf", height: DeviceInfo.isTablet() ? 80 : 45, justifyContent: "center",marginLeft:0 }
    }
    return { height: DeviceInfo.isTablet() ? 80 : 45, justifyContent: "center",marginLeft:0 }
  }

  _titleStyle(scene) {
    if (scene.includes(Actions.currentScene)) {
      return { color: "#FFF", fontSize: DeviceInfo.isTablet() ? 25 : 14 }
    }
    return { color: "#037aaf", fontSize: DeviceInfo.isTablet() ? 25 : 14 }
  }

  _leftIcon(scene, key) {
    switch (key) {
      case 1:
        if (scene.includes(Actions.currentScene)) {
          return { name: "home", color: "#FFF" }
        }
        return { name: "home", color: "#037aaf" }
        break
      case 2:
        if (scene.includes(Actions.currentScene)) {
          return { name: "person", color: "#FFF" }
        }
        return { name: "person", color: "#037aaf" }
        break
      case 3:
        if (scene.includes(Actions.currentScene)) {
          return { name: "ios-notifications", type: "ionicon", color: "#FFF" }
        }
        return { name: "ios-notifications", type: "ionicon", color: "#037aaf" }
        break
      case 4:
        if (scene.includes(Actions.currentScene)) {
          return { name: "file-text", color: "#FFF", type: "font-awesome" }
        }
        return { name: "file-text", color: "#037aaf", type: "font-awesome" }
        break
      case 5:
        if (scene.includes(Actions.currentScene)) {
          return { name: "exclamation", color: "#FFF", type: "material-community" }
        }
        return { name: "exclamation", color: "#037aaf", type: "material-community" }
        break
      case 6:
        if (scene.includes(Actions.currentScene)) {
          return { name: "logout", color: "#FFF", type: "simple-line-icon" }
        }
        return { name: "logout", color: "#037aaf", type: "simple-line-icon" }
        break
      case 7:
        if (scene.includes(Actions.currentScene)) {
          return { name: "dashboard", color: "#FFF", type: "font-awesome" }
        }
        return { name: "dashboard", color: "#037aaf", type: "font-awesome" }
        break
      default:
        break
    }
  }

  _leftIcon_Color(scene) {
        if (scene.includes(Actions.currentScene)) {
          return"#FFF"
        }
        return "#037aaf" 
  }

  render() {
    // console.warn('language',I18n.locale)
    return (
      <View style={{ flex: 1}}>
        <List containerStyle={{ marginTop: 10, borderTopWidth: 0.2, borderBottomWidth: 0.2 }}>

          <ListItem style={this._containerStyle(["_joblist", "_projectlist", "_mainpile"])} onPress={()=>  Actions.joblist()}>
            <Left style={{flex:2,paddingLeft:20}}>
                <Icon name="home" color={this._leftIcon_Color(["_joblist", "_projectlist", "_mainpile"])} />
            </Left>
            <Body style={{flex:8}}>
              <Text style={this._titleStyle(["_joblist", "_projectlist", "_mainpile"])}>{I18n.t("drawer.mainpage")}</Text>
            </Body>
          </ListItem>

          <ListItem style={this._containerStyle(["_userprofile"])} onPress={()=> Actions.userprofile()}>
            <Left style={{flex:2,paddingLeft:20}}>
                <Icon name="person" color={this._leftIcon_Color(["_userprofile"])} />
            </Left>
            <Body style={{flex:8}}>
              <Text style={this._titleStyle(["_userprofile"])}>{I18n.t("drawer.userprofile")}</Text>
            </Body>
          </ListItem>

          {this.state.noti > 0 ?<ListItem style={this._containerStyle(["_notification"])} onPress={()=> Actions.notification({ fromnoti: false })}>
            <Left style={{flex:2,paddingLeft:25}}>
                <Icon name="ios-notifications" type="ionicon" color={this._leftIcon_Color(["_notification"])} />
            </Left>
            <Body style={{flex:6}}>
              <Text style={this._titleStyle(["_notification"])}>{I18n.t("drawer.notification")}</Text>
            </Body>
            <Right style={{flex:2}}>
              <Badge style={{ backgroundColor: '#333333' ,alignItems:'center',justifyContent:'center'}}>
                <Text style={{ color: "orange" ,marginHorizontal:5}}>{this.state.noti}</Text>
              </Badge>
           
            </Right>
          </ListItem>:
          <ListItem style={this._containerStyle(["_notification"])} onPress={()=> Actions.notification({ fromnoti: false })}>
            <Left style={{flex:2,paddingLeft:25}}>
                <Icon name="ios-notifications" type="ionicon" color={this._leftIcon_Color(["_notification"])} />
            </Left>
            <Body style={{flex:8}}>
              <Text style={this._titleStyle(["_notification"])}>{I18n.t("drawer.notification")}</Text>
            </Body>
          </ListItem>
          }
          {
            this.state.dashboardlist.map((item,index)=>{
              return(
                <ListItem key={index} style={this._containerStyle(["_dashboard"])} onPress={()=> this.toDashboard(item.Link)}>
                  <Left style={{flex:2,paddingLeft:20}}>
                    <Thumbnail square source={{ uri: item.Icon }} style={{width:30,height:30}}/>
                  </Left>
                  <Body style={{flex:8}}>
                    <Text style={this._titleStyle(["_dashboard"])}>{item.Label}</Text>
                  </Body>
                </ListItem>
              )
            })
          }
          {/*<ListItem
            key="1"
            containerStyle={this._containerStyle(["_joblist", "_projectlist", "_mainpile"])}
            title={I18n.t("drawer.mainpage")}
            titleStyle={this._titleStyle(["_joblist", "_projectlist", "_mainpile"])}
            titleContainerStyle={{ marginLeft: 5 }}
            leftIcon={this._leftIcon(["_joblist", "_projectlist", "_mainpile"], 1)}
            onPress={() => this._toHome(Actions.currentScene)}
            hideChevron
          />*/}
          {/*<ListItem
            key="2"
            title={I18n.t("drawer.userprofile")}
            titleStyle={this._titleStyle(["_userprofile"])}
            titleContainerStyle={{ marginLeft: 5 }}
            containerStyle={this._containerStyle(["_userprofile"])}
            leftIcon={this._leftIcon(["_userprofile"], 2)}
            hideChevron
            onPress={() => this.toUserProfile()}
          />*/}
          {/*this.state.noti > 0 ? (
            <ListItem
              key="3"
              title={I18n.t("drawer.notification")}
              titleStyle={this._titleStyle(["_notification"])}
              titleContainerStyle={{ marginLeft: 11.5 }}
              containerStyle={this._containerStyle(["_notification"])}
              leftIcon={this._leftIcon(["_notification"], 3)}
              hideChevron
              onPress={() => this.toNoti()}
              badge={{ value: this.state.noti, textStyle: { color: "orange" } }}
            />
          ) : (
            <ListItem
              key="3"
              title={I18n.t("drawer.notification")}
              titleStyle={this._titleStyle(["_notification"])}
              titleContainerStyle={{ marginLeft: 11.5 }}
              containerStyle={this._containerStyle(["_notification"])}
              leftIcon={this._leftIcon(["_notification"], 3)}
              hideChevron
              onPress={() => this.toNoti()}
            />
          )*/}
          {/*
            this.state.dashboardlist.map((item,index)=>{
              return(
                <ListItem
                  key={index+4}
                  title={item.Label}
                  titleStyle={this._titleStyle(["_dashboard"])}
                  titleContainerStyle={{ marginLeft: 5 }}
                  containerStyle={this._containerStyle(["_dashboard"])}
                  // leftIcon={this._leftIcon(["_dashboard"], 7)}
                  avatar={{ uri: item.Icon }}
                  hideChevron
                  onPress={() => this.toDashboard(item.Link)}
                />
              )
            })
          */}
        </List>
        <View style={{ alignItems: "flex-end", marginTop: 5, marginRight: 10 }}>
          <Text style={{ color: "#037aaf" }}>{"v" + DeviceInfo.getVersion()}</Text>
        </View>
          <TouchableOpacity style={styles.logout} onPress={() => this.logout()}>
            <Icon name="logout" color="#037aaf" type="simple-line-icon" />
            <Text style={styles.logoutText}>{I18n.t("drawer.logout")}</Text>
          </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  logout: {
    height: DeviceInfo.isTablet() ? 80 : 50,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    flexDirection: "row",
    bottom: 0,
    position: 'absolute',
    width: '100%',
    alignItems: 'center'
  },
  logoutText: {
    color: "#037aaf",
    marginLeft: 20,
    fontSize: DeviceInfo.isTablet() ? 25 : 14
  }
})

const mapStateToProps = state => {
  return {
    noti: state.noti.count,
    dashboard: state.auth.dashboard,
    dashboard1: state.auth.dashboard1
  }
}

export default connect(mapStateToProps, actions)(SideBar)
