import React, { Component } from "react"
import { StyleSheet, View, Text, TouchableOpacity, FlatList, Alert,Image,Linking } from "react-native"
import { Button, Icon } from "react-native-elements"
import { Actions } from "react-native-router-flux"
import { PileDetail } from "../../Controller/API"
import PileDetailRightButton from "../PileDetailRightButton"
import ERPButton from "../ERPButton"
import { MAIN_COLOR, CATEGORY_1, CATEGORY_2, CATEGORY_3, SUB_COLOR } from "../../Constants/Color"
import I18n from "../../../assets/languages/i18n"
import DeviceInfo from "react-native-device-info"

export default class FinishProject extends Component {
  setCategoryColor(category) {
    if (category == 1 || category == 4) {
      return {
        borderLeftColor: CATEGORY_1
      }
    } else if (category == 2) {
      return {
        borderLeftColor: CATEGORY_2
      }
    } else if (category == 3 || category == 5) {
      return {
        borderLeftColor: CATEGORY_3
      }
    }
  }

  _pileDetailIcon(data) {
    return <PileDetailRightButton data={data} />
  }

  _onPressButton(pile_id, pile_no, category, sp_parentid, pile_childs, icon, url) {
    Actions.mainpile({ title: pile_no, pileid: pile_id, jobid: this.props.jobid, category: category,viewType:0 , finish:true, sp_parentid: sp_parentid, pile_childs,icon_image:icon, url_dash:url})
  }

  _keyExtractor = (item, index) => index.toString()
  opendash(item){
    Linking.openURL(item.Link)
    // Linking.canOpenURL(item.Link).then(supported => {
    //   if (supported) {
    //     Linking.openURL(item.Link)
    //   } else {
    //     console.log("Don't know how to open URI: " + item.Link)
    //   }
    // })
  }
  _renderItem = ({ item }) => {
    // console.warn('item',item)
  return(
    <View style={[styles.topic, { borderTopWidth: 1 }, this.setCategoryColor(item.category)]}>
      <TouchableOpacity
        style={{ marginLeft: 10, width: "57%" }}
        onPress={() => this._onPressButton(item.pile_id, item.pile_no, item.category, item.sp_parentid, item.pile_childs,item.links.Icon,item.links.Link)}
      >
        <View style={{ marginTop: 10 }}>
          <Text numberOfLines={1} style={styles.pileText}>{item.pile_no}</Text>
        </View>
        <View style={{ flexDirection: "row", marginTop: 10 }}>
          <Icon name="calendar" type="entypo" color={MAIN_COLOR} size={DeviceInfo.isTablet() ? 20 : 14} />
          <Text style={styles.dateText}> {item.end_date} </Text>
         { /*<Icon name="access-time" type="MaterialIcons" color={MAIN_COLOR} size={DeviceInfo.isTablet() ? 20 : 14} />
  <Text style={styles.dateText}> {item.total_date} </Text>*/}
        </View>
      </TouchableOpacity>
      <View style={{ width: "43%" }}>
        <View style={{ alignItems: "center", justifyContent: "center", flex: 1 ,flexDirection:'row'}}>
          { /* item.olddata_flag && <ERPButton /> */}
          {
            item.links != null?
            <TouchableOpacity style={{
              backgroundColor: SUB_COLOR,
            height: 30,
            width: 30,
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "row",
            marginTop:5,
            marginLeft:item.modifydata_flag == false ? 80:0,
            // borderRadius: 10
          }} onPress={()=>this.opendash(item.links)}>
                <Image 
                    style={{width:30,height: 30}}
                    source={{uri:item.links.Icon}}
                 />
          </TouchableOpacity>
          :<View/>
          }
          {
            item.modifydata_flag?
            <ERPButton Language={this.props.Language} data={item} jobid={this.props.jobid} lat={this.props.lat} log={this.props.log} pile_no={item.pile_no} category={item.category}/>
            :<View/>
          }
          {/*<Text style={[styles.dateText,{marginRight:15}]}> ส่ง ERP เรียบร้อย </Text>*/}
        </View>
      </View>
    </View>
  )
  }
  render() {
    const piles = this.props.array
    return (
      <View style={{ marginTop: 10 }}>
        <FlatList
          data={piles}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          keyboardShouldPersistTaps="always"
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  topic: {
    borderTopWidth: 1,
    // borderColor: '#D9D9D9',
    borderLeftWidth: 5,
    borderLeftColor: "blue",
    borderTopWidth: 0,
    height: DeviceInfo.isTablet() ? 100 : 80,
    flexDirection: "row"
  },
  button: {
    width: 20,
    height: 20,
    borderRadius: 5,
    marginLeft: 5
  },
  pileText: {
    color: "#000",
    fontSize: DeviceInfo.isTablet() ? 25 : 18
  },
  dateText: {
    color: "#000",
    fontSize: DeviceInfo.isTablet() ? 21 : 14
  }
})
