import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, FlatList, Alert,Dimensions, ToastAndroid,Image,Linking } from 'react-native'
import { Button, Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'
import { PileDetail } from '../../Controller/API'
import ERPButton from '../ERPButton'
import PileDetailRightButton from '../PileDetailRightButton'
import { SUB_COLOR,MENU_GREY, MENU_ORANGE, MENU_GREEN, MENU_GREY_BORDER, MENU_ORANGE_BORDER, MENU_GREEN_BORDER, CATEGORY_1, CATEGORY_2, CATEGORY_3, STATUS_1, STATUS_2, STATUS_3 } from '../../Constants/Color'
import I18n from '../../../assets/languages/i18n'
import DeviceInfo from 'react-native-device-info'
import Svg,{
  Line,
} from 'react-native-svg'
import RNTooltips from "react-native-tooltips"
import * as actions from "../../Actions"
import { connect } from "react-redux"
const sliderWidth = Dimensions.get("window").width
const itemWidth = sliderWidth * 0.11
class RecentProject extends Component {
  constructor(props){
    super(props)
    this.state = {
      visible1:false,
      visible2:false,
      visible3:false,
      visible4:false,
      visible5:false,
      visible6:false,
      visible7:false,
      visible8:false,
      visible9:false,
      visible10:false,
      visible11:false,
    }
  }
  setStatusColor(category) {
    if (category == 0) {
      return {
        backgroundColor: STATUS_1,
        borderWidth: 0.2
      }
    }
    else if (category == 1) {
      return {
        backgroundColor: STATUS_2,
        borderWidth: 0.2
      }
    }
    else if (category == 2) {
      return {
        backgroundColor: STATUS_3,
        borderWidth: 0.2
      }
    }
  }
  opendash(item){
    Linking.openURL(item.Link)
    // Linking.canOpenURL(item.Link).then(supported => {
    //   if (supported) {
    //     Linking.openURL(item.Link)
    //   } else {
    //     console.log("Don't know how to open URI: " + item.Link)
    //   }
    // })
  }
  importERP(item){
    // console.warn('importERP',item.pile_id)
    // navigator.geolocation.watchPosition(async position => {
    //   let lat = position.coords.latitude
    //   let log = position.coords.longitude
    Alert.alert(I18n.t('projectlist.confrim'), I18n.t('projectlist.confrimtext'), [
      {
        text: "Cancel",
      },
      {
        text: "Ok",
        onPress: () => {
          var data = {
            Language:I18n.locale,
            JobId: this.props.jobid,
            PileId: item.pile_id,
            lat:this.props.lat,
            log:this.props.log
          }
          this.props.importtoerp(data)
          this.props.setloading1()
          setTimeout(() => {
            this.props.pileitemsclear({clearerp:true})
            this.props.pileItems({jobid: this.props.jobid,Language:this.props.Language,start:0,length:27,status:0 })
            this.props.pileItems({jobid: this.props.jobid,Language:this.props.Language,start:0,length:10,status:1 })
            this.props.pileItems({jobid: this.props.jobid,Language:this.props.Language,start:0,length:10,status:2 })
          }, 0)
         
        }
      }
    ])
    //   this.props.importtoerp(data)
    // })
  }
  setCategoryColor(category) {
    if (category == 1 || category == 4) {
      return {
        borderLeftColor: CATEGORY_1
      }
    }
    else if (category == 2) {
      return {
        borderLeftColor: CATEGORY_2
      }
    }
    else if (category == 3 || category == 5) {
      return {
        borderLeftColor: CATEGORY_3
      }
    }
  }

  _pileDetailIcon(data) {
    return (
      <PileDetailRightButton data={data} />
    )
  }

  _onPressButton(pile_id, pile_no,category,item, sp_parentid, pile_childs, icon, url) {
    // console.warn('item',item.status,item.importerp_flag)
    var temp = false
    if(item.status == 2 && !item.importerp_flag){
      temp = true
    }
    
    Actions.mainpile({title: pile_no, pileid: pile_id, jobid: this.props.jobid, category:category,viewType:0 , finish:temp, sp_parentid:sp_parentid, pile_childs:pile_childs,icon_image:icon, url_dash:url})
  }

  _keyExtractor = (item, index) => index.toString()

  _renderItem = ({item, index}) => (
    <View style={[styles.topic, this.setCategoryColor(item.category)]}>
      <TouchableOpacity style={{marginLeft: 10, width: '70%'}} onPress={ () => this._onPressButton(item.pile_id, item.pile_no,item.category,item, item.sp_parentid, item.pile_childs,item.links.Icon,item.links.Link) }>
        <View style={{marginTop: 10}}>
          <Text style={styles.pileText} numberOfLines={1}>{item.pile_no}</Text>
          
        </View>
        <View style={{flexDirection: 'row', marginTop: 10}}>
          <View style={[styles.button, this.setStatusColor(item.secound_status[0])]} />
          <View style={[styles.button, this.setStatusColor(item.secound_status[1])]} />
          <View style={[styles.button, this.setStatusColor(item.secound_status[2])]} />
          <View style={[styles.button, this.setStatusColor(item.secound_status[3])]} />
          <View style={[styles.button, this.setStatusColor(item.secound_status[4])]} />
          <View style={[styles.button, this.setStatusColor(item.secound_status[5])]} />
          <View style={[styles.button, this.setStatusColor(item.secound_status[6])]} />
          <View style={[styles.button, this.setStatusColor(item.secound_status[7])]} />
          <View style={[styles.button, this.setStatusColor(item.secound_status[8])]} />
          <View style={[styles.button, this.setStatusColor(item.secound_status[9])]} />
          <View style={[styles.button, this.setStatusColor(item.secound_status[10])]} />
        </View>
      </TouchableOpacity>
      {/* <View style={{width: '30%'}}>
        <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
          <ERPButton />
        </View>
      </View> */}
    </View>
  )
  getTooltip(index){
    var time = 1000
    if(index == 1){
      this.setState({visible1:true , visible2: false, visible3: false, visible4: false, visible5: false, visible6: false,visible7: false, visible8: false, visible9: false, visible10:false, visible11:false},()=>{
        setTimeout(() => 
        this.setState({visible1:false})
        ,time)
      })
    
    }else if(index == 2){
      this.setState({visible1:false , visible2: true, visible3: false, visible4: false, visible5: false, visible6: false,visible7: false, visible8: false, visible9: false, visible10:false, visible11:false},()=>{
        setTimeout(() => 
        this.setState({visible2:false})
        ,time)
      })
    }else if(index == 3){
      this.setState({visible1:false , visible2: false, visible3: true, visible4: false, visible5: false, visible6: false,visible7: false, visible8: false, visible9: false, visible10:false, visible11:false},()=>{
        setTimeout(() => 
        this.setState({visible3:false})
        ,time)
      })
    }else if(index == 4){
      this.setState({visible1:false , visible2: false, visible3: false, visible4: true, visible5: false, visible6: false,visible7: false, visible8: false, visible9: false, visible10:false, visible11:false},()=>{
        setTimeout(() => 
        this.setState({visible4:false})
        ,time)
      })
    }else if(index == 5){
      this.setState({visible1:false , visible2: false, visible3: false, visible4: false, visible5: true, visible6: false,visible7: false, visible8: false, visible9: false, visible10:false, visible11:false},()=>{
        setTimeout(() => 
        this.setState({visible5:false})
        ,time)
      })
    }else if(index == 6){
      this.setState({visible1:false , visible2: false, visible3: false, visible4: false, visible5: false, visible6: true,visible7: false, visible8: false, visible9: false, visible10:false, visible11:false},()=>{
        setTimeout(() => 
        this.setState({visible6:false})
        ,time)
      })
    }else if(index == 7){
      this.setState({visible1:false , visible2: false, visible3: false, visible4: false, visible5: false, visible6: false,visible7: true, visible8: false, visible9: false, visible10:false, visible11:false},()=>{
        setTimeout(() => 
        this.setState({visible7:false})
        ,time)
      })
    }else if(index == 8){
      this.setState({visible1:false , visible2: false, visible3: false, visible4: false, visible5: false, visible6: false,visible7: false, visible8: true, visible9: false, visible10:false, visible11:false},()=>{
        setTimeout(() => 
        this.setState({visible8:false})
        ,time)
      })
    }else if(index == 9){
      this.setState({visible1:false , visible2: false, visible3: false, visible4: false, visible5: false, visible6: false,visible7: false, visible8: false, visible9: true, visible10:false, visible11:false},()=>{
        setTimeout(() => 
        this.setState({visible9:false})
        ,time)
      })
    }else if(index == 10){
      this.setState({visible1:false , visible2: false, visible3: false, visible4: false, visible5: false, visible6: false,visible7: false, visible8: false, visible9: false, visible10:true, visible11:false},()=>{
        setTimeout(() => 
        this.setState({visible10:false})
        ,time)
      })
    }else if(index == 11){
      this.setState({visible1:false , visible2: false, visible3: false, visible4: false, visible5: false, visible6: false,visible7: false, visible8: false, visible9: false, visible10:false, visible11:true},()=>{
        setTimeout(() => 
        this.setState({visible11:false})
        ,time)
      })
    }
  }
  render() {
    const item = this.props.data
    var flag = false
            if(item.pile_id != item.sp_parentid){
              flag = true
            }
    return (
      <View style={[styles.topic, this.setCategoryColor(item.category)]} key={item.pile_id}>
      <RNTooltips
          text={I18n.t('mainpile.step.1')} 
          textSize={16} 
          visible={this.state.visible1}
          reference={this.main1}
          tintColor="#ffffff"
          textColor="#000000"
          position={3}
          duration={800}
        />
        <RNTooltips
          text={I18n.t('mainpile.step.2')}
          textSize={16}
          visible={this.state.visible2}
          reference={this.main2}
          tintColor="#ffffff"
          textColor="#000000"
          position={3}
          duration={800}
        />
        <RNTooltips
          text={I18n.t('mainpile.step.3')}
          textSize={16}
          visible={this.state.visible3}
          reference={this.main3}
          tintColor="#ffffff"
          textColor="#000000"
          position={3}
          duration={800}
        />
        <RNTooltips
          text={I18n.t('mainpile.step.4')}
          textSize={16}
          visible={this.state.visible4}
          reference={this.main4}
          tintColor="#ffffff"
          textColor="#000000"
          position={3}
          duration={800}
        />
        <RNTooltips
          text={item.category==1?I18n.t('mainpile.step.5'):I18n.t('mainpile.step.5_dw')}
          textSize={16}
          visible={this.state.visible5}
          reference={this.main5}
          tintColor="#ffffff"
          textColor="#000000"
          position={3}
          duration={800}
        />
        <RNTooltips
          text={item.category==1?I18n.t('mainpile.step.6'):I18n.t('mainpile.step.6_dw')}
          textSize={16}
          visible={this.state.visible6}
          reference={this.main6}
          tintColor="#ffffff"
          textColor="#000000"
          position={3}
          duration={800}
        />
        <RNTooltips
          text={I18n.t('mainpile.step.7')}
          textSize={16}
          visible={this.state.visible7}
          reference={this.main7}
          tintColor="#ffffff"
          textColor="#000000"
          position={3}
          duration={800}
        />
        <RNTooltips
          text={I18n.t('mainpile.step.8')}
          textSize={16}
          visible={this.state.visible8}
          reference={this.main8}
          tintColor="#ffffff"
          textColor="#000000"
          position={3}
          duration={800}
        />
        <RNTooltips
          text={I18n.t('mainpile.step.9')}
          textSize={16}
          visible={this.state.visible9}
          reference={this.main9}
          tintColor="#ffffff"
          textColor="#000000"
          position={3}
          duration={800}
        />
        <RNTooltips
          text={I18n.t('mainpile.step.10')}
          textSize={16}
          visible={this.state.visible10}
          reference={this.main10}
          tintColor="#ffffff"
          textColor="#000000"
          position={3}
          duration={800}
        />
        <RNTooltips
          text={I18n.t('mainpile.step.11')}
          textSize={16}
          visible={this.state.visible11}
          reference={this.main11}
          tintColor="#ffffff"
          textColor="#000000"
          position={3}
          duration={800}
        />
        <View style={{marginLeft: 10, width: '100%',flexDirection:'row'}}>
      <TouchableOpacity style={{marginLeft: 10, width: '63%'}} onPress={ () => this._onPressButton(item.pile_id, item.pile_no,item.category,item, item.sp_parentid, item.pile_childs,item.links.Icon,item.links.Link) }>
      <View style={{marginTop: 10}}>
        <Text style={styles.pileText} numberOfLines={1}>{item.pile_no}</Text>
      </View>
    </TouchableOpacity>
    {
      item.links != null?
      <TouchableOpacity style={{
        backgroundColor: SUB_COLOR,
      height: 30,
      width: 30,
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "row",
      marginTop:5,
      marginRight:5,
      marginLeft:item.importerp_flag == false ? 73:0,
      // borderRadius: 10
    }} onPress={()=>this.opendash(item.links)}>
          <Image 
              style={{width:30,height: 30}}
              source={{uri:item.links.Icon}}
           />
    </TouchableOpacity>
    :<View/>
    }
    {
      item.importerp_flag?
      <TouchableOpacity style={{backgroundColor: SUB_COLOR,
      height: 30,
      width: "20%",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "row",
      marginTop:5,
      borderRadius: 10}} onPress={()=>this.importERP(item)}>
          <Text style={{color:'#fff'}}>{I18n.t('projectlist.senterp')}</Text>
    </TouchableOpacity>
    :<View/>
  }
        </View>
      <View style={{flexDirection: 'row', marginTop: 10,marginLeft: 10}}>
      {this.state.visible1 ?<View style={[styles.arrowtooltip,{marginLeft:10}]}/>:<View/>}
      {this.state.visible1 ?<View style={[styles.Containertooltip]}>
        <Text style={{color:'#000'}}>{I18n.t('mainpile.step.1')}</Text>
      </View>:<View/>}
      
      <TouchableOpacity onPress={()=> this.getTooltip(1)}>
        <View style={[styles.button, this.setStatusColor(item.secound_status[0])]}><Text style={[styles.textnum]}>1</Text></View>
      </TouchableOpacity>
      {this.state.visible2 ?<View style={[styles.arrowtooltip,{marginLeft:42}]}/>:<View/>}
      {this.state.visible2 ?<View style={[styles.Containertooltip,{marginLeft:15}]}>
        <Text style={{color:'#000'}}>{I18n.t('mainpile.step.2')}</Text>
        </View>:<View/>}
      <TouchableOpacity onPress={()=> this.getTooltip(2)}>
        <View style={[styles.button, this.setStatusColor(item.secound_status[1])]}><Text style={[styles.textnum]}>2</Text></View>
      </TouchableOpacity>
      {this.state.visible3 ?<View style={[styles.arrowtooltip,{marginLeft:78}]}/>:<View/>}
      {this.state.visible3 ?<View style={[styles.Containertooltip,{marginLeft:20}]}>
        <Text style={{color:'#000'}}>{I18n.t('mainpile.step.3')}</Text>
        </View>:<View/>}
        {
        item.category==2 || item.category==3 ||item.category==5 ?
          <View style={{position:"absolute",marginLeft:sliderWidth-375.4,marginTop:2}}>
            <Svg
                height={DeviceInfo.isTablet() ? 60 : 30}
                width={itemWidth * 0.6}
            >
                <Line
                    x1="0"
                    y1="0"
                    x2={itemWidth * 0.6}
                    y2={DeviceInfo.isTablet() ? 60 : 28}
                    stroke="red"
                    strokeWidth="2"
                />
                <Line
                x1={itemWidth * 0.6}
                y1="0"
                x2="0"
                y2={DeviceInfo.isTablet() ? 58 : 28}
                stroke="red"
                strokeWidth="2"
            />
            </Svg>
        </View>
        :
        <View/>
      }
      
      <TouchableOpacity onPress={()=> this.getTooltip(3)}>
        <View style={[styles.button, this.setStatusColor(item.secound_status[2])]} ><Text style={[styles.textnum]}>3</Text></View>
      </TouchableOpacity>
      {this.state.visible4 ?<View style={[styles.arrowtooltip,{marginLeft:110}]}/>:<View/>}
      {this.state.visible4 ?<View style={[styles.Containertooltip,{marginLeft:65}]}>
        <Text style={{color:'#000'}}>{I18n.t('mainpile.step.4')}</Text>
        </View>:<View/>}
        {((item.category==3||item.category==5)&&flag==true)&&<View style={{position:"absolute",marginLeft:sliderWidth-341,marginTop:2}}>
                <Svg
                    height={DeviceInfo.isTablet() ? 60 : 30}
                    width={itemWidth * 0.6}
                >
                    <Line
                        x1="0"
                        y1="0"
                        x2={itemWidth * 0.6}
                        y2={DeviceInfo.isTablet() ? 60 : 28}
                        stroke="red"
                        strokeWidth="2"
                    />
                    <Line
                    x1={itemWidth * 0.6}
                    y1="0"
                    x2="0"
                    y2={DeviceInfo.isTablet() ? 58 : 28}
                    stroke="red"
                    strokeWidth="2"
                />
                </Svg>
            </View>}
      <TouchableOpacity onPress={()=> this.getTooltip(4)}>
        <View style={[styles.button, this.setStatusColor(item.secound_status[3])]} ><Text style={[styles.textnum]}>4</Text></View>
      </TouchableOpacity>
      {this.state.visible5 ?<View style={[styles.arrowtooltip,{marginLeft:145}]}/>:<View/>}
      {this.state.visible5 ?<View style={[styles.Containertooltip,{marginLeft:110}]}>
        <Text style={{color:'#000'}}>{I18n.t('mainpile.step.5')}</Text>
        </View>:<View/>}
        {
          item.category==4 ?
            <View style={{position:"absolute",marginLeft:sliderWidth-307,marginTop:2}}>
              <Svg
                  height={DeviceInfo.isTablet() ? 60 : 30}
                  width={itemWidth * 0.6}
              >
                  <Line
                      x1="0"
                      y1="0"
                      x2={itemWidth * 0.6}
                      y2={DeviceInfo.isTablet() ? 60 : 28}
                      stroke="red"
                      strokeWidth="2"
                  />
                  <Line
                  x1={itemWidth * 0.6}
                  y1="0"
                  x2="0"
                  y2={DeviceInfo.isTablet() ? 58 : 28}
                  stroke="red"
                  strokeWidth="2"
              />
              </Svg>
          </View>
          :
          <View/>
        }
      <TouchableOpacity onPress={()=> this.getTooltip(5,)}>
        <View style={[styles.button, this.setStatusColor(item.secound_status[4])]} ><Text style={[styles.textnum]}>5</Text></View>
      </TouchableOpacity>
      {this.state.visible6 ?<View style={[styles.arrowtooltip,{marginLeft:180}]}/>:<View/>}
      {this.state.visible6 ?<View style={[styles.Containertooltip,{marginLeft:150}]}>
        <Text style={{color:'#000'}}>{I18n.t('mainpile.step.6')}</Text>
        </View>:<View/>}
      <TouchableOpacity onPress={()=> this.getTooltip(6)}>
        <View style={[styles.button, this.setStatusColor(item.secound_status[5])]} ><Text style={[styles.textnum]}>6</Text></View>
      </TouchableOpacity>
      {this.state.visible7 ?<View style={[styles.arrowtooltip,{marginLeft:215}]}/>:<View/>}
      {this.state.visible7 ?<View style={[styles.Containertooltip,{marginLeft:180}]}>
        <Text style={{color:'#000'}}>{I18n.t('mainpile.step.7')}</Text>
        </View>:<View/>}
        {
        item.category==2 || item.category==3 || item.category==4 ||(item.category==5 && item.shape==2)?
          <View style={{position:"absolute",marginLeft:sliderWidth-238.4,marginTop:2}}>
            <Svg
            height={DeviceInfo.isTablet() ? 60 : 30}
            width={itemWidth * 0.6}
            >
            <Line
                x1="0"
                y1="0"
                x2={itemWidth * 0.6}
                y2={DeviceInfo.isTablet() ? 60 : 28}
                stroke="red"
                strokeWidth="2"
            />
            <Line
            x1={itemWidth * 0.6}
            y1="0"
            x2="0"
            y2={DeviceInfo.isTablet() ? 58 : 28}
            stroke="red"
            strokeWidth="2"
        />
        </Svg>
        </View>
        :
        <View/>
      }
    
    <TouchableOpacity onPress={()=> this.getTooltip(7)}>
      <View style={[styles.button, this.setStatusColor(item.secound_status[6])]} ><Text style={[styles.textnum]}>7</Text></View>
    </TouchableOpacity>
    {this.state.visible8 ?<View style={[styles.arrowtooltip,{marginLeft:250}]}/>:<View/>}
    {this.state.visible8 ?<View style={[styles.Containertooltip,{marginLeft:I18n.locale=='en'?70:185}]}>
        <Text style={{color:'#000'}}>{I18n.t('mainpile.step.8')}</Text>
        </View>:<View/>}
    <TouchableOpacity onPress={()=> this.getTooltip(8)}>  
      <View style={[styles.button, this.setStatusColor(item.secound_status[7])]} ><Text style={[styles.textnum]}>8</Text></View>
    </TouchableOpacity>
    {this.state.visible9 ?<View style={[styles.arrowtooltip,{marginLeft:280}]}/>:<View/>}
    {this.state.visible9 ?<View style={[styles.Containertooltip,{marginLeft:230}]}>
        <Text style={{color:'#000'}}>{I18n.t('mainpile.step.9')}</Text>
        </View>:<View/>}
    <TouchableOpacity onPress={()=> this.getTooltip(9)}>
      <View style={[styles.button, this.setStatusColor(item.secound_status[8])]} ><Text style={[styles.textnum]}>9</Text></View>
    </TouchableOpacity>
    {this.state.visible10 ?<View style={[styles.arrowtooltip,{marginLeft:317}]}/>:<View/>}
    {this.state.visible10 ?<View style={[styles.Containertooltip,{marginLeft:I18n.locale=='en'?250:280}]}>
        <Text style={{color:'#000'}}>{I18n.t('mainpile.step.10')}</Text>
        </View>:<View/>}
    <TouchableOpacity onPress={()=> this.getTooltip(10)}>
      <View style={[styles.button, this.setStatusColor(item.secound_status[9])]} ><Text style={[styles.textnum]}>10</Text></View>
    </TouchableOpacity>
    {this.state.visible11 ?<View style={[styles.arrowtooltip,{marginLeft:350}]}/>:<View/>}
    {this.state.visible11 ?<View style={[styles.Containertooltip,{marginLeft:I18n.locale=='en'?250:300}]}>
        <Text style={{color:'#000'}}>{I18n.t('mainpile.step.11')}</Text>
        </View>:<View/>}
    <TouchableOpacity onPress={()=> this.getTooltip(11)}>
      <View style={[styles.button, this.setStatusColor(item.secound_status[10])]} ><Text style={[styles.textnum]}>11</Text></View>
    </TouchableOpacity>
      {
        item.category==2 || item.category==3 ||item.category==5 ?
          <View style={{position:"absolute",marginLeft:sliderWidth-68.4,marginTop:2}}>
            <Svg
            height={DeviceInfo.isTablet() ? 60 : 30}
            width={itemWidth * 0.6}
            >
            <Line
                x1="0"
                y1="0"
                x2={itemWidth * 0.6}
                y2={DeviceInfo.isTablet() ? 60 : 28}
                stroke="red"
                strokeWidth="2"
            />
            <Line
            x1={itemWidth * 0.6}
            y1="0"
            x2="0"
            y2={DeviceInfo.isTablet() ? 58 : 28}
            stroke="red"
            strokeWidth="2"
        />
        </Svg>
        </View>
        :
        <View/>
      }
      
      </View>
    

    {/* <View style={{width: '30%'}}>
      <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
        <ERPButton />
      </View>
    </View> */}
  </View>
    )
  }

  render2() {
    const piles = this.props.array
    // console.log(piles, 'Inside unfinish');
    return (
      <View style={{marginTop: 10}}>
        <FlatList
          data={piles}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          keyboardShouldPersistTaps='always'
        />
      </View>
    )
  }
}
const mapStateToProps = state => {
  // console.log(state.pile)
  return{
    loading: state.pile.loading
  }
    
}
export default connect(mapStateToProps, actions )(RecentProject)
const styles = StyleSheet.create({
  topic: {
    borderTopWidth: 1,
    // borderColor: '#D9D9D9',
    borderLeftWidth:5,
    borderLeftColor: 'blue',
    // height: DeviceInfo.isTablet() ? 100 : 100,
    height: DeviceInfo.isTablet() ? 100 : 90,
    // flexDirection: 'row',
    borderBottomWidth: 1,
    backgroundColor: '#FFF',
    borderRightWidth: 1
    // paddingLeftWidth: 5
    // borderColor: 'blue'
  },
  button: {
    width: DeviceInfo.isTablet() ? 34 : 32,
    height: DeviceInfo.isTablet() ? 34 : 32,
    borderRadius: 5,
    borderWidth: 1,
    marginRight: 2,
    backgroundColor: '#FFF'
  },
  pileText: {
    color: '#000',
    fontSize: DeviceInfo.isTablet() ? 25 : 18
  },
  textnum : {
    fontSize:DeviceInfo.isTablet() ? 25 : 18,
    color: '#000',
    textAlign: 'center',
    flex:1,
    marginTop:2
  },
  Containertooltip:{
    position: "absolute",
    marginTop:-45,
    backgroundColor:'#fff',
    padding:8,
    borderWidth:0.5,
    borderRadius:5,
    // elevation:3
  },
  arrowtooltip:{
    width:12,
    height:12,
    backgroundColor:'#fff',
    position: "absolute",
    marginTop:-14,
    borderWidth:0.5,
    transform:[{ rotate: '45deg'}]
    
  }
})
