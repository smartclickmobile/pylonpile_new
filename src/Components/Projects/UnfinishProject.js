import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, FlatList,Dimensions, Alert, ToastAndroid,Image,Linking } from 'react-native'
import { Button, Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'
import { PileDetail } from '../../Controller/API'
import ERPButton from '../ERPButton'
import PileDetailRightButton from '../PileDetailRightButton'
import { MENU_GREY, MENU_ORANGE, MENU_GREEN, MENU_GREY_BORDER, MENU_ORANGE_BORDER, MENU_GREEN_BORDER, CATEGORY_1, CATEGORY_2, CATEGORY_3, STATUS_1, STATUS_2, STATUS_3,SUB_COLOR } from '../../Constants/Color'
import I18n from '../../../assets/languages/i18n'
import DeviceInfo from 'react-native-device-info'
import Svg,{
  Line,
} from 'react-native-svg'
import * as actions from "../../Actions"
import { connect } from "react-redux"
import {loading} from '../../Actions/types'
const sliderWidth = Dimensions.get("window").width
const itemWidth = sliderWidth * 0.11
class UnfinishProject extends Component {
  constructor(props){
    super(props)
    this.state = {
      tooltip1:false,
      tooltip2:false,
      tooltip3:false,
      tooltip4:false,
      tooltip5:false,
      tooltip6:false,
      tooltip7:false,
      tooltip8:false,
      tooltip9:false,
      tooltip10:false,
      tooltip11:false,
      index:0
    }
  }
  setStatusColor(category) {
    if (category == 0) {
      return {
        backgroundColor: STATUS_1,
        borderWidth: 0.2
      }
    }
    else if (category == 1) {
      return {
        backgroundColor: STATUS_2,
        borderWidth: 0.2
      }
    }
    else if (category == 2) {
      return {
        backgroundColor: STATUS_3,
        borderWidth: 0.2
      }
    }
  }

  setCategoryColor(category) {
    if (category == 1 || category == 4) {
      return {
        borderLeftColor: CATEGORY_1
      }
    }
    else if (category == 2) {
      return {
        borderLeftColor: CATEGORY_2
      }
    }
    else if (category == 3 || category == 5) {
      return {
        borderLeftColor: CATEGORY_3
      }
    }
  }

  _pileDetailIcon(data) {
    return (
      <PileDetailRightButton data={data} />
    )
  }

  _onPressButton(pile_id, pile_no,category,sp_parentid,pile_childs,icon,url) {
    Actions.mainpile({title: pile_no, pileid: pile_id, jobid: this.props.jobid, category:category,viewType:0, finish:false, sp_parentid:sp_parentid, pile_childs:pile_childs, icon_image:icon, url_dash:url})
  }

  _keyExtractor = (item, index) => index.toString()
  gettooltip(step,index){
    // console.warn('stepindex',step,index)
    var time = 1000
    if(step == 1){
      this.setState({tooltip1:true,tooltip2:false,tooltip3:false,tooltip4:false,tooltip5:false,tooltip6:false,tooltip7:false,tooltip8:false,tooltip9:false,tooltip10:false,tooltip11:false,index:index},()=>{
        setTimeout(() => 
        this.setState({tooltip1:false})
        ,time)
      })
    }else if(step == 2){
      this.setState({tooltip1:false,tooltip2:true,tooltip3:false,tooltip4:false,tooltip5:false,tooltip6:false,tooltip7:false,tooltip8:false,tooltip9:false,tooltip10:false,tooltip11:false,index:index})
      setTimeout(() => 
      this.setState({tooltip2:false})
      ,time)
    }else if(step == 3){
      this.setState({tooltip1:false,tooltip2:false,tooltip3:true,tooltip4:false,tooltip5:false,tooltip6:false,tooltip7:false,tooltip8:false,tooltip9:false,tooltip10:false,tooltip11:false,index:index})
      setTimeout(() => 
      this.setState({tooltip3:false})
      ,time)
    }else if(step == 4){
      this.setState({tooltip1:false,tooltip2:false,tooltip3:false,tooltip4:true,tooltip5:false,tooltip6:false,tooltip7:false,tooltip8:false,tooltip9:false,tooltip10:false,tooltip11:false,index:index})
      setTimeout(() => 
      this.setState({tooltip4:false})
      ,time)
    }else if(step == 5){
      this.setState({tooltip1:false,tooltip2:false,tooltip3:false,tooltip4:false,tooltip5:true,tooltip6:false,tooltip7:false,tooltip8:false,tooltip9:false,tooltip10:false,tooltip11:false,index:index})
      setTimeout(() => 
      this.setState({tooltip5:false})
      ,time)
    }else if(step == 6){
      this.setState({tooltip1:false,tooltip2:false,tooltip3:false,tooltip4:false,tooltip5:false,tooltip6:true,tooltip7:false,tooltip8:false,tooltip9:false,tooltip10:false,tooltip11:false,index:index})
      setTimeout(() => 
      this.setState({tooltip6:false})
      ,time)
    }else if(step == 7){
      this.setState({tooltip1:false,tooltip2:false,tooltip3:false,tooltip4:false,tooltip5:false,tooltip6:false,tooltip7:true,tooltip8:false,tooltip9:false,tooltip10:false,tooltip11:false,index:index})
      setTimeout(() => 
      this.setState({tooltip7:false})
      ,time)
    }else if(step == 8){
      this.setState({tooltip1:false,tooltip2:false,tooltip3:false,tooltip4:false,tooltip5:false,tooltip6:false,tooltip7:false,tooltip8:true,tooltip9:false,tooltip10:false,tooltip11:false,index:index})
      setTimeout(() => 
      this.setState({tooltip8:false})
      ,time)
    }else if(step == 9){
      this.setState({tooltip1:false,tooltip2:false,tooltip3:false,tooltip4:false,tooltip5:false,tooltip6:false,tooltip7:false,tooltip8:false,tooltip9:true,tooltip10:false,tooltip11:false,index:index})
      setTimeout(() => 
      this.setState({tooltip9:false})
      ,time)
    }else if(step == 10){
      this.setState({tooltip1:false,tooltip2:false,tooltip3:false,tooltip4:false,tooltip5:false,tooltip6:false,tooltip7:false,tooltip8:false,tooltip9:false,tooltip10:true,tooltip11:false,index:index})
      setTimeout(() => 
      this.setState({tooltip10:false})
      ,time)
    }else if(step == 11){
      this.setState({tooltip1:false,tooltip2:false,tooltip3:false,tooltip4:false,tooltip5:false,tooltip6:false,tooltip7:false,tooltip8:false,tooltip9:false,tooltip10:false,tooltip11:true,index:index})
      setTimeout(() => 
      this.setState({tooltip11:false})
      ,time)
    }
  }
  importERP(item){
    // console.warn('importERP',item.pile_id)
    // navigator.geolocation.watchPosition(async position => {
    //   let lat = position.coords.latitude
    //   let log = position.coords.longitude
    Alert.alert(I18n.t('projectlist.confrim'), I18n.t('projectlist.confrimtext'), [
      {
        text: "Cancel",
      },
      {
        text: "Ok",
        onPress: () => {
          var data = {
            Language:I18n.locale,
            JobId: this.props.jobid,
            PileId: item.pile_id,
            lat:this.props.lat,
            log:this.props.log
          }
          this.props.importtoerp(data)
          this.props.setloading1()
          setTimeout(() => {
            this.props.pileitemsclear({clearerp:true})
            this.props.pileItems({jobid: this.props.jobid,Language:this.props.Language,start:0,length:27,status:0 })
            this.props.pileItems({jobid: this.props.jobid,Language:this.props.Language,start:0,length:10,status:1 })
            this.props.pileItems({jobid: this.props.jobid,Language:this.props.Language,start:0,length:10,status:2 })
          }, 0)
        }
      }
    ])
     
    // })
    
    
  }
  opendash(item){
    Linking.openURL(item.Link)
    // Linking.canOpenURL(item.Link).then(supported => {
    //   if (supported) {
    //     Linking.openURL(item.Link)
    //   } else {
    //     console.log("Don't know how to open URI: " + item.Link)
    //   }
    // })
  }
  render() {
    const piles = this.props.array
    // console.log(piles, 'Inside unfinish');
    return (
      <View style={{marginTop: 10}}>
        {
          piles.map((item,index)=>{
            // console.warn('item pile_id',item.pile_id,item.sp_parentid)
            var flag = false
            if(item.pile_id != item.sp_parentid){
              flag = true
            }
            return(
              <View style={[styles.topic, this.setCategoryColor(item.category), index + 1 == this.props.array.length ? {borderBottomWidth: 1} : {}]} key={item.pile_id}>
      <View style={{marginLeft: 10, width: '100%',flexDirection:'row'}}>
        <TouchableOpacity style={{width: '63%'}} onPress={ () => this._onPressButton(item.pile_id, item.pile_no,item.category,item.sp_parentid,item.pile_childs,item.links.Icon,item.links.Link) }>
          <View style={{marginTop: 10}}>
            <Text style={styles.pileText} numberOfLines={1}>{item.pile_no}</Text>
          </View>
        </TouchableOpacity>
        {
          item.links != null?
          <TouchableOpacity style={{
            backgroundColor: SUB_COLOR,
          height: 30,
          width: 30,
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "row",
          marginTop:5,
          marginRight:5,
          marginLeft:item.importerp_flag == false ? 80:0,
          // borderRadius: 10
        }} onPress={()=>this.opendash(item.links)}>
              <Image 
                  style={{width:30,height: 30}}
                  source={{uri:item.links.Icon}}
               />
        </TouchableOpacity>
        :<View/>
        }
        {item.importerp_flag?<TouchableOpacity style={{backgroundColor: SUB_COLOR,
          height: 35,
          width: "20%",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "row",
          marginTop:5,
          borderRadius: 10}} onPress={()=>this.importERP(item)}>
              <Text style={{color:'#fff'}}>{I18n.t('projectlist.senterp')}</Text>
        </TouchableOpacity>:<View/>}
      </View>
        <View style={{flexDirection: 'row', marginTop: 10,marginLeft: 10}}>
        {this.state.tooltip1 && this.state.index==index?<View style={[styles.arrowtooltip,{marginLeft:10}]}/>:<View/>}
          {this.state.tooltip1 && this.state.index==index?<View style={[styles.Containertooltip]}>
            <Text style={{color:'#000'}}>{I18n.t('mainpile.step.1')}</Text>
          </View>:<View/>}
          
          <TouchableOpacity onPress={()=> this.gettooltip(1,index)}>
            <View style={[styles.button, this.setStatusColor(item.secound_status[0])]}><Text style={[styles.textnum]}>1</Text></View>
          </TouchableOpacity>
          {this.state.tooltip2 && this.state.index==index?<View style={[styles.arrowtooltip,{marginLeft:42}]}/>:<View/>}
          {this.state.tooltip2 && this.state.index==index?<View style={[styles.Containertooltip,{marginLeft:15}]}>
            <Text style={{color:'#000'}}>{I18n.t('mainpile.step.2')}</Text>
            </View>:<View/>}
          <TouchableOpacity onPress={()=> this.gettooltip(2,index)}>
            <View style={[styles.button, this.setStatusColor(item.secound_status[1])]}><Text style={[styles.textnum]}>2</Text></View>
          </TouchableOpacity>
          {this.state.tooltip3 && this.state.index==index?<View style={[styles.arrowtooltip,{marginLeft:78}]}/>:<View/>}
          {this.state.tooltip3 && this.state.index==index?<View style={[styles.Containertooltip,{marginLeft:20}]}>
            <Text style={{color:'#000'}}>{I18n.t('mainpile.step.3')}</Text>
            </View>:<View/>}
            {
            item.category==2 || item.category==3 ||item.category==5 ?
              <View style={{position:"absolute",marginLeft:sliderWidth-375.4,marginTop:2}}>
                <Svg
                    height={DeviceInfo.isTablet() ? 60 : 30}
                    width={itemWidth * 0.6}
                >
                    <Line
                        x1="0"
                        y1="0"
                        x2={itemWidth * 0.6}
                        y2={DeviceInfo.isTablet() ? 60 : 28}
                        stroke="red"
                        strokeWidth="2"
                    />
                    <Line
                    x1={itemWidth * 0.6}
                    y1="0"
                    x2="0"
                    y2={DeviceInfo.isTablet() ? 58 : 28}
                    stroke="red"
                    strokeWidth="2"
                />
                </Svg>
            </View>
            :
            <View/>
          }
          
          <TouchableOpacity onPress={()=> this.gettooltip(3,index)}>
            <View style={[styles.button, this.setStatusColor(item.secound_status[2])]} ><Text style={[styles.textnum]}>3</Text></View>
          </TouchableOpacity>

          {this.state.tooltip4 && this.state.index==index?<View style={[styles.arrowtooltip,{marginLeft:110}]}/>:<View/>}
          {this.state.tooltip4 && this.state.index==index?<View style={[styles.Containertooltip,{marginLeft:65}]}>
            <Text style={{color:'#000'}}>{I18n.t('mainpile.step.4')}</Text>
            </View>:<View/>}

            {((item.category==3||item.category==5)&&flag==true)&&<View style={{position:"absolute",marginLeft:sliderWidth-341,marginTop:2}}>
                <Svg
                    height={DeviceInfo.isTablet() ? 60 : 30}
                    width={itemWidth * 0.6}
                >
                    <Line
                        x1="0"
                        y1="0"
                        x2={itemWidth * 0.6}
                        y2={DeviceInfo.isTablet() ? 60 : 28}
                        stroke="red"
                        strokeWidth="2"
                    />
                    <Line
                    x1={itemWidth * 0.6}
                    y1="0"
                    x2="0"
                    y2={DeviceInfo.isTablet() ? 58 : 28}
                    stroke="red"
                    strokeWidth="2"
                />
                </Svg>
            </View>}

          <TouchableOpacity onPress={()=> this.gettooltip(4,index)}>
            <View style={[styles.button, this.setStatusColor(item.secound_status[3])]} ><Text style={[styles.textnum]}>4</Text></View>
          </TouchableOpacity>
          {this.state.tooltip5 && this.state.index==index?<View style={[styles.arrowtooltip,{marginLeft:145}]}/>:<View/>}
          {this.state.tooltip5 && this.state.index==index?<View style={[styles.Containertooltip,{marginLeft:110}]}>
            <Text style={{color:'#000'}}>{item.category==1?I18n.t('mainpile.step.5'):I18n.t('mainpile.step.5_dw')}</Text>
            </View>:<View/>}
            {
              item.category==4 ?
                <View style={{position:"absolute",marginLeft:sliderWidth-307,marginTop:2}}>
                  <Svg
                      height={DeviceInfo.isTablet() ? 60 : 30}
                      width={itemWidth * 0.6}
                  >
                      <Line
                          x1="0"
                          y1="0"
                          x2={itemWidth * 0.6}
                          y2={DeviceInfo.isTablet() ? 60 : 28}
                          stroke="red"
                          strokeWidth="2"
                      />
                      <Line
                      x1={itemWidth * 0.6}
                      y1="0"
                      x2="0"
                      y2={DeviceInfo.isTablet() ? 58 : 28}
                      stroke="red"
                      strokeWidth="2"
                  />
                  </Svg>
              </View>
              :
              <View/>
            }
          <TouchableOpacity onPress={()=> this.gettooltip(5,index)}>
            <View style={[styles.button, this.setStatusColor(item.secound_status[4])]} ><Text style={[styles.textnum]}>5</Text></View>
          </TouchableOpacity>
          {this.state.tooltip6 && this.state.index==index?<View style={[styles.arrowtooltip,{marginLeft:180}]}/>:<View/>}
          {this.state.tooltip6 && this.state.index==index?<View style={[styles.Containertooltip,{marginLeft:150}]}>
            <Text style={{color:'#000'}}>{I18n.t('mainpile.step.6')}</Text>
            </View>:<View/>}
          <TouchableOpacity onPress={()=> this.gettooltip(6,index)}>
            <View style={[styles.button, this.setStatusColor(item.secound_status[5])]} ><Text style={[styles.textnum]}>6</Text></View>
          </TouchableOpacity>
          {this.state.tooltip7 && this.state.index==index?<View style={[styles.arrowtooltip,{marginLeft:215}]}/>:<View/>}
          {this.state.tooltip7 && this.state.index==index?<View style={[styles.Containertooltip,{marginLeft:180}]}>
            <Text style={{color:'#000'}}>{I18n.t('mainpile.step.7')}</Text>
            </View>:<View/>}
            {
            item.category==2 || item.category==3 || item.category==4 ||(item.category==5 && item.shape ==2)?
              <View style={{position:"absolute",marginLeft:sliderWidth-238.4,marginTop:2}}>
                <Svg
                height={DeviceInfo.isTablet() ? 60 : 30}
                width={itemWidth * 0.6}
                >
                <Line
                    x1="0"
                    y1="0"
                    x2={itemWidth * 0.6}
                    y2={DeviceInfo.isTablet() ? 60 : 28}
                    stroke="red"
                    strokeWidth="2"
                />
                <Line
                x1={itemWidth * 0.6}
                y1="0"
                x2="0"
                y2={DeviceInfo.isTablet() ? 58 : 28}
                stroke="red"
                strokeWidth="2"
            />
            </Svg>
            </View>
            :
            <View/>
          }
        
        <TouchableOpacity onPress={()=> this.gettooltip(7,index)}>
          <View style={[styles.button, this.setStatusColor(item.secound_status[6])]} ><Text style={[styles.textnum]}>7</Text></View>
        </TouchableOpacity>
        {this.state.tooltip8 && this.state.index==index?<View style={[styles.arrowtooltip,{marginLeft:250}]}/>:<View/>}
        {this.state.tooltip8 && this.state.index==index?<View style={[styles.Containertooltip,{marginLeft:I18n.locale=='en'?70:185}]}>
            <Text style={{color:'#000'}}>{I18n.t('mainpile.step.8')}</Text>
            </View>:<View/>}
        <TouchableOpacity onPress={()=> this.gettooltip(8,index)}>  
          <View style={[styles.button, this.setStatusColor(item.secound_status[7])]} ><Text style={[styles.textnum]}>8</Text></View>
        </TouchableOpacity>
        {this.state.tooltip9 && this.state.index==index?<View style={[styles.arrowtooltip,{marginLeft:280}]}/>:<View/>}
        {this.state.tooltip9 && this.state.index==index?<View style={[styles.Containertooltip,{marginLeft:230}]}>
            <Text style={{color:'#000'}}>{I18n.t('mainpile.step.9')}</Text>
            </View>:<View/>}
        <TouchableOpacity onPress={()=> this.gettooltip(9,index)}>
          <View style={[styles.button, this.setStatusColor(item.secound_status[8])]} ><Text style={[styles.textnum]}>9</Text></View>
        </TouchableOpacity>
        {this.state.tooltip10 && this.state.index==index?<View style={[styles.arrowtooltip,{marginLeft:317}]}/>:<View/>}
        {this.state.tooltip10 && this.state.index==index?<View style={[styles.Containertooltip,{marginLeft:I18n.locale=='en'?250:280}]}>
            <Text style={{color:'#000'}}>{I18n.t('mainpile.step.10')}</Text>
            </View>:<View/>}
        <TouchableOpacity onPress={()=> this.gettooltip(10,index)}>
          <View style={[styles.button, this.setStatusColor(item.secound_status[9])]} ><Text style={[styles.textnum]}>10</Text></View>
        </TouchableOpacity>
        {this.state.tooltip11 && this.state.index==index?<View style={[styles.arrowtooltip,{marginLeft:350}]}/>:<View/>}
        {this.state.tooltip11 && this.state.index==index?<View style={[styles.Containertooltip,{marginLeft:I18n.locale=='en'?250:300}]}>
            <Text style={{color:'#000'}}>{I18n.t('mainpile.step.11')}</Text>
            </View>:<View/>}
        <TouchableOpacity onPress={()=> this.gettooltip(11,index)}>
          <View style={[styles.button, this.setStatusColor(item.secound_status[10])]} ><Text style={[styles.textnum]}>11</Text></View>
        </TouchableOpacity>
          {
            item.category==2 || item.category==3 ||item.category==5 ?
              <View style={{position:"absolute",marginLeft:sliderWidth-68.4,marginTop:2}}>
                <Svg
                height={DeviceInfo.isTablet() ? 60 : 30}
                width={itemWidth * 0.6}
                >
                <Line
                    x1="0"
                    y1="0"
                    x2={itemWidth * 0.6}
                    y2={DeviceInfo.isTablet() ? 60 : 28}
                    stroke="red"
                    strokeWidth="2"
                />
                <Line
                x1={itemWidth * 0.6}
                y1="0"
                x2="0"
                y2={DeviceInfo.isTablet() ? 58 : 28}
                stroke="red"
                strokeWidth="2"
            />
            </Svg>
            </View>
            :
            <View/>
          }
        
        </View>
      

      {/* <View style={{width: '30%'}}>
        <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
          <ERPButton />
        </View>
      </View> */}
    </View>
            )
          })
        }
      </View>
    )
  }
}
const mapStateToProps = state => {
  // console.log(state.pile)
  return{
    loading: state.pile.loading,
    loading1: state.pile.loading1
  }
    
}

export default connect(mapStateToProps, actions )(UnfinishProject)

const styles = StyleSheet.create({
  topic: {
    borderTopWidth: 1,
    // borderColor: '#D9D9D9',
    borderLeftWidth:5,
    borderLeftColor: 'blue',
    height: DeviceInfo.isTablet() ? 100 : 90,
    // height: DeviceInfo.isTablet() ? 100 : 100,
    // flexDirection: 'row',
    // paddingLeftWidth: 5
    // borderColor: 'blue'
  },
  button: {
    width: DeviceInfo.isTablet() ? 34 : 32,
    height: DeviceInfo.isTablet() ? 34 : 32,
    borderRadius: 5,
    borderWidth: 1,
    marginRight: 2,
    backgroundColor: '#FFF'
  },
  pileText: {
    color: '#000',
    fontSize: DeviceInfo.isTablet() ? 25 : 18
  },
  textnum : {
    fontSize:DeviceInfo.isTablet() ? 25 : 18,
    color: '#000',
    textAlign: 'center',
    flex:1,
    marginTop:2
  },
  Containertooltip:{
    position: "absolute",
    marginTop:-45,
    backgroundColor:'#fff',
    padding:8,
    borderWidth:0.5,
    borderRadius:5,
    // elevation:3
  },
  arrowtooltip:{
    width:12,
    height:12,
    backgroundColor:'#fff',
    position: "absolute",
    marginTop:-14,
    borderWidth:0.5,
    transform:[{ rotate: '45deg'}]
    
  }
})
