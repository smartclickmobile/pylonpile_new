import React, { Component } from "react"
import { StyleSheet, View, Text, TouchableOpacity } from "react-native"
import { Button, Icon } from "react-native-elements"
import { Actions } from "react-native-router-flux"
import I18n from "../../../assets/languages/i18n"
export default class Zone extends Component {
  render() {
    return (
      <View style={[styles.content,this.props.sub?{marginLeft:15}:{}]}>
        <View style={[styles.zone,this.props.name == ''?{backgroundColor:'#f88e01'}:{}]}>
          <View style={styles.textView}>
            <Text style={styles.text}>{this.props.name == ''?I18n.t('typezone.nottypezone'):''} {this.props.name==''?this.props.type:this.props.name}</Text>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  zone: {
    height: 40,
    width: "100%",
    backgroundColor: "#2196f3",
    justifyContent: "center"
  },
  textView: {
    marginLeft: 10,
    flexDirection: "row"
  },
  text: {
    color: "#FFF",
    fontSize: 17
  },
  content:{
    marginTop: 5
  }
})
