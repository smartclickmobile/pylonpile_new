import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Alert, FlatList } from 'react-native'
import { Button, Icon } from 'react-native-elements'
import { PileDetail } from '../../Controller/API'
import { Actions } from 'react-native-router-flux'
import PileDetailRightButton from '../PileDetailRightButton'
import I18n from '../../../assets/languages/i18n'
import { MAIN_COLOR, CATEGORY_1, CATEGORY_2, CATEGORY_3 } from '../../Constants/Color'
import DeviceInfo from 'react-native-device-info'

export default class NotStartProjectList extends Component {
  setCategoryColor(category) {
    if(category == 1 || category == 4) {
      return {
        borderLeftColor: CATEGORY_1
      }
    }
    else if(category == 2) {
      return {
        borderLeftColor: CATEGORY_2
      }
    }
    else if(category == 3 || category == 5) {
      return {
        borderLeftColor: CATEGORY_3
      }
    }
  }

  _pileDetailIcon(data) {
    return (
      <PileDetailRightButton data={data} />
    )
  }

  _onPressButton(pile_id, pile_no, category, sp_parentid, pile_childs, icon, url) {
    Actions.mainpile({ title: pile_no, pileid: pile_id, jobid: this.props.jobid, category: category,viewType:0, finish:false, sp_parentid: sp_parentid, pile_childs:pile_childs, icon_image:icon, url_dash:url})
  }

  _keyExtractor = (item, index) => index.toString()

  _renderItem = ({ item }) => (
    <TouchableOpacity style={[styles.topic, this.setCategoryColor(item.category)]} onPress={() => this._onPressButton(item.pile_id, item.pile_no, item.category, item.sp_parentid, item.pile_childs,item.links.Icon,item.links.Link)}>
      <View style={{ marginLeft: 10, alignItems: 'center', justifyContent: 'center', }}>
        <Text style={styles.textStyle}>{item.pile_no}</Text>
      </View>
    </TouchableOpacity>
  )

  render() {
    const arr = this.props.array
    return (
      <View style={{ marginTop: 10 }}>
        <FlatList
          data={arr}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          keyboardShouldPersistTaps='always'
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  topic: {
    borderTopWidth: 1,
    // borderColor: '#D9D9D9',
    borderLeftWidth: 5,
    borderLeftColor: 'blue',
    height: DeviceInfo.isTablet() ? 100 : 80,
    flexDirection: 'row'
  },
  textStyle: {
    color: '#000',
    fontSize: DeviceInfo.isTablet() ? 25 : 14
  },
})
