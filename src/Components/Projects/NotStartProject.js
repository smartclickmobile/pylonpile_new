import React, { Component } from "react"
import { StyleSheet, View, Text, TouchableOpacity, Alert, FlatList } from "react-native"
import AsyncStorage from '@react-native-community/async-storage';
import { Icon } from "react-native-elements"
import { Actions } from "react-native-router-flux"
import { PileDetail } from "../../Controller/API"
import PileDetailRightButton from "../PileDetailRightButton"
import I18n from "../../../assets/languages/i18n"
import { MAIN_COLOR, CATEGORY_1, CATEGORY_2, CATEGORY_3 } from "../../Constants/Color"
import DeviceInfo from 'react-native-device-info'

export default class NotStartProject extends Component {
  setCategoryColor(category) {
    if (category == 1 || category == 4) {
      return {
        borderLeftColor: CATEGORY_1
      }
    } else if (category == 2) {
      return {
        borderLeftColor: CATEGORY_2
      }
    } else if (category == 3 || category == 5) {
      return {
        borderLeftColor: CATEGORY_3
      }
    }
  }

  _pileDetailIcon(data) {
    return <PileDetailRightButton data={data} />
  }

  _onPressButton(pile_id, pile_no, category, sp_parentid, pile_childs, icon, url) {
    Actions.mainpile({ title: pile_no, pileid: pile_id, jobid: this.props.jobid, category: category,viewType:0, finish:false, sp_parentid: sp_parentid,pile_childs:pile_childs, icon_image:icon, url_dash:url})
  }

  render() {
    const data = this.props.value
    // console.log(piles, 'Inside unfinish');
    return (
      <View style={{ marginTop: 10 }}>
        <FlatList
          data={data}
          renderItem={this.renderItem}
          keyExtractor={this._keyExtractor}
          keyboardShouldPersistTaps="always"
        />
      </View>
    )
  }

  _keyExtractor = (item, index) => index.toString()

  renderItem = ({ item, index }) => {
    return (
      <View style={{ flexDirection: "row", padding: 10 }}>
        {item.length >= 1 && (
          <View style={styles.box}>
            <TouchableOpacity
              onPress={() => this._onPressButton(item[0].pile_id, item[0].pile_no, item[0].category, item[0].sp_parentid, item[0].pile_childs,item[0].links.Icon,item[0].links.Link)}
              style={[styles.button, this.setCategoryColor(item[0].category)]}
            >
              <View style={styles.text}>
                <Text style={styles.textStyle} numberOfLines={1}>{item[0].pile_no}</Text>
              </View>
            </TouchableOpacity>
          </View>
        )}
        {item.length >= 2 && (
          <View style={styles.box}>
            <TouchableOpacity
              onPress={() => this._onPressButton(item[1].pile_id, item[1].pile_no, item[1].category, item[1].sp_parentid, item[1].pile_childs,item[1].links.Icon,item[1].links.Link)}
              style={[styles.button, this.setCategoryColor(item[1].category)]}
            >
              <View style={styles.text}>
                <Text style={styles.textStyle} numberOfLines={1}>{item[1].pile_no}</Text>
              </View>
            </TouchableOpacity>
          </View>
        )}
        {item.length >= 3 && (
          <View style={styles.box}>
            <TouchableOpacity
              onPress={() => this._onPressButton(item[2].pile_id, item[2].pile_no, item[2].category, item[2].sp_parentid, item[2].pile_childs,item[2].links.Icon)}
              style={[styles.button, this.setCategoryColor(item[2].category)]}
            >
              <View style={styles.text}>
                <Text style={styles.textStyle} numberOfLines={1}>{item[2].pile_no}</Text>
              </View>
            </TouchableOpacity>
          </View>
        )}
        {item.length == undefined && (
          <View style={[styles.box,{width: "100%"}]}>
            <TouchableOpacity
              onPress={() => this._onPressButton(item.pile_id, item.pile_no, item.category, item.sp_parentid, item.pile_childs,item.links.Icon,item.links.Link)}
              style={[styles.button, this.setCategoryColor(item.category)]}
            >
              <View style={styles.text}>
                <Text style={styles.textStyle} numberOfLines={1}>{item.pile_no}</Text>
              </View>
            </TouchableOpacity>
          </View>
        )}
        
      </View>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    height: DeviceInfo.isTablet() ? 70 : 50,
    width: "90%",
    backgroundColor: "#d9d9db",
    borderLeftColor: "red",
    borderLeftWidth: 5,
    justifyContent: "center"
  },
  box: {
    width: "33.333%",
    alignItems: "center"
    // borderWidth: 1
  },
  textStyle: {
    color: '#000',
    fontSize: DeviceInfo.isTablet() ? 25 : 14
  },
  text: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  }
})
