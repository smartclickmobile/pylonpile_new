
import React, { Component, PropTypes } from 'react'
import {
  Alert,
  Image,
  Text,
  TouchableOpacity,
  View,
  ViewPropTypes
} from 'react-native'
// import FingerprintScanner from 'react-native-fingerprint-scanner'
import AsyncStorage from '@react-native-community/async-storage';
// import ShakingText from './ShakingText.component'
import styles from '../Piles/styles/Fingerprint.style'
class FingerprintPopup extends Component {

  constructor(props) {
    super(props)
    this.state = { errorMessage: undefined }
  }

  async componentDidMount() {
    var value = await this.getpass()
    // FingerprintScanner
    //   .authenticate({ onAttempt: this.handleAuthenticationAttempted })
    //   .then(() => {
        
    //     this.props.handlePopupDismissed(value[0][1],value[1][1],true)
        
    //   })
    //   .catch((error) => {
    //     this.setState({ errorMessage: error.message })
    //     Alert.alert("Error", error.message)
    //     // this.description.shake()
    //   })
  }
  async getpass(){
    try {
      const value = await AsyncStorage.multiGet([
        "user",
        "password",
        "Touch"
      ])
      return value
    } catch (error) {
      console.log(error)
    }
  }
  componentWillUnmount() {
    // FingerprintScanner.release()
  }

  handleAuthenticationAttempted = (error) => {
    this.setState({ errorMessage: error.message })
    Alert.alert("Error", error.message)
    // this.description.shake()
  }

  render() {
    const { errorMessage } = this.state
    const { style, handlePopupDismissed } = this.props

    return (
      <View style={styles.container}>
        <View style={[styles.contentContainer, style]}>

          <Image
            style={styles.logo}
            source={require('../../assets/image/finger_print.png')}
          />

          <Text style={styles.heading}>
            Fingerprint{'\n'}Authentication
          </Text>
          {/*<ShakingText
            ref={(instance) => { this.description = instance }}
            style={styles.description(!!errorMessage)}>
            {errorMessage || 'Scan your fingerprint on the\ndevice scanner to continue'}
          </ShakingText>*/}

          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={handlePopupDismissed}
          >
            <Text style={styles.buttonText}>
              BACK TO MAIN
            </Text>
          </TouchableOpacity>

        </View>
      </View>
    )
  }
}

// FingerprintPopup.propTypes = {
//   style: ViewPropTypes.style,
//   handlePopupDismissed: PropTypes.func.isRequired,
// }

export default FingerprintPopup