import React from 'react'
import { StyleSheet, View, Linking } from 'react-native'
import { Container, Content, Footer, FooterTab, Text, Button, Body, Left, Icon } from 'native-base'
import { MAIN_COLOR } from '../Constants/Color'
import StepIndicator from 'react-native-step-indicator'
import I18n from '../../assets/languages/i18n'
export default class PileFooter extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      step: 0
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.step !== this.props.step) {
      this.onCurrentPositionChanged(nextProps.currentPosition)
    }
  }

  render() {
    return (
      <View>
        <View style={{ flexDirection: 'column'}}>
          <View style={styles.first}>
            <Text>123</Text>
          </View>
          <StepIndicator
             stepCount={2}
             onPress={ (step) => this.setState({ step }) }
             currentPosition={this.state.step}
          />
          <View style={styles.second}>
            <View style={styles.firstButton}>
              <Text>{I18n.t('mainpile.button.delete1')}</Text>
            </View>
            <View>
              <Text>{I18n.t('mainpile.button.continue')}</Text>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  first: {
    height: 50,
    backgroundColor: MAIN_COLOR
  },
  second: {
    height: 50,
    backgroundColor: MAIN_COLOR,
    flexDirection: 'row'
  },
  firstButton: {
    width: '40%',
    backgroundColor: '#BABABA',
    justifyContent: 'center',
    alignItems: 'center'
  },
  secondButton: {

  },
  indicator: {

  }
})
