import React, { Component } from "react"
import { StyleSheet, View, ActivityIndicator, Alert, Linking, Text } from "react-native"
import { connect } from "react-redux"
import * as actions from "../Actions"
import { Actions } from "react-native-router-flux"
import firebase from "react-native-firebase"
import DeviceInfo from "react-native-device-info"
import RNFS from "react-native-fs"
import AsyncStorage from '@react-native-community/async-storage';
// import ApkInstaller from 'react-native-apk-installer'
import type { Notification, NotificationOpen, RemoteMessage } from 'react-native-firebase'

class Splash extends Component {
  constructor(props) {
    super(props)
    this.state = {
      progress: 0,
      finish1: false,
      finish2: false,
      debug: true
    }
  }
  async componentDidMount() {
    const DEBUG = false
    /** NO LONGER RELOGIN IN DEV MODE */
    // if(DEBUG && __DEV__) {
    //   const token = await AsyncStorage.getItem("token")
    //   if(token == null || token == "") {
    //     Actions.login({ type: "reset" })
    //     return
    //   }
    //   Actions.reset("drawer")
    //   return
    // }
    this.createNotificationChannel()
    // this.test()
    this.getVersion()

    this.notification()

    firebase.notifications().getInitialNotification()
      .then((notificationOpen: NotificationOpen) => {
        if(notificationOpen) {
          // App was opened by a notification
          // Get the action triggered by the notification being opened
          const action = notificationOpen.action
          // Get information about the notification that was opened
          const notification: Notification = notificationOpen.notification

          // console.log('action:', action)
          console.log(notification.smallIcon)
          console.log(notification.data)
          this.props.setNoti(notification.data)
        }
        else {
          // App open normally
          this.setState({ finish1: true }, () => this.toNextPage())
        }
      })

    firebase.notifications().onNotification((notification: Notification) => {
      // You've received a notification that hasn't been displayed by the OS
      // To display it whilst the app is in the foreground, simply call the following
      this.props.notiList()
      notification.android.setChannelId('alert')
      notification.android.setColor('#029CD1')
      console.log('--------------')
      console.log(notification)
      console.log('--------------')
      console.log(notification.smallIcon)
      console.log(notification.data)
      firebase.notifications().displayNotification(notification)
    })

    firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
      // Get the action triggered by the notification being opened
      // Get information about the notification that was opened
      if (Actions.currentScene == '_splash' || Actions.currentScene == '_login'
          || Actions.currentScene == 'splash' || Actions.currentScene == 'login')
      return
      const notification: Notification = notificationOpen.notification
      var id = notification.notificationId
      // console.warn(id)
      firebase.notifications().removeDeliveredNotification(id)
      const data = notification.data;
      // console.warn(data)
      if(data.type == 'approve') {
        Actions.notidetail({ alertid: data.alertid ,status:1})
        // Actions.reset("drawer")
        // Actions.notification({ fromnoti: true, alertid: data.alertid })

      }
      else if(data.type == 'redirectpile') {
        const noti = data
        var pileno = noti.pileno || ''
        var pileid = noti.pileid || 0
        var jobid = noti.jobid || 0
        var toProcess = noti.process || 1
        var category = noti.category || 1
        var sp_parentid = noti.sp_parentid
        var pile_childs = noti.pile_childs
        toProcess = toProcess - 1
        Actions.replace("drawer")
        Actions.mainpile({ title: pileno, pileid, jobid, toProcess, fromnoti: true,viewType:0, category: category, finish:false, sp_parentid:sp_parentid, pile_childs:pile_childs})
      }else if(data.type == 'acceptionprocess3'){
        Actions.notidetail({ alertid: data.alertid ,status:2})

      }


    })
  }

  notification() {
    if(!this.checkPermission()) {
      firebase.messaging().requestPermission()
        .then(() => {
          // User has authorised
        })
        .catch(error => {
          // User has rejected permissions
          return
        })
    }

    firebase.messaging().onMessage((message: RemoteMessage) => {
      console.log(message.data)
    })
  }

  checkPermission() {
    firebase.messaging().hasPermission()
      .then(enabled => {
        if(enabled) {
          return true
        } else {
          return false
        }
      })
  }

  createNotificationChannel() {
    // Build a channel
    const channel = new firebase.notifications.Android.Channel('alert', 'Alert', firebase.notifications.Android.Importance.Max)
      .setDescription('Alert')

    // Create the channel
    firebase.notifications().android.createChannel(channel)
  }

  getVersion() {
    const VERSION = DeviceInfo.getVersion()
    firebase
      .config()
      .fetch()
      .then(() => firebase.config().activateFetched())
      .then(() => firebase.config().getValues(["versiondev", "version", "apk", "apkdev", "activate", "versiontest", "apktest", "versiondemo", "apkdemo"]))
      .then(data => {
        if(data.activate.val()) {
          switch(DeviceInfo.getBundleId()) {
            case 'com.pylonpile':
              if(data.version.val() > VERSION) {
                Alert.alert("Update", "New update available version " + data.version.val(), [
                  {
                    text: "Cancel",
                    style: "cancel",
                    onPress: () => this.setState({ finish2: true }, () => this.toNextPage())
                  },
                  {
                    text: "Update",
                    onPress: () => {
                      this.download(data.apk.val(), data.version.val())
                    }
                  }
                ])
              }
              else {
                this.setState({ finish2: true }, () => this.toNextPage())
              }
              break
            case 'com.pylonpile.dev':
              if(data.versiondev.val() > VERSION) {
                Alert.alert("Update", "New update available version " + data.versiondev.val(), [
                  {
                    text: "Cancel",
                    style: "cancel",
                    onPress: () => this.setState({ finish2: true }, () => this.toNextPage())
                  },
                  {
                    text: "Update",
                    onPress: () => {
                      this.download(data.apkdev.val(), data.versiondev.val())
                    }
                  }
                ])
              }
              else {
                this.setState({ finish2: true }, () => this.toNextPage())
              }
              break
            case 'com.pylonpile.test':
              if(data.versiontest.val() > VERSION) {
                Alert.alert("Update", "New update available version " + data.versiontest.val(), [
                  {
                    text: "Cancel",
                    style: "cancel",
                    onPress: () => this.setState({ finish2: true }, () => this.toNextPage())
                  },
                  {
                    text: "Update",
                    onPress: () => {
                      this.download(data.apktest.val(), data.versiontest.val())
                    }
                  }
                ])
              }
              else {
                this.setState({ finish2: true }, () => this.toNextPage())
              }
              break
            case 'com.pylonpile.demo':
              if(data.versiondemo.val() > VERSION) {
                Alert.alert("Update", "New update available version " + data.versiondemo.val(), [
                  {
                    text: "Cancel",
                    style: "cancel",
                    onPress: () => this.setState({ finish2: true }, () => this.toNextPage())
                  },
                  {
                    text: "Update",
                    onPress: () => {
                      this.download(data.apkdemo.val(), data.versiondemo.val())
                    }
                  }
                ])
              }
              else {
                this.setState({ finish2: true }, () => this.toNextPage())
              }
              break
            default:
              this.setState({ finish2: true }, () => this.toNextPage())
              break
          }
        }
        else {
          this.setState({ finish2: true }, () => this.toNextPage())
        }
      })
      .catch(error => console.log(`Error processing config: ${error}`))
  }

  download(url, version) {
    var apk = 'pylon' + version + '.apk'
    var downloadprogress = response => {
      var percentage = Math.floor((response.bytesWritten / response.contentLength) * 100)
      this.setState({ progress: percentage })
    }
    // var filepath = RNFS.DocumentDirectoryPath + '/pylon.apk'
    RNFS.mkdir(RNFS.ExternalStorageDirectoryPath + '/Pylon/Updates/')
    .then( () => {
      let filepath = RNFS.ExternalStorageDirectoryPath + '/Pylon/Updates/' + apk
      RNFS.downloadFile({
        fromUrl: url,
        toFile: filepath,
        progress: downloadprogress
      })
        .promise.then(result => {
          // ApkInstaller.install(filepath)
          console.log('download finish')
        })
        .catch(err => console.log(err))
    })
  }

  // Call props authlogout
  async toNextPage() {
    console.log('wow')
    if(this.state.finish1 && this.state.finish2){
      if(this.state.debug && __DEV__) {
        const token = await AsyncStorage.getItem("token")
        if(token == null || token == "") {
          Actions.login({ type: "reset" })
          return
        }
        Actions.reset("drawer")
        return
      }
      else {
        // Actions.login({ type: "reset" })
        this.props.authLogout()
      }
    }
  }

  // toNextPage() {

  // }

  render() {
    return (
      <View style={styles.spinnerStyle}>
        <ActivityIndicator size={"large"} />
        {this.state.progress > 0 &&
          <View>
            <Text>Downloading ...</Text>
            <Text>{this.state.progress}%</Text>
          </View>
        }

      </View>
    )
  }
}

const styles = {
  spinnerStyle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
}

const mapStateToProps = state => {
  return {
    noti: state.noti.notidata
  }
}

export default connect(mapStateToProps, actions)(Splash)
