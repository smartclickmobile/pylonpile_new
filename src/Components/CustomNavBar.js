import { Image, Platform, StyleSheet, Text, TouchableOpacity, View, TextInput, Keyboard } from 'react-native'
import React, { Component } from 'react'
import { Actions } from 'react-native-router-flux'
import SideBar from './SideBar'
import I18n from "../../assets/languages/i18n"
// import SearchBar from 'react-native-searchbar'
import { SearchBar } from 'react-native-elements'
import * as actions from "../Actions"
import { connect } from "react-redux"
const styles = StyleSheet.create({})

export class CustomNavBar extends Component {

  constructor(props) {
    super(props)
    this.state = {
      result: ''
    }
  }
  componentDidMount(){
    // console.warn('componentDidMount')
    if(Actions.currentScene == '_joblist'){
      this.setState({ result: '' })
      Actions.refresh({ result: '' })
    }
  }

   _handleResults = async (input) => {
    // this.setState({ result: input })
    // // console.log(input);
    // Actions.refresh({ result: input })
    if(Actions.currentScene == '_joblist'){
      this.setState({ result: input })
      Actions.refresh({ result: input })
    }else{
      // if(input == ''){
      //   await this.props.pileitemsclear()
      //   // this.setState({ result: input },()=>{console.warn('save',this.state.result)})
      //   this.props.pileItems({jobid: this.props.idjob,Language:I18n.currentLocale(),start:0,length:27,status:0 ,load:true})
      //   this.props.pileItems({jobid: this.props.idjob,Language:I18n.currentLocale(),start:0,length:10,status:1 ,load:true})
      //   this.props.pileItems({jobid: this.props.idjob,Language:I18n.currentLocale(),start:0,length:10,status:2 ,load:true})
      //   Actions.refresh()
      // }else{
        this.setState({ result: input })
        // await this.props.sreachpileno({jobid: this.props.idjob,Language:I18n.currentLocale(),pileno:input })
        // Actions.refresh()
      // }
    }
    // this.props.sreachpileno({jobid: this.props.idjob,Language:I18n.currentLocale(),pileno:input })
  }

  async searchpileno(){
    // console.warn('searchpileno func',this.state.result)
    if(Actions.currentScene == '_joblist'){

    }else{
      await this.props.pileitemsclear({clearerp:false})
      if(this.state.result==''){
        
        if(this.props.items == null){
          // console.warn('items',this.props.items)
          // this.props.pileItems({jobid: this.props.idjob,Language:I18n.currentLocale(),start:0,length:27,status:0 ,load:true})
          // this.props.pileItems({jobid: this.props.idjob,Language:I18n.currentLocale(),start:0,length:10,status:1 ,load:true})
          // this.props.pileItems({jobid: this.props.idjob,Language:I18n.currentLocale(),start:0,length:10,status:2 ,load:true})
          await this.props.sreachpileno({jobid: this.props.idjob,Language:I18n.currentLocale(),pileno:this.state.result,length0:27,length1:10,length2:10,input1:this.state.result})
          Actions.refresh()
        }
        
      }else{
        // console.warn('re have',this.props.idjob,I18n.currentLocale(),this.state.result)
        await this.props.sreachpileno({jobid: this.props.idjob,Language:I18n.currentLocale(),pileno:this.state.result,input1:this.state.result})
        Actions.refresh()
      }
    }

    
     
  }
  _handleCancel = (input) => {
    console.log('cancel search')
    Actions.refresh({ result: '', navBar: '', searching: false })
    this.searchBar.hide()
  }

  _onBlur = () => {
    console.log('blur')
    // this.setState({ result: '' });
    Actions.refresh({ result: '', navBar: '', searching: false })
    this.searchBar.hide()
  }

  onFocus = () => {
    // Actions.refresh({ searching: true, result: '' })
  }

  render() {
    // console.warn('items',this.props.items)
    return (
      <View style={{height: '100%',marginTop:-2.7}}>
        <SearchBar
        ref={(ref) => this.searchBar = ref}
        lightTheme
        searchIcon={{ size: 24 }}
        onChangeText={this._handleResults}
        placeholder='Search'
        containerStyle={{backgroundColor:this.props.color,borderColor:this.props.color}}
        inputStyle={{backgroundColor:'#fff'}}
        onEndEditing={()=>this.searchpileno()}
        />
        </View>
    )
  }
}
const mapStateToProps = state => {
  // console.log(state.pile)
  return {
    error: state.pile.error,
    items: state.pile.items,
    idjob: state.pile.jobid,
    
  }
}
export default connect(mapStateToProps, actions)(CustomNavBar)
