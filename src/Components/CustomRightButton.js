import { View } from 'react-native'
import React, { Component } from 'react'
import { Icon } from 'react-native-elements'

export default class CustomRightButton extends Component {


  render() {

    return (
      <Icon name="search" type="EvilIcons" />
    )
  }
}
