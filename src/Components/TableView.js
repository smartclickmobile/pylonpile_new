import React, { Component } from "react"
import { View, StyleSheet, Text } from "react-native"
import { SUB_COLOR, DEFAULT_COLOR_6 } from "../Constants/Color"

export default class TableView extends Component {
  render() {
    const value = this.props.value
    if (value.length == 5) {
      return (
        <View style={[styles.table, this.props.last ? { borderBottomWidth: 0.5 } : {}]}>
        <View style={[styles.tableColumn5, this.props.head ?  (this.props.category==3|| this.props.category == 5?{backgroundColor: DEFAULT_COLOR_6}:this.props.shape == 1 ? {backgroundColor: "#daeef8"}:{backgroundColor: "#cc6600"}):{}]}>
          <Text style={styles.text}>{value[0]}</Text>
        </View>
        <View style={[styles.tableColumn5, this.props.head ?  (this.props.category==3|| this.props.category == 5?{backgroundColor: DEFAULT_COLOR_6}:this.props.shape == 1 ? {backgroundColor: "#daeef8"}:{backgroundColor: "#cc6600"}):{}]}>
          <Text style={styles.text}>{value[1]}</Text>
        </View>
        <View style={[styles.tableColumn5, this.props.head ?  (this.props.category==3|| this.props.category == 5?{backgroundColor: DEFAULT_COLOR_6}:this.props.shape == 1 ? {backgroundColor: "#daeef8"}:{backgroundColor: "#cc6600"}):{}]}>
          <Text style={styles.text}>{value[2]}</Text>
        </View>
        <View style={[styles.tableColumn5, this.props.head ?  (this.props.category==3|| this.props.category == 5?{backgroundColor: DEFAULT_COLOR_6}:this.props.shape == 1 ? {backgroundColor: "#daeef8"}:{backgroundColor: "#cc6600"}):{}]}>
          <Text style={styles.text}>{value[3]}</Text>
        </View>
        <View style={[styles.tableColumn5, this.props.head ?  (this.props.category==3|| this.props.category == 5?{backgroundColor: DEFAULT_COLOR_6}:this.props.shape == 1 ? {backgroundColor: "#daeef8"}:{backgroundColor: "#cc6600"}):{}]}>
          <Text style={[styles.text, this.props.red ? { color: 'red' } : {}]}>{value[4]}</Text>
        </View>
      </View>
      )
    }
    return (
      <View style={[styles.table, this.props.last ? { borderBottomWidth: 0.5 } : {}]}>
        <View style={[styles.tableColumn, this.props.head ?  (this.props.category==3|| this.props.category == 5?{backgroundColor: DEFAULT_COLOR_6}:this.props.shape == 1 ? {backgroundColor: "#daeef8"}:{backgroundColor: "#cc6600"}):{}]}>
          <Text style={styles.text}>{value[0]}</Text>
        </View>
        <View style={[styles.tableColumn, this.props.head ?  (this.props.category==3|| this.props.category == 5?{backgroundColor: DEFAULT_COLOR_6}:this.props.shape == 1 ? {backgroundColor: "#daeef8"}:{backgroundColor: "#cc6600"}):{}]}>
          <Text style={styles.text}>{value[1]}</Text>
        </View>
        <View style={[styles.tableColumn, this.props.head ?  (this.props.category==3|| this.props.category == 5?{backgroundColor: DEFAULT_COLOR_6}:this.props.shape == 1 ?{backgroundColor: "#daeef8"}:{backgroundColor: "#cc6600"}):{}]}>
          <Text style={styles.text}>{value[2]}</Text>
        </View>
        <View style={[styles.tableColumn, this.props.head ?  (this.props.category==3|| this.props.category == 5?{backgroundColor: DEFAULT_COLOR_6}:this.props.shape == 1 ?{backgroundColor: "#daeef8"}:{backgroundColor: "#cc6600"}):{}]}>
          <Text style={[styles.text, this.props.red ? { color: 'red' } : {}]}>{value[3]}</Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  table: {
    flexDirection: "row",
    height: "auto",
    minHeight: 40,
    borderTopWidth: 0.5
  },
  tableColumn: {
    width: "25%",
    alignItems: "center",
    justifyContent: "center",
    borderRightWidth: 0.5
  },
  tableColumn5: {
    width: "20%",
    alignItems: "center",
    justifyContent: "center",
    borderRightWidth: 0.5
  },
  text: {
    textAlignVertical: "center",
    textAlign: "center"
    // color: 'black'
  }
})
