import React, { Component } from "react"
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Alert,
  Image,
  Linking
} from "react-native"
import { Icon } from "react-native-elements"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "../Actions"
import DeviceInfo from "react-native-device-info"
import Icons from "react-native-vector-icons/FontAwesome"
import I18n from "../../assets/languages/i18n"
import { SUB_COLOR} from '../Constants/Color'

class PileDetailRightButton extends Component {
  constructor(props) {
    super(props)
    this.state = {
      viewType: true,
      // note: null
      // process: [],
      // PileDetail: null
      // stack_item:null,
      // pileAll:[],
      waitApprove:false,
    }
   
  }

  refresh(data) {

    console.log("up to ref")
    var process = data.process + 1
    if (process == 9) {
      this.props.getConcreteList({
        pileid: data.pileid,
        jobid: data.jobid
      })
      // return
    }
    if (process == 5) {
      this.props.getDrilling({
        pileid: data.pileid,
        jobid: data.jobid,
        Language: I18n.locale
      })
      this.props.getDrillingChild({
        pileid: data.pileid,
        jobid: data.jobid,
        Language: I18n.locale
      })
    }
    if(process == 3){
      if(this.props.data.state.pile.shape == 1){
        var temp = this.props.data.state.stack
        if(temp[temp.length-1].step == 4 && this.props.data.state.stepStatus.Step3Status != 2 ){
          this.props.setStack({
            process: 3,
            step: 3
          })
        }
      }
      
    }
    if(process == 6){
      this.props.setStack({
        process: 6,
        step: 1
      })
    }
    if (process == 4) {
      this.props.getDrillingFluids({
        pileid: data.pileid,
        jobid: data.jobid,
        Language: I18n.locale
      })
    }
    if (process == 8) {
      this.props.pileValueInfo({ jobid: data.jobid, pileid: data.pileid })
      this.props.getTremieList({ pileid: data.pileid, jobid: data.jobid })
      // return
    }
    if (process == 10) {
      this.props.pileValueInfo({ jobid: data.jobid, pileid: data.pileid })
      this.props.getConcreteRecord({
        pileid: data.pileid,
        jobid: data.jobid
      })
      // return
    }
    // Actions.refreshPage(data)
    this.props.clearStatus()
    this.props.pileClear()
    this.props.ungetStepStatus2()
    if (this.state.viewType == false) {
      if(this.props.sp_parentid != data.pileid && process == 5 && this.props.category == 5){
        this.props.mainRefresh(0)
      }else{
        this.props.mainRefresh(1)
      }
    } else {
      if(this.props.sp_parentid != data.pileid && process == 5 && this.props.category == 5){
        this.props.mainRefresh(0)
      }else{
        this.props.mainRefresh(0)
      }
    }
  }

  opendash(item){
    Linking.openURL(item)
    // Linking.canOpenURL(item).then(supported => {
    //   if (supported) {
    //     Linking.openURL(item)
    //   } else {
    //     console.log("Don't know how to open URI: " + item)
    //   }
    // })
  }

  render() {
    var links = []
    var data = this.props.data.state.pile
    if(data.links != undefined){
      links = data.links
    }
    // console.warn('Icon image',this.props.icon_image)
    var child_category_5 = false
    if(this.props.sp_parentid != this.props.data.state.pile.pile_id && this.props.data.state.index + 1 == 5 && this.props.category == 5){
      child_category_5 = true
    }
    return (
      
      <View style={styles.navigationTool}>
        {Actions.currentScene == "_mainpile" ? (
          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                if(this.props.finish){
                  Alert.alert('', I18n.t('mainpile.alert.finishmode'), [
                    {
                      text: 'OK'
                    }
                  ])
                }else{
                  console.warn('test look',
                  this.props.sp_parentid,
                  this.props.data.state.pile.pile_id,
                  this.props.data.state.index + 1,
                  this.props.category
                  )
                  if(this.props.sp_parentid != this.props.data.state.pile.pile_id && this.props.data.state.index + 1 == 5 && this.props.category == 5){
                    Alert.alert('',I18n.t("mainpile.lockprocess.error12"))
                  }else{
                    Alert.alert(
                      "",
                      this.state.viewType == false
                        ? I18n.t("alert.changeview")
                        : I18n.t("alert.changeedit"),
                      [
                        { text: "Cancel" },
                        {
                          text: "OK",
                          onPress: () => {
                            this.setState({ viewType: !this.state.viewType })
                            this.refresh({
                              jobid: this.props.data.state.pile.job_id,
                              pileid: this.props.data.state.pile.pile_id,
                              process: this.props.data.state.index
                            })
                          }
                        }
                      ],
                      { cancelable: false }
                    )
                  }
                  
                }
                
              }}
              style={styles.iconRefresh}
            >
              {this.state.viewType == false && child_category_5 == false? (
                <Icons
                  name="edit"
                  type="Entypo"
                  color="#FFF"
                  size={DeviceInfo.isTablet() ? 50 : 26}
                />
              ) : (
                <Icons
                  name="eye"
                  type="Entypo"
                  color="#FFF"
                  size={DeviceInfo.isTablet() ? 50 : 26}
                />
              )}
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                this.refresh({
                  jobid: this.props.data.state.pile.job_id,
                  pileid: this.props.data.state.pile.pile_id,
                  process: this.props.data.state.index
                })
              }}
              style={styles.iconRefresh}
            >
              <Icon
                name="refresh"
                type="FontAwesome"
                color="#FFF"
                size={DeviceInfo.isTablet() ? 50 : 26}
              />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
               

                
                Actions.note({
                  jobid: this.props.data.state.pile.job_id,
                  pileid: this.props.data.state.pile.pile_id,
                  process: this.props.data.state.index + 1,
                  shape: this.props.data.state.pile.shape,
                  category: this.props.category,
                  viewType:this.state.viewType,
                  title:I18n.t('drawer.note')
                })
              
              }}
              style={styles.iconRefresh}
            >
              
                <Icon
                name="sticky-note"
                type="font-awesome"
                color= "#FFF"
                size={DeviceInfo.isTablet() ? 50 : 26}
              />
                
              
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                Actions.history({
                  jobid: this.props.data.state.pile.job_id,
                  pileid: this.props.data.state.pile.pile_id,
                  index: this.props.data.state.index,
                  shape: this.props.data.state.pile.shape,
                  category: this.props.category,
                  title:I18n.t('drawer.history')
                })
              }}
              style={styles.iconRefresh}
            >
              <Icon
                name="list-ol"
                type="font-awesome"
                color="#FFF"
                size={DeviceInfo.isTablet() ? 50 : 26}
              />
            </TouchableOpacity>
          </View>
        ) : (
          <View />
        )}
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() =>
            Actions.piledetail({
              title: I18n.t("piledetail.piledetail"),
              shape: this.props.data.state.pile.shape,
              category: this.props.category,
              // Language: I18n.locale
            })
          }
        >
          <Icon
            name="file-text"
            type="font-awesome"
            color="#FFF"
            size={DeviceInfo.isTablet() ? 50 : 26}
          />
        </TouchableOpacity>
       {Actions.currentScene == "_mainpile" &&<TouchableOpacity
          style={{
            // backgroundColor: SUB_COLOR,
            height: 30,
            width: 30,
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "row",
            marginLeft:8,
          }}
          activeOpacity={0.8}
          onPress={() =>
            {
              this.opendash(this.props.url_dash)
            }
          }
        >
          <Image 
              style={{width:26,height: 26}}
              source={require('../../assets/image/Pylon-App-Icon-report2-500-3.png')}
            />
        </TouchableOpacity>}
      </View>
    )
  }
}



const styles = StyleSheet.create({
  navigationTool: {
    flexDirection: "row",
    marginRight: 10
  },
  iconRefresh: {
    marginRight: 10,
    // backgroundColor:"red"
  }
})

export default connect(null, actions)(PileDetailRightButton)
