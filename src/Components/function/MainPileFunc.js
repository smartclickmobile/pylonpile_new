import React, {Component} from 'react'
import AsyncStorage from '@react-native-community/async-storage';

export class MainPileFunc extends Component {
  static async getStorage(pile_id, index='') {
    const pileData = await AsyncStorage.getItem("pileData")
    const pile = JSON.parse(pileData) || JSON.stringify([])

    if(index!='') {
      return pile[pile_id][index]
    }else{
      return pile[pile_id]
    }
  }
}
