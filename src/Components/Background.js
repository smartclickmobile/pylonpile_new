import React, { Component } from 'react';
import { StyleSheet, View, Image, Dimensions, ImageBackground, Text } from 'react-native';
import { Container, Content } from 'native-base';

const window = Dimensions.get('window')
const height = window.height || window.height
const width = window.width

export default class Background extends Component {

  render() {
    let { height, width } = Dimensions.get('window');
    return (
      <ImageBackground source={this.props.source} style={styles.background}>
          {this.props.children}
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    opacity: 0.01,
    resizeMode: 'cover'
  },
  temp: {
    resizeMode: 'stretch',
    width: window.width,
    height: window.height
  },
  background: {
    flex: 1,
    justifyContent: 'center',
    width: window.width
  }
});
