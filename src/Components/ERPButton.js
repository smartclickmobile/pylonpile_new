import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableOpacity,Alert} from 'react-native'
import { Button, Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'
import { MAIN_COLOR } from '../Constants/Color'
import * as actions from "../Actions"
import { connect } from "react-redux"
import I18n from '../../assets/languages/i18n'
class ERPButton extends Component {
  modifydata(item){
    // console.warn('modifydata',this.props.jobid)
    // navigator.geolocation.watchPosition(async position => {
    //   let lat = position.coords.latitude
    //   let log = position.coords.longitude
    // })
    Alert.alert(I18n.t('projectlist.confrim'), I18n.t('projectlist.confrimtext1'), [
      {
        text: "Cancel",
      },
      {
        text: "Ok",
        onPress: () => {
          var data = {
            Language:I18n.locale,
            JobId: this.props.jobid,
            PileId: item.pile_id,
            lat:this.props.lat,
            log:this.props.log,
            pile_no:this.props.pile_no,
            category:this.props.category
          }
            this.props.unblockerp(data)
            this.props.setloading1()
            
            setTimeout(() => {
              this.props.pileitemsclear({clearerp:true})
              this.props.pileItems({jobid: this.props.jobid,Language:this.props.Language,start:0,length:27,status:0 })
              this.props.pileItems({jobid: this.props.jobid,Language:this.props.Language,start:0,length:10,status:1 })
              this.props.pileItems({jobid: this.props.jobid,Language:this.props.Language,start:0,length:10,status:2 })
            }, 5000)
        }
      }
    ])
    
  }
  render() {
    const active = this.props.active
    return (
      <View style={{marginLeft: 5}}>
        <TouchableOpacity style={styles.button} onPress={()=> this.modifydata(this.props.data)}>
          <Text style={{ color: 'white' }}>{I18n.t('projectlist.editerp')}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
const mapStateToProps = state => {
  // console.log(state.pile)
  return{
    loading: state.pile.loading
  }
    
}
export default connect(mapStateToProps, actions )(ERPButton)
const styles = StyleSheet.create({
  button: {
    height: 40,
    width: 100,
    backgroundColor: '#ee5253',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  }
})
