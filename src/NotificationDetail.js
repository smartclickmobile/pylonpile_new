import React, { Component } from "react"
import { View, Text, StyleSheet, TouchableOpacity, TextInput, Alert,PermissionsAndroid } from "react-native"
import { MAIN_COLOR, SUB_COLOR, MENU_GREY_ITEM, MENU_GREY_BORDER, MENU_BACKGROUND } from "./Constants/Color"
import { Container, Content } from "native-base"
import ModalSelector from "react-native-modal-selector"
import { Icon } from "react-native-elements"
import I18n from "../assets/languages/i18n"
import { Extra } from "./Controller/API"
import TableView from "./Components/TableView"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import * as actions from "./Actions"
import FusedLocation from 'react-native-fused-location'
class NotificationDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      latitude: "",
      longitude: ""
    }
  }
  _renderTopic() {
    if(this.props.processid == 9){
      return (
        <View style={styles.topic}>
          <Text style={styles.topicText}>{this.props.status==1|| this.props.status==15?I18n.t('notificationdetail.approvelist'):I18n.t('notificationdetail.approvelist1')}</Text>
        </View>
      )
    }else{
      return (
        <View style={styles.topic}>
          <Text style={styles.topicText}>{this.props.status==1|| this.props.status==5?I18n.t('notificationdetail.approvelist'):I18n.t('notificationdetail.approvelist1')}</Text>
        </View>
      )
    }
    
  }

  _xButton = () => {
    return (
      <View style={{ marginRight: 5 }}>
        <Icon
          name="close"
          type="evilicons"
          color="#FFF"
          onPress={() => this.props.authLogout()}
          style={{ marginRight: 10 }}
        />
      </View>
    )
  }

  componentWillMount() {
    // console.warn(this.props.status)
    this.getLocation()
    if (this.props.noti) {
      Actions.refresh({ right: this._xButton })
      return
    }
    this.props.notiDetail(this.props.alertid)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error == null) {
      this.setState({ data: nextProps.data })
      if(nextProps.data.langth > 0){
        Actions.refresh({ shape: nextProps.data.PileValue.Shape })
      }
    }
  }

  async getLocation() {
    // navigator.geolocation.getCurrentPosition(
    //   position => {
    //     this.setState({ latitude: position.coords.latitude, longitude: position.coords.longitude })
    //   },
    //   error => {
    //     console.log("error: ", error.message)
    //   },
    //   { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    // )
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
          title: 'App needs to access your location',
          message: 'App needs access to your location ' +
          'so we can let our app be even more awesome.'
          }
      )
      if (granted) {
        FusedLocation.setLocationPriority(FusedLocation.Constants.HIGH_ACCURACY)
        const location = await FusedLocation.getFusedLocation()
        this.props.setlocation(location)
        if(location == undefined){
          this.setState({ latitude: this.props.location_lat, longitude: this.props.location_log })
        }else{
          this.setState({ latitude: location.latitude, longitude: location.longitude })
        }
       
        // console.warn('FusedLocation',location.latitude,location.longitude)
        // navigator.geolocation.getCurrentPosition(
        //   position => {
        //     console.warn('geolocation',position.coords)
        //     this.setState({ latitude: position.coords.latitude, longitude: position.coords.longitude })
    
        //   },
        //   error => {
        //     console.warn("error: ", error.message)
        //     this.setState({ loc: false })
        //   },
        //   { timeout: 20000,  maximumAge: 1000 }
        // )
      }else{
        // console.warn('granted')
      }
  }

  _renderBasicInformation() {
    const pilevalue = this.state.data.PileValue || []
    const job = this.state.data.JobDetail || []
    return (
      <View style={{ marginTop: 15 }}>
        <View style={styles.topicBox}>
          <Text style={styles.subTopic}>{I18n.t('notificationdetail.information')}</Text>
        </View>
        <View style={{ marginTop: 5 }}>
          <BoxView left={I18n.t('notificationdetail.projectName')} right={`${job.job_no} ${job.job_name}`} />
          <BoxView left={I18n.t('notificationdetail.pileNo')} right={this.state.data.PileNo} />
          <BoxView left={I18n.t('notificationdetail.diameter')} right={pilevalue.size} />
          <BoxView left={I18n.t('notificationdetail.cutoff')} right={parseFloat(pilevalue.cutoff).toFixed(3)} />
          <BoxView left={I18n.t('notificationdetail.piletip')} right={parseFloat(pilevalue.piletip).toFixed(3)} last={!pilevalue.tgw} />
          {pilevalue.tgw && <BoxView left="Top Guild Wall" right={parseFloat(pilevalue.tgw).toFixed(3)} last />}
        </View>
      </View>
    )
  }

  _renderMeasurePosition() {
    const pilevalue = this.state.data.PileValue || []
    const surveyorObj = pilevalue.PileSurvey_Surveyor || []
    const name = surveyorObj.firstname
      ? surveyorObj.nickname
        ? surveyorObj.nickname + "-" + surveyorObj.firstname + " " + surveyorObj.lastname
        : surveyorObj.firstname + " " + surveyorObj.lastname
      : "-"
    const locObj = pilevalue.PileSurvey_Location || []
    const location = locObj.utm_n ? locObj.utm_n.toFixed(3) + ", " + locObj.utm_e.toFixed(3) : "-"
    const bsObj = pilevalue.PileSurvey_BSFlagLocation || []
    const bs = bsObj.utm_n ? bsObj.utm_n.toFixed(3) + ", " + bsObj.utm_e.toFixed(3) : "-"
    return (
      <View style={{ marginTop: 15 }}>
        <View style={styles.topicBox}>
          <Text style={styles.subTopic}>{I18n.t('notificationdetail.informations')}</Text>
        </View>
        <View style={{ marginTop: 5 }}>
          <BoxView left={I18n.t('notificationdetail.surveyor')} right={name} />
          <BoxView left={I18n.t('notificationdetail.station')} right={locObj.no + "  " + location} />
          <BoxView left={I18n.t('notificationdetail.stationES')} right={bsObj.no + "  " + bs} />
          <BoxView left="Resection" right={pilevalue.PileSurvey_IsResection ? "Yes" : "No"} last />
        </View>
      </View>
    )
  }

  _renderCasingPosition() {
    const pilevalue = this.state.data.PileValue || []
    var topcasing = pilevalue.PileSurvey_TopCasingLevel || 0
    topcasing = topcasing.toFixed(3)
    var ground = pilevalue.PileSurvey_GroundLevel || 0
    ground = ground.toFixed(3)
    return (
      <View style={{ marginTop: 15 }}>
        <View style={styles.topicBox}>
          <Text style={styles.subTopic}>{pilevalue.Shape == 1 ? I18n.t('notificationdetail.Pro_casing') : I18n.t('notificationdetail.Pro_topdwall')}</Text>
        </View>
        <View style={{ marginTop: 5 }}>
          <BoxView left={pilevalue.Shape == 1 ? I18n.t('notificationdetail.top') : I18n.t('notificationdetail.top_dwall')} right={topcasing} />
          <BoxView left={I18n.t('mainpile.3_3.ground')} right={ground} last />
        </View>
      </View>
    )
  }

  _renderTable() {
    const pilevalue = this.state.data.PileValue || []
    const coord = pilevalue.Image_ReadingCoordinate || []
    const diff = pilevalue.Image_SurveyDiff || []
    const casing = pilevalue.CasingPosition || []
    const image = coord
    const diffimage = diff
    const NCal = (pilevalue.PileSurvey_ReadingNCoordinate - casing.northing).toFixed(4)
    const ECal = (pilevalue.PileSurvey_ReadingECoordinate - casing.easting).toFixed(4)
    const constant = pilevalue.constant || 0.03
    var n = pilevalue.PileSurvey_ReadingNCoordinate || 0
    var e = pilevalue.PileSurvey_ReadingECoordinate || 0
    n = n.toFixed(4)
    e = e.toFixed(4)

    var northing = parseFloat(casing.northing).toFixed(4)
    var easting = parseFloat(casing.easting).toFixed(4)
    var northing_str = ''
    var easting_str = ''
    if(northing != undefined){
      northing_str = northing.toString()
    }
    if(easting != undefined){
      easting_str = easting.toString()
    }

    if(isNaN(northing_str)){
      northing_str = ''
    }
    if(isNaN(easting_str)){
      easting_str = ''
    }

    return (
      <View style={{ marginTop: 15 }}>
        <TableView value={[I18n.t('notificationdetail.coordinate'), I18n.t('notificationdetail.plan'), I18n.t('notificationdetail.read'), I18n.t('notificationdetail.deviation')]} head />
        <TableView
          value={[`N (${I18n.t('mainpile.3_4.m')})`, northing_str || "-", n || "-", NCal]}
          red={Math.abs(parseFloat(NCal)).toFixed(4) > parseFloat(constant).toFixed(4)}
        />
        <TableView
          value={[`E (${I18n.t('mainpile.3_4.m')})`, easting_str || "-", e || "-", ECal]}
          red={Math.abs(parseFloat(ECal)).toFixed(4) > parseFloat(constant).toFixed(4)}
          last
        />
        <View
          style={{
            alignItems: "center",
            marginTop: 10
          }}
        >
          <View style={{ marginLeft: 10 }}>
            <Text>{I18n.t("mainpile.3_2.read")}</Text>
          </View>

          <TouchableOpacity style={[styles.picButton,image.length>0?{backgroundColor:'#6dcc64'}:{}]} onPress={() => this.onCameraRoll("notidetail", image, "view")}>
            <Icon name="image" type="entypo" color="#FFF" />
            <Text style={styles.textButton}>{I18n.t("mainpile.3_2.view")}</Text>
          </TouchableOpacity>

          <View style={{ marginTop: 10, marginLeft: 10 }}>
            <Text>{I18n.t("mainpile.3_2.deviation")}</Text>
          </View>

          <TouchableOpacity
            style={[styles.picButton,diffimage.length>0?{backgroundColor:'#6dcc64'}:{}]}
            onPress={() => this.onCameraRoll("notidetaildiff", diffimage, "view")}
          >
            <Icon name="image" type="entypo" color="#FFF" />
            <Text style={styles.textButton}>{I18n.t("mainpile.3_2.view")}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  _renderButton() {
    return (
      <View>
        <TouchableOpacity
          style={[styles.button, { backgroundColor: "#e65004" }]}
          onPress={() => this.onConfirmPress(0)}
        >
          <Text style={styles.textButton}>{I18n.t('notificationdetail.notapprove')}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => this.onConfirmPress(1)}>
          <Text style={styles.textButton}>{I18n.t('notificationdetail.approve')}</Text>
        </TouchableOpacity>
      </View>
    )
  }
  _renderButtonacception() {
    return (
      <View>
        <TouchableOpacity style={styles.button} onPress={() => this.onConfirmPress(1)}>
          <Text style={styles.textButton}>{I18n.t('notificationdetail.accept')}</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderConcrete() {
    const pilevalue = this.state.data.PileValue || []
    const concrete = pilevalue.ConcreteTruckRegister || []
    const concreteImage1 = concrete.ImageTicket || []
    const concreteImage2 = concrete.ImageTruck || []
    const concreteImage3 = concrete.ImageSlump || []
    const concreteSlump = concrete.Slump || "-"
    // console.warn(pilevalue.ConcreteTruckRegister )
    return (
      <View style={{ marginTop: 15 }}>
        <View style={styles.topicBox}>
          <Text style={styles.subTopic}>{I18n.t('notificationdetail.concreteInfo')}</Text>
        </View>
        <View style={{ marginTop: 5 }}>
          <BoxView left={I18n.t('notificationdetail.concrete')} right={concrete.ConcreteSuplierName} />
          <BoxView left={I18n.t('notificationdetail.No_mix')} right={concrete.MixName} />
          <BoxView left={I18n.t('notificationdetail.No_car')} right={concrete.TruckNo} />
          <BoxView left={I18n.t('notificationdetail.concrete_per')} right={concrete.TruckConcreteVolume} />
          <BoxView left={I18n.t('notificationdetail.slump')} right={concreteSlump} red last />
        </View>
        <View
          style={{
            alignItems: "center",
            marginTop: 10
          }}
        >
          <View style={{ marginLeft: 10 }}>
            <Text>{I18n.t('notificationdetail.img_concrete')}</Text>
          </View>

          <TouchableOpacity
            style={[styles.picButton,concreteImage1.length>0?{backgroundColor:'#6dcc64'}:{}]}
            onPress={() => this.onCameraRoll("concrete1", concreteImage1, "view")}
          >
            <Icon name="image" type="entypo" color="#FFF" />
            <Text style={styles.textButton}>{I18n.t("mainpile.3_2.view")}</Text>
          </TouchableOpacity>

          <View style={{ marginTop: 10, marginLeft: 10 }}>
            <Text>{I18n.t('notificationdetail.img_nocar')}</Text>
          </View>

          <TouchableOpacity
            style={[styles.picButton,concreteImage2.length>0?{backgroundColor:'#6dcc64'}:{}]}
            onPress={() => this.onCameraRoll("concrete2", concreteImage2, "view")}
          >
            <Icon name="image" type="entypo" color="#FFF" />
            <Text style={styles.textButton}>{I18n.t("mainpile.3_2.view")}</Text>
          </TouchableOpacity>

          <View style={{ marginTop: 10, marginLeft: 10 }}>
            <Text>{I18n.t('notificationdetail.img_slump')}</Text>
          </View>

          <TouchableOpacity
            style={[styles.picButton,concreteImage3.length>0?{backgroundColor:'#6dcc64'}:{}]}
            onPress={() => this.onCameraRoll("concrete3", concreteImage3, "view")}
          >
            <Icon name="image" type="entypo" color="#FFF" />
            <Text style={styles.textButton}>{I18n.t("mainpile.3_2.view")}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  onCameraRoll(keyname, photos, type) {
    Actions.camearaRoll({
      process: 0,
      step: 0,
      pileId: 0,
      keyname: keyname,
      photos: photos,
      typeViewPhoto: type,
      shape: 1,
      category: this.props.category,
      Edit_Flag:0
    })
  }

  onConfirmPress(flag) {
    this.props.notiConfirm(this.state.data.Id, flag, this.state.latitude, this.state.longitude)
    // Actions.pop()
    // setTimeout(()=> Actions.refresh({ force: true }), 500)
    Actions.pop({
      refresh: {
        value: {
          force: true
        }
      }
    })
    setTimeout(() => Actions.refresh({ value: { force: false } }), 500)
  }

  render() {
   
    if (this.props.error != null ) {
      return null
    }
    if (this.props.processid == 9) {
      return (
        <Container>
          <Content>
            {this._renderTopic()}
            {this.renderConcrete()}
            {this.props.status==1|| this.props.status==15?this._renderButton():this._renderButtonacception()}
          </Content>
        </Container>
      )
    } else {
      return (
        <Container>
          <Content>
            {this._renderTopic()}
            {this._renderBasicInformation()}
            {this._renderMeasurePosition()}
            {this._renderCasingPosition()}
            {this._renderTable()}
            {this.props.status==1 || this.props.status==5?this._renderButton():this._renderButtonacception()}
          </Content>
        </Container>
      )
    }
  }
}

class BoxView extends Component {
  render() {
    return (
      <View style={[styles.box, this.props.last ? { borderBottomWidth: 0.5 } : {}]}>
        <View style={styles.leftBox}>
          <Text style={styles.textBox}>{this.props.left}</Text>
        </View>
        <View style={styles.rightBox}>
          <Text style={[styles.textBox, this.props.red && { color: 'red' }]}>{this.props.right}</Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  topic: {
    marginTop: 10,
    marginLeft: 10,
    alignItems: "center"
  },
  topicText: {
    fontSize: 25,
    color: MAIN_COLOR
  },
  subTopic: {
    fontSize: 15,
    fontWeight: "bold",
    color: "black",
    marginLeft: 10
  },
  box: {
    height: "auto",
    minHeight: 30,
    flexDirection: "row",
    borderTopWidth: 0.5,
    borderColor: "black"
  },
  leftBox: {
    backgroundColor: MENU_GREY_ITEM,
    width: "45%",
    justifyContent: "center"
  },
  rightBox: {
    width: "60%",
    justifyContent: "center"
  },
  textBox: {
    marginLeft: 10,
    textAlignVertical: "center"
  },
  topicBox: {
    borderTopWidth: 0.5
  },
  button: {
    margin: 10,
    backgroundColor: MAIN_COLOR,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
  textButton: {
    color: "#FFF",
    fontSize: 22,
    fontFamily: "THSarabunNew",
    fontWeight: "bold"
  },
  picButton: {
    backgroundColor: SUB_COLOR,
    height: 40,
    width: "50%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10,
    marginTop: 10
  },
  textButton: {
    marginLeft: 10,
    color: "white"
  }
})

const mapStateToProps = state => {
  return {
    error: state.noti.error,
    data: state.noti.notidetail,
    process: state.noti.process,
    location_lat: state.location.location_lat,
    location_log:state.location.location_log,
  }
}

export default connect(mapStateToProps, actions)(NotificationDetail)
