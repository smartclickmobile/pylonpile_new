import { API_IP } from "../Constants/Constant"
import React, { Component } from "react"
import { Alert } from "react-native"
import { getVersionApp } from '../Actions/AuthAction'

export async function Login(data, callback, error) {
  var formBody = []
  for (var property in data) {
    var encodedKey = encodeURIComponent(property)
    var encodedValue = encodeURIComponent(data[property])
    formBody.push(encodedKey + "=" + encodedValue)
  }
  let version =  getVersionApp()
  formBody.push('Version='+version)
  formBody = formBody.join("&")

  try {
    var response = await fetch(API_IP + "login", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: formBody,
      timeout: 3000
    })
    if (response.ok) {
      let responseJson = await response.json()
      var message = responseJson.Message
      if (message.Code != "001") {
        error(message)
        return
      }
      callback(responseJson)
    } else {
      error({ Message: "error" })
    }
  } catch (e) {
    error({ Message: e.message })
  }
}

export function Login2(data, callback, error) {
  var formBody = []
  for (var property in data) {
    var encodedKey = encodeURIComponent(property)
    var encodedValue = encodeURIComponent(data[property])
    formBody.push(encodedKey + "=" + encodedValue)
  }
  let version =  getVersionApp()
  formBody.push('Version='+version)
  formBody = formBody.join("&")
  fetch(API_IP + "login", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/x-www-form-urlencoded"
    },
    body: formBody,
    timeout: 3000
  })
    .then(response => response.json())
    .then(json => {
      var message = json.Message
      if (message.Code != "001") {
        console.log("Called inside")
        error(message)
        return
      }
      callback(responseJson)
    })
    .catch(error => {
      console.log("I called", e.message)
      error({ Message: e.message })
    })
}

export async function JobList(data, callback, error) {
  var formBody = []
  for (var property in data) {
    var encodedKey = encodeURIComponent(property)
    var encodedValue = encodeURIComponent(data[property])
    formBody.push(encodedKey + "=" + encodedValue)
  }
  let version =  getVersionApp()
  formBody.push('Version='+version)
  formBody = formBody.join("&")
  try {
    var response = await fetch(API_IP + "joblist", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: formBody
    })
    if (response.ok) {
      let responseJson = await response.json()
      var message = responseJson.Message
      if (message.Code != "001") {
        error(message)
        return
      }
      callback(responseJson)
    } else {
      error({ Message: "error" })
    }
  } catch (e) {
    error({ Message: e.message })
  }
}

export async function PileAll(data, callback, error) {
  var formBody = []
  for (var property in data) {
    var encodedKey = encodeURIComponent(property)
    var encodedValue = encodeURIComponent(data[property])
    formBody.push(encodedKey + "=" + encodedValue)
  }
  let version =  getVersionApp()
  formBody.push('Version='+version)
  formBody = formBody.join("&")
  try {
    var response = await fetch(API_IP + "pileall", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: formBody
    })
    if (response.ok) {
      let responseJson = await response.json()
      var message = responseJson.Message
      if (message.Code != "001") {
        error(message)
        return
      }
      callback(responseJson)
    } else {
      error({ Message: "error" })
    }
  } catch (e) {
    error({ Message: e.message })
  }
}

export async function JobDetail(data, callback, error) {
  var formBody = []
  for (var property in data) {
    var encodedKey = encodeURIComponent(property)
    var encodedValue = encodeURIComponent(data[property])
    formBody.push(encodedKey + "=" + encodedValue)
  }
  let version =  getVersionApp()
  formBody.push('Version='+version)
  formBody = formBody.join("&")
  try {
    var response = await fetch(API_IP + "jobdetail", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: formBody
    })
    if (response.ok) {
      let responseJson = await response.json()
      var message = responseJson.Message
      if (message.Code != "001") {
        error(message)
        return
      }
      callback(responseJson)
    } else {
      error({ Message: "error" })
    }
  } catch (e) {
    error({ Message: e.message })
  }
}

export async function PileDetail(data, callback, error) {
  var formBody = []
  for (var property in data) {
    var encodedKey = encodeURIComponent(property)
    var encodedValue = encodeURIComponent(data[property])
    formBody.push(encodedKey + "=" + encodedValue)
  }
  let version =  getVersionApp()
  formBody.push('Version='+version)
  formBody = formBody.join("&")
  try {
    var response = await fetch(API_IP + "piledetail", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: formBody
    })
    if (response.ok) {
      let responseJson = await response.json()
      var message = responseJson.Message
      if (message.Code != "001") {
        error(message)
        return
      }
      callback(responseJson)
    } else {
      error({ Message: "error" })
    }
  } catch (e) {
    error({ Message: e.message })
  }
}

export async function GroupEmployee(data, callback, error) {
  var formBody = []
  for (var property in data) {
    var encodedKey = encodeURIComponent(property)
    var encodedValue = encodeURIComponent(data[property])
    formBody.push(encodedKey + "=" + encodedValue)
  }
  let version =  getVersionApp()
  formBody.push('Version='+version)
  formBody = formBody.join("&")
  try {
    var response = await fetch(API_IP + "groupemployee", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: formBody
    })
    if (response.ok) {
      let responseJson = await response.json()
      var message = responseJson.Message
      if (message.Code != "001") {
        throw new Error(message.Message)
        error(message.Message)
      }
      callback(responseJson)
    } else {
      error("error")
      console.log(response)
    }
  } catch (e) {
    console.log(e)
    error(e.message)
  }
}

export async function Extra(data, type, callback, error) {
  var formBody = []
  for (var property in data) {
    var encodedKey = encodeURIComponent(property)
    var encodedValue = encodeURIComponent(data[property])
    formBody.push(encodedKey + "=" + encodedValue)
  }
  let version =  getVersionApp()
  formBody.push('Version='+version)
  formBody = formBody.join("&")

  try {
    var response = await fetch(API_IP + type, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: formBody
    })
    if (response.ok) {
      let responseJson = await response.json()
      var message = responseJson.Message
      console.log('Testlogin employees api',responseJson.Message)
      if (message.Code != "001") {
        error(message)
        return
      }
      callback(responseJson)
    } else {
      console.log('Testlogin employees api error1')
      error({ Message: "error" })
    }
  } catch (e) {
    console.log('Testlogin employees api error2',e.message)
    error({ Message: e.message })
  }
}
