import React, { Component } from "react"
import { StyleSheet, Text, View, TouchableOpacity, Dimensions, Alert, BackHandler,ScrollView,PermissionsAndroid } from "react-native"
import {
  Container,
  Content,
  Header,
  Left,
  Body,
  Right,
  Title,
  Icon,
  List,
  ListItem,
  Button,
  Segment
} from "native-base"
import AsyncStorage from '@react-native-community/async-storage';
import { ButtonGroup, Icon as Ic } from "react-native-elements"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import { pileItems, pileitemsclear } from "./Actions"
import NotStartProject from "./Components/Projects/NotStartProject"
import UnfinishProject from "./Components/Projects/UnfinishProject"
import FinishProject from "./Components/Projects/FinishProject"
import NotStartProjectList from "./Components/Projects/NotStartProjectList"
import RecentProject from "./Components/Projects/RecentProject"
import { PileAll, JobDetail } from "./Controller/API"
import Zone from "./Components/Projects/Zone"
import { MAIN_COLOR, SUB_COLOR,GRAY } from "./Constants/Color"
import I18n from "../assets/languages/i18n"
import Spinner from "react-native-loading-spinner-overlay"
import moment from "moment"
import DeviceInfo from "react-native-device-info"
import naturalSort from "javascript-natural-sort"
import Loader from './Components/Loader'
import RNFS from 'react-native-fs'
import FusedLocation from 'react-native-fused-location'
import * as actions from "./Actions"
export class ProjectList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedIndex: 0,
      loading: true,
      pileall: [],
      notStartProject: 0,
      sort: false,
      list: false,
      group: 1,
      notStartNumber: 0,
      unfinishNumber: 0,
      finishNumber: 0,
      sortdate: true,
      sortdate2: false,
      result:'',
      now:'',
      before:'',
      az:true,
      all:true,
      lengthstatus0:27,
      lengthstatus1:10,
      lengthstatus2:10,
      lat:'',
      log:'',
      sortERP:false,
      loader:false,
      Unblockerp_random:'',
      search:false,
      refresh_btn: true,
      loading_refresh: false
      
    }
    this.updateIndex = this.updateIndex.bind(this)
    this._onScroll = this._onScroll.bind(this)
    // console.warn("data ProjectList",this.props.title)
  }

  updateIndex(selectedIndex) {
    this.setState({ selectedIndex })
    
    // Actions.refresh({result: '', navBar: ''})
  }

  iconPressed() {
    Actions.jobdetail({ title: I18n.t('projectlist.projectinfo'), jobid: this.props.jobid })
  }
  iconPressedRefresh() {
    console.warn('iconPressedRefresh',this.state.refresh_btn)
    if(this.state.refresh_btn == true){
      this.setState({refresh_btn:false,pileall:[],loading_refresh:true},()=>{
        setTimeout(()=>{
            this.props.pileItems({jobid: this.props.jobid,Language:I18n.currentLocale(),start:0,length:27,status:0,load:true })
            this.props.pileItems({jobid: this.props.jobid,Language:I18n.currentLocale(),start:0,length:10,status:1,load:true })
            this.props.pileItems({jobid: this.props.jobid,Language:I18n.currentLocale(),start:0,length:10,status:2,load:true })    
        },500)
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    
    if (nextProps.error != null) {
      this.setState({ loading: false })
      Alert.alert("ERROR", nextProps.error, [{ text: "OK" }])
    }
    // console.warn('clearerp',nextProps.clearerp,nextProps.searchpile,nextProps.loading1)
    // AsyncStorage.setItem("jobid", this.props.jobid)
    if(this.props.jobid == nextProps.idjob){
      if(nextProps.clearerp == true){
        if(nextProps.items != undefined && nextProps.items != null && nextProps.items.length != 0 && nextProps.items != []){
          var data = this.state.pileall.concat(nextProps.items)
          this.setState({pileall:data})
        }else{
          this.setState({pileall:[]})
        }
      }else{
        if(nextProps.searchpile == true){
          console.warn('s',nextProps.input)
            if(nextProps.items != undefined && nextProps.items != null && nextProps.items.length != 0 && nextProps.items != []){
              this.setState({pileall:nextProps.items,lengthstatus0:27,lengthstatus1:10,lengthstatus2:10,search:nextProps.input==''?false:true})
            }else{
              this.setState({pileall:[],search:nextProps.input==''?false:true,loading_refresh:false,loading:false})
            }
        }else{
          if(nextProps.loading1 != true){
            if(nextProps.items != undefined && nextProps.items != null && nextProps.items.length != 0 && nextProps.items != []){
              var data = this.state.pileall.concat(nextProps.items)
              console.warn('concat',nextProps.items)
              this.setState({pileall:data,refresh_btn:true,loading_refresh:false})
            }else{
              // console.warn('not s bank')
              // this.setState({pileall:[]})
              // this.setState({refresh_btn:true,loading_refresh:false})
            }
          }
          
        }
      }
      
      
    }
    if(nextProps.loading1 != undefined){
      this.setState({loader:nextProps.loading1},()=>{
        // console.warn('set loader',this.state.loader)
        if(this.state.loader){
          this.setState({lengthstatus0:27,lengthstatus1:10,lengthstatus2:10})
        }
      })
    }
    // console.warn('data_Unblockerp',nextProps.Unblockerp_success,nextProps.Unblockerp_random,nextProps.data_Unblockerp)
    if(nextProps.Unblockerp_success == true && nextProps.Unblockerp_random != this.state.Unblockerp_random){
      this.setState({Unblockerp_random:nextProps.Unblockerp_random},()=>{
        if(nextProps.data_Unblockerp != null){
          var data = nextProps.data_Unblockerp
          // console.warn('data4444',data)
          Actions.mainpile({title: data.title, pileid: data.pileid, jobid: data.jobid, category:data.category,viewType:0, finish:false,sp_parentid:data.sp_parentid,pile_childs:data.pile_childs})
        }
      })
    }
    // Actions.mainpile({title: pile_no, pileid: pile_id, jobid: this.props.jobid, category:category,viewType:0})
    // if(nextProps.items && nextProps.error == null) {
    //   this.setState({ pileall: nextProps.items, loading: false })
    //   // this.getProjectValue(nextProps.items)
    // }
  }

  getProjectValue(project) {
    var notStartNumber = 0
    var unfinishNumber = 0
    var finishNumber = 0
    project.map((pile, index) => {
      switch (pile.status) {
        case 0:
          notStartNumber++
          break
        case 1:
          unfinishNumber++
          break
        case 2:
          finishNumber++
          break
        default:
          console.log("nope")
      }
    })
    this.setState({
      notStartNumber: notStartNumber,
      unfinishNumber: unfinishNumber,
      finishNumber: finishNumber,
      notStartProject: project.length
    })
  }

  componentWillMount() {
    BackHandler.addEventListener("projectlistback", () => this._handleBack())
  }

  async componentDidMount() {
    console.log("fetch")
    this.fetchData()
    // navigator.geolocation.watchPosition(async position => {
    //   let lat = position.coords.latitude
    //   let log = position.coords.longitude
    //   this.setState({lat:lat,log:log})
    // })
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
          title: 'App needs to access your location',
          message: 'App needs access to your location ' +
          'so we can let our app be even more awesome.'
          }
      )
      if (granted) {
        FusedLocation.setLocationPriority(FusedLocation.Constants.HIGH_ACCURACY)
        const location = await FusedLocation.getFusedLocation()
        this.props.setlocation(location)
        if(location == undefined){
          this.setState({ lat: this.props.location_lat, log: this.props.location_log })
        }else{
          this.setState({ lat: location.latitude, log: location.longitude })
        }
        console.warn('FusedLocation',location.latitude,location.longitude)
        //  navigator.geolocation.watchPosition(async position => {
        //     let lat = position.coords.latitude
        //     let log = position.coords.longitude
        //     this.setState({lat:lat,log:log})
        //   })
      }else{
        // console.warn('granted')
      }

      this.removeResizFolder()
  }
  removeResizFolder(){
    
    var new_path = '/storage/emulated/0/Pylon/Images/resize'
    // console.warn('removeResizFolder project',new_path)
    RNFS.readDir(RNFS.ExternalStorageDirectoryPath+"/Pylon/Images")
      .then( (result) => {
          console.log("file exists: readDir", result);

          if(result){
            return RNFS.unlink(new_path)
              .then(() => {
                console.log('FILE DELETED');
              })
              // `unlink` will throw an error, if the item to unlink does not exist
              .catch((err) => {
                console.log('err unlink',err.message);
              });
          }

        })
        .catch((err) => {
          console.log('err readDir',err.message);
        });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("projectlistback")
    this.props.pileitemsclear({clearerp:false})
  }

  _onScroll(e){

    if(e.nativeEvent.layoutMeasurement.height + e.nativeEvent.contentOffset.y >= e.nativeEvent.contentSize.height-0.1){
    // console.warn('re',this.props.input)
      if(this.props.input == undefined || this.props.input == ''){
        if(this.state.selectedIndex == 0){
          // if(this.state.pileall.length < this.props.status0){
          //   this.props.pileItems({jobid: this.props.jobid,Language:I18n.currentLocale(),start:this.state.lengthstatus0,length:10,status:0 ,load:true})
          //   this.setState({lengthstatus0:this.state.lengthstatus0+10})
          // }
          
          this.props.pileItems({jobid: this.props.jobid,Language:I18n.currentLocale(),start:this.state.lengthstatus0,length:10,status:0 ,load:true})
          this.setState({lengthstatus0:this.state.lengthstatus0+10})
        }else if(this.state.selectedIndex == 1){
          // console.warn('lengthstatus1',this.state.lengthstatus1)
          this.props.pileItems({jobid: this.props.jobid,Language:I18n.currentLocale(),start:this.state.lengthstatus1,length:10,status:1 ,load:true})
          this.setState({lengthstatus1:this.state.lengthstatus1+10})
        }else if(this.state.selectedIndex == 2){
          this.props.pileItems({jobid: this.props.jobid,Language:I18n.currentLocale(),start:this.state.lengthstatus2,length:10,status:2 ,load:true})
          this.setState({lengthstatus2:this.state.lengthstatus2+10})
        }
      // }
      }
    }
  
  }
  fetchData() {
    
    this.props.pileItems({jobid: this.props.jobid,Language:I18n.currentLocale(),start:0,length:27,status:0 })
    this.props.pileItems({jobid: this.props.jobid,Language:I18n.currentLocale(),start:0,length:10,status:1 })
    this.props.pileItems({jobid: this.props.jobid,Language:I18n.currentLocale(),start:0,length:10,status:2 })
  }

  splitIntoSubArray(arr, count) {
    var newArray = []
    while (arr.length > 0) {
      newArray.push(arr.splice(0, count))
    }
    return newArray
  }

  _handleBack() {
    if (Actions.currentScene == "_projectlist") {
      Actions.pop()
      return true
    }
    return false
  }

  seperateZone = piles => {
    var zone = []
    var groupBy = function(xs, key) {
      return xs.reduce(function(rv, x) {
        ;(rv[x[key]] = rv[x[key]] || []).push(x)
        return rv
      }, {})
    }

    var groubedByTeam = groupBy(piles, "zone_name")
    return groubedByTeam
  }
  seperateType = piles => {
    var zone = []
    var groupBy = function(xs, key) {
      return xs.reduce(function(rv, x) {
        ;(rv[x[key]] = rv[x[key]] || []).push(x)
        return rv
      }, {})
    }

    var groubedByTeam = groupBy(piles, "type_name")
    return groubedByTeam
  }
  
  pressButton(type,index){
    if(this.state.now == ''){
      this.setState({now:type})
      if(index == 2){
        this.setState({all:false})
      }else if(index == 1){
        this.setState({az:false})
      }
    }else{
      if(type != ''){
        if(type != this.state.now){
          this.setState({before:this.state.now},()=>{
            this.setState({now:type})
          })
        }
        if(index == 2){
          this.setState({all:false})
        }else if(index == 1){
          this.setState({az:false})
        }
      }else{
     
        if(index == 1){
          this.setState({az:true})
        }else if(index == 2){
          this.setState({all:true})
        }
       
      }
    }
  }
  setGroup(){
    if(this.state.group == 1){
      this.setState({group: 2,sortERP:false})
      this.pressButton('ZONE',2)
    }else if(this.state.group == 2){
      this.setState({group: 1,sortERP:false})
      this.pressButton('',2)
    }
  }
  _sortButton = status => {
    if (status == 0) {
      return (
        <View style={styles.sortButtonView}>
          <TouchableOpacity onPress={() => 
            this.setState({ sort: !this.state.sort,sortERP:false },()=>{this.state.sort?this.pressButton('TYPE',1):this.pressButton('',1)})
          }>
            <View style={[styles.sortButton, this.state.sort && styles.sortButtonPressed]}>
              <Text style={[styles.sortButtonText, this.state.sort && styles.sortButtonTextPressed]}>
                {this.state.sort ? "A-Z" : "TYPE"}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.setState({ list: !this.state.list })}>
            <View style={[styles.sortButton, this.state.list && styles.sortButtonPressed]}>
              {!this.state.list && <Ic name="list" type="entypo" color={MAIN_COLOR} />}
              {this.state.list && <Ic name="dots-three-horizontal" type="entypo" color="#fff" />}
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.setGroup()}>
            <View style={[styles.sortButton, this.state.group !=1 && styles.sortButtonPressed]}>
              <Text style={[styles.sortButtonText, this.state.group !=1 && styles.sortButtonTextPressed]}>
                {this.state.group == 1 ? "ZONE" : this.state.group == 2 ? "ALL":"ZONE"}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      )
    }

    if (status == 1) {
      return (
        <View style={styles.sortButtonView}>
          {/*<TouchableOpacity onPress={() => this.setState({ sortdate2: !this.state.sortdate2 })}>
            <View style={[styles.sortButton, this.state.sortdate2 && styles.sortButtonPressed]}>
              <Text style={[styles.sortButtonText, this.state.sortdate2 && styles.sortButtonTextPressed]}>
                {this.state.sortdate2 ? "A-Z" : "DATE"}
              </Text>
            </View>
      </TouchableOpacity>*/}
          <TouchableOpacity onPress={() => 
            this.setState({ sort: !this.state.sort,sortERP:false },()=>{this.state.sort?this.pressButton('TYPE',1):this.pressButton('',1)})
          }>
            <View style={[styles.sortButton, this.state.sort && styles.sortButtonPressed]}>
              <Text style={[styles.sortButtonText, this.state.sort && styles.sortButtonTextPressed]}>
                {this.state.sort ? "DATE" : "TYPE"}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=> this.setState({sortERP:!this.state.sortERP,group:1,az:true,all:true,sort:false})}>
            <View style={[styles.sortButton, this.state.sortERP  && styles.sortButtonPressed]}>
              <Text style={[styles.sortButtonText, this.state.sortERP  && styles.sortButtonTextPressed]}>
              {this.state.sortERP == false ? "ERP" : "ERP"}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() =>this.setGroup()}>
            <View style={[styles.sortButton, this.state.group != 1 && styles.sortButtonPressed]}>
              <Text style={[styles.sortButtonText, this.state.group != 1 && styles.sortButtonTextPressed]}>
              {this.state.group == 1 ? "ZONE" : this.state.group == 2 ? "ALL":"ZONE"}
              </Text>
            </View>
          </TouchableOpacity>
          {/*<TouchableOpacity >
            <View style={[styles.sortButton]}>
              <Text style={[styles.sortButtonText]}>
              {"FINISH"}
              </Text>
            </View>
          </TouchableOpacity>*/}
        </View>
      )
    } else {
      return (
        <View style={styles.sortButtonView}>
          {/*<TouchableOpacity onPress={() => this.setState({ sortdate: !this.state.sortdate })}>
            <View style={[styles.sortButton, this.state.sortdate && styles.sortButtonPressed]}>
              <Text style={[styles.sortButtonText, this.state.sortdate && styles.sortButtonTextPressed]}>
                {this.state.sortdate ? "A-Z" : "DATE"}
              </Text>
            </View>
      </TouchableOpacity>*/}
          <TouchableOpacity onPress={() => 
            this.setState({ sort: !this.state.sort },()=>{this.state.sort?this.pressButton('TYPE',1):this.pressButton('',1)})
          }>
            <View style={[styles.sortButton, this.state.sort && styles.sortButtonPressed]}>
              <Text style={[styles.sortButtonText, this.state.sort && styles.sortButtonTextPressed]}>
                {this.state.sort ? "A-Z" : "TYPE"}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=> this.setState({sortERP:!this.state.sortERP,group:1,az:true,all:true,sort:false})}>
            <View style={[styles.sortButton, this.state.sortERP  && styles.sortButtonPressed]}>
              <Text style={[styles.sortButtonText, this.state.sortERP  && styles.sortButtonTextPressed]}>
              {this.state.sortERP == false ? "ERP" : "ERP"}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.setGroup()}>
            <View style={[styles.sortButton, this.state.group != 1 && styles.sortButtonPressed]}>
              <Text style={[styles.sortButtonText, this.state.group != 1 && styles.sortButtonTextPressed]}>
              {this.state.group == 1 ? "ZONE" : this.state.group == 2 ? "All":"ZONE"}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      )
    }
  }

  _notStartButton = () => {
    return (
      <View style={styles.tabBox}>
        <Text style={styles.textTablet}>{I18n.t('projectlist.notstart')}</Text>
        <Text style={styles.textTablet}>
          {this.props.status0}/{this.props.totalpile}
        </Text>
      </View>
    )
  }

  _unfinishButton = () => {
    return (
      <View style={styles.tabBox}>
        <Text style={styles.textTablet}>{I18n.t('projectlist.unfinish')}</Text>
        <Text style={styles.textTablet}>{this.props.status1}</Text>
      </View>
    )
  }

  _finishButton = () => {
    return (
      <View style={styles.tabBox}>
        <Text style={styles.textTablet}>{I18n.t('projectlist.finish')}</Text>
        <Text style={styles.textTablet}>
          {this.props.status2}/{this.props.totalpile}
        </Text>
      </View>
    )
  }

  notStartProject = () => {
   
    if(this.state.az){
      if(this.state.all){
        if(this.state.list){
          return this.notStartProjectList()
        }else{
          return this.notStart()
        }
      }else{
        if (this.state.group == 2 && this.state.list) {
          return this.notStartProjectZoneList()
        } else if (this.state.group == 2 && !this.state.list) {
          return this.notStartProjectZone()
        } else if (!this.state.group == 2 && this.state.list) {
          return this.notStartProjectList()
        }
      }
    }else{
      if(!this.state.all){
        if(this.state.before == 'ZONE'){
          if (this.state.group == 2 && this.state.list) {
            return this.notStartProjectZoneList()
          } else if (this.state.group == 2 && !this.state.list) {
            return this.notStartProjectZone()
          } else if (!this.state.group == 2 && this.state.list) {
            return this.notStartProjectList()
          }
        }else{
          if(this.state.sort && !this.state.list){
            return this.notStartProjectType()
          }else if(this.state.sort && this.state.list){
            return this.notStartProjectTypeList()
          }else if(!this.state.sort && this.state.list){
            return this.notStartProjectList()
          }
        }
      }else{
        if(this.state.sort && !this.state.list){
          return this.notStartProjectType()
        }else if(this.state.sort && this.state.list){
          return this.notStartProjectTypeList()
        }else if(!this.state.sort && this.state.list){
          return this.notStartProjectList()
        }
      }
      
    }
  }

  notStart = () => {
    var pileall = this.state.pileall
    pileall = pileall.filter(pile => {
      if (
        typeof this.props.result !== "undefined" &&
        !pile.pile_no.toLowerCase().includes(this.props.result.toLowerCase())
      ) {
        return
      }
      return pile.status == 0
    })
    pileall = pileall.sort((a, b) => {
      return naturalSort(a.pile_no, b.pile_no)
    })

    var pile3each = this.splitIntoSubArray(pileall, 3)
    return (
      <View>
        <NotStartProject value={pile3each} jobid={this.props.jobid} />
      </View>
    )
  }
  notStartProjectTypeList = () => {
    var pileall = this.state.pileall
      pileall = pileall.filter(pile => {
        if (
          typeof this.props.result !== "undefined" &&
          !pile.pile_no.toLowerCase().includes(this.props.result.toLowerCase())
        ) {
          return
        }
        return pile.status == 0
      })
      pileall = pileall.sort(this._sortType)
      var pilestype = this.seperateType(pileall)
      var zoneName = Object.keys(pilestype)
      var container = []
      var count = 0
      var counttype = 50000
      for (key in pilestype) {
        var temp = []
        pilestype[key].map(value => {
          temp.push(value)
        })
        container.push(<Zone name={key} key={counttype} type={I18n.t('typezone.type')} />)
        if(this.state.now == 'ZONE' && this.state.all == false){
          var pileszone = this.seperateZone(pilestype[key])
          var container1 = []
          var count1 = 0
          var counttype1 = 50000
          for (data in pileszone) {
            container1.push(<Zone name={data} key={counttype1} type={I18n.t('typezone.zone')} sub={true}/>)
            container1.push(<NotStartProject value={pileszone[data]} key={count1} jobid={this.props.jobid} />)
          }
          container.push(container1)
          count1 += 1
          counttype1 += 1
          
        }else{
          container.push(<NotStartProject value={temp} key={count} jobid={this.props.jobid} />)
          count += 1
          
        }
        counttype += 1
    }
    return container
  }
  notStartProjectZoneList = () => {
    var pileall = this.state.pileall
    pileall = pileall.filter(pile => {
      if (
        typeof this.props.result !== "undefined" &&
        !pile.pile_no.toLowerCase().includes(this.props.result.toLowerCase())
      ) {
        return
      }
      return pile.status == 0
    })
    pileall = pileall.sort(this._sortType)
    var pileszone = this.seperateZone(pileall)
    var zoneName = Object.keys(pileszone)
    var container = []
    var count = 0
    var countzone = 50000
    for (key in pileszone) {
      var temp = []
      pileszone[key].map(value => {
        // console.log(value, key);
        // console.log(value.pilename);
        temp.push(value)
      })
      container.push(<Zone name={key} key={countzone} type={I18n.t('typezone.zone')} />)
      if(this.state.now == 'TYPE' && this.state.az == false){
        var pilestype = this.seperateType(pileszone[key])
        var container1 = []
        var count1 = 0
        var countzone1 = 50000
        console.log(pilestype)
        for (data in pilestype) {

          container1.push(<Zone name={data} key={countzone1} type={I18n.t('typezone.type')} sub={true}/>)
          container1.push(<NotStartProject value={pilestype[data]} key={count1} jobid={this.props.jobid} />)
         
        }
        container.push(container1)
        count1 += 1
        countzone1 += 1
      }else{
        container.push(<NotStartProject value={temp} key={count} jobid={this.props.jobid} />)
        count += 1
      }
      
      countzone += 1
      
    }
    return container
  }
  notStartProjectType = () => {
    var pileall = this.state.pileall
      pileall = pileall.filter(pile => {
        if (
          typeof this.props.result !== "undefined" &&
          !pile.pile_no.toLowerCase().includes(this.props.result.toLowerCase())
        ) {
          return
        }
        return pile.status == 0
      })
      // pileall = pileall.sort(this._sortType)
      var pilestype = this.seperateType(pileall)
      var zoneName = Object.keys(pilestype)
      var container = []
      var count = 0
      var counttype = 50000
      for (key in pilestype) {
        var temp = []
        pilestype[key].map(value => {
          temp.push(value)
        })
        // var temp1 = temp.sort()
        var pile3each = this.splitIntoSubArray(temp, 3)
        container.push(<Zone name={key} key={counttype} type={I18n.t('typezone.type')} />)
        if(this.state.now == 'ZONE' && this.state.all == false){
          var pileszone = this.seperateZone(pilestype[key])
          var container1 = []
          var count1 = 0
          var counttype1 = 50000
          for (data in pileszone) {
            var pile3each1 = this.splitIntoSubArray(pileszone[data], 3)
            container1.push(<Zone name={data} key={counttype1} type={I18n.t('typezone.zone')} sub={true}/>)
            container1.push(<NotStartProject value={pile3each1} key={count1} jobid={this.props.jobid} />)
          }
          container.push(container1)
          count1 += 1
          counttype1 += 1
          
        }else{
          container.push(<NotStartProject value={pile3each} key={count} jobid={this.props.jobid} />)
          count += 1
          
        }
        counttype += 1
        
        
    }
    return container
  }
  notStartProjectZone = () => {
    var pileall = this.state.pileall
    pileall = pileall.filter(pile => {
      if (
        typeof this.props.result !== "undefined" &&
        !pile.pile_no.toLowerCase().includes(this.props.result.toLowerCase())
      ) {
        return
      }
      return pile.status == 0
    })
    // pileall = pileall.sort(this._sortType)

    var pileszone = this.seperateZone(pileall)
    var zoneName = Object.keys(pileszone)
    var container = []
    var count = 0
    var countzone = 50000
    for (key in pileszone) {
      var temp = []
      pileszone[key].map(value => {
        // console.log(value, key);
        // console.log(value.pilename);
        temp.push(value)
      })
      var pile3each = this.splitIntoSubArray(temp, 3)
      container.push(<Zone name={key} key={countzone} type={I18n.t('typezone.zone')} />)
      if(this.state.now == 'TYPE' && this.state.az == false){
        var pilestype = this.seperateType(pileszone[key])
        var container1 = []
        var count1 = 0
        var countzone1 = 50000
        console.log(pilestype)
        for (data in pilestype) {
          
          var pile3each1 = this.splitIntoSubArray(pilestype[data], 3)
          container1.push(<Zone name={data} key={countzone1} type={I18n.t('typezone.type')} sub={true}/>)
          container1.push(<NotStartProject value={pile3each1} key={count1} jobid={this.props.jobid} />)
         
        }
        container.push(container1)
        count1 += 1
        countzone1 += 1
      }else{
        container.push(<NotStartProject value={pile3each} key={count} jobid={this.props.jobid} />)
        count += 1
        
      }
      countzone += 1
      
    }
    return container
  }

  notStartProjectList = () => {
    var pileall = this.state.pileall
    pileall = pileall.sort((a, b) => {
      return naturalSort(a.pile_no, b.pile_no)
    })
    pileall = pileall.filter(pile => {
      if (
        typeof this.props.result !== "undefined" &&
        !pile.pile_no.toLowerCase().includes(this.props.result.toLowerCase())
      ) {
        return
      }
      return pile.status == 0
    })
    return (
      <View>
        <NotStartProjectList array={pileall} jobid={this.props.jobid} jobid={this.props.jobid} />
      </View>
    )
  }

  unFinish() {
    if (this.props.recent && this.props.recent != null && this.props.recent.pile_no != null)
    // console.warn('recent',this.props.recent)
    return (
      <View>
        <View style={{ marginTop: 5 }}>
          <View style={styles.zone}>
            <View style={styles.textView}>
              <Text style={styles.text}>{I18n.t('projectlist.recent')}</Text>
            </View>
            <View style={{ marginLeft: 5, marginRight: 5}}>
              <RecentProject Language={I18n.currentLocale()} data={this.props.recent} jobid={this.props.jobid} lat={this.state.lat} log={this.state.log}/>
            </View>
          </View>
        </View>
          
          {this._unFinishProject()}
      </View>
    )
    return this._unFinishProject()
  }

  _unFinishProject = () => {
    if(this.state.sortERP){
      return this._unFinishProjectERP()
    }else{
      if(this.state.az){
        if(this.state.all){
          return this._unFinishProject1()
        }else{
          if (this.state.group == 2) {
            return this._unFinishProjectZone()
          } else if (!this.state.group == 2) {
            return this._unFinishProjectType()
          }
        }
      }else{
        if(!this.state.all){
          if(this.state.before == 'ZONE'){
            if (this.state.group == 2) {
              return this._unFinishProjectZone()
            } else if (!this.state.group == 2) {
              return this._unFinishProjectType()
            }
          }else{
            return this._unFinishProjectType()
          }
        }else{
          return this._unFinishProjectType()
        }
      }
    }
    
  }
  _unFinishProject1 =()=>{
    var pileall = this.state.pileall
    // var pileall = this.props.items.sort(this._sortUnFinishType)
    pileall = pileall.filter(pile => {
      if (
        typeof this.props.result !== "undefined" &&
        !pile.pile_no.toLowerCase().includes(this.props.result.toLowerCase())
      ) {
        return
      }
      return pile.status == 1
    })
    pileall = pileall.sort((a, b) => {
      if (this.state.search == false) {
        // console.warn('sortdate2')
        var aa = moment(a.modified_date, "DD/MM/YYYY HH:mm:ss")
        var bb = moment(b.modified_date, "DD/MM/YYYY HH:mm:ss")
        return bb.diff(aa)
      } else {
        return naturalSort(a.pile_no, b.pile_no)
      }
    })
    return <UnfinishProject Language={I18n.currentLocale()} array={pileall} jobid={this.props.jobid} lat={this.state.lat} log={this.state.log}/>
  }
  _unFinishProjectERP =()=>{
    var pileall = this.state.pileall
    // var pileall = this.props.items.sort(this._sortUnFinishType)
    pileall = pileall.filter(pile => {
      if (
        typeof this.props.result !== "undefined" &&
        !pile.pile_no.toLowerCase().includes(this.props.result.toLowerCase())
      ) {
        return
      }
      return pile.status == 1
    })
    pileall = pileall.sort((a, b) => {
      if (this.state.search == false) {
        // console.warn('sortdate2')
        var aa = moment(a.modified_date, "DD/MM/YYYY HH:mm:ss")
        var bb = moment(b.modified_date, "DD/MM/YYYY HH:mm:ss")
        return bb.diff(aa)
      } else {
        return naturalSort(a.pile_no, b.pile_no)
      }
    })
    var temp = []
    pileall.map((item,index)=>{
      if(item.importerp_flag){
        temp.push(item)
      }
    })
    console.warn('temp',temp)
    return <UnfinishProject Language={I18n.currentLocale()} array={temp} jobid={this.props.jobid} lat={this.state.lat} log={this.state.log}/>
  }

  _unFinishProjectZone = () => {
    var pileall = this.state.pileall
    // var pileall = this.props.items.sort(this._sortUnFinishType)
    pileall = pileall.filter(pile => {
      if (
        typeof this.props.result !== "undefined" &&
        !pile.pile_no.toLowerCase().includes(this.props.result.toLowerCase())
      ) {
        return
      }
      return pile.status == 1
    })
    pileall = pileall.sort((a, b) => {
      if (this.state.search == false) {
        // console.warn('sortdate2')
        var aa = moment(a.modified_date, "DD/MM/YYYY HH:mm:ss")
        var bb = moment(b.modified_date, "DD/MM/YYYY HH:mm:ss")
        return bb.diff(aa)
      } else {
        return naturalSort(a.pile_no, b.pile_no)
      }
    })
    var pileszone = this.seperateZone(pileall)
    var zoneName = Object.keys(pileszone)
    var container = []
    var count = 0
    var countzone = 50000
    for (key in pileszone) {
      var temp = []
      pileszone[key].map(value => {
        temp.push(value)
      })
      container.push(<Zone name={key} key={countzone} type={I18n.t('typezone.zone')}/>)
      if(this.state.now == 'TYPE' && this.state.az == false){
        var pilestype = this.seperateType(pileszone[key])
        var container1 = []
        var count1 = 0
        var countzone1 = 50000
        console.log(pilestype)
        for (data in pilestype) {
          container1.push(<Zone name={data} key={countzone1} type={I18n.t('typezone.type')} sub={true}/>)
          container1.push(<UnfinishProject Language={I18n.currentLocale()} array={pilestype[data]} key={count1} jobid={this.props.jobid} lat={this.state.lat} log={this.state.log}/>)
        }
        container.push(container1)
        count1 += 1
        countzone1 += 1
      }else{
        container.push(<UnfinishProject Language={I18n.currentLocale()} array={temp} key={count} jobid={this.props.jobid} lat={this.state.lat} log={this.state.log}/>)
        count += 1
        
      }
      countzone += 1
    }
    return container  
  }
  _unFinishProjectType = () => {
    var pileall = this.state.pileall
    // var pileall = this.props.items.sort(this._sortUnFinishType)
    pileall = pileall.filter(pile => {
      if (
        typeof this.props.result !== "undefined" &&
        !pile.pile_no.toLowerCase().includes(this.props.result.toLowerCase())
      ) {
        return
      }
      return pile.status == 1
    })
    pileall = pileall.sort((a, b) => {
      if (this.state.search == false) {
        // console.warn('sortdate2')
        var aa = moment(a.modified_date, "DD/MM/YYYY HH:mm:ss")
        var bb = moment(b.modified_date, "DD/MM/YYYY HH:mm:ss")
        return bb.diff(aa)
      } else {
        return naturalSort(a.pile_no, b.pile_no)
      }
    })
    var pilestype = this.seperateType(pileall)
    var zoneName = Object.keys(pilestype)
    var container = []
    var count = 0
    var counttype = 50000
    for (key in pilestype) {
      var temp = []
      pilestype[key].map(value => {
        temp.push(value)
      })
      container.push(<Zone name={key} key={counttype} type={I18n.t('typezone.type')}/>)
      if(this.state.now == 'ZONE' && this.state.all == false){
        var pileszone = this.seperateZone(pilestype[key])
        var container1 = []
        var count1 = 0
        var countzone1 = 50000
        for (data in pileszone) {
          container1.push(<Zone name={data} key={countzone1} type={I18n.t('typezone.zone')} sub={true}/>)
          container1.push(<UnfinishProject Language={I18n.currentLocale()} array={pileszone[data]} key={count1} jobid={this.props.jobid} />)
        }
        container.push(container1)
        count1 += 1
        countzone1 += 1
      }else{
        container.push(<UnfinishProject Language={I18n.currentLocale()} array={temp} key={count} jobid={this.props.jobid} lat={this.state.lat} log={this.state.log}/>)
        count += 1
        
      }
      counttype += 1
    }
    return container
  }
  _FinishProjectERP =()=>{
    var pileall = this.state.pileall
    // var pileall = this.props.items.sort(this._sortUnFinishType)
    pileall = pileall.filter(pile => {
      if (
        typeof this.props.result !== "undefined" &&
        !pile.pile_no.toLowerCase().includes(this.props.result.toLowerCase())
      ) {
        return
      }
      return pile.status == 2
    })
    pileall = pileall.sort((a, b) => {
      if (this.state.sortdate2) {
        var aa = moment(a.start_date, "DD/MM/YYYY")
        var bb = moment(b.start_date, "DD/MM/YYYY")
        return bb.diff(aa)
      } else {
        return naturalSort(a.pile_no, b.pile_no)
      }
    })
    var temp = []
    pileall.map((item,index)=>{
      if(item.modifydata_flag){
        temp.push(item)
      }
    })
    console.warn('temp',temp)
    return <FinishProject Language={I18n.currentLocale()} Language={I18n.currentLocale()} array={temp} jobid={this.props.jobid} lat={this.state.lat} log={this.state.log}/>
  }
  _finishProject = () => {
    // if (this.state.group==2) {
    //   return this.finishProjectZone()
    // }
    // if(this.state.group == 3){
    //   return this.finishProjectType()
    // }
    if(this.state.sortERP){
      return this._FinishProjectERP()
    }else{
      if(this.state.az){
        if(this.state.all){
          return this._finishProject1()
        }else{
          if (this.state.group == 2) {
            return this.finishProjectZone()
          } else if (!this.state.group == 2) {
            return this.finishProjectType()
          }
        }
      }else{
        if(!this.state.all){
          if(this.state.before == 'ZONE'){
            if (this.state.group == 2) {
              return this.finishProjectZone()
            } else if (!this.state.group == 2) {
              return this.finishProjectType()
            }
          }else{
            return this.finishProjectType()
          }
        }else{
          return this.finishProjectType()
        }
      }
    }
    
    
  }
  _finishProject1 =()=>{
    var pileall = this.state.pileall
    pileall = pileall.filter(pile => {
      if (
        typeof this.props.result !== "undefined" &&
        !pile.pile_no.toLowerCase().includes(this.props.result.toLowerCase())
      ) {
        return
      }
      return pile.status == 2
    })
    // var t0 = new Date().getTime()
    pileall = pileall.sort((a, b) => {
      if (this.state.sortdate) {
        var aa = moment(a.end_date, "DD/MM/YYYY")
        var bb = moment(b.end_date, "DD/MM/YYYY")
        return bb.diff(aa)
      } else {
        return naturalSort(a.pile_no, b.pile_no)
      }
    })
    // var t1 = new Date().getTime()
    // Alert.alert("SLOW","Call to sort by date took " + (t1 - t0) + " milliseconds.")
    // console.log('pileall',pileall[0].status)
    return <FinishProject Language={I18n.currentLocale()} array={pileall} jobid={this.props.jobid} lat={this.state.lat} log={this.state.log}/>
  }
  finishProjectZone = () => {
    var pileall = this.state.pileall
    pileall = pileall.filter(pile => {
      if (
        typeof this.props.result !== "undefined" &&
        !pile.pile_no.toLowerCase().includes(this.props.result.toLowerCase())
      ) {
        return
      }
      return pile.status == 2
    })
    pileall = pileall.sort((a, b) => {
      if (this.state.sortdate) {
        var aa = moment(a.end_date, "DD/MM/YYYY")
        var bb = moment(b.end_date, "DD/MM/YYYY")
        return bb.diff(aa)
      } else {
        return naturalSort(a.pile_no, b.pile_no)
      }
    })
    var pileszone = this.seperateZone(pileall)
    var zoneName = Object.keys(pileszone)
    var container = []
    var count = 0
    var countzone = 50000
    for (key in pileszone) {
      var temp = []
      pileszone[key].map(value => {
        // console.log(value, key);
        // console.log(value.pilename);
        temp.push(value)
      })
      container.push(<Zone name={key} key={countzone} type={I18n.t('typezone.zone')}/>)
      if(this.state.now == 'TYPE' && this.state.az == false){
        var pilestype = this.seperateType(pileszone[key])
        var container1 = []
        var count1 = 0
        var countzone1 = 50000
        console.log(pilestype)
        for (data in pilestype) {
          container1.push(<Zone name={data} key={countzone1} type={I18n.t('typezone.type')} sub={true}/>)
          container1.push(<FinishProject Language={I18n.currentLocale()} array={pilestype[data]} key={count1} jobid={this.props.jobid} lat={this.state.lat} log={this.state.log}/>)
        }
        container.push(container1)
        count1 += 1
        countzone1 += 1
      }else{
        container.push(<FinishProject Language={I18n.currentLocale()} array={temp} key={count} jobid={this.props.jobid} lat={this.state.lat} log={this.state.log}/>)
        count += 1
        
      }
      countzone += 1
    }
    return container
  }

  finishProjectType = () => {
    var pileall = this.state.pileall
    pileall = pileall.filter(pile => {
      if (
        typeof this.props.result !== "undefined" &&
        !pile.pile_no.toLowerCase().includes(this.props.result.toLowerCase())
      ) {
        return
      }
      return pile.status == 2
    })
    pileall = pileall.sort((a, b) => {
      if (this.state.sortdate) {
        var aa = moment(a.end_date, "DD/MM/YYYY")
        var bb = moment(b.end_date, "DD/MM/YYYY")
        return bb.diff(aa)
      } else {
        return naturalSort(a.pile_no, b.pile_no)
      }
    })
    var pilestype = this.seperateType(pileall)
    var zoneName = Object.keys(pilestype)
    var container = []
    var count = 0
    var counttype = 50000
    for (key in pilestype) {
      var temp = []
      pilestype[key].map(value => {
        // console.log(value, key);
        // console.log(value.pilename);
        temp.push(value)
      })
      container.push(<Zone name={key} key={counttype} type={I18n.t('typezone.type')}/>)
      if(this.state.now == 'ZONE' && this.state.all == false){
        var pileszone = this.seperateZone(pilestype[key])
        var container1 = []
        var count1 = 0
        var countzone1 = 50000
        for (data in pileszone) {
          container1.push(<Zone name={data} key={countzone1} type={I18n.t('typezone.zone')} sub={true}/>)
          container1.push(<FinishProject Language={I18n.currentLocale()} array={pileszone[data]} key={count1} jobid={this.props.jobid} lat={this.state.lat} log={this.state.log}/>)
        }
        container.push(container1)
        count1 += 1
        countzone1 += 1
      }else{
        container.push(<FinishProject Language={I18n.currentLocale()} array={temp} key={count} jobid={this.props.jobid} lat={this.state.lat} log={this.state.log}/>)
        count += 1
        
      }
      counttype += 1
    }
    return container
  }

  backToProject() {
    Alert.alert("Back", I18n.t('projectlist.backToProject'), [{ text: "Cancel" }, { text: "Ok", onPress: () => 
    {
      Actions.pop({result:null}) 
    }

  }])
  }

  render() {
    const buttons = [
      { element: this._notStartButton },
      { element: this._unfinishButton },
      { element: this._finishButton }
    ]
    const { selectedIndex } = this.state
    // const pileall = this.props.items
    console.warn('pileall leng',this.props.loading)
    if (this.props.loading || this.state.loading_refresh==true) {
      return (
        <Container style={{backgroundColor:GRAY}}>
          <Content>
            {__DEV__ && <Text>{this.props.loading.toString()}</Text>}
            <View style={{ flex: 1 }}>
              <Spinner
                visible={true}
                textContent={"Loading..."}
                textStyle={{ color: "#FFF" }}
                overlayColor="rgba(0, 0, 0, 0.5)"
              />
            </View>
          </Content>
        </Container>
      )
    } else {
      return (
        <Container style={{backgroundColor:GRAY}}>
          {this.state.loader?<Loader/>:<View/>}
          <View style={styles.title}>
            <View style={{ marginLeft: 10, width: "75%" }}>
              <TouchableOpacity onPress={() => this.backToProject()}>
                <Text style={styles.textTitle}>{this.props.project}</Text>
              </TouchableOpacity>
            </View>
            <View>
              <Ic
                reverse
                size={DeviceInfo.isTablet() ? 25 : 15}
                name="refresh"
                type="font-awesome"
                color={MAIN_COLOR}
                onPress={() => this.iconPressedRefresh()}  
              />
            </View>
            <View>
              <Ic
                reverse
                size={DeviceInfo.isTablet() ? 25 : 15}
                name="file-text"
                type="font-awesome"
                color={MAIN_COLOR}
                onPress={() => this.iconPressed()}
              />
            </View>
          </View>
          <View style={styles.buttonGroup}>
            <ButtonGroup
              selectedButtonStyle={{ backgroundColor: SUB_COLOR }}
              onPress={this.updateIndex}
              selectedIndex={selectedIndex}
              buttons={buttons}
              containerStyle={{ height: DeviceInfo.isTablet() ? 80 : 50 }}
            />
          </View>
          <View style={styles.sortButtonStyle}>{this._sortButton(this.state.selectedIndex)}</View>
          <ScrollView style={{ backgroundColor: "#FFF" }} keyboardShouldPersistTaps="always" onScroll={this._onScroll}>
            <View style={{ marginTop: 10 }}>
              {this.state.selectedIndex == 0 && this.notStartProject()}
              {this.state.selectedIndex == 1 && this.unFinish()}
              {this.state.selectedIndex == 2 && this._finishProject()}
            </View>
          </ScrollView>
        </Container>
      )
    }
  }
}

const styles = StyleSheet.create({
  textTitle: {
    fontSize: DeviceInfo.isTablet() ? 35 : 20,
    fontFamily: "THSarabunNewBold",
    color: MAIN_COLOR
  },
  sortButton: {
    borderWidth: 2,
    borderColor: MAIN_COLOR,
    borderRadius: 10,
    marginLeft: 20,
    marginRight: 20,
    width: Dimensions.get("window").width / 5,
    height: DeviceInfo.isTablet() ? 32 : 25,
    justifyContent: "center",
    alignItems: "center"
  },
  sortButtonPressed: {
    backgroundColor: MAIN_COLOR
  },
  sortButtonText: {
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    fontSize: DeviceInfo.isTablet() ? 25 : 15,
    color: MAIN_COLOR
  },
  sortButtonTextPressed: {
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    fontSize: 15,
    color: "#fff"
  },
  sortButtonView: {
    flexDirection: "row",
    width: "100%",
    paddingLeft: 10,
    paddingRight: 10,
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center",
    height: 25
  },
  title: {
    flexDirection: "row",
    backgroundColor: "#FFF",
    alignItems: "center"
  },
  buttonGroup: {
    height: DeviceInfo.isTablet() ? 90 : 60,
    marginTop: 5,
    backgroundColor: "#FFF",
    justifyContent: "center",
    alignItems: "center"
  },
  sortButtonStyle: {
    // backgroundColor: '#FFF',
    height: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  tabBox: {
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  textTablet: {
    fontSize: DeviceInfo.isTablet() ? 23 : 14
  },
  zone: {
    // height: DeviceInfo.isTablet() ? 150 : 130,
    height: DeviceInfo.isTablet() ? 150 : 120,
    width: "100%",
    backgroundColor: "#7ED12B",
    justifyContent: "center"
  },
  textView: {
    marginLeft: 10,
    flexDirection: "row",
    alignItems: 'center'
  },
  text: {
    color: "#FFF",
    fontSize: DeviceInfo.isTablet() ? 25 : 17
  }
})

const mapStateToProps = state => {
  // console.log(state.pile)
  return {
    error: state.pile.error,
    items: state.pile.items,
    status0: state.pile.status0,
    status1: state.pile.status1,
    status2: state.pile.status2,
    loading: state.pile.loading,
    recent: state.pile.recent,
    idjob: state.pile.jobid,
    totalpile:state.pile.totalpile,
    searchpile:state.pile.searchpile,
    input:state.pile.input,
    clearerp:state.pile.clearerp,
    loading1: state.pile.loading1,
    Unblockerp_success:state.pile.Unblockerp_success,
    Unblockerp_random:state.pile.Unblockerp_random,
    data_Unblockerp:state.pile.data_Unblockerp,

    location_lat: state.location.location_lat,
    location_log:state.location.location_log,
  }
}

export default connect(mapStateToProps, { pileItems,pileitemsclear })(ProjectList)
