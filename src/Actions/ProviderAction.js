import axios from "axios"
import AsyncStorage from '@react-native-community/async-storage';
import I18n from "../../assets/languages/i18n"
import { API_IP } from "../Constants/Constant"
import { PROVIDER_ERROR, PROVIDER_ITEMS, DUPLICATE_LOGIN } from "./types"
import { getVersionApp } from './AuthAction'

export const providerItems = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.language = I18n.locale
    data.Version =  getVersionApp()

    axios
      .get(API_IP + "provider", { params: data })
      .then(function(response) {
        console.log("providerItems", response)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          return dispatch({ type: PROVIDER_ITEMS, payload: response.data.ProviderList })
        } else {
          return dispatch({ type: PROVIDER_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log("providerItems", error.message)
        dispatch({ type: PROVIDER_ERROR, payload: error.message })
      })
  }
}
