import axios from "axios"
import AsyncStorage from '@react-native-community/async-storage';
import { API_IP_GET } from "../Constants/Constant"
import firebase from "react-native-firebase"
import { TREMIELIST, TREMIEERROR, DUPLICATE_LOGIN, TREMIELOADING } from "./types"
import { getVersionApp } from './AuthAction'

export const getTremieList = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.Version =  getVersionApp()

    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }

    axios
      .get(API_IP_GET + "step10/gettremielist", { params: data })
      .then(function(response) {
        // console.warn(response.data)
        dispatch({ type: TREMIELOADING, loading: true })
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          var total = 0
          var pipe = []
          var tremiesizeid = null
          var last = false
          response.data.TremieList.map(data => {
            total += data.Length
            pipe.push(data.Length)
            tremiesizeid = data.TremieSize.Id
            last = data.IsLast
          })
          // console.warn('Depth_child',response.data.Depth_child)
          return dispatch({ type: TREMIELIST, payload: response.data.TremieList, volume: total, pipe: pipe, depth: response.data.Depth,depth_child: response.data.Depth_child ,loading: false, tremiesizeid, haslast: last })
        } else {
          return dispatch({ type: TREMIEERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        firebase.crashlytics().log('step10/gettremielist')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        return dispatch({ type: TREMIEERROR, payload: error.message })
      })
  }
}