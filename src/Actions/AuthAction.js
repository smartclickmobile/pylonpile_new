import axios from "axios"
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from "react-native-router-flux"
import { API_IP, API_IP_GET } from "../Constants/Constant"
import {
  AUTH_LOGIN_ERROR,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGOUT_SUCCESS,
  AUTH_LOGOUT_ERROR,
  AUTH_LOGIN_DUP,
  AUTH_RESET,
  AUTH_CHANGEPASS_SUCCESS,
  AUTH_CHANGEPASS_FAIL,
  AUTH_LOADING,
  USER_INFO_SUCCESS,
  USER_INFO_ERROR
} from "./types"
import firebase from "react-native-firebase"
import DeviceInfo from "react-native-device-info"

export const authCheck = () => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")

    if (token == null || token == "") {
      Actions.login({ type: "reset" })
    } else {
      let obj = {
        token
      }

      const config = {
        headers: {
          "content-type": "application/x-www-form-urlencoded"
        },
        timeout: 10000
      }

      var data = []
      for (var property in obj) {
        var encodedKey = encodeURIComponent(property)
        var encodedValue = encodeURIComponent(obj[property])
        data.push(encodedKey + "=" + encodedValue)
      }
      let version =  getVersionApp()
      data.push('Version='+version)
      data = data.join("&")

      axios
        .post(API_IP + "login", data, config)
        .then(function(response) {
          // console.log(response)
          if (response.data.Message.Code == "001") {
            AsyncStorage.setItem("token", response.data.UserInfo.token)
            Actions.reset("drawer")
          } else {
            Actions.login({ type: "reset" })
          }
        })
        .catch(function(error) {
          console.log(error.response)
          Actions.login({ type: "reset" })
        })
    }
  }
}

export const TokenautoLogin = obj => {
  return async dispatch => {
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      },
      timeout: 10000
    }
    console.log('wowkk', obj)
    var data = []
    for (var property in obj) {
      
      var encodedKey = encodeURIComponent("token")
      console.log('wowkkkkkkkkk', encodedKey)
      var encodedValue = encodeURIComponent(obj[property].substring(22))
      data.push(encodedKey + "=" + encodedValue)
    }
    let version =  getVersionApp()
    data.push('Version='+version)
    data = data.join("&")
    axios
      .post(API_IP + "login", data, config)
      .then(function(response) {
        console.log('testยิงข้อมูล deeplink!!!',response)
        console.log('DashBoardlist!!!!!!!!!!!!!!!!!!!!!!',response.data)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: AUTH_LOGIN_DUP, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          
          firebase.messaging().subscribeToTopic("employee_" + String(response.data.UserInfo.employeeid))
          firebase.crashlytics().setUserIdentifier(String(response.data.UserInfo.username))
          firebase.analytics().setUserId(String(response.data.UserInfo.username))
          firebase.analytics().logEvent("login", { name: String(response.data.UserInfo.username), id: String(response.data.UserInfo.employeeid) })
          AsyncStorage.multiSet([
            ["token", response.data.UserInfo.token],
            ["userid", String(response.data.UserInfo.userid)],
            ["employeeid", String(response.data.UserInfo.employeeid)],
            ["username", String(response.data.UserInfo.username)],
            ["usergroupname", String(response.data.UserInfo.usergroupname)],
            ["qrcode", String(response.data.UserInfo.qrcode)],
            ["profilepic", String(response.data.UserInfo.picture)],
            ["fullname", String(response.data.UserInfo.firstname) + " " + String(response.data.UserInfo.lastname)],
            ["nickname", String(response.data.UserInfo.nickname)],
            ["user", String('')],
            ["password", String('')],
            ["position",String(response.data.UserInfo.position)]
          ]).then(value => {})
          return dispatch({ type: AUTH_LOGIN_SUCCESS, payload: response.data.UserInfo, dashboard: response.data.DashboardList})
        }else if (response.data.Message.Code == "105") {
          return dispatch({ type: AUTH_LOGIN_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        firebase.crashlytics().log('login')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        return dispatch({ type: AUTH_LOGIN_ERROR, payload: error.message })
      })
    }
}

export const TokenLogin = obj => {
  return async dispatch => {
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      },
      timeout: 10000
    }

    var data = []
    for (var property in obj) {
      var encodedKey = encodeURIComponent(property)
      var encodedValue = encodeURIComponent(obj[property])
      data.push(encodedKey + "=" + encodedValue)
    }
    let version =  getVersionApp()
    data.push('Version='+version)
    data = data.join("&")

    axios
      .post(API_IP + "login", data, config)
      .then(function(response) {
        console.log('testยิงข้อมูล deeplink!!!',response)
        console.log('DashBoardlist!!!!!!!!!!!!!!!!!!!!!!',response.data)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: AUTH_LOGIN_DUP, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          
          firebase.messaging().subscribeToTopic("employee_" + String(response.data.UserInfo.employeeid))
          firebase.crashlytics().setUserIdentifier(String(response.data.UserInfo.username))
          firebase.analytics().setUserId(String(response.data.UserInfo.username))
          firebase.analytics().logEvent("login", { name: String(response.data.UserInfo.username), id: String(response.data.UserInfo.employeeid) })
          AsyncStorage.multiSet([
            ["token", response.data.UserInfo.token],
            ["userid", String(response.data.UserInfo.userid)],
            ["employeeid", String(response.data.UserInfo.employeeid)],
            ["username", String(response.data.UserInfo.username)],
            ["usergroupname", String(response.data.UserInfo.usergroupname)],
            ["qrcode", String(response.data.UserInfo.qrcode)],
            ["profilepic", String(response.data.UserInfo.picture)],
            ["fullname", String(response.data.UserInfo.firstname) + " " + String(response.data.UserInfo.lastname)],
            ["nickname", String(response.data.UserInfo.nickname)],
            ["user", String('')],
            ["password", String('')],
            ["position",String(response.data.UserInfo.position)]
          ]).then(value => {})
          return dispatch({ type: AUTH_LOGIN_SUCCESS, payload: response.data.UserInfo, dashboard: response.data.DashboardList})
        }else if (response.data.Message.Code == "105") {
          return dispatch({ type: AUTH_LOGIN_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        firebase.crashlytics().log('login')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        return dispatch({ type: AUTH_LOGIN_ERROR, payload: error.message })
      })
    }
}

export const authLogin = obj => {
  return async dispatch => {
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      },
      timeout: 10000
    }

    var data = []
    for (var property in obj) {
      var encodedKey = encodeURIComponent(property)
      var encodedValue = encodeURIComponent(obj[property])
      data.push(encodedKey + "=" + encodedValue)
    }
    // dispatch()
    let version =  getVersionApp()
    data.push('Version='+version)
    console.log('authLogin log',version,data)
    
    data = data.join("&")

    axios
      .post(API_IP + "login", data, config)
      .then(function(response) {
        console.log('login data!!!',response.data)
        console.log('ว่าไงusername!!',obj.username)
        console.log('ว่าไงpassword!!',obj.password)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: AUTH_LOGIN_DUP, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          // console.warn('data1',response.data.DashboardList)
          firebase.messaging().subscribeToTopic("employee_" + String(response.data.UserInfo.employeeid))
          firebase.crashlytics().setUserIdentifier(String(response.data.UserInfo.username))
          firebase.analytics().setUserId(String(response.data.UserInfo.username))
          firebase.analytics().logEvent("login", { name: String(response.data.UserInfo.username), id: String(response.data.UserInfo.employeeid) })
          AsyncStorage.multiSet([
            ["token", response.data.UserInfo.token],
            ["userid", String(response.data.UserInfo.userid)],
            ["employeeid", String(response.data.UserInfo.employeeid)],
            ["username", String(response.data.UserInfo.username)],
            ["usergroupname", String(response.data.UserInfo.usergroupname)],
            ["qrcode", String(response.data.UserInfo.qrcode)],
            ["profilepic", String(response.data.UserInfo.picture)],
            ["fullname", String(response.data.UserInfo.firstname) + " " + String(response.data.UserInfo.lastname)],
            ["nickname", String(response.data.UserInfo.nickname)],
            ["user", String(obj.username)],
            ["password", String(obj.password)],
            ["position",String(response.data.UserInfo.position)]
          ]).then(value => {})
          return dispatch({ type: AUTH_LOGIN_SUCCESS, payload: response.data.UserInfo, dashboard: response.data.DashboardList})
        } else {
          return dispatch({ type: AUTH_LOGIN_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        firebase.crashlytics().log('login')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        return dispatch({ type: AUTH_LOGIN_ERROR, payload: error.message })
      })
  }
}



export const authLogout = () => {
  return async dispatch => {
    // firebase.messaging().deleteInstanceId()
    const token = await AsyncStorage.getItem("token")
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }
    const obj = {
      token
    }
    var data = []
    for (var property in obj) {
      var encodedKey = encodeURIComponent(property)
      var encodedValue = encodeURIComponent(obj[property])
      data.push(encodedKey + "=" + encodedValue)
    }
    let version =  getVersionApp()
    data.push('Version='+version)
    data = data.join("&")
    axios
      .post(API_IP + "logout", data, config)
      .then(function(response) {
        // console.log(response)
          AsyncStorage.removeItem("token")
        if (response.data.Message.Code == "001") {
          Actions.replace("login")
          AsyncStorage.removeItem("token")
          AsyncStorage.clear()
          return dispatch({ type: AUTH_LOGOUT_SUCCESS, payload: response.data.Message.Message })
        } else {
          Actions.replace("login")
          return dispatch({ type: AUTH_LOGOUT_SUCCESS, payload: response.data.Message.Message })
        }
      })
      .catch(function(response) {
        Actions.replace("login")
        return dispatch({ type: AUTH_LOGOUT_ERROR, payload: response.message })
      })
  }
}

export const authReset = () => dispatch => dispatch({ type: AUTH_RESET })

export const authChangePass = obj => {
  return async dispatch => {
    dispatch({ type: AUTH_LOADING })
    const token = await AsyncStorage.getItem("token")
    obj.token = token

    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }

    var data = []
    for (var property in obj) {
      var encodedKey = encodeURIComponent(property)
      var encodedValue = encodeURIComponent(obj[property])
      data.push(encodedKey + "=" + encodedValue)
    }
    let version =  getVersionApp()
    data.push('Version='+version)
    data = data.join("&")

    axios
      .post(API_IP_GET + "changepassword", data, config)
      .then(response => {
        // console.warn(response)
        if (response.data.Code == '001') {
          return dispatch({ type: AUTH_CHANGEPASS_SUCCESS, payload: response.data.Message })
        }
        else {
          return dispatch({ type: AUTH_CHANGEPASS_FAIL, payload: response.data.Message })
        }
      })
      .catch(error => {
        firebase.crashlytics().log('changepassword')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        return dispatch({ type: AUTH_CHANGEPASS_FAIL, payload: error.message })
      })
  }
}

export const getUserinfo = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.Version =  getVersionApp()
    console.log('viewUserไหมล่ะ!!!',data)
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }

    axios
      .get(API_IP + "viewuser", { params: data })
      .then(function(response) {
        console.log('response viewuser',response.data)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          return dispatch({ type: USER_INFO_SUCCESS, payload: response.data.ViewUser,dashboard: response.data.DashboardList})
        } else {
          return dispatch({ type: USER_INFO_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        firebase.crashlytics().log('getUserinfo')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: ConcreteError10, payload: error.message })
      })
  }
}

export const getVersionApp = () => {
    let version = DeviceInfo.getVersion()
    let VerArr = version.split('-');
    console.log('getVersionApp',VerArr)

    return VerArr[0]


}