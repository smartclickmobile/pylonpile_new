import axios from "axios"
import AsyncStorage from '@react-native-community/async-storage';
import { DUPLICATE_LOGIN, ConcreteList, ConcreteError, ConcreteList10, ConcreteError10, ConcreteRecord, ConcreteRecordError } from "./types"
import { API_IP_GET } from "../Constants/Constant"
import firebase from "react-native-firebase"
import { getVersionApp } from './AuthAction'

export const getConcreteList = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.Version =  getVersionApp()
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }

    axios
      .get(API_IP_GET + "step09/getconcretetruckregister", { params: data })
      .then(function(response) {
        // console.warn(response.data)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          var concreteid = null
          var mixid = null
          var total = 0
          var status = 0
          var indexarr = []
          response.data.ConcreteTruckRegisterList.map((data, index) => {
            // 3 is Reject so it not count toward totalconcrete
            if (data.ApproveStatus != 3 && data.IsReject == false) {
              total += data.TruckConcreteVolume
            }
            indexarr.push(index + 1)
            concreteid = data.ConcreteSuplierId
            mixid = data.MixNoId
          })
          response.data.Step9.Status !== undefined ? status = response.data.Step9.Status : 0
          // status = response.data.Step9.Status || 0
          return dispatch({ type: ConcreteList, payload: response.data.ConcreteTruckRegisterList, volume: total, status, index: indexarr, concreteid, mixid })
        } else {
          return dispatch({ type: ConcreteError, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        firebase.crashlytics().log('step09/getconcretetruckregister')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: ConcreteError, payload: error.message })
      })
  }
}

export const getConcreteList10 = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.Version =  getVersionApp()
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }

    axios
      .get(API_IP_GET + "step10/getconcretetruckregister", { params: data })
      .then(function(response) {
        // console.warn(response.data)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          var total = 0
          response.data.ConcreteTruckRegisterList.map(data => {
            total += data.TruckConcreteVolume
          })
          return dispatch({ type: ConcreteList10, payload: response.data.ConcreteTruckRegisterList, volume: total })
        } else {
          return dispatch({ type: ConcreteError10, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        firebase.crashlytics().log('step10/getconcretetruckregister')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: ConcreteError10, payload: error.message })
      })
  }
}

export const getConcreteRecord = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.Version =  getVersionApp()
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }

    axios
      .get(API_IP_GET + "step10/getconcreterecord", { params: data })
      .then(function(response) {
        // console.log('CONCRETE RECORD', response.data)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          //Status
          var status = response.data.Step10.Status || 0
          //TODO TREMIELIST
          var tremielist = []
          var tremievolume = 0
          response.data.TremieList.map(data => {
            tremielist.push(data.Length)
            tremievolume += data.Length
          })
          var tremiecuttotal = 0
          var tremiecut = []
          var tremieleft = []
          var tremiedeep = []

          var concretecumulative = 0
          var tremiecutcumulative = 0
          var tremiecutcumulativelength = 0
          var concretecumulativeArr = [0]
          var tremiecutcumulativeArr = [0]
          var tremiecutcumulativelengthArr = [0]
          var lasttruck = false
          var lastdepth = null
          var lastdepthconcrete = null
          var lastdepthconcreteArr = [null]
          var lastdepthArr = [null]
          
          var stoppouringarr = 0
          var lastrstoppouring  =null
          var beforestoppouringarr = null
          
          
          response.data.ConcreteRecordList.map((data,index) => {
            let  beforelastrstoppouring  =false
            // console.warn("ConcreteRecordList")
            if (data.IsLastTruck) {
              lasttruck = true
            }
            var concrete = tremievolume - data.Depth
            // concretecumulative += data.ConcreteCalculateVolumn!=''&& data.ConcreteCalculateVolumn!=null&& data.ConcreteCalculateVolumn!=undefined?(data.ConcreteCalculateVolumn):((data.IsLastTruck == true ? (data.LastTruckConcreteVolume !==null && data.LastTruckConcreteVolume!=="" ? (data.LastTruckConcreteVolume) : (data.ConcreteTruckRegister.TruckConcreteVolume)) : data.ConcreteTruckRegister.TruckConcreteVolume))
            // concretecumulative +=  data.IsLastTruck == true ? (data.LastTruckConcreteVolume !==null && data.LastTruckConcreteVolume!=="" ? (data.LastTruckConcreteVolume) : data.ConcreteCalculateVolumn!=''&& data.ConcreteCalculateVolumn!=null&& data.ConcreteCalculateVolumn!=undefined?(data.ConcreteCalculateVolumn):(data.ConcreteTruckRegister.TruckConcreteVolume) ):data.ConcreteCalculateVolumn!=''&& data.ConcreteCalculateVolumn!=null&& data.ConcreteCalculateVolumn!=undefined?(data.ConcreteCalculateVolumn):(data.ConcreteTruckRegister.TruckConcreteVolume)  
            

          
            if(data.ConcreteCalculateVolumn!=undefined&&data.ConcreteCalculateVolumn!=''&& data.ConcreteCalculateVolumn!=null &&data.StopPouringConcrete == true){
              stoppouringarr += data.ConcreteCalculateVolumn
            }else if(data.StopPouringConcrete == false  ){
              if(index-1>=0 &&response.data.ConcreteRecordList[index-1]){
                // console.warn('response.data.ConcreteRecordList',response.data.ConcreteRecordList[index-1],'index-1',index-1,' response.data.ConcreteRecordList[index-1].StopPouringConcrete', response.data.ConcreteRecordList[index-1].StopPouringConcrete)
               if(response.data.ConcreteRecordList[index-1].StopPouringConcrete==true){
                beforelastrstoppouring = true
                
               }else{
                stoppouringarr= 0
               }
              }
             
            }

            if( data.IsLastTruck == true){
              if(data.LastTruckConcreteVolume !==null && data.LastTruckConcreteVolume!==""){
              
                // if( data.ConcreteCalculateVolumn!=undefined&&data.ConcreteCalculateVolumn!=''&& data.ConcreteCalculateVolumn!=null ){
                //   // concretecumulative +=data.ConcreteCalculateVolumn
                //   console.warn('api concretecumulative',data)
                //   concretecumulative = concretecumulative - stoppouringarr
                //   console.warn('api concretecumulative',concretecumulative)
                //   concretecumulative +=data.LastTruckConcreteVolume
                // }else{
                //   concretecumulative += data.LastTruckConcreteVolume
                // }
                if(data.StopPouringConcrete == true){
                  concretecumulative +=data.LastTruckConcreteVolume
                }else{
                  if(beforelastrstoppouring==true){
                    concretecumulative = concretecumulative - stoppouringarr
                    stoppouringarr= 0
                    concretecumulative  +=data.LastTruckConcreteVolume
                  }else{
                    concretecumulative +=data.LastTruckConcreteVolume
                  }
                }

                
               
              }else{

                // console.log('beforelastrstoppouring==true',index,stoppouringarr,data.ConcreteCalculateVolumn,beforelastrstoppouring,data.StopPouringConcrete)
                if(data.StopPouringConcrete == true){
                  if( data.ConcreteCalculateVolumn!=undefined&&data.ConcreteCalculateVolumn!=''&& data.ConcreteCalculateVolumn!=null ){
                    concretecumulative += data.ConcreteCalculateVolumn
                  }
                }else{
                
                  if(beforelastrstoppouring==true){
                    concretecumulative = concretecumulative - stoppouringarr
                    stoppouringarr= 0
                    concretecumulative += data.ConcreteTruckRegister.TruckConcreteVolume
                  }else{
                    concretecumulative += data.ConcreteTruckRegister.TruckConcreteVolume
                  }
                  
                }
              }
            }else{

              if(data.StopPouringConcrete == true){
                if( data.ConcreteCalculateVolumn!=undefined&&data.ConcreteCalculateVolumn!=''&& data.ConcreteCalculateVolumn!=null ){
                  concretecumulative += data.ConcreteCalculateVolumn
                }
              }else{
                if(beforelastrstoppouring==true){
                  concretecumulative = concretecumulative - stoppouringarr
                  stoppouringarr= 0
                  concretecumulative += data.ConcreteTruckRegister.TruckConcreteVolume
                }else{
                  concretecumulative += data.ConcreteTruckRegister.TruckConcreteVolume
                }
                
              }
              // if( data.ConcreteCalculateVolumn!=undefined&&data.ConcreteCalculateVolumn!=''&& data.ConcreteCalculateVolumn!=null ){
              //   if(data.StopPouringConcrete == true){
              //     concretecumulative += data.ConcreteCalculateVolumn
              //   }else{
              //     concretecumulative = concretecumulative - stoppouringarr
              //     concretecumulative += data.ConcreteTruckRegister.TruckConcreteVolume
              //   }
              // }else{
              //   concretecumulative += data.ConcreteTruckRegister.TruckConcreteVolume
              // }
            }
              
            // console.warn('concrerecord',data.ConcreteCalculateVolumn,data.LastTruckConcreteVolume,data.ConcreteTruckRegister.TruckConcreteVolume,data.ConcreteTruckRegister.TruckConcreteVolume,data.IsLastTruck )
            concretecumulativeArr.push(concretecumulative)

            if (data.Depth != null) {
              lastdepthArr.push(data.Depth)
              lastdepth = data.Depth
              lastdepthconcrete = concretecumulative
              lastdepthconcreteArr.push(concretecumulative)
            }
            else {
              if (lastdepth == null) {
                lastdepthArr.push(null)
              }
              else {
                lastdepthArr.push(lastdepth)
              }
              if (lastdepthconcrete == null) {
                lastdepthconcreteArr.push(null)
              }
              else {
                lastdepthconcreteArr.push(lastdepthconcrete)
              }
            }

            if (data.TremieCutCount != null && data.TremieCutCount > 0) {
              tremiecutcumulative += data.TremieCutCount
              tremiecutcumulativeArr.push(tremiecutcumulative)
              var tremiecuttemp = 0
              console.log(tremielist)
              for (var i=0; i < data.TremieCutCount; i++) {
                tremiecuttemp += tremielist[tremielist.length - 1 - tremiecuttotal]
                tremiecuttotal += 1
                console.log(tremiecuttotal, tremiecuttemp)
              }
              tremiecutcumulativelength += tremiecuttemp
              tremiecutcumulativelengthArr.push(tremiecutcumulativelength)
              tremievolume = tremievolume - tremiecuttemp
              tremiecut.push(tremiecuttemp)
              tremieleft.push(tremievolume.toFixed(2))
              tremiedeep.push((concrete - tremiecuttemp).toFixed(2))
            }
            else {
              tremiecut.push(0)
              tremieleft.push(tremievolume.toFixed(2))
              tremiedeep.push(concrete.toFixed(2))
              tremiecutcumulativeArr.push(tremiecutcumulative)
              tremiecutcumulativelengthArr.push(tremiecutcumulativelength)
            }
          })

          return dispatch({ type: ConcreteRecord, payload: response.data.ConcreteRecordList, tremiecut, tremieleft, tremiedeep, concretecumulative, tremiecutcumulative, tremiecutcumulativelength, record: response.data.IsBreak,concretecumulativeArr,tremiecutcumulativeArr,tremiecutcumulativelengthArr, status, lasttruck, lastdepthArr, lastdepthconcreteArr })
        } else {
          return dispatch({ type: ConcreteRecordError, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        firebase.crashlytics().log('step10/getconcreterecord')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: ConcreteRecordError, payload: error.message })
      })
  }
}