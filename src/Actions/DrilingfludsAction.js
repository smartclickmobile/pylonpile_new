import axios from "axios"
import AsyncStorage from '@react-native-community/async-storage';
import { DUPLICATE_LOGIN, DrillingFluidslist, DrillingFluidsError} from "./types"
import { API_IP_GET } from "../Constants/Constant"
import firebase from "react-native-firebase"
import { getVersionApp } from './AuthAction'

export const getDrillingFluids = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.Version =  getVersionApp()
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }

    axios
      .get(API_IP_GET + "step04/getdrillingfluids", { params: data })
      .then(function(response) {
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          // console.log('testtt',response.data.DrillingFluids)
          return dispatch({ type: DrillingFluidslist, payload: response.data.DrillingFluids })
        } else {
          return dispatch({ type: DrillingFluidsError, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        firebase.crashlytics().log('step04/getdrillingfluids')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: DrillingFluidsError, payload: error.message })
      })
  }
}