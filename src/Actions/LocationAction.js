import axios from "axios"
import { LOCATION} from "./types"
import { API_IP_GET, API_WEATHER } from "../Constants/Constant"
import firebase from "react-native-firebase"

export const setlocation = data => {
  return async dispatch => {
      if(data != undefined && data != null){
        return dispatch({ type: LOCATION, payload: data })
      }
    
  }
}