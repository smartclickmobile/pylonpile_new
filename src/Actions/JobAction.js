import axios from "axios"
import AsyncStorage from '@react-native-community/async-storage';
import { API_IP } from "../Constants/Constant"
import { JOBLIST_ERROR, JOBLIST_SUCCESS, JOBDETAIL_ERROR, JOBDETAIL_SUCCESS, DUPLICATE_LOGIN, JOBLIST_CLEAR, JOBDETAIL_CLEAR } from "./types"
import firebase from "react-native-firebase"
import { getVersionApp } from './AuthAction'

export const jobItems = () => {
  return async dispatch => {
    console.log('tokenจริงหรือเปล่าาgssgspot',await AsyncStorage.getItem("token"))
    const token = await AsyncStorage.getItem("token")
    const employeeid = await AsyncStorage.getItem("employeeid")
    var data = {
      token,
      employeeid
    }
    data.Version =  getVersionApp()
    console.log('jobItems log test',data)
    axios
      .get(API_IP + "joblist", { params: data })
      .then(function(response) {
        // console.log(response)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          var haveperm = []
          var noperm = []
          response.data.JobLists.map((data, index) => {
            if (data.permission_flag) {
              haveperm.push(data)
            }
            else {
              noperm.push(data)
            }
          })
          return dispatch({ type: JOBLIST_SUCCESS, payload: response.data.JobLists, haveperm, noperm })
        } else {
          return dispatch({ type: JOBLIST_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        firebase.crashlytics().log('joblist')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: JOBLIST_ERROR, payload: error.message })
      })
  }
}

export const getJobDetail = data => {
  return async dispatch => {
    var formBody = []
    for (var property in data) {
      var encodedKey = encodeURIComponent(property)
      var encodedValue = encodeURIComponent(data[property])
      formBody.push(encodedKey + "=" + encodedValue)
    }
    let version =  getVersionApp()
    formBody.push('Version='+version)
    formBody = formBody.join("&")
    // console.warn('getJobDetail',formBody)
    try {
      var response = await fetch(API_IP + "jobdetail", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        body: formBody,
        timeout: 3000
      })
      if (response.ok) {
        let responseJson = await response.json()
        var message = responseJson.Message
        console.log('jobdetail',responseJson)
        if (message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: message.Message })
        } else if (message.Code != "001") {
          return dispatch({ type: JOBDETAIL_ERROR, payload: message.Message })
        } else {
          return dispatch({ type: JOBDETAIL_SUCCESS, payload: responseJson })
        }
      } else {
        return dispatch({ type: JOBDETAIL_ERROR, payload: "error" })
      }
    } catch (error) {
      firebase.crashlytics().log('jobdetail')
      firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
      return dispatch({ type: JOBDETAIL_ERROR, payload: error.message })
    }
  }
}
export const jobclear = data =>{
  return dispatch => {
    
    dispatch({ type: JOBLIST_CLEAR})
    return dispatch({ type: JOBDETAIL_CLEAR})
  }
}
