import axios from "axios"
import AsyncStorage from '@react-native-community/async-storage';
import I18n from "../../assets/languages/i18n"
import { API_IP } from "../Constants/Constant"
import { STEEL_CAGE_ERROR, STEEL_CAGE_ERROR_RESET, STEEL_CAGE_ITEMS, DUPLICATE_LOGIN } from "./types"
import { getVersionApp } from './AuthAction'

export const steelCageItems = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.language = I18n.locale
    data.Version =  getVersionApp()

    axios
      .get(API_IP + "steelcage", { params: data })
      .then(function(response) {
        console.log(response)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          return dispatch({
            type: STEEL_CAGE_ITEMS,
            payload: response.data.Steelcage.SectionList,
            steelcage_no: response.data.Steelcage.steelcage_no
          })
        } else {
          return dispatch({ type: STEEL_CAGE_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error.response)
        dispatch({ type: STEEL_CAGE_ERROR, payload: error.message })
      })
  }
}

export const steelCageErrorReset = data => {
  return async dispatch => dispatch({ type: STEEL_CAGE_ERROR_RESET })
}
