import axios from "axios"
import AsyncStorage from '@react-native-community/async-storage';
import I18n from "../../assets/languages/i18n"
import { API_IP } from "../Constants/Constant"
import { STEEL_SEC_DETAIL_ERROR, STEEL_SEC_DETAIL_ERROR_RESET, STEEL_SEC_DETAIL, DUPLICATE_LOGIN } from "./types"
import { getVersionApp } from './AuthAction'

export const steelSecDetail = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.language = I18n.locale
    data.Version =  getVersionApp()

    axios
      .get(API_IP + "steelsecdetail", { params: data })
      .then(function(response) {
        console.log(response)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          return dispatch({ type: STEEL_SEC_DETAIL, payload: response.data.SectionDetail })
        } else {
          return dispatch({ type: STEEL_SEC_DETAIL_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        dispatch({ type: STEEL_SEC_DETAIL_ERROR, payload: error.message })
      })
  }
}

export const steelSecDetailErrorReset = data => {
  return async dispatch => dispatch({ type: STEEL_SEC_DETAIL_ERROR_RESET })
}
