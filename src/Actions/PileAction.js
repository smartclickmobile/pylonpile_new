import axios from "axios"
import { Alert } from "react-native"
import AsyncStorage from '@react-native-community/async-storage';
import { API_IP, API_IP_GET, API_WEATHER } from "../Constants/Constant"
import {
  PILE_ERROR,
  PILE_ITEM,
  PILE_ITEMS,
  PILE_VALUE_INFO,
  PILE_VALUE_INFO_RESET,
  PILE_MASTER_INFO,
  PILE_PILE01_SUCCESS,
  PILE_PILE01_SUCCESS_1,
  PILE_PILE01_SUCCESS_3,
  PILE_PILE02_SUCCESS,
  PILE_PILE02_SUCCESS_1,
  PILE_PILE03_SUCCESS,
  PILE_PILE03_SUCCESS_1,
  PILE_PILE03_SUCCESS_2,
  PILE_PILE03_SUCCESS_3,
  PILE_PILE03_SUCCESS_4,
  PILE_PILE03_APPROVE_SUCCESS,
  PILE_PILE03_APPROVE_FAIL,
  PILE_PILE03_CANCEL_APPROVE_SUCCESS,
  PILE_PILE03_CANCEL_APPROVE_FAIL,
  PILE_PILE04_SUCCESS,
  PILE_PILE04_DELETE_SUCCESS,
  PILE_PILE04_CLEAR_ID,
  PILE_PILE05_DELETE_SUCCESS,
  PILE_PILE05_SUCCESS,
  PILE_PILE05_SUCCESS_1,
  PILE_PILE05_SUCCESS_4,
  PILE_PILE05_ADDROW_SUCCESS,
  PILE_PILE05_CHECK_SUCCESS,
  PILE_PILE06_SUCCESS,
  PILE_PILE07_SUCCESS,
  PILE_PILE07_SUPER_SUCCESS,
  PILE_PILE07_SUCCESS_CASING,
  PILE_PILE07_CHECK_SUCCESS,
  PILE_PILE06_CHECK_SUCCESS,
  PILE_ERROR_CLEAR,
  DUPLICATE_LOGIN,
  PILE_COUNT,
  PILE_CLEAR,
  PILE_PILE08_SUCCESS,
  PILE_PILE09_SUCCESS,
  PILE_PILE09_ERROR,
  PILE_PILE10_SUCCESS,
  PILE_PILE11_SUCCESS,
  PILE_SLUMP_CANCEL_APPROVE_SUCCESS,
  PILE_SLUMP_CANCEL_APPROVE_FAIL,
  ConcreteList,
  PILE_LOADING,
  TREMIESAVETEMP,
  CONCRETE_ADDTEMP,
  DrillingFluidId_ADDTEMP,
  Drilling_ADDTEMP,
  DrillingChild_ADDTEMP,
  DrillingFluidId_EDITTEMP,
  CONCRETE_EDITTEMP,
  Drilling_EDITTEMP,
  DrillingChild_EDITTEMP,
  ConcreteRecordConfirm,
  PILE_ITEMS_CLEAR,
  Importtoerp,
  Unblockerp,
  loading

} from "./types"
import * as actions from "../Actions"
import { getConcreteList, getConcreteRecord } from "../Actions/ConcreteAction"
import { getDrillingFluids } from '../Actions/DrilingfludsAction'
import firebase from "react-native-firebase"
import { getTremieList } from "../Actions/TremieAction"
import { clearStatus, mainRefresh, getStepStatus, ungetStepStatus2 } from "../Actions/MainPileAction"
import { getDrilling, getDrillingChild } from '../Actions/DrillingAction'
import moment from 'moment'
import { Actions } from 'react-native-router-flux'
import I18n from "../../assets/languages/i18n"
import { getVersionApp } from './AuthAction'
export const pileItem = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.Version =  getVersionApp()
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }
    // console.log("pileItemData", data)
    axios
      .get(API_IP + "piledetail", { params: data }, config)
      .then(function (response) {
        console.log("piledetail", response.data)
        if(response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if(response.data.Message.Code == "001") {
          return dispatch({ type: PILE_ITEM, payload: response.data.PileDetail })
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function (error) {
        console.log(error.response)
        firebase.crashlytics().log('piledetail')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: error.message })
      })
  }
}

export const pileItems = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.Token = token
    data.Version =  getVersionApp()
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }

    var formBody = []
    for(var property in data) {
      var encodedKey = encodeURIComponent(property)
      var encodedValue = encodeURIComponent(data[property])
      formBody.push(encodedKey + "=" + encodedValue)
    }
    formBody = formBody.join("&")
    if(data.load){
      console.log('load',data.start,data.length)
    }else{
      dispatch({ type: PILE_LOADING, loading: true })
    }
    axios
      // .get(API_IP + "pileall?" + formBody, null, config)
      .get(API_IP_GET + "searchpile/pileall", { params: data }, config)
      .then(function (response) {
        console.log('searchpile/pileall',response.data)
        if(response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if(response.data.Message.Code == "001") {
          var status0 = response.data.NumberPile.total_notstart
          var status1 = response.data.NumberPile.total_inprocess
          var status2 = response.data.NumberPile.total_success
          var totalpile = response.data.NumberPile.total_pile
          var recent = response.data.RecentPile
         
          var position = response.data.UserPosition
          var searchpile = data.sreach
          console.warn('PileAlls.length',response.data.PileAlls.length)
          // if(response.data.PileAlls.length !=0){
            dispatch({ type: loading, loading:false })
            return dispatch({ type: PILE_ITEMS, payload: response.data.PileAlls, status0, status1, status2, position, recent,totalpile,searchpile, jobid: data.jobid})
          // }else{
          //   dispatch({ type: loading, loading:false })
          // }
          
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function (error) {
        console.log('pileItems',error.response)
        firebase.crashlytics().log('pileall')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: error.message })
      })
  }
}

export const sreachpileno = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.Token = token
    data.Version =  getVersionApp()
    // console.warn('sreachpileno')
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }
    axios
      .get(API_IP_GET + "searchpile/searchbypileno", { params: data }, config)
      .then(function (response) {
        console.log('searchpile/searchbypileno',response.data)
        if(response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if(response.data.Message.Code == "001") {
          var status0 = response.data.NumberPile.total_notstart
          var status1 = response.data.NumberPile.total_inprocess
          var status2 = response.data.NumberPile.total_success
          var totalpile = response.data.NumberPile.total_pile
          var recent = response.data.RecentPile
          var position = response.data.UserPosition
          var searchpile = true
          var input = data.input1
          return dispatch({ type: PILE_ITEMS, payload: response.data.PileAlls, status0, status1, status2, position, recent,totalpile,searchpile,input, jobid: data.jobid})
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function (error) {
        console.log('sreachpileno',error.response)
        firebase.crashlytics().log('pileall')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: error.message })
      })
  }
}

export const pileValueInfo = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.Version =  getVersionApp()
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }
    // console.log("pileValueInfoData", data)
    axios
      .get(API_IP + "pilevalueinfo", { params: data }, config)
      .then(function (response) {
        console.log("pileValueInfo", response.data)
        if(response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if(response.data.Message.Code == "001") {
          return dispatch({ type: PILE_VALUE_INFO, payload: response.data.PileValue })
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function (error) {
        console.log("error", error.response)
        firebase.crashlytics().log('pilevalueifno')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: error.message })
      })
  }
}

export const pileValueInfoReset = data => {
  return async dispatch => {
    return dispatch({ type: PILE_VALUE_INFO_RESET })
  }
}

export const pileMasterInfo = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.Version =  getVersionApp()
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }
    // console.log("pileMasterInfoData", data)
    axios
      .get(API_IP + "pilemasterinfo", { params: data }, config)
      .then(function (response) {
        console.log("pileMasterInfo", response.data)
        if(response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: message.Message })
        } else if(response.data.Message.Code == "001") {
          return dispatch({ type: PILE_MASTER_INFO, payload: response.data })
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function (error) {
        console.log(error.response)
        firebase.crashlytics().log('pilemasterinfo')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: error.message })
      })
  }
}

export const pileSaveStep01 = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("Page", option.Page)
    data.append("startdate", option.startdate)
    data.append("enddate", option.enddate)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)
    }
    data.append("startdate", option.startdate)
    data.append("enddate", option.enddate)
    if(option.Page == 1) {
      data.append("ProviderId", option.ProviderId)
      data.append("ProviderName", option.ProviderName)
     
    } else if(option.Page == 2) {
      
          if(option.SectionDetail.length > 0 && option.SectionDetail != null) {
            for(var i = 0; i < option.SectionDetail.length; i++) {
              var countLocal = 0
              var countServer = 0
              if(option.SectionDetail[i].ImageSteelcage != null && option.SectionDetail[i].ImageSteelcage.length > 0) {
                for(var j = 0; j < option.SectionDetail[i].ImageSteelcage.length; j++) {
                  if(option.SectionDetail[i].ImageSteelcage[j].id) {
                    var photo = option.SectionDetail[i].ImageSteelcage[j].id
                    data.append("SectionDetail[" + i + "].ImageSteelcageIds[" + countServer + "]", photo)
                    countServer++
                  } else {
                    var photo = {
                      uri: option.SectionDetail[i].ImageSteelcage[j].image,
                      type: "image/jpeg",
                      name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                    }
                    data.append("SectionDetail[" + i + "].ImageSteelcage[" + countLocal + "]", photo)
                    var type = option.SectionDetail[i].ImageSteelcage[j].type == "camera" ? 1 : 2
                    data.append(
                      "SectionDetail[" + i + "].ImageSteelcageDate[" + countLocal + "]",
                      option.SectionDetail[i].ImageSteelcage[j].timestamp
                    )
                    data.append("SectionDetail[" + i + "].ImageSteelcageType[" + countLocal + "]", type)
                    countLocal++
                  }
                }
              }
              if(
                option.SectionDetail[i].ImageConcretespacer != null &&
                option.SectionDetail[i].ImageConcretespacer.length > 0
              ) {
                var countLocal = 0
                var countServer = 0
                for(var j = 0; j < option.SectionDetail[i].ImageConcretespacer.length; j++) {
                  if(option.SectionDetail[i].ImageConcretespacer[j].id) {
                    var photo = option.SectionDetail[i].ImageConcretespacer[j].id
                    data.append("SectionDetail[" + i + "].ImageConcretespacerIds[" + countServer + "]", photo)
                    countServer++
                  } else {
                    var photo = {
                      uri: option.SectionDetail[i].ImageConcretespacer[j].image,
                      type: "image/jpeg",
                      name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                    }
                    data.append("SectionDetail[" + i + "].ImageConcretespacer[" + countLocal + "]", photo)
                    var type = option.SectionDetail[i].ImageConcretespacer[j].type == "camera" ? 1 : 2
                    data.append(
                      "SectionDetail[" + i + "].ImageConcretespacerDate[" + countLocal + "]",
                      option.SectionDetail[i].ImageConcretespacer[j].timestamp
                    )
                    data.append("SectionDetail[" + i + "].ImageConcretespacerType[" + countLocal + "]", type)
                    countLocal++
                  }
                }
              }

              data.append("SectionDetail[" + i + "].sectionId", option.SectionDetail[i].sectionId)
              data.append("SectionDetail[" + i + "].Checksteelcage", option.SectionDetail[i].Checksteelcage)
              data.append("SectionDetail[" + i + "].CheckConcretespacer", option.SectionDetail[i].CheckConcretespacer)
            }
          }
        
          axios
            .post(API_IP_GET + "savesteppile/savestep01", data, config)
            .then(function (response) {
              console.log("saveStep01 ", response)
              if(response.data.Code == "102") {
                return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
              } else if(response.data.Code == "001") {
                return dispatch({ type: PILE_PILE01_SUCCESS })
              } else {
                return dispatch({ type: PILE_ERROR, payload: response.data.Message })
              }
            })
            .catch(function (error) {
              console.log(error)
              dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')+'1_1'})
            })
    }

    if(option.Page != 2) {
      
      console.log("save01 test",data)
        
      axios
        .post(API_IP_GET + "savesteppile/savestep01", data, config)
        .then(function (response) {
          console.log("saveStep01", response.data)
          if(response.data.Code == "102") {
            return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
          } else if(response.data.Code == "001") {
            if(option.Page == 3){
              return dispatch({ type: PILE_PILE01_SUCCESS_3 })
            }else{
              return dispatch({ type: PILE_PILE01_SUCCESS_1 })
            }
            
          } else {
            return dispatch({ type: PILE_ERROR, payload: response.data.Message })
          }
        })
        .catch(function (error) {
          console.log('testtest1',error.response)
          firebase.crashlytics().log('savesteppile/savestep01')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
          dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')+'1_2'})
        })
    }
  }
}

export const pileSaveStep02 = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("Page", option.Page)
    data.append("startdate", option.startdate)
    data.append("enddate", option.enddate)
    data.append("Version", getVersionApp())
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}
    if(option.Page == 1) {
      data.append("Length", option.Length)
      data.append("MachineCraneId", option.MachineCraneId)
      data.append("MachineVibroId", option.MachineVibroId)
      data.append("DriverId", option.DriverId)

      data.append("MachineCraneName", option.MachineCraneName)
      data.append("MachineVibroName", option.MachineVibroName)
      data.append("DriverName", option.DriverName)
    } else if(option.Page == 2) {
      data.append("CheckPlummet", option.CheckPlummet)
      data.append("CheckWaterLevel", option.CheckWaterLevel)
      // console.warn('latlat',option.latitude,option.longitude)
      // axios
      //   .get(
      //     API_WEATHER+"?lat=" +
      //     option.latitude +
      //     "&lon=" +
      //     option.longitude +
      //     "&APPID=67fb763683db01d90f672ce07e78bd8a"
      //   )
      //   .then(function (response) {
      //     console.log("response weather", response.data)
          if(option.ImagePlummet != "") {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.ImagePlummet.length; i++) {
              if(option.ImagePlummet[i].id) {
                var photo = option.ImagePlummet[i].id
                data.append("ImagePlummetIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.ImagePlummet[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }
                data.append("ImagePlummet[" + countLocal + "]", photo)
                var type = option.ImagePlummet[i].type == "camera" ? 1 : 2
                data.append("ImagePlummetDate[" + countLocal + "]", option.ImagePlummet[i].timestamp)
                data.append("ImagePlummetType[" + countLocal + "]", type)
                countLocal++
              }
            }
          }
          if(option.ImageWaterLevel != "") {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.ImageWaterLevel.length; i++) {
              if(option.ImageWaterLevel[i].id) {
                var photo = option.ImageWaterLevel[i].id
                data.append("ImageWaterLevelIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.ImageWaterLevel[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }
                data.append("ImageWaterLevel[" + countLocal + "]", photo)
                var type = option.ImageWaterLevel[i].type == "camera" ? 1 : 2
                data.append("ImageWaterLevelDate[" + countLocal + "]", option.ImageWaterLevel[i].timestamp)
                data.append("ImageWaterLevelType[" + countLocal + "]", type)
                countLocal++
              }
            }
          }
          
          axios
            .post(API_IP_GET + "savesteppile/savestep02", data, config)
            .then(function (response) {
              // console.log("saveStep02", response)
              if(response.data.Code == "102") {
                return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
              } else if(response.data.Code == "001") {
                return dispatch({ type: PILE_PILE02_SUCCESS })
              } else {
                return dispatch({ type: PILE_ERROR, payload: response.data.Message })
              }
            })
            .catch(function (error) {
              console.log(error)
              firebase.crashlytics().log('savesteppile/savestep02')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
              dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
            })
    }
    // console.log("dataAppend02", data)
    if(option.Page != 2) {
      axios
        .post(API_IP_GET + "savesteppile/savestep02", data, config)
        .then(function (response) {
          // console.warn("saveStep02", response)
          if(response.data.Code == "102") {
            return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
          } else if(response.data.Code == "001") {
            return dispatch({ type: PILE_PILE02_SUCCESS_1 })
          } else {
            return dispatch({ type: PILE_ERROR, payload: response.data.Message })
          }
        })
        .catch(function (error) {
          console.log(error)
          firebase.crashlytics().log('savesteppile/savestep02')
          firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
          dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
        })
    }
  }
}

export const pileSaveStep03 = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    // console.warn('pileSaveStep03',option)
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("Page", option.Page)
    data.append("startdate", option.startdate)
    data.append("enddate", option.enddate)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}
    if(option.Page == 1) {
      data.append("SurveyorId", option.SurveyorId)
      data.append("LocationId", option.LocationId)
      data.append("BSFlagLocationId", option.BSFlagLocationId)
      data.append("IsResection", option.IsResection)

      data.append("SurveyorName", option.SurveyorName)
      data.append("LocationName", option.LocationName)
      data.append("BSFlagLocationName", option.BSFlagLocationName)
    } else if(option.Page == 2) {
      data.append("ReadingNCoordinate", option.ReadingNCoordinate)
      data.append("ReadingECoordinate", option.ReadingECoordinate)
          if(option.ImageFile != "") {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.ImageFile.length; i++) {
              if(option.ImageFile[i].id) {
                var photo = option.ImageFile[i].id
                data.append("ImageIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.ImageFile[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }

                data.append("ImageFile[" + countLocal + "]", photo)
                var type = option.ImageFile[i].type == "camera" ? 1 : 2
                data.append("ImageDate[" + countLocal + "]", option.ImageFile[i].timestamp)
                data.append("ImageType[" + countLocal + "]", type)
                countLocal++
              }
            }
          }
          if(option.Image_SurveyDiff != "") {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.Image_SurveyDiff.length; i++) {
              if(option.Image_SurveyDiff[i].id) {
                var photo = option.Image_SurveyDiff[i].id
                data.append("ImageDiffIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.Image_SurveyDiff[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }

                data.append("ImageDiffFile[" + countLocal + "]", photo)
                var type = option.Image_SurveyDiff[i].type == "camera" ? 1 : 2
                data.append("ImageDiffDate[" + countLocal + "]", option.Image_SurveyDiff[i].timestamp)
                data.append("ImageDiffType[" + countLocal + "]", type)
                countLocal++
              }
            }
          }
          
          axios
            .post(API_IP_GET + "savesteppile/savestep03", data, config)
            .then(function (response) {
              // console.warn("saveStep03", response)
              if(response.data.Code == "102") {
                return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
              } else if(response.data.Code == "001") {
                return dispatch({ type: PILE_PILE03_SUCCESS_2 })
              } else {
                return dispatch({ type: PILE_ERROR, payload: response.data.Message })
              }
            })
            .catch(function (error) {
              console.log(error)
              firebase.crashlytics().log('savesteppile/savestep03')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
              dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
            })
        
    } else if(option.Page == 3) {
      data.append("TopCasingLevel", option.TopCasingLevel)
      data.append("GroundLevel", option.GroundLevel)
       
    } else if(option.Page == 4) {
      data.append("ApproveType", option.ApproveType)
      data.append("ApprovedUserId", option.ApprovedUserId)
    } else if(option.Page == 5) {
      data.append("SurveyorId", option.SurveyorId)
      data.append("SurveyorName", option.SurveyorName)
      data.append("TopCasingLevel", option.TopCasingLevel)
      data.append("GroundLevel", option.GroundLevel)
    }

    console.log("dataAppend03", data)
    if(option.Page != 2) {
      console.log(1)
          axios
            .post(API_IP_GET + "savesteppile/savestep03", data, config)
            .then(function (response) {
              console.log("saveStep03", response)
              if(response.data.Code == "102") {
                return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
              } else if(response.data.Code == "001") {
                if(option.Page == 1){
                  return dispatch({ type: PILE_PILE03_SUCCESS_1 })
                }else if(option.Page == 3){
                  return dispatch({ type: PILE_PILE03_SUCCESS_3 })
                }else if(option.Page == 5){
                  return dispatch({ type: PILE_PILE03_SUCCESS_4 })
                }
                
              } else {
                return dispatch({ type: PILE_ERROR, payload: response.data.Message })
              }
            })
        .catch(function (error) {
          console.log(error)
          firebase.crashlytics().log('savesteppile/savestep03')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
          dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
        })
    }
  }
}

export const pileSaveApproveStep03 = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("ApproveType", option.ApproveType)
    data.append("PileId", option.PileId)
    data.append("LocationLat",option.LocationLat)
    data.append("LocationLong",option.LocationLong)
    if(option.ApproveType != 2) {
      data.append("ApprovedUserId", option.ApprovedUserId)
    }

    // console.warn("datasaveApproveStep03", data)

    axios
      .post(API_IP_GET + "savesteppile/approvestep03", data, config)
      .then(function (response) {
        console.log("saveApproveStep03", response)
        if(response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          return dispatch({ type: PILE_PILE03_APPROVE_SUCCESS })
        } else {
          return dispatch({ type: PILE_PILE03_APPROVE_FAIL, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error.response)
        firebase.crashlytics().log('savesteppile/approvestep03')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_PILE03_APPROVE_FAIL, payload: error.message })
      })
  }
}

export const pileCancelApproveStep03 = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("Version", getVersionApp())
    console.log("dataCancelStep03", data)

    axios
      .post(API_IP_GET + "savesteppile/rejectapproved", data, config)
      .then(function (response) {
        console.log("CancelStep03", response)
        if(response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          return dispatch({ type: PILE_PILE03_CANCEL_APPROVE_SUCCESS })
        } else {
          return dispatch({ type: PILE_PILE03_CANCEL_APPROVE_FAIL, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error.response)
        firebase.crashlytics().log('savesteppile/rejectapproved')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_PILE03_CANCEL_APPROVE_FAIL, payload: error.message })
      })
  }
}

export const pileSaveStep04 = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token
    if (option.DrillingFluidId == null) {
      var temp = option
      temp.TypeName = option.ProcessTesterList
      temp.PositionName = option.SubProcessTesterList || null
      temp.ImagePh = option.ImagePhFiles
      temp.Ph = option.Ph
      temp.ImageDensity = option.ImageDensityFiles
      temp.Density = option.Density
      temp.ImageViscosity = option.ImageViscosityFiles
      temp.Viscosity = option.Viscosity
      temp.ImageSand = option.ImageSandFiles
      temp.Sand = option.Sand
      temp.Local = true
      temp.startdate = option.startdate
      temp.enddate = option.enddate
      temp.SlurryTypeId = option.SlurryID
      temp.SlurryTypeName = option.SlurryList
      dispatch({ type: DrillingFluidId_ADDTEMP, payload: temp})
    }else{
      var temp = option
      temp.TypeName = option.ProcessTesterList
      temp.PositionName = option.SubProcessTesterList || null
      temp.ImagePh = option.ImagePhFiles
      temp.Ph = option.Ph
      temp.ImageDensity = option.ImageDensityFiles
      temp.Density = option.Density
      temp.ImageViscosity = option.ImageViscosityFiles
      temp.Viscosity = option.Viscosity
      temp.ImageSand = option.ImageSandFiles
      temp.Sand = option.Sand
      temp.Local = true
      temp.startdate = option.startdate
      temp.enddate = option.enddate
      temp.SlurryTypeId = option.SlurryID
      temp.SlurryTypeName = option.SlurryList
      dispatch({ type: DrillingFluidId_EDITTEMP, payload: temp , index: option.No-1})
    }
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("DrillingFluidId",option.DrillingFluidId)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}
    data.append("TypeId", option.ProcessTesterListID)
    data.append("TypeName", option.TypeName)
    data.append("No", option.No)
    data.append("Ph", parseFloat(option.Ph))
    data.append("Density", option.Density)
    data.append("Viscosity", option.Viscosity)
    data.append("Sand", option.Sand)
    data.append("TesterERPId", option.TesterERPId)
    data.append("TesterERPName", option.TesterERPName)
    if(option.SubProcessTesterListID != '') {
      data.append("PositionId", option.SubProcessTesterListID)
      data.append("PositionName", option.PositionName)
    }
    data.append("startdate",option.startdate)
    data.append("enddate",option.enddate)
    if(option.SlurryID != ''){
      data.append("SlurryTypeId",option.SlurryID)
      data.append("SlurryTypeName",option.SlurryTypeName)
    }
    if(option.Page == 0) {
          if(option.ImagePhFiles != "") {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.ImagePhFiles.length; i++) {
              if(option.ImagePhFiles[i].id) {
                var photo = option.ImagePhFiles[i].id
                data.append("ImagePhIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.ImagePhFiles[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }

                data.append("ImagePhFiles[" + countLocal + "]", photo)
                var type = option.ImagePhFiles[i].type == "camera" ? 1 : 2
                data.append("ImagePhDates[" + countLocal + "]", option.ImagePhFiles[i].timestamp)
                data.append("ImagePhTypes[" + countLocal + "]", type)
                countLocal++
              }
            }
          }
          if(option.ImageDensityFiles != "") {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.ImageDensityFiles.length; i++) {
              if(option.ImageDensityFiles[i].id) {
                var photo = option.ImageDensityFiles[i].id
                data.append("ImageDensityIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.ImageDensityFiles[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }

                data.append("ImageDensityFiles[" + countLocal + "]", photo)
                var type = option.ImageDensityFiles[i].type == "camera" ? 1 : 2
                data.append("ImageDensityDates[" + countLocal + "]", option.ImageDensityFiles[i].timestamp)
                data.append("ImageDensityTypes[" + countLocal + "]", type)
                countLocal++
              }
            }
          }
          if(option.ImageViscosityFiles != "") {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.ImageViscosityFiles.length; i++) {
              if(option.ImageViscosityFiles[i].id) {
                var photo = option.ImageViscosityFiles[i].id
                data.append("ImageViscosityIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.ImageViscosityFiles[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }

                data.append("ImageViscosityFiles[" + countLocal + "]", photo)
                var type = option.ImageViscosityFiles[i].type == "camera" ? 1 : 2
                data.append("ImageViscosityDates[" + countLocal + "]", option.ImageViscosityFiles[i].timestamp)
                data.append("ImageViscosityTypes[" + countLocal + "]", type)
                countLocal++
              }
            }
          }
          if(option.ImageSandFiles != "") {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.ImageSandFiles.length; i++) {
              if(option.ImageSandFiles[i].id) {
                var photo = option.ImageSandFiles[i].id
                data.append("ImageSandIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.ImageSandFiles[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }

                data.append("ImageSandFiles[" + countLocal + "]", photo)
                var type = option.ImageSandFiles[i].type == "camera" ? 1 : 2
                data.append("ImageSandDates[" + countLocal + "]", option.ImageSandFiles[i].timestamp)
                data.append("ImageSandTypes[" + countLocal + "]", type)
                countLocal++
              }
            }
          }
          
          axios
            .post(API_IP_GET + "step04/savedrillingfluid", data, config)
            .then(function (response) {
              console.log('datadata',data)
              console.log("saveStep04", response)
              if(response.data.Code == "102") {
                return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
              } else if(response.data.Code == "001") {
                dispatch(getDrillingFluids({
                  jobid: option.JobId,
                  pileid: option.PileId,
                  Language: I18n.locale
                }))
                return dispatch({ type: PILE_PILE04_SUCCESS,id:response.data.Id})
              } else {
                return dispatch({ type: PILE_ERROR, payload: response.data.Message })
              }
            })
            .catch(function (error) {
              console.log(error)
              firebase.crashlytics().log('step04/savedrillingfluid')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
              dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
            })
    }

    console.log("dataAppend04", data)
    if(option.Page != 0) {
      console.log(data)
      axios
        .post(API_IP_GET + "step04/savedrillingfluid", data, config)
        .then(function (response) {
          console.log("saveStep04", response)
          if(response.data.Code == "102") {
            return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
          } else if(response.data.Code == "001") {
            return dispatch({ type: PILE_PILE04_SUCCESS,id:response.data.Id })
          } else {
            return dispatch({ type: PILE_ERROR, payload: response.data.Message })
          }
        })
        .catch(function (error) {
          console.log(error)
          firebase.crashlytics().log('step04/savedrillingfluid')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
          dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
        })
    }
  }
}

export const checkstepofsuperstructure5 = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("Version", getVersionApp())

    console.log("dataCancelStep03", data)

    axios
      .post(API_IP_GET + "step05/checkstepofsuperstructure", data, config)
      .then(function (response) {
        // console.warn("checkstepofsuperstructure 5", response.data)
        if(response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          return dispatch({ type: PILE_PILE05_CHECK_SUCCESS,lockstep: option.lockstep })
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message,lockstep: option.lockstep,process:option.process })
        }
      })
      .catch(function (error) {
        console.log(error.response)
        firebase.crashlytics().log('step05/checkstepofsuperstructure')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        // dispatch({ type: PILE_PILE03_CANCEL_APPROVE_FAIL, payload: error.message })
      })
  }
}

export const clearError = () => {
  return dispatch => {
    return dispatch({ type: PILE_ERROR_CLEAR })
  }
}

export const pileClear = () => {
  return dispatch => {
    return dispatch({ type: PILE_CLEAR })
  }
}

export const pile2Count = () => {
  return dispatch => {
    return dispatch({ type: PILE_COUNT, payload: 1 })
  }
}

export const pile4DeleteRow = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("DrillingFluidId", option.DrillingFluidId)
    if((option.lat||option.log) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
      data.append("LocationLat",option.lat),
      data.append("LocationLong",option.log)
    }

    console.log("dataPile4deleteRow", option)

    axios
      .post(API_IP_GET + "step04/removedrillingfluid", data, config)
      .then(function (response) {
        console.log("response dataPile4deleteRow", response)
        if(response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          dispatch(getDrillingFluids({
            jobid: option.JobId,
            pileid: option.PileId,
            Language: I18n.locale
          }))
          return dispatch({ type: PILE_PILE04_DELETE_SUCCESS })
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error.response)
        firebase.crashlytics().log('step04/removedrillingfluid')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: error.message })
      })
  }
}

export const pile5DeleteRow = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("Version", getVersionApp())
    data.append("No", option.No)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
      data.append("LocationLat",option.latitude),
      data.append("LocationLong",option.longitude)
    }
    console.log("dataPile5deleteRow", option)

    axios
      .post(API_IP_GET + "step05/removedrilling", data, config)
      .then(function (response) {
        console.log("response removedrilling", response)
        if(response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          dispatch(getDrilling({
            jobid: option.JobId,
            pileid: option.PileId,
            Language: I18n.locale
          }))
          return dispatch({ type: PILE_PILE05_DELETE_SUCCESS })
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error.response)
        firebase.crashlytics().log('step05/removedrilling')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: error.message })
      })
  }
}

export const pile5DeleteRowChild = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("No", option.No)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
      data.append("LocationLat",option.latitude),
      data.append("LocationLong",option.longitude)
    }
    console.log("dataPile5deleteRow", option)

    axios
      .post(API_IP_GET + "step05/RemoveDrillingChild", data, config)
      .then(function (response) {
        console.log("response RemoveDrillingChild data", response.data)
        if(response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          dispatch(getDrillingChild({
            jobid: option.JobId,
            pileid: option.PileId,
            Language: I18n.locale
          }))
          return dispatch({ type: PILE_PILE05_DELETE_SUCCESS })
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error.response)
        firebase.crashlytics().log('step05/RemoveDrillingChild')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: error.message })
      })
  }
}

export const pileSaveStep05 = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("Page", option.Page)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}

    if(option.Page == 1 || option.Page == 6) {
      data.append("OverLifting", option.OverLifting)
      data.append("Pcoring", option.Pcoring)
      // data.append("HangingBarsLength", option.hanginglength)
      console.log('option.category !== 5',option.category,option.category !== 5)
    // if(option.category !== 5){
      data.append("CheckPlummet", option.CheckPlummet)
      data.append("CheckWaterLevel", option.CheckWaterLevel)
    
      if(option.ImageCheckPlummet != "") {
        var countLocal = 0
        var countServer = 0
        for(var i = 0; i < option.ImageCheckPlummet.length; i++) {
          if(option.ImageCheckPlummet[i].id) {
            var photo = option.ImageCheckPlummet[i].id
            data.append("ImagePlummetIds[" + countServer + "]", photo)
            countServer++
          } else {
            var photo = {
              uri: option.ImageCheckPlummet[i].image,
              type: "image/jpeg",
              name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
            }

            data.append("ImagePlummetFiles[" + countLocal + "]", photo)
            var type = option.ImageCheckPlummet[i].type == "camera" ? 1 : 2
            data.append("ImagePlummetDates[" + countLocal + "]", option.ImageCheckPlummet[i].timestamp)
            data.append("ImagePlummetTypes[" + countLocal + "]", type)
            countLocal++
          }
        }
      }
      if(option.ImageCheckWaterLevel != "") {
        var countLocal = 0
        var countServer = 0
        for(var i = 0; i < option.ImageCheckWaterLevel.length; i++) {
          if(option.ImageCheckWaterLevel[i].id) {
            var photo = option.ImageCheckWaterLevel[i].id
            data.append("ImageWaterLevelIds[" + countServer + "]", photo)
            countServer++
          } else {
            var photo = {
              uri: option.ImageCheckWaterLevel[i].image,
              type: "image/jpeg",
              name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
            }

            data.append("ImageWaterLevelFiles[" + countLocal + "]", photo)
            var type = option.ImageCheckWaterLevel[i].type == "camera" ? 1 : 2
            data.append("ImageWaterLevelDates[" + countLocal + "]", option.ImageCheckWaterLevel[i].timestamp)
            data.append("ImageWaterLevelTypes[" + countLocal + "]", type)
            countLocal++
          }
        }
      }
    // }
          // if(option.ImageLengthSteelCarry != "") {
          //   var countLocal = 0
          //   var countServer = 0
          //   for(var i = 0; i < option.ImageLengthSteelCarry.length; i++) {
          //     if(option.ImageLengthSteelCarry[i].id) {
          //       var photo = option.ImageLengthSteelCarry[i].id
          //       data.append("ImageLengthSteelCarryIds[" + countServer + "]", photo)
          //       countServer++
          //     } else {
          //       var photo = {
          //         uri: option.ImageLengthSteelCarry[i].image,
          //         type: "image/jpeg",
          //         name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
          //       }

          //       data.append("ImageLengthSteelCarryFiles[" + countLocal + "]", photo)
          //       var type = option.ImageLengthSteelCarry[i].type == "camera" ? 1 : 2
          //       data.append("ImageLengthSteelCarryDates[" + countLocal + "]", option.ImageLengthSteelCarry[i].timestamp)
          //       data.append("ImageLengthSteelCarryTypes[" + countLocal + "]", type)
          //       countLocal++
          //     }
          //   }
          // }
          console.log('step05/saveall data',data)
          axios
            .post(API_IP_GET + "step05/saveall", data, config)
            .then(function (response) {
              console.log("saveStep05", response)
              if(response.data.Code == "102") {
                return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
              } else if(response.data.Code == "001") {
                if(option.flag_notnext == true){

                }else{
                  return dispatch({ type: PILE_PILE05_SUCCESS_1 })
                }
              } else {
                return dispatch({ type: PILE_ERROR, payload: response.data.Message })
              }
            })
            .catch(function (error) {
              console.log('error pileSaveStep05 page1_6',error,data)
              var object = {};
              var json =''
              try{
                data.forEach(function(value, key){
                  object[key] = value;
              });
               json = JSON.stringify(object);
              }catch(e){
                console.log('error pileSaveStep05 page1_6 catch',e)
              }

              if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log('error pileSaveStep05 page1_6 error.response')
                console.log(error.response.data)
                console.log(error.response.status)
                console.log(error.response.headers)
                return { status: false, message: error.response.data }
              } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the
                // browser and an instance of
                // http.ClientRequest in node.js
                console.log('error pileSaveStep05 page1_6 error.response',error.request)
                console.log(error.request)
              }
             
              firebase.crashlytics().log('step05/saveall')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
              dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')+'test '+error.message+' '+json})
            })
        
    } else if(option.Page == 4) {
      
          var DrillingList = []
          if(option.DrillingList.length > 0 && option.DrillingList != null) {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.DrillingList.length; i++) {
              if(option.DrillingList[i].Images != null && option.DrillingList[i].Images.length > 0) {
                for(var j = 0; j < option.DrillingList[i].Images.length; j++) {
                  if(option.DrillingList[i].Images[j].id) {
                    var photo = option.DrillingList[i].Images[j].id
                    data.append("DrillingList[" + i + "].ImageIds[" + countServer + "]", photo)
                    countServer++
                  } else {
                    var photo = {
                      uri: option.DrillingList[i].Images[j].image,
                      type: "image/jpeg",
                      name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                    }
                    data.append("DrillingList[" + i + "].ImageFiles[" + countLocal + "]", photo)
                    var type = option.DrillingList[i].Images[j].type == "camera" ? 1 : 2
                    data.append(
                      "DrillingList[" + i + "].ImageDates [" + countLocal + "]",
                      option.DrillingList[i].Images[j].timestamp
                    )
                    data.append("DrillingList[" + i + "].ImageTypes[" + countLocal + "]", type)
                    countLocal++
                  }
                }
              }

              data.append("DrillingList[" + i + "].No", option.DrillingList[i].No)
              data.append("DrillingList[" + i + "].DrillingToolTypeId", option.DrillingList[i].DrillingToolTypeId)
              data.append("DrillingList[" + i + "].MachineId", option.DrillingList[i].MachineId)
              data.append("DrillingList[" + i + "].DrillingToolId", option.DrillingList[i].DrillingToolId)
              data.append("DrillingList[" + i + "].PowerPackId", option.DrillingList[i].PowerPackId)
              data.append("DrillingList[" + i + "].DriverId", option.DrillingList[i].DriverId)
              data.append("DrillingList[" + i + "].StartDrilling", option.DrillingList[i].StartDrilling)
              data.append("DrillingList[" + i + "].EndDrilling", option.DrillingList[i].EndDrilling)
              data.append("DrillingList[" + i + "].Depth", option.DrillingList[i].Depth)
              data.append("DrillingList[" + i + "].IsBreak", option.DrillingList[i].IsBreak)
              
            }
          }
          console.log(data)

          axios
            .post(API_IP_GET + "step05/saveall", data, config)
            .then(function (response) {
              console.log("saveStep05", response)
              if(response.data.Code == "102") {
                return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
              } else if(response.data.Code == "001") {
                return dispatch({ type: PILE_PILE05_SUCCESS })
              } else {
                return dispatch({ type: PILE_ERROR, payload: response.data.Message })
              }
            })
            .catch(function (error) {
              console.log(error)
              firebase.crashlytics().log('step05/saveall')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
              dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
            })
        
    } else if(option.Page == 5) {
      data.append("CheckStopEnd", option.CheckStopEnd)
      data.append("CheckWaterStop", option.CheckWaterStop)
      data.append("CheckInstallStopEnd", option.CheckInstallStopEnd)
      data.append("CheckDrillingFluid", option.CheckDrillingFluid)
      data.append("StartDrillingFluidDate", option.StartDrillingFluidDate)
      data.append("EndDrillingFluidDate", option.EndDrillingFluidDate)
      data.append("StartStopEndDate", option.StartStopEndDate)
      data.append("EndStopEndDate", option.EndStopEndDate)
      data.append("WaterStopTypeId", option.WaterStopTypeId)
      data.append("WaterStopLength", option.WaterStopLength)
      data.append("WaterStopTypeName", option.WaterStopTypeName)
      
          if(option.ImageCheckStopEnd != "") {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.ImageCheckStopEnd.length; i++) {
              if(option.ImageCheckStopEnd[i].id) {
                var photo = option.ImageCheckStopEnd[i].id
                data.append("ImageCheckStopEndIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.ImageCheckStopEnd[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }

                data.append("ImageCheckStopEndFiles[" + countLocal + "]", photo)
                var type = option.ImageCheckStopEnd[i].type == "camera" ? 1 : 2
                data.append("ImageCheckStopEndDates[" + countLocal + "]", option.ImageCheckStopEnd[i].timestamp)
                data.append("ImageCheckStopEndTypes[" + countLocal + "]", type)
                countLocal++
              }
            }
          }
          if(option.ImageCheckWaterStop != "") {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.ImageCheckWaterStop.length; i++) {
              if(option.ImageCheckWaterStop[i].id) {
                var photo = option.ImageCheckWaterStop[i].id
                data.append("ImageCheckWaterStopIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.ImageCheckWaterStop[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }

                data.append("ImageCheckWaterStopFiles[" + countLocal + "]", photo)
                var type = option.ImageCheckWaterStop[i].type == "camera" ? 1 : 2
                data.append("ImageCheckWaterStopDates[" + countLocal + "]", option.ImageCheckWaterStop[i].timestamp)
                data.append("ImageCheckWaterStopTypes[" + countLocal + "]", type)
                countLocal++
              }
            }
          }
          if(option.ImageCheckInstallStopEnd != "") {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.ImageCheckInstallStopEnd.length; i++) {
              if(option.ImageCheckInstallStopEnd[i].id) {
                var photo = option.ImageCheckInstallStopEnd[i].id
                data.append("ImageCheckInstallStopEndIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.ImageCheckInstallStopEnd[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }

                data.append("ImageCheckInstallStopEndFiles[" + countLocal + "]", photo)
                var type = option.ImageCheckInstallStopEnd[i].type == "camera" ? 1 : 2
                data.append("ImageCheckInstallStopEndDates[" + countLocal + "]", option.ImageCheckInstallStopEnd[i].timestamp)
                data.append("ImageCheckInstallStopEndTypes[" + countLocal + "]", type)
                countLocal++
              }
            }
          }
          if(option.ImageCheckDrillingFluid != "") {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.ImageCheckDrillingFluid.length; i++) {
              if(option.ImageCheckDrillingFluid[i].id) {
                var photo = option.ImageCheckDrillingFluid[i].id
                data.append("ImageCheckDrillingFluidIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.ImageCheckDrillingFluid[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }

                data.append("ImageCheckDrillingFluidFiles[" + countLocal + "]", photo)
                var type = option.ImageCheckDrillingFluid[i].type == "camera" ? 1 : 2
                data.append("ImageCheckDrillingFluidDates[" + countLocal + "]", option.ImageCheckDrillingFluid[i].timestamp)
                data.append("ImageCheckDrillingFluidTypes[" + countLocal + "]", type)
                countLocal++
              }
            }
          }

          axios
            .post(API_IP_GET + "step05/saveall", data, config)
            .then(function (response) {
              // console.warn("saveStep05",response.data.Code)
              if(response.data.Code == "102") {
                return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
              } else if(response.data.Code == "001") {
                  return dispatch({ type: PILE_PILE05_SUCCESS_4 })
                
              } else {
                return dispatch({ type: PILE_ERROR, payload: response.data.Message })
              }
            })
            .catch(function (error) {
              console.log(error)
              firebase.crashlytics().log('step05/saveall')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
              dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
            })
       
    }
  }
}

export const pileSaveStep05Drilling = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token
    if (option.drillingId == null) {
      var temp = option
      temp.DrillingToolType = []
      temp.Crane = []
      temp.DrillingTool = []
      temp.PowerPack = []
      temp.DrillingToolType.Id = option.DrillingToolTypeId
      temp.DrillingToolType.Name = option.drilltype
      temp.Depth = option.Depth
      temp.StartDrilling = option.StartDrilling
      temp.EndDrilling = option.EndDrilling
      temp.Images = option.Images
      temp.No = option.No
      temp.Crane.itemid = option.MachineId
      temp.Crane.machine_no = option.machine
      temp.DrillingTool.itemid = option.DrillingToolId
      temp.DrillingTool.machine_no = option.drilltool
      temp.DriverId = option.DriverId
      temp.PowerPack.itemid = option.PowerPackId
      temp.IsBreak = option.IsBreak

      temp.ImageBucketCuttingEdgeWidths = option.ImageBucketCuttingEdgeWidths
      temp.BucketCuttingEdgeWidth = option.BucketCuttingEdgeWidth
      // temp.drilltoolDisable = option.drilltoolDisable 
      temp.Local = true
      console.log('tempstep5',temp)
      dispatch({ type: Drilling_ADDTEMP, payload: temp})
    }else{
      var temp = option
      temp.DrillingToolType = []
      temp.Crane = []
      temp.DrillingTool = []
      temp.PowerPack = []
      temp.DrillingToolType.Id = option.DrillingToolTypeId
      temp.DrillingToolType.Name = option.drilltype
      temp.Depth = option.Depth
      temp.StartDrilling = option.StartDrilling
      temp.EndDrilling = option.EndDrilling
      temp.Images = option.Images
      temp.No = option.No
      temp.Crane.itemid = option.MachineId
      temp.Crane.machine_no = option.machine
      temp.DrillingTool.itemid = option.DrillingToolId
      temp.DrillingTool.machine_no = option.drilltool
      temp.DriverId = option.DriverId
      temp.PowerPack.itemid = option.PowerPackId
      temp.IsBreak = option.IsBreak
      temp.ImageBucketCuttingEdgeWidths = option.ImageBucketCuttingEdgeWidths
      temp.BucketCuttingEdgeWidth = option.BucketCuttingEdgeWidth
      // temp.drilltoolDisable = option.drilltoolDisable 
      temp.Local = true
      console.log('tempstep5',temp)
      dispatch({ type: Drilling_EDITTEMP, payload: temp , index:option.index})
    }
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("No", option.No)
    data.append("DrillingToolTypeId", option.DrillingToolTypeId)
    data.append("DrillingToolTypeName", option.DrillingToolTypeName)
    data.append("MachineId", option.MachineId)
    data.append("MachineName", option.machine)
    data.append("DrillingToolId", option.DrillingToolId)
    data.append("DrillingToolName", option.DrillingToolName)
    data.append("PowerPackId", option.PowerPackId)
    data.append("PowerPackName", option.PowerPackName)
    data.append("DriverId", option.DriverId)
    data.append("DriverName", option.driver)
    data.append("StartDrilling", moment(option.StartDrilling,'DD-MM-YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss'))
    data.append("EndDrilling", moment(option.EndDrilling,'DD-MM-YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss'))
    data.append("Depth", option.Depth)
    data.append("IsBreak", option.IsBreak)
    data.append("BucketCuttingEdgeWidth", option.BucketCuttingEdgeWidth)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}

        if(option.Images != "") {
          var countLocal = 0
          var countServer = 0
          for(var i = 0; i < option.Images.length; i++) {
            if(option.Images[i].id) {
              var photo = option.Images[i].id
              data.append("ImageIds[" + countServer + "]", photo)
              countServer++
            } else {
              var photo = {
                uri: option.Images[i].image,
                type: "image/jpeg",
                name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
              }

              data.append("ImageFiles[" + countLocal + "]", photo)
              var type = option.Images[i].type == "camera" ? 1 : 2
              data.append("ImageDates[" + countLocal + "]", option.Images[i].timestamp)
              data.append("ImageTypes[" + countLocal + "]", type)
              countLocal++
            }
          }
        }

        if(option.ImageBucketCuttingEdgeWidths != "") {
          var countLocal = 0
          var countServer = 0
          for(var i = 0; i < option.ImageBucketCuttingEdgeWidths.length; i++) {
            if(option.ImageBucketCuttingEdgeWidths[i].id) {
              var photo = option.ImageBucketCuttingEdgeWidths[i].id
              data.append("ImageBucketCuttingEdgeWidthIds[" + countServer + "]", photo)
              countServer++
            } else {
              var photo = {
                uri: option.ImageBucketCuttingEdgeWidths[i].image,
                type: "image/jpeg",
                name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
              }

              data.append("ImageBucketCuttingEdgeWidthFiles[" + countLocal + "]", photo)
              var type = option.ImageBucketCuttingEdgeWidths[i].type == "camera" ? 1 : 2
              data.append("ImageBucketCuttingEdgeWidthDates[" + countLocal + "]", option.ImageBucketCuttingEdgeWidths[i].timestamp)
              data.append("ImageBucketCuttingEdgeWidthTypes[" + countLocal + "]", type)
              countLocal++
            }
          }
        }

        console.log("dataAppend05_savedrilling", data)
        axios
          .post(API_IP_GET + "step05/savedrilling", data, config)
          .then(function (response) {
            console.log("savedrilling", response)
            if(response.data.Code == "102") {
              return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
            } else if(response.data.Code == "001") {
              dispatch(getDrilling({
                jobid: option.JobId,
                pileid: option.PileId,
                Language: I18n.locale
              }))
              return dispatch({ type: PILE_PILE05_ADDROW_SUCCESS })
            } else {
              return dispatch({ type: PILE_ERROR, payload: response.data.Message })
            }
          })
          .catch(function (error) {
            console.log(error)
            firebase.crashlytics().log('step05/savedrilling')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
            dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
          })

  }
}
export const pileSaveStep05DrillingChild = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token
    
    if (option.drillingId == null) {
      var temp = option
      temp.DrillingToolType = []
      temp.Crane = []
      temp.DrillingTool = []
      temp.PowerPack = []
      temp.DrillingToolType.Id = option.DrillingToolTypeId
      temp.DrillingToolType.Name = option.drilltype
      temp.Depth = option.Depth
      temp.StartDrilling = option.StartDrilling
      temp.EndDrilling = option.EndDrilling
      temp.Images = option.Images
      temp.No = option.No
      temp.Crane.itemid = option.MachineId
      temp.Crane.machine_no = option.machine
      temp.DrillingTool.itemid = option.DrillingToolId
      temp.DrillingTool.machine_no = option.drilltool
      temp.DriverId = option.DriverId
      temp.PowerPack.itemid = option.PowerPackId
      temp.IsBreak = option.IsBreak

      temp.ImageBucketCuttingEdgeWidths = option.ImageBucketCuttingEdgeWidths
      temp.BucketCuttingEdgeWidth = option.BucketCuttingEdgeWidth
      // temp.drilltoolDisable = option.drilltoolDisable 
      temp.Local = true
      console.log('tempstep5',temp)
      dispatch({ type: DrillingChild_ADDTEMP, payload: temp})
    }else{
      var temp = option
      temp.DrillingToolType = []
      temp.Crane = []
      temp.DrillingTool = []
      temp.PowerPack = []
      temp.DrillingToolType.Id = option.DrillingToolTypeId
      temp.DrillingToolType.Name = option.drilltype
      temp.Depth = option.Depth
      temp.StartDrilling = option.StartDrilling
      temp.EndDrilling = option.EndDrilling
      temp.Images = option.Images
      temp.No = option.No
      temp.Crane.itemid = option.MachineId
      temp.Crane.machine_no = option.machine
      temp.DrillingTool.itemid = option.DrillingToolId
      temp.DrillingTool.machine_no = option.drilltool
      temp.DriverId = option.DriverId
      temp.PowerPack.itemid = option.PowerPackId
      temp.IsBreak = option.IsBreak
      temp.ImageBucketCuttingEdgeWidths = option.ImageBucketCuttingEdgeWidths
      temp.BucketCuttingEdgeWidth = option.BucketCuttingEdgeWidth
      // temp.drilltoolDisable = option.drilltoolDisable 
      temp.Local = true
      console.log('tempstep5',temp)
      dispatch({ type: DrillingChild_EDITTEMP, payload: temp , index:option.index})
    }
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("No", option.No)
    data.append("DrillingToolTypeId", option.DrillingToolTypeId)
    data.append("DrillingToolTypeName", option.DrillingToolTypeName)
    data.append("MachineId", option.MachineId)
    data.append("MachineName", option.machine)
    data.append("DrillingToolId", option.DrillingToolId)
    data.append("DrillingToolName", option.DrillingToolName)
    data.append("PowerPackId", option.PowerPackId)
    data.append("PowerPackName", option.PowerPackName)
    data.append("DriverId", option.DriverId)
    data.append("DriverName", option.driver)
    data.append("StartDrilling", moment(option.StartDrilling,'DD-MM-YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss'))
    data.append("EndDrilling", moment(option.EndDrilling,'DD-MM-YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss'))
    data.append("Depth", option.Depth)
    data.append("IsBreak", option.IsBreak)
    data.append("BucketCuttingEdgeWidth", option.BucketCuttingEdgeWidth)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}

        if(option.Images != "") {
          var countLocal = 0
          var countServer = 0
          for(var i = 0; i < option.Images.length; i++) {
            if(option.Images[i].id) {
              var photo = option.Images[i].id
              data.append("ImageIds[" + countServer + "]", photo)
              countServer++
            } else {
              var photo = {
                uri: option.Images[i].image,
                type: "image/jpeg",
                name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
              }

              data.append("ImageFiles[" + countLocal + "]", photo)
              var type = option.Images[i].type == "camera" ? 1 : 2
              data.append("ImageDates[" + countLocal + "]", option.Images[i].timestamp)
              data.append("ImageTypes[" + countLocal + "]", type)
              countLocal++
            }
          }
        }

        if(option.ImageBucketCuttingEdgeWidths != "") {
          var countLocal = 0
          var countServer = 0
          for(var i = 0; i < option.ImageBucketCuttingEdgeWidths.length; i++) {
            if(option.ImageBucketCuttingEdgeWidths[i].id) {
              var photo = option.ImageBucketCuttingEdgeWidths[i].id
              data.append("ImageBucketCuttingEdgeWidthIds[" + countServer + "]", photo)
              countServer++
            } else {
              var photo = {
                uri: option.ImageBucketCuttingEdgeWidths[i].image,
                type: "image/jpeg",
                name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
              }

              data.append("ImageBucketCuttingEdgeWidthFiles[" + countLocal + "]", photo)
              var type = option.ImageBucketCuttingEdgeWidths[i].type == "camera" ? 1 : 2
              data.append("ImageBucketCuttingEdgeWidthDates[" + countLocal + "]", option.ImageBucketCuttingEdgeWidths[i].timestamp)
              data.append("ImageBucketCuttingEdgeWidthTypes[" + countLocal + "]", type)
              countLocal++
            }
          }
        }

        console.log("dataAppend05_SaveDrillingChild", data)
        axios
          .post(API_IP_GET + "step05/SaveDrillingChild", data, config)
          .then(function (response) {
            console.log("SaveDrillingChild", response.data)
            if(response.data.Code == "102") {
              return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
            } else if(response.data.Code == "001") {
              dispatch(getDrillingChild({
                jobid: option.JobId,
                pileid: option.PileId,
                Language: I18n.locale
              }))
              return dispatch({ type: PILE_PILE05_ADDROW_SUCCESS })
            } else {
              return dispatch({ type: PILE_ERROR, payload: response.data.Message })
            }
          })
          .catch(function (error) {
            console.log(error)
            firebase.crashlytics().log('step05/SaveDrillingChild')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
            dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
          })

  }
}

export const pileSaveStep06 = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token
    console.log('pileSaveStep06')
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("Page", option.Page)
    data.append("startdate", option.startdate)
    data.append("enddate", option.enddate)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}
    data.append("CrossFlag", option.CrossFlag)

    if(option.Page == 1) {
      if(option.CrossFlag){
        data.append("ChangeDrillingFluidStartDate", option.ChangeDrillingFluidStartDate)
        data.append("ChangeDrillingFluidEndDate", option.ChangeDrillingFluidEndDate)
        data.append("ChangeDrillingFluidBeforeDepth", option.ChangeDrillingFluidBeforeDepth)
        data.append("ChangeDrillingFluidAfterDepth", option.ChangeDrillingFluidAfterDepth)
        
      }else{
        data.append("BucketSizeId", option.BucketSizeId == undefined ? "" : option.BucketSizeId)
        data.append("MachineId", option.MachineId == undefined ? "" : option.MachineId)
        data.append("DriverId", option.DriverId)

        data.append("BucketSizeName", option.BucketSizeId == undefined ? "" : option.BucketSizeName)
        data.append("MachineName", option.MachineId == undefined ? "" : option.MachineName)
        data.append("DriverName", option.DriverName)
      }
    }
    if(option.Page == 2) {
          if(option.ImageFiles != "") {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.ImageFiles.length; i++) {
              if(option.ImageFiles[i].id) {
                var photo = option.ImageFiles[i].id
                data.append("ImageIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.ImageFiles[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }

                data.append("ImageFiles[" + countLocal + "]", photo)
                var type = option.ImageFiles[i].type == "camera" ? 1 : 2
                data.append("ImageDates[" + countLocal + "]", option.ImageFiles[i].timestamp)
                data.append("ImageTypes[" + countLocal + "]", type)
                countLocal++
              }
            }
          }
          data.append("BucketChanged", option.BucketChanged)
          data.append("WaitingTimeMinute", option.WaitingTimeMinute)
          data.append("BeforeDepth", option.BeforeDepth)
          data.append("AfterDepth", option.AfterDepth)
          axios
            .post(API_IP_GET + "step06/saveall", data, config)
            .then(function (response) {
              // console.warn("saveStep06", response)
              if(response.data.Code == "102") {
                return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
              } else if(response.data.Code == "001") {
                return dispatch({ type: PILE_PILE06_SUCCESS , idupdate:2})
              } else {
                return dispatch({ type: PILE_ERROR, payload: response.data.Message })
              }
            })
            .catch(function (error) {
              console.log(error)
              firebase.crashlytics().log('step06/saveall')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
              dispatch({ type: PILE_ERROR, payload: error.message })
            })
    }

    // console.warn("dataAppend06", data)
    if(option.Page != 2) {
      console.log(data)
      if(option.CrossFlag){
          axios
          .post(API_IP_GET + "step06/saveall", data, config)
          .then(function (response) {
            // console.warn("saveStep06", response)
            if(response.data.Code == "102") {
              return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
            } else if(response.data.Code == "001") {
              return dispatch({ type: PILE_PILE06_SUCCESS , idupdate:1})
            } else {
              return dispatch({ type: PILE_ERROR, payload: response.data.Message })
            }
          })
          .catch(function (error) {
            console.log(error)
            firebase.crashlytics().log('step06/saveall')
              firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
            dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
          })
      }else{
        axios
        .post(API_IP_GET + "step06/saveall", data, config)
        .then(function (response) {
          // console.warn("saveStep06", response)
          if(response.data.Code == "102") {
            return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
          } else if(response.data.Code == "001") {
            return dispatch({ type: PILE_PILE06_SUCCESS , idupdate:1})
          } else {
            return dispatch({ type: PILE_ERROR, payload: response.data.Message })
          }
        })
        .catch(function (error) {
          console.log(error)
          firebase.crashlytics().log('step06/saveall')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
          dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
        })
      }

      
    }
  }
}
export const pileCheckStep07super = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    
    axios
      .post(API_IP_GET + "step07/checkstepofsuperstructure", data, config)
      .then(function (response) {
        if(response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          return dispatch({ type: PILE_PILE07_CHECK_SUCCESS ,lockstep: option.lockstep,step:option.step,process:option.process })
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message,lockstep: option.lockstep,process:option.process })
        }
      })
      .catch(function (error) {
        console.log(error)
        firebase.crashlytics().log('step07/checkstepofsuperstructure')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
      })
  }
}
export const pileCheckStep06super = option => {
  return async dispatch => {
    // console.warn('pileCheckStep07super')
    const token = await AsyncStorage.getItem("token")
    option.token = token
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    
    axios
      .post(API_IP_GET + "step06/checkstepofsuperstructure", data, config)
      .then(function (response) {
        
        if(response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          // console.warn('pileCheckStep06super',response.data)
          return dispatch({ type: PILE_PILE06_CHECK_SUCCESS ,lockstep: option.lockstep })
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error)
        firebase.crashlytics().log('step06/checkstepofsuperstructure')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')+'step06/checkstepofsuperstructure' })
      })
  }
}

export const pileSaveStep07 = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("Page", option.Page)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
      data.append("LocationLat", option.latitude)
      data.append("LocationLong", option.longitude)
    }
    data.append("startdate", option.startdate)
    data.append("enddate", option.enddate)
    
    if(option.Page == 1) {
      data.append("MachineId", option.MachineId == undefined ? "" : option.MachineId)
      data.append("MachineName", option.MachineName == undefined ? "" : option.MachineName)
      data.append("DriverId", option.DriverId == undefined ? "" : option.DriverId)
      data.append("DriverName", option.DriverName == undefined ? "" : option.DriverName)
    } else if(option.Page == 2) {
      if(option.category == 5&&option.parent==false){

      }else{
        // data.append("OverLifting", option.OverLifting)
        // data.append("Pcoring", option.Pcoring)
        data.append("HangingBarsLength", option.hanginglength)
        if(option.ImageLengthSteelCarry != "") {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.ImageLengthSteelCarry.length; i++) {
              if(option.ImageLengthSteelCarry[i].id) {
                var photo = option.ImageLengthSteelCarry[i].id
                data.append("ImageLengthSteelCarryIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.ImageLengthSteelCarry[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }

                data.append("ImageLengthSteelCarryFiles[" + countLocal + "]", photo)
                var type = option.ImageLengthSteelCarry[i].type == "camera" ? 1 : 2
                data.append("ImageLengthSteelCarryDates[" + countLocal + "]", option.ImageLengthSteelCarry[i].timestamp)
                data.append("ImageLengthSteelCarryTypes[" + countLocal + "]", type)
                countLocal++
              }
            }
        }
      }
    } else if(option.Page == 3) {
      
    }
   

    axios
      .post(API_IP_GET + "step07/saveall", data, config)
      .then(function (response) {
        console.log("saveStep07", response.data,data)
        if(response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          return dispatch({ type: PILE_PILE07_SUCCESS,payload:option.Page })
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error)
        firebase.crashlytics().log('step07/saveall')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail') })
      })
  }
}
export const pileSaveStep07super = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token
    // console.warn('pileSaveStep07super')
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    
    

    axios
      .post(API_IP_GET + "step08/checkstepofsuperstructure", data, config)
      .then(function (response) {
        console.log("checkstepofsuperstructure", response.data)
        if(response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          return dispatch({ type: PILE_PILE07_SUPER_SUCCESS ,lockstep: option.lockstep })
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error)
        firebase.crashlytics().log('step08/checkstepofsuperstructure')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail') })
      })
  }
}

export const pileSaveStep07Casing = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      },
      timeout: 10000
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("Page", option.Page)
    data.append("startdate", option.startdate)
    data.append("enddate", option.enddate)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}
    console.log("response weather2",data)

        var SectionDetail = []
        if(option.SectionDetail.length > 0 && option.SectionDetail != null) {
          var countLocal = 0
          var countServer = 0
          for(var i = 0; i < option.SectionDetail.length; i++) {
            if(option.SectionDetail[i].ImageSteelcage != null && option.SectionDetail[i].ImageSteelcage.length > 0) {
              for(var j = 0; j < option.SectionDetail[i].ImageSteelcage.length; j++) {
                if(option.SectionDetail[i].ImageSteelcage[j].id) {
                  var photo = option.SectionDetail[i].ImageSteelcage[j].id
                  // data.append("SectionDetail[" + i + "].ImageSteelcageIds[" + countServer + "]", photo)
                  data.append("ImageSteelcageIds", photo)
                  countServer++
                } else {
                  var photo = {
                    uri: option.SectionDetail[i].ImageSteelcage[j].image,
                    type: "image/jpeg",
                    name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                  }
                  data.append("ImageSteelcageFiles", photo)
                  var type = option.SectionDetail[i].ImageSteelcage[j].type == "camera" ? 1 : 2
                
                  data.append("ImageSteelcageDates", option.SectionDetail[i].ImageSteelcage[j].timestamp)
                  data.append("ImageSteelcageTypes", type)
                  countLocal++
                }
              }
            }
            if(
              option.SectionDetail[i].ImageConcretespacer != null &&
              option.SectionDetail[i].ImageConcretespacer.length > 0
            ) {
              var countLocal = 0
              var countServer = 0
              for(var j = 0; j < option.SectionDetail[i].ImageConcretespacer.length; j++) {
                if(option.SectionDetail[i].ImageConcretespacer[j].id) {
                  var photo = option.SectionDetail[i].ImageConcretespacer[j].id
                  data.append("ImageConcretespacerIds", photo)
                  countServer++
                } else {
                  var photo = {
                    uri: option.SectionDetail[i].ImageConcretespacer[j].image,
                    type: "image/jpeg",
                    name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                  }
                  data.append("ImageConcretespacerFiles", photo)
                  var type = option.SectionDetail[i].ImageConcretespacer[j].type == "camera" ? 1 : 2
                  data.append("ImageConcretespacerDates", option.SectionDetail[i].ImageConcretespacer[j].timestamp)
                  data.append("ImageConcretespacerTypes", type)
                  countLocal++
                }
              }
            }

            data.append("SectionId", option.SectionDetail[i].sectionId)
            data.append("Checksteelcage", option.SectionDetail[i].Checksteelcage)
            data.append("CheckConcretespacer", option.SectionDetail[i].CheckConcretespacer)
          }
        }

        console.log(data)

        axios
          .post(API_IP_GET + "step07/savesection", data, config)
          .then(function (response) {
            console.log("saveStep07casing", response)
            if(response.data.Code == "102") {
              return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
            } else if(response.data.Code == "001") {
              return dispatch({ type: PILE_PILE07_SUCCESS_CASING })
            } else {
              return dispatch({ type: PILE_ERROR, payload: response.data.Message })
            }
          })
          .catch(function (error) {
            console.log(error)
            firebase.crashlytics().log('step07/savesection')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
            dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
          })
      
  }
}

export const pileSaveStep08 = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("Page", option.Page)
    data.append("startdate",option.startdate)
    data.append("enddate",option.enddate)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}

    if(option.Page == 1) {
      data.append("MachineId", option.MachineId == undefined ? "" : option.MachineId)
      data.append("MachineName", option.MachineName == undefined ? "" : option.MachineName)
      data.append("DriverId", option.DriverId == undefined ? "" : option.DriverId)
      data.append("DriverName", option.DriverName == undefined ? "" : option.DriverName)
      if(option.shape!=1){
      data.append("TremieTypeId", option.TremieTypeId == undefined ? "" : option.TremieTypeId)
      data.append("TremieTypeName", option.TremieTypeId == undefined ? "" : option.TremieTypeName)
      }
      axios
        .post(API_IP_GET + "step08/saveall", data, config)
        .then(function (response) {
          if(response.data.Code == "102") {
            return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
          } else if(response.data.Code == "001") {
            return dispatch({ type: PILE_PILE08_SUCCESS,payload:option.Page  })
          } else {
            return dispatch({ type: PILE_ERROR, payload: response.data.Message })
          }
        })
        .catch(function (error) {
          console.log(error)
          firebase.crashlytics().log('step08/saveall')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
          dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail') })
        })
    } else if(option.Page == 3) {
          if(option.FoamImage != "") {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.FoamImage.length; i++) {
              if(option.FoamImage[i].id) {
                var photo = option.FoamImage[i].id
                data.append("ImageIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.FoamImage[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }
                data.append("ImageFiles[" + countLocal + "]", photo)
                var type = option.FoamImage[i].type == "camera" ? 1 : 2
                data.append("ImageDates[" + countLocal + "]", option.FoamImage[i].timestamp)
                data.append("ImageTypes[" + countLocal + "]", type)
                countLocal++
              }
            }
          }
         
          data.append("AddFoam", option.AddFoam)

          axios
            .post(API_IP_GET + "step08/saveall", data, config)
            .then(function (response) {
              // console.warn("saveStep08", response.data.Code)
              if(response.data.Code == "102") {
                return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
              } else if(response.data.Code == "001") {
                return dispatch({ type: PILE_PILE08_SUCCESS,payload:option.Page })
              } else {
                return dispatch({ type: PILE_ERROR, payload: response.data.Message })
              }
            })
            .catch(function (error) {
              console.log(error)
              firebase.crashlytics().log('step08/saveall')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
              dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail') })
            })
    }
  }
}

export const pileSaveTremieInsert = option => {
  return async dispatch => {

    // Save tremie temporary on local will be replace once API finish fetching
    const tempdata = {
      No: option.No,
      TremieSize: {
        Id: option.sizeid,
        Name: option.sizename,
        IsDefault: false
      },
      Length: option.length,
      IsLast: option.islast,
      Images: option.image
    }

    // dispatch({ type: TREMIESAVETEMP, payload: tempdata, volume: option.length, tremiesizeid: option.sizeid })  
    // console.warn(option.latitude,option.longitude)

    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    if(option.TremieId!=null&&option.TremieId!=undefined){
      data.append("TremieId", option.TremieId)
    }
    data.append("startdate", option.startdate)
    data.append("enddate", option.enddate)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}
    
    data.append("SizeId", option.sizeid)
    data.append("SizeName", option.sizename)
    data.append("Length", option.length)
    data.append("IsLast", option.islast)
    data.append("No", option.No)

        if(option.image != "") {
          var countLocal = 0
          var countServer = 0
          for(var i = 0; i < option.image.length; i++) {
            if(option.image[i].id) {
              var photo = option.image[i].id
              data.append("ImageIds[" + countServer + "]", photo)
              countServer++
            } else {
              var photo = {
                uri: option.image[i].image,
                type: "image/jpeg",
                name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
              }

              data.append("ImageFiles[" + countLocal + "]", photo)
              var type = option.image[i].type == "camera" ? 1 : 2
              data.append("ImageDates[" + countLocal + "]", option.image[i].timestamp)
              data.append("ImageTypes[" + countLocal + "]", type)
              countLocal++
            }
          }
        }
      

        axios
          .post(API_IP_GET + "step08/savetremie", data, config)
          .then(function (response) {
            console.log("saveStep08 tremie", response.data)
            if(response.data.Code == "102") {
              return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
            } else if(response.data.Code == "001") {
              console.log("saveStep08 tremie sucess", response.data)
              return dispatch(getTremieList({
                jobid: option.JobId,
                pileid: option.PileId
              }))
            } else {
              return dispatch({ type: PILE_ERROR, payload: response.data.Message })
            }
          })
          .catch(function (error) {
            console.log(error)
            firebase.crashlytics().log('step08/savetremie')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
            dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail') })
          })
     
  }
}

export const pileSaveTremieDelete = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("No", option.index)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}

    axios
      .post(API_IP_GET + "step08/removetremie", data, config)
      .then(function (response) {
        // console.log("SAVE STEP8")
        // console.log(response)
        if(response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          return dispatch(getTremieList({
            jobid: option.JobId,
            pileid: option.PileId
          }))
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error)
        firebase.crashlytics().log('step08/removetremie')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: error.message })
      })
  }
}


export const pileSaveStep9 = option => {
  return async dispatch => {

    if (option.TruckRegisterId == null) {
      var temp = option
      temp.ImageTicket = option.ticketimage
      temp.ImageSlump = option.slumpimage
      temp.ImageTruck = option.truckimage
      temp.Local = true
      dispatch({ type: CONCRETE_ADDTEMP, payload: temp, volume: option.TruckConcreteVolume, mixid: option.MixNoId, concreteid: option.ConcreteSuplierId })
    }else{
      var temp = option
      temp.ImageTicket = option.ticketimage
      temp.ImageSlump = option.slumpimage
      temp.ImageTruck = option.truckimage
      temp.Local = true
      dispatch({ type: CONCRETE_EDITTEMP, payload: temp, volume: option.TruckConcreteVolume, mixid: option.MixNoId, concreteid: option.ConcreteSuplierId, index:option.No-1 })
    }
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("TruckRegisterId", option.TruckRegisterId)
    data.append("TruckNo", option.TruckNo)
    data.append("TruckArrivalTime", option.TruckArrivalTime)
    data.append("TruckDepartureTime", option.TruckDepartureTime)
    data.append("TruckConcreteVolume", option.TruckConcreteVolume)
    data.append("ConcreteSuplierId", option.ConcreteSuplierId)
    data.append("ConcreteSuplierName", option.ConcreteSuplierName)
    data.append("MixNoId", option.MixNoId)
    data.append("MixNoName", option.MixNoName)
    data.append("Slump", option.Slump)
    data.append("IsReject", option.IsReject)
    data.append("ApproveType", option.ApproveType)
    data.append("ApproveEmployeeId", option.ApproveEmployeeId)
    data.append("SlumpFlag", option.SlumpFlag)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}
    console.log("savestep9 data",data)
    
        if(option.ticketimage != "") {
          var countLocal = 0
          var countServer = 0
          for(var i = 0; i < option.ticketimage.length; i++) {
            if(option.ticketimage[i].id) {
              var photo = option.ticketimage[i].id
              data.append("ImageTicketIds[" + countServer + "]", photo)
              countServer++
            } else {
              var photo = {
                uri: option.ticketimage[i].image,
                type: "image/jpeg",
                name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
              }

              data.append("ImageTicketFiles[" + countLocal + "]", photo)
              var type = option.ticketimage[i].type == "camera" ? 1 : 2
              data.append("ImageTicketDates[" + countLocal + "]", option.ticketimage[i].timestamp)
              data.append("ImageTicketTypes[" + countLocal + "]", type)
              countLocal++
            }
          }
        }

        // Slump image
        if(option.slumpimage != "") {
          var countLocal = 0
          var countServer = 0
          for(var i = 0; i < option.slumpimage.length; i++) {
            if(option.slumpimage[i].id) {
              var photo = option.slumpimage[i].id
              data.append("ImageSlumpIds[" + countServer + "]", photo)
              countServer++
            } else {
              var photo = {
                uri: option.slumpimage[i].image,
                type: "image/jpeg",
                name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
              }

              data.append("ImageSlumpFiles[" + countLocal + "]", photo)
              var type = option.slumpimage[i].type == "camera" ? 1 : 2
              data.append("ImageSlumpDates[" + countLocal + "]", option.slumpimage[i].timestamp)
              data.append("ImageSlumpTypes[" + countLocal + "]", type)
              countLocal++
            }
          }
        }
        // Image truck
        if(option.truckimage != "") {
          var countLocal = 0
          var countServer = 0
          for(var i = 0; i < option.truckimage.length; i++) {
            if(option.truckimage[i].id) {
              var photo = option.truckimage[i].id
              data.append("ImageTruckIds[" + countServer + "]", photo)
              countServer++
            } else {
              var photo = {
                uri: option.truckimage[i].image,
                type: "image/jpeg",
                name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
              }

              data.append("ImageTruckFiles[" + countLocal + "]", photo)
              var type = option.truckimage[i].type == "camera" ? 1 : 2
              data.append("ImageTruckDates[" + countLocal + "]", option.truckimage[i].timestamp)
              data.append("ImageTruckTypes[" + countLocal + "]", type)
              countLocal++
            }
          }
        }
      

        axios
          .post(API_IP_GET + "step09/saveconcreteregister", data, config)
          .then(function (response) {
            // console.log("saveStep09 saveconcrete", response)
            if(response.data.Code == "102") {
              return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
            } else if(response.data.Code == "001") {
              dispatch(getStepStatus({
                jobid: option.JobId,
                pileid: option.PileId
              }))
              return dispatch(getConcreteList({
                jobid: option.JobId,
                pileid: option.PileId
              }))
              // return dispatch({ type: PILE_PILE09_SUCCESS })
            } else {
              dispatch(getStepStatus({
                jobid: option.JobId,
                pileid: option.PileId
              }))
              dispatch(getConcreteList({
                jobid: option.JobId,
                pileid: option.PileId
              }))
              return dispatch({ type: PILE_ERROR, payload: response.data.Message })
            }
          })
          .catch(function (error) {
            console.log(error)
            firebase.crashlytics().log('step09/saveconcreteregister')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
            dispatch({ type: PILE_ERROR, payload: error.message })
          })
     
  }
}

export const pileCancelSlumpApprove = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("TruckRegisterId", option.TruckRegisterId)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}

    console.log("dataCancelSlump", data)

    axios
      .post(API_IP_GET + "step09/rejectapproved", data, config)
      .then(function (response) {
        console.log("CancelStep09", response)
        if(response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          return dispatch({ type: PILE_SLUMP_CANCEL_APPROVE_SUCCESS })
        } else {
          return dispatch({ type: PILE_SLUMP_CANCEL_APPROVE_FAIL, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error.response)
        firebase.crashlytics().log('step09/rejectapproved')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_SLUMP_CANCEL_APPROVE_FAIL, payload: error.message })
      })
  }
}

export const removeConcreteList = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("TruckRegisterId", option.TruckRegisterId)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}

    axios
      .post(API_IP_GET + "step09/removeconcreteregister", data, config)
      .then(function (response) {
        // console.warn(response)
        if(response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          return dispatch(getConcreteList({
            jobid: option.JobId,
            pileid: option.PileId
          }))
          // return dispatch({ type: PILE_PILE09_SUCCESS })
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error)
        firebase.crashlytics().log('step09/removeconcreteregister')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: error.message })
      })
  }
}

export const pileSaveStep10 = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("Page", option.Page)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}
    if(option.Page == 1) {
      data.append("ForemanSetId", option.foremansetid)
      data.append("ForemanSetName", option.ForemanSetName)
      data.append("ForemanId", option.foremanid)
      data.append("ForemanName", option.ForemanName)
      data.append("ConcreteOvercast", option.Overcast)
    } else if(option.Page == 2) {
      data.append("Weather", option.Weather != "null"? option.Weather:"")
      data.append("IsFine",option.IsFine)
      data.append("IsCloudy",option.IsCloudy)
      data.append("IsRainy",option.IsRainy)
      data.append("IsHail",option.IsHail)
      data.append("IsWindy",option.IsWindy)
      
    }
   

    axios
      .post(API_IP_GET + "step10/saveall", data, config)
      .then(function (response) {
        // console.log(response)
        if(response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          return dispatch({ type: PILE_PILE10_SUCCESS ,payload:option.Page})
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error)
        firebase.crashlytics().log('step10/saveall')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
      })
    
  }
}

export const pileSaveStep10_3 = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    console.warn('option.No',option.No)
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}
    data.append("No", option.No)
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("ConcreteTruckRegisterId", option.ConcreteTruckRegisterId)
    data.append("StartConcreting", option.StartConcreting)
    data.append("EndConcreting", option.EndConcreting)
    data.append("Depth", option.Depth)
    data.append("IsLastTruck", option.IsLastTruck)
    data.append("LastTruckCheckPVCandPlummet", option.LastTruckCheckPVCandPlummet)
    data.append("LastTruckPVCConcreteHeight", option.LastTruckPVCConcreteHeight)
    data.append("LastTruckPlummetConcreteHeight", option.LastTruckPlummetConcreteHeight)
    data.append("LastTruckConcreteVolume", option.LastTruckConcreteVolume)
    data.append("TremieCutCount", option.TremyCutCount)
    data.append("ConcreteRecordId", option.ConcreteRecordId)
    data.append("TremyCutLength", option.TremyCutLength)
    data.append("TremyCutSinkAfter", option.TremyCutSinkAfter)
    data.append("TremyCutSinkBefore", option.TremyCutSinkBefore)
    data.append("TremyCutLeft", option.TremyCutLeft)

    data.append("WidthConcrete", option.LastTruckCheckOverWidth)
    data.append("LengthConcrete", option.LastTruckCheckOverLength)
    data.append("HeightConcrete", option.LastTruckCheckOverHeight)
    data.append("RemainConcreteVolumn", option.RemainConcreteVolumn)
    data.append("RemainConcreteId", option.RemainConcreteId)
    data.append("RemainConcreteName", option.RemainConcreteName)
    data.append("RemainConcretePileNo", option.RemainConcretePileNo)

    data.append('ResidualConcreteVolumn',option.ResidualConcreteVolumn)

    data.append('StopPouringConcrete',option.StopPouringConcrete)
    data.append('ConcreteCalculateVolumn',option.ConcreteCalculateVolumn)
    
    
     
        if(option.imageconcrete != "") {
          var countLocal = 0
          var countServer = 0
          for(var i = 0; i < option.imageconcrete.length; i++) {
            if(option.imageconcrete[i].id) {
              var photo = option.imageconcrete[i].id
              data.append("ImageConcreteIds[" + countServer + "]", photo)
              countServer++
            } else {
              var photo = {
                uri: option.imageconcrete[i].image,
                type: "image/jpeg",
                name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
              }

              data.append("ImageConcreteFiles[" + countLocal + "]", photo)
              var type = option.imageconcrete[i].type == "camera" ? 1 : 2
              data.append("ImageConcreteDates[" + countLocal + "]", option.imageconcrete[i].timestamp)
              data.append("ImageConcreteTypes[" + countLocal + "]", type)
              countLocal++
            }
          }
        }

        // Slump image
        if(option.imagelasttruck != "") {
          var countLocal = 0
          var countServer = 0
          for(var i = 0; i < option.imagelasttruck.length; i++) {
            if(option.imagelasttruck[i].id) {
              var photo = option.imagelasttruck[i].id
              data.append("ImageLastTruckIds[" + countServer + "]", photo)
              countServer++
            } else {
              var photo = {
                uri: option.imagelasttruck[i].image,
                type: "image/jpeg",
                name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
              }

              data.append("ImageLastTruckFiles[" + countLocal + "]", photo)
              var type = option.imagelasttruck[i].type == "camera" ? 1 : 2
              data.append("ImageLastTruckDates[" + countLocal + "]", option.imagelasttruck[i].timestamp)
              data.append("ImageLastTruckTypes[" + countLocal + "]", type)
              countLocal++
            }
          }
        }

        //lasttruckoverimage

        if(option.imagelasttruckover != "") {
          var countLocal = 0
          var countServer = 0
          for(var i = 0; i < option.imagelasttruckover.length; i++) {
            if(option.imagelasttruckover[i].id) {
              var photo = option.imagelasttruckover[i].id
              data.append("ImageConcreteRecordDumpIds[" + countServer + "]", photo)
              countServer++
            } else {
              var photo = {
                uri: option.imagelasttruckover[i].image,
                type: "image/jpeg",
                name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
              }

              data.append("ImageConcreteRecordDumpFiles[" + countLocal + "]", photo)
              var type = option.imagelasttruckover[i].type == "camera" ? 1 : 2
              data.append("ImageConcreteRecordDumpDates[" + countLocal + "]", option.imagelasttruckover[i].timestamp)
              data.append("ImageConcreteRecordDumpTypes[" + countLocal + "]", type)
              countLocal++
            }
          }

        }
       
        console.log('imagelasttruckover',data)
        axios
          .post(API_IP_GET + "step10/saveconcreterecord", data, config)
          // .post("google.com", data, config)
          .then(function (response) {
            // console.log("save 10-3", response.da)
            if(response.data.Code == "102") {
              return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
            } else if(response.data.Code == "001") {
              return dispatch(getConcreteRecord({
                jobid: option.JobId,
                pileid: option.PileId
              }))
              // return dispatch({ type: PILE_PILE09_SUCCESS })
            } else {
              return dispatch({ type: PILE_ERROR, payload: response.data.Message })
            }
          })
          .catch(function (error) {
            console.log('error 10_drop');
            if (error.response) {
              // Request made and server responded
              console.log('response')
              console.log(error.response.data);
              console.log(error.response.status);
              console.log(error.response.headers);
            } else if (error.request) {
               // The request was made but no response was received
               console.log('request',error.request);
            } else {
               // Something happened in setting up the request that triggered an Error
               console.log('Error message', error.message);
            }
            console.log(error)
            firebase.crashlytics().log('step10/saveconcreterecord')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
            dispatch({ type: PILE_ERROR, payload: error.message })
          })
     
  }
}
export const pileSaveStep10_3confirm = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }

    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("IsNext", option.confirm)

    axios
          .post(API_IP_GET + "step10/ConfirmNextProcess", data, config)
          .then(function (response) {
            // console.warn("save 10-3", response)
            if(response.data.Code == "102") {
              return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
            } else if(response.data.Code == "001") {

              dispatch(getStepStatus({
                jobid: option.JobId,
                pileid: option.PileId
              }))
              dispatch(getConcreteRecord({
                jobid: option.JobId,
                pileid: option.PileId
              }))
              // return
              // return dispatch({ type: PILE_PILE09_SUCCESS })
              if(option.confirm==true){
                return dispatch({ type: PILE_PILE10_SUCCESS ,payload:3,confirm:true})
              }else{
                return dispatch({ type: PILE_PILE10_SUCCESS ,payload:3,confirm:false})
              }
              
              // return dispatch({ type: PILE_PILE10_SUCCESS ,payload:})
            } else {
              return dispatch({ type: PILE_ERROR, payload: response.data.Message })
            }
          })
          .catch(function (error) {
            console.log(error)
            firebase.crashlytics().log('step10/ConfirmNextProcess')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
            dispatch({ type: PILE_ERROR, payload: error.message })
          })


  }
}

export const pileSaveStep10_3confirmToNext = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token
    // console.warn('pileSaveStep07super')
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    
    

   let Tonext = await axios
      .post(API_IP_GET + "step11/checkinsertdata", data, config)
      .then(function (response) {
        console.log("ConfirmNextProcess step10", response.data)
        if(response.data.Code == "102") {
          // return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
          return {status:false,Message:response.data.Message }
        } else if(response.data.Code == "001") {
          
          // return dispatch({ type: PILE_PILE07_SUPER_SUCCESS ,lockstep: option.lockstep })
          return {status:true,Message: ''}
        } else {
          return {status:false,Message:response.data.Message }
          // return dispatch({ type: PILE_ERROR, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error)
        firebase.crashlytics().log('step08/checkstepofsuperstructure')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail') })
      })
      return Tonext
  }
}

export const removeConcreteRecord = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("ConcreteRecordId", option.ConcreteRecordId)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)}

    axios
      .post(API_IP_GET + "step10/removeconcreterecord", data, config)
      .then(function (response) {
        console.log(response)
        if(response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          dispatch(getStepStatus({
            jobid: option.JobId,
            pileid: option.PileId
          }))
          return dispatch(getConcreteRecord({
            jobid: option.JobId,
            pileid: option.PileId
          }))
          // return dispatch({ type: PILE_PILE09_SUCCESS })
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error)
        firebase.crashlytics().log('step10/removeconcreteregister')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: error.message })
      })
  }
}

export const pileSaveStep11 = option => {
  return async dispatch => {
    console.log("data save step 11 ",option)
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    data.append("Page", option.Page)
    data.append("startdate", option.startdate)
    data.append("enddate", option.enddate)
    if((option.latitude||option.longitude) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
    data.append("LocationLat", option.latitude)
    data.append("LocationLong", option.longitude)
  }
    // console.log(,data)
    if(option.Page == 1) {
      data.append("MachineId", option.MachineId == undefined ? "" : option.MachineId)
      data.append("MachineName", option.MachineName == undefined ? "" : option.MachineName)
      data.append("DriverId", option.DriverId == undefined ? "" : option.DriverId)
      data.append("DriverName", option.DriverId == undefined ? "" : option.DriverName)
      data.append("VibroId", option.VibroId == undefined ? "" : option.VibroId)
      data.append("VibroName", option.VibroName == undefined ? "" : option.VibroName)

      axios
        .post(API_IP_GET + "step11/saveall", data, config)
        .then(function (response) {
          // console.log("SAVE STEP8")
          // console.log("data save step 11 ",response.data)
          if(response.data.Code == "102") {
            return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
          } else if(response.data.Code == "001") {
            return dispatch({ type: PILE_PILE11_SUCCESS,payload:option.Page })
          } else {
            return dispatch({ type: PILE_ERROR, payload: response.data.Message })
          }
        })
        .catch(function (error) {
          console.log(error)
          dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
        })
    } else if(option.Page == 2) {
      data.append("CheckPlummet", option.CheckPlummet)
      data.append("CheckWaterLevel", option.CheckWaterLevel)
    
          if(option.ImagePlummet != (""||null||undefined)) {
            var countLocal = 0
            var countServer = 0
            console.log("plummetimage lenght",option.ImagePlummet)
            // try{
              console.log("plummetimage lenght",option.plummetimage)
            // }catch(error){

            // }
            
            for(var i = 0; i < option.ImagePlummet.length; i++) {
              if(option.ImagePlummet[i].id) {
                var photo = option.ImagePlummet[i].id
                data.append("ImagePlummetIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.ImagePlummet[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }
                data.append("ImagePlummetFiles[" + countLocal + "]", photo)
                var type = option.ImagePlummet[i].type == "camera" ? 1 : 2
                data.append("ImagePlummetDates[" + countLocal + "]", option.ImagePlummet[i].timestamp)
                data.append("ImagePlummetTypes[" + countLocal + "]", type)
                countLocal++
              }
            }
          
          }
          if(option.waterimage != (""||null||undefined)) {
            var countLocal = 0
            var countServer = 0
            for(var i = 0; i < option.waterimage.length; i++) {
              if(option.waterimage[i].id) {
                var photo = option.waterimage[i].id
                data.append("ImageWaterLevelIds[" + countServer + "]", photo)
                countServer++
              } else {
                var photo = {
                  uri: option.waterimage[i].image,
                  type: "image/jpeg",
                  name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
                }
                data.append("ImageWaterLevelFiles[" + countLocal + "]", photo)
                var type = option.waterimage[i].type == "camera" ? 1 : 2
                data.append("ImageWaterLevelDates[" + countLocal + "]", option.waterimage[i].timestamp)
                data.append("ImageWaterLevelTypes[" + countLocal + "]", type)
                countLocal++
              }
            }
          }
         

          axios
            .post(API_IP_GET + "step11/saveall", data, config)
            .then(function (response) {
              console.log("saveStep08", response)
              console.log("SAVE step2", response)
              if(response.data.Code == "102") {
                return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
              } else if(response.data.Code == "001") {
                return dispatch({ type: PILE_PILE11_SUCCESS,payload:option.Page })
              } else {
                return dispatch({ type: PILE_ERROR, payload: response.data.Message })
              }
            })
            .catch(function (error) {
              console.log(error)
              dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
            })
       
    }
    else if(option.Page == 3) {
      data.append("ConcreteLevelBeforeWithdraw", option.ConcreteLevelBeforeWithdraw == undefined ? "" : option.ConcreteLevelBeforeWithdraw)
      data.append("ConcreteLevelAfterWithdraw", option.ConcreteLevelAfterWithdraw == undefined ? "" : option.ConcreteLevelAfterWithdraw)
      data.append("ConcreteBleeding", option.ConcreteBleeding == undefined ? "" : option.ConcreteBleeding)

      axios
        .post(API_IP_GET + "step11/saveall", data, config)
        .then(function (response) {
          // console.log("SAVE STEP8")
          // console.warn(response.data)
          if(response.data.Code == "102") {
            return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
          } else if(response.data.Code == "001") {
            return dispatch({ type: PILE_PILE11_SUCCESS,payload:option.Page  })
          } else {
            return dispatch({ type: PILE_ERROR, payload: response.data.Message })
          }
        })
        .catch(function (error) {
          console.log(error)
          firebase.crashlytics().log('step11/saveall')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
          dispatch({ type: PILE_ERROR, payload: I18n.t('alert.recordfail')})
        })
    }
  }
}

export const pileDelete = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("JobId", option.jobid)
    data.append("Version", getVersionApp())
    data.append("PileId", option.pileid)
    data.append("ProcessId", option.processid)
    if((option.lat||option.log) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
      data.append("LocationLat", option.lat)
      data.append("LocationLong", option.log)
    }

    axios
      .post(API_IP_GET + "savesteppile/removestep", data, config)
      .then(function (response) {
        console.log("delete data",response)
        if(response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          dispatch(clearStatus())
          dispatch(pileClear())
          dispatch(mainRefresh(1))
          dispatch(ungetStepStatus2())
          return dispatch({ type: PILE_PILE08_SUCCESS })
        } else {
          return dispatch({ type: PILE_ERROR, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error)
        firebase.crashlytics().log('savesteppile/removestep')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: error.message })
      })
  }
}

export const pile4ClearId = option => {
  console.log('pile4ClearId')
  return async dispatch => {
    dispatch({type:PILE_PILE04_CLEAR_ID})
  }
}
export const pileitemsclear = (data) => {
  console.log('pileitemsclear')
  return async dispatch => {
    dispatch({type:PILE_ITEMS_CLEAR, clearerp:data.clearerp})
  }
}

export const importtoerp = option => {
 
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    // data.append("ProcessId", option.processid)
    if((option.lat||option.log) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
      data.append("LocationLat", option.lat)
      data.append("LocationLong", option.log)
    }
    // console.log("Importtoerp",data)
    axios
      .post(API_IP_GET + "blockprocess/importtoerp", data, config)
      .then(function (response) {
        console.log("Importtoerp",response)
        if(response.data.Code == "102") {
          // return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          // dispatch(pileitemsclear({clearerp:true}))
          // dispatch(pileItems({jobid: option.JobId,Language:option.Language,start:0,length:27,status:0 }))
          // dispatch(pileItems({jobid: option.JobId,Language:option.Language,start:0,length:10,status:1 }))
          // dispatch(pileItems({jobid: option.JobId,Language:option.Language,start:0,length:10,status:2 }))
      
          // return dispatch({ type: Importtoerp })
        } else {
          // return dispatch({ type: PILE_ERROR, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error)
        firebase.crashlytics().log('blockprocess/importtoerp')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        // dispatch({ type: PILE_ERROR, payload: error.message })
      })
  }
}
export const unblockerp = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.Language)
    data.append("Version", getVersionApp())
    data.append("JobId", option.JobId)
    data.append("PileId", option.PileId)
    // data.append("ProcessId", option.processid)
    if((option.lat||option.log) == 1){
      data.append("LocationLat", "")
      data.append("LocationLong", "")
    }else{
      data.append("LocationLat", option.lat)
      data.append("LocationLong", option.log)
    }

    axios
      .post(API_IP_GET + "blockprocess/unblockerp", data, config)
      .then(function (response) {
        // console.warn("Unblockerp",response)
        if(response.data.Code == "102") {
          // return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message })
        } else if(response.data.Code == "001") {
          var data1 = {
            title: option.pile_no, 
            pileid: option.PileId, 
            jobid: option.JobId, 
            category:option.category
          }
          dispatch({ type: loading, loading:false })
          return dispatch({ type: Unblockerp, data_Unblockerp:data1 })

        } else {
          // return dispatch({ type: PILE_ERROR, payload: response.data.Message })
        }
      })
      .catch(function (error) {
        console.log(error)
        firebase.crashlytics().log('blockprocess/unblockerp')
            firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        // dispatch({ type: PILE_ERROR, payload: error.message })
      })
  }
}
export const checkmessagepile = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.Version =  getVersionApp()
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }
    axios
      .get(API_IP_GET + "searchpile/checkmessagepile", { params: data }, config)
      .then(function (response) {
        // console.warn("checkmessagepile", response.data.Code)
        if(response.data.Code == "001") {
          
        } else {
          Alert.alert('',response.data.Message)
        }
      })
      .catch(function (error) {
        console.log(error.response)
        firebase.crashlytics().log('checkmessagepile')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: PILE_ERROR, payload: error.message })
      })
  }
}
export const setloading1 = option => {
  return async dispatch => {
    dispatch({ type: loading, loading:true })
  }

}
