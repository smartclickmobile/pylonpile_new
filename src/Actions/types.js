/* AUTH */
export const AUTH_LOGIN_SUCCESS = 'auth_login_success'
export const AUTH_LOGIN_ERROR = 'auth_login_error'
export const AUTH_LOGIN_DUP = 'auth_login_dup'
export const AUTH_LOGOUT_SUCCESS = 'auth_logout_success'
export const AUTH_LOGOUT_ERROR = 'auth_logout_error'
export const AUTH_RESET = 'auth_reset'
export const AUTH_CHANGEPASS_SUCCESS = 'auth_changepass_success'
export const AUTH_CHANGEPASS_FAIL = 'auth_changepass_fail'
export const AUTH_LOADING = 'auth_loading'
export const USER_INFO_SUCCESS = 'user_info_success'
export const USER_INFO_ERROR = 'user_info_error'
/* JOB */
export const JOBLIST_SUCCESS = 'joblist_success'
export const JOBLIST_ERROR = 'joblist_error'
export const JOBDETAIL_SUCCESS = 'jobdetail_success'
export const JOBDETAIL_ERROR = 'jobdetail_error'
export const JOBLIST_CLEAR = 'joblist_clear'
export const JOBDETAIL_CLEAR = 'jobdetail_clear'
/** Main Pile */
export const MAIN_PILE_GET_STORAGE = 'main_pile_get_storage'
export const MAIN_PILE_GET_STORAGE_ERROR = 'main_pile_get_storage_error'
export const MAIN_PILE_GET_ALL_STORAGE = 'main_pile_get_all_storage'
export const MAIN_PILE_GET_ALL_STORAGE_ERROR = 'main_pile_get_all_storage_error'
export const MAIN_PILE_STACK = 'main_pile_stack'
export const MAIN_PILE_CLEAR_STACK = 'main_pile_clear_stack'
export const MAIN_PILE_STEP_STATUS = 'main_pile_step_status'
export const MAIN_PILE_CLEAR_STATUS = 'main_pile_clear_status'
export const STEP_STATUS = 'step_status'
export const STEP_STATUS_UN = 'step_status_un'
export const MAIN_PILE_CLEAR = 'main_pile_clear'

/* PILE */
export const PILE_ERROR = 'pile_error'
export const PILE_ITEM = 'pile_item'
export const PILE_ITEMS = 'pile_items'
export const PILE_ITEMS_CLEAR = 'pile_items_clear'
export const PILE_VALUE_INFO = 'pile_value_info'
export const PILE_VALUE_INFO_RESET = 'pile_value_info_reset'
export const PILE_MASTER_INFO = 'pile_master_info'
export const PILE_PILE01_SUCCESS = 'pile_pile01_success'
export const PILE_PILE01_SUCCESS_1 = 'pile_pile01_success_1'
export const PILE_PILE01_SUCCESS_3 = 'pile_pile01_success_3'
export const PILE_PILE02_SUCCESS = 'pile_pile02_success'
export const PILE_PILE02_SUCCESS_1 = 'pile_pile02_success_1'
export const PILE_PILE03_SUCCESS = 'pile_pile03_success'
export const PILE_PILE03_SUCCESS_1 = 'pile_pile03_success_1'
export const PILE_PILE03_SUCCESS_2 = 'pile_pile03_success_2'
export const PILE_PILE03_SUCCESS_3 = 'pile_pile03_success_3'
export const PILE_PILE03_SUCCESS_4 = 'pile_pile03_success_4'
export const PILE_PILE04_SUCCESS = 'pile_pile04_success'
export const PILE_PILE05_SUCCESS = 'pile_pile05_success'
export const PILE_PILE05_SUCCESS_1 = 'pile_pile05_success_1'
export const PILE_PILE05_SUCCESS_4 = 'pile_pile05_success_4'
export const PILE_PILE05_CHECK_SUCCESS = 'pile_pile05_check_success'
export const PILE_PILE06_SUCCESS = 'pile_pile06_success'
export const PILE_PILE07_SUCCESS = 'pile_pile07_success'
export const PILE_PILE07_SUPER_SUCCESS = 'pile_pile07_super_success'
export const PILE_PILE07_SUCCESS_CASING = 'pile_pile07_success_casing'
export const PILE_PILE07_CHECK_SUCCESS = 'pile_pile07_check_success'
export const PILE_PILE06_CHECK_SUCCESS = 'pile_pile06_check_success'
export const PILE_PILE08_SUCCESS = 'pile_pile08_success'
export const PILE_PILE09_SUCCESS = 'pile_pile09_success'
export const PILE_PILE10_SUCCESS = 'pile_pile10_success'
export const PILE_PILE11_SUCCESS = 'pile_pile11_success'
export const PILE_PILE05_ADDROW_SUCCESS = 'pile_pile05_addrow_success'
export const PILE_PILE04_DELETE_SUCCESS = 'pile_pile04_delete_success'
export const PILE_PILE05_DELETE_SUCCESS = 'pile_pile05_delete_success'
export const PILE_PILE03_APPROVE_SUCCESS = 'pile_pile03_approve_success'
export const PILE_PILE03_APPROVE_FAIL = 'pile_pile03_approve_fail'
export const PILE_PILE03_CANCEL_APPROVE_SUCCESS = 'pile_pile03_cancel_approve_success'
export const PILE_PILE03_CANCEL_APPROVE_FAIL = 'pile_pile03_cancel_approve_fail'

export const PILE_ERROR_CLEAR = 'pile_error_clear'
export const PILE_COUNT = 'pile_count'
export const PILE_CLEAR = 'pile_clear'
export const PILE_LOADING = 'pile_loading'
export const PILE_PILE04_CLEAR_ID = 'pile_pile04_clear_id'

export const PILE_PILE09_ERROR = 'pile_pile09_error'
export const PILE_SLUMP_CANCEL_APPROVE_SUCCESS = 'pile_slump_cancel_approve_success'
export const PILE_SLUMP_CANCEL_APPROVE_FAIL = 'pile_slump_cancel_approve_fail'

export const Importtoerp = 'Importtoerp'
export const Unblockerp = 'Unblockerp'

/* Provider */
export const PROVIDER_ERROR = 'provider_error'
export const PROVIDER_ITEMS = 'provider_items'

/* Steel Cage */
export const STEEL_CAGE_ERROR = 'steel_cage_error'
export const STEEL_CAGE_ERROR_RESET = 'steel_cage_error_reset'
export const STEEL_CAGE_ITEMS = 'steel_cage_items'

/* Steel Sec Detail */
export const STEEL_SEC_DETAIL_ERROR = 'steel_sec_detail_error'
export const STEEL_SEC_DETAIL_ERROR_RESET = 'steel_sec_detail_error_reset'
export const STEEL_SEC_DETAIL = 'steel_sec_detail'

/* Notification */
export const NOTILIST_SUCCESS = 'notilist_success'
export const NOTILIST_ERROR = 'notilist_error'
export const NOTIDETAIL_SUCCESS = 'notidetail_success'
export const NOTIDETAIL_ERROR = 'notidetail_error'
export const NOTICONFIRM_SUCCESS = 'noticonfirm_success'
export const NOTICONFIRM_ERROR = 'noticonfirm_error'
export const NOTISET = 'notiset'
export const NOTICLEAR = 'noticlear'

/* Duplicate */
export const DUPLICATE_LOGIN = 'duplicate_login'

/** History */
export const HISTORYLIST_SUCCESS = 'historylist_success'
export const HISTORYLISTMORE_SUCCESS = 'historylistmore_success'
export const HISTORYLISTMORE_END = 'historylistmore_end'
export const HISTORYLIST_ERROR = 'historylist_error'
export const HISTORYLISTMORE_ERROR = 'historylistmore_error'

/** Concrete */
export const ConcreteList = 'concrete_list'
export const ConcreteError = 'concrete_error'
export const ConcreteList10 = 'concrete_list10'
export const ConcreteError10 = 'concrete_error10'
export const ConcreteRecord = 'concrete_record'
export const ConcreteRecordError = 'concrete_record_error'
export const CONCRETE_ADDTEMP = 'concrete_addtemp'
export const CONCRETE_EDITTEMP = 'concrete_edittemp'
export const ConcreteRecordConfirm = 'concrete_record_Confirm'
/** Note */
export const NOTE = 'note'
export const NOTE_SAVE = 'note_save'
export const NOTE_ERROR = 'note_error'

/** Tremie */
export const TREMIELIST = 'tremielist'
export const TREMIEERROR = 'tremieerror'
export const TREMIELOADING = 'tremieloading'
export const TREMIESAVETEMP = 'tremiesavetemp'

/** DrillingFluids */
export const DrillingFluidslist = 'drillingfluids_list'
export const DrillingFluidsError = 'drillingfluids_error'
export const DrillingFluidId_ADDTEMP = 'drillingfluids_addtemp'
export const DrillingFluidId_EDITTEMP = 'drillingfluids_edittemp'

/** Drilling */
export const Drilling = 'drilling'
export const DrillingError = 'drilling_error'
export const Drilling_ADDTEMP = 'drilling_addtemp'
export const Drilling_EDITTEMP = 'drilling_edittemp'

export const DrillingChild = 'drillingchild'
export const DrillingChildError = 'drillingchild_error'
export const DrillingChild_ADDTEMP = 'drillingchild_addtemp'
export const DrillingChild_EDITTEMP = 'drillingchild_edittemp'

/** Get step */
export const STEP = 'step'

export const loading = 'loading'

export const LOCATION = 'location'
export const LOCATION_ERROR = 'location_error'
