import axios from "axios"
import AsyncStorage from '@react-native-community/async-storage';
import { API_IP } from "../Constants/Constant"
import I18n from "../../assets/languages/i18n"
import {
  DUPLICATE_LOGIN,
  HISTORYLIST_SUCCESS,
  HISTORYLIST_ERROR,
  HISTORYLISTMORE_SUCCESS,
  HISTORYLISTMORE_ERROR,
  HISTORYLISTMORE_END
} from "./types"
import firebase from "react-native-firebase"
import { getVersionApp } from './AuthAction'
export const historyList = value => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    var api = {
      token,
      language: I18n.currentLocale()
    }
    var data = { ...api, ...value }
    data.Version =  getVersionApp()
    axios
      .get(API_IP + "history", { params: data })
      .then(function(response) {
        // console.log(response)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          return dispatch({ type: HISTORYLIST_SUCCESS, payload: response.data.HistoryList })
        } else {
          return dispatch({ type: HISTORYLIST_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        firebase.crashlytics().log('history')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: HISTORYLIST_ERROR, payload: error.message })
      })
  }
}

export const historyListMore = value => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    var api = {
      token,
      language: I18n.currentLocale()
    }
    var data = { ...api, ...value }
    data.Version =  getVersionApp()
    axios
      .get(API_IP + "history", { params: data })
      .then(function(response) {
        // console.log(response)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          if (response.data.HistoryList.length < 50)
            return dispatch({ type: HISTORYLISTMORE_END, payload: response.data.HistoryList })
          return dispatch({ type: HISTORYLISTMORE_SUCCESS, payload: response.data.HistoryList })
        } else {
          return dispatch({ type: HISTORYLISTMORE_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        dispatch({ type: HISTORYLISTMORE_ERROR, payload: error.message })
      })
  }
}
