import axios from "axios"
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from "react-native-router-flux"
import { API_IP_GET } from "../Constants/Constant"
import I18n from "../../assets/languages/i18n"
import {
  NOTILIST_SUCCESS,
  NOTILIST_ERROR,
  NOTIDETAIL_SUCCESS,
  NOTIDETAIL_ERROR,
  NOTICONFIRM_ERROR,
  NOTICONFIRM_SUCCESS,
  DUPLICATE_LOGIN,
  NOTISET,
  NOTICLEAR
} from "./types"
import { getVersionApp } from './AuthAction'

export const notiList = () => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    var data = {
      token,
      language: I18n.currentLocale()
    }
    data.Version =  getVersionApp()

    axios
      .get(API_IP_GET + "alert", { params: data })
      .then(function(response) {
        // console.log(response)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          var count = 0
          response.data.AlertList.map(alert => {
            if (alert.Flag == null) count += 1
          })
          return dispatch({ type: NOTILIST_SUCCESS, payload: response.data.AlertList, count: count })
        } else {
          return dispatch({ type: NOTILIST_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        dispatch({ type: NOTILIST_ERROR, payload: error.message })
      })
  }
}

export const notiDetail = id => {
  return async dispatch => {
    dispatch(clearNoti())
    const token = await AsyncStorage.getItem("token")
    var data = {
      token,
      language: I18n.locale,
      alertid: id
    }
    data.Version =  getVersionApp()
    axios
      .get(API_IP_GET + "alert/alertdetail", { params: data })
      .then(function(response) {
        // console.warn(response)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          var process = 0
          process = response.data.AlertDetail.ProcessId
          // dispatch(notiList())
          return dispatch({ type: NOTIDETAIL_SUCCESS, payload: response.data.AlertDetail, process })
        } else {
          return dispatch({ type: NOTIDETAIL_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        // console.warn(error)
        dispatch({ type: NOTIDETAIL_ERROR, payload: error.message })
      })
  }
}

export const notiConfirm = (id, flag, lat, long) => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    var data = {
      token,
      language: I18n.currentLocale(),
      alertid: id,
      flag,
      LocationLat: lat,
      LocationLong: long
    }
    data.Version =  getVersionApp()
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      },
      timeout: 10000
    }

    param = []
    for (var property in data) {
      var encodedKey = encodeURIComponent(property)
      var encodedValue = encodeURIComponent(data[property])
      param.push(encodedKey + "=" + encodedValue)
    }
    param = param.join("&")

    axios
      .post(API_IP_GET + "alert/confirmalert", param, config)
      .then(function(response) {
        // console.warn(response)
        if (response.data.Message.Code == "102") {
          // dispatch(notiList())
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          return dispatch({ type: NOTICONFIRM_SUCCESS, payload: response.data.AlertDetail  })
        } else {
          return dispatch({ type: NOTICONFIRM_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        // console.warn(error)
        dispatch({ type: NOTICONFIRM_ERROR, payload: error.message })
      })
  }
}

export const setNoti = data => dispatch => {
  // if (data.redirect && Actions.currentScene != "_login") {
  //   Actions.notification({ fromnoti: true, alertid: data.alertid })
  // }
  dispatch({ type: NOTISET, payload: data })
}

export const clearNoti = () => dispatch => dispatch({ type: NOTICLEAR })
