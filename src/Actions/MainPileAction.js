import { Alert } from "react-native"
import AsyncStorage from '@react-native-community/async-storage';
import {
  MAIN_PILE_GET_STORAGE,
  MAIN_PILE_GET_STORAGE_ERROR,
  MAIN_PILE_GET_ALL_STORAGE,
  MAIN_PILE_GET_ALL_STORAGE_ERROR,
  MAIN_PILE_STACK,
  MAIN_PILE_CLEAR_STACK,
  MAIN_PILE_STEP_STATUS,
  MAIN_PILE_CLEAR_STATUS,
  STEP,

  STEP_STATUS,
  STEP_STATUS_UN,
  MAIN_PILE_CLEAR
} from "./types"
import axios from "axios"
import { Actions } from "react-native-router-flux"
import { API_IP_GET } from "../Constants/Constant"
import I18n from '../../assets/languages/i18n'
import { getVersionApp } from './AuthAction'
// export const storeProvider = data => dispatch => dispatch({type: ADD_PROVIDER, name: data.name, id: data.id})

// export const storeProvider = data => {
//   return dispatch => {
//     return dispatch({ type: ADD_PROVIDER ,name: data.name, id: data.id })
//   }
// }
export const mainRefresh = (type = 1) => {
  return dispatch => {
    Actions.refresh({
      action: "refresh",
      viewType: type
    })
  }
}
export const mainpileClear = () => {
  return dispatch => {
    // console.warn('mainpileClear')
    return dispatch({ type: MAIN_PILE_CLEAR })
  }
}
export const mainPileGetStorage = (pile_id, index) => {
  return async dispatch => {
    
    pile_id = "pileid_" + pile_id
    AsyncStorage.getItem("pileData").then(data => {
      const pile = JSON.parse(data) || null
      if (pile != null && pile[pile_id] != undefined) {
        if (pile[pile_id][index] != null) {
          if (index == "step_status") {
            // console.warn('mainPileGetStorage',pile[pile_id][index])
            return dispatch({ type: MAIN_PILE_STEP_STATUS, payload: pile[pile_id][index] })
          } else {
            return dispatch({ type: MAIN_PILE_GET_STORAGE, payload: pile[pile_id][index] })
          }
        } else {
          return dispatch({ type: MAIN_PILE_GET_STORAGE_ERROR })
        }
      } else {
        return dispatch({ type: MAIN_PILE_GET_STORAGE_ERROR })
      }
    })
  }
}

export const mainPileGetAllStorage = () => {
  return async dispatch => {
    AsyncStorage.getItem("pileData").then(data => {
      const pile = JSON.parse(data) || null
      if (pile != null) {
        newPile = {}
        for (var i in pile) {
          newPile["" + i.replace("pileid_", "") + ""] = pile[i]
        }
        return dispatch({ type: MAIN_PILE_GET_ALL_STORAGE, payload: newPile })
      } else {
        return dispatch({ type: MAIN_PILE_GET_ALL_STORAGE_ERROR })
      }
    })
  }
}

export const mainPileSetStorage = (pile_id, index, data) => {
  if(index == 'step_status'){
    // console.warn("Mainpileset storage", data)
  }
  
  // console.log(data)
  return async dispatch => {
    pile_id = "pileid_" + pile_id
    const pileDataJSON = (await AsyncStorage.getItem("pileData")) || JSON.stringify({})
    const pileData = JSON.parse(pileDataJSON)
    if (!pileData[pile_id]) pileData[pile_id] = {}
    if (pileData[pile_id][index]) {
      pileData[pile_id][index] = {
        ...pileData[pile_id][index],
        ...data
      }
    } else {
      pileData[pile_id][index] = { ...data }
    }

    await AsyncStorage.setItem("pileData", JSON.stringify(pileData))
    // console.log("mainPileSetStorage"+index,pileData[pile_id])
    if (pileData != null) {
      newPile = {}
      for (var i in pileData) {
        newPile["" + i.replace("pileid_", "") + ""] = pileData[i]
      }
      // console.warn("newpile ", pile_id,pileData[pile_id])
      return dispatch({ type: MAIN_PILE_GET_ALL_STORAGE, payload: newPile })
    } else {
      return dispatch({ type: MAIN_PILE_GET_ALL_STORAGE_ERROR })
    }
  }
}

export const mainPileSetStorageNoneRedux = (pile_id, data) => {
  return async dispatch => {
    pile_id = "pileid_" + pile_id
    const pileDataJSON = (await AsyncStorage.getItem("pileData")) || JSON.stringify({})
    const pileData = JSON.parse(pileDataJSON)
    if (!pileData[pile_id]) pileData[pile_id] = {}
    if (pileData[pile_id]) {
      pileData[pile_id] = {
        ...pileData[pile_id],
        ...data
      }
    } else {
      pileData[pile_id] = { ...data }
    }
    await AsyncStorage.setItem("pileData", JSON.stringify(pileData))
  }
}

export const setStack = data => {
  // console.warn('setstack',data)
  return dispatch => {
    return dispatch({ type: MAIN_PILE_STACK, payload: data })
  }
}

export const clearStack = () => {
  return dispatch => {
    return dispatch({ type: MAIN_PILE_CLEAR_STACK })
  }
}

export const clearStatus = () => {
  return dispatch => {
    return dispatch({ type: MAIN_PILE_CLEAR_STATUS })
  }
}

export const mainpileAction_checkStepStatus = option => {
  return async dispatch => {
    var status = []
    var stepStatus = null
    var statuscolor = 0
    var check = true
    var errorText = []
    if (option.pile_id != null) option.pile_id = "pileid_" + option.pile_id

    const pileData =
      (await AsyncStorage.getItem("pileData").then(data => {
        const pile = JSON.parse(data) || JSON.stringify([])
        if (pile != null && pile[option.pile_id] != undefined) {
          if (pile[option.pile_id] != null) {
            // if(option.index=="step_status"){
            //   return dispatch({type:MAIN_PILE_STEP_STATUS,payload:pile[option.pile_id][option.index]})
            // }else{
            return dispatch({ type: "", payload: pile[option.pile_id] })
            // }
          } else {
            return dispatch({ type: "", payload: null })
          }
        } else {
          return dispatch({ type: "", payload: null })
        }
      })) || JSON.stringify([])

    // console.log(pileData.payload);

    // if (pileData.payload != null) {
    if (option.process == 1) {
      var error1=[]
      if (option.process == 1 && option.step == 1) {
        if (
          pileData.payload["1_1"].data.ProviderId != null &&
          pileData.payload["1_1"].data.ProviderId != "" &&
          pileData.payload["1_1"].data.ProviderId != 0
        ) {
          status.push(0 + status.length)
        }
        if (
          pileData.payload["1_1"].data.ProviderId == null ||
          pileData.payload["1_1"].data.ProviderId == "" ||
          pileData.payload["1_1"].data.ProviderId == 0
        ) {
          check = false
        } else {
          check = true
        }
        if (pileData.payload["1_2"].data.SectionDetail.length > 0) {
          for (var i = 0; i < pileData.payload["1_2"].data.SectionDetail.length; i++) {
            if (option.shape == 1) {
              if(pileData.payload["1_2"].data.SectionDetail[i].image_concretespacer != undefined && pileData.payload["1_2"].data.SectionDetail[i].image_steelcage != undefined){
                if (
                  pileData.payload["1_2"].data.SectionDetail[i].checksteelcage == true &&
                    pileData.payload["1_2"].data.SectionDetail[i].checkconcretespacer == true &&
                  pileData.payload["1_2"].data.SectionDetail[i].image_concretespacer.length > 0 &&
                  pileData.payload["1_2"].data.SectionDetail[i].image_steelcage.length > 0 
                ) {
                  status.push(0 + status.length)
                }
              }else{
                // console.warn(pileData.payload["1_2"].data.SectionDetail[i])
                if (
                  pileData.payload["1_2"].data.SectionDetail[i].Checksteelcage == true &&
                    pileData.payload["1_2"].data.SectionDetail[i].CheckConcretespacer == true &&
                  pileData.payload["1_2"].data.SectionDetail[i].ImageConcretespacer.length > 0 &&
                  pileData.payload["1_2"].data.SectionDetail[i].ImageSteelcage.length > 0 
                ) {
                  status.push(0 + status.length)
                }

              }
              

            } else {
              if(pileData.payload["1_2"].data.SectionDetail[i].image_steelcage != undefined){
                if (
                  pileData.payload["1_2"].data.SectionDetail[i].checksteelcage == true &&
                  pileData.payload["1_2"].data.SectionDetail[i].image_steelcage.length > 0 
                ) {
                  status.push(0 + status.length)
                }
              }else{
                if (
                  pileData.payload["1_2"].data.SectionDetail[i].Checksteelcage == true &&
                  pileData.payload["1_2"].data.SectionDetail[i].ImageSteelcage.length > 0 
                ) {
                  status.push(0 + status.length)
                }
              }
             
            }
            
          }
        } 
      } else if (option.process == 1 && option.step == 2) {
        
        if (
          pileData.payload["1_1"].data.ProviderId != null &&
          pileData.payload["1_1"].data.ProviderId != "" &&
          pileData.payload["1_1"].data.ProviderId != 0
        ) {
          status.push(0 + status.length)
        }else{
          error1.push(I18n.t('mainpile.1_1.alert.select'))
        }
        if (pileData.payload["1_2"].data.SectionDetail.length > 0 && pileData.payload["1_2"].data.SectionDetail.length == pileData.payload["1_2"].masterInfo.Steelcage.SectionList.length) {
          for (var i = 0; i < pileData.payload["1_2"].data.SectionDetail.length; i++) {
            if (option.shape == 1) {
              if(pileData.payload["1_2"].data.SectionDetail[i].image_concretespacer != undefined && pileData.payload["1_2"].data.SectionDetail[i].image_steelcage != undefined){
                if(pileData.payload["1_2"].data.SectionDetail[i].checksteelcage == true){
                  if(pileData.payload["1_2"].data.SectionDetail[i].image_steelcage.length > 0){
                    if(pileData.payload["1_2"].data.SectionDetail[i].checkconcretespacer == true){
                      if(pileData.payload["1_2"].data.SectionDetail[i].image_concretespacer.length > 0){
                        status.push(0 + status.length)
                      }else{
                        error1.push(I18n.t('mainpile.1_2.errorconcrete_img'))
                      }
                    }else{
                      error1.push(I18n.t('mainpile.1_2.errorconcrete'))
                    }
                  }else{
                    error1.push(I18n.t('mainpile.1_2.error_img'))
                  }
                }else{
                  error1.push(I18n.t('mainpile.1_2.error_nextstep'))
                }
              }else{
                if(pileData.payload["1_2"].data.SectionDetail[i].Checksteelcage == true){
                  if(pileData.payload["1_2"].data.SectionDetail[i].ImageSteelcage.length > 0){
                    if(pileData.payload["1_2"].data.SectionDetail[i].CheckConcretespacer == true){
                      if(pileData.payload["1_2"].data.SectionDetail[i].ImageConcretespacer.length > 0){
                        status.push(0 + status.length)
                      }else{
                        error1.push(I18n.t('mainpile.1_2.errorconcrete_img'))
                      }
                    }else{
                      error1.push(I18n.t('mainpile.1_2.errorconcrete'))
                    }
                  }else{
                    error1.push(I18n.t('mainpile.1_2.error_img'))
                  }
                }else{
                  error1.push(I18n.t('mainpile.1_2.error_nextstep'))
                }
              }
              

            } else {
              if(pileData.payload["1_2"].data.SectionDetail[i].image_steelcage != undefined){
                if (
                  pileData.payload["1_2"].data.SectionDetail[i].checksteelcage == true
                  
                ) {
                  if(pileData.payload["1_2"].data.SectionDetail[i].image_steelcage.length > 0 ){
                    status.push(0 + status.length)
                  }else{
                    error1.push(I18n.t('mainpile.1_2.error_img'))
                  }
                }else{
                  error1.push(I18n.t('mainpile.1_2.error_nextstep'))
                }
              }else{
                if (
                  pileData.payload["1_2"].data.SectionDetail[i].Checksteelcage == true
                  
                ) {
                  if(pileData.payload["1_2"].data.SectionDetail[i].ImageSteelcage.length > 0 ){
                    status.push(0 + status.length)
                  }else{
                    error1.push(I18n.t('mainpile.1_2.error_img'))
                  }
                  
                }else{
                  error1.push(I18n.t('mainpile.1_2.error_nextstep'))
                }
              }
             
            }
            if (option.shape == 1) {
              if(pileData.payload["1_2"].data.SectionDetail[i].image_concretespacer != undefined && pileData.payload["1_2"].data.SectionDetail[i].image_steelcage != undefined){
                // console.warn('if', pileData.payload["1_2"].data.SectionDetail[i].checksteelcage,pileData.payload["1_2"].data.SectionDetail[i].checkconcretespacer,pileData.payload["1_2"].data.SectionDetail[i].image_concretespacer.length,pileData.payload["1_2"].data.SectionDetail[i].image_steelcage.length)
                if (
                  pileData.payload["1_2"].data.SectionDetail[i].checksteelcage == false ||
                  pileData.payload["1_2"].data.SectionDetail[i].checkconcretespacer == false ||
                  pileData.payload["1_2"].data.SectionDetail[i].image_concretespacer.length == 0 ||
                  pileData.payload["1_2"].data.SectionDetail[i].image_steelcage.length == 0 
                ) {
                  // console.warn('if2')
                  check = false
                }
              }else{
                // console.warn('else')
                if (
                  pileData.payload["1_2"].data.SectionDetail[i].checksteelcage == false ||
                  pileData.payload["1_2"].data.SectionDetail[i].checkconcretespacer == false ||
                  pileData.payload["1_2"].data.SectionDetail[i].ImageConcretespacer.length == 0 ||
                  pileData.payload["1_2"].data.SectionDetail[i].ImageSteelcage.length == 0 
                ) {
                  check = false
                }
              }
              
            } else {
              if(pileData.payload["1_2"].data.SectionDetail[i].image_steelcage != undefined){
                if (
                  pileData.payload["1_2"].data.SectionDetail[i].checksteelcage == false ||
                  pileData.payload["1_2"].data.SectionDetail[i].image_steelcage.length == 0 
                ) {
                  check = false
                }
              }else{
                if (
                  pileData.payload["1_2"].data.SectionDetail[i].checksteelcage == false ||
                  pileData.payload["1_2"].data.SectionDetail[i].ImageSteelcage.length == 0 
                ) {
                  check = false
                }
              }
              
            }
          }
        } else {
          check = false
          error1.push(I18n.t('mainpile.1_2.error_nextstep'))
        }
        if (pileData.payload["1_2"].masterInfo.Steelcage.SectionList.length == 0) {
          check = true
          statuscolor = 2
        }
        if(pileData.payload["1_2"].data.SectionDetail.length != pileData.payload["1_2"].masterInfo.Steelcage.SectionList.length){
          check = false
        }
      }
      // console.warn('status.length',status.length,check)
      if (status.length == 0) {
        
        statuscolor = 0
      } else if (status.length < pileData.payload["1_2"].masterInfo.Steelcage.SectionList.length + 1) {
     
        statuscolor = 1
      } else if (status.length == pileData.payload["1_2"].masterInfo.Steelcage.SectionList.length + 1) {
        // console.warn('SectionDetail.length',pileData.payload["1_2"].data.SectionDetail.length)
        if (pileData.payload["1_2"].data.SectionDetail.length > 0) {
        
          statuscolor = 2
        } else {
         
          statuscolor = 1
        }
        // if (pileData.payload['1_2'].data.SectionDetail.length == 0) {
        //   statuscolor = 1
        // }else {
        //   statuscolor = 2
        // }
      } else if (
        status.length >= pileData.payload["1_2"].masterInfo.Steelcage.SectionList.length + 1 &&
        check == true
      ) {
        statuscolor = 2
      }
      if (status.length > 0 && check == false) {
        statuscolor = 1
      }
      
      // console.log(pileData.payload['1_2'].data.SectionDetail.length);
      stepStatus = {
        statuscolor: statuscolor,
        check: check,
        error:error1[error1.length-1]
      }
      return stepStatus
    } else if (option.process == 2) {
      errorText=[]
      // console.log('pileData',pileData.payload);
      ////////////////////////////////////////////////////////////////////////////////////////////
      if (pileData.payload["2_1"].data.MachineCraneId != "" && pileData.payload["2_1"].data.MachineCraneId != null) {
        status.push(0 + status.length)
        if (pileData.payload["2_1"].data.MachineVibroId != "" && pileData.payload["2_1"].data.MachineVibroId != null) {
          status.push(0 + status.length)
        }
      }
      if (pileData.payload["2_1"].data.DriverId != "" && pileData.payload["2_1"].data.DriverId != null) {
        status.push(0 + status.length)
      }
      if (pileData.payload["2_1"].data.Length != "" && pileData.payload["2_1"].data.Length != null) {
        status.push(0 + status.length)
      }
      ////////////////////////////////////////////////////////////////////////////////////////////
      if (pileData.payload["2_2"].data.checkplummet == true) {
        status.push(0 + status.length)
      }
      if (pileData.payload["2_2"].data.checkwater == true) {
        status.push(0 + status.length)
      }
      ////////////////////////////////////////////////////////////////////////////////////////////
      if (option.process == 2 && option.step == 1) {
        // console.warn('pileData.payload["2_1"].data.Length',pileData.payload["2_1"].data.Length)
        // if(pileData.payload["2_1"].data.Length == null || pileData.payload["2_1"].data.Length == ''){
        //   check = false
        //   errorText.push(I18n.t('alert.errorlegth'))
        // }else{
          if (
            pileData.payload["2_1"].data.MachineCraneId == null ||
            pileData.payload["2_1"].data.MachineCraneId == ""
          ) {
            check = false
            errorText.push(I18n.t('alert.selectMachine'))
          } else {
            // console.warn(pileData.payload['2_1'].data.vibro_flag)
            if (pileData.payload["2_1"].data.vibro_flag == false) {
              if(pileData.payload["2_1"].data.DriverId == null ||
                pileData.payload["2_1"].data.DriverId == ""){
                check = false
                errorText.push(I18n.t('alert.selectdriver'))
              }else{
            check = true
            }
            } else {
              // console.log(pileData.payload['2_1'].data.MachineVibroId);
              if (
                pileData.payload["2_1"].data.MachineVibroId == null ||
                pileData.payload["2_1"].data.MachineVibroId == ""
              ) {
                check = false
                errorText.push(I18n.t('alert.selectvi'))
              } else {
                // check = true
                if(pileData.payload["2_1"].data.DriverId == null ||
                  pileData.payload["2_1"].data.DriverId == ""){
                  check = false
                 errorText.push(I18n.t('alert.selectdriver'))
                }else{
                check = true
                }
              }
            }
          }
        // }
        
      } else if (option.process == 2 && option.step == 2) {
        console.log('2_2data',pileData.payload["2_2"].data)
        if (pileData.payload["2_2"].data.checkplummet == true) {
          check = true
        } else {
          check = false
          errorText.push(I18n.t('alert.checkplummet'))
        }
      }
      if (pileData.payload["2_1"].data.MachineCraneId != "" && pileData.payload["2_1"].data.MachineCraneId != null) {
        if (pileData.payload["2_1"].data.vibro_flag == true) {
          // vibroDisable
          if (status.length == 0) {
            statuscolor = 0
          } else if (status.length < 5) {
            statuscolor = 1
            if (
              option.step == 2 &&
              pileData.payload["2_1"].data.DriverId != null &&
              pileData.payload["2_1"].data.DriverId != "" &&
              pileData.payload["2_1"].data.MachineVibroId != null &&
              pileData.payload["2_1"].data.MachineVibroId != "" &&
              pileData.payload["2_2"].data.checkplummet == true
            ) {
              statuscolor = 2
            }
            //   if (pileData.payload['2_1'].data.MachineVibroId == null || pileData.payload['2_1'].data.MachineVibroId == '') {
            //     check = false
            //   }else {
            //     if (status.length > 3) {
            //       check = true
            //     }else {
            //       check = false
            //     }
            //   }
            // }
          } else if (status.length >= 5) {
            if (check == false) {
              statuscolor = 1
            } else {
              if (
                pileData.payload["2_1"].data.MachineVibroId == null ||
                pileData.payload["2_1"].data.MachineVibroId == ""
              ) {
                if (option.step == 1) {
                  check = false
                  errorText.push(I18n.t('alert.selectvi'))
                  statuscolor = 1
                } else {
                  check = true
                  statuscolor = 2
                }
              } else {
                check = true
                statuscolor = 2
              }
            }
          }
        } else {
          if (status.length == 0) {
            statuscolor = 0
          } else if (status.length < 4) {
            statuscolor = 1
          } else if (status.length >= 4 && pileData.payload["2_2"].data.checkplummet == true) {
            statuscolor = 2
            if (
              (option.step == 2 && check == false) ||
              pileData.payload["2_1"].data.MachineVibroId == null ||
              pileData.payload["2_1"].data.MachineVibroId == ""
            ) {
              statuscolor = 1
              if (pileData.payload["2_1"].data.vibro_flag == false) {
                statuscolor = 2
              }
            }
          } else if (status.length >= 4 && pileData.payload["2_2"].data.checkplummet == false) {
            statuscolor = 1
          }
        }
      } else {
        if (status.length == 0) {
          statuscolor = 0
        } else {
          statuscolor = 1
        }
      }
      console.log(
        "status.length ",
        status.length + " " + statuscolor + " " + pileData.payload["2_1"].data.vibro_flag + " " + check
      )
      console.log(pileData.payload["2_1"])
      stepStatus = {
        statuscolor: statuscolor,
        check: check,
        errorText: errorText
      }
      return stepStatus
    } else if (option.process == 3) {
      errorText=[]
      // console.log(pileData.payload["step_status"])
      // if (pileData.payload["step_status"].Step2Status == 2) {
      //   statuscolor = 0
      //   check = true
      // } else {
      //   statuscolor = 0
      //   check = false
      // }

      if (pileData.payload["3_1"].data.SurveyorId != "" && pileData.payload["3_1"].data.SurveyorId != null) {
        status.push(0 + status.length)
      }
      if (pileData.payload["3_1"].data.LocationId != "" && pileData.payload["3_1"].data.LocationId != null) {
        status.push(0 + status.length)
      }
      if (
        pileData.payload["3_1"].data.BSFlagLocationId != "" &&
        pileData.payload["3_1"].data.BSFlagLocationId != null
      ) {
        status.push(0 + status.length)
      }
      //////////////////////////////////////////////////////////////////////////////////
      if (pileData.payload["3_2"].data.eastCalBy !== "" && pileData.payload["3_2"].data.eastCalBy !== null) {
        status.push(0 + status.length)
      }
      if (pileData.payload["3_2"].data.northCalBy !== "" && pileData.payload["3_2"].data.northCalBy !== null) {
        status.push(0 + status.length)
      }
      if (pileData.payload["3_2"].data.Image_ReadingCoordinate !== null) {
        if (pileData.payload["3_2"].data.Image_ReadingCoordinate.length > 0) {
          status.push(0 + status.length)
        }
      }
      if (pileData.payload["3_2"].data.Image_SurveyDiff !== null) {
        if (pileData.payload["3_2"].data.Image_SurveyDiff.length > 0) {
          status.push(0 + status.length)
        }
      }
      //////////////////////////////////////////////////////////////////////////////////
      if (pileData.payload["3_3"].data.ground !== "" && pileData.payload["3_3"].data.ground !== null) {
        status.push(0 + status.length)
      }
      if (pileData.payload["3_3"].data.top !== "" && pileData.payload["3_3"].data.top !== null) {
        status.push(0 + status.length)
      }
      //////////////////////////////////////////////////////////////////////////////////
      if (pileData.payload["3_4"].data.approveBy != "" && pileData.payload["3_4"].data.approveBy != null) {
        status.push(0 + status.length)
      }
      //////////////////////////////////////////////////////////////////////////////////
      if (pileData.payload["3_5"].data.SurveyorId != "" && pileData.payload["3_5"].data.SurveyorId != null) {
        status.push(0 + status.length)
      }
      if (pileData.payload["3_5"].data.ground !== "" && pileData.payload["3_5"].data.ground !== null) {
        status.push(0 + status.length)
      }
      if (pileData.payload["3_5"].data.top !== "" && pileData.payload["3_5"].data.top !== null) {
        status.push(0 + status.length)
      }
      //////////////////////////////////////////////////////////////////////////////////
      if (option.process == 3 && option.step == 1) {
        // console.log('data3_1',pileData.payload['3_1']);
        if (
          pileData.payload["3_1"].data.SurveyorId == "" ||
          pileData.payload["3_1"].data.SurveyorId == null
          
          
        ) {
          check = false
          errorText.push(I18n.t('alert.selectsurvey'))
        } else {
          if(pileData.payload["3_1"].data.LocationId == "" ||
          pileData.payload["3_1"].data.LocationId == null){
            check = false
            errorText.push(I18n.t('alert.selectstation'))
          }else{
            if(pileData.payload["3_1"].data.BSFlagLocationId == "" ||
            pileData.payload["3_1"].data.BSFlagLocationId == null){
              check = false
              errorText.push(I18n.t('alert.selactBS'))
            }else{
              check = true
            }
          }
        }
      } else if (option.process == 3 && option.step == 2) {
        if (
          pileData.payload["3_2"].data.northCalBy === "" ||
          pileData.payload["3_2"].data.northCalBy === null
         ) {
          check = false
              errorText.push(I18n.t('alert.putN'))
        } else {
          if( pileData.payload["3_2"].data.eastCalBy === "" ||
          pileData.payload["3_2"].data.eastCalBy === null){
            check = false
            errorText.push(I18n.t('alert.putE'))
          }else{
            if(pileData.payload["3_2"].data.Image_ReadingCoordinate.length == 0 ||
            pileData.payload["3_2"].data.Image_ReadingCoordinate === null){
              check = false
              errorText.push(I18n.t('alert.addreadimage'))
            }else{
              if( pileData.payload["3_2"].data.Image_SurveyDiff.length == 0 ||
              pileData.payload["3_2"].data.Image_SurveyDiff === null){
                check = false
                errorText.push(I18n.t('alert.addsurveydiff'))
              }else{
                check = true
              }
            }
          }
        }
      } else if (option.process == 3 && option.step == 3) {
        // console.warn('check',pileData.payload["3_3"].data.top)
        if (
          
          pileData.payload["3_3"].data.top === "" ||
          pileData.payload["3_3"].data.top === null
        ) {
          check = false
          errorText.push(I18n.t('alert.addtop'))
        } else {
          if(pileData.payload["3_3"].data.ground === "" ||
          pileData.payload["3_3"].data.ground === null){
            check = false
            errorText.push(I18n.t('alert.addground'))
          }else{
            check = true
          }
        }
      } else if (option.process == 3 && option.step == 4) {
        // console.log(pileData.payload['3_4'].data)
        if (pileData.payload["3_4"].data.approveBy == "" || pileData.payload["3_4"].data.approveBy == null) {
          if (
            pileData.payload["3_4"].data.approve == false &&
            pileData.payload["3_2"].data.Image_SurveyDiff !== null &&
            pileData.payload["3_2"].data.Image_SurveyDiff.length > 0 &&
            pileData.payload["3_2"].data.Image_ReadingCoordinate !== null &&
            pileData.payload["3_2"].data.Image_ReadingCoordinate.length > 0 &&
            pileData.payload["3_1"].data.SurveyorId != "" &&
            pileData.payload["3_1"].data.SurveyorId != null &&
            pileData.payload["3_1"].data.LocationId != '' &&
            pileData.payload["3_1"].data.LocationId != null &&
            pileData.payload["3_1"].data.BSFlagLocationId != '' &&
            pileData.payload["3_1"].data.BSFlagLocationId != null &&
            pileData.payload["3_3"].data.top != '' &&
            pileData.payload["3_3"].data.top != null &&
            pileData.payload["3_3"].data.ground != '' &&
            pileData.payload["3_3"].data.ground != null
          ) {
            check = true

          } else {
            check = false
            errorText.push(I18n.t('mainpile.3_4.errorsave'))
          }
        } else {
          check = true
        }
      }else if(option.process == 3 && option.step == 5){
        if (
          pileData.payload["3_5"].data.SurveyorId == "" ||
          pileData.payload["3_5"].data.SurveyorId == null
        ) {
          check = false
          errorText.push(I18n.t('alert.selectsurvey'))
        } else {
          // console.warn('check3_5',pileData.payload["3_5"].data.top,pileData.payload["3_5"].data.ground)
          if (
          
            pileData.payload["3_5"].data.top == "" ||
            pileData.payload["3_5"].data.top == null
          ) {
            check = false
            errorText.push(I18n.t('alert.addtop1'))
          } else {
            if(pileData.payload["3_5"].data.ground == "" ||
            pileData.payload["3_5"].data.ground == null){
              check = false
              errorText.push(I18n.t('alert.addground'))
            }else{
              check = true
            }
          }
        }
      }
      // console.warn('status.length3',status.length,pileData.payload["3_4"].data.approve)
      if(option.step == 5){
        if (status.length == 0) {
          statuscolor = 0
        } else if(status.length < 3){
          statuscolor = 1
        } else if(status.length >= 3 && check){
          statuscolor = 2
        }else{
          statuscolor = 1
        }
      }else{
        if (status.length == 0) {
          statuscolor = 0
        } else if (status.length < 13) {
          if (pileData.payload["step_status"].Step3Status != 2) {
            statuscolor = 1
          } else {
            if (check == true) {
              statuscolor = 2
            } else {
              statuscolor = 1
            }
          }
        } else if (status.length == 13) {
          statuscolor = 2
        }
      }
      
      
      stepStatus = {
        statuscolor: statuscolor,
        check: check,
        size: status.length,
        errorText: errorText
      }
      // console.warn('stepStatus',stepStatus)
      // console.log(stepStatus)
      return stepStatus
    } else if (option.process == 4) {
      // if (pileData.payload["4_1"].data.DrillingFluids.length > 0) {
        status.push(0 + status.length)
      // }

      // if (
      //   pileData.payload["4_1"].data.DrillingFluids != null &&
      //   pileData.payload["4_1"].data.DrillingFluids.length > 0
      // ) {
      //   check = true
      // } else {
      //   check = false
      // }
      // console.log(pileData.payload["4_1"])
      
      if (status.length == 0) {
        statuscolor = 0
      } else if (status.length > 0) {
        statuscolor = 1
        var datamaster=[]
        var nullcheck = 0
        pileData.payload['4_1'].masterInfo.ProcesstesterList.map(data=>{
          datamaster.push({id:data.Id, count:0,PositionFlag:data.PositionFlag,SlurryTypeFlag:data.SlurryTypeFlag})
        })
        for(var i = 0 ; i < option.drillingFluids.length; i++){
          for(var j = 0 ; j < datamaster.length;j++){
            if(datamaster[j].id == option.drillingFluids[i].TypeId){
              datamaster[j].count++
            }
            
            
          }
          if(option.drillingFluids[i].Ph == null || option.drillingFluids[i].Density == null || option.drillingFluids[i].Viscosity == null || option.drillingFluids[i].Sand == null){
            nullcheck++
          }
        }
        var count1=0
        var check4_success = false
        datamaster.map(data =>{
          if(data.count > 0){
            count1++
          }
        })
        if(count1 == datamaster.length && nullcheck == 0){
          check4_success = true
          // console.warn('check4',pileData.payload)
          if(option.shape == 1){
            if(pileData.payload.step_status.Step11Status == 2){
              statuscolor = 2
            }else{
              statuscolor = 1
            }
          }else{
            if(pileData.payload.step_status.Step10Status == 2){
              statuscolor = 2
            }else{
              statuscolor = 1
            }
          }
          
          
        }
      }
      
      
      stepStatus = {
        statuscolor: statuscolor,
        check: check,
        size: status.length,
        check4_success: check4_success
      }
      return stepStatus
    } else if (option.process == 5) {
      errorText=[]
      if (option.step == 0) {
       
        if (option.shape == 1 && option.category != 5) {
          if(option.category != 4){
            if (option.drillingFluids) {
              if (option.drillingFluids.length >= 1) {
                
                check = true
              }else {
                check = false
                errorText.push(I18n.t('alert.process4'))
              }
            }else {
              check = false
              errorText.push(I18n.t('alert.process4'))
            }
          }
          if(option.category == 5 && (option.pile_id != option.sp_parentid)){
            if (pileData.payload["step_status"].Step2Status == 2  && check == true) {
              statuscolor = 0
              check = true
            } else {
              statuscolor = 0
              check = false
              errorText.push(I18n.t('alert.process23'))
            }
          }else{
            if (pileData.payload["step_status"].Step2Status == 2 && pileData.payload["step_status"].Step3Status == 2 && check == true) {
              statuscolor = 0
              check = true
            } else {
              statuscolor = 0
              check = false
              errorText.push(I18n.t('alert.process23'))
            }
          }
        }else {
          if (option.drillingFluids) {
            if (option.drillingFluids.length >= 1) {
              check = true
            }else {
              check = false
              errorText.push(I18n.t('alert.process4'))
            }
          }else {
            check = false
            errorText.push(I18n.t('alert.process4'))
          }
          if(option.category == 5 && (option.pile_id != option.sp_parentid)){
            if (check == true) {
              statuscolor = 0
              check = true
            } else {
              statuscolor = 0
              check = false
              errorText.push(I18n.t('alert.process23'))
            }
          }else if(option.category == 5){
            if (pileData.payload["step_status"].Step2Status == 2 && check == true) {
              statuscolor = 0
              check = true
            } else {
              statuscolor = 0
              check = false
              errorText.push(I18n.t('alert.process23'))
            }
          }else{
            if (pileData.payload["step_status"].Step3Status == 2 && check == true) {
              statuscolor = 0
              check = true
            } else {
              statuscolor = 0
              check = false
              errorText.push(I18n.t('alert.process23'))
            }
          }
          
        }
      }
      ///////////////////////
      if (option.process == 5 && option.step != 0) {
        // if(option.category != 5){
          if (pileData.payload["5_1"].data.CheckPlummet == true) {
            status.push(0 + status.length)
          }
          if (pileData.payload["5_1"].data.CheckWaterLevel == true) {
            status.push(0 + status.length)
          }
        // }
        if (option.shape == 1) {
          if (pileData.payload["5_3"].data.DrillingList.length > 0) {
            status.push(0 + status.length)
          }
        }
        if (option.shape == 2) {
          if (pileData.payload["5_3"].data.DrillingList.length > 0) {
            status.push(0 + status.length)
          }
          if (pileData.payload["5_4"].data.CheckStopEnd == true) {
            status.push(0 + status.length)
          }
          if (pileData.payload["5_4"].data.CheckWaterStop == true) {
            status.push(0 + status.length)
          }
          if (pileData.payload["5_4"].data.CheckInstallStopEnd == true) {
            status.push(0 + status.length)
          }
          if (pileData.payload["5_4"].data.CheckDrillingFluid == true) {
            status.push(0 + status.length)
          }
          if (pileData.payload["5_4"].data.StartDrillingFluidDate) {
            status.push(0 + status.length)
          }
          if (pileData.payload["5_4"].data.EndDrillingFluidDate) {
            status.push(0 + status.length)
          }
        }
        if (option.process == 5 && option.step == 1) {
          // console.warn('check2',pileData.payload["5_1"].data.HangingBarsLength)
          if(pileData.payload["5_1"].data.OverLifting != ""){
            if(pileData.payload["5_1"].data.Pcoring != ""){
              // if(pileData.payload["5_1"].data.HangingBarsLength != ""){
                // if(pileData.payload["5_1"].data.ImageLengthSteelCarry.length > 0){
                  if(option.category != 5){
                    if (
                      pileData.payload["5_1"].data.CheckPlummet == true
                    ) {
                      if(pileData.payload["5_1"].data.CheckWaterLevel == true){
                        check = true
                      }else{
                        check = false
                        errorText.push(I18n.t('alert.checkwater'))
                      }
                    } else {
                      check = false
                      errorText.push(I18n.t('alert.checkplummet'))
                    }
                  }else{
                    check = true
                  }
                // }else{
                //   check = false
                //   errorText.push(I18n.t('alert.error71_image'))
                // }
              // }else{
              //   check = false
              //   errorText.push(I18n.t('alert.error71_hanging'))
              // }
              
            }else{
              check = false
              errorText.push(I18n.t('alert.error71'))
            }
          }else{
            check = false
            errorText.push(I18n.t('alert.length7'))
          }
          
        } else
        if (option.process == 5 && option.step == 1) {
          if(option.category != 5){
            if (
              pileData.payload["5_1"].data.CheckPlummet == true
            ) {
              if(pileData.payload["5_1"].data.CheckWaterLevel == true){
                check = true
              }else{
                check = false
                errorText.push(I18n.t('alert.checkwater'))
              }
            } else {
              check = false
              errorText.push(I18n.t('alert.checkplummet'))
            }
          }else{
            check = true
          }
        }
        
         if (option.process == 5 && option.step == 4) {
          if(pileData.payload["5_4"].data.CheckInstallStopEnd == true){
            if (
              pileData.payload["5_4"].data.StartStopEndDate != "" &&
              pileData.payload["5_4"].data.StartStopEndDate != null &&
              check == true
            ) {
              check = true
            } else {
              check = false
              errorText.push(I18n.t('alert.error5_4_start1'))
            }
            if (
              pileData.payload["5_4"].data.EndStopEndDate != "" &&
              pileData.payload["5_4"].data.EndStopEndDate != null &&
              check == true
            ) {
              check = true
            } else {
              check = false
              errorText.push(I18n.t('alert.error5_4_stop1'))
            }
          }
          // console.warn('CheckWaterStop',pileData.payload["5_4"].data.CheckWaterStop,pileData.payload["5_4"].data.waterstopid,pileData.payload["5_4"].data.WaterStopLength,check)
          if(pileData.payload["5_4"].data.CheckWaterStop == true && check == true){
            if( pileData.payload["5_4"].data.waterstopid != ''){
              if(pileData.payload["5_4"].data.WaterStopLength != ''){
                check = true
              }else{
                check = false
                errorText.push(I18n.t('alert.error5_4_waterstop2'))
              }
            }else{
              check = false
              errorText.push(I18n.t('alert.error5_4_waterstop1'))
            }

          }
          if (pileData.payload["5_4"].data.CheckDrillingFluid == true && check == true) {
            check = true
          } else {
            check = false
            errorText.push(I18n.t('alert.error5_4_4'))
          }
          if (
            pileData.payload["5_4"].data.StartDrillingFluidDate != "" &&
            pileData.payload["5_4"].data.StartDrillingFluidDate != null &&
            check == true
          ) {
            check = true
          } else {
            check = false
            errorText.push(I18n.t('alert.error5_4_start'))
          }
          if (
            pileData.payload["5_4"].data.EndDrillingFluidDate != "" &&
            pileData.payload["5_4"].data.EndDrillingFluidDate != null &&
            check == true
          ) {
            check = true
          } else {
            check = false
            errorText.push(I18n.t('alert.error5_4_stop'))
          }
        }
        /////////////////////////////////////
        var check_length = 10
        if(option.category == 5){
          check_length = 10
        }
        if (option.shape == 1) {
          if (status.length == 0) {
            statuscolor = 0
          } else if (status.length > 0) {
            statuscolor = 1
          }
        } else {
          if (status.length == 0) {
            statuscolor = 0
          } else if (status.length > 0 && check == false) {
            statuscolor = 1
          } else if (status.length >= check_length && check == true) {
            statuscolor = 2
          } else {
            statuscolor = 1
          }
        }

        if (option.step == 4) {
          if (pileData.payload["5_3"].data.DrillingList.length == 0) {
            status = []
          }
        }
      }

      stepStatus = {
        statuscolor: statuscolor,
        check: check,
        size: status.length,
        errorText: errorText
      }
      // console.log(stepStatus)
      return stepStatus
    } else if (option.process == 6) {
      errorText=[]
      if (
        pileData.payload["step_status"].Step5Status == 2 
        // && pileData.payload["step_status"].Step1Status == 2
        ) {
        statuscolor = 0
        check = true
      } else {
        statuscolor = 0
        check = false
      }
      if (option.step != 0) {
        // console.warn('WaitingTimeMinute',pileData.payload["6_2"].data.WaitingTimeMinute)
        if (pileData.payload["6_1"].data.BucketSize != "" && pileData.payload["6_1"].data.BucketSize != null) {
          status.push(0 + status.length)
        }
        if (pileData.payload["6_1"].data.Machine != "" && pileData.payload["6_1"].data.Machine != null) {
          status.push(0 + status.length)
        }
        if (pileData.payload["6_1"].data.DriverId != "" && pileData.payload["6_1"].data.DriverId != null) {
          status.push(0 + status.length)
        }
        if (pileData.payload["6_2"].data.AfterDepth !== "" && pileData.payload["6_2"].data.AfterDepth !== null) {
          status.push(0 + status.length)
        }
        if (pileData.payload["6_2"].data.BeforeDepth !== "" && pileData.payload["6_2"].data.BeforeDepth !== null) {
          status.push(0 + status.length)
        }
        if (
          pileData.payload["6_2"].data.WaitingTimeMinute !== "" &&
          pileData.payload["6_2"].data.WaitingTimeMinute !== null
        ) {
          status.push(0 + status.length)
        }
        if (pileData.payload["6_2"].data.BucketChanged == true) {
          status.push(0 + status.length)
        }

        if (option.process == 6 && option.step == 1) {
          if (pileData.payload["6_1"].data.BucketSize != "" && pileData.payload["6_1"].data.BucketSize != null) {
            check = true
          } else {
            check = false
            errorText.push(I18n.t('alert.selectbucket'))
          }
          if (pileData.payload["6_1"].data.Machine != "" && pileData.payload["6_1"].data.Machine != null) {
            check = true
          } else {
            check = false
            errorText.push(I18n.t('alert.selectMachine'))
          }
          if (pileData.payload["6_1"].data.DriverId != "" && pileData.payload["6_1"].data.DriverId != null) {
            check = true
          } else {
            check = false
            errorText.push(I18n.t('alert.selectdriver'))
          }
          // console.warn('check 61',check,pileData.payload["6_1"].data.Machine)
        }

        if (option.process == 6 && option.step == 2) {
          if (pileData.payload["6_2"].data.BucketChanged == true) {
            check = true
          } else {
            check = false
            errorText.push(I18n.t('alert.changebucket'))
          }
          // console.warn('check',pileData.payload["6_2"].data.WaitingTimeMinute !== "" &&
          // pileData.payload["6_2"].data.WaitingTimeMinute !== null &&
          // check == true)
          if (
            pileData.payload["6_2"].data.WaitingTimeMinute !== "" &&
            pileData.payload["6_2"].data.WaitingTimeMinute !== null &&
            check == true
          ) {
            check = true
          } else {
            check = false
            errorText.push(I18n.t('alert.timecross'))
          }

          var pile5_3 = pileData.payload['5_3'].data
          var pile5_5 = pileData.payload['5_5'].data
          var pileMaster5_2 = pileData.payload['5_2'].masterInfo
          var DepthLast = 0
          if(option.category == 5){
            if(pile5_5.DrillingList[pile5_5.DrillingList.length - 1] && check==true) {
              DepthLast = pile5_5.DrillingList[pile5_5.DrillingList.length - 1].Depth
              check = true
            }else {
              check = false
            }
          }else{
            if(pile5_3.DrillingList[pile5_3.DrillingList.length - 1] && check==true) {
              DepthLast = pile5_3.DrillingList[pile5_3.DrillingList.length - 1].Depth
              check = true
            }else {
              check = false
            }
          }
          
          var DepthNeed = 0
          var DepthNeed = pileData.payload['3_3'].data.top - pileData.payload['5_2'].masterInfo.PileInfo.PileTip
          // pileData.payload['5_2'].masterInfo.PileInfo.Depth
          if(pileData.payload["6_2"].data.BeforeDepth !== "" && pileData.payload["6_2"].data.BeforeDepth !== null  &&  check  == true) {
            check = true
            console.warn('BeforeDepth',pileData.payload["6_2"].data.BeforeDepth,DepthLast,parseFloat(pileData.payload["6_2"].data.BeforeDepth),parseFloat(DepthLast),pileData.payload["6_2"].data.BeforeDepth> DepthLast)
            // console.warn('DepthLast 6_2',parseFloat(pileData.payload["6_2"].data.BeforeDepth),DepthLast,pile5_5.DrillingList[pile5_5.DrillingList.length - 1].Depth , pileMaster5_2.PileInfo.PileTipChild)
            // if(parseFloat(pileData.payload["6_2"].data.BeforeDepth) < DepthLast) {

            //   check = false
            //   if(option.category == 5){
            //     errorText.push(I18n.t('alert.depthnotcol2_dw'))
            //   }else{
            //     errorText.push(I18n.t('alert.depthnotcol2'))
            //   }
             

            // }  
            
            if(parseFloat(pileData.payload["6_2"].data.BeforeDepth) > DepthLast) {
              check = false
              if(option.category == 5){
                errorText.push(I18n.t('alert.depthnotcol1_dw'))
              }else{
                errorText.push(I18n.t('alert.depthnotcol1'))
              }
              
            }
          } else {
            check = false
            if(option.category == 5){
              errorText.push(I18n.t('alert.adddepth_dw'))
            }else{
              errorText.push(I18n.t('alert.adddepth'))
            }
            
          }
          if(pileData.payload["6_2"].data.AfterDepth !== "" && pileData.payload["6_2"].data.AfterDepth !== null && check == true) {
            // console.warn('DepthLast 6_2',parseFloat(pileData.payload["6_2"].data.BeforeDepth),DepthLast)
            if(parseFloat(pileData.payload["6_2"].data.AfterDepth)< DepthLast){
              check = false
              if(option.category == 5){
                errorText.push(I18n.t('alert.depthnotcol_dw'))
              }else{
                errorText.push(I18n.t('alert.depthnotcol'))
              }
             
            }
            // if(DepthNeed>=DepthLast){
            //   if(parseFloat(pileData.payload["6_2"].data.AfterDepth) >= DepthNeed) {
            //     check = true
            //   } else {
            //     check = false
            //     errorText.push(I18n.t('alert.depthnotcol'))
            //   }
            // }else{
            //   if(parseFloat(pileData.payload["6_2"].data.AfterDepth) >= DepthLast) {
            //     check = true
            //   } else {
            //     check = false
            //     errorText.push(I18n.t('alert.depthnotcol'))
            //   }
            // }
            
          } else {
            check = false
            // errorText.push(I18n.t('alert.adddepth1'))
            if(option.category == 5){
              errorText.push(I18n.t('alert.adddepth1_dw'))
            }else{
              errorText.push(I18n.t('alert.adddepth1'))
            }
          }
          // console.warn('hhh',check)
          if (
            pileData.payload["6_1"].data.BucketSize != "" &&
            pileData.payload["6_1"].data.BucketSize != null &&
            check == true
          ) {
            check = true
          } else {
            check = false
            errorText.push(I18n.t('alert.selectbucketsize'))
          }
          if (
            pileData.payload["6_1"].data.Machine != "" &&
            pileData.payload["6_1"].data.Machine != null &&
            check == true
          ) {
            check = true
          } else {
            check = false
            errorText.push(I18n.t('alert.selectMachine'))
          }
          if (
            pileData.payload["6_1"].data.DriverId != "" &&
            pileData.payload["6_1"].data.DriverId != null &&
            check == true
          ) {
            check = true
          } else {
            check = false
            errorText.push(I18n.t('alert.selectdriver'))
          }
        }
        // console.log(status.length)
        // console.log(pileData.payload["6_2"])
        // console.log(pileData.payload["5_2"])

        if (status.length == 0) {
          statuscolor = 0
        } else if (status.length < 7) {
          statuscolor = 1
        } else {
          statuscolor = 2
        }
      }
      // console.log(pileData.payload['6_1'])

      stepStatus = {
        statuscolor: statuscolor,
        check: check,
        size: status.length,
        errorText: errorText
      }
      // console.warn(stepStatus)
      return stepStatus
    } else if (option.process == 7) {
      /**
       * Block step
       */
      
      if(option.step == 0) {
        if (pileData.payload["step_status"].Step1Status != 2) {
          check = false
        }
        if (option.shape == 1) {
          if(option.category == 4){
            if(pileData.payload["step_status"].Step5Status != 2){
              check = false
            }
          }else{
            if(pileData.payload["step_status"].Step6Status != 2){
              check = false
            }
          }
          
        }
        if(option.shape == 2 && pileData.payload["step_status"].Step5Status != 2) {
          check = false
        }
       return {
         stepStatus: 0,
         check: check
       }
      }


      var check1 = true
      var check2 = true
      var check3 = true
      var checkloop  = false
      let errorText=[]
      console.log("beforeCheck7",check1,check2,check3)
      // console.log("beforeCheckstep",option.step)
      var step = option.step
      // for(;step>0;step--){
      console.log("loopCheckstep",step,check1,check2,check3)
      /**
       * Step 1
       */
      // if(step==1){
      if (pileData.payload["7_1"].data.MachineId == null || pileData.payload["7_1"].data.MachineId == "") {
        check = false
        check1 = false
        // errorText.push(I18n.t('alert.selectMachine'))
        if(option.step!==1) {errorText.push(I18n.t('alert.selectMachine'))}
        else{ errorText.unshift(I18n.t('alert.selectMachine'))}
       
      } else {
        if (pileData.payload["7_1"].data.DriverId == null || pileData.payload["7_1"].data.DriverId == "") {
          check = false
          check1 = false
          // errorText.push(I18n.t('alert.selectdriver'))
          if(option.step!==1){ errorText.push(I18n.t('alert.selectdriver'))}
          else{errorText.unshift(I18n.t('alert.selectdriver'))}
        } 
        
      }
    // }
      /**
       * Step 2
       */ 
       console.log('pileData.payload["7_2"].data',option.category,pileData.payload["7_2"].data)
       
        if(option.category!==5){
          if(pileData.payload["7_2"].data.HangingBarsLength == null || pileData.payload["7_2"].data.HangingBarsLength == "" ){
            check = false
            check2 = false
            if(option.step!==2) {
              if(option.category!=1&&option.category!=4){
                errorText.push(I18n.t('alert.error71_hanging_dw'))
              }else{
                errorText.push(I18n.t('alert.error71_hanging'))
              }
            }else{
              if(option.category!==1&&option.category!==4){
                errorText.unshift(I18n.t('alert.error71_hanging_dw'))
              }else{
                errorText.unshift(I18n.t('alert.error71_hanging'))
              }
            }
          }
          if(pileData.payload["7_2"].data.ImageLengthSteelCarry==null||pileData.payload["7_2"].data.ImageLengthSteelCarry.length == 0 ){
            check = false
            check2 = false
            if(option.step!==2){
              if(option.category!=1&&option.category!=4){
                errorText.push(I18n.t('alert.error71_image_dw'))
              }else{
                errorText.push(I18n.t('alert.error71_image'))
              }
            }else{
              if(option.category!=1&&option.category!=4){
                errorText.unshift(I18n.t('alert.error71_image_dw'))
              }else{
                errorText.unshift(I18n.t('alert.error71_image'))
              }
              
            }
            
          }

          // if(pileData.payload["7_2"].data.Pcoring == null || pileData.payload["7_2"].data.Pcoring == ""){
          //   check = false
          //   check2 = false
          //   if(option.step!==2){ errorText.push(I18n.t('alert.error71'))}
          //   else {errorText.unshift(I18n.t('alert.error71'))}
          // }

          // if(pileData.payload["7_2"].data.OverLifting  == null || pileData.payload["7_2"].data.OverLifting == ""){
          
          //   check = false
          //   check2 = false
          //   if(option.step!==2) {errorText.push(I18n.t('alert.length7'))}
          //   else{ errorText.unshift(I18n.t('alert.length7'))}
          // }
        }else{
          console.log('option.pile_id===option.sp_parentid',option.pile_id,option.sp_parentid,pileData.payload["7_2"].data.ImageLengthSteelCarry==null||pileData.payload["7_2"].data.ImageLengthSteelCarry.length == 0)
          // if(option.pile_id===option.sp_parentid){
          if(option.parent==true){
            if(pileData.payload["7_2"].data.HangingBarsLength == null || pileData.payload["7_2"].data.HangingBarsLength == "" ){
              check = false
              check2 = false
              if(option.step!==2) {
                if(option.category!=1&&option.category!=4){
                  errorText.push(I18n.t('alert.error71_hanging_dw'))
                }else{
                  errorText.push(I18n.t('alert.error71_hanging'))
                }
              }else{
                if(option.category!==1&&option.category!==4){
                  errorText.unshift(I18n.t('alert.error71_hanging_dw'))
                }else{
                  errorText.unshift(I18n.t('alert.error71_hanging'))
                }
              }
            }
            if(pileData.payload["7_2"].data.ImageLengthSteelCarry==null||pileData.payload["7_2"].data.ImageLengthSteelCarry.length == 0 ){
              check = false
              check2 = false
              if(option.step!==2){
                if(option.category!=1&&option.category!=4){
                  errorText.push(I18n.t('alert.error71_image_dw'))
                }else{
                  errorText.push(I18n.t('alert.error71_image'))
                }
              }else{
                if(option.category!=1&&option.category!=4){
                  errorText.unshift(I18n.t('alert.error71_image_dw'))
                }else{
                  errorText.unshift(I18n.t('alert.error71_image'))
                }
                
              }
              
            }
    
            // if(pileData.payload["7_2"].data.Pcoring == null || pileData.payload["7_2"].data.Pcoring == ""){
            //   check = false
            //   check2 = false
            //   if(option.step!==2){ errorText.push(I18n.t('alert.error71'))}
            //   else {errorText.unshift(I18n.t('alert.error71'))}
            // }
    
            // if(pileData.payload["7_2"].data.OverLifting  == null || pileData.payload["7_2"].data.OverLifting == ""){
            
            //   check = false
            //   check2 = false
            //   if(option.step!==2) {errorText.push(I18n.t('alert.length7'))}
            //   else{ errorText.unshift(I18n.t('alert.length7'))}
            // }
          }
        }
      /**
       * Step 3
       */
      // else if (step==3){
        console.log('SectionDetail71',pileData.payload["7_3"].data.SectionDetail.length,)
      if (pileData.payload["7_3"].data.local) {
       
        if (pileData.payload["7_3"].data.SectionDetail.length > 0) {
          console.log('SectionDetail7',pileData.payload["7_3"].data.SectionDetail)
          pileData.payload["7_3"].data.SectionDetail.map((data, index) => {
            var Sectionlength = index
            console.log("Sectionlength local",Sectionlength,data.checksteelcage)
            // for(;Sectionlength>0;Sectionlength--){

            if (data.checksteelcage == false) {
              check = false
              check3 = false
              errorText.push(I18n.t('alert.error73'))
            }
            // if(data.startdate == null || data.startdate == '' || data.enddate == null || data.enddate == ''){
            //   check = false
            //   check3 = false
            // }
            if (data.image_steelcage && data.image_steelcage.length > 0) {
              // have picture
            } else {
              // Not checking picture
              // check = false
              // check3 = false
              // errorText.push(I18n.t('alert.error73img'))
            }
            if (option.shape == 2) {
              console.warn('data testt',data)
              if (data.checkconcretespacer == false) {
                check = false
                check3 = false
                errorText.push(I18n.t('alert.error73spacer'))
              }
              if (data.image_concretespacer && data.image_concretespacer.length > 0) {
                // have picture
              } else {
                // Not checking picture
                check = false
                check3 = false
                errorText.push(I18n.t('alert.error73spacerimg'))
              }
            }
          }
        // }
        )
        } 
        else {
          check = false
          check3 = false
          errorText.push(I18n.t('alert.error73'))
        }
      } else {
        if (pileData.payload["7_3"].data.SectionDetail && pileData.payload["7_3"].data.SectionDetail.length > 0) {
          pileData.payload["7_3"].data.SectionDetail.map((data, index) => {
            var Sectionlength = index
            // console.log("detail check data+index",index)
            // for(;Sectionlength>0;Sectionlength--){
              console.log("Sectionlength",Sectionlength,data.checksteelcage)
              // pileData.payload["7_3"].data.SectionDetail[i].checksteelcage == true 
            if (data.checksteelcage == false) {
              check = false
              check3 = false
              errorText.push(I18n.t('alert.error73'))
            }
            // console.log('data7-3',data)
            // if(data.startdate == null || data.startdate == '' || data.enddate == null || data.enddate == ''){
            //   check = false
            //   check3 = false
            // }
            if (data.image_steelcage && data.image_steelcage.length > 0) {
              // have picture
            } else {
              // check = false
              // check3 = false
            }
            if (option.shape == 2) {
              console.warn('data testt',data)
              if (data.checkconcretespacer == false) {
                check = false
                check3 = false
                errorText.push(I18n.t('alert.error73spacer'))
              }
              // if(data.startdate == null || data.startdate == '' || data.enddate == null || data.enddate == ''){
              //   check = false
              //   check3 = false
              // }
              if (data.image_concretespacer && data.image_concretespacer.length > 0) {
                // have picture
              } else {
                check = false
                check3 = false
                errorText.push(I18n.t('alert.error73spacerimg'))
              }
            }
          // }
        })
        } 
        else {
          check = false
          check3 = false
          errorText.push(I18n.t('alert.error73'))
        }
      }
    // }
  // }
      
      if (pileData.payload["step_status"].Step5Status == 2) {
        statuscolor = 0
        check = true
      } else {
        statuscolor = 0
        check = false
      }
      console.log("step7 check1",check1)
      console.log("step7 check2",check2)
      console.log("step7 check3",check3)

      if(check1==true&&check2==true&&check3==true){
        
            check = true
          
      }else{
        check = false
      }
    //   if (pileData.payload["step_status"].Step7Status == 2) {
    //     check = true
    //  }
    // console.warn('errorText',errorText,"check : "+check)

      switch (option.step) {
        case 1:
          return {
            statuscolor: check ? 2 : 1,
            check: check1,
            errorText:errorText
          }
        case 2:
          return {
            statuscolor: check ? 2 : 1,
            check: check2,
            errorText:errorText
          }
        case 3:
          return {
            statuscolor: check ? 2 : 1,
            check: check3,
            errorText:errorText
          }
        default:
          return {
            statuscolor: check ? 2 : 1,
            check: check,
            errorText:errorText
          }
      }

    } else if (option.process == 8) {
       /**
       * Block step
       */
      if(option.step == 0) {
        if (pileData.payload["step_status"].Step7Status != 2) {
          check = false
       }
       return {
         stepStatus: 0,
         check: check
       }
      }
      var check1 = true
      var check2 = true
      var check3 = true
      errorText=[]
      /**
       * Step 1
       */
      if (pileData.payload["8_1"].data.MachineId == null || pileData.payload["8_1"].data.MachineId == "") {
        check = false
        check1 = false
        errorText.push(I18n.t('alert.selectMachine'))
        // console.warn('machine false')
      }
      if (pileData.payload["8_1"].data.DriverId == null || pileData.payload["8_1"].data.DriverId == "") {
        check = false
        check1 = false
        errorText.push(I18n.t('alert.selectdriver'))
        // console.warn('driver false')
      }
      
      if(option.shape == 2){
        // console.warn("option.shape")
        // if(option.category!=3&&option.category!=5){
        if(option.category!=5){
          // console.warn("option.shape",  option.category)
          if (pileData.payload["8_1"].data.TremieTypeId == null || pileData.payload["8_1"].data.TremieTypeId == "") {
            check = false
            check1 = false
            errorText.push(I18n.t('alert.selecttret'))
            // console.warn("option.shape",option.shape)
          }
        }
       
    }
      /**
       * Step 2
       */
      // if(pileData.payload["8_2"].data.Last == null) {
      //   check = false
      //   check2 = false
      // }
      // else {
      //   if (!pileData.payload["8_2"].data.Last) {
      //     check = false
      //     check2 = false
      //   }
      // }
      if (option.haslast != null && option.haslast == false) {
        check = false
        check2 = false
        errorText.push(I18n.t('alert.error8_1'))
        // console.warn('tremie false')
      }
      /**
       * Step 3
       */
      // console.warn(pileData.payload["8_3"].data)
      if (
        option.category!= 4 &&
        (
          pileData.payload["8_3"].data.checkfoam == null ||
          pileData.payload["8_3"].data.checkfoam == "" ||
          pileData.payload["8_3"].data.checkfoam == false
        )
      ) {
        check = false
        check3 = false
        errorText.push(I18n.t('alert.error8_2'))
        // console.warn('checkfoam false')
      }else{
        if(option.category== 4 &&(check1==false||check2==false)){
          check = false
          check3 = false
        }
      }
      if (pileData.payload["step_status"].Step8Status == 2) {
        check = true
     }
      // console.warn('check 8', !check && !check1 && !check2 && !check3,check3)
      if(!check && !check1 && !check2 && !check3){
        switch (option.step) {
          case 1:
            return {
              statuscolor: 0,
              check: check1,
              errorText:errorText
            }
          case 2:
            return {
              statuscolor: 0,
              check: check2,
              errorText:errorText
            }
          case 3:
            return {
              statuscolor: 0,
              check: check3,
              errorText:errorText
            }
          default:
            return {
              statuscolor: 0,
              check: check,
              errorText:errorText
            }
        }
      }else{
        switch (option.step) {
          case 1:
            return {
              statuscolor: check ? 2 : 1,
              check: check1,
              errorText:errorText
            }
          case 2:
            return {
              statuscolor: check ? 2 : 1,
              check: check2,
              errorText:errorText
            }
          case 3:
            return {
              statuscolor: check ? 2 : 1,
              check: check3,
              errorText:errorText
            }
          default:
            return {
              statuscolor: check ? 2 : 1,
              check: check,
              errorText:errorText
            }
        }
      }
      
    } else if (option.process == 10) {
       /**
       * Block step
       */
      
      if(option.step == 0) {
        if (pileData.payload["step_status"].Step8Status != 2) {
          check = false
       }
       return {
         stepStatus: 0,
         check: check
       }
      }
      /*************************************
       * Start Process 10
       */
      var check1 = true
      var check2 = true
      var check3 = true
      errorText=[]
      /**
       * Step 1
       */
      if (
        pileData.payload["10_1"].data.ForemanSet == null ||
        pileData.payload["10_1"].data.ForemanSet.Id == null ||
        pileData.payload["10_1"].data.ForemanSet.Id == ""
      ) {
        check = false
        check1 = false
        check3 = false
        errorText.push(I18n.t('alert.selectforemans'))
      }
      if (pileData.payload["10_1"].data.ForemanId == null || pileData.payload["10_1"].data.ForemanId == "") {
        check = false
        check1 = false
        check3 = false
        errorText.push(I18n.t('alert.selectforeman'))
      }
      if (pileData.payload["10_1"].data.Overcast === null || pileData.payload["10_1"].data.Overcast === "") {
        check = false
        check1 = false
        check3 = false
        errorText.push(I18n.t('alert.errorovercast1'))

        // if(option.category==4||option.category==1){

        //   errorText.push(I18n.t('alert.errorovercast2'))
        // }else{
        //   errorText.push(I18n.t('alert.errorovercast1'))
        // }
      }

      /**
       * Step 2
       */

      if ((pileData.payload["10_2"].data.Weather == null || pileData.payload["10_2"].data.Weather == "")&&pileData.payload["10_2"].data.IsFine==false&&pileData.payload["10_2"].data.IsCloudy==false&&pileData.payload["10_2"].data.IsRainy==false&&pileData.payload["10_2"].data.IsHail==false&&pileData.payload["10_2"].data.IsWindy==false) {
        check = false
        check2 = false
        check3 = false
        errorText.push(I18n.t('alert.selectwather'))
      }
      // Validate color on step3 via API instead of this
      check = false
      if (pileData.payload["step_status"].Step10Status == 2) {
        check = true
     }

    //  console.warn("check: 10",check,check1,check2,check3)
      switch (option.step) {
        case 1:
          return {
            statuscolor: check ? 2 : 1,
            check: check1,
            errorText:errorText
          }
        case 2:
          return {
            statuscolor: check ? 2 : 1,
            check: check2,
            errorText:errorText
          }
        case 3:
          return {
            statuscolor: check ? 2 : 1,
            check: check3,
            errorText:errorText
          }
        default:
          return {
            statuscolor: check ? 2 : 1,
            check: check,
            errorText:errorText
          }
      }
    } else if (option.process == 11) {
       /**
       * Block step
       */
      if(option.step == 0) {
        if (pileData.payload["step_status"].Step10Status != 2) {
          check = false
       }
       return {
         stepStatus: 0,
         check: check
       }
      }
      /********
       * Start step 11
       */
      var check1 = true
      var check2 = true
      var check3 = true
      errorText =[]

      /**
       * Step 1
       */

      
      if (
        pileData.payload["11_1"].data.Machine == null ||
        pileData.payload["11_1"].data.Machine.itemid == null ||
        pileData.payload["11_1"].data.Machine.itemid == ""
      ) {
        check = false
        check1 = false
        errorText.push(I18n.t('alert.selectMachine'))
      }
      if (pileData.payload["11_1"].data.Machine && pileData.payload["11_1"].data.Machine.vibro_flag) {
        if (
          pileData.payload["11_1"].data.Vibro == null ||
          pileData.payload["11_1"].data.Vibro.itemid == null ||
          pileData.payload["11_1"].data.Vibro.itemid == ""
        ) {
          check = false
          check1 = false
          errorText.push(I18n.t('alert.selectviname'))
        }
      }
      if (pileData.payload["11_1"].data.DriverId == null || pileData.payload["11_1"].data.DriverId == "") {
        check = false
        check1 = false
        errorText.push(I18n.t('alert.selectdriver'))
      }

      /**
       * Step 2
       */
      // if (pileData.payload["11_2"].data.CheckPlummet == null || pileData.payload["11_2"].data.CheckWaterLevel == null) {
      //   check = false
      //   check2 = false
      // }else {
      //   if(pileData.payload["11_2"].data.CheckPlummet == false &&
      //   pileData.payload["11_2"].data.CheckWaterLevel == false) {
      //     check = false
      //     check2 = false
      //  }
      // }
      if (!pileData.payload["11_2"].data.CheckPlummet) {
        check = false
        check2 = false
        errorText.push(I18n.t('alert.checkplummet'))
      }

      /**
       * Step 3
       */
      console.log(pileData.payload["11_3"].data)
      if (
        pileData.payload["11_3"].data.ConcreteLevelBeforeWithdraw === null ||
        pileData.payload["11_3"].data.ConcreteLevelBeforeWithdraw === ""
      ) {
        check = false
        check3 = false
        errorText.push(I18n.t('alert.checkdepth'))
      }

      // if (
      //   pileData.payload["11_3"].data.ConcreteLevelAfterWithdraw == null ||
      //   pileData.payload["11_3"].data.ConcreteLevelAfterWithdraw == ""
      // ) {
      //   check = false
      //   check3 = false
      // }

      // if(pileData.payload["11_3"].data.ConcreteBleeding == null || pileData.payload["11_3"].data.ConcreteBleeding == "" ) {
      //   check = false
      //   check3 = false
      // }

      switch (option.step) {
        case 1:
          return {
            statuscolor: check ? 2 : 1,
            check: check1,
            errorText:errorText
          }
        case 2:
          return {
            statuscolor: check ? 2 : 1,
            check: check2,
            errorText:errorText
          }
        case 3:
        console.log("check",check3)
          return {
            statuscolor: check ? 2 : 1,
            check: check3,
            errorText:errorText
          }
        default:
          return {
            statuscolor: check ? 2 : 1,
            check: check,
            errorText:errorText
          }
      }
    }

    // else if (option.process == 8 && option.step == 1) {
    //   if (pileData.payload["8_1"].data.MachineId == null || pileData.payload["8_1"].data.MachineId == "") {
    //     check = false
    //   }
    //   if (pileData.payload["8_1"].data.DriverId == null || pileData.payload["8_1"].data.DriverId == "") {
    //     check = false
    //   }
    //   return {
    //     statuscolor: 1,
    //     check: check
    //   }
    // }
    // else if (option.process == 8 && option.step == 2) {
    //   check = false
    //   if (pileData.payload["8_2"].data.TremieList == null) {
    //     check = false
    //   }
    //   else {
    //     pileData.payload["8_2"].data.TremieList.map( (data, index) => {
    //       if (data.last || data.IsLast) {
    //         check = true
    //       }
    //     })
    //   }
    //   return {
    //     statuscolor: 1,
    //     check: check
    //   }
    // }
    // else if (option.process == 8 && option.step == 3) {
    //   if (pileData.payload["8_3"].data.checkfoam == null || pileData.payload["8_3"].data.checkfoam == "" || pileData.payload["8_3"].data.checkfoam == false) {
    //     check = false
    //   }
    //   return {
    //     statuscolor: 2,
    //     check: check
    //   }
    // }
    //   stepStatus = {
    //     statuscolor:0,
    //     check:false,
    //   }
    //   return stepStatus
    // }
  }
}

/**
 * Get step status from API and set it.
 * @param {Object} data
 * @param {String} data.pileid - Pile Id to set status
 * @param {String} data.jobid - Job Id to set status
 */
export const getStepStatus = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.Version =  getVersionApp()
      
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }

    axios
      .get(API_IP_GET + "pilestatus/getstepstatus", { params: data })
      .then(response => {
        // console.warn("Step status",response.data)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          let status = response.data.Step
          return dispatch(mainPileSetGetStorage(data.pileid, "step_status", status))
        }
      })
      .catch(function(error) {
        console.log(error)
      })
  }
}

export const getStepStatus2 = data => {
  // console.warn('getStepStatus2',data)
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.Version =  getVersionApp()
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }

    axios
      .get(API_IP_GET + "pilestatus/getstepstatus", { params: data })
      .then(response => {
        // console.warn("Step status2",response.data.Step)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          return dispatch({ type: STEP_STATUS, payload:response.data.Step, process:data.process})
        }
      })
      .catch(function(error) {
        console.log(error)
      })
  }
}
export const ungetStepStatus2 = data => {
  return async dispatch => {
    // console.warn('ungetStepStatus2')

   

    // axios
    //   .get(API_IP_GET + "pilestatus/getstepstatus", { params: data })
    //   .then(response => {
    //     console.warn("Step status2",response.data.Step)
    //     if (response.data.Message.Code == "102") {
    //       return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        // } else if (response.data.Message.Code == "001") {
          return dispatch({ type: STEP_STATUS_UN})
      //   }
      // })
      // .catch(function(error) {
      //   console.log(error)
      // })
  }
}

export const mainPileSetGetStorage = (pile_id_param, index, data) => {
  console.log("Mainpileset storage")
  // console.log(data)
  return async dispatch => {
    pile_id = "pileid_" + pile_id_param
    const pileDataJSON = (await AsyncStorage.getItem("pileData")) || JSON.stringify({})
    const pileData = JSON.parse(pileDataJSON)
    if (!pileData[pile_id]) pileData[pile_id] = {}
    if (pileData[pile_id][index]) {
      pileData[pile_id][index] = {
        ...pileData[pile_id][index],
        ...data
      }
    } else {
      pileData[pile_id][index] = { ...data }
    }

    await AsyncStorage.setItem("pileData", JSON.stringify(pileData))
    // console.log("mainPileSetStorage"+index,pileData[pile_id])
    if (pileData != null) {
      newPile = {}
      for (var i in pileData) {
        newPile["" + i.replace("pileid_", "") + ""] = pileData[i]
      }
      return dispatch(mainPileGetStorage(pile_id_param, "step_status"))
    } else {
      return dispatch({ type: MAIN_PILE_GET_ALL_STORAGE_ERROR })
    }
  }
}

export const setstep = (data) => {
  return async dispatch => {
    // const step = {PileId:pile_id,Step:data}
    
    return dispatch({ type: STEP, payload: data})
  }}