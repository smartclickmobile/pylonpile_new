import axios from "axios"
import AsyncStorage from '@react-native-community/async-storage';
import { DUPLICATE_LOGIN, NOTE_SAVE, NOTE_ERROR, NOTE } from "./types"
import { API_IP_GET, API_WEATHER } from "../Constants/Constant"
import firebase from "react-native-firebase"
import { getVersionApp } from './AuthAction'

export const getNote = data => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.Version =  getVersionApp()
    // console.log("dedddd",data)

    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }

    axios
      .get(API_IP_GET + "notation/getnotation", { params: data })
      .then(function(response) {
        // console.warn("dedddd",response.data.Notation.Detail)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          return dispatch({ type: NOTE, payload: response.data.Notation })
        } else {
          return dispatch({ type: NOTE_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        firebase.crashlytics().log('notation/getnotation')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: NOTE_ERROR, payload: error.message })
      })
  }
}

export const saveNote = option => {
  return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    option.token = token

    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    }
    const data = new FormData()
    data.append("Token", token)
    data.append("Language", option.language)
    data.append("JobId", option.jobid)
    data.append("PileId", option.pileid)
    data.append("ProcessId", option.processid)
    data.append("Detail", option.detail)
    data.append("LocationLat", option.lat)
    data.append("LocationLong", option.log)
    data.append("Version", getVersionApp())

    // console.warn("upddddd",data)
    // axios
    // .get(
    //   API_WEATHER+"?lat=" +
    //   option.lat +
    //   "&lon=" +
    //   option.log +
    //   "&APPID=67fb763683db01d90f672ce07e78bd8a"
    // )
    // .then( (response) => {
      
      if(option.noteimage != ""&&option.noteimage != []&&option.noteimage != null) {
        var countLocal = 0
        var countServer = 0
        for(var i = 0; i < option.noteimage.length; i++) {
          if(option.noteimage[i].id) {
            var photo = option.noteimage[i].id
            data.append("ImageIds[" + countServer + "]", photo)
            countServer++
          } else {
            var photo = {
              uri: option.noteimage[i].image,
              type: "image/jpeg",
              name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
            }
            data.append("ImageFiles[" + countLocal + "]", photo)
            var type = option.noteimage[i].type == "camera" ? 1 : 2
            data.append("ImageDates[" + countLocal + "]", option.noteimage[i].timestamp)
            data.append("ImageTypes[" + countLocal + "]", type)
            countLocal++
          }
        }
      }
      // var celcius = parseFloat(parseFloat(response.data.main.temp) - 273.15).toFixed(0) + " °C"
      // var location = response.data.name
      // var weathertype = "http://openweathermap.org/img/w/" + response.data.weather[0].icon + ".png"

      // data.append("WeatherType", weathertype)
      // data.append("Temperature", celcius)
      // data.append("Location", location)
      // console.warn("notation",location)
    axios
      .post(API_IP_GET + "notation/addnotation", data, config)
      .then(function(response) {
        // console.warn(response.data)
        if (response.data.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Code == "001") {
          return dispatch({ type: NOTE_SAVE })
        } else {
          return dispatch({ type: NOTE_ERROR, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        firebase.crashlytics().log('notation/addnotation')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: NOTE_ERROR, payload: error.message })
      })
    // })
    // .catch(function (error) {
    //   // console.warn(error)
    //   if(option.noteimage != ""&&option.noteimage != []&&option.noteimage != null) {
    //     var countLocal = 0
    //     var countServer = 0
    //     for(var i = 0; i < option.noteimage.length; i++) {
    //       if(option.noteimage[i].id) {
    //         var photo = option.noteimage[i].id
    //         data.append("ImageIds[" + countServer + "]", photo)
    //         countServer++
    //       } else {
    //         var photo = {
    //           uri: option.noteimage[i].image,
    //           type: "image/jpeg",
    //           name: Math.floor(Math.random() * 999 + 100).toString() + ".jpg"
    //         }
    //         data.append("ImageFiles[" + countLocal + "]", photo)
    //         var type = option.noteimage[i].type == "camera" ? 1 : 2
    //         data.append("ImageDates[" + countLocal + "]", option.noteimage[i].timestamp)
    //         data.append("ImageTypes[" + countLocal + "]", type)
    //         countLocal++
    //       }
    //     }
    //   }
    //   data.append("WeatherType", '')
    //   data.append("Temperature", '')
    //   data.append("Location", '')
      
    //   axios
    //   .post(API_IP_GET + "notation/addnotation", data, config)
    //   .then(function(response) {
    //     // console.warn(response.data)
    //     if (response.data.Code == "102") {
    //       return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
    //     } else if (response.data.Code == "001") {
    //       return dispatch({ type: NOTE_SAVE })
    //     } else {
    //       return dispatch({ type: NOTE_ERROR, payload: response.data.Message.Message })
    //     }
    //   })
    //   .catch(function(error) {
    //     console.log(error)
    //     firebase.crashlytics().log('notation/addnotation')
    //     firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
    //     dispatch({ type: NOTE_ERROR, payload: error.message })
    //   })
    // })
  }
}