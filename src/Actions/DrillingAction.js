import axios from "axios"
import AsyncStorage from '@react-native-community/async-storage';
import { DUPLICATE_LOGIN, Drilling, DrillingError, DrillingChild, DrillingChildError} from "./types"
import { API_IP_GET } from "../Constants/Constant"
import firebase from "react-native-firebase"
import { getVersionApp } from './AuthAction'

export const getDrilling = data => {
    return async dispatch => {
    const token = await AsyncStorage.getItem("token")
    data.token = token
    data.Version =  getVersionApp()
    const config = {
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }
    }

    axios
      .get(API_IP_GET + "step05/getdrilling", { params: data })
      .then(function(response) {
        console.log('getdrilling response',response)
        if (response.data.Message.Code == "102") {
          return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
        } else if (response.data.Message.Code == "001") {
          return dispatch({ type: Drilling, payload: response.data.DrillingList })
        } else {
          return dispatch({ type: DrillingError, payload: response.data.Message.Message })
        }
      })
      .catch(function(error) {
        console.log(error)
        firebase.crashlytics().log('step05/getdrilling')
        firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
        dispatch({ type: DrillingError, payload: error.message })
      })
    }
}

export const getDrillingChild = data => {
  return async dispatch => {
  const token = await AsyncStorage.getItem("token")
  data.token = token
  data.Version =  getVersionApp()
  const config = {
    headers: {
      "content-type": "application/x-www-form-urlencoded"
    }
  }
  // console.warn('getDrillingChild',data)
  axios
    .get(API_IP_GET + "step05/GetDrillingChild", { params: data })
    .then(function(response) {
      console.log('getDrillingChild response',response.data.DrillingList)
      if (response.data.Message.Code == "102") {
        return dispatch({ type: DUPLICATE_LOGIN, payload: response.data.Message.Message })
      } else if (response.data.Message.Code == "001") {
        return dispatch({ type: DrillingChild, payload: response.data.DrillingList })
      } else {
        return dispatch({ type: DrillingChildError, payload: response.data.Message.Message })
      }
    })
    .catch(function(error) {
      console.log(error)
      firebase.crashlytics().log('step05/GetDrillingChild')
      firebase.crashlytics().recordError(error.response ? error.response.status : 0, error.message)
      dispatch({ type: DrillingChildError, payload: error.message })
    })
  }
}