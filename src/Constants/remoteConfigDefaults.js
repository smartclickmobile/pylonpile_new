import I18n from '../../assets/languages/i18n'

const remoteConfigDefaults = {
  'joblist_checkbox_th': I18n.t('joblist.myjobonly'),
  'joblist_mywork_th': I18n.t('joblist.myjob'),
  'joblist_otherwork_th': I18n.t('joblist.otherjob'),
}  
export default remoteConfigDefaults