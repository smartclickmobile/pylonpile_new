export const MAIN_COLOR = '#007CC2'
export const SUB_COLOR = '#029CD1'
export const BASIC_COLOR = '#000000'
export const MENU_BACKGROUND = '#eaeaea'
export const MENU_GREY = '#d1d1d1'
export const MENU_GREY_ITEM = '#b7b7b7'
export const MENU_GREY_BORDER = '#969696'
export const MENU_ORANGE = '#f4964a'
export const MENU_ORANGE_ITEM = '#f4964a'
export const MENU_ORANGE_BORDER = '#93430d'
export const MENU_GREEN = '#6ecc64'
export const MENU_GREEN_ITEM = '#6ecc64'
export const MENU_GREEN_BORDER = '#136607'
export const CATEGORY_1 = '#007CC2'
export const CATEGORY_2 = '#9a480b'
export const CATEGORY_3 = '#da2eed'
export const BUTTON_COLOR = '#ffffff'
export const BLUE_COLOR = '#2e92cf'
export const STATUS_1 = '#d9d9db'
export const STATUS_2 = '#f4964a'
export const STATUS_3 = '#6dcc64'
export const DEFAULT_COLOR_1 = '#9a480b'
export const DEFAULT_COLOR_2 = '#b0530c'
export const DEFAULT_COLOR_3 = '#d1630f'
export const DEFAULT_COLOR_4 = '#da2eed'
export const DEFAULT_COLOR_5 = '#d743e8'
export const DEFAULT_COLOR_6 = '#d06fdb'
export const GRAY = '#ededed'
//#f77721
//#1f9606